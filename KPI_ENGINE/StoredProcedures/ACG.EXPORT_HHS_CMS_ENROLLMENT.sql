SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [ACG].[EXPORT_HHS_CMS_ENROLLMENT] @status varchar(20)
AS

Begin

/*
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
*/

Declare @rootId INT=159


;with grp_starts as 
	(
	  select 
		patient_id
		,hhs_cms_benefit_begin_date
		,hhs_cms_benefit_end_date
		,case
			when datediff(day,lag(hhs_cms_benefit_end_date) over(partition by patient_id order by hhs_cms_benefit_begin_date, hhs_cms_benefit_end_date),hhs_cms_benefit_begin_date) <=1 	
			then 0 
			else 1
		end grp_start
		,hhs_metal_level
		,cms_factor_type
		,cms_orig_rsn_for_entitlement
	  from 
	  (
			select distinct
				E.EMPI AS patient_id
				,E.EFF_DATE AS hhs_cms_benefit_begin_date
				,E.TERM_DATE AS hhs_cms_benefit_end_date
				--,CASE WHEN en_data_src='MSSP' THEN NULL ELSE CASE WHEN E.HHS_PLAN_METAL_LEVEL IS NULL THEN 'GOLD' END END AS hhs_metal_level
				--,CASE WHEN en_data_src='MSSP' THEN 'CN' END AS cms_factor_type
				,NULL AS hhs_metal_level
				,'CN' AS cms_factor_type
				,Case when E.MEDICARE_BASIS in('10','11') then '0' when E.MEDICARE_BASIS in ('20','21') then '3' when E.MEDICARE_BASIS='31' then '2' else 0 END  AS cms_orig_rsn_for_entitlement
			FROM dbo.enrollment E
			where 
				e.ROOT_COMPANIES_ID=@rootId
				and E.EFF_DATE>='2018-01-01'
			--	and EMPI='1AFA2AFA-F635-4F8F-AD74-60A1A0D4F059'
	  )tbl

	)
	, grps as 
	(
	  select 
		patient_id
		,hhs_cms_benefit_begin_date
		,hhs_cms_benefit_end_date
		,hhs_metal_level
		,cms_factor_type
		,cms_orig_rsn_for_entitlement
		,sum(grp_start) over(partition by patient_id order by hhs_cms_benefit_begin_date, hhs_cms_benefit_end_date) grp
	  from grp_starts
	)
	Select 
		s.EMPI as patient_id
		,'Individual' AS hhs_market
		,'' AS hhs_rating_area
		,'' AS hhs_risk_pool
		,hhs_cms_benefit_begin_date
		,hhs_cms_benefit_end_date
		,hhs_metal_level
		,0 AS hhs_csr_indicator
		,'Y' AS hhs_premium_calc
		,'' AS hhs_cms_enrollment_key
		,cms_factor_type
		,cms_orig_rsn_for_entitlement
		,'N' AS cms_medicaid_eligible
		,'' AS cms_lti_status
		,'' AS cms_pace_status
	From
	(
			select 
				patient_id
				,min(hhs_cms_benefit_begin_date) as hhs_cms_benefit_begin_date
				,max(hhs_cms_benefit_end_date) as hhs_cms_benefit_end_date
				,hhs_metal_level
				,cms_factor_type
				,cms_orig_rsn_for_entitlement
			from grps 
			group by 
				patient_id,grp,hhs_metal_level,cms_factor_type,cms_orig_rsn_for_entitlement
		
	)t2
	Join Rpt.ConsolidatedAttribution_Snapshot s on convert(varchar(100),patient_id)=s.EMPI and  s.Attribution_Type='Ambulatory_PCP' and s.EnrollmentStatus=@status
	order by patient_id,hhs_cms_benefit_begin_date

	




/* adding logic to merge consecutive enrollment lines
Select
 patient_id
,hhs_market
,hhs_rating_area
,hhs_risk_pool
,hhs_cms_benefit_begin_date
,hhs_cms_benefit_end_date
,hhs_metal_level
,hhs_csr_indicator
,hhs_premium_calc
,hhs_cms_enrollment_key
,cms_factor_type
,cms_orig_rsn_for_entitlement
,cms_medicaid_eligible
,cms_lti_status
,cms_pace_status
FROM
(
select 
E.EMPI AS patient_id
,'Individual' AS hhs_market
,'' AS hhs_rating_area
,'' AS hhs_risk_pool
,E.EFF_DATE AS hhs_cms_benefit_begin_date
,E.TERM_DATE AS hhs_cms_benefit_end_date
,CASE WHEN en_data_src='MSSP' THEN NULL ELSE CASE WHEN E.HHS_PLAN_METAL_LEVEL IS NULL THEN 'GOLD' END END AS hhs_metal_level
,0 AS hhs_csr_indicator
,'Y' AS hhs_premium_calc
,'' AS hhs_cms_enrollment_key
,CASE WHEN en_data_src='MSSP' THEN 'CN' END AS cms_factor_type
,Case when E.MEDICARE_BASIS in('10','11') then '0' when E.MEDICARE_BASIS in ('20','21') then '3' when E.MEDICARE_BASIS='31' then '2' END  AS cms_orig_rsn_for_entitlement
,'N' AS cms_medicaid_eligible
,'' AS cms_lti_status
,'' AS cms_pace_status
,ROW_NUMBER() OVER(PARTITION BY E.EMPI,E.EFF_DATE order by E.TERM_DATE desc) AS RN
FROM dbo.enrollment E
Join Rpt.ConsolidatedAttribution_Snapshot s on e.EMPI=s.EMPI and e.EN_DATA_SRC=s.Payer and s.Attribution_Type='Ambulatory_PCP' and s.EnrollmentStatus=@status
where 
	e.ROOT_COMPANIES_ID=@rootId
--where EN_DATA_SRC='MSSP'
--and E.EMPI in (select distinct TOP 500 EMPI from dbo.MEMBER where MEM_DATA_SRC='MSSP' order by EMPI)
--and e.empi='{0015435D-5EFA-4901-8127-C36760E6ECD9}'
and E.EFF_DATE>='2018-01-01'
)A where RN=1
order by patient_id,hhs_cms_benefit_begin_date
*/

/*
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
*/
End
GO
