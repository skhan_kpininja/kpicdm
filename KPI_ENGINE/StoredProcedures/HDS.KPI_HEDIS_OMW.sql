SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROCEDURE [HDS].[KPI_HEDIS_OMW] @rundate date
AS

BEGIN
/*
	CR-1-2022-03-15-Shanawaz : Added logic to exclude deceased members from denominator, this is a non-hedis logic
	KPISQL-797 - 2022-05-15 - SG: Added Logic to add Gap details in KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
*/

DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Declare Variables
--Declare @rundate date=GetDate();
declare @meas_year varchar(4)=Year(@rundate)
declare @rootId varchar(10)='159'


Declare @runid INT=0;
DECLARE @ce_startdt Date;
DECLARE @ce_Intakestartdt Date
DECLARE @ce_enddt Date;
DECLARE @ce_Intakeenddt Date;
DECLARE @ce_startdt1 Date
DECLARE @ce_enddt1 Date;
DECLARE @ce_startdt2 Date;
DECLARE @ce_enddt2 Date;
DECLARE @meas VARCHAR(10);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;


SET @meas='OMW';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_Intakestartdt=concat(@meas_year-1,'-07-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_Intakeenddt=concat(@meas_year,'-06-30');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');

Set @reporttype='Physician'
Set @measurename='Osteoporosis Management in Women Who Had a Fracture'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=67
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='30'


-- Get List of deceased members
--CR-1-2022-03-15-Shanawaz : Added logic to exclude deceased members from denominator, this is a non-hedis logic
Drop table if exists #deceasedmembers
select EMPI into #deceasedmembers from KPI_ENGINE.dbo.deceasedmembers(@rootId,@ce_startdt,@ce_enddt)


-- Identify Female members between age 67 and 85
drop table if exists #omw_popset;
Create table #omw_popset
(
	EMPI varchar(100),
	Gender varchar(10),
	Age INT
)
Insert into #omw_popset
select 
	EMPI_ID
	,Gender
	,Year(@ce_enddt)-Year(Date_of_Birth) as Age 
from KPI_ENGINE.dbo.open_empi_master
Left Outer Join #deceasedmembers d on EMPI_ID=d.EMPI
where 
	Root_Companies_ID=@rootId and 
	Year(@ce_enddt)-Year(Date_of_Birth) between 67 and 85 and 
	Gender='F'
	and d.EMPI is null


-- Create Fracture Set 
-- Identify members who had a fracture between last year and current year
Drop table if exists #omw_fractureset;
Create table #omw_fractureset
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #omw_fractureset
Select distinct
	t1.*
From
(
	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Fractures')

	Union all

	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From KPI_ENGINE.dbo.GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Fractures')
)t1
Join #omw_popset p on
	t1.EMPI=p.EMPI
Where
	PROC_DATA_SRC not in(Select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)




-- Create Inpatient Set
-- Identify members who had an inpatient stay in last year and current year
Drop table if exists #omw_InpatientStaySet;
Create table #omw_InpatientStaySet
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #omw_InpatientStaySet
select 
	i.EMPI
	,FROM_DATE
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
from KPI_ENGINE.dbo.inpatientstays(@rootId,@ce_startdt1,@ce_enddt) i
Join #omw_popset p on
	i.EMPI=p.EMPI
Where
	CL_DATA_SRC not in(Select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)


-- Create OWM Visit Set without Telehealth Modifier
-- Identify visits based on definition
Drop table if exists #omw_visitlist
Create Table #omw_visitlist
(
	EMPI varchar(100),
	FROM_DATE DATE,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #omw_visitlist
Select distinct
	t1.*
From
(

	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From KPI_ENGINE.dbo.GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt,'Outpatient','Telehealth Modifier')
	

	Union all

	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From KPI_ENGINE.dbo.GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt,'Observation','Telehealth Modifier')

	Union all

	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From KPI_ENGINE.dbo.GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt,'ED','Telehealth Modifier')

	Union all

	Select
		EMPI
		,FROM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From KPI_ENGINE.dbo.CLAIMLINE
	Where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		FROM_DATE Between @ce_startdt1 and @ce_enddt and
		REV_CODE in
		(
			Select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Outpatient','ED')
		)

)t1
join #omw_fractureset f on
	t1.EMPI=f.EMPI and
	t1.CLAIM_ID=f.CLAIM_ID and
	t1.PROC_DATA_SRC=f.DATA_SRC
Where
	PROC_DATA_SRC not in(Select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)


--Identify all Visits with Telehealth POS to exclude from visit list(#omw_visitlist)
Drop table if exists #omw_telePOSvisits
Create Table #omw_telePOSvisits
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #omw_telePOSvisits
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From KPI_ENGINE.dbo.CLAIMLINE
Where 
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt1 and @ce_enddt and
	ISNULL(POS,'0') in
	(
		Select code from KPI_ENGINE.HDS.ValueSet_TO_Code where Value_Set_name='Telehealth POS'
	)




-- Identify Direct Transfers
-- Merge all consecutive stays
drop table if exists #omw_directtransfers;
Create table #omw_directtransfers
(
    EMPI varchar(100),
    ADM_DATE Date,
    DIS_DATE Date,
    AdmissionClaimId varchar(100),
    DischargeClaimId varchar(100),
    grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
    EMPI
    ,ADM_DATE
    ,DIS_DATE
    ,CLAIM_ID
    ,case
        when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1     
        or 
        (datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
        then 0 
        else 1
    end grp_start
  from #omw_InpatientStaySet
  
)
, grps as 
(
  select 
    EMPI
    ,ADM_DATE
    , DIS_DATE
    ,CLAIM_ID
    ,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #omw_directtransfers
select 
    t1.EMPI
    ,AdmissionDate
    ,DischargeDate
    ,AdmissionClaimId
    ,DischargeClaimId
    ,grp_claimid 
from    
(
            select 
                EMPI
                ,min(ADM_DATE) as AdmissionDate
                ,max(DIS_DATE) as DischargeDate
                ,min(CLAIM_ID) as AdmissionClaimId
                ,max(CLAIM_ID) as DischargeClaimId
                ,STRING_AGG(CLAIM_ID,',') as grp_claimid
            from grps 
            group by EMPI,grp
        
)t1



-- Create a normalized transfer table to improve lookup,storing the comma separated claim ids in columns for performance
drop table if exists #omw_directtransfers_normalized;
Create table #omw_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #omw_directtransfers_normalized
select 
	p.* 
from 
(
	select 
		* 
	from #omw_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p




-- discharge with transfers and adjusting the outpatient to inpatient dates
-- If an outpatient visit with fracture results in inpatient stay then adjust the Episode date to be the outpatient visit date instead of admission date
drop table if exists #omw_adjustedstays
Create table #omw_adjustedstays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #omw_adjustedstays
Select
	s.EMPI
	,Case
		When v.FROM_DATE is not null Then v.FROM_DATE
		else s.ADM_DATE
	end as ADM_DATE
	,s.DIS_DATE
	,AdmissionClaimId
	,DischargeClaimId
	,CLAIM1
	,CLAIM2
	,CLAIM3
	,CLAIM4
	,CLAIM5
	
From #omw_directtransfers_normalized s
left outer Join #omw_visitlist v on 
		s.EMPI=v.EMPI and
		(
			DATEDIFF(day,v.FROM_DATE,s.ADM_DATE) between 0 and 1 
			or 
			v.CLAIM_ID in(s.CLAIM1,s.CLAIM2,s.CLAIM3,s.CLAIM4,s.CLAIM5)
		)
		and
		s.DIS_DATE!=v.FROM_DATE
Join #omw_fractureset f on
	s.EMPI=f.EMPI and
	f.CLAIM_ID=s.DischargeClaimId
	
--Where s.EMPI=159656

-- Create Ambulatory Visit Set
-- Identify members who had a fracture on specific set of visits as per definition
Drop table if exists #omw_Ambulatoryvstset;
Create table #omw_Ambulatoryvstset
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #omw_Ambulatoryvstset
Select
	v.*
From #omw_visitlist v
-- excluding telehealth POS visits
left outer join #omw_telePOSvisits tv on
	v.EMPI=tv.EMPI and
	v.CLAIM_ID=tv.CLAIM_ID and
	v.DATA_SRC=tv.CL_DATA_SRC
-- excluding any outpatient visit that leads to an inpattient stay
Left outer Join #omw_directtransfers i on
	v.EMPI=i.EMPI and
	(
		datediff(day,v.FROM_DATE,i.ADM_DATE) between 0 and 1 
		or 
		v.FROM_DATE between i.ADM_DATE and i.DIS_DATE 
		or 
		v.CLAIM_ID=i.AdmissionClaimId
	)
Where
	tv.EMPI is null and
	i.EMPI is null


	
	






-- Identify Visits and Stays with Fractures

-- Compiling Episode Dates for Members from Ambulatory and Inpatient Stays between the intake period and identifying the index episode date
Drop Table if Exists #omw_episodedates;
Create table #omw_episodedates
(
	EMPI varchar(100),
	StartDate Date,
	EpisodeDate Date,
	IESD Date
)

;with CTE_Episodes as 
(
	Select distinct	
		*
	From
	(
		Select
			a.EMPI
			,a.FROM_DATE as StartDate
			,a.FROM_DATE as EpisodeDate
		From #omw_Ambulatoryvstset a
		

		Union all

		Select
			s.EMPI
			,s.ADM_DATE as StartDate
			,s.DIS_DATE as EpisodeDate
		From #omw_adjustedstays s
		
	)t1

)
,CTE_IESD as
(
	select 
		EMPI
		,min(EpisodeDate) as IESD 
	from CTE_Episodes 
	where 
		EpisodeDate between @ce_Intakestartdt and @ce_Intakeenddt
		group by 
			EMPI
)
Insert into #omw_episodedates
select 
	t1.*
	,t2.IESD 
from CTE_Episodes t1
join CTE_IESD t2 on 
	t1.EMPI=t2.EMPI
where 
	t1.EpisodeDate between @ce_Intakestartdt and @ce_Intakeenddt
	
	-- select * from #omw_episodedates  where EMPi=155646

--Negative Diagnosis History
-- Identify Visits with fracture diagnosis as per Negative Diagnosis History directions

Drop table if exists #omw_NegDiagHistVstListwoexcl;
Create table #omw_NegDiagHistVstListwoexcl
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
;With CTE_AmbulatoryExcl as
(

	
	Select distinct
		*
	From
	(

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')
		--Where EMPI=145800

		Union all


		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all


		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')
		
		Union all 


		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,FROM_DATE
			,CLAIM_ID
			,CL_DATA_SRC
		From KPI_ENGINE.dbo.CLAIMLINE
		Where
			ROOT_COMPANIES_ID=@rootId and
			ISNULL(POS,'0')!='81' and
			FROM_DATE Between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				Select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Outpatient','ED')
			)

	)t1
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)

	
)
Insert into #omw_NegDiagHistVstListwoexcl
Select 
	t1.* from 
CTE_AmbulatoryExcl t1
join #omw_fractureset f on 
	t1.EMPI=f.EMPI and 
	t1.CLAIM_ID=f.CLAIM_ID and
	t1.PROC_DATA_SRC=f.DATA_SRC
--Where t1.EMPI=145800



-- Negative Disgnosis History Visit List
-- Excluding any visit that leads to stay
Drop table if exists #omw_NegDiagHistVstList;
Create table #omw_NegDiagHistVstList
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #omw_NegDiagHistVstList
Select 
	t1.*
from #omw_NegDiagHistVstListwoexcl t1
left outer join #omw_InpatientStaySet i on 
	t1.EMPI=i.EMPI and 
	(
		datediff(day,t1.FROM_DATE,i.ADM_DATE) between 0 and 1 
		or 
		t1.FROM_DATE between i.ADM_DATE and i.DIS_DATE 
		or 
		t1.CLAIM_ID=i.CLAIM_ID
	)
	--and
	--t1.FROM_DATE!=i.DIS_DATE
where 
	i.EMPI is null
	


	-- select * from #omw_NegDiagHistVstList where EMPI=143683
	-- select * from #omw_NegDiagHistVstListwoexcl where EMPI=143683
	-- select * from #omw_EpisodeDatesWNMH where EMPI=155646
	-- select * from #omw_episodedates where EMPI=155646


-- Exclude Records with Positive Diagnosis History 
Drop table if exists #omw_EpisodeDatesWNMH;
Create table #omw_EpisodeDatesWNMH
(
	EMPI varchar(100),
	StartDate Date,
	EpisodeDate Date,
	IESD Date
)
Insert into #omw_EpisodeDatesWNMH
select distinct
	e.* 
from #omw_episodedates e
left outer join #omw_NegDiagHistVstList v on
	e.EMPI=v.EMPI and 
	v.FROM_DATE between DATEADD(day,-60,e.StartDate) and DateAdd(day,-1,e.StartDate) and
	e.EpisodeDate!=v.FROM_DATE
left outer join #omw_adjustedstays s on 
	e.EMPI=s.EMPI and 
	s.DIS_DATE between DATEADD(day,-60,e.StartDate) and DateAdd(day,-1,e.StartDate)
where 
	v.EMPI is null and 
	s.EMPI is null 	


-- Adding Payer Mapping, after completing steps  1 and 2, creating a dataset for further steps
-- identifying payer at the end of continuous enrollment , handling double enrollment cases and translating payer codes as per Hedis direction
-- Create OMW Dataset
drop table if exists #omwdataset;
CREATE TABLE #omwdataset (
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [Gender] varchar(45) NOT NULL,
  [Age] INT,
  [Rexcl] smallint DEFAULT '0',
  [Rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [Excl] smallint DEFAULT '0',
  [Num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'
  
  
) ;
Insert into #omwdataset(EMPI,Meas,Payer,Age,Gender)
Select distinct
	EMPI
	,'OMW'
	,pm.PayerMapping
	
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(

				Select
					*
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn
				From
				(
					select distinct
						en.EMPI
						,EFF_DATE as StartDate
						,TERM_DATE as EndDate
						,PAYER_TYPE as payer
						,gender
						,age
					 
					from KPI_ENGINE.dbo.ENROLLMENT en
					join #omw_EpisodeDatesWNMH e on
						en.EMPI=e.EMPI
					join #omw_popset p on
						en.EMPI=p.EMPI
					where
						en.ROOT_COMPANIES_ID=@rootId and
						en.EFF_DATE<=dateadd(day,180,e.EpisodeDate) and 
						en.TERM_DATE>=dateadd(Month,-12,e.EpisodeDate) and 
						EFF_DATE<=@ce_enddt
					--	and EMPI=95066
				)z1
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join KPI_ENGINE.HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='OMW'
Order by 1





---- Continuous Enrollment
-- Identifying if the member is enrolled continuously as per definition
DROP TABLE IF EXISTS #omw_contenroll;
CREATE table #omw_contenroll
(
	EMPI varchar(100),
	EpisodeDate Date
		
);
With coverage_CTE(EMPI,lastcoveragedate,Startdate,Finishdate,nextcoveragedate,EpisodeDate) as
(
	
	select distinct
		ed.EMPI
		,isnull(lag(en.TERM_DATE,1) over(partition by ed.EMPI order by en.EFF_DATE,en.TERM_DATE desc),convert(varchar,cast(dateadd(Month,-12,ed.EpisodeDate) as date),23)) as lastcoveragedate
		,Case 
			when en.EFF_DATE<dateadd(MONTH,-12,ed.EpisodeDate) then convert(varchar,cast(dateadd(MONTH,-12,ed.EpisodeDate) as date),23)
			else en.EFF_DATE 
		end as Startdate
		,case 
			when en.TERM_DATE>dateadd(day,180,ed.EpisodeDate) then convert(varchar,cast(dateadd(day,180,ed.EpisodeDate) as date),23)
			else TERM_DATE 
		end as Finishdate
		,isnull(lead(en.EFF_DATE,1) over(partition by ed.EMPI order by en.EFF_DATE,en.TERM_DATE),convert(varchar,cast(dateadd(day,180,ed.EpisodeDate) as date),23)) as nextcoveragedate
		,ed.EpisodeDate 
		from #omw_EpisodeDatesWNMH ed
		Join
		(
			Select distinct
				EMPI
				,EFF_DATE
				,TERM_DATE
				,DRUG_BENEFIT
			From ENROLLMENT
			Where
				ROOT_COMPANIES_ID=@rootId
		)en on 
			ed.EMPI=en.EMPI and 
			en.DRUG_BENEFIT=1 and 
			en.EFF_DATE<=dateadd(day,180,ed.EpisodeDate) and 
			en.TERM_DATE>=dateadd(MONTH,-12,ed.EpisodeDate)
	
)
Insert into #omw_contenroll
select 
	EMPI
	,EpisodeDate 
from
(
	select 
		*
		,case 
			when rn=1 and startdate>dateadd(MONTH,-12,EpisodeDate) then 1 
			else 0 
		end as startgap
		,case 
			when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
			else 0 
		end as gaps
		,datediff(day,Startdate,finishdate) as coveragedays
		,case 
			when EpisodeDate between Startdate and newfinishdate then 1 
			else 0 
		end as anchor 
		from
		(
			
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI order by Startdate,FinishDate),convert(varchar,cast(dateadd(day,180,EpisodeDate) as date),23)) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by EMPI order by Startdate,Finishdate) as rn 
			from coverage_CTE
		)t1                        
	)t2 
group by 
	EMPI
	,EpisodeDate 
having
	(sum(gaps)+sum(startgap)<=1) and 
	sum(coveragedays)>=(datediff(day,dateadd(Month,-12,EpisodeDate),dateadd(day,180,EpisodeDate))-45) and
	sum(anchor)>0


update #omwdataset set CE=1 from #omwdataset ds join #omw_contenroll ce on ds.EMPI=ce.EMPI



-- Required Exclusions
--?	Members who had a BMD test (Bone Mineral Density Tests Value Set) during the 730 days (24 months) prior to the Episode Date
-- Bone Mineral Density Tests
Drop table if Exists #omw_BMDTSet
Create Table #omw_BMDTSet
(
	EMPI Varchar(100),
	EpisodeDate Date
)
;With CTE_BMDT as
(
	Select distinct
		*
	From
	(
		Select
			EMPI
			,PROC_START_DATE as ServiceDate
		From KPI_ENGINE.dbo.GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Bone Mineral Density Tests')

		Union all

		Select 
			EMPI
			,PROC_START_DATE as ServiceDate
		From KPI_ENGINE.dbo.GetICDPCS(@rootId,'1900-01-01',@ce_enddt,'Bone Mineral Density Tests')

		Union all

		Select 
			EMPI
			,DIAG_START_DATE as ServiceDate
		From KPI_ENGINE.dbo.GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Bone Mineral Density Tests')

	)t1
		
)
Insert into #omw_BMDTSet
select 
	b.EMPI
	,e.EpisodeDate 
from #omw_EpisodeDatesWNMH e
join CTE_BMDT b on 
	e.EMPI=b.EMPI and 
	b.servicedate between DATEADD(day,-730,e.StartDate) and DATEADD(day,-1,e.StartDate)




--?	Members who had a claim/encounter for osteoporosis therapy (Osteoporosis Medication Therapy Value Set) during the 365 days (12 months) prior to the Episode Date.

Drop table if exists #omw_OsteoporosisSet
Create Table #omw_OsteoporosisSet
(
	EMPI varchar(100),
	EpisodeDate Date
)
;With CTE_Osteo as
(
	Select distinct 
		*
	From
	(
		-- --?	Members who had a claim/encounter for osteoporosis therapy (Osteoporosis Medication Therapy Value Set) during the 365 days (12 months) prior to the Episode Date.
		Select
			EMPI
			,PROC_START_DATE as servicedate
			,PROC_START_DATE as Enddate
		From KPI_ENGINE.dbo.GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Osteoporosis Medication Therapy')
		
		Union all

		-- ?	Members who received a dispensed prescription or had an active prescription to treat osteoporosis (Osteoporosis Medications List) during the 365 days (12 months) prior to the Episode Date. 

		Select
			EMPI
			,FILL_DATE as servicedate
			,convert(varchar,cast(dateadd(day,cast(cast(SUPPLY_DAYS as numeric) as INT),FILL_DATE) as date),23) as Enddate
		From KPI_ENGINE.dbo.MEDICATION
		where
			ROOT_COMPANIES_ID=@rootId and
			MEDICATION_CODE in
			(
				select code from KPI_ENGINE.HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Osteoporosis Medications'
			)
			and 
			cast(cast(SUPPLY_DAYS as numeric) as INT)>=0
	)t1
)
Insert into #omw_OsteoporosisSet
select distinct 
	b.EMPI
	,e.episodedate 
from #omw_EpisodeDatesWNMH e
join CTE_Osteo b on 
	e.EMPI=b.EMPI and 
	(
		b.servicedate between DATEADD(day,-366,e.StartDate) and DATEADD(day,-1,e.StartDate) 
		or 
		b.enddate between DATEADD(day,-366,e.StartDate) and DATEADD(day,-1,e.StartDate)
	) 
--where e.memid=100025         

--
drop table if exists #omw_reqdexcl
create table #omw_reqdexcl
(
	EMPI varchar(100)
)
Insert into #omw_reqdexcl
select distinct 
	f2.EMPI 
from
(
	select 
		f1.* 
	from #omw_EpisodeDatesWNMH f1
	left outer join
	(

		select 
			e.EMPI
			,e.EpisodeDate
			,e.ct 
		from
		(
			select 
				*
				,count(EMPI) over(partition by EMPI) as ct 
			from #omw_EpisodeDatesWNMH
		)e
		join 
		(
			Select 
				*
				,count(EMPI) over(partition by EMPI) as ct
				,DENSE_RANK() over(partition by EMPI order by episodedate) as rn 
				from
				(
					select distinct 
						EMPI
						,EpisodeDate 
					from
					(
						select 
							* 
						from #omw_BMDTSet
						
						Union all
						
						Select 
							* 
						from #omw_OsteoporosisSet
					)d1
				)t1
			)t2 on 
				e.EMPI=t2.EMPI and 
				e.EpisodeDate=t2.EpisodeDate  
				and e.ct>1
	
		)t3 on 
			f1.EMPI=t3.EMPI and 
			f1.EpisodeDate=t3.EpisodeDate
		where t3.EMPI is null
	)f2
	join
	(
		select distinct 
			EMPI
			,EpisodeDate 
		from
		(
			select 
				* 
			from #omw_BMDTSet
			
			Union all
			
			Select 
				* 
			from #omw_OsteoporosisSet
		
		)d2
	)t4 on 
		f2.EMPI=t4.EMPI and 
		f2.EpisodeDate=t4.EpisodeDate


-- Palliative Care
--?	Members who received palliative care (Palliative Care Assessment Value Set; Palliative Care Encounter Value Set; Palliative Care Intervention Value Set) during the intake period through the end of the measurement year.
Drop table if exists #omw_palliativecare;
Create Table #omw_palliativecare
(
	EMPI varchar(100)
)
Insert into #omw_palliativecare
select
	EMPI
From KPI_ENGINE.dbo.palliativecare(@rootId,@ce_Intakestartdt,@ce_enddt)

update #omwdataset set Rexcld=1 from #omwdataset ds join #omw_palliativecare t1 on ds.EMPI=t1.EMPI
update #omwdataset set Rexcld=1 from #omwdataset ds join #omw_reqdexcl t1 on ds.EMPI=t1.EMPI




--Rexcl Logic
	-- AdvancedIllness

	drop table if exists #omw_advillness;
	CREATE table #omw_advillness
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #omw_advillness
	select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID 
	from KPI_ENGINE.dbo.advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)
	
	-- Members with Institutinal SNP

	UPDATE ds SET 
		ds.rexcl=1 
	FROM #omwdataset ds 
	JOIN KPI_ENGINE.dbo.ENROLLMENT s on 
		ds.EMPI=s.EMPI and 
		s.Root_Companies_ID=@rootId 
	WHERE  
		ds.age>=67 AND 
		ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MC','MR') AND 
		s.EFF_DATE<=@ce_enddt AND 
		s.TERM_DATE>=@ce_Intakestartdt AND 
		s.Payer_Type='SN2';


	-- LTI Exclusion
	
	drop table if exists #omw_LTImembers;
	CREATE table #omw_LTImembers
	(
		EMPI varchar(100)
				
	);
	Insert into #omw_LTImembers
	Select
		EMPI
	From KPI_ENGINE.dbo.LTImembers(@rootId,@ce_Intakestartdt,@ce_enddt)


	
	update ds set rexcl=1 from #omwdataset ds join #omw_LTImembers re on ds.EMPI=re.EMPI where ds.age>=67 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MC','MR');

	   
	-- Hospice Exclusion

	drop table if exists #omw_hospicemembers;
	CREATE table #omw_hospicemembers
	(
		EMPI varchar(100)
		
	);
	Insert into #omw_hospicemembers
	select
		EMPI
	From KPI_ENGINE.dbo.hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	
	update ds set ds.rexcl=1 from #omwdataset ds join #omw_hospicemembers hos on hos.EMPI=ds.EMPI;


	
			
	-- Frailty Members LIST
	
	drop table if exists #omw_frailtymembers;
	CREATE table #omw_frailtymembers
	(
		
		EMPI varchar(100)
			
	);
	Insert into #omw_frailtymembers
	Select distinct
		EMPI
	From KPI_ENGINE.dbo.Frailty(@rootId,@ce_Intakestartdt,@ce_enddt)
	Where
		DataSource not in(select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)

	update ds Set ds.Rexcl=1 from #omwdataset ds join #omw_frailtymembers f on ds.EMPI=f.EMPI and ds.age>=81;

	

	-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #omw_inpatientstaylist;
	CREATE table #omw_inpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #omw_inpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from KPI_ENGINE.dbo.Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)

	
	
	-- Non acute Inpatient stay list
	drop table if exists #omw_noncauteinpatientstaylist;
	CREATE table #omw_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #omw_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from KPI_ENGINE.dbo.nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1)

	
	-- Outpatient and other visits
	drop table if exists #omw_requiredexclvisitlist;
	CREATE table #omw_requiredexclvisitlist
	(
		EMPI varchar(100),
		FROM_DATE DATE,
		CLAIM_ID varchar(100)
	);
	Insert into #omw_requiredexclvisitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
		from KPI_ENGINE.dbo.CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from KPI_ENGINE.dbo.DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1


	
	
	-- Required exclusion table
	
	drop table if exists #omw_reqdexcl1;
	CREATE table #omw_reqdexcl1
	(
		EMPI varchar(100)
			
	);
	Insert into #omw_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.FROM_DATE 
			from
			(
				select 
					EMPI
					,FROM_DATE
					,CLAIM_ID  
				from #omw_requiredexclvisitlist 
				
				union all
				
				select 
					na.EMPI
					,na.FROM_DATE
					,na.CLAIM_ID 
				from #omw_noncauteinpatientstaylist na
				join #omw_inpatientstaylist inp on 
					na.EMPI=inp.EMPI and 
					na.CLAIM_ID=inp.CLAIM_ID
	
		)t1
		Join #omw_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2 
	group by 
		t2.EMPI 
		having 
			count(t2.EMPI)>1
)t3 
Join #omw_frailtymembers f on 
	f.EMPI=t3.EMPI
	
	
	update ds set rexcl=1 from #omwdataset ds
	join #omw_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age BETWEEN 67 AND 80;

	
	-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #omw_reqdexcl2;
	CREATE table #omw_reqdexcl2
	(
		EMPI varchar(100)
				
	);
	insert into #omw_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(

			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From KPI_ENGINE.dbo.GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')

		)t1
		Join #omw_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #omw_frailtymembers f on 
		f.EMPI=t2.EMPI

		
	
	update ds set ds.rexcl=1 from #omwdataset ds
	join #omw_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age BETWEEN 67 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #omw_reqdexcl3;
	CREATE table #omw_reqdexcl3
	(
		EMPI varchar(100)
			
	);
	insert into #omw_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.FROM_DATE
			,t1.CLAIM_ID 
		from
		(
			select 
				inp.EMPI
				,inp.FROM_DATE
				,inp.CLAIM_ID 
			from #omw_inpatientstaylist inp
			left outer join #omw_noncauteinpatientstaylist na on 
				inp.EMPI=na.EMPI and 
				inp.CLAIM_ID=na.CLAIM_ID
			where 
				na.EMPI is null
		)t1
		join #omw_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #omw_frailtymembers f on 
		f.EMPI=t2.EMPI

	
	update ds set ds.rexcl=1 from #omwdataset ds
	join #omw_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age BETWEEN 67 AND 80;

		
	-- RequiredExcl 4
	drop table if exists #omw_reqdexcl4;
	CREATE table #omw_reqdexcl4
	(
		EMPI varchar(100)
				
	);
	insert into #omw_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		Select
			EMPI
		From KPI_ENGINE.dbo.Medication
		Where
			Root_Companies_ID=@rootId and
			MED_DATA_SRC not in(Select Data_source from KPI_ENGINE.dbo.DATA_SOURCE where Supplemental=1) and
			FILL_DATE between @ce_startdt1 and @ce_enddt and
			MEDICATION_CODE in
			(
				select code from KPI_ENGINE.HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)

	
	)t1
	Join #omw_frailtymembers f on 
		f.EMPI=t1.EMPI;

	update ds set ds.rexcl=1 from #omwdataset ds
	join #omw_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age BETWEEN 67 AND 80;



-- Numerator Logic
	-- Identifying IESD as per the logic
	-- ?  Step 4 required exclusions exclude Episode Dates from the event/diagnosis. If a member has an eligible Episode Date (i.e., meets steps 1 - 3) and meets the step 4 required exclusion, the member should be flagged as Event = 1, RExclD = 1. If a member has two eligible Episode Dates (i.e., meets steps 1 -3) and meets the step 4 required exclusion for their first Episode but the second is still eligible, the member should be reported as Event = 1, RExclD = 0.
	-- ?  If members have two events and meets step 4 required exclusion for both events, the member should be reported based on the first episode date during the intake period. The member should be flagged as Event = 1 and RExclD = 1, based on the earliest episode date.
	drop table if Exists #omw_IESD
	Create Table #omw_IESD
	(	
		EMPI varchar(100),
		IESD Date
	)
	;with CTE_exclusions as
	(
		select distinct 
			EMPI
			,EpisodeDate
		from
		(
					
			select 
				* 
			from #omw_BMDTSet
			--Where EMPI=175972
					
			Union all

			Select 
				* 
			from #omw_OsteoporosisSet
			--Where EMPI=175972

			Union all

			select 
				e.EMPI
				,e.EpisodeDate 
			from #omw_EpisodeDatesWNMH  e
			left outer join  #omw_contenroll ce on 
				e.EMPI=ce.EMPI and 
				e.EPisodedate=ce.episodedate
			where 
				ce.EMPI is null
			--	and e.EMPI=175972		
				/*
			Union all
		
			select 
				e.EMPI
				,e.EpisodeDate 
			from #omw_EpisodeDatesWNMH  e
			join  #omw_palliativecare p on 
				e.EMPI=p.EMPI 
		--	Where e.EMPI=138205
				*/		
		)d1
					
	),CTE_Inclusion as
	(
		select 
			e.EMPI
			,e.EpisodeDate
		from
		(
			select 
				*
				,count(EMPI) over(partition by EMPI) as ct 
			from #omw_EpisodeDatesWNMH
		)e
		left outer join CTE_exclusions excl on 
			e.EMPI=excl.EMPI and 
			e.EpisodeDate=excl.EpisodeDate 
		where 
			excl.EMPI is null
	)
	Insert into #omw_IESD
	select 
		EMPI
		,min(Episodedate) as IESD 
	from
	(
		select 
			* 
		from CTE_exclusions 
		where 
			EMPI not in
			(
				select 
					EMPI 
				from CTE_Inclusion
			)
		
		Union all
		
		select 
			* 
		from CTE_Inclusion
	)t1 
	group by 
		EMPI
	--having Memid=158800


	

	-- Bone Density

	--?	A BMD test (Bone Mineral Density Tests Value Set), in any setting, on the IESD or in the 180-day (6-month) period after the IESD. 
	--?	If the IESD was an inpatient stay, a BMD test (Bone Mineral Density Tests Value Set) during the inpatient stay. 
	-- Bone Mineral Density Tests
	Drop table if Exists #omw_NUM_BMDTSet
	Create Table #omw_NUM_BMDTSet
	(
		EMPI Varchar(100),
		EpisodeDate Date,
		ServiceDate Date,
		Code varchar(20)

	)
	;With CTE_BMDT as
	(
		Select distinct 
			EMPI
			,servicedate 
			,Code
		from
		(
			
				Select
					EMPI
					,PROC_START_DATE as ServiceDate
					,PROC_CODE as Code
				From KPI_ENGINE.dbo.GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Bone Mineral Density Tests')

				Union all

				Select 
					EMPI
					,PROC_START_DATE as ServiceDate
					,ICDPCS_CODE as Code
				From KPI_ENGINE.dbo.GetICDPCS(@rootId,'1900-01-01',@ce_enddt,'Bone Mineral Density Tests')

				Union all

				Select 
					EMPI
					,DIAG_START_DATE as ServiceDate
					,DIAG_CODE as Code
				From KPI_ENGINE.dbo.GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Bone Mineral Density Tests')

		)t1
	--	where memid=143020
	)
	Insert into #omw_NUM_BMDTSet
	select 
		ie.EMPI
		,ie.IESD
		,b.servicedate
		,b.Code
	from #omw_IESD ie
	join #omw_EpisodeDatesWNMH e on 
		ie.EMPI=e.EMPI and 
		ie.IESD=e.EpisodeDate
	join CTE_BMDT b on 
		ie.EMPI=b.EMPI and 
		b.servicedate between e.StartDate and DATEADD(day,180,ie.IESD)
	--where ie.Memid=143020

	
	

	-- Osteoporosis

	
	Drop table if exists #omw_NUM_OsteoporosisSet
	Create Table #omw_NUM_OsteoporosisSet
	(
		EMPI varchar(100),
		IESD Date,
		ServiceDate Date,
		Code varchar(20)
	)

	;With CTE_Osteo as
	(
		--?	Osteoporosis therapy (Osteoporosis Medication Therapy Value Set) on the IESD or in the 180-day (6-month) period after the IESD.
		select distinct 
			EMPI
			,Servicedate
			,Enddate 
			,Code
		from
		(
			Select
				EMPI
				,PROC_START_DATE as servicedate
				,PROC_START_DATE as Enddate
				,PROC_CODE as Code
			From KPI_ENGINE.dbo.GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Osteoporosis Medication Therapy')
		
			Union all

			-- ?	A dispensed prescription to treat osteoporosis (Osteoporosis Medications List) on the IESD or in the 180-day (6-month) period after the IESD
			Select
				EMPI
				,FILL_DATE as servicedate
				,convert(varchar,cast(dateadd(day,cast(cast(SUPPLY_DAYS as numeric) as INT),FILL_DATE) as date),23) as Enddate
				,MEDICATION_CODE as Code
			From KPI_ENGINE.dbo.MEDICATION
			where
				ROOT_COMPANIES_ID=@rootId and
				MEDICATION_CODE in
				(
					select code from KPI_ENGINE.HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Osteoporosis Medications'
				)
				and 
				cast(cast(SUPPLY_DAYS as numeric) as INT)>=0

		)t1
		--where memid=158800
	)
	Insert into #omw_NUM_OsteoporosisSet
	select 
		b.EMPI
		,ie.IESD 
		,b.ServiceDate
		,b.Code
	from #omw_IESD ie
	join #omw_EpisodeDatesWNMH e on 
		ie.EMPI=e.EMPI and 
		ie.IESD=e.EpisodeDate
	join CTE_Osteo b on 
		e.EMPI=b.EMPI and 
		b.servicedate between ie.IESD and DATEADD(day,180,ie.IESD)
	--where ie.EMPI=170588

	

	
	
	-- Long Osteporosis
	-- ?	If the IESD was an inpatient stay, long-acting osteoporosis therapy (Long-Acting Osteoporosis Medications Value Set) during the inpatient stay.
	Drop table if exists #omw_NUM_LongOsteoporosisSet
	Create Table #omw_NUM_LongOsteoporosisSet
	(
		EMPI varchar(100),
		IESD Date,
		ServiceDate Date,
		Code varchar(20)
	)

	;With CTE_Osteo as
	(

		Select distinct
			EMPI
			,PROC_START_DATE as servicedate
			,PROC_START_DATE as Enddate
			,PROC_CODE as Code
		From KPI_ENGINE.dbo.GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Long-Acting Osteoporosis Medications')

	)
	Insert into #omw_NUM_LongOsteoporosisSet
	select 
		ie.* 
		,b.servicedate
		,b.Code
	from #omw_EpisodeDatesWNMH e
	join #omw_IESD ie on 
		e.EMPI=ie.EMPI and 
		e.EpisodeDate=ie.IESD
	join CTE_Osteo b on 
		e.EMPI=b.EMPI and 
		b.servicedate between e.StartDate and e.EpisodeDate

	Update ds Set Num=1 from #omwdataset ds join #omw_NUM_LongOsteoporosisSet n on ds.EMPI=n.EMPI
	Update ds Set Num=1 from #omwdataset ds join #omw_NUM_OsteoporosisSet n on ds.EMPI=n.EMPI
	Update ds Set Num=1 from #omwdataset ds join #omw_NUM_BMDTSet n on ds.EMPI=n.EMPI



	-- Create Numerator Details
	Drop table if exists #omw_numdetails;
	Create Table #omw_numdetails
	(
		EMPI varchar(100),
		EpisodeDate Date,
		ServiceDate Date,
		Code varchar(20),
		Rownumber INT
	)
	Insert into #omw_numdetails
	Select
		*
	From
	(

		Select
			*
			,ROW_NUMBER() over(partition by EMPI order by EpisodeDate desc,servicedate desc) as rn
		From
		(
			Select
				*
			From #omw_NUM_BMDTSet

			Union all

			select 
				*
			From #omw_NUM_OsteoporosisSet

			Union all

			Select
				*
			from #omw_NUM_LongOsteoporosisSet
		)t1
	)t2
	where 
		rn=1


		--Shanawaz:2022-04-01:Adding OptionalExclusion members table logic
	-- OptionalExclusions: Patients marked as having no Fracture by UHN

	drop table if exists #omw_optexcl_tbl
	select distinct EMPI into #omw_optexcl_tbl  from KPI_ENGINE.DBO.OptionalExclusions oet 
	where oet.ExclusionCode = 'Excl-NoFracture' and oet.ExclusionReason = 'No Fracture' and ROOT_COMPANIES_ID = @rootId and ExclusionDate between @ce_startdt and @ce_enddt
	
	update #omwdataset set excl = 1 from #omwdataset ds
	join #omw_optexcl_tbl oetbl on oetbl.EMPI = ds.EMPI


-- Get ReportId from Report_Details

	exec KPI_ENGINE.dbo.GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output
	

	Delete from KPI_ENGINE.HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into KPI_ENGINE.HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN Event=1 and CE=1  AND rexcl=0 and rexcld=0 and payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MC','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,Gender AS gender,@meas,@meas_year,@reportId,@rootId FROM #omwdataset

	


	-- Insert data into Measure Detailed Line

	Delete from KPI_ENGINE.RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into KPI_ENGINE.RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService,EpisodeDate)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,case
			when Rexcl=1 or rexcld=1 then 1
			else 0
		end as Rexcl
		,CE
		,Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0 and Event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
		,ie.IESD
	From #omwdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #omw_numdetails nd on d.EMPI=nd.EMPI
	Left outer Join #omw_iesd ie on d.EMPI=ie.EMPI
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from KPI_ENGINE.RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into KPI_ENGINE.RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,MYM.Target
	From KPI_ENGINE.RPT.MEASURE_DETAILED_LINE m
	Join KPI_ENGINE.RPT.ConsolidatedAttribution_MultiYearSnapshot a on
		m.EMPI=a.EMPI
		AND m.report_Id = a.reportId
	Join KPI_ENGINE.RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	JOIN KPI_ENGINE.RFT.MeasureYearMapping MYM
		ON L.Measure_id = MYM.MeasureId
		AND MYM.LatestReportId = @reportId
	Join KPI_ENGINE.RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target

	-- Adding logic to identify OMW opportunities

	Delete from KPI_ENGINE_MART.RPT.GapSupportingReport where Root_companies_id=@rootId and Measure_id=@measure_id and reportId = @reportId;
	insert into KPI_ENGINE_MART.RPT.GapSupportingReport(EMPI,ServiceDate,Measure_id,ROOT_COMPANIES_ID,ReportId)
	select distinct
		i.*,@measure_id,@rootId,@reportId
	from #omw_IESD i
	Join #omw_Ambulatoryvstset a on i.EMPI=a.EMPI and i.IESD=a.FROM_DATE
	Join #omwdataset d on i.EMPI=d.EMPI and d.Excl=0 and d.Rexcl=0 and d.Num=0
	where
		Dateadd(d,180,i.IESD) between dateadd(d,30,GetDate()) and @ce_enddt

--KPISQL-797: Added below DELETE/INSERT code block to insert gap data in the KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS table

	DELETE KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
	WHERE 1=1
		AND measureid = @measure_id
		AND REPORTID = @REPORTID
	
	INSERT INTO KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
	(EMPI,root_companies_id,MeasureId,NotAGap,ImpactDate,Gap_Reason,AddDetails1,AddValue1,AddDetails2,AddValue2,RefreshDate,ReportId,AdditionalDetails)
	SELECT 
		EMPI
		,ROOT_COMPANIES_ID
		,MEASURE_ID
		,0 AS NOTAGAP
		,@ce_enddt AS IMPACTATE
		,'No Action taken after Fracture' as Gap_Reason
		,'Fracture Date' as AddDetails1
		,EpisodeDate as AddValue1
		,'Impact Date' as AddDetails1
		,Dateadd(d, 180, EpisodeDate) as AddValue2
		,@rundate as RefreshDate
		,@reportId
		,NULL AS AdditionalDetails
	FROM KPI_ENGINE.RPT.Measure_DetailLine_MultiYearSnapshot
	WHERE 1=1
	AND measure_id = @measure_id
	AND REPORT_ID = @REPORTID
	AND DEN = 1 AND NUM = 0 AND EXCL = 0 AND REXCL = 0


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
