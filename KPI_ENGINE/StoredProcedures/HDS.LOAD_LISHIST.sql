SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
Create PROC HDS.LOAD_LISHIST
AS

----TRUNCATE TABLE [HDS].[HEDIS_LISHIST]
SELECT NULL AS [Record_Type]
      ,NULL AS [MCO_Contract_Number]
      ,NULL AS [PBP_Number]
      ,MEMBER_ID AS [Beneficiary_ID]
      ,NULL AS [Surname]
      ,NULL AS [First_Name]
      ,NULL AS [Middle_Initial]
      ,NULL AS [Sex]
      ,NULL AS [Date_of_Birth]
      ,EFF_DATE AS [Low_Income_Period_Start_Date]
      ,TERM_DATE AS [Low_Income_Period_End_Date]
      ,NULL AS [LIPS_Percentage]
      ,NULL AS [Premium_LIS_Amount]
      ,NULL AS [Low_Income_Co_pay_Level_ID]
      ,NULL AS [Beneficiary_Source_of_Subsidy_Code]
      ,NULL AS [LIS_Activity_Flag]
      ,NULL AS [PBP_Start_Date]
      ,NULL AS [Net_Part_D_Premium_Amount]
      ,NULL AS [Contract_Year]
      ,NULL AS [Institutional_Status_Indicator]
      ,NULL AS [PBP_Enrollment_Termination_Date]
      ,NULL AS [Filler]
      ,NULL AS [RECORD_KEY]
      ,NULL AS [MEASURE_ID]
      ,NULL AS [LOAD_DATE]
  ---FROM [KPI_ENGINE].[HDS].[HEDIS_LISHIST]
FROM DBO.ENROLLMENT E
LEFT JOIN DBO.MEMBER M ON E.MEMBER_KEY=M.MEMBER_KEY
WHERE MEDICAID_PART_B_BUYIN=1
GO
