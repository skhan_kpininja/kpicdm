SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE PROC HDS.LOAD_MEMBER_EN
AS
----TRUNCATE TABLE [HDS].[HEDIS_MEMBER_EN]
SELECT M.MEMBER_ID AS [MemID]  ----M.KPI_PERSON_KEY
      ,EFF_DATE AS [StartDate]
      ,TERM_DATE AS [FinishDate]
      ,CASE WHEN DN_UNITS=1 then 'Y' ELSE 'N' END AS [Dental]
      ,CASE WHEN RX_UNITS=1 then 'Y' ELSE 'N' END  AS [Drug]
      ,CASE WHEN Mental_Health_Benefit_IP=1 then 'Y' ELSE 'N' END  AS [MHInpt]
      ,CASE WHEN Mental_Health_Benefit_Intensive_OP=1 then 'Y' ELSE 'N' END  AS [MHDN]
      ,CASE WHEN Mental_Health_Benefit_OP_ED=1 then 'Y' ELSE 'N' END AS [MHAMB]
      ,CASE WHEN ChemDep_Benefit_IP=1 then 'Y' ELSE 'N' END AS [CDInpt]
      ,CASE WHEN ChemDep_Benefit_Intensive_OP=1 then 'Y' ELSE 'N' END AS [CDDN]
      ,CASE WHEN ChemDep_Benefit_OP_ED=1 then 'Y' ELSE 'N' END AS [CDAMB]
      ,P.PAYER_TYPE AS [Payer]
      ,CASE WHEN Health_Plan_Employee_Flag=1 then 'Y' ELSE 'N' END  AS [PEFlag]
      ,Indicator AS [Ind]
FROM KPI_ENGINE.dbo.ENROLLMENT E
LEFT JOIN KPI_ENGINE.dbo.MEMBER M ON E.MEMBER_KEY=M.MEMBER_KEY
LEFT JOIN KPI_ENGINE.dbo.RFT_PAYER_TYPE P ON E.PAYER_TYPE_KEY=P.PAYER_TYPE_KEY

GO
