SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC HDS.LOAD_MEMBER_TO_HEDIS
AS
BEGIN
INsert into [KPI_ENGINE].[HDS].[HEDIS_MEMBER_GM]
(
       [MemID]
      ,[Gender]
      ,[DOB]
      ,[LName]
      ,[FName]
      ,[MMidName]
      ,[SubID]
      ,[Add1]
      ,[Add2]
      ,[City]
      ,[State]
      ,[MZip]
      ,[MPhone]
      ,[PFirstName]
      ,[PMidName]
      ,[PLastName]
      ,[Race]
      ,[Ethn]
      ,[RaceDS]
      ,[EthnDS]
      ,[SpokenLang]
      ,[SpokenLangSource]
      ,[WrittenLang]
      ,[WrittenLangSource]
      ,[OtherLang]
      ,[OtherSource]
      ,[MEASURE_ID]
      ,[LOAD_DATE]
)
SELECT M.EMPI AS [MemID]
      ,M.MEM_GENDER AS [Gender]
      ,M.MEM_DOB AS [DOB]
      ,M.MEM_LNAME AS [LName]
      ,M.MEM_FNAME AS [FName]
      ,M.MEM_MNAME AS [MMidName]
      ,SUBS.EMPI AS [SubID]
      ,M.MEM_ADDR1 AS [Add1]
      ,M.MEM_ADDR2 AS [Add2]
      ,M.MEM_CITY AS [City]
      ,M.MEM_STATE AS [State]
      ,M.MEM_ZIP AS [MZip]
      ,M.MEM_PHONE AS [MPhone]
      ,M.MEM_FNAME AS [PFirstName]
      ,M.MEM_MNAME AS [PMidName]
      ,M.MEM_LNAME AS [PLastName]
      ,M.MEM_RACE AS [Race]
      ,M.MEM_ETHNICITY AS [Ethn]
      ,M.MEM_RACE AS [RaceDS]
      ,NULL AS [EthnDS]
      ,M.MEM_LANGUAGE AS [SpokenLang]
      ,NULL AS [SpokenLangSource]
      ,M.MEM_LANGUAGE AS [WrittenLang]
      ,NULL AS [WrittenLangSource]
      ,NULL AS [OtherLang]
      ,NULL AS [OtherSource]
      ,'159' AS [MEASURE_ID]
      ,CONVERT(DATE,M.LoadDateTime) AS [LOAD_DATE]
from [KPI_ENGINE].dbo.member M
LEFT JOIN
(
Select M.EMPI,M.MEMBER_ID AS SUBSCRIBER_ID,E.MEMBER_ID
FROM [KPI_ENGINE].dbo.member M
INNER JOIN 
(
Select DISTINCT MEMBER_ID,SUBSCRIBER_ID FROM [KPI_ENGINE].dbo.ENROLLMENT
)E
ON M.MEMBER_ID=E.SUBSCRIBER_ID
) SUBS
ON M.MEMBER_ID=SUBS.MEMBER_ID


END
GO
