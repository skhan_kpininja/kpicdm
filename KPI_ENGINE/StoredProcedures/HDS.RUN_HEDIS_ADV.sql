SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [HDS].[RUN_HEDIS_ADV]
AS


drop table if exists #ADV_memlist; 

CREATE TABLE #ADV_memlist (
    memid varchar(100)    
);

insert into #ADV_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='ADV_TEST' 
and convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) BETWEEN 2 AND 20
---AND gm.memid='100034'
CREATE CLUSTERED INDEX ix_memlist_memid ON #ADV_memlist ([memid]);

--select count(memid) from #ADV_memlist
---select count(distinct memid) from hds.HEDIS_score where measure_id='ADV_TEST'



-- Create Temp Patient Enrollment
drop table if exists #ADV_tmpsubscriber;

CREATE TABLE #ADV_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #ADV_tmpsubscriber ([memid],[StartDate],[EndDate]);

insert into #ADV_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='ADV_TEST' and
convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) BETWEEN 2 AND 20


--select count(distinct memid) from #ADV_tmpsubscriber

Drop table if exists #ADVdataset;

CREATE TABLE #ADVdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT 0,
) ;


Declare @pt_ctr INT;
Declare @i INT = 0;
Declare @patientid varchar(100);
Declare @gender varchar(1);
Declare @age int;
Declare @latest_insenddate varchar(10);
Declare @plan_ct INT=0;
Declare @planid varchar(20);
Declare @meas varchar(10)='ADV';
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);


SELECT @pt_ctr=COUNT(*)  FROM #ADV_memlist;


-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #ADV_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=Year('2020-12-31')-Year(dob) FROM #ADV_tmpsubscriber WHERE memid=@patientid;

		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=convert(date,EndDate) FROM #ADV_tmpsubscriber WHERE  memid=@patientid  AND convert(date,StartDate)<='2020-12-31' ORDER BY convert(date,StartDate) DESC,convert(date,EndDate) Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #ADV_tmpsubscriber WHERE  memid=@patientid AND convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate);
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #ADV_tmpsubscriber WHERE  memid=@patientid AND convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate) ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MDE','MD','MLI','MRB','SN3','MMP'))
			BEGIN
				
				SET @planid='MCD';
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'a'+@planid
			END
			
			Else if(@planid IN('MCR'))
			Begin
				SET @planid='MCR';
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'b'+@planid
			END

			Else if(@planid IN('MP'))
			Begin
				SET @planid='MP';
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'c'+@planid
			END

			Else if(@planid IN('MC'))
			Begin
				SET @planid='MC';
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'd'+@planid
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				SET @planid='MCR';
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'e'+@planid
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'f'+@planid
				
			END
			ELSE
			BEGIN
				if(@planid IN('CEP','HMO','POS','PPO'))
				BEGIN
					Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'g'+@planid
				END
				ELSE
				BEGIN
					Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'h'+@planid
				End
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #ADV_tmpsubscriber where memid=@patientid and convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate) order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #ADV_tmpsubscriber where memid=@patientid and convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate) order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid='MCR';
					Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'i'+@planid
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'j'+@planid
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					Set @planid='MCR';
					Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'k'+@planid
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'm'+@planid
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);	
				Print 'n'+@planid
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'o'+@planid
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				---Set @planid='MCR';  ----Commented
				Set @planid='MCD';
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'p '+@planid+' '+@plan1+' '+@plan2
				
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'q'+@planid
				Set @planid=@plan2;
				Insert INTO #ADVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'r'+@planid
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #ADVdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #ADVdataset ([patient_id],[payer]);


/*
select * from 
#ADVdataset A
left join hds.HEDIS_SCORE S 
on A.patient_id=S.MemID AND A.payer=S.Payer and S.MEASURE_ID='ADV_TEST'
where S.MemID is NULL




select * from 
HDS.HEDIS_SCORE S
left join #ADVdataset A 
on A.patient_id=S.MemID AND A.payer=S.Payer 
where S.MEASURE_ID='ADV_TEST' AND A.patient_id is NULL



select * from hds.HEDIS_MEMBER_EN where MEASURE_ID='ADV_TEST' and MemID='95000'
select * from #ADVdataset where patient_id='95000'
--100034          '
select * from HDS.HEDIS_SCORE where MemID='95000' AND MEASURE_ID='ADV_TEST'
*/


/*
-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #ADV_contenroll;
	
	CREATE table #ADV_contenroll
	(
		Memid varchar(50),
		CE INT
	);

	CREATE CLUSTERED INDEX ix_ADV_contenroll ON #ADV_contenroll ([memid]);

	
	Insert into #ADV_contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
	
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN Convert(date,StartDate)<'2020-01-01' THEN '2020-01-01'
				ELSE Convert(date,StartDate)
				END AS start_date,
				CASE
				WHEN Convert(date,FinishDate)>'2020-12-31' THEN '2020-12-31'
				ELSE Convert(date,FinishDate)
				END AS end_date
				FROM HDS.HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,Convert(date,s.StartDate) as StartDate,Convert(date,s.FinishDate) as FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HDS.HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND Convert(date,s1.StartDate)<='2020-12-31' AND Convert(date,s1.FinishDate)>='2020-01-01'
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HDS.HEDIS_MEMBER_EN s
								WHERE MEASURE_ID='ADV_TEST' AND Convert(date,s.StartDate)<='2020-12-31' AND Convert(date,s.FinishDate)>='2020-01-01'
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID='ADV_TEST'
				-- SQLINES DEMO *** 100001
				AND Convert(date,s.StartDate)<='2020-12-31' AND Convert(date,s.FinishDate)>='2020-01-01'
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, '2020-12-31')-45)

		Union All

		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN Convert(date,StartDate)<'2020-01-01' THEN '2020-01-01'
				ELSE Convert(date,StartDate)
			END AS start_date,
			CASE
				WHEN Convert(date,FinishDate)>'2020-12-31' THEN '2020-12-31'
				ELSE Convert(date,FinishDate)
			END AS end_date
			FROM HDS.HEDIS_MEMBER_EN 
			WHERE MEASURE_ID='ADV_TEST'
			-- SQLINES DEMO *** 0000          
			AND '2020-12-31' BETWEEN Convert(date,StartDate) AND Convert(date,FinishDate)
		
		)t4  GROUP BY MemID
 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=2 order by MemID;


*/

DROP TABLE IF EXISTS #ADV_contenroll;
	
	CREATE table #ADV_contenroll
	(
		Memid varchar(50)
		
	);


With coverage_CTE (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
select Memid,isnull(lag(convert(date,Finishdate),1) over(partition by Memid order by convert(date,Startdate),convert(date,Finishdate) desc),'2020-12-01') as lastcoveragedate,Case when convert(date,Startdate)<'2020-01-01' then '2020-01-01' else convert(date,Startdate) end as Startdate,case when convert(date,Finishdate)>'2020-12-31' then '2020-12-31' else convert(date,Finishdate) end as Finishdate,isnull(lead(convert(date,Startdate),1) over(partition by Memid order by convert(date,Startdate),convert(date,Finishdate)),'2020-12-31') as nextcoveragedate  from HDS.hedis_member_en where measure_id='ADV_TEST' and convert(date,Startdate)<='2020-12-31' and convert(date,Finishdate)>='2020-01-01' and DENTAL='Y'   
)
Insert into #ADV_contenroll
select Memid from(
select *, case when rn=1 and convert(date,Startdate)>'2020-01-01' then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,convert(date,Startdate),newfinishdate)+1 as coveragedays,case when '2020-12-31' between convert(date,Startdate) and newfinishdate then 1 else 0 end as anchor from(
Select *,case when datediff(day,convert(date,Finishdate),nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,convert(date,Finishdate)),'2020-12-31') else convert(date,Finishdate) end as newfinishdate,ROW_NUMBER() over(partition by memid order by convert(date,Startdate),convert(date,Finishdate)) as rn from coverage_CTE
)t1           
)t2 
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, '2020-12-31')-45) and sum(anchor)>0

CREATE CLUSTERED INDEX ix_cdc_contenroll ON #ADV_contenroll ([memid]);


update #ADVdataset set CE=1 from #ADVdataset ds join #ADV_contenroll ce on ds.patient_id=ce.MemID;


/*
update #ADVdataset set CE=0;


select * from 
#ADVdataset A
left join hds.HEDIS_SCORE S 
on A.patient_id=S.MemID 
WHERE S.MEASURE_ID='ADV_TEST' AND  A.CE<>S.CE


select * from 
hds.HEDIS_SCORE S 
left join #ADVdataset A
on A.patient_id=S.MemID 
WHERE S.MEASURE_ID='ADV_TEST' AND  A.CE<>S.CE
*/




-- Hospice Exclusion
	drop table if exists #hospicemembers;
	CREATE table #hospicemembers
	(
		Memid varchar(50)
		
	);

	Insert into #hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HDS.HEDIS_OBS where Measure_id='ADV_TEST' and date !='' and convert(date,[date]) between '2020-01-01' and '2020-12-31' and OCode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	select MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='ADV_TEST' and HCFAPOS!=81 and Date_S!='' and Convert(date,Date_S) between '2020-01-01' and '2020-12-31'
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #hospicemembers ([memid]);
	
	
	update #ADVdataset set rexcl=1 from #ADVdataset ds join #hospicemembers hos on hos.memid=ds.patient_id;


/*
select * from 
#ADVdataset A
left join hds.HEDIS_SCORE S 
on A.patient_id=S.MemID 
WHERE S.MEASURE_ID='ADV_TEST' AND  A.EXCL<>S.Excl


select * from 
hds.HEDIS_SCORE S 
left join #ADVdataset A
on A.patient_id=S.MemID 
WHERE S.MEASURE_ID='ADV_TEST' AND  A.EXCL<>S.Excl
*/


drop table if exists #num;
	CREATE table #num
	(
		Memid varchar(50)
	);

	Insert into #num(Memid)
	select distinct Memid from(
	----select distinct memid from HDS.HEDIS_PROC where MEASURE_ID='WCV_SAMPLE' and pstatus='EVN' and convert(date,sdate) between '2020-01-01' and '2020-12-31' and sdate!='' and pcode in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Well-Care'))
	---Union all
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='ADV_TEST' and Date_S!='' and convert(date,Date_S) between '2020-01-01' AND '2020-12-31' AND HCFAPOS<>'81'
	and ProvID in (select distinct ProvID from hds.HEDIS_PROVIDER where measure_id='ADV_TEST' and (Dentist='Y')) ----AND SuppData='N' 
)t1

CREATE CLUSTERED INDEX idx_NUM ON #NUM ([memid]);

update #ADVdataset set NUM=1 from #ADVdataset ds join #num NUM on NUM.memid=ds.patient_id;




Declare @runid INT;
Declare @meas_year nvarchar(4)='2020'
select @runid=max(RUN_ID) from HDS.HEDIS_MEASURE_OUTPUT where measure_id='ADV';

if(@runid>=1)
Begin
SET @runid=@runid+1;
End
Else
Begin
		SET @runid=1;
END
	

Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
SELECT patient_id AS memid,meas,payer,CE,0 as EVENT,CASE WHEN CE='1' AND rexcl='0' and rexcld='0' AND PAYER in ('MCD','MEP','MMO','MOS','MPO') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'ADV' as Measure_ID,@meas_year,@runid FROM #ADVdataset

GO
