SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [HDS].[RUN_HEDIS_ART_PROD] @meas_year nvarchar(4)
AS


-- Declare Variables
Declare @meas varchar(10)='159'--'ART';
Declare @ce_startdt varchar(10);


Declare @ce_startdt1 varchar(10);
Declare @ce_novdt varchar(10);
Declare @ce_enddt varchar(10);
Declare @ce_enddt1 varchar(10);
DECLARE @pt_ctr Int=0;
DECLARE @i INT=0;
declare @patientid varchar(50);
declare @plan_ct INT;
declare @gender varchar(10);
declare @age INT;
Declare @latest_insenddate varchar(10);
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);
Declare @planid varchar(20);
Declare @runid INT;




-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');


SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


-- Create artdataset
drop table if exists #artdataset;

CREATE TABLE #artdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
  
  
) ;



-- Eligible Patient List


drop table if exists #art_memlist; 

CREATE TABLE #art_memlist (
    memid varchar(100)
    
);


insert into #art_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) >=18 ORDER BY 1;


CREATE CLUSTERED INDEX ix_memlist_memid ON #art_memlist ([memid]);


-- Create Temp Patient Enrollment

drop table if exists #art_tmpsubscriber;

CREATE TABLE #art_tmpsubscriber (
    memid varchar(100),
    dob varchar(10),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(10),
	EndDate varchar(10),
);
insert into #art_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) >=18 ORDER BY en.MemID,en.StartDate,en.FinishDate;

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #art_tmpsubscriber ([memid],[StartDate],[EndDate]);

-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #art_memlist;


-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #art_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=Year(@ce_enddt)-Year(dob) FROM #art_tmpsubscriber WHERE memid=@patientid;

		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #art_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #art_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #art_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				
				SET @planid='MCR';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				SET @planid='MCR';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
				if(@planid IN('CEP','HMO','POS','PPO'))
				BEGIN
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				ELSE
				BEGIN
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				End
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #art_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #art_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid='MCR';
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					Set @planid='MCR';
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);	
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				Set @planid='MCR';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Set @planid=@plan2;
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);				
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #artdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #artdataset ([patient_id],[payer]);

-- Continuous Enrollment
DROP TABLE IF EXISTS #art_contenroll;
	
	CREATE table #art_contenroll
	(
		Memid varchar(100)
		
	);


With coverage_CTE (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
select Memid,isnull(lag(Finishdate,1) over(partition by Memid order by Startdate,FinishDate desc),@ce_startdt) as lastcoveragedate,Case when StartDate<@ce_startdt then @ce_startdt else startdate end as Startdate,case when Finishdate>@ce_enddt then @ce_enddt else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),@ce_enddt) as nextcoveragedate  from hds.hedis_member_en where measure_id=@meas and Startdate<=@ce_enddt and Finishdate>=@ce_startdt and drug='Y'   
)
Insert into #art_contenroll
select Memid from(
select *, case when rn=1 and startdate>@ce_startdt then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,newfinishdate)+1 as coveragedays,case when @ce_enddt between Startdate and newfinishdate then 1 else 0 end as anchor from(
Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),@ce_enddt) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE
)t1           
)t2 
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, @ce_enddt)-45) and sum(anchor)>0

CREATE CLUSTERED INDEX ix_cdc_contenroll ON #art_contenroll ([memid]);

update #artdataset set CE=1 from #artdataset ds join #art_contenroll ce on ds.patient_id=ce.MemID;
	


-- Exclusions
	-- AdvancedIllness
	drop table if exists #art_advillness;
	
	CREATE table #art_advillness
		(
			Memid varchar(100),
			
			servicedate varchar(10),
			claimid varchar(100)
		);

	

	Insert into #art_advillness
	select MemID,Date_S,claimid from hds.HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!='81' and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_7 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_8 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_9 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_10 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Proc_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	
	)
	
	CREATE CLUSTERED INDEX ix_cdc_advillness ON #art_advillness ([memid],[servicedate],[claimid]);
	

	
	-- Members with Institutinal SNP

	UPDATE #artdataset SET #artdataset.rexcl=1 FROM #artdataset ds JOIN hds.HEDIS_MEMBER_EN s on ds.patient_id=s.MemID and s.MEASURE_ID=@meas WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt AND s.Payer='SN2';


	-- LTI Exclusion
	drop table if exists #art_LTImembers;
	
	CREATE table #art_LTImembers
	(
		Memid varchar(100)
				
	);

	

	Insert into #art_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM hds.HEDIS_MMDF WHERE Measure_ID=@meas AND Run_Date BETWEEN @ce_startdt AND @ce_enddt AND LTI_Flag='Y';

	CREATE CLUSTERED INDEX idx_art_ltimembers ON #art_LTImembers ([memid]);

	update #artdataset set rexcl=1 from #artdataset ds join #art_LTImembers re on ds.patient_id=re.Memid where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3');

	

	-- Hospice Exclusion
	drop table if exists #art_hospicemembers;
	CREATE table #art_hospicemembers
	(
		Memid varchar(100)
		
	);

	

	Insert into #art_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from hds.HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from hds.HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from hds.Hedis_MMDF where measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from hds.HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!='81' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
	or
	Rev in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #art_hospicemembers ([memid]);
	update #artdataset set rexcl=1 from #artdataset ds join #art_hospicemembers hos on hos.memid=ds.patient_id;
			
				
	-- Frailty Members LIST
	drop table if exists #art_frailtymembers;
	
	CREATE table #art_frailtymembers
	(
		
		Memid varchar(100)
			
	);

	

	Insert into #art_frailtymembers
	select MemID from hds.HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_7 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_8 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_9 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_10 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	
	or
	Rev in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);

	CREATE CLUSTERED INDEX idx_cdc_frailtymembers ON #art_frailtymembers ([memid]);
	
	
	-- Outpatient and other visits
	drop table if exists #art_visitlist;
	CREATE table #art_visitlist
	(
		Memid varchar(100),	
		date_s varchar(10),
		claimid varchar(100)
	);

	Insert into #art_visitlist
	select distinct MemID,Date_S,claimid from hds.HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_7 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_8 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_9 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_10 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	or 
	Rev in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)
	
	CREATE CLUSTERED INDEX ix_cdc_visitlist ON #art_visitlist ([memid],[date_s]);
	



	-- Required exclusion table
	drop table if exists #art_reqdexcl1;
	CREATE table #art_reqdexcl1
	(
		Memid varchar(100)
			
	);

	Insert into #art_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #art_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #art_noncauteinpatientstaylist na
	join #art_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #art_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #art_frailtymembers f on f.Memid=t3.Memid


	CREATE CLUSTERED INDEX idx_cdc_reqdexcl1 ON #art_reqdexcl1 ([memid]);
	
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl1 re1 on re1.Memid=ds.patient_id and ds.age Between 66 AND 80;

-- Required Exclusion 2
	-- Acute Inpatient with Advanced Illness

	drop table if exists #art_reqdexcl2;
	CREATE table #art_reqdexcl2
	(
		Memid varchar(100)
				
	);

	insert into #art_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID from (
	select distinct MemID,Date_S,claimid from hds.HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	
	)
	)t1
	Join #art_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #art_frailtymembers f on f.Memid=t2.MemID

	CREATE CLUSTERED INDEX idx_cdc_reqdexcl2 ON #art_reqdexcl2 ([memid]);
	
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl2 re2 on re2.Memid=ds.patient_id and ds.age Between 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #art_reqdexcl3;
	CREATE table #art_reqdexcl3
	(
		Memid varchar(100)
			
	);

	

	insert into #art_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #art_inpatientstaylist inp
	left outer join #art_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #art_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #art_frailtymembers f on f.Memid=t2.Memid

	CREATE CLUSTERED INDEX idx_cdc_reqdexcl3 ON #art_reqdexcl3 ([memid]);
	
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl3 re3 on re3.Memid=ds.patient_id and ds.age Between 66 AND 80;


-- RequiredExcl 4

	drop table if exists #art_reqdexcl4;
	CREATE table #art_reqdexcl4
	(
		Memid varchar(100)
				
	);

	

	insert into #art_reqdexcl4
	select t1.Memid from(
	select Memid from hds.HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from hds.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #art_frailtymembers f on f.MemId=t1.Memid;

	CREATE CLUSTERED INDEX idx_cdc_reqdexcl4 ON #art_reqdexcl4 ([memid]);
	
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl4 re4 on re4.Memid=ds.patient_id and ds.age Between 66 AND 80;
	
	-- reqdexcl 5 fraity with 81 years and above
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_frailtymembers f on f.Memid=ds.patient_id and ds.age>=81;

-- Event LIST
	drop table if exists #art_vstlist;
	CREATE table #art_vstlist
	(
			Memid varchar(100),
			servicedate varchar(10),
			claimid varchar(100)

			
	);


insert into #art_vstlist
Select distinct memid,servicedate,claimid from(
select distinct MemID,Date_S as servicedate,claimid from hds.HEDIS_VISIT v
where MEASURE_ID=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and Date_S between @ce_startdt and @ce_novdt
and (
CPT in(select code_new from hds.valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))
or
HCPCS in(select code_new from hds.valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))
or
REV in(select code_new from hds.valueset_to_code where code_system='UBREV' and value_set_name in('Outpatient','Telephone Visits','Online Assessments'))
)

union all
/*
select memid,sdate as servicedate from hedis_proc where measure_id=@meas and sdate between  @ce_startdt and @ce_novdt and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))

Union all

Select Memid,date as servicedate from Hedis_Obs where measure_id=@meas and date between  @ce_startdt and @ce_novdt and date!='' and ocode in(select code from valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))

Union all 

Select Memid,sdate as servicedate from hedis_visit_e where measure_id=@meas and sdate between  @ce_startdt and @ce_novdt and sdate!='' and activity in(select code from valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))
Union all

Select Memid,sdate as servicedate from hedis_diag where measure_id=@meas and sdate between  @ce_startdt and @ce_novdt and sdate!=''  and dcode in(select code from valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))

Union all
*/
select Memid,date_s as servicedate,claimid from hds.HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!='81' and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt and @ce_novdt and rev in(select code_new from hds.valueset_to_code where code_system='UBREV'and value_set_name='Inpatient Stay') and (rev in(select code_new from hds.valueset_to_code where code_system='UBREV'and value_set_name='Nonacute Inpatient Stay') or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code_new from hds.valueset_to_code where code_system='UBTOB'and value_set_name='Nonacute Inpatient Stay'))
	

)t1

CREATE CLUSTERED INDEX idx_art_vstlist ON #art_vstlist ([memid],[servicedate],[claimid]);



drop table if exists #art_rheumatoidlist;
CREATE table #art_rheumatoidlist
(
	Memid varchar(100),
	servicedate varchar(10),
	claimid varchar(100)
);

Insert into #art_rheumatoidlist
select distinct Memid,Servicedate,claimid from(

select distinct MemID,Date_S as servicedate,claimid from hds.HEDIS_VISIT v
where MEASURE_ID=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and Date_S between @ce_startdt and @ce_novdt
and (
Diag_i_1 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_2 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_3 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_4 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_5 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_6 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_7 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_8 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_9 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')
or
Diag_i_10 in(select code_new from hds.valueset_to_code where value_set_name='Rheumatoid Arthritis')

)
/*
Union all


Select Memid,sdate as servicedate from hedis_diag where measure_id=@meas and sdate between  @ce_startdt and @ce_novdt and sdate!=''  and dcode in(select code from valueset_to_code where value_set_name='Rheumatoid Arthritis')
*/

)t1


CREATE CLUSTERED INDEX idx_art_rheumatoidlist ON #art_rheumatoidlist ([memid],servicedate,claimid);



drop table if exists #art_eventlist;
CREATE table #art_eventlist
(
	Memid varchar(100)
	
);

insert into #art_eventlist
select memid from(
select distinct r.memid,r.servicedate from #art_rheumatoidlist r
join #art_vstlist v on v.memid=r.memid and v.claimid=r.claimid

)t1 group by memid having count(memid)>1
	

CREATE CLUSTERED INDEX idx_art_eventlist ON #art_eventlist ([memid]);	


update #artdataset set Event=1 from #artdataset ds
	join #art_eventlist e on e.Memid=ds.patient_id;
	
	
-- Numerator
drop table if exists #art_numlist;
CREATE table #art_numlist
(
	Memid varchar(100)	
);


insert into #art_numlist
Select distinct memid from(
select distinct MemID from hds.HEDIS_VISIT v
where MEASURE_ID=@meas and HCFAPOS!='81' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
and 
HCPCS in(select code_new from hds.valueset_to_code where value_set_name in('DMARD'))

Union all
select distinct Memid from hds.hedis_pharm where measure_id=@meas and Prservdate between @ce_startdt and @ce_enddt and Prservdate!='' and NDC in(select code from hds.Medication_list_to_codes where Medication_list_name='DMARD Medications')

Union all

select distinct Memid from hds.hedis_pharm_c where measure_id=@meas and Startdate between @ce_startdt and @ce_enddt and Startdate!='' and Rxnorm in(select code from hds.Medication_list_to_codes where Medication_list_name='DMARD Medications')

)t1

CREATE CLUSTERED INDEX idx_art_vstlist ON #art_numlist ([memid]);


update #artdataset set num=1 from #artdataset ds
	join #art_numlist n on n.Memid=ds.patient_id;
	
-- Optional Exclusions
	
-- HIV Exclusions
drop table if exists #art_optexcl1;
CREATE table #art_optexcl1
(
	Memid varchar(100)	
);

Insert into #art_optexcl1
select distinct Memid from(
select Memid from hds.HEDIS_VISIT v
where MEASURE_ID=@meas and HCFAPOS!='81'  and Date_S!='' and Date_S <=@ce_enddt
and (
Diag_i_1 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_2 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_3 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_4 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_5 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_6 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_7 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_8 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_9 in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
or
Diag_i_10 in(select Code_New from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))

)

Union all

select Memid from hds.Hedis_diag where measure_id=@meas and sdate<=@ce_enddt and sdate!='' and dcode in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))

Union all

select Memid from hds.Hedis_proc where measure_id=@meas and sdate<=@ce_enddt and sdate!='' and pstatus='EVN' and pcode in(select code_new from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2'))
)t1

CREATE CLUSTERED INDEX idx_art_optexcl1 ON #art_optexcl1 ([memid]);
	
update #artdataset set excl=1 from #artdataset ds
	join #art_optexcl1 oe1 on oe1.Memid=ds.patient_id;
	
-- Pregnancy
drop table if exists #art_optexcl2;
CREATE table #art_optexcl2
(
	Memid varchar(100)	
);

Insert into #art_optexcl2
select distinct Memid from(
select Memid from hds.HEDIS_VISIT v
where MEASURE_ID=@meas and HCFAPOS!='81'  and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
and (
Diag_i_1 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_2 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_3 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_4 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_5 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_6 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_7 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_8 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_9 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
or
Diag_i_10 in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))

)

Union all

select Memid from hds.Hedis_diag where measure_id=@meas and sdate between @ce_startdt and @ce_enddt and sdate!='' and dcode in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))

Union all

select Memid from hds.Hedis_proc where measure_id=@meas and sdate between @ce_startdt and @ce_enddt and sdate!='' and pstatus='EVN' and pcode in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))

Union all 

Select Memid from hds.Hedis_Obs where measure_id=@meas and date between @ce_startdt and @ce_enddt and date!='' and Ocode in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
Union all

Select Memid from hds.hedis_visit_e where measure_id=@meas and sdate between @ce_startdt and @ce_enddt and sdate!='' and activity in(select code_new from hds.valueset_to_code where value_set_name in('Pregnancy'))
)t1

CREATE CLUSTERED INDEX idx_art_optexcl2 ON #art_optexcl2 ([memid]);	

update #artdataset set excl=1 from #artdataset ds
	join #art_optexcl2 oe2 on oe2.Memid=ds.patient_id and ds.patient_gender='F';


	-- Insert data in Output Table
	select @runid=max(RUN_ID) from hds.HEDIS_MEASURE_OUTPUT where measure_id='ART' and ROOT_COMPANIES_ID='159';

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT patient_id AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 and payer in('MCR','MC','MCS','MP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'ART' as Measure_ID,@meas_year,@runid,'159' AS ROOT_COMPANIES_ID FROM #artdataset





GO
