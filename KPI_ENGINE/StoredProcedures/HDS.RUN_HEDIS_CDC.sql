SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [HDS].[RUN_HEDIS_CDC] @meas_year nvarchar(4)
AS


-- Declare Variables

Declare @meas varchar(10)='CDC';
Declare @ce_startdt varchar(8);
Declare @ce_rsltstartdt varchar(8);
Declare @ce_rsltenddt varchar(8);
Declare @ce_startdt1 varchar(8);
Declare @ce_middt varchar(8);
Declare @ce_enddt varchar(8);
Declare @ce_enddt1 varchar(8);
DECLARE @pt_ctr Int=0;
DECLARE @i INT=0;
declare @patientid varchar(50);
declare @plan_ct INT;
declare @gender varchar(10);
declare @age INT;
Declare @latest_insenddate varchar(8);
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);
Declare @planid varchar(20);
Declare @runid INT;




-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_rsltstartdt=concat(@meas_year-1,'1225');
SET @ce_rsltenddt=concat(@meas_year+1,'0107');
SET @ce_middt=concat(@meas_year,'0630');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');
SET @ce_enddt1=concat(@meas_year-1,'1231');


-- Create cdcdataset
drop table if exists #cdcdataset;

CREATE TABLE #cdcdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
  
  
) ;



-- Eligible Patient List

drop table if exists #cdc_memlist; 

CREATE TABLE #cdc_memlist (
    memid varchar(100)
    
);


insert into #cdc_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 18 AND 75 ORDER BY 1;

CREATE CLUSTERED INDEX ix_memlist_memid ON #cdc_memlist ([memid]);



-- Create Temp Patient Enrollment
drop table if exists #cdc_tmpsubscriber;

CREATE TABLE #cdc_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #cdc_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 18 AND 75 ORDER BY en.MemID,en.StartDate,en.FinishDate;

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #cdc_tmpsubscriber ([memid],[StartDate],[EndDate]);

-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #cdc_memlist;


-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #cdc_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR(@ce_enddt)-YEAR(DOB) FROM #cdc_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #cdc_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #cdc_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #cdc_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				/*
				IF(@planid in('MMP','SN3'))
				Begin
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				*/
				SET @planid='MCD';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				--Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				--Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				/*
				SET @planid='MCD';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				--Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
				*/
				SET @planid='MCR';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
				if(@planid IN('CEP','HMO','POS','PPO'))
				BEGIN
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				ELSE
				BEGIN
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				End
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #cdc_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #cdc_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					/*
					SET @planid='MCD';
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					--Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
					*/
					SET @planid='MCR';
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
					
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					/*
					Set @planid='MCD';
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					--Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
					*/
					Set @planid='MCR';
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
					
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				-- Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
					
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				-- Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				/*
				if(@plan1 IN('SN3','MMP'))
				Begin
					Set @planid=@plan1;
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				End
				
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				*/
					
				Set @planid='MCR';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
				--Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				
			END
			/*
			ELSE IF(@plan2 IN('SN3','MMP','MDE'))
			BEGIN
			
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
					
				Set @planid='MCR';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			*/
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				If(@plan1 in('MEP','MMO','MOS','MPO'))
				Begin
					
					
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				Else If(@plan1 in('CEP','HMO','POS','PPO'))
				Begin
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				ELSE
				BEGIN
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				
				Set @planid=@plan2;
				If(@plan2 in('MEP','MMO','MOS','MPO'))
				Begin
					
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				Else If(@plan1 in('CEP','HMO','POS','PPO'))
				Begin
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				ELSE
				BEGIN
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC1',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC2',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC4',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC7',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC9',@planid,@age,@gender);
					Insert INTO #cdcdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'CDC10',@planid,@age,@gender);
				END
				
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #cdcdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #cdcdataset ([patient_id],[payer]);

-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #cdc_contenroll;
	
	CREATE table #cdc_contenroll
	(
		Memid varchar(50),
		CE INT
	);

	

	
	Insert into #cdc_contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
	
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
				END AS start_date,
				CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt AND s1.FinishDate>=@ce_startdt
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID=@meas AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID=@meas
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt)-45)

		Union All

		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
			END AS start_date,
			CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
			END AS end_date
			FROM HEDIS_MEMBER_EN 
			WHERE MEASURE_ID=@meas
			-- SQLINES DEMO *** 0000          
			AND @ce_enddt BETWEEN StartDate AND FinishDate
		
		)t4  GROUP BY MemID
 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=2 order by MemID;

	CREATE CLUSTERED INDEX ix_cdc_contenroll ON #cdc_contenroll ([memid]);
	
	update #cdcdataset set CE=1 from #cdcdataset ds join #cdc_contenroll ce on ds.patient_id=ce.MemID;
	

	-- AdvancedIllness

	drop table if exists #cdc_advillness;
	
	CREATE table #cdc_advillness
		(
			Memid varchar(50),
			
			servicedate varchar(8),
			claimid int
		);

	

	Insert into #cdc_advillness
	select MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	
	)
	
	CREATE CLUSTERED INDEX ix_cdc_advillness ON #cdc_advillness ([memid],[servicedate],[claimid]);
	-- Required Exclusion

	drop table if exists #cdc_reqdexcl
	CREATE table #cdc_reqdexcl
	(
		Memid varchar(50)
			
	);

	


	insert into #cdc_reqdexcl
	select distinct memid from(

	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))

	Union all

	select Memid from HEDIS_OBS where Measure_id=@meas and date between @ce_startdt and @ce_enddt and date !='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
		
	union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S between @ce_startdt and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	
	)
	)t1 
	
	CREATE CLUSTERED INDEX idx_cdc_reqdexcl ON #cdc_reqdexcl ([memid]);
	
	update #cdcdataset set rexcld=1 from #cdcdataset ds join #cdc_reqdexcl re on ds.patient_id=re.memid;
	
	-- Members with Institutinal SNP

	UPDATE #cdcdataset SET #cdcdataset.rexcl=1 FROM #cdcdataset ds JOIN HEDIS_MEMBER_EN s on ds.patient_id=s.MemID and s.MEASURE_ID=@meas WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt AND s.Payer='SN2';


	-- LTI Exclusion
	
	drop table if exists #cdc_LTImembers;
	
	CREATE table #cdc_LTImembers
	(
		Memid varchar(50)
				
	);

	

	Insert into #cdc_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM HEDIS_MMDF WHERE Measure_ID=@meas AND Run_Date BETWEEN @ce_startdt AND @ce_enddt AND LTI_Flag='Y';

	CREATE CLUSTERED INDEX idx_cdc_ltimembers ON #cdc_LTImembers ([memid]);

	update #cdcdataset set rexcl=1 from #cdcdataset ds join #cdc_LTImembers re on ds.patient_id=re.Memid where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3');

	

	-- Hospice Exclusion

	drop table if exists #cdc_hospicemembers;
	CREATE table #cdc_hospicemembers
	(
		Memid varchar(50)
		
	);

	

	Insert into #cdc_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from Hedis_MMDF where measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #cdc_hospicemembers ([memid]);
	update #cdcdataset set rexcl=1 from #cdcdataset ds join #cdc_hospicemembers hos on hos.memid=ds.patient_id;
			
				
	-- Frailty Members LIST
	drop table if exists #cdc_frailtymembers;
	
	CREATE table #cdc_frailtymembers
	(
		
		Memid varchar(50)
			
	);

	

	Insert into #cdc_frailtymembers
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);

	CREATE CLUSTERED INDEX idx_cdc_frailtymembers ON #cdc_frailtymembers ([memid]);
	
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #cdc_inpatientstaylist;
	CREATE table #cdc_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #cdc_inpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
	CREATE CLUSTERED INDEX ix_cdc_inpatientstaylist ON #cdc_inpatientstaylist ([memid],[date_s]);
	
	-- Non acute Inpatient stay list
	
	drop table if exists #cdc_noncauteinpatientstaylist;
	CREATE table #cdc_noncauteinpatientstaylist
		(
			Memid varchar(50),
			
			date_s varchar(8),
			claimid int
		);

	

	Insert into #cdc_noncauteinpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and (v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')) or RIGHT('0000'+CAST(Trim(v.BillType) AS VARCHAR(4)),4) in (select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')))
	
	CREATE CLUSTERED INDEX ix_cdc_nonacuteinpatientstaylist ON #cdc_noncauteinpatientstaylist ([memid],[date_s]);
	
	-- Outpatient and other visits
	drop table if exists #cdc_visitlist;
	CREATE table #cdc_visitlist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #cdc_visitlist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	or 
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)
	
	CREATE CLUSTERED INDEX ix_cdc_visitlist ON #cdc_visitlist ([memid],[date_s]);
	
	-- Required exclusion table
	drop table if exists #cdc_reqdexcl1;
	CREATE table #cdc_reqdexcl1
	(
		Memid varchar(50)
			
	);

	

	Insert into #cdc_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #cdc_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #cdc_noncauteinpatientstaylist na
	join #cdc_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #cdc_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #cdc_frailtymembers f on f.Memid=t3.Memid


	CREATE CLUSTERED INDEX idx_cdc_reqdexcl1 ON #cdc_reqdexcl1 ([memid]);
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl1 re1 on re1.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #cdc_reqdexcl2;
	CREATE table #cdc_reqdexcl2
	(
		Memid varchar(50)
				
	);

	

	insert into #cdc_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID from (
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	
	)
	)t1
	Join #cdc_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #cdc_frailtymembers f on f.Memid=t2.MemID

	CREATE CLUSTERED INDEX idx_cdc_reqdexcl2 ON #cdc_reqdexcl2 ([memid]);
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl2 re2 on re2.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #cdc_reqdexcl3;
	CREATE table #cdc_reqdexcl3
	(
		Memid varchar(50)
			
	);

	

	insert into #cdc_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #cdc_inpatientstaylist inp
	left outer join #cdc_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #cdc_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #cdc_frailtymembers f on f.Memid=t2.Memid

	CREATE CLUSTERED INDEX idx_cdc_reqdexcl3 ON #cdc_reqdexcl3 ([memid]);
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl3 re3 on re3.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #cdc_reqdexcl4;
	CREATE table #cdc_reqdexcl4
	(
		Memid varchar(50)
				
	);

	

	insert into #cdc_reqdexcl4
	select t1.Memid from(
	select Memid from HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #cdc_frailtymembers f on f.MemId=t1.Memid;

	CREATE CLUSTERED INDEX idx_cdc_reqdexcl4 ON #cdc_reqdexcl4 ([memid]);
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl4 re4 on re4.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	
	
-- Patients with Indueced Diabetes
	drop table if exists #cdc_induceddiabetesmemlist;
	CREATE table #cdc_induceddiabetesmemlist
	(
		Memid varchar(50)
				
	);

	

	Insert into #cdc_induceddiabetesmemlist
	select Memid from(
	select MemId from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions')) 
	Union all
	select MemID from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt1 and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions')))
	union all
	select MemId from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt1 and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	Union all
	select MemID from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	Union all
	Select Memid from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))

	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetes Exclusions'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_cdc_induceddiabetesmemlist ON #cdc_induceddiabetesmemlist ([memid]);
	
	-- Memebers with no diabetes
	drop table if exists #cdc_nondiabeticmemlist;
	CREATE table #cdc_nondiabeticmemlist
	(
		Memid varchar(50)
				
	);

	

	Insert into #cdc_nondiabeticmemlist
	select Memid from #cdc_memlist where memid not in(
	select MemId from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')) 
	Union all
	select MemID from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt1 and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')))
	union all
	select MemId from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt1 and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	Union all
	select MemID from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	Union all
	Select Memid from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))

	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetes'))
	)
)
	
	CREATE CLUSTERED INDEX idxcdc_nondiabeticmemlist ON #cdc_nondiabeticmemlist ([memid]);
	
-- Optional Exclusion
	drop table if exists #cdc_optexcl;
	CREATE table #cdc_optexcl
	(
		Memid varchar(50)
				
	);

	

	Insert into #cdc_optexcl
	select t1.memid from #cdc_nondiabeticmemlist t1
	join #cdc_induceddiabetesmemlist t2 on t1.memid=t2.memid
	
	
	CREATE CLUSTERED INDEX idx_cdc_optexcl ON #cdc_optexcl ([memid]);
	
	update #cdcdataset set excl=1 from #cdcdataset ds
	join #cdc_optexcl oe on oe.Memid=ds.patient_id;
	
	
	-- Event

	-- Create list of patients with Diabetes
	
	drop table if exists #cdc_diabeticmemlist;
	CREATE table #cdc_diabeticmemlist
	(
		Memid varchar(50),
		servicedate varchar(8),
		claimid int
				
	);

	

	Insert into #cdc_diabeticmemlist
	/*
	select MemId,sdate as servicedate,0 as claimid from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')) 
	Union all
	select MemID,date_s as servicedate,0 as claimid from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt1 and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')))
	union all
	select MemId,date as servicedate,0 as claimid from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt1 and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	Union all
	select MemID,sdate as servicedate, 0 as claimid from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	Union all
	Select Memid,sdate as servicedate,0 as claimid from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))

	Union all
	*/
	select MemID,date_s as servicedate,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt and Date_S!='' and hcfapos!=81 and suppdata='N'
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetes'))
	)


	CREATE CLUSTERED INDEX idx_cdc_diabeticmemlist ON #cdc_diabeticmemlist ([memid],[servicedate],[claimid]);
	
	
	-- Acute Inpatient list
	drop table if exists #cdc_acuteipmemlist;
	CREATE table #cdc_acuteipmemlist
	(
		Memid varchar(50),
		servicedate varchar(8),
		claimid int
				
	);

	

	Insert into #cdc_acuteipmemlist
	/*
	select MemId,sdate as servicedate,0 as claimid from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient')) 
	Union all
	select MemID,date_s as servicedate,0 as claimid from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt1 and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient')))
	union all
	select MemId,date as servicedate,0 as claimid from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt1 and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	Union all
	select MemID,sdate as servicedate, 0 as claimid from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	Union all
	Select Memid,sdate as servicedate,0 as claimid from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))

	Union all
	*/
	select MemID,date_s as servicedate,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt and Date_S!='' and hcfapos!=81 and suppdata='N' and hcfapos not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth POS')) and (CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth Modifier')) and CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth Modifier')) and CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth Modifier')))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Acute Inpatient'))
	)

	
	
	CREATE CLUSTERED INDEX idx_cdc_acuteipmemlist ON #cdc_acuteipmemlist ([memid],[servicedate],[claimid]);
	
	--	Acute Inpatient visits with diabetes diagnosis and without telehealth POS and modifiers
	drop table if exists #cdc_eventlist1;				
	CREATE table #cdc_eventlist1
	(
		Memid varchar(50),
		
	);
	
	
	
	Insert into #cdc_eventlist1
	select d.memid from #cdc_diabeticmemlist d
	join #cdc_acuteipmemlist acip on d.memid=acip.memid and d.claimid=acip.claimid
	
	CREATE CLUSTERED INDEX ix_cdc_cdc_eventlist1 ON #cdc_eventlist1 ([memid]);
	
	update #cdcdataset set event=1 from #cdcdataset ds join #cdc_eventlist1 el1 on ds.patient_id=el1.memid;
	
	
	-- Event List 2
	-- Acute inpatient discharge with diabetes
	
	drop table if exists #cdc_eventlist2;				
	CREATE table #cdc_eventlist2
	(
		Memid varchar(50),
		
	);
	
	
	
	Insert into #cdc_eventlist2
	select t1.Memid from(
	select inp.Memid,inp.date_s,inp.claimid from #cdc_inpatientstaylist inp
	left outer join #cdc_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #cdc_diabeticmemlist d on d.memid=t1.memid and d.claimid=t1.claimid
	
	CREATE CLUSTERED INDEX ix_cdc_eventlist2 ON #cdc_eventlist2 ([memid]);
	
	update #cdcdataset set event=1 from #cdcdataset ds join #cdc_eventlist2 el2 on ds.patient_id=el2.memid;
	
	
	-- EventList 3
	-- Outpatient,ED,Observation,Telephone,Online,Non acute encounters and discharges
	
	drop table if exists #cdc_eventvisitlist;
	CREATE table #cdc_eventvisitlist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #cdc_eventvisitlist
	select distinct MemID,Date_S,claimid from(
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	
	or 
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments'))
	
	)

	Union all
	
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt and hcfapos not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth POS')) and (CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth Modifier')) and CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth Modifier')) and CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('Telehealth Modifier')))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient'))
	
	or 
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Nonacute Inpatient'))
	
	)
	)t1
	
	CREATE CLUSTERED INDEX ix_cdc_eventvisitlist ON #cdc_eventvisitlist ([memid],[date_s]);
	
	-- Combining visits to check for count
	drop table if exists #cdc_eventlist3;
	CREATE table #cdc_eventlist3
	(
		Memid varchar(50)
			
	);

	

	Insert into #cdc_eventlist3
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #cdc_eventvisitlist
	union all
	select na.Memid,na.Date_s,na.claimid from #cdc_noncauteinpatientstaylist na
	join #cdc_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #cdc_diabeticmemlist d on d.Memid=t1.Memid and d.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3

	CREATE CLUSTERED INDEX idx_cdc_eventlit3 ON #cdc_eventlist3 ([memid]);
	
	update #cdcdataset set event=1 from #cdcdataset ds join #cdc_eventlist3 el3 on ds.patient_id=el3.memid;
	
	-- Event List 4
	-- Members with insulin or hypoglycemics/ antihyperglycemics on an ambulatory basis

	drop table if exists #cdc_eventlist4;
	CREATE table #cdc_eventlist4
	(
		Memid varchar(50)
				
	);

	

	insert into #cdc_eventlist4
	select distinct Memid from HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Diabetes Medications')
	
	CREATE CLUSTERED INDEX idx_cdc_eventlist4 ON #cdc_eventlist4 ([memid]);
	
	update #cdcdataset set event=1 from #cdcdataset ds	join #cdc_eventlist4 el4 on el4.Memid=ds.patient_id ;
	

	
	-- Numerator 

	-- HbA1c Testing
	drop table if exists #cdc_hba1c;
	CREATE table #cdc_hba1c
	(
		Memid varchar(50),
		servicedate varchar(8)
				
	);

	
	insert into #cdc_hba1c
	select distinct memid,servicedate from(
	select MemId,sdate as servicedate from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')) 
	Union all
	select MemID,date_s as servicedate from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')))
	union all
	select MemId,date as servicedate from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	Union all
	select MemID ,sdate as servicedate from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	Union all
	Select Memid,sdate as servicedate from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	union all
	select MemID,date_s as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' 	
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	CPT in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where code_system !='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	)


	Union all

	select MemID,date_s as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and HCFAPOS!=81 and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	
	and CPT2 in(select code from VALUESET_TO_CODE where code_system ='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))
	
	)t1
	
	
	CREATE CLUSTERED INDEX idx_cdc_hba1c ON #cdc_hba1c ([memid],[servicedate]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_hba1c n1 on ds.patient_id=n1.memid and ds.meas='CDC1';
	
	
	-- Numerator 3 - HbA1c Control <8%
	-- Identify visits with HBa1c<8
	
-- Identify All HbA1c Visits during the measurement year

drop table if exists #cdc_hba1cvst;

CREATE table #cdc_hba1cvst
(
	Memid varchar(50),
	servicedate varchar(8)
	
				
);

Insert into #cdc_hba1cvst
select distinct MemID,servicedate from(
select MemID,date_s as servicedate from HEDIS_VISIT v
where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' 	
and 
CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

union all

select MemID,date_s as servicedate from HEDIS_VISIT v
where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and HCFAPOS!=81 and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and CPT2 in(select code from VALUESET_TO_CODE where code_system ='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

Union all

select MemID,date_s as servicedate from hedis_lab where Measure_id=@meas and Date_s between @ce_startdt and @ce_enddt and Date_S!='' and LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

Union all

select MemID,date_s as servicedate from hedis_lab where Measure_id=@meas and Date_s between @ce_startdt and @ce_enddt and Date_S!='' and CPT_Code in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

Union all

select MemID,date as servicedate from hedis_obs where Measure_id=@meas and date between @ce_startdt and @ce_enddt and date!='' and Ocode in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))


)t1


CREATE CLUSTERED INDEX idx_cdc_hba1c ON #cdc_hba1cvst ([memid],[servicedate]);

-- Identify the  most recent HbA1c Visit
drop table if exists #cdc_hba1cmostrecentvisit;
CREATE table #cdc_hba1cmostrecentvisit
(
	Memid varchar(50),
	servicedate varchar(8)
	
);
	
	

insert into #cdc_hba1cmostrecentvisit
select h1.memid,h1.servicedate from #cdc_hba1cvst h1 where h1.servicedate=(select max(servicedate) from #cdc_hba1cvst h2 where h1.memid=h2.memid)
	
CREATE CLUSTERED INDEX idx_cdc_hba1cmostrecentvisit ON #cdc_hba1cmostrecentvisit ([memid],[servicedate]);
	
-- Identify the HbA1c Results during measurement year with 1 week buffer
drop table if exists #cdc_hba1crslt;
CREATE table #cdc_hba1crslt
(
	Memid varchar(50),
	servicedate varchar(8),
	value varchar(6)
				
);


Insert into #cdc_hba1crslt
select distinct Memid,servicedate,value from(
select MemID,date_s as servicedate,value from HEDIS_LAB where Measure_id=@meas and date_s between @ce_rsltstartdt and @ce_rsltenddt and date_s!='' and LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

Union all

select MemID,date_s as servicedate,value from HEDIS_LAB where Measure_id=@meas and date_s between @ce_rsltstartdt and @ce_rsltenddt and date_s!='' and CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

union all

select MemId,date as servicedate,value from HEDIS_OBS where Measure_id=@meas and date between @ce_rsltstartdt and @ce_rsltenddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test'))

union all

select MemID,Date_S as servicedate,'7.00' as value from HEDIS_VISIT v
where Measure_id=@meas and Date_S between @ce_rsltstartdt and @ce_rsltenddt and Date_S!='' and hcfapos!=81 and 
CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
and 
CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Less Than 7.0','HbA1c Level Greater Than or Equal To 7.0 and Less Than 8.0'))

Union all

select MemID,Date_S as servicedate,'9.50' as value from HEDIS_VISIT v
where Measure_id=@meas and Date_S between @ce_rsltstartdt and @ce_rsltenddt and Date_S!='' and hcfapos!=81 and
CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
and 
CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than 9.0'))

Union all

select MemID,Date_S as servicedate,'8.50' as value from HEDIS_VISIT v
where Measure_id=@meas and Date_S between @ce_rsltstartdt and @ce_rsltenddt and Date_S!='' and hcfapos!=81 and
CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
and 
CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than or Equal To 8.0 and Less Than or Equal To 9.0'))

Union all

select MemId,date as servicedate,'7.00' as value from HEDIS_OBS where Measure_id=@meas and date between @ce_rsltstartdt and @ce_rsltenddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Less Than 7.0','HbA1c Level Greater Than or Equal To 7.0 and Less Than 8.0'))

Union all

select MemId,date as servicedate,'9.50' as value from HEDIS_OBS where Measure_id=@meas and date between @ce_rsltstartdt and @ce_rsltenddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than 9.0'))

Union all

select MemId,date as servicedate,'8.50' as value from HEDIS_OBS where Measure_id=@meas and date between @ce_rsltstartdt and @ce_rsltenddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than or Equal To 8.0 and Less Than or Equal To 9.0'))


)t1
	

CREATE CLUSTERED INDEX idx_cdc_hba1c ON #cdc_hba1crslt ([memid],[servicedate]);


-- Create Numerator Compliant list

drop table if exists #cdc_num3list;

CREATE table #cdc_num3list
(
	Memid varchar(50),
						
);
	
	
insert into #cdc_num3list
-- Members who had HbA1c result<=8 with 7 days of HbA1c Test
select t1.Memid from(
select rv.memid,rs.servicedate,rs.value from #cdc_hba1cmostrecentvisit rv
join #cdc_hba1crslt rs on rv.Memid=rs.Memid
where 
 DATEDIFF(DAY,rs.servicedate,rv.servicedate )<=7 and DATEDIFF(DAY,rs.servicedate,rv.servicedate )>=-7

)t1 where t1.servicedate = (
select max(t2.servicedate) from(
select rv.memid,rs.servicedate,rs.value from #cdc_hba1cmostrecentvisit rv
join #cdc_hba1crslt rs on rv.Memid=rs.Memid
where 
 DATEDIFF(DAY,rs.servicedate,rv.servicedate )<=7 and DATEDIFF(DAY,rs.servicedate,rv.servicedate )>=-7 and rs.value!=''
 )t2 where t1.memid=t2.memid)
 and value<cast(8 as float) and value!=''
 
CREATE CLUSTERED INDEX idx_cdc_hba1c ON #cdc_num3list ([memid]);


update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num3list n3 on ds.patient_id=n3.memid and ds.meas='CDC10';

-- CDC 2 HbA1c Poor Control

drop table if exists #cdc_num2list;
CREATE table #cdc_num2list
(
	Memid varchar(50),
						
);
	
	
insert into #cdc_num2list
select distinct memid from(
-- Identify Members who had Hba1c with a reading of >9 or no reading within 7 days
select Memid  from(
 select rv.memid,rs.servicedate,rs.value,ROW_NUMBER() OVER 
     ( 
         PARTITION BY rv.Memid
         ORDER BY value DESC 
     ) AS Recency from #cdc_hba1cmostrecentvisit rv
join #cdc_hba1crslt rs on rv.Memid=rs.Memid
where  DATEDIFF(DAY,rs.servicedate,rv.servicedate ) between -7 and 7
 )t3
 where (value>cast(9 as float) or value='') and recency=1 

 Union all
-- Identify members who had HbA1c test but had not result within 7 days
 select distinct rv.memid from #cdc_hba1cmostrecentvisit rv
	join #cdc_hba1crslt rs on rv.Memid=rs.Memid
	where 
	 (DATEDIFF(DAY,rs.servicedate,rv.servicedate )>7 or DATEDIFF(DAY,rs.servicedate,rv.servicedate )<-7)
	 and rv.memid not in (
 select rv.memid from #cdc_hba1cmostrecentvisit rv
join #cdc_hba1crslt rs on rv.Memid=rs.Memid
where  DATEDIFF(DAY,rs.servicedate,rv.servicedate ) between -7 and 7
 )

	 Union all
-- Identify members who had no HbA1c test during measurement year
 select memid from #cdc_memlist where memid not in(select memid from #cdc_hba1cvst) 

 union all
-- Identify Members who had HbA1c test but no result was ever recorded
 select distinct v.memid from #cdc_hba1cmostrecentvisit v
 left outer join #cdc_hba1crslt r on v.memid=r.memid
 where
 r.memid is null


 )t1

CREATE CLUSTERED INDEX idx_cdc_hba1c ON #cdc_num2list ([memid]);



update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num2list n2 on ds.patient_id=n2.memid and ds.meas='CDC2' and ds.CE=1 AND ds.EVENT=1 AND ds.rexcl=0 and ds.rexcld=0


	
-- Numerator 4 - Eye Exam
-- Diabetic Retinal Screening  During the Measurement Year
	
	drop table if exists #cdc_num_4_1_list;
	CREATE table #cdc_num_4_1_list
	(
		Memid varchar(50)
						
	);
	
	
	
	insert into #cdc_num_4_1_list
	Select distinct Memid from(
	select p.memid from HEDIS_proc p
	join hedis_visit_e v on p.memid=v.memid and p.sdate=v.sdate and p.measure_id=v.measure_id
	where p.Measure_id=@meas and p.pstatus='EVN' and v.providerid in(select ProvId from HEDIS_PROVIDER where Measure_id=@meas and EyeCProv=
	'Y') and p.sdate between @ce_startdt and @ce_enddt and p.sdate!='' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	
	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and hcfapos!=81 and v.provid in(select ProvId from HEDIS_PROVIDER where Measure_id='CDC' and EyeCProv='Y')
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetic Retinal Screening'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_1_list ON #cdc_num_4_1_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_1_list n4_1 on ds.patient_id=n4_1.memid and ds.meas='CDC4';
	
	-- Num 4.2 - Diabetic Retinal Screening During the year prior with negative retinopathy
	-- Current no way to identify negative retinopathy
	
	
	-- Numerator 4.3 - Diabetic Retinal Screening with Diabetes Mellitus Without Complications prior year
	drop table if exists #cdc_memberswdbmwoc
	CREATE table #cdc_memberswdbmwoc
	(
		Memid varchar(50),
		servicedate varchar(8)
						
	);
	
	

	Insert into #cdc_memberswdbmwoc
	select distinct MemId,servicedate from(
	select MemId,sdate as servicedate from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt1 and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications')) 
	Union all
	select MemID,Date_S as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt1 and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Mellitus Without Complications'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetes Mellitus Without Complications'))
	)
)t1

	CREATE CLUSTERED INDEX idx_cdc_memberswdbmwoc ON #cdc_memberswdbmwoc ([memid],[servicedate]);
	
	-- Members with Diabetic Retinal Screening prior year
	drop table if exists #cdc_mem_w_retinalscreening;
	CREATE table #cdc_mem_w_retinalscreening
	(
		Memid varchar(50),
		servicedate varchar(8)
						
	);
	
	
	
	insert into #cdc_mem_w_retinalscreening
	Select distinct Memid,servicedate from(
	select p.memid,p.sdate as servicedate  from HEDIS_proc p
	join hedis_visit_e v on p.memid=v.memid and p.sdate=v.sdate and p.measure_id=v.measure_id
	where p.Measure_id=@meas and p.pstatus='EVN' and v.providerid in(select ProvId from HEDIS_PROVIDER where Measure_id=@meas and EyeCProv=
	'Y') and p.sdate between @ce_startdt1 and @ce_enddt1 and p.sdate!='' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	
	Union all
	select MemID,Date_S as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt1 and Date_S!='' and hcfapos!=81 and v.provid in(select ProvId from HEDIS_PROVIDER where Measure_id='CDC' and EyeCProv='Y')
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetic Retinal Screening'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_cdc_mem_w_retinalscreening ON #cdc_mem_w_retinalscreening ([memid],[servicedate]);
	
	drop table if exists #cdc_num_4_3_list
	CREATE table #cdc_num_4_3_list
	(
		Memid varchar(50)
						
	);
	
	
	
	Insert into #cdc_num_4_3_list
	select distinct t1.memid from #cdc_mem_w_retinalscreening t1
	join #cdc_memberswdbmwoc t2 on t1.memid=t2.memid and t1.servicedate=t2.servicedate
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_3_list ON #cdc_num_4_3_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_3_list n4_3 on ds.patient_id=n4_3.memid and ds.meas='CDC4';
	
	-- Numerator 4 - Eye Exam With Evidence of Retinopathy Value Set or Eye Exam Without Evidence of Retinopathy Value Set measurement year
	
	drop table if exists #cdc_num_4_4_list;
	CREATE table #cdc_num_4_4_list
	(
		Memid varchar(50)
						
	);
	
	
	
	insert into #cdc_num_4_4_list
		select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and hcfapos!=81 and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Eye Exam With Evidence of Retinopathy','Eye Exam Without Evidence of Retinopathy'))
	)
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_4_list ON #cdc_num_4_4_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_4_list n4_4 on ds.patient_id=n4_4.memid and ds.meas='CDC4';
	
	-- Numerator 4 the Eye Exam Without Evidence of Retinopathy prior year
	drop table if exists #cdc_num_4_5_list;
	CREATE table #cdc_num_4_5_list
	(
		Memid varchar(50)
						
	);
	
	
	
	insert into #cdc_num_4_5_list
		select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt1 and Date_S!='' and hcfapos!=81 and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Eye Exam Without Evidence of Retinopathy'))
	)
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_5_list ON #cdc_num_4_5_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_5_list n4_5 on ds.patient_id=n4_5.memid and ds.meas='CDC4';
	
	-- Numerator 4 Eye Exam "Diabetic Retinal Screening Negative In Prior Year " during the measurement year
	
	drop table if exists #cdc_num_4_6_list;
	CREATE table #cdc_num_4_6_list
	(
		Memid varchar(50)
						
	);
	
	
	
	insert into #cdc_num_4_6_list
		select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and hcfapos!=81 and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetic Retinal Screening Negative In Prior Year'))
	)
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_6_list ON #cdc_num_4_6_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_6_list n4_6 on ds.patient_id=n4_6.memid and ds.meas='CDC4';
	
	-- Numerator 4.7 Eye Exam ?	Unilateral eye enucleation (Unilateral Eye Enucleation Value Set) with a bilateral modifier (Bilateral Modifier Value Set). 
	
	drop table if exists #cdc_num_4_7_list;
	CREATE table #cdc_num_4_7_list
	(
		Memid varchar(50)
						
	);
	
	
	
	insert into #cdc_num_4_7_list
		select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S <=@ce_enddt and Date_S!='' and hcfapos!=81 and (CPTMod_1 in (select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Modifier')) or CPTMod_2 in (select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Modifier')) or CPT2Mod in (select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Modifier')))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Unilateral Eye Enucleation'))
	)
		
	CREATE CLUSTERED INDEX idx_cdc_num_4_7_list ON #cdc_num_4_7_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_7_list n4_7 on ds.patient_id=n4_7.memid and ds.meas='CDC4';
	
	-- Numerator 4.8 - Two unilateral eye enucleations (Unilateral Eye Enucleation Value Set) with service dates 14 days or more apart
drop table if exists #cdc_num_4_8_list;
CREATE table #cdc_num_4_8_list
(
	Memid varchar(50)

);
	

	

with cte as
(select MemID
       ,datediff(d
                ,lag(servicedate) over (partition by Memid order by servicedate)
                ,servicedate) DaysSinceLastOrder from(
				select memid,sdate as servicedate from Hedis_proc where Measure_id=@meas and sdate<=@ce_enddt and sdate!='' and pstatus='EVN' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	union all
	select memid,date_s as servicedate from HEDIS_VISIT v
	where Measure_id=@meas  and Date_S <=@ce_enddt and Date_S!='' and hcfapos!=81 
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Unilateral Eye Enucleation'))
	)
	)t1)
	Insert into #cdc_num_4_8_list
	select Memid
	from cte
	where DaysSinceLastOrder >= 14
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_8_list ON #cdc_num_4_8_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_8_list n4_8 on ds.patient_id=n4_8.memid and ds.meas='CDC4';
	
	-- Numerator 4.9 : ?	Left unilateral eye enucleation (Unilateral Eye Enucleation Left Value Set) and right unilateral eye enucleation (Unilateral Eye Enucleation Right Value Set) on the same or different dates of service
	
	drop table if exists #cdc_mem_leftEyeNucleation
	CREATE table #cdc_mem_leftEyeNucleation
	(
		Memid varchar(50),
		servicedate varchar(8)

	);
		
	
	
	
	
	Insert into #cdc_mem_leftEyeNucleation
	select distinct MemId,servicedate from(
	select MemID,sdate as servicedate from HEDIS_PROC where MEASURE_ID=@meas and sdate <=@ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	Union all
	select MemID,date_s as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S <=@ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Left'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Unilateral Eye Enucleation Left'))
	)
)t1

	CREATE CLUSTERED INDEX idx_cdc_mem_leftEyeNucleation ON #cdc_mem_leftEyeNucleation ([memid],[servicedate]);
	
	-- Right eye nucelation
	drop table if exists #cdc_mem_rightEyeNucleation
	CREATE table #cdc_mem_rightEyeNucleation
	(
		Memid varchar(50),
		servicedate varchar(8)

	);
	

	
	
	Insert into #cdc_mem_rightEyeNucleation
	select distinct MemId,servicedate from(
	select MemID,sdate as servicedate from HEDIS_PROC where MEASURE_ID=@meas and sdate <=@ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	Union all
	select MemID,Date_S as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S <=@ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation Right'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Unilateral Eye Enucleation Right'))
	)
)t1
	
	CREATE CLUSTERED INDEX idx_cdc_mem_rightEyeNucleation ON #cdc_mem_rightEyeNucleation ([memid],[servicedate]);
	
	-- Identify members who had left and right enucleation
	drop table if exists #cdc_num_4_9_list;
	CREATE table #cdc_num_4_9_list
	(
		Memid varchar(50)

	);
		
	
	
	insert into #cdc_num_4_9_list
	select t1.memid from #cdc_mem_leftEyeNucleation t1
	join #cdc_mem_rightEyeNucleation t2 on t1.memid=t2.memid
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_9_list ON #cdc_num_4_9_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_9_list n4_9 on ds.patient_id=n4_9.memid and ds.meas='CDC4';
	
	--Numerator 4.10 :?	A unilateral eye enucleation (Unilateral Eye Enucleation Value Set) and a left unilateral eye enucleation (Unilateral Eye Enucleation Left Value Set) with service dates 14 days or more apart
	
	
	drop table if exists #cdc_mem_EyeNucleation
	CREATE table #cdc_mem_EyeNucleation
	(
		Memid varchar(50),
		servicedate varchar(8)

	);
		
	
	
	insert into #cdc_mem_EyeNucleation
	select distinct MemId,servicedate from(
	select MemID,sdate as servicedate from HEDIS_PROC where MEASURE_ID=@meas and sdate <=@ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	Union all
	select MemID,date_s as servicedate from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S <=@ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Eye Enucleation'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Unilateral Eye Enucleation'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_cdc_mem_EyeNucleation ON #cdc_mem_EyeNucleation ([memid],[servicedate]);
	
	drop table if exists #cdc_num_4_10_list;
	CREATE table #cdc_num_4_10_list
	(
		Memid varchar(50)

	);
		
	
	
	insert into #cdc_num_4_10_list
	select t1.memid from #cdc_mem_EyeNucleation t1
	join #cdc_mem_leftEyeNucleation t2 on t1.memid=t2.memid
	where Abs(DATEDIFF(day,t1.servicedate,t2.servicedate))>=14
	
	CREATE CLUSTERED INDEX idx_cdc_num_4_10_list ON #cdc_num_4_10_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_10_list n4_10 on ds.patient_id=n4_10.memid and ds.meas='CDC4';
	
	
	-- Numerator 4.11:?	A unilateral eye enucleation (Unilateral Eye Enucleation Value Set) and a right unilateral eye enucleation (Unilateral Eye Enucleation Right Value Set) with service dates 14 days or more apart.
	
	drop table if exists #cdc_num_4_11_list;
	CREATE table #cdc_num_4_11_list
	(
		Memid varchar(50)

	);
		
	
	
	insert into #cdc_num_4_11_list
	select t1.memid from #cdc_mem_EyeNucleation t1
	join #cdc_mem_rightEyeNucleation t2 on t1.memid=t2.memid
	where Abs(DATEDIFF(day,t1.servicedate,t2.servicedate))>=14
	

	CREATE CLUSTERED INDEX idx_cdc_num_4_11_list ON #cdc_num_4_11_list ([memid]);
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_11_list n4_11 on ds.patient_id=n4_11.memid and ds.meas='CDC4';
	

	-- Numerator Nephropathy CDC 7
	drop table if exists #cdc_num_7_list;
	CREATE table #cdc_num_7_list
	(
		Memid varchar(50)

	);
		
	
	
	insert into #cdc_num_7_list
	select distinct Memid from(
	select MemId from HEDIS_DIAG where Measure_id=@meas and sdate between @ce_startdt and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure')) 
	Union all
	select MemID from HEDIS_LAB where Measure_id=@meas and date_s between @ce_startdt and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure')))
	union all
	select MemId from HEDIS_OBS where Measure_id=@meas and date between @ce_startdt and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	Union all
	select MemID from HEDIS_PROC where Measure_id=@meas and sdate between @ce_startdt and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))

	Union all
	select MemID from HEDIS_VISIT v
	where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and hcfapos!=81 and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Urine Protein Tests','Nephropathy Treatment','CKD Stage 4','ESRD Diagnosis','Nephrectomy','Kidney Transplant','Dialysis Procedure'))
	)
	
	Union all 

	select MemID from HEDIS_VISIT v
	where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Urine Protein Tests'))
	)
	
	union all
	select Memid from HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='ACE Inhibitor and ARB Medications')
	union all
	select MemID from Hedis_pharm_c where startdate between @ce_startdt and @ce_enddt and rxnorm in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='ACE Inhibitor and ARB Medications')
	Union all
	select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt and ProvID in(select ProvId from hedis_provider where Measure_id=@meas and Neph='Y')
	union all
	select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate between @ce_startdt and @ce_enddt and providerid in(select ProvId from hedis_provider where Measure_id=@meas and Neph='Y')
	
	)t1

	CREATE CLUSTERED INDEX idx_cdc_num_7_list ON #cdc_num_7_list ([memid]);

	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_7_list n7 on ds.patient_id=n7.memid and ds.meas='CDC7';
	
	-- Numerator CDC 9 :BP Control <140/90 mm Hg
	
	-- Identify Vst visit (Outpatient Without UBREV Value Set), telephone visit (Telephone Visits Value Set), e-visit or virtual check-in (Online Assessments Value Set), a nonacute inpatient encounter (Nonacute Inpatient Value Set), or remote monitoring event (Remote Blood Pressure Monitoring Value Set) during the measurement year. 
	
	-- Identify Eligible viit list
	drop table if exists #cdc_numvstlist;				
	CREATE table #cdc_numvstlist
	(
		Memid varchar(50),
		servicedate varchar(8)
						
	);

	
	
	Insert into #cdc_numvstlist
	select distinct Memid,servicedate from(
	select MemId,sdate as servicedate from HEDIS_DIAG where Measure_id=@meas and sdate='' and sdate between @ce_startdt and @ce_enddt and  dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')) 
	
	union all
	select MemID ,date_s as servicedate from HEDIS_LAB where Measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')))
	Union all
	select Memid,date as servicedate from HEDIS_OBS where Measure_id=@meas and date!='' and  date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	Union all

	select MemID,sdate as servicedate from HEDIS_PROC where Measure_id=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt and @ce_enddt and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	union all
	Select Memid,sdate as servicedate from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	union all
	select MemID,Date_S as servicedate from HEDIS_VISIT v
	where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_cdc_numvstlist ON #cdc_numvstlist ([memid],[servicedate]);	

	-- Identify BP Reading visit

	drop table if exists #cdc_bpreadinglist;				
	CREATE table #cdc_bpreadinglist
	(
		Memid varchar(50),
		servicedate varchar(8)
						
	);

	Insert into #cdc_bpreadinglist
	select distinct memid,servicedate from(
	select Memid,Date as servicedate from hedis_obs where Measure_id=@meas and Date between @ce_startdt and @ce_enddt and OCode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure')) 
	Union all
	select MemID,Date_S as servicedate from HEDIS_VISIT v
	where Measure_id=@meas  and Date_S between @ce_startdt and @ce_enddt
	and 
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure'))
	and
	CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	)t1

	

	
	CREATE CLUSTERED INDEX idx_#cdc_mostrecentbpvstlist ON #cdc_bpreadinglist ([memid],[servicedate]);	

	-- Join readings with visit list
	drop table if exists #cdc_bpwithnumvstlist;				
	CREATE table #cdc_bpwithnumvstlist
	(
		Memid varchar(50),
		servicedate varchar(8)
						
	);

	insert into #cdc_bpwithnumvstlist
	select t1.memid,t1.servicedate from #cdc_numvstlist t1
	join #cdc_bpreadinglist t2 on t1.Memid=t2.Memid and t1.servicedate=t2.servicedate


	CREATE CLUSTERED INDEX idx_#cdc_bpwithnumvstlist ON #cdc_bpwithnumvstlist ([memid],[servicedate]);	

	
	-- identify most recent reading
	drop table if exists #cdc_mostrecentbpreadinglist;				
	CREATE table #cdc_mostrecentbpreadinglist
	(
		Memid varchar(50),
		servicedate varchar(8)
						
	);

	Insert into #cdc_mostrecentbpreadinglist
	select t1.memid,t1.servicedate from #cdc_bpwithnumvstlist t1 where t1.servicedate=(select max(t2.servicedate) from #cdc_bpwithnumvstlist t2 where t1.Memid=t2.Memid)

	
	CREATE CLUSTERED INDEX idx_#cdc_mostrecentbpreadinglist ON #cdc_mostrecentbpreadinglist ([memid],[servicedate]);	
	
	
	-- Numerator compliant members
	drop table if exists #cdc_numcompliantmemlist;				
	CREATE table #cdc_numcompliantmemlist
	(
		Memid varchar(50),
		servicedate varchar(8)
				
	);

	
	
	insert into #cdc_numcompliantmemlist
	-- Members with Systolic <140 and diastolic<90 based on results
	select distinct memid,servicedate from(
	select distinct s.Memid,s.servicedate from(
	select Memid,date as servicedate from HEDIS_OBS where Measure_id=@meas and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')) and value<cast(140 as float)
	)s
	join
	(
	select Memid,date as servicedate from HEDIS_OBS where Measure_id=@meas and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')) and value<cast(90 as float)
	)d on d.MemID=s.MemID and d.servicedate=s.servicedate

	Union all

-- Members with Systolic <140 and diastolic<90 based on CPT || category codes
	select distinct s.MemId,s.servicedate from(
	select MemID,Date_S as servicedate,claimid from HEDIS_VISIT v
	where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Systolic Less Than 140')) 
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Systolic Less Than 140'))
	
	)
	and
	CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	
	)s
	Join
	(
	select distinct MemID,Date_S as servicedate,claimid from HEDIS_VISIT v
	where Measure_id=@meas and Date_S between @ce_startdt and @ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic 80-89','Diastolic Less Than 80')) 
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic 80-89','Diastolic Less Than 80'))
	
	)
	and
	CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	)d on s.MemID=d.MemID and s.servicedate=d.servicedate

	)numcompmemlist
	
				
	CREATE CLUSTERED INDEX idx_cdc_numcompliantmemlist ON #cdc_numcompliantmemlist ([memid],[servicedate]);
	
    -- Identify bp readings taken during the specified visit types        
	drop table if exists #cdc_num_9_list;						
	CREATE table #cdc_num_9_list
	(
		Memid varchar(50),
		
				
	);

	
	
	Insert into #cdc_num_9_list
	select distinct nc.memid from #cdc_numcompliantmemlist nc
	join #cdc_mostrecentbpreadinglist nv on nv.memid=nc.memid and nv.servicedate=nc.servicedate
	
	
	
	
	CREATE CLUSTERED INDEX idx_cdc_num_9_list ON #cdc_num_9_list ([memid]);
	

	-- Updating NUM cdcumn
	update #cdcdataset set num=1 from #cdcdataset ds	join #cdc_num_9_list n9 on n9.Memid=ds.patient_id  and ds.meas='CDC9';
	
	
	-- SES for CDC 4
	--SES Startification

update #cdcdataset set lis=1 from #cdcdataset ds join HEDIS_MMDF m on m.beneficiary_id=ds.patient_id and m.Measure_id=@meas
where m.run_date between @ce_startdt and @ce_enddt and m.LIS_Premium_Subsidy>0 and ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

update #cdcdataset set lis=1 from #cdcdataset ds join HEDIS_LISHIST l on l.beneficiary_id=ds.patient_id and l.Measure_id=@meas
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and ((Low_Income_Period_End_Date>=@ce_startdt and Low_Income_Period_Start_Date<=@ce_enddt) or Low_Income_Period_End_Date='');


-- Item 48
drop table if exists #cdc_orec;
CREATE table #cdc_orec
(
		Memid varchar(50),
		orec varchar(2)
			
);

CREATE CLUSTERED INDEX idx_cdc_orec ON #cdc_orec ([memid],[orec]);

Insert into #cdc_orec
select m1.Beneficiary_ID,m1.Original_Reason_for_Entitlement_Code_OREC as orec from HEDIS_MMDF m1 where m1.Measure_id=@meas  and m1.Run_Date=(select max(m2.Run_Date) from HEDIS_MMDF m2 where m2.MEASURE_ID=@meas  and m1.Beneficiary_ID=m2.Beneficiary_ID and m2.Run_Date between @ce_startdt and @ce_enddt and m2.Original_Reason_for_Entitlement_Code_OREC!='') order by m1.Beneficiary_ID


update #cdcdataset set orec=o.orec from #cdcdataset ds
join #cdc_orec o on o.Memid=ds.patient_id
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') ;

update #cdcdataset set meas='CDC4NON' WHERE orec=0 AND lis=0 AND event=1 and ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')  and meas='CDC4';


UPDATE #cdcdataset SET meas='CDC4LISDE' WHERE orec=0 AND lis=1 AND event=1 and ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')  and meas='CDC4';

UPDATE #cdcdataset SET meas='CDC4DIS' WHERE orec IN(1,3) AND lis=0 AND event=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')  and meas='CDC4';

UPDATE #cdcdataset SET meas='CDC4CMB' WHERE orec IN(1,3) AND lis=1 AND event=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')  and meas='CDC4';


UPDATE #cdcdataset SET meas='CDC4OT' WHERE orec IN(2,9) AND event=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')  and meas='CDC4';




	
	-- Insert data in Output Table
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END

	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,@meas as Measure_ID,@meas_year,@runid FROM #cdcdataset
	

GO
