SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [HDS].[RUN_HEDIS_PCR_prod] @meas_year nvarchar(4)
AS


--Declare vaiable

Declare @ce_startdt varchar(10);
Declare @ce_startdt1 varchar(10);
Declare @ce_dischstartdt varchar(10);
Declare @ce_enddt varchar(10);
Declare @ce_enddt1 varchar(10);
Declare @ce_dischenddt varchar(10);
Declare @meas varchar(10);
Declare @runid INT=0;

Set @meas='159';--'PCR';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_dischstartdt=concat(@meas_year,'-01-03');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_dischenddt=concat(@meas_year,'-12-01');

-- hospice Exclusion

drop table if exists #pcr_hospicemembers;

CREATE table #pcr_hospicemembers
(
	Memid varchar(100)
		
);

	

Insert into #pcr_hospicemembers
select distinct t1.MemID from
(
	Select 
		Memid 
		from hds.HEDIS_VISIT_E 
		where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt 
		and activity IN(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select 
		Beneficiary_id as Memid 
		from hds.Hedis_MMDF 
		where Measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select 
		MemID 
		from hds.HEDIS_VISIT v
		where Measure_id=@meas and HCFAPOS!='81' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
		and (
			CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name ='Hospice Intervention')
			or
			HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Rev in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name ='Hospice Encounter')
		)
)t1
	
CREATE CLUSTERED INDEX idx_hospicemembers ON #pcr_hospicemembers ([memid]);


-- Inpatient and Observation Stay

drop table if exists #pcr_ipstaylist;
CREATE table #pcr_ipstaylist
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	claimid varchar(100),
	claimstatus int,
	staytype varchar(100)
				
);

Insert into #pcr_ipstaylist
-- Inpatient Stay list
select 
	Memid
	,Date_S
	,Date_Adm
	,Date_Disch
	,ClaimID
	,ClaimStatus
	,'Acute' as staytype 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!='81' and
	REV in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')) and claimstatus=1
	-- and memid=100598

Union all

-- Observation Stay List
select 
	Memid
	,Date_S
	,Date_Adm
	,Date_Disch
	,ClaimID
	,ClaimStatus
	,'Observation' as staytype 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!='81' and
	REV in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Observation Stay')) and claimstatus=1


CREATE CLUSTERED INDEX idx_pcr_ipstaylist ON #pcr_ipstaylist ([memid],[Date_S],[Date_Disch],[claimid]);


-- Identify SNF Stays


drop table if exists #pcr_SNFStays;

create table #pcr_SNFStays
(
	Memid varchar(100),
	Date_Adm varchar(10),
	claimid varchar(100)
)
Insert into #pcr_SNFStays
select 
	Memid
	,Date_Adm
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_dischenddt and SuppData='N' and HCFAPOS!='81' 
	and	(
		REV in(select code_new from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Skilled Nursing Stay') 
		or 
		RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code_new from hds.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Skilled Nursing Stay')
	)
	and ClaimStatus=1



-- Identify Nonacute Inpatient Stay


drop table if exists #pcr_naipstaylist;
CREATE table #pcr_naipstaylist
(
	Memid varchar(100),
	claimid varchar(100),
				
);

-- Identify non acute Inpatient Stays
Insert into #pcr_naipstaylist
select 
	Memid
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!='81' 
	and (
		REV in(select code_new from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Nonacute Inpatient Stay') 
		or 
		RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code_new from hds.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay')
	)

CREATE CLUSTERED INDEX idx_pcr_ipstaylist ON #pcr_naipstaylist ([memid],[claimid]);

-- Identify only Acute Inatient and Observation Stays

drop table if exists #pcr_ai_obs_staylist;
CREATE table #pcr_ai_obs_staylist
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	claimid varchar(100),
	claimstatus INT,
	staytype varchar(20)
				
);
-- removing non acute inpatient stays from total Inpatient stays
Insert into #pcr_ai_obs_staylist
select 
	t1.Memid
	,t1.Date_S
	,t1.Date_Adm
	,t1.Date_Disch
	,t1.claimid
	,t1.claimstatus
	,staytype 
	from #pcr_ipstaylist t1
	left outer join #pcr_naipstaylist t2 on t1.Memid=t2.memid and t1.claimid=t2.claimid
	Where t2.memid is null

CREATE CLUSTERED INDEX idx_pcr_ai_obs_staylist ON #pcr_ai_obs_staylist ([memid],[Date_S],[Date_Disch],[claimid]);



-- Identifying transfers and assigning new discharge dates if transfer

drop table if exists #pcr_mergedstays;
CREATE table #pcr_mergedstays
(
	Memid varchar(100),
	AdmissionDate varchar(10),
	DischargeDate varchar(10),
	StayType varchar(20),
	claimid varchar(100),
	tclaimid varchar(100),
	claimstatus INT
	
				
);

Insert into #pcr_mergedstays
select 
	t3.Memid
	,t3.newadmdate as AdmissionDate
	,t3.newdischdate as DischargeDate
	,t3.newstaytype as StayType
	,t3.claimid
	,t3.tclaimid
	,t3.claimstatus
	from(
		select 
			distinct  t2.Memid,t2.Date_S,t2.claimid,t2.claimstatus
			-- logic to adjust dates,stay type,claim id if it is a direct transfer
			,case 
				when prevstaydiff between 0 and 1 then prevadmdate 
				else date_adm 
				end as newadmdate
			,case 
				when nxtstaydiff between 0 and 1 then nxtdischdate 
				else date_disch 
				end as newdischdate
			,case 
				when nxtstaydiff between 0 and 1 then nxtstaytype 
				else staytype 
				end as newstaytype 
			,case 
				when nxtstaydiff between 0 and 1 then nxtclaimid
				when prevstaydiff between 0 and 1 then prevclaimid
				else claimid 
				end as tclaimid


			from(
	
				select 
					t1.*
					-- checking for difference between stays
					,DATEDIFF(day,date_disch,nxtadmdate) as nxtstaydiff
					,datediff(day,prevdischdate,Date_Adm) as prevstaydiff 
					from(
						select 
							s.*
							,isnull(lead(s.Date_Adm,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Adm) as nxtadmdate
							,isnull(lead(s.Date_Disch,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Disch) as nxtdischdate
							,isnull(lead(s.staytype,1) over(partition by Memid order by Date_Adm,Date_Disch),staytype) as nxtstaytype
							,isnull(lead(s.claimid,1) over(partition by Memid order by Date_Adm,Date_Disch),claimid) as nxtclaimid
							,isnull(lag(s.Date_Adm,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Adm) as prevadmdate
							,isnull(lag(s.Date_Disch,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Disch) as prevdischdate
							,isnull(lag(s.claimid,1) over(partition by Memid order by Date_Adm,Date_Disch),claimid) as prevclaimid
							from #pcr_ai_obs_staylist s
							--where s.memid=95338
					)t1
			)t2 
)t3 order by 1,2,3


-- List of direct transfers Direct Transfers


drop table if exists #pcr_directtransfers;
CREATE table #pcr_directtransfers
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	claimid varchar(100),
	claimstatus INT,
	staytype varchar(20),
	nxtclaimid varchar(100),
	prvclaimid varchar(100)
);


Insert into #pcr_directtransfers
select 
	Memid
	,Date_S
	,Date_Adm
	,Date_Disch
	,claimid
	,Claimstatus
	,Staytype
	,case 
		when nxtadmdiff between 0 and 1 then nextclaimid 
		else claimid 
	end as nxtclaimid
	,case 
		when prevadmdiff between 0 and 1 then prevclaimid 
		else claimid 
	end as prvclaimid
	from(
		select *
			,datediff(day,Date_disch,nextadmdate) as nxtadmdiff
			,datediff(day,Date_adm,prevdischdate) as prevadmdiff 
			from(
				select *
					,isnull(lead(Date_Adm,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,-1,Date_adm) as date),112)) as nextadmdate
					,isnull(lead(Date_disch,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,-1,Date_disch) as date),112)) as nextdischdate
					,isnull(lead(claimid,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),claimid) as nextclaimid
					,isnull(lag(claimid,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),claimid) as prevclaimid
					,isnull(lag(Date_Adm,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,1,Date_adm) as date),112)) as prevadmdate
					,isnull(lag(Date_disch,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,1,Date_disch) as date),112)) as prevdischdate 
					from #pcr_ipstaylist --where memid=95167
			)t1
		)t2 
	where Date_Disch between @ce_startdt and @ce_dischenddt and ( nxtadmdiff between 0 and 1 or prevadmdiff between -1 and 0)


-- Looking Direct transfers which are extending after 20201201


drop table if exists #pcr_directtransfersexcl;

CREATE table #pcr_directtransfersexcl
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	claimid varchar(100),
	claimstatus INT,
	staytype varchar(20),
	nextadmdate varchar(10),
	nextdischdate varchar(10),
	prevadmdate varchar(10),
	prevdischdate varchar(10),
	nxtadmdiff Int,
	prevadmdiff INT
);


Insert into #pcr_directtransfersexcl
select * from(
	select *
		,datediff(day,Date_disch,nextadmdate) as nxtadmdiff
		,datediff(day,Date_adm,prevdischdate) as prevadmdiff 
		from(
			select *
				,isnull(lead(Date_Adm,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,-1,Date_adm) as date),112)) as nextadmdate
				,isnull(lead(Date_disch,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,-1,Date_disch) as date),112)) as nextdischdate
				,isnull(lag(Date_Adm,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,1,Date_adm) as date),112)) as prevadmdate
				,isnull(lag(Date_disch,1) over(partition by Memid order by Date_S,Date_Adm,Date_Disch),convert(varchar,cast(dateadd(day,1,Date_disch) as date),112)) as prevdischdate 
				from #pcr_ai_obs_staylist
			)t1
		)t2 
	where (Date_Disch<=@ce_dischenddt and nextdischdate>@ce_dischenddt and nxtadmdiff between 0 and 1) 
	  or
	  (Date_Disch>@ce_dischenddt and prevdischdate<=@ce_dischenddt and prevadmdiff between -1 and 0) order by memid


-- Identify Prgnacncy Visits

drop table if exists #pcr_pregvstlist;

CREATE table #pcr_pregvstlist
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_pregvstlist
select t1.* from(
	select 
		Memid
		,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and hcfapos!='81' and suppdata='N' 
	and Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	)t1
join hds.hedis_member_gm gm on t1.MemID=gm.MemID and gm.Measure_id=@meas and Gender='F'


--Identify Perinatal Conditions

drop table if exists #pcr_perinatalvstlist;

CREATE table #pcr_perinatalvstlist
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_perinatalvstlist
select 
	Memid
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and hcfapos!='81' and suppdata='N' 
	and Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Perinatal Conditions')


--Identify members who were dead on discharge


drop table if exists #pcr_deadmembers;
CREATE table #pcr_deadmembers
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_deadmembers
select 
	Memid
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_dischenddt and hcfapos!='81' and suppdata='N' and DischStatus=20;

--Idntifyinh Planned Admissions
-- Indetifying Chemotherapy


drop table if exists #pcr_chemovstlist;

CREATE table #pcr_chemovstlist
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_chemovstlist
select 
	Memid
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_dischstartdt and @ce_enddt and hcfapos!='81' and suppdata='N' 
	and Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Chemotherapy Encounter')


-- Rehabilitation

drop table if exists #pcr_rehabvstlist;

CREATE table #pcr_rehabvstlist
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_rehabvstlist
select Memid
	,ClaimID
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_dischstartdt and @ce_enddt and hcfapos!='81' and suppdata='N' 
	and Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Rehabilitation')

-- Organ Transplant

drop table if exists #pcr_organtransplanttlist;

CREATE table #pcr_organtransplanttlist
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_organtransplanttlist
select 
	Memid
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_dischstartdt and @ce_enddt and hcfapos!='81' and suppdata='N' and 
	(
		CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Organ Transplant Other Than Kidney'))
		or
		HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Organ Transplant Other Than Kidney'))
		or
		Proc_i_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells'))
		or
		Proc_i_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells'))
		or
		Proc_i_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells'))
		or
		Proc_i_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells'))
		or
		Proc_i_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells'))
		or
		Proc_i_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells'))
	)



--Potentially Planned Procedure

drop table if exists #pcr_plannedproctlist;

CREATE table #pcr_plannedproctlist
(
	Memid varchar(100),
	claimid varchar(100)
	
);

Insert into #pcr_plannedproctlist
select Memid
	,ClaimID 
	from hds.HEDIS_VISIT 
	where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_dischstartdt and @ce_enddt and hcfapos!='81' and suppdata='N' 
	and diag_i_1 not in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Condition') 
	and (
		Proc_i_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures')
		or
		Proc_i_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures')
		or
		Proc_i_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures')
		or
		Proc_i_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures')
		or
		Proc_i_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures')
		or
		Proc_i_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures')
	)




-- Identify Plan and age at dischargedate and exlucde xclusions

drop table if exists #pcr_mergedstaywpayer;
create table #pcr_mergedstaywpayer
(
	Memid varchar(100),
	AdmissionDate varchar(10),
	DischargeDate varchar(10),
	StayType varchar(20),
	claimid varchar(100),
	tclaimid varchar(100),
	claimstatus INT,
	dob varchar(10),
	Gender varchar(10),
	Payer varchar(10),
	Age INT,
	StayNumber INT

)


Insert into #pcr_mergedstaywpayer
select * from(
	select 
		t1.*
		,CASE 
			WHEN convert(varchar,cast(DATEADD(year,DATEDIFF(year, dob  ,dischargedate) , dob) as date),112)> dischargedate THEN DATEDIFF(year, dob  ,dischargedate) -1
			ELSE DATEDIFF(year, dob  ,dischargedate)
			END as age 
		,ROW_Number() over(partition by Memid order by Memid,AdmissionDate,DischargeDate) as staynumber
			

		from(

			select 
				s.*
				,gm.dob
				,gm.Gender
				,en.payer 
				from #pcr_mergedstays s
				join hds.HEDIS_MEMBER_GM gm on s.Memid=gm.MemID and gm.Measure_id=@meas
				join hds.HEDIS_MEMBER_EN en on s.Memid=en.MemID and en.Measure_id=@meas and dateadd(day,30,s.DischargeDate) between en.StartDate and en.FinishDate
				left outer join #pcr_deadmembers dmexcl on s.Memid=dmexcl.Memid and dmexcl.claimid in(s.claimid,s.tclaimid)
				left outer join #pcr_perinatalvstlist prnexcl on s.Memid=prnexcl.Memid and prnexcl.claimid in(s.claimid,s.tclaimid)
				left outer join #pcr_pregvstlist prgexcl on s.Memid=prgexcl.Memid and prgexcl.claimid in(s.claimid,s.tclaimid)
				left outer join #pcr_hospicemembers hspexcl on s.Memid=hspexcl.Memid
				where dmexcl.Memid is null 
					  and prnexcl.Memid is null 
					  and hspexcl.Memid is null 
					  and prgexcl.Memid is null 
					  and s.AdmissionDate!=DischargeDate 
					  and DischargeDate between @ce_startdt and @ce_dischenddt
					 -- and s.Memid=95338                     
		)t1 
	)t2
		where (payer in('MMP','SN3','SN2','SN1','MCR','MCS','MP') and age>=18) 
				or (payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO') and age between 18 and 64)
				or (payer in('MD','MLI','MRB') and age between 18 and 64)


CREATE CLUSTERED INDEX idx_pcr_mergedstaywpayer ON #pcr_mergedstaywpayer ([memid],[Dischargedate],[payer]);


-- Check for Continuous Enrollment


drop table if exists #pcr_contenroll;
create table #pcr_contenroll
(
	Memid varchar(100),
	staynumber INT
);


--Check for continuous enrollment
				
--CTE1 to check covergae during last 365 days
With coverage_CTE1(Memid,dischargedate,staynumber,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select 
	s.Memid
	,dischargedate
	,staynumber
	,isnull(lag(Finishdate,1) over(partition by s.Memid,staynumber order by Startdate,FinishDate desc),convert(varchar,cast(dateadd(day,-365,dischargedate) as date),112)) as lastcoveragedate
	,Case 
		when StartDate<convert(varchar,cast(dateadd(day,-365,dischargedate) as date),112) then convert(varchar,cast(dateadd(day,-365,dischargedate) as date),112) 
		else startdate 
	end as Startdate
	,case 
		when Finishdate>dischargedate then dischargedate 
		else finishdate 
	end as Finishdate
	,isnull(lead(Startdate,1) over(partition by s.Memid,staynumber order by Startdate,FinishDate),dischargedate) as nextcoveragedate
	from #pcr_mergedstaywpayer s
	join hds.HEDIS_MEMBER_EN en on s.memid=en.MemID and en.Measure_id=@meas
	where  Startdate<=dischargedate and Finishdate>=convert(varchar,cast(dateadd(day,-365,dischargedate) as date),112) 
						        
),
-- CTE2 to check coverage in next 30 days
coverage_CTE2(Memid,dischargedate,staynumber,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
		select 
		s.Memid
		,dischargedate
		,staynumber
		,isnull(lag(Finishdate,1) over(partition by s.Memid,staynumber order by Startdate,FinishDate desc),dischargedate) as lastcoveragedate
		,Case 
			when StartDate<dischargedate then dischargedate 
			else startdate 
			end as Startdate
		,case 
			when Finishdate>convert(varchar,cast(dateadd(day,30,dischargedate) as date),112) then convert(varchar,cast(dateadd(day,30,dischargedate) as date),112)
			else finishdate 
			end as Finishdate
		,isnull(lead(Startdate,1) over(partition by s.Memid,staynumber order by Startdate,FinishDate),convert(varchar,cast(dateadd(day,30,dischargedate) as date),112)) as nextcoveragedate
		from #pcr_mergedstaywpayer s
		join hds.HEDIS_MEMBER_EN en on s.memid=en.MemID and en.Measure_id=@meas
		where  Startdate<=convert(varchar,cast(dateadd(day,30,dischargedate) as date),112) and Finishdate>=dischargedate 
		--and s.memid=95044
				     
)
Insert into #pcr_contenroll
select t3.Memid,t3.staynumber from(
	select t2.Memid,t2.staynumber,sum(anchor) as anchor from(
	
		select 
			*
			, case 
				when rn=1 and startdate>convert(varchar,cast(dateadd(day,-365,dischargedate) as date),112) then 1 
				else 0 
				end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
				end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when dischargedate between Startdate and newfinishdate then 1 
				else 0 
				end as anchor 
				from(
					Select 
						*
						,case 
							when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid,staynumber order by Startdate,FinishDate),dischargedate) 
							else finishdate 
							end as newfinishdate
						,ROW_NUMBER() over(partition by memid,staynumber order by Startdate,Finishdate) as rn 
						from coverage_CTE1
				)t1           
		)t2  
			group by memid,staynumber having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=320 
	)t3
Join(

	select * from
			(
				select Memid,staynumber,sum(anchor) as anchor from
				(
					select 
						*
						, case 
							when rn=1 and startdate>dischargedate then 1 
							else 0 
							end as startgap
						,case 
							when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
							else 0 
							end as gaps
						,datediff(day,Startdate,Finishdate)+1 as coveragedays
						,case 
							when dischargedate between Startdate and newfinishdate then 1 
							else 0 
							end as anchor 
						from(
							Select 
								*
								,case 
									when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid,staynumber order by Startdate,FinishDate),convert(varchar,cast(dateadd(day,30,dischargedate) as date),112)) 
									else finishdate 
									end as newfinishdate
								,ROW_NUMBER() over(partition by memid,staynumber order by Startdate,Finishdate) as rn 
								from coverage_CTE2
						)z1           
					)z2 
				group by memid,staynumber having(sum(gaps)+sum(startgap)<1) and sum(coveragedays)>=30 
		)z3
 
)z4 on z4.memid=t3.memid and z4.staynumber=t3.staynumber and (z4.anchor+t3.anchor)>0


-- Creating the list of members who need to be reported

drop table if exists #pcr_baselist;

create table #pcr_baselist
(
	Memid varchar(100),
	Age INT,
	Gender varchar(4),
	Payer varchar(10),
	AdmissionDate varchar(10),
	DischargeDate varchar(10),
	claimid varchar(10),
	tclaimid varchar(10),
	StayType varchar(20),
	rownumber INT,
	StayNumber INT
)


Insert into #pcr_baselist
select 
	s.Memid
	,s.age
	,s.gender
	,s.payer
	,S.AdmissionDate
	,S.DischargeDate
	,s.claimid
	,s.tclaimid
	,s.staytype
	--,rank() over(partition by s.Memid,s.AdmissionDate,s.DischargeDate order by s.memid,s.AdmissionDate,s.DischargeDate) as staynumber
	,row_number() over(order by S.Memid,s.Staynumber) as rn,DENSE_RANK() over(partition by s.Memid order by s.Memid,s.AdmissionDate,s.DischargeDate) as staynumber 
from #pcr_mergedstaywpayer s
join #pcr_contenroll ce on ce.Memid=s.Memid and ce.staynumber=s.staynumber
order by Memid,staynumber

CREATE CLUSTERED INDEX idx_pcr_baselist ON #pcr_baselist ([memid],[payer],[Dischargedate],[rownumber]);



-- get discharge claims with stay number

drop table if exists #pcr_dischargeclaims;
Create table #pcr_dischargeclaims
(
	Memid varchar(100),
	StayNumber INT,
	claimid varchar(100),
	tclaimid varchar(100),
	claimid1 varchar(100),
	claimid2 varchar(100)
	
)
Insert into #pcr_dischargeclaims
select 
	ms.Memid
	,b.staynumber
	,b.claimid
	,b.tclaimid
	,case 
		when dt.nxtclaimid is null then ms.claimid 
		else dt.nxtclaimid 
	end as claimid1
	,case 
		when dt.prvclaimid is null then ms.claimid 
		else dt.prvclaimid 
	end as claimid2
	from #pcr_mergedstays ms
	join #pcr_baselist b on ms.memid=b.memid and ms.claimid=b.claimid and ms.tclaimid=b.tclaimid
	left outer join #pcr_directtransfers dt on ms.Memid=dt.Memid and ms.claimid in(dt.nxtclaimid,dt.prvclaimid)
	--where ms.Memid=95167



-- Identifying the PPayer Plan

drop table if exists #pcr_ppayerlist;
Create table #pcr_ppayerlist
(
	Memid varchar(100),
	Ppayer varchar(10)
)

Insert into #pcr_ppayerlist
select distinct ds.Memid
		,case 
			when en1.Payer is null and en2.payer is null then en3.payer 
			when en1.payer is null and en2.payer is not null then en2.payer
			else en1.payer
		end as payermapping
		from #pcr_baselist ds
		-- check the coverage on 20190101
		left outer join hds.HEDIS_MEMBER_EN en1 on ds.Memid=en1.memid and en1.Measure_id=@meas and @ce_startdt1 between en1.startdate and en1.finishdate
		-- if not covered on 20190101 then check 20200101
		left outer join hds.HEDIS_MEMBER_EN en2 on ds.Memid=en2.memid and en2.Measure_id=@meas and @ce_startdt between en2.startdate and en2.finishdate
		-- if not enrolled in both check for first enrolled plan in measurement year
		left outer join
		(
			select Memid,payer from hds.hedis_member_en t1 where Measure_id=@meas and startdate=(select min(t2.startdate) from hds.HEDIS_MEMBER_EN t2 where Measure_id=@meas and t1.memid=t2.memid and t2.startdate>@ce_startdt)
		)en3 on ds.Memid=en3.memid



--Combining the list of Payer and PPayer

drop table if exists #pcr_t1_payernppayer;
create table #pcr_t1_payernppayer
(
	Memid varchar(100),
	Payer varchar(10),
	PPayer varchar(10),
	AdmissionDate varchar(10),
	DischargeDate varchar(10),
	claimid varchar(100),
	tclaimid varchar(100),
	StayType varchar(20),
	StayNumber INT,
	Age INT,
	Gender varchar(4),
	rownumber INT,
	
)

Insert into #pcr_t1_payernppayer
select 
	b.Memid
	,b.Payer
	,p.Ppayer
	,b.AdmissionDate
	,b.DischargeDate
	,b.claimid
	,b.tclaimid
	,b.StayType
	,b.StayNumber
	,b.Age
	,b.Gender
	,ROW_NUMBER() over(order by b.Memid,Staynumber,Payer,p.PPayer) as rownumber 
	from #pcr_baselist b
	join #pcr_ppayerlist p on b.Memid=p.Memid 


-- Applying the Dual Enrollment logic


drop table if exists #pcr_t2_payernppayer;
create table #pcr_t2_payernppayer
(
	Memid varchar(100),
	Age INT,
	Gender varchar(10),
	StayNumber INT,
	Payer varchar(10),
	PPayer varchar(10),
	rownumber INT
);


With mappedpayers(Memid,Payer,Ppayer,StayNumber,Age,Gender) as
(
	
	select distinct Memid,Payer,Ppayer,StayNumber,Age,gender from #pcr_t1_payernppayer --where memid=108893          
	
	
)
Insert into #pcr_t2_payernppayer(Memid,Age,Gender,StayNumber,Payer,Ppayer,rownumber)
select distinct t4.* from(
select 	distinct Memid,age,gender
	,StayNumber
	,newPayer as Payer
	,newPPayer as PPayer
	,row_number() over(order by Memid,StayNumber) as rownumber 
from(
	select t2.*,
	-- Applying Dual enrollment for Payer field
		case 
			when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB')) and staynumber=nxtstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB')) and staynumber=prvstaynumber then Payer
			When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO')) and staynumber=prvstaynumber then prvpayer
			when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO')) and staynumber=nxtstaynumber then Payer
			When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO')) and staynumber=prvstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) and staynumber=prvstaynumber then prvpayer
			else Payer 
		end as newpayer
		-- Applying Dual Enrollment for PPayer
		,case 
			when (Ppayer in('HMO','CEP','POS','PPO') and nxtPpayer in('MD','MLI','MRB')) and staynumber=nxtstaynumber then Ppayer
			When (Ppayer in('HMO','CEP','POS','PPO') and prvPpayer in('MD','MLI','MRB')) and staynumber=prvstaynumber then Ppayer
			When (Ppayer in('MD','MLI','MRB') and nxtPpayer in('HMO','CEP','POS','PPO')) and staynumber=nxtstaynumber then nxtPpayer
			When (Ppayer in('MD','MLI','MRB') and prvPpayer in('HMO','CEP','POS','PPO')) and staynumber=prvstaynumber then prvPpayer
			when (Ppayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtPpayer in('HMO','CEP','POS','PPO')) and staynumber=nxtstaynumber then Ppayer
			When (Ppayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvPpayer in('HMO','CEP','POS','PPO')) and staynumber=prvstaynumber then Ppayer
			When (Ppayer in('HMO','CEP','POS','PPO') and nxtPpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) and staynumber=nxtstaynumber then nxtPpayer
			When (Ppayer in('HMO','CEP','POS','PPO') and prvPpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) and staynumber=prvstaynumber then prvPpayer
			else PPayer 
		end as newPpayer  
		from(
			select 
				m1.*
				,isnull(lead(Payer,1) over(partition by Memid order by Memid,staynumber),Payer) as nxtpayer
				,isnull(lag(Payer,1) over(partition by Memid order by Memid,staynumber),Payer) as prvpayer
				,isnull(lead(Staynumber,1) over(partition by Memid order by Memid,staynumber),Staynumber) as nxtStaynumber
				,isnull(lag(Staynumber,1) over(partition by Memid order by Memid,staynumber),Staynumber) as prvStaynumber
				,isnull(lead(Ppayer,1) over(partition by Memid order by Memid,Ppayer),Ppayer) as nxtPpayer
				,isnull(lag(Ppayer,1) over(partition by Memid order by Memid,Ppayer),Ppayer) as prvPpayer
			from mappedpayers m1
		)t2
	)t3 
)t4 

-- Logic to handle SN1,SN2,SN3,MMP and MCD


Drop table if exists #pcr_payernppayer;
Create table #pcr_payernppayer
(
	Memid varchar(100),
	Age INT,
	Gender varchar(10),
	Meas varchar(10),
	Payer varchar(10),
	Ppayer varchar(10)
)


Insert into #pcr_payernppayer
select distinct 
	Memid
	,Age
	,Gender
	,concat('PCR',char(64+staynumber)) as Meas
	,Payer
	,PPayer 
from(
-- First Set is to join on Memid,staynumber and Payer to avoid duplication in case of SN1,SN2,SN3 and MMP
	select distinct 
		f1.Memid
		,f1.Gender
		,f1.Age
		,f1.StayNumber
		,f1.Payer
		,f2.PPayer  
	from(
		select distinct 
			t2.rownumber
			,t2.Memid
			,t2.Age
			,t2.Gender
			,t2.StayNumber
			,pm1.PayerMapping as Payer 
		from #pcr_t2_payernppayer t2
		join hds.HEDIS_PAYER_MAPPING pm1 on t2.Payer=pm1.Payer
	)f1
	left outer join(
		select distinct 
			t2.rownumber
			,t2.Memid
			,t2.StayNumber
			,pm2.PayerMapping as PPayer
		from #pcr_t2_payernppayer t2
		join hds.HEDIS_PAYER_MAPPING pm2 on t2.Ppayer=pm2.Payer
	)f2 on f1.Memid=f2.Memid and f1.StayNumber=f2.StayNumber and f1.rownumber=f2.rownumber and f1.Payer=f2.PPayer
	where PPayer is not null

Union all
--logic to handle records which were left from set viz..where payer and PPayer is not the same
select distinct 
	f3.Memid
	,f3.Gender
	,f3.Age
	,f3.StayNumber
	,f3.Payer
	,f4.PPayer 
from(
	select distinct 
		f1.Memid
		,f1.Gender
		,f1.Age
		,f1.StayNumber
		,f1.Payer
		,f2.PPayer
	from(
		select distinct 
			t2.rownumber
			,t2.Memid
			,t2.Gender
			,t2.Age
			,t2.StayNumber
			,pm1.PayerMapping as Payer 
		from #pcr_t2_payernppayer t2
		join hds.HEDIS_PAYER_MAPPING pm1 on t2.Payer=pm1.Payer

	)f1
	left outer join(
		select distinct t2.rownumber,t2.Memid,t2.StayNumber,pm2.PayerMapping as PPayer from #pcr_t2_payernppayer t2
		join hds.HEDIS_PAYER_MAPPING pm2 on t2.Ppayer=pm2.Payer
	)f2 on f1.Memid=f2.Memid and f1.StayNumber=f2.StayNumber and f1.rownumber=f2.rownumber and f1.Payer=f2.PPayer
	where PPayer is null
)f3
join
(
	select distinct t2.rownumber,t2.Memid,t2.StayNumber,pm2.PayerMapping as PPayer from #pcr_t2_payernppayer t2
	join hds.HEDIS_PAYER_MAPPING pm2 on t2.Ppayer=pm2.Payer

)f4 on f3.Memid=f4.Memid and f3.StayNumber=f4.StayNumber
)final


-- Calculating Outliers
drop table if exists #pcr_outliers;
create table #pcr_outliers
(
	[Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  outlier INT,
  epop INT
  
);

Insert into #pcr_outliers
select 
	t1.Memid
	,t1.Meas
	,t1.Payer
	,t1.Ppayer
	,case 
		when payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MCD') and staycount>=4 then 1 
		when payer in('CEP','HMO','POS','PPO','MOS','MEP','MPO','MMO') and staycount>=3 then 1
		else 0 
	end as outlier
	,case 
		when payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MCD') and staycount>=4 then 0 
		when payer in('CEP','HMO','POS','PPO','MOS','MEP','MPO','MMO') and staycount>=3 then 0
		else 1 
	end as epop
	from #pcr_payernppayer t1
	join(
		select Memid,count(distinct Meas) as Staycount from #pcr_payernppayer group by Memid
	)t2 on t1.Memid=t2.Memid


-- generating the ouput in required format

drop table if exists #pcrdataset;
CREATE TABLE #pcrdataset (
  [Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  [Epop] bit DEFAULT '0',
  [Ppop] bit DEFAULT '1',
  [Num] bit DEFAULT '0',
  [Outlier] bit DEFAULT '0',
  [SNF] bit DEFAULT '0',
  [ObsWt] float DEFAULT '0',
  [SurgWt] float DEFAULT '0',
  [DCC1] INT DEFAULT '0',
  [DCCWt1] float DEFAULT '0',
  [DCC2] INT DEFAULT '0',
  [DCCWt2] float DEFAULT '0',
  [DCC3] INT DEFAULT '0',
  [DCCWt3] float DEFAULT '0',
  [ComorbidWt] float DEFAULT '0',
  [AgeGenWt] float DEFAULT '0',
  [ObsWtSNF] float DEFAULT '0',
  [SurgWtSNF] float DEFAULT '0',
  [DCC1SNF] INT DEFAULT '0',
  [DCCWt1SNF] float DEFAULT '0',
  [DCC2SNF] INT DEFAULT '0',
  [DCCWt2SNF] float DEFAULT '0',
  [DCC3SNF] INT DEFAULT '0',
  [DCCWt3SNF] float DEFAULT '0',
  [ComorbidWtSNF] float DEFAULT '0',
  [AgeGenWtSNF] float DEFAULT '0',
  [Age] Int DEFAULT NULL,
  [Gender] varchar(45) NULL,
  LIS INT DEFAULT 0,
  OREC INT DEFAULT NULL
    
) ;

Insert into #pcrdataset(Memid,Age,Gender,Meas,Payer,PPayer)
select * from #pcr_payernppayer 


update ds set ds.Outlier=o.outlier,ds.epop=o.epop from #pcrdataset ds join #pcr_outliers o on ds.memid=o.memid and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer


-- combining with staynumber


drop table if exists #pcr_base1;
create table #pcr_base1
(
	Memid varchar(50),
	Age INT,
	Gender varchar(10),
	Meas varchar(10),
	Payer varchar(10),
	PPayer varchar(10),
	AdmissionDate varchar(10),
	DischargeDate varchar(10),
	claimid varchar(100),
	tclaimid varchar(100),
	Staytype varchar(20),
	Staynumber INT
)

Insert into #pcr_base1
select distinct p.*,b.AdmissionDate,b.DischargeDate,b.claimid,b.tclaimid,b.StayType,b.Staynumber from #pcr_payernppayer p
join 
(
	select distinct Memid,Staynumber,Age,Gender,AdmissionDate,DischargeDate,claimid,tclaimid,staytype from #pcr_baselist
)b on p.memid=b.memid and p.meas=concat('PCR',char(64+b.staynumber))
order by 1,2,3


-- Calculating Observation Stay weight

drop table if exists #pcr_obswtlist
create table #pcr_obswtlist
(
	[Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  obswt float
  
);


Insert into #pcr_obswtlist
select distinct Memid,concat('PCR',char(64+staynumber)) as Meas,Payer,ppayer,isNull(weight,0) as obswt from(
select b.*,hra.Weight from #pcr_base1 b
join hds.HEDIS_RISK_ADJUSTMENT hra on substring(b.staytype,1,3)=hra.VariableName and hra.Measure_ID='PCR' and b.Payer in('MCD') and hra.ProductLine='Medicaid'
Union all
select b.*,hra.Weight from #pcr_base1 b
join hds.HEDIS_RISK_ADJUSTMENT hra on substring(b.staytype,1,3)=hra.VariableName and hra.Measure_ID='PCR' and b.Payer in('HMO','CEP','POS','PPO','MEP','MMO','MOS','MPO') and hra.ProductLine='Commercial'
)t1 


update ds set ds.ObsWt=o.obswt from #pcrdataset ds join #pcr_obswtlist o on ds.memid=o.memid and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0


-- Surgery Weight

drop table if exists #pcr_surgwtlist;
create table #pcr_surgwtlist
(
  [Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  surgwt float
  
);

Insert into #pcr_surgwtlist
select distinct t1.Memid,t1.Meas,t1.Payer,t1.Ppayer,r.Weight from(
select b.Memid,b.Meas,b.Payer,b.PPayer,b.age,'Surg' as staytype,
case 
	when  b.payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO') then 'Commercial'
	When b.payer='MCD' then 'Medicaid'
	When b.payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP') then 'Medicare'
end as productline,
case
	when b.age between 18 and 64 then 'Standard - 18-64'
	when b.age>=65 then 'Standard - 65+'
end as reportingindicator
from #pcr_base1 b 
join hds.HEDIS_VISIT v on b.Memid=v.MemID and v.Measure_id=@meas and v.Date_S between b.AdmissionDate and b.Dischargedate
where v.CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Surgery Procedure') and v.HCFAPOS!='81' and v.SuppData='N'
--and b.Memid=101028
)t1
join hds.HEDIS_RISK_ADJUSTMENT r on t1.staytype=r.VariableName and r.Measure_ID='PCR' and t1.productline=r.ProductLine and t1.reportingindicator=r.ReportingIndicator



update ds set ds.SurgWt=o.surgwt from #pcrdataset ds join #pcr_surgwtlist o on ds.memid=o.memid and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0;


-- Discharge Condition

drop table if exists #pcr_dischcc;
create table #pcr_dischcc
(
  [Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  DCC INT,
  dccct INT,
  weight float
  
)

insert into #pcr_dischcc
select f2.Memid,f2.Meas,f2.Payer,f2.PPayer,DCC,row_number() over(partition by Memid,meas,payer order by Memid,meas,payer,DCC) as dccct,weight from(
select distinct f1.*,isnull(r.Weight,0) as weight,Cast(replace(Comorbid_CC,'CC-','') as INT) as DCC from(

	select t2.Memid,t2.Meas,t2.Payer,t2.PPayer,t2.Age,t2.Gender,cc.Comorbid_CC,
	case 
		when t2.payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO') then 'Commercial'
		When t2.payer='MCD' then 'Medicaid'
		When t2.payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP') then 'Medicare'
	end as productline,
	case
		when t2.age between 18 and 64 then 'Standard - 18-64'
		when t2.age>=65 then 'Standard - 65+'
	end as reportingindicator
	from(
	select t1.*,replace(v.Diag_I_1,'.','') as diagnosis from(
	select *,case when claimid>=tclaimid then claimid else tclaimid end as tempclaim from #pcr_base1
	)t1
	join hds.HEDIS_VISIT v on v.MemID=t1.Memid and v.ClaimID=t1.tempclaim and v.Measure_id=@meas
	)t2
	join hds.HEDIS_TABLE_CC cc on t2.diagnosis=cc.DiagnosisCode and t2.payer ='MCD' and cc.CC_type='Medicaid'

	Union all

	select t2.Memid,t2.Meas,t2.Payer,t2.PPayer,t2.Age,t2.Gender,cc.Comorbid_CC,
	case 
		when t2.payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO') then 'Commercial'
		When t2.payer='MCD' then 'Medicaid'
		When t2.payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP') then 'Medicare'
	end as productline,
	case
		when t2.age between 18 and 64 then 'Standard - 18-64'
		when t2.age>=65 then 'Standard - 65+'
	end as reportingindicator
	
	from(
	select t1.*,replace(v.Diag_I_1,'.','') as diagnosis from(
	select *,case when claimid>=tclaimid then claimid else tclaimid end as tempclaim from #pcr_base1
	)t1
	join hds.HEDIS_VISIT v on v.MemID=t1.Memid and v.ClaimID=t1.tempclaim and v.Measure_id=@meas
	)t2
	join hds.HEDIS_TABLE_CC cc on t2.diagnosis=cc.DiagnosisCode and t2.payer !='MCD' and cc.CC_type='Shared'
)f1
left outer join hds.HEDIS_RISK_ADJUSTMENT r on f1.Comorbid_CC=r.VariableName and f1.productline=r.ProductLine and f1.reportingindicator=r.ReportingIndicator and r.Measure_ID='PCR' and r.VariableType='DCC' and r.Model='Logistic'

)f2




update ds set ds.DCCWt1=o.weight,ds.DCC1=o.DCC from #pcrdataset ds join #pcr_dischcc o on ds.memid=o.memid and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=1
update ds set ds.DCCWt2=o.weight,ds.DCC2=o.DCC from #pcrdataset ds join #pcr_dischcc o on ds.memid=o.memid and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=2
update ds set ds.DCCWt3=o.weight,ds.DCC3=o.DCC from #pcrdataset ds join #pcr_dischcc o on ds.memid=o.memid and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=3


-- Comorbidities


drop table if exists #pcr_diagnosis;
Create table #pcr_diagnosis
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	Claimid varchar(100),
	claimstatus INT,
	diagnosis varchar(50)
	
)


Insert into #pcr_diagnosis
select Memid,Date_S,Date_Adm,Date_Disch,claimid,claimstatus,replace(value,'.','') as diagnosis
from hds.HEDIS_VISIT v
unpivot
(
  value
  for col in (Diag_i_1, Diag_i_2, Diag_i_3,Diag_i_4,Diag_i_5,Diag_i_6,Diag_i_7,Diag_i_8,Diag_i_9,Diag_i_10)
) un
where Measure_id=@meas and value!='' and Date_S!='' and Date_S between @ce_startdt1 and @ce_dischenddt and suppdata='N' and HCFAPOS!='81' and (
	CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Observation','ED','Nonacute Inpatient','Acute Inpatient'))
	or 
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Outpatient') 
	or
	REV in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')) 
) --  and memid=99311
Union all
select Memid,Date_Disch as servicedate,Date_Adm,Date_Disch,claimid,claimstatus,replace(value,'.','') as diagnosis
from hds.HEDIS_VISIT
unpivot
(
  value
  for col in (Diag_i_1, Diag_i_2, Diag_i_3,Diag_i_4,Diag_i_5,Diag_i_6,Diag_i_7,Diag_i_8,Diag_i_9,Diag_i_10)
) un
where Measure_id=@meas and value!=''  and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_dischenddt and suppdata='N' and HCFAPOS!='81' and (
	CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient'))
	or
	REV in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')) 
) 


CREATE CLUSTERED INDEX idx_pcr_diagnosis ON #pcr_diagnosis ([memid],[Date_S],[claimid],diagnosis);



drop table if exists #pcr_dischdiagnosis;
create table #pcr_dischdiagnosis
(
	Memid varchar(100),
	Meas varchar(10),
	Payer varchar(10),
	PPayer varchar(10),
	staynumber int,
	dischargeclaimid INT,
	dischdiagnosis varchar(20)
	
	
);
Insert into #pcr_dischdiagnosis
select distinct t1.Memid,t1.Meas,t1.Payer,t1.PPayer,t1.Staynumber,t1.dischargeclaim,replace(Diag_I_1,'.','') as dischdiag from(
select *,case when claimid>=tclaimid then claimid else tclaimid end as dischargeclaim from #pcr_base1
)t1
Join hds.HEDIS_VISIT vd on vd.MemID=t1.Memid and vd.Measure_id=@meas and vd.ClaimID=t1.dischargeclaim



drop table if exists #pcr_diagnosiswexcl;
Create table #pcr_diagnosiswexcl
(
	Memid varchar(100),
	Meas varchar(10),
	Payer varchar(8),
	PPayer varchar(8),
	Claimid INT,
	diagnosis varchar(50),
	StayNumber INT,
	Age INT,
	Gender varchar(10)
	
)

Insert into #pcr_diagnosiswexcl
select distinct t2.* from(
select distinct t1.Memid,t1.Meas,t1.Payer,t1.PPayer,t1.Claimid,t1.diagnosis,t1.Staynumber,t1.Age,t1.Gender from(
--select * from(
select b.Memid,b.Meas,b.Payer,b.PPayer,d.Claimid,d.diagnosis,b.Staynumber,b.Age,b.Gender from
#pcr_base1 b
join #pcr_diagnosis d on b.Memid=d.Memid and d.Date_s!='' and d.Date_S between dateadd(day,-365,b.DischargeDate) and b.DischargeDate
--where d.memid=100107
)t1
left outer join #pcr_dischdiagnosis dd on t1.Memid=dd.Memid and t1.Claimid=dd.dischargeclaimid and t1.diagnosis=dd.dischdiagnosis and t1.staynumber=dd.staynumber
where dd.Memid is null
)t2


-- HCC RANK

drop table if exists #pcr_hccrank;

Create table #pcr_hccrank
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)

Insert into #pcr_hccrank
select t3.Memid,t3.Meas,t3.payer,t3.ppayer,
case 
	when age between 18 and 44 and gender='F' then 'F_18-44'
	when age between 45 and 54 and gender='F' then 'F_45-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 44 and gender='M' then 'M_18-44'
	when age between 45 and 54 and gender='M' then 'M_45-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup,
case 
	when age between 18 and 64 then 'Standard - 18-64'
	when age>=65 then 'Standard - 65+'
	end as reportingindicator,
case
	When payer in('CEP','HMO','POS','PPO','MEP','MPO','MOS','MMO') then 'Commercial'
	When payer='MCD' then 'Medicaid'
	else 'Medicare'
	end as productline,
	hcc
from(
select distinct Memid,Meas,payer,ppayer,HCC,cast(age as INT) as age,gender,calcrank from (

select Memid,Meas,payer,ppayer,cc.Comorbid_CC,hcc.RankingGroup,isnull(hcc.Rank,1) as Rank,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC,Rank() over(partition by Memid,meas,RankingGroup order by Memid,RankingGroup,Rank) as calcrank,age,gender  from
	(
	select *,case when payer in('MCD','MLI','MRB','MD') then 'Medicaid' else 'Shared' end as producttype from #pcr_diagnosiswexcl d
	)t1
left outer join hds.HEDIS_TABLE_CC cc on t1.diagnosis=cc.diagnosiscode and t1.producttype=cc.CC_type
left outer join hds.HEDIS_HCC_RANK hcc on cc.Comorbid_CC=hcc.CC and t1.producttype=hcc.hcc_type
where Comorbid_CC is not null
--and memid=100035 and meas='PCRA'
)t2 where calcrank=1
 --memid=100035 and meas='PCRA'
)t3






-- HCC RANK COMB


drop table if exists #pcr_hccrankcmb;
Create table #pcr_hccrankcmb
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #pcr_hccrankcmb
select distinct h.*,cmb.hcccomb from #pcr_hccrank h 
left outer join hds.HEDIS_HCC_COMB cmb on cmb.ComorbidHCC1 in(select HCC from #pcr_hccrank t1 where t1.memid=h.memid and t1.productline=h.productline and t1.reportingindicator=h.reportingindicator and t1.Meas=h.Meas and t1.payer=h.payer) and cmb.ComorbidHCC2 in(select HCC from #pcr_hccrank t1 where t1.memid=h.memid and t1.productline=h.productline and t1.reportingindicator=h.reportingindicator and t1.Meas=h.Meas and t1.payer=h.payer)
--where h.memid=141630


drop table if exists #pcr_agegenwt;
Create table #pcr_agegenwt
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	AgeGenWt float
	
)

Insert into #pcr_agegenwt
select distinct t1.Memid,t1.Meas,t1.payer,t1.Ppayer
,r1.Weight as AgeGenWt 
from(
select Memid,Meas,payer,ppayer,case 
	when age between 18 and 44 and gender='F' then 'F_18-44'
	when age between 45 and 54 and gender='F' then 'F_45-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 44 and gender='M' then 'M_18-44'
	when age between 45 and 54 and gender='M' then 'M_45-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup,
case 
	when payer in('MCR','MP','MCS','SN1','SN2','SN3','MMP') and age between 18 and 64 then 'Standard - 18-64'
	when payer in('MCR','MP','MCS','SN1','SN2','SN3','MMP') and age>=65 then 'Standard - 65+'
	else 'Standard - 18-64'
	end as reportingindicator,
case
	When payer in('CEP','HMO','POS','PPO','MEP','MPO','MOS','MMO') then 'Commercial'
	When payer='MCD' then 'Medicaid'
	else 'Medicare'
	end as productline
 from #pcrdataset where outlier=0 --and memid=100021          
 )t1
 join hds.HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=t1.genagegroup and t1.productline=r1.ProductLine and r1.variabletype='DEMO' and r1.Model='Logistic' and r1.Measure_id='PCR' and r1.reportingindicator=t1.reportingindicator





drop table if exists #pcr_comorbidwt;
Create table #pcr_comorbidwt
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	ComorbidWt float
	
)

Insert into #pcr_comorbidwt
select Memid,Meas,payer,Ppayer,sum(weight) as ComorbidWt from(
select distinct h.Memid,h.Meas,h.payer,h.Ppayer,h.genagegroup,h.reportingindicator,h.hcc as hcc,r1.Weight from #pcr_hccrankcmb h
join hds.HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcc and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='Logistic' and r1.ReportingIndicator=h.reportingindicator and r1.Measure_ID='PCR'

Union all
select distinct h.Memid,h.meas,h.payer,h.ppayer,h.genagegroup,h.reportingindicator,h.hcccmb as hcc,r1.Weight from #pcr_hccrankcmb h
join hds.HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcccmb and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='Logistic' and r1.ReportingIndicator=h.reportingindicator and r1.Measure_ID='PCR'

)t1 group by memid,Meas,payer,PPayer

update #pcrdataset set ComorbidWt=0
update ds set ds.AgeGenWt=p.AgeGenWt from #pcrdataset ds join #pcr_agegenwt p on p.Memid=ds.Memid and p.payer=ds.payer and p.Meas=ds.Meas and p.ppayer=ds.PPayer
update ds set ds.ComorbidWt=p.ComorbidWt from #pcrdataset ds join #pcr_comorbidwt p on p.Memid=ds.Memid and p.payer=ds.payer and p.Meas=substring(ds.Meas,1,4) and p.ppayer=ds.PPayer and ds.Outlier=0


-- Numerator Logic

	-- Identifying Inpatient and Observation Stays with admissiondate between 20200103 and 20201231

	-- Inpatient and Observation Stay

	drop table if exists #pcr_num_ipstaylist;
	CREATE table #pcr_num_ipstaylist
	(
		Memid varchar(100),
		Date_S varchar(10),
		Date_Adm varchar(10),
		Date_Disch varchar(10),
		claimid varchar(100),
		claimstatus INT,
		staytype varchar(20)
				
	);

	Insert into #pcr_num_ipstaylist
	-- Inpatient Stay list
	select Memid,Date_S,Date_Adm,Date_Disch,ClaimID,ClaimStatus,'Acute' as staytype from hds.HEDIS_VISIT where Measure_id=@meas and Date_Adm!='' and Date_Adm between @ce_dischstartdt and @ce_enddt and SuppData='N' and HCFAPOS!='81' and	REV in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')) and claimstatus=1
	-- and memid=100598
	Union all
	-- Observation Stay List
	select Memid,Date_S,Date_Adm,Date_Disch,ClaimID,ClaimStatus,'Observation' as staytype from hds.HEDIS_VISIT where Measure_id=@meas and Date_Adm!='' and Date_Adm between @ce_dischstartdt and @ce_enddt and SuppData='N' and HCFAPOS!='81' and	REV in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Observation Stay')) and claimstatus=1


	CREATE CLUSTERED INDEX idx_pcr_num_ipstaylist ON #pcr_num_ipstaylist ([memid],[Date_S],[Date_Disch],[claimid]);


	-- Identify Nonacute Inpatient Stay


	drop table if exists #pcr_num_naipstaylist;
	CREATE table #pcr_num_naipstaylist
	(
		Memid varchar(100),
		claimid varchar(100),
				
	);

	-- Identify non acute Inpatient Stays
	Insert into #pcr_num_naipstaylist
	select Memid,ClaimID from hds.HEDIS_VISIT where Measure_id=@meas and Date_Adm!='' and Date_Adm between @ce_dischstartdt and @ce_enddt and SuppData='N' and HCFAPOS!='81' and (REV in(select code_new from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Nonacute Inpatient Stay') or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code_new from hds.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay'))

	CREATE CLUSTERED INDEX idx_pcr_num_naipstaylist ON #pcr_num_naipstaylist ([memid],[claimid]);

	-- Identify only Acute Inatient and Observation Stays


	drop table if exists #pcr_num_ai_obs_staylist;
	CREATE table #pcr_num_ai_obs_staylist
	(
		Memid varchar(100),
		Date_S varchar(10),
		Date_Adm varchar(10),
		Date_Disch varchar(10),
		claimid varchar(100),
		claimstatus INT,
		staytype varchar(20)
				
	);
	-- removing non acute inpatient stays from total Inpatient stays
	Insert into #pcr_num_ai_obs_staylist
	select t1.Memid,t1.Date_S,t1.Date_Adm,t1.Date_Disch,t1.claimid,t1.claimstatus,staytype from #pcr_num_ipstaylist t1
	left outer join #pcr_num_naipstaylist t2 on t1.Memid=t2.memid and t1.claimid=t2.claimid
	Where t2.memid is null

	CREATE CLUSTERED INDEX idx_pcr_num_ai_obs_staylist ON #pcr_num_ai_obs_staylist ([memid],[Date_S],[Date_Disch],[claimid]);



	-- Identifying transfers



	drop table if exists #pcr_num_mergedstays;
	CREATE table #pcr_num_mergedstays
	(
		Memid varchar(100),
		AdmissionDate varchar(10),
		DischargeDate varchar(10),
		StayType varchar(20),
		claimid varchar(100),
		tclaimid varchar(100),
		claimstatus INT
	
				
	);

	Insert into #pcr_num_mergedstays
	select 
		t3.Memid,t3.newadmdate as AdmissionDate,t3.newdischdate as DischargeDate,t3.newstaytype as StayType,t3.claimid,t3.tclaimid,t3.claimstatus
	
		from(
			select 
				distinct  t2.Memid,t2.Date_S,t2.claimid,t2.claimstatus
				-- logic to adjust dates,stay type,claim id if it is a direct transfer
				,case 
					when prevstaydiff between 0 and 1 then prevadmdate 
					else date_adm 
					end as newadmdate
				,case 
					when nxtstaydiff between 0 and 1 then nxtdischdate 
					else date_disch 
					end as newdischdate
				,case 
					when nxtstaydiff between 0 and 1 then nxtstaytype 
					else staytype 
					end as newstaytype 
				,case 
					when nxtstaydiff between 0 and 1 then nxtclaimid
					when prevstaydiff between 0 and 1 then prevclaimid
					else claimid 
					end as tclaimid


				from(
	
					select 
						t1.*
						-- checking for difference between stays
						,DATEDIFF(day,date_disch,nxtadmdate) as nxtstaydiff
						,datediff(day,prevdischdate,Date_Adm) as prevstaydiff 
						from(
							select 
								s.*
								,isnull(lead(s.Date_Adm,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Adm) as nxtadmdate
								,isnull(lead(s.Date_Disch,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Disch) as nxtdischdate
								,isnull(lead(s.staytype,1) over(partition by Memid order by Date_Adm,Date_Disch),staytype) as nxtstaytype
								,isnull(lead(s.claimid,1) over(partition by Memid order by Date_Adm,Date_Disch),claimid) as nxtclaimid
								,isnull(lag(s.Date_Adm,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Adm) as prevadmdate
								,isnull(lag(s.Date_Disch,1) over(partition by Memid order by Date_Adm,Date_Disch),Date_Disch) as prevdischdate
								,isnull(lag(s.claimid,1) over(partition by Memid order by Date_Adm,Date_Disch),claimid) as prevclaimid
								from #pcr_num_ai_obs_staylist s
								--where s.memid=95338
						)t1
				)t2 
	)t3 order by 1,2,3



	drop table if exists #pcr_numstaylist;
	create table #pcr_numstaylist
	(
		Memid varchar(100),
		AdmissionDate varchar(10),
		DischargeDate varchar(10),
		StayType varchar(20),
		claimid varchar(100),
		tclaimid varchar(100),
		claimstatus INT
	)
	Insert into #pcr_numstaylist
	select b.* from #pcr_num_mergedstays b
	left outer join #pcr_chemovstlist c on b.memid=c.memid and c.claimid in(b.claimid,b.tclaimid)
	left outer join #pcr_hospicemembers h on b.memid=h.memid
	left outer join #pcr_rehabvstlist r on b.memid=r.memid and r.claimid in(b.claimid,b.tclaimid)
	left outer join #pcr_pregvstlist pg on b.memid=pg.memid and pg.claimid in(b.claimid,b.tclaimid)
	left outer join #pcr_organtransplanttlist o on b.memid=o.memid and o.claimid in(b.claimid,b.tclaimid)
	left outer join #pcr_plannedproctlist p on b.memid=p.memid and p.claimid in(b.claimid,b.tclaimid)
	left outer join #pcr_perinatalvstlist pn on b.memid=pn.memid and pn.claimid in(b.claimid,b.tclaimid)
	where AdmissionDate between @ce_dischstartdt and @ce_enddt and c.Memid is null and r.Memid is null and o.Memid is null and p.Memid is null and h.Memid is null and pg.Memid is null and pn.Memid is null


	drop table if exists #pcr_readmissionset;
	create table #pcr_readmissionset
	(
		Memid varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
		StayNumber INT,
		ReadmissionDate varchar(8)
	)

	Insert into #pcr_readmissionset
	select b.Memid,b.Meas,b.Payer,b.PPayer,b.Staynumber,ns.AdmissionDate from #pcr_base1 b
	join #pcr_numstaylist ns on b.Memid=ns.Memid and ns.AdmissionDate between b.DischargeDate and dateadd(day,30,b.DischargeDate)
	order by b.Memid,b.Meas



	
	drop table if exists #pcr_readmissionnumlist;
	create table #pcr_readmissionnumlist
	(
		Memid varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
	
	)

	Insert into #pcr_readmissionnumlist
	select distinct Memid,Meas,Payer,PPayer from #pcr_readmissionset t1 where StayNumber=(select max(StayNumber) from #pcr_readmissionset t2 where t1.Memid=t2.Memid and t1.Meas=t2.Meas and t1.Payer=t2.Payer and t1.PPayer=t2.PPayer and t1.ReadmissionDate=t2.ReadmissionDate)



	update #pcrdataset set num=0
	update ds set ds.num=1 from #pcrdataset ds join #pcr_readmissionnumlist n on n.Memid=ds.Memid and n.Meas=ds.Meas and n.Payer=ds.Payer and n.PPayer=ds.PPayer and ds.Outlier=0



--SES Startification


	drop table if exists #pcr_lislist;
	create table #pcr_lislist
	(
		Memid varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
	
	)

	Insert into #pcr_lislist
	select b.Memid,b.Meas,b.Payer,b.PPayer from #pcr_base1 b
	join hds.HEDIS_LISHIST l on l.beneficiary_id=b.Memid and l.Measure_id=@meas and ((l.Low_Income_Period_End_Date>=dateadd(day,-365,b.DischargeDate) and l.Low_Income_Period_Start_Date<=DATEADD(day,30,DischargeDate)) or Low_Income_Period_End_Date='')
	where b.Payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP')

	update ds set ds.LIS=1 from #pcrdataset ds join #pcr_lislist n on n.Memid=ds.Memid and n.Meas=ds.Meas and n.Payer=ds.Payer and n.PPayer=ds.PPayer and ds.Outlier=0

	
	-- Item 48

	drop table if exists #pcr_orec;
	CREATE table #pcr_orec
	(
		Memid varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
		orec varchar(2)
			
	);

	
	

	with CTE_orec(Memid,Meas,Payer,PPayer,DishargeDate,Run_date,OREC) as
	(
		select b.Memid,b.Meas,b.Payer,b.PPayer,b.DischargeDate,m.Run_Date,m.Original_Reason_for_Entitlement_Code_OREC as OREC from #pcr_base1 b
		join hds.HEDIS_MMDF m on m.Beneficiary_ID=b.Memid and m.Measure_id=@meas and m.Run_Date between dateadd(day,-365,b.DischargeDate) and dateadd(day,30,b.DischargeDate)
		where b.Payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP')
		--and memid=115645 
		--order by Run_Date desc

	)
	insert into #pcr_orec
	select distinct Memid,Meas,Payer,PPayer,OREC from CTE_orec t1 
	where t1.Run_date=(select max(Run_date) from CTE_orec t2 where t1.Memid=t2.Memid and t1.Meas=t2.Meas) 
	
		
	update ds set ds.OREC=n.orec from #pcrdataset ds join #pcr_orec n on n.Memid=ds.Memid and n.Meas=ds.Meas and n.Payer=ds.Payer and n.PPayer=ds.PPayer and ds.Outlier=0

	update #pcrdataset set meas=concat(meas,'NON') WHERE orec=0 AND lis=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

	update #pcrdataset set meas=concat(meas,'LISDE') WHERE orec=0 AND lis=1 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

	update #pcrdataset set meas=concat(meas,'DIS') WHERE orec in(1,3) AND lis=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

	update #pcrdataset set meas=concat(meas,'CMB') WHERE orec in(1,3) AND lis=1 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');
	
	update #pcrdataset set meas=concat(meas,'OT') WHERE orec in(2,9) AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

-- Identifying Skilled Nursing Discharge and Transfers


drop table if exists #pcr_snfdischargeortransfers;
create table #pcr_snfdischargeortransfers
(
	Memid varchar(100),
	Meas varchar(10),
	Payer varchar(10),
	PPayer varchar(10),
		
)
Insert into #pcr_snfdischargeortransfers
Select b.Memid,b.Meas,b.Payer,b.PPayer from #pcr_base1 b
join #pcr_SNFStays s on b.Memid=s.Memid and DATEDIFF(day,b.DischargeDate,s.Date_Adm) between 0 and 1
where payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP')


update ds set ds.SNF=1 from #pcrdataset ds join #pcr_snfdischargeortransfers n on n.Memid=ds.Memid and n.Meas=substring(ds.Meas,1,4) and n.Payer=ds.Payer and n.PPayer=ds.PPayer and ds.Outlier=0


-- SNF Calculations

-- Surgery Weight - SNF



drop table if exists #pcr_snf_surgwtlist;
create table #pcr_snf_surgwtlist
(
  [Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  surgwt float
  
);

Insert into #pcr_snf_surgwtlist
select distinct t1.Memid,t1.Meas,t1.Payer,t1.Ppayer,r.Weight from(
select b.Memid,b.Meas,b.Payer,b.PPayer,b.age,'Surg' as staytype
from #pcr_base1 b 
join #pcr_SNFStays s on b.Memid=s.Memid and DATEDIFF(day,b.DischargeDate,s.Date_Adm) between 0 and 1 and b.payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP')
join hds.HEDIS_VISIT v on b.Memid=v.MemID and v.Measure_id=@meas and v.Date_S between b.AdmissionDate and b.Dischargedate
where v.CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Surgery Procedure') and v.HCFAPOS!='81' and v.SuppData='N'
--and b.Memid=101918
)t1
join hds.HEDIS_RISK_ADJUSTMENT r on t1.staytype=r.VariableName and r.Measure_ID='PCR' and r.ProductLine='Medicare' and r.reportingindicator='SNF - 18+'


update ds set ds.SurgWtSNF=o.surgwt from #pcrdataset ds join #pcr_snf_surgwtlist o on ds.memid=o.memid and substring(ds.Meas,1,4)=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0;

-- Discharge Condition - SNF


drop table if exists #pcr_snf_dischcc;
create table #pcr_snf_dischcc
(
  [Memid] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  DCC INT,
  dccct INT,
  weight float
  
)

insert into #pcr_snf_dischcc
select f2.Memid,f2.Meas,f2.Payer,f2.PPayer,DCC,row_number() over(partition by Memid,meas,payer order by Memid,meas,payer,DCC) as dccct,weight from(
select distinct f1.*,isnull(r.Weight,0) as weight,Cast(replace(Comorbid_CC,'CC-','') as INT) as DCC from(

	select t2.Memid,t2.Meas,t2.Payer,t2.PPayer,t2.Age,t2.Gender,cc.Comorbid_CC
	from(
	select t1.*,replace(v.Diag_I_1,'.','') as diagnosis from(
		select b.*,case when b.claimid>=b.tclaimid then b.claimid else b.tclaimid end as tempclaim from #pcr_base1 b
		join #pcr_SNFStays s on b.Memid=s.Memid and DATEDIFF(day,b.DischargeDate,s.Date_Adm) between 0 and 1 and b.payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP')

	)t1
	join hds.HEDIS_VISIT v on v.MemID=t1.Memid and v.ClaimID=t1.tempclaim and v.Measure_id=@meas
	)t2
	join hds.HEDIS_TABLE_CC cc on t2.diagnosis=cc.DiagnosisCode and cc.CC_type='Shared'
		
)f1
left outer join hds.HEDIS_RISK_ADJUSTMENT r on f1.Comorbid_CC=r.VariableName and r.productline='Medicare' and r.reportingindicator='SNF - 18+' and r.Measure_ID='PCR' and r.VariableType='DCC' and r.Model='Logistic'

)f2




update ds set ds.DCCWt1SNF=o.weight,ds.DCC1SNF=o.DCC from #pcrdataset ds join #pcr_snf_dischcc o on ds.memid=o.memid and substring(ds.Meas,1,4)=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=1
update ds set ds.DCCWt2SNF=o.weight,ds.DCC2SNF=o.DCC from #pcrdataset ds join #pcr_snf_dischcc o on ds.memid=o.memid and substring(ds.Meas,1,4)=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=2
update ds set ds.DCCWt3SNF=o.weight,ds.DCC3SNF=o.DCC from #pcrdataset ds join #pcr_snf_dischcc o on ds.memid=o.memid and substring(ds.Meas,1,4)=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=3

-- AgeGenWt-SNF


drop table if exists #pcr_snf_agegenwt;
Create table #pcr_snf_agegenwt
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	AgeGenWt float
	
)

Insert into #pcr_snf_agegenwt
select distinct t1.Memid,t1.Meas,t1.payer,t1.Ppayer
,r1.Weight as AgeGenWt 
from(
select b.Memid,Meas,payer,ppayer,case 
	when age between 18 and 54 and gender='F' then 'F_18-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 54 and gender='M' then 'M_18-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup
    from #pcr_base1 b
	join #pcr_SNFStays s on b.Memid=s.Memid and DATEDIFF(day,b.DischargeDate,s.Date_Adm) between 0 and 1 and b.payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP')

 )t1
 join hds.HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=t1.genagegroup and r1.productline='Medicare' and r1.variabletype='DEMO' and r1.Model='Logistic' and r1.Measure_id='PCR' and r1.reportingindicator='SNF - 18+'


update ds set ds.AgeGenWtSNF=o.AgeGenWt from #pcrdataset ds join #pcr_snf_agegenwt o on ds.memid=o.memid and substring(ds.Meas,1,4)=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0


-- SNF- ComorbidWt

drop table if exists #pcr_snf_diagnosiswexcl;
Create table #pcr_snf_diagnosiswexcl
(
	Memid varchar(100),
	Meas varchar(10),
	Payer varchar(8),
	PPayer varchar(8),
	Claimid varchar(100),
	diagnosis varchar(50),
	StayNumber INT,
	Age INT,
	Gender varchar(10)
	
)

Insert into #pcr_snf_diagnosiswexcl
select distinct t2.* from(
select distinct t1.Memid,t1.Meas,t1.Payer,t1.PPayer,t1.Claimid,t1.diagnosis,t1.Staynumber,t1.Age,t1.Gender from(
--select * from(
select b.Memid,b.Meas,b.Payer,b.PPayer,d.Claimid,d.diagnosis,b.Staynumber,b.Age,b.Gender from
#pcr_base1 b
join #pcr_diagnosis d on b.Memid=d.Memid and d.Date_s!='' and d.Date_S between dateadd(day,-365,b.DischargeDate) and b.DischargeDate
join #pcr_SNFStays s on b.Memid=s.Memid and DATEDIFF(day,b.DischargeDate,s.Date_Adm) between 0 and 1 and b.payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP') 
--where d.memid=100107
)t1
left outer join #pcr_dischdiagnosis dd on t1.Memid=dd.Memid and t1.Claimid=dd.dischargeclaimid and t1.diagnosis=dd.dischdiagnosis and t1.staynumber=dd.staynumber
where dd.Memid is null
)t2



-- HCC RANK

drop table if exists #pcr_snf_hccrank;

Create table #pcr_snf_hccrank
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)

Insert into #pcr_snf_hccrank
select t3.Memid,t3.Meas,t3.payer,t3.ppayer,
case 
	when age between 18 and 44 and gender='F' then 'F_18-44'
	when age between 45 and 54 and gender='F' then 'F_45-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 44 and gender='M' then 'M_18-44'
	when age between 45 and 54 and gender='M' then 'M_45-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup,
	'SNF - 18+' as reportingindicator,
	case
		When payer in('CEP','HMO','POS','PPO','MEP','MPO','MOS','MMO') then 'Commercial'
		When payer in('MCD','MLI','MRB','MD') then 'Medicaid'
		else 'Medicare'
	end as productline,
	hcc
from(
select distinct Memid,Meas,payer,ppayer,HCC,cast(age as INT) as age,gender,calcrank from (

select Memid,Meas,payer,ppayer,cc.Comorbid_CC,hcc.RankingGroup,isnull(hcc.Rank,1) as Rank,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC,Rank() over(partition by Memid,meas,RankingGroup order by Memid,RankingGroup,Rank) as calcrank,age,gender  from
	(
		select *,case when payer in('MCD','MLI','MRB','MD') then 'Medicaid' else 'Shared' end as producttype from #pcr_snf_diagnosiswexcl d
		
	)t1
left outer join hds.HEDIS_TABLE_CC cc on t1.diagnosis=cc.diagnosiscode and t1.producttype=cc.CC_type
left outer join hds.HEDIS_HCC_RANK hcc on cc.Comorbid_CC=hcc.CC and t1.producttype=hcc.hcc_type
where Comorbid_CC is not null
--and memid=100035 and meas='PCRA'
)t2 where calcrank=1
 --memid=100035 and meas='PCRA'
)t3



-- HCC RANK COMB



drop table if exists #pcr_snf_hccrankcmb;
Create table #pcr_snf_hccrankcmb
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #pcr_snf_hccrankcmb
select distinct h.*,cmb.hcccomb from #pcr_snf_hccrank h 
left outer join hds.HEDIS_HCC_COMB cmb on cmb.ComorbidHCC1 in(select HCC from #pcr_snf_hccrank t1 where t1.memid=h.memid and t1.productline=h.productline and t1.reportingindicator=h.reportingindicator and t1.Meas=h.Meas and t1.payer=h.payer and t1.ppayer=h.ppayer) and cmb.ComorbidHCC2 in(select HCC from #pcr_snf_hccrank t1 where t1.memid=h.memid and t1.productline=h.productline and t1.reportingindicator=h.reportingindicator and t1.Meas=h.Meas and t1.payer=h.payer and t1.ppayer=h.ppayer)
--where h.memid=141630


drop table if exists #pcr_snf_comorbidwt;
Create table #pcr_snf_comorbidwt
(
	Memid varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	ComorbidWt float
	
)

Insert into #pcr_snf_comorbidwt
select Memid,Meas,payer,Ppayer,sum(weight) as ComorbidWt from(
select distinct h.Memid,h.Meas,h.payer,h.Ppayer,h.genagegroup,h.reportingindicator,h.hcc as hcc,r1.Weight from #pcr_snf_hccrankcmb h
join hds.HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcc and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='Logistic' and r1.ReportingIndicator=h.reportingindicator and r1.Measure_ID='PCR'

Union all
select distinct h.Memid,h.meas,h.payer,h.ppayer,h.genagegroup,h.reportingindicator,h.hcccmb as hcc,r1.Weight from #pcr_snf_hccrankcmb h
join hds.HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcccmb and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='Logistic' and r1.ReportingIndicator=h.reportingindicator and r1.Measure_ID='PCR'

)t1 group by memid,Meas,payer,PPayer

update #pcrdataset set ComorbidWt=0
update ds set ds.ComorbidWtSNF=p.ComorbidWt from #pcrdataset ds join #pcr_snf_comorbidwt p on p.Memid=ds.Memid and p.payer=ds.payer and p.Meas=substring(ds.Meas,1,4) and p.ppayer=ds.PPayer and ds.Outlier=0

-- Insert data in Output Table
	select @runid=max(RUN_ID) from hds.HEDIS_MEASURE_OUTPUT_PCR where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END

--	INSERT INTO HEDIS_MEASURE_OUTPUT_PCR(Memid,Meas,Payer,PPayer,Epop,Ppop,Num,Outlier,SNF,ObsWt,SurgWt,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,ComorbidWt,AgeGenWt,ObsWtSNF,SurgWtSNF,DCC1SNF,DCCWt1SNF,DCC2SNF,DCCWt2SNF,DCC3SNF,DCCWt3SNF,ComorbidWtSNF,AgeGenWtSNF,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	select Memid,Meas,Payer,PPayer,Epop,Ppop,Num,Outlier,SNF,ObsWt,SurgWt,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,ComorbidWt,AgeGenWt,ObsWtSNF,SurgWtSNF,DCC1SNF,DCCWt1SNF,DCC2SNF,DCCWt2SNF,DCC3SNF,DCCWt3SNF,ComorbidWtSNF,AgeGenWtSNF,Age,Gender,@meas as Measure_ID,@meas_year as Measurement_year,@runid as RUN_ID from #pcrdataset
	

GO
