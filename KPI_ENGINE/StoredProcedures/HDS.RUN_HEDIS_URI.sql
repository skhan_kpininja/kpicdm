SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




Create PROCEDURE [HDS].[RUN_HEDIS_URI]  @meas_year nvarchar(4)
AS
-- Declare Variables

DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_startdt2 DATE;
DECLARE @ce_enddt2 DATE;
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;
DECLARE @t_planid VARCHAR(10);  


SET @meas='URI';
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');
SET @ce_enddt1=concat(@meas_year-1,'1231');
SET @ce_startdt2=concat(@meas_year-2,'1001');
SET @ce_enddt2=concat(@meas_year-2,'1231');


-- Create temp table to store URI data
drop table if exists #URIdataset;

CREATE TABLE #URIdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
  
  
) ;

-- Eligible Patient List

drop table if exists #URI_memlist; 

CREATE TABLE #URI_memlist (
    memid varchar(100)
    
);


insert into #URI_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt2 and gm.Gender='F'
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 52 AND 74 ORDER BY 1;

CREATE CLUSTERED INDEX ix_memlist_memid ON #URI_memlist ([memid]);


-- Create Temp Patient Enrollment
drop table if exists #URI_tmpsubscriber;

CREATE TABLE #URI_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #URI_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt2
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 52 AND 74 and gm.gender='F' ORDER BY en.MemID,en.StartDate,en.FinishDate;

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #URI_tmpsubscriber ([memid],[StartDate],[EndDate]);


-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #URI_memlist;


While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #URI_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR(@ce_enddt)-YEAR(DOB) FROM #URI_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #URI_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #URI_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #URI_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
			
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				IF(@planid in('MMP','SN3'))
				
				SET @planid='MCD';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				
				SET @planid='MCR';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			ELSE
			BEGIN
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #URI_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #URI_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					
					SET @planid='MCR';
					Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					
					Set @planid='MCR';
					Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				
					
				Set @planid='MCR';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			/*
			ELSE IF(@plan2 IN('SN3','MMP','MDE'))
			BEGIN
			
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
					
				Set @planid='MCR';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			*/
			ELSE
			BEGIN
			
				Set @planid=@plan1;
				
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid=@plan2;
				Insert INTO #URIdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
		END
		
		Set @i=@i+1;
		
	END;
	

-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #URIdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #URIdataset ([patient_id],[payer]);



-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #URI_contenroll;
	
	CREATE table #URI_contenroll
	(
		Memid varchar(50),
		CE INT
	);

	

	
	Insert into #URI_contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
	
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
				END AS start_date,
				CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt AND s1.FinishDate>=@ce_startdt
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID=@meas AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID=@meas
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt)-45)

		Union all

		-- CE Check for 2nd year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN StartDate<@ce_startdt1 THEN @ce_startdt1
				ELSE StartDate
				END AS start_date,
				CASE
				WHEN FinishDate>@ce_enddt1 THEN @ce_enddt1
				ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN @ce_enddt1
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt1 AND s1.FinishDate>=@ce_startdt1
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID=@meas AND s.StartDate<=@ce_enddt1 AND s.FinishDate>=@ce_startdt1
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID=@meas
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt1 AND s.FinishDate>=@ce_startdt1
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt1)-45)


		Union All
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN StartDate<@ce_startdt2 THEN @ce_startdt2
				ELSE StartDate
				END AS start_date,
				CASE
				WHEN FinishDate>@ce_enddt2 THEN @ce_enddt2
				ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN @ce_enddt2
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt2 AND s1.FinishDate>=@ce_startdt2
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID=@meas AND s.StartDate<=@ce_enddt2 AND s.FinishDate>=@ce_startdt2
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID=@meas
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt2 AND s.FinishDate>=@ce_startdt2
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt2)-DATEPART(dy, @ce_startdt2)+1)

			   
		Union all
		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
			END AS start_date,
			CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
			END AS end_date
			FROM HEDIS_MEMBER_EN 
			WHERE MEASURE_ID=@meas
			-- SQLINES DEMO *** 0000          
			AND @ce_enddt BETWEEN StartDate AND FinishDate
		
		)t4  GROUP BY MemID
 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=4 order by MemID;

	CREATE CLUSTERED INDEX ix_URI_contenroll ON #URI_contenroll ([memid]);
	
	update #URIdataset set CE=1 from #URIdataset ds join #URI_contenroll ce on ds.patient_id=ce.MemID;
	
	
	-- AdvancedIllness

	drop table if exists #URI_advillness;
	
	CREATE table #URI_advillness
		(
			Memid varchar(50),
			
			servicedate varchar(8),
			claimid int
		);

	

	Insert into #URI_advillness
	select MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	)
	
	CREATE CLUSTERED INDEX ix_URI_advillness ON #URI_advillness ([memid],[servicedate],[claimid]);
	
	
	
	-- Required Exclusion

	drop table if exists #URI_reqdexcl
	CREATE table #URI_reqdexcl
	(
		Memid varchar(50)
			
	);

	


	insert into #URI_reqdexcl
	select distinct memid from(

	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))

	Union all

	select Memid from HEDIS_OBS where Measure_id=@meas and date between @ce_startdt and @ce_enddt and date !='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
		
	union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S between @ce_startdt and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	
	)
	)t1 
	
	CREATE CLUSTERED INDEX idx_URI_reqdexcl ON #URI_reqdexcl ([memid]);
	
	update #URIdataset set rexcld=1 from #URIdataset ds join #URI_reqdexcl re on ds.patient_id=re.memid;
	
	
	-- Members with Institutinal SNP
	UPDATE #URIdataset SET #URIdataset.rexcl=1 FROM #URIdataset ds JOIN HEDIS_MEMBER_EN s on ds.patient_id=s.MemID and s.MEASURE_ID=@meas WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt AND s.Payer='SN2';


	-- LTI Exclusion
	
	drop table if exists #URI_LTImembers;
	
	CREATE table #URI_LTImembers
	(
		Memid varchar(50)
				
	);

	

	Insert into #URI_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM HEDIS_MMDF WHERE Measure_ID=@meas AND Run_Date BETWEEN @ce_startdt AND @ce_enddt AND LTI_Flag='Y';

	CREATE CLUSTERED INDEX idx_URI_ltimembers ON #URI_LTImembers ([memid]);

	update #URIdataset set rexcl=1 from #URIdataset ds join #URI_LTImembers re on ds.patient_id=re.Memid where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3');




	-- Hospice Exclusion

	drop table if exists #URI_hospicemembers;
	CREATE table #URI_hospicemembers
	(
		Memid varchar(50)
		
	);

	

	Insert into #URI_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from Hedis_MMDF where measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #URI_hospicemembers ([memid]);
	update #URIdataset set rexcl=1 from #URIdataset ds join #URI_hospicemembers hos on hos.memid=ds.patient_id;
			
-- Frailty Members LIST
	drop table if exists #URI_frailtymembers;
	
	CREATE table #URI_frailtymembers
	(
		
		Memid varchar(50)
			
	);

	

	Insert into #URI_frailtymembers
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);

	CREATE CLUSTERED INDEX idx_URI_frailtymembers ON #URI_frailtymembers ([memid]);
	
	
	
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #URI_inpatientstaylist;
	CREATE table #URI_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #URI_inpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
	CREATE CLUSTERED INDEX ix_URI_inpatientstaylist ON #URI_inpatientstaylist ([memid],[date_s]);
	
	-- Non acute Inpatient stay list
	
	drop table if exists #URI_noncauteinpatientstaylist;
	CREATE table #URI_noncauteinpatientstaylist
		(
			Memid varchar(50),
			
			date_s varchar(8),
			claimid int
		);

	

	Insert into #URI_noncauteinpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and (v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')) or RIGHT('0000'+CAST(Trim(v.BillType) AS VARCHAR(4)),4) in (select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')))
	
	CREATE CLUSTERED INDEX ix_URI_nonacuteinpatientstaylist ON #URI_noncauteinpatientstaylist ([memid],[date_s]);
	
	-- Outpatient and other visits
	drop table if exists #URI_visitlist;
	CREATE table #URI_visitlist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #URI_visitlist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	or 
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)
	
	CREATE CLUSTERED INDEX ix_URI_visitlist ON #URI_visitlist ([memid],[date_s]);
	
	-- Required exclusion table
	drop table if exists #URI_reqdexcl1;
	CREATE table #URI_reqdexcl1
	(
		Memid varchar(50)
			
	);

	

	Insert into #URI_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #URI_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #URI_noncauteinpatientstaylist na
	join #URI_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #URI_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #URI_frailtymembers f on f.Memid=t3.Memid


	CREATE CLUSTERED INDEX idx_URI_reqdexcl1 ON #URI_reqdexcl1 ([memid]);
	
	update #URIdataset set rexcl=1 from #URIdataset ds
	join #URI_reqdexcl1 re1 on re1.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #URI_reqdexcl2;
	CREATE table #URI_reqdexcl2
	(
		Memid varchar(50)
				
	);

	

	insert into #URI_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID from (
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	
	)
	)t1
	Join #URI_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #URI_frailtymembers f on f.Memid=t2.MemID

	CREATE CLUSTERED INDEX idx_URI_reqdexcl2 ON #URI_reqdexcl2 ([memid]);
	
	update #URIdataset set rexcl=1 from #URIdataset ds
	join #URI_reqdexcl2 re2 on re2.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #URI_reqdexcl3;
	CREATE table #URI_reqdexcl3
	(
		Memid varchar(50)
			
	);

	

	insert into #URI_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #URI_inpatientstaylist inp
	left outer join #URI_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #URI_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #URI_frailtymembers f on f.Memid=t2.Memid

	CREATE CLUSTERED INDEX idx_URI_reqdexcl3 ON #URI_reqdexcl3 ([memid]);
	
	update #URIdataset set rexcl=1 from #URIdataset ds
	join #URI_reqdexcl3 re3 on re3.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #URI_reqdexcl4;
	CREATE table #URI_reqdexcl4
	(
		Memid varchar(50)
				
	);

	

	insert into #URI_reqdexcl4
	select t1.Memid from(
	select Memid from HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #URI_frailtymembers f on f.MemId=t1.Memid;

	CREATE CLUSTERED INDEX idx_URI_reqdexcl4 ON #URI_reqdexcl4 ([memid]);
	
	update #URIdataset set rexcl=1 from #URIdataset ds
	join #URI_reqdexcl4 re4 on re4.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	

--


-- Numerator

	drop table if exists #URI_numlist;
	CREATE table #URI_numlist
	(
		Memid varchar(50)
		
	);

	

	Insert into #URI_numlist
	select distinct Memid from(
	select distinct memid from HEDIS_PROC where MEASURE_ID=@meas and pstatus='EVN' and sdate between @ce_startdt2 and @ce_enddt and sdate!='' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S!='' and Date_S between @ce_startdt2 and @ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Mammography'))
	)

)t1

	CREATE CLUSTERED INDEX idx_URI_numlist ON #URI_numlist ([memid]);
	
update #URIdataset set num=1 from #URIdataset ds
	join #URI_numlist num on num.Memid=ds.patient_id ;
	
	
-- Optional Exclussion


--mastectomy on both the left and right side on the same or different dates of service

	-- Left Mastectomy 
	drop table if exists #URI_leftmatectomy;
	CREATE table #URI_leftmatectomy
	(
		Memid varchar(50)
		
	);

	insert into #URI_leftmatectomy
	Select distinct memid from(
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and 
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy'))
	and
	(
		CPTMod_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Left Modifier'))
		or
		CPTMod_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Left Modifier'))
	)

	union all

	select memid from HEDIS_DIAG where MEASURE_ID=@meas and sdate<=@ce_enddt and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Clinical Unilateral Mastectomy')) and attcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Clinical Left Modifier'))

	Union all

	
	select distinct memid from HEDIS_diag where MEASURE_ID=@meas and sdate <=@ce_enddt and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	or 
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Left Breast'))
	
	)
	
	Union all

	select distinct memid from HEDIS_PROC where MEASURE_ID=@meas and pstatus='EVN' and sdate <=@ce_enddt and sdate!='' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and (
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Left'))
	)
	)leftmastectomy

	CREATE CLUSTERED INDEX idx_URI_rightmatectomy ON #URI_leftmatectomy ([memid]);
	
	-- right Mastectomy 
	drop table if exists #URI_rightmatectomy;
	CREATE table #URI_rightmatectomy
	(
		Memid varchar(50)
		
	);

	insert into #URI_rightmatectomy
	Select distinct memid from(
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and 
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy'))
	and
	(
		CPTMod_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Right Modifier'))
		or
		CPTMod_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Right Modifier'))
	)

	union all

	select memid from HEDIS_DIAG where MEASURE_ID=@meas and sdate<=@ce_enddt and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Clinical Unilateral Mastectomy')) and attcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Clinical Right Modifier'))

	Union all

	
	select distinct memid from HEDIS_diag where MEASURE_ID=@meas and sdate <=@ce_enddt and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	or 
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Absence of Right Breast'))
	
	)
	
	Union all

	select distinct memid from HEDIS_PROC where MEASURE_ID=@meas and pstatus='EVN' and sdate <=@ce_enddt and sdate!='' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and (
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy Right'))
	)
	)rightmastectomy

	CREATE CLUSTERED INDEX idx_#URI_rightmatectomy ON #URI_rightmatectomy ([memid]);


	drop table if exists #URI_eventlist;
	CREATE table #URI_eventlist
	(
		Memid varchar(50)
		
	);


	Insert into #URI_eventlist
	Select distinct memid from(
	select distinct t1.memid from #URI_rightmatectomy t1
	join #URI_leftmatectomy t2 on t1.memid=t2.memid

	Union all
	--?	Bilateral mastectomy 
	select distinct memid from HEDIS_PROC where MEASURE_ID=@meas and pstatus='EVN' and sdate <=@ce_enddt and sdate!='' and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81  and Date_S!='' and Date_S <=@ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Mastectomy'))
	)

	Union all
	-- Unilateral mastectomy (Unilateral Mastectomy Value Set) with a bilateral modifier (Bilateral Modifier Value Set). 
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S <=@ce_enddt
	and 
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Unilateral Mastectomy'))
	and
	(
		CPTMod_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Modifier'))
		or
		CPTMod_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Bilateral Modifier'))
	)
	
	Union all

	-- Unilateral mastectomy found in clinical data (Clinical Unilateral Mastectomy Value Set) with a bilateral modifier (Clinical Bilateral Modifier Value Set)

	select memid from HEDIS_DIAG where MEASURE_ID=@meas and sdate<=@ce_enddt and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Clinical Unilateral Mastectomy')) and attcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Clinical Bilateral Modifier'))

	Union all
	--History of bilateral mastectomy 

	select distinct memid from HEDIS_diag where MEASURE_ID=@meas and sdate <=@ce_enddt and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))

	Union all
	select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81  and Date_S!='' and Date_S <=@ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	or 
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('History of Bilateral Mastectomy'))
	
	)
	
)t1

CREATE CLUSTERED INDEX idx_URI_eventlist ON #URI_eventlist ([memid]);	

update #URIdataset set Excl=1 from #URIdataset ds
	join #URI_eventlist e on e.Memid=ds.patient_id ;



--SES Startification

update #URIdataset set lis=1 from #URIdataset ds join HEDIS_MMDF m on m.beneficiary_id=ds.patient_id and m.Measure_id=@meas
where m.run_date between @ce_startdt2 and @ce_enddt and m.LIS_Premium_Subsidy>0 and ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');;

update #URIdataset set lis=1 from #URIdataset ds join HEDIS_LISHIST l on l.beneficiary_id=ds.patient_id and l.Measure_id=@meas
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and ((Low_Income_Period_End_Date>=@ce_startdt2 and Low_Income_Period_Start_Date<=@ce_enddt) or Low_Income_Period_End_Date='');


-- Item 48
drop table if exists #URI_orec;
CREATE table #URI_orec
	(
		Memid varchar(50),
		orec varchar(2)
			
	);

	CREATE CLUSTERED INDEX idx_cdc_orec ON #URI_orec ([memid],[orec]);

Insert into #URI_orec
select m1.Beneficiary_ID,m1.Original_Reason_for_Entitlement_Code_OREC as orec from HEDIS_MMDF m1 where m1.Measure_id=@meas  and m1.Run_Date=(select max(m2.Run_Date) from HEDIS_MMDF m2 where m2.MEASURE_ID=@meas  and m1.Beneficiary_ID=m2.Beneficiary_ID and m2.Run_Date between @ce_startdt2 and @ce_enddt and m2.Original_Reason_for_Entitlement_Code_OREC!='') order by m1.Beneficiary_ID


update #URIdataset set orec=o.orec from #URIdataset ds
join #URI_orec o on o.Memid=ds.patient_id
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

update #URIdataset set meas='URINON' WHERE orec=0 AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');


UPDATE #URIdataset SET meas='URILISDE' WHERE orec=0 AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

UPDATE #URIdataset SET meas='URIDIS' WHERE orec IN(1,3) AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

UPDATE #URIdataset SET meas='URICMB' WHERE orec IN(1,3) AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');


UPDATE #URIdataset SET meas='URIOT' WHERE orec IN(2,9) AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

	
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	

	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,0 as EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0  THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,@meas,@meas_year,@runid FROM #URIdataset

GO
