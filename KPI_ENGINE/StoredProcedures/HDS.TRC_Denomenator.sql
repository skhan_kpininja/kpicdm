SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
---USE KPI_ENGINE
CREATE PROC [HDS].[TRC_Denomenator]
AS



drop table if exists #TRC_memlist; 

CREATE TABLE #TRC_memlist (
    memid varchar(100)
    
);


insert into #TRC_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY 1;


	-- Inpatient Stay List
	drop table if exists #TRC_inpatientstaylist;
	CREATE table #trc_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		date_disch varchar(8),
		claimid int
	);

Insert into #TRC_inpatientstaylist
select distinct MemID,Date_S,date_disch,claimid from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
CREATE CLUSTERED INDEX ix_cdc_inpatientstaylist ON #TRC_inpatientstaylist ([memid],[date_s]);

----Readmission---------
/*	
drop table if exists #Readmission;
	CREATE table #Readmission
	(
		Memid varchar(50)
	);
Insert into #Readmission(Memid)
Select v.MemID from HDS.HEDIS_VISIT v 
Inner join #TRC_inpatientstaylist IP on V.MemID=IP.Memid and convert(date,V.Date_S) Between convert(date,IP.date_disch) and dateadd(d,30,convert(date,IP.date_disch))
where V.MEASURE_ID='TRC_SAMPLE' AND v.HCFAPOS!=81 and suppdata='N' ---and V.Memid='102855' 
*/


drop table if exists #Readmission;
	CREATE table #Readmission
	(
		Memid varchar(50)
	);
Insert into #Readmission(Memid)
select distinct MemID from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) >'2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));


drop table if exists #TRC_inpatientstaylist_Final;
	CREATE table #TRC_inpatientstaylist_Final
	(
		Memid varchar(50)
	);
Insert into #TRC_inpatientstaylist_Final(Memid)
select distinct IP.Memid from #TRC_inpatientstaylist Ip
left join #Readmission R on IP.Memid=R.Memid
where  R.Memid is NULL ----AND ip.Memid='148480'





	drop table if exists #Denomerator;
	CREATE table #Denomerator
	(
		Memid varchar(50)
	);

Insert into #Denomerator
Select distinct M.memid from #TRC_memlist M
inner join (select distinct MemId from #TRC_inpatientstaylist_Final) IP on M.memid=Ip.Memid
----where M.memid='146792'
CREATE CLUSTERED INDEX ix_TRC_Denomeratort ON #Denomerator ([memid]);




-- Create Temp Patient Enrollment
drop table if exists #TRC_tmpsubscriber;

CREATE TABLE #TRC_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #TRC_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
Inner join #Denomerator D on gm.MemID=D.Memid
WHERE en.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY en.MemID,CONVERT(DATE,en.StartDate),CONVERT(DATE,FinishDate);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #TRC_tmpsubscriber ([memid],[StartDate],[EndDate]);



Drop table if exists #TRCdataset;

CREATE TABLE #TRCdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT 0,
) ;

Declare @pt_ctr INT;
Declare @i INT = 0;
Declare @patientid varchar(100);
Declare @gender varchar(1);
Declare @age int;
Declare @latest_insenddate varchar(10);
Declare @plan_ct INT=0;
Declare @planid varchar(20);
Declare @meas varchar(10)='TRC';
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);

-- Loop through enrollment for each patient
SELECT @pt_ctr=COUNT(*)  FROM #Denomerator;

PRINT 'pt_ctr:' + Cast(@pt_ctr as nvarchar);


While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #Denomerator ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=Year('2020-12-31')-Year(dob) FROM #TRC_tmpsubscriber WHERE memid=@patientid;

		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #TRC_tmpsubscriber WHERE  memid=@patientid  AND convert(date,StartDate)<='2020-12-31' ORDER BY convert(date,StartDate) DESC,CONVERT(DATE,EndDate) Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #TRC_tmpsubscriber WHERE  memid=@patientid AND CONVERT(DATE,@latest_insenddate) BETWEEN convert(date,StartDate) AND CONVERT(DATE,EndDate);
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #TRC_tmpsubscriber WHERE  memid=@patientid AND CONVERT(DATE,@latest_insenddate) BETWEEN convert(date,StartDate) AND CONVERT(DATE,EndDate) ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print '1'+@planid
				SET @planid='MCR';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'A'+@planid
				---SET @planid='MCS';
				----Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				----Print '2'+@planid
				
			END

			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'B'+@planid
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				SET @planid='MCR';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'C'+@planid
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'D'+@planid
				
			END
			ELSE
			BEGIN
				if(@planid IN('CEP','HMO','POS','PPO'))
				BEGIN
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'E'+@planid
				END
				ELSE
				BEGIN
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'F'+@planid
				End
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #TRC_tmpsubscriber where memid=@patientid and CONVERT(DATE,@latest_insenddate) between CONVERT(DATE,StartDate) and CONVERT(DATE,EndDate) order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #TRC_tmpsubscriber where memid=@patientid and CONVERT(DATE,@latest_insenddate) between CONVERT(DATE,StartDate) and CONVERT(DATE,EndDate) order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid='MCR';
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'G'+@planid
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'H'+@planid
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					Set @planid='MCR';
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'I'+@planid
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'J'+@planid
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'K'+@planid
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'L'+@planid
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				Set @planid='MCR';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'M'+@planid
				
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'N'+@planid
				Set @planid=@plan2;
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'O'+@planid
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;	
-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #TRCdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #TRCdataset ([patient_id],[payer]);

-- Continuous Enrollment


/*
select * from #TRCdataset T
left join hds.HEDIS_SCORE S On T.Patient_Id=S.MemId and S.Measure_Id='TRC_SAMPLE'
AND T.Payer=S.Payer
where S.MemID is NULL
---S.MemID='146792'
order by S.MemID 

select * from HDS.TRCdataset where patient_id='135993' order by payer
select * from #TRC_tmpsubscriber where memid='146792'
---select * from #TRCdataset where patient_id='146792' order by payer
select * from hds.HEDIS_SCORE where memId='146792' and Measure_Id='TRC_SAMPLE' order by payer
*/



-- Continuous Enrollment


drop table if exists #MEMBERLIST_WITH_DISCHARGE;
	CREATE table #MEMBERLIST_WITH_DISCHARGE
	(
		Memid varchar(50),
		date_disch varchar(50)
	);
Insert into #MEMBERLIST_WITH_DISCHARGE(Memid,date_disch)
select distinct IP.Memid,Ip.date_disch from #TRC_inpatientstaylist Ip
left join #Readmission R on IP.Memid=R.Memid
where  R.Memid is NULL ----AND ip.Memid='148480'


drop table if exists #CE;
	CREATE table #CE
	(
		Memid varchar(50)
	);
Insert into #CE(Memid)
Select distinct S.MemID----,*
from #TRC_tmpsubscriber S
inner join #MEMBERLIST_WITH_DISCHARGE D
on S.memid=D.Memid ----and convert(date,D.date_disch) BETWEEN CONVERT(DATE,S.StartDate) AND CONVERT(DATE,S.EndDate) 
AND CONVERT(DATE,S.EndDate)>=DATEADD(d,30,convert(date,D.date_disch))
where S.memid='102855'

select * from #TRC_tmpsubscriber where memid='100819'
select * from #MEMBERLIST_WITH_DISCHARGE where memid='100819'



update #TRCdataset set CE=1 from #TRCdataset ds join #CE CE on CE.memid=ds.patient_id;
update #TRCdataset set CE=0 

select * from #TRCdataset E
left join hds.HEDIS_score S 
on E.patient_id=S.MemID 
where measure_id='TRC_SAMPLE' and E.CE<>S.CE



select top 100 *,ROW_NUMBER() OVER(PARTITION by MEMID order by CONVERT(DATE,StartDate),CONVERT(DATE,EndDate) ) from #TRC_tmpsubscriber where memid='102855'
select top 100 * from #MEMBERLIST_WITH_DISCHARGE where memid='102855'
--select top 100 * from hds.HEDIS_VISIT where memid='102855' and Measure_Id='TRC_SAMPLE'
select * from hds.HEDIS_SCORE where memId='102855' and Measure_Id='TRC_SAMPLE'



select * from hds.HEDIS_member_en where memid='135993' and Measure_Id='TRC_SAMPLE' 
select * from hds.HEDIS_SCORE where memid='135993' and Measure_Id='TRC_SAMPLE' 
select * from #MEMBERLIST_WITH_DISCHARGE where  memid='135993' ---and Measure_Id='TRC_SAMPLE' 



exec Kpi_ENGINE.[dbo].[SP_KPI_DROPTABLE] '#ENROLLMENT_GAP_CHECK'
select A.memid, CASE WHEN convert(date,D.date_disch)<CONVERT(DATE,B.StartDate) then 1 else 0 END AS Flag1, 
CASE WHEN datediff(d,convert(date,A.EndDate),CONVERT(DATE,B.StartDate))>1 then 1 else 0 END AS Flag2
into #ENROLLMENT_GAP_CHECK
from 
(select memid,StartDate,EndDate,ROW_NUMBER() OVER(PARTITION by MEMID order by CONVERT(DATE,StartDate),CONVERT(DATE,EndDate) ) as RN
from #TRC_tmpsubscriber) A
inner join 
(
select memid,StartDate,EndDate,ROW_NUMBER() OVER(PARTITION by MEMID order by CONVERT(DATE,StartDate),CONVERT(DATE,EndDate) ) as RN
from #TRC_tmpsubscriber ---where memid='100033'
) B ON A.memid=B.memid and B.RN>A.RN
left join #MEMBERLIST_WITH_DISCHARGE D
on A.memid=D.Memid ----and convert(date,D.date_disch) BETWEEN CONVERT(DATE,S.StartDate) AND CONVERT(DATE,S.EndDate) 
AND CONVERT(DATE,B.EndDate)>=DATEADD(d,30,convert(date,D.date_disch)) 
---where A.memid='100033' 



exec Kpi_ENGINE.[dbo].[SP_KPI_DROPTABLE] '#ENROLLMENT_GAP'
select distinct MEMID 
into #ENROLLMENT_GAP
from #ENROLLMENT_GAP_CHECK
where Flag1='1' AND flag2='1'


select CE.Memid 
---into #CE_FINAL
from #CE CE
left join #ENROLLMENT_GAP GAP
on CE.Memid=GAP.memid
where GAP.memid is null

update #TRCdataset set CE=0 
update #TRCdataset set CE=1 from #TRCdataset ds join #CE_FINAL CE on CE.memid=ds.patient_id;
GO
