SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc  [REG].[RUN_REGISTRY_RheumatoidArthritis]
As

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)

Declare @reportId INT;


-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Rheumatoid Arthritis' and ROOT_COMPANIES_ID=@rootId;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Rheumatoid Arthritis' and ROOT_COMPANIES_ID=@rootId;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Rheumatoid Arthritis Opportunities' and ROOT_COMPANIES_ID=@rootId;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Rheumatoid Arthritis Opportunities' and ROOT_COMPANIES_ID=@rootId;

		
-- Identify Members with ('Outpatient','Telephone Visits','Online Assessments','Non Acute Inpatient Stays') between Jan and Nov

drop table if exists #rheumatoidvstlist;
CREATE table #rheumatoidvstlist
(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		ServiceDate Date,
		Claim_id varchar(100),
		Data_Source varchar(50)
)
insert into #rheumatoidvstlist
Select distinct * from(
select distinct 
	p.EMPI
	,p.MEMBER_ID
	,PROC_START_DATE as servicedate
	,p.CLAIM_ID
	,p.PROC_DATA_SRC 
from PROCEDURES p
join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
					p.SV_LINE=c.SV_LINE and 
					p.PROC_DATA_SRC=c.CL_DATA_SRC and 
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where p.ROOT_COMPANIES_ID=@rootId and 
	  PROC_START_DATE between DATEADD(yy, DATEDIFF(yy, 0, @enddate), 0) and Dateadd(month,-1,DATEADD(yy, DATEDIFF(yy, 0, @enddate) + 1, -1)) and
	  ISNULL(c.POS,'')!='81' and 
	  PROC_CODE in(select code from KPI_ENGINE.hds.VALUESET_TO_CODE where value_set_name in('Outpatient','Telephone Visits','Online Assessments'))

Union All


select distinct 
	c.EMPI
	,c.MEMBER_ID
	,FROM_DATE as servicedate
	,c.CLAIM_ID
	,c.CL_DATA_SRC 
from CLAIMLINE c
where c.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between DATEADD(yy, DATEDIFF(yy, 0, @enddate), 0) and Dateadd(month,-1,DATEADD(yy, DATEDIFF(yy, 0, @enddate) + 1, -1)) and 
	  ISNULL(c.POS,'')!='81' and 
	  c.REV_CODE in(select code from KPI_ENGINE.hds.VALUESET_TO_CODE where Code_System='UBREV' and value_set_name='Outpatient')


Union all

select 
	EMPI
	,MEMBER_ID
	,FROM_DATE as servicedate
	,CLAIM_ID 
	,CL_DATA_SRC
from CLAIMLINE v 
where v.ROOT_COMPANIES_ID=@rootId and 
	  ISNULL(v.POS,'')!='81' and 
	  v.DIS_DATE!='' and 
	  v.DIS_DATE between DATEADD(yy, DATEDIFF(yy, 0, @enddate), 0) and Dateadd(month,-1,DATEADD(yy, DATEDIFF(yy, 0, @enddate) + 1, -1)) and 
	  REV_CODE in(select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV'and value_set_name='Inpatient Stay') 
	  and 
     (
	   REV_CODE in(select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV'and value_set_name='Nonacute Inpatient Stay') 
	   or 
	   RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in(select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBTOB'and value_set_name='Nonacute Inpatient Stay')

	 )
	
)t1


CREATE CLUSTERED INDEX idx_art_vstlist ON #rheumatoidvstlist (EMPI,ServiceDate,Claim_Id);


-- Identify Members with Rheumatoid Diagnosis

drop table if exists #rheumatoidlist;
CREATE table #rheumatoidlist
(
	EMPI varchar(100),
	Memid varchar(100),
	ServiceDate Date,
	DiagnosisCode varchar(20),
	AttendingNPI varchar(20),
	AttendingName varchar(200),
	AttendingSpecialty varchar(100),
	Claim_Id varchar(100),
	DataSource varchar(50)
)
Insert into #rheumatoidlist
select distinct 
	d.EMPI
	,d.MEMBER_ID
	,d.DIAG_START_DATE  as servicedate
	,d.DIAG_CODE
	,c.ATT_NPI
	,c.ATT_PROV_NAME
	,c.ATT_PROV_SPEC
	,d.CLAIM_ID
	,d.DIAG_DATA_SRC
from DIAGNOSIS d
Join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  ISNULL(c.POS,'')!='81' and 
	  DIAG_START_DATE!='' and 
	  DIAG_START_DATE between DATEADD(yy, DATEDIFF(yy, 0, @enddate), 0) and Dateadd(month,-1,DATEADD(yy, DATEDIFF(yy, 0, @enddate) + 1, -1))
	  and 
	  DIAG_CODE in
	  (
		select Code_New from KPI_ENGINE.hds.VALUESET_TO_CODE where value_set_name='Rheumatoid Arthritis'
	  )


CREATE CLUSTERED INDEX idx_art_rheumatoidlist ON #rheumatoidlist (EMPI,servicedate,CLAIM_ID);


-- Identify members with Rheumatoid diagnosis on multiple encounters and only with('Outpatient','Telephone Visits','Online Assessments','Non Acute Inpatient Stays')

Drop table if exists #rheumatoidwithmultipleoccurrence;
Create Table #rheumatoidwithmultipleoccurrence
(
	Empi varchar(100),
	MemId varchar(100)
)
Insert into #rheumatoidwithmultipleoccurrence
select 
	EMPI
	,MEMId 
from(
	Select distinct 
		r.EMPi
		,r.Memid
		,r.ServiceDate 
	from #rheumatoidlist r
	join #rheumatoidvstlist v on r.Claim_Id=v.Claim_id and 
								 r.DataSource=v.Data_Source
)t1 
group by EMPI,MemId 
having count(EMPI)>1


-- Identify members with Rheumatoid diagnosis with only one encounter and with('Outpatient','Telephone Visits','Online Assessments','Non Acute Inpatient Stays')

Drop table if exists #rheumatoidwithoneoccurrence;
Create Table #rheumatoidwithoneoccurrence
(
	Empi varchar(100),
	MemId varchar(100)
)
Insert into #rheumatoidwithoneoccurrence
select 
	EMPI
	,MEMId 
from(
	Select distinct 
		r.EMPi
		,r.Memid
		,r.ServiceDate 
	from #rheumatoidlist r
	join #rheumatoidvstlist v on r.Claim_Id=v.Claim_id and 
								 r.DataSource=v.Data_Source
)t1 
group by EMPI,MemId 
having count(EMPI)=1



-- Generate Output for  Rheumatoid Arthritis 
-- Insert into registry Line
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
	select	
		EMPI
		,Payer
		,PayerId
		,MemberLastName
		,MemberFirstName
		,MemberMiddleName
		,MemberDOB
		,MEM_GENDER
		,EnrollmentStatus
		,PracticeName
		,AttributedProviderName
		,AttributedProviderSpecialty
		,AttributedProviderNPI
		,RegistryName
		,DiagnosisCode
		,ServiceDate
		,AttendingProviderName
		,AttendingProviderNPI
		,Case  
			when AttendingProviderSpecialty is null then PreferredConceptName 
			else AttendingProviderSpecialty 
		end as AttendingProviderSpecialty
		,DataSource as  DiagnosisDataSource
		,DENSE_RANK() over (partition by EMPI order by ServiceDate desc,AttendingProviderNPI desc,rn desc) as rnk 
	From(

		Select 
			a.AmbulatoryPCPPractice as PracticeName
			,a.AmbulatoryPCPName as AttributedProviderName
			,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
			,a.AmbulatoryPCPNPI as AttributedProviderNPI
			,a.MemberLastName
			,a.MemberFirstName
			,a.MemberMiddleName
			,a.DATA_SOURCE as Payer
			,a.PayerId
			,a.MemberDOB
			,a.MEM_GENDER
			,a.EnrollmentStatus
			,a.AmbulatoryPCPRecentVisit as RecentVisit
			,'Rheumatoid Arthritis' as RegistryName
			,t1.*
			,t.PreferredConceptName
			,row_number() over(partition by t1.EMPI order by ServiceDate Desc) as rn
		from(
			select distinct 
				r.EMPI
				,r.Memid
				,r.ServiceDate
				,r.DiagnosisCode
				,r.AttendingNPI as AttendingProviderNPI
				,case 
					when n.NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
					else r.AttendingName 
				end as AttendingProviderName
				,r.AttendingName
				,r.AttendingSpecialty as AttendingProviderSpecialty
				,Case 
					when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
					when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
					when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
					when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
					when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
				end as PrimarySpecialtyCode
				,r.DataSource 
			from #rheumatoidlist r
			join #rheumatoidvstlist v on r.Claim_Id=v.Claim_id and r.DataSource=v.Data_Source
			left outer join KPI_ENGINE.dbo.RFT_NPI n on r.AttendingNPI=n.NPI
			where r.EMPI in (select EMPI from #rheumatoidwithmultipleoccurrence)
		)t1
		join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t1.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId and a.ROOT_COMPANIES_ID=@rootId
		left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
	)t2
)t3



--Insert into Registry
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
	select	
		EMPI
		,Payer
		,PayerId
		,MemberLastName
		,MemberFirstName
		,MemberMiddleName
		,MemberDOB
		,MEM_GENDER
		,EnrollmentStatus
		,PracticeName
		,AttributedProviderName
		,AttributedProviderSpecialty
		,AttributedProviderNPI
		,RegistryName
		,DiagnosisCode
		,ServiceDate
		,AttendingProviderName
		,AttendingProviderNPI
		,Case  
			when AttendingProviderSpecialty is null then PreferredConceptName 
			else AttendingProviderSpecialty 
		end as AttendingProviderSpecialty
		,DataSource as  DiagnosisDataSource
		,DENSE_RANK() over (partition by EMPI order by ServiceDate desc,AttendingProviderNPI desc,rn desc) as rnk 
	From(

		Select 
			a.AmbulatoryPCPPractice as PracticeName
			,a.AmbulatoryPCPName as AttributedProviderName
			,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
			,a.AmbulatoryPCPNPI as AttributedProviderNPI
			,a.MemberLastName
			,a.MemberFirstName
			,a.MemberMiddleName
			,a.DATA_SOURCE as Payer
			,a.PayerId
			,a.MemberDOB
			,a.MEM_GENDER
			,a.EnrollmentStatus
			,a.AmbulatoryPCPRecentVisit as RecentVisit
			,'Rheumatoid Arthritis' as RegistryName
			,t1.*
			,t.PreferredConceptName
			,row_number() over(partition by t1.EMPI order by ServiceDate Desc) as rn
		from(
			select distinct 
				r.EMPI
				,r.Memid
				,r.ServiceDate
				,r.DiagnosisCode
				,r.AttendingNPI as AttendingProviderNPI
				,case 
					when n.NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
					else r.AttendingName 
				end as AttendingProviderName
				,r.AttendingName
				,r.AttendingSpecialty as AttendingProviderSpecialty
				,Case 
					when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
					when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
					when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
					when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
					when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
				end as PrimarySpecialtyCode
				,r.DataSource 
			from #rheumatoidlist r
			join #rheumatoidvstlist v on r.Claim_Id=v.Claim_id and r.DataSource=v.Data_Source
			left outer join KPI_ENGINE.dbo.RFT_NPI n on r.AttendingNPI=n.NPI
			where r.EMPI in (select EMPI from #rheumatoidwithmultipleoccurrence)
		)t1
		join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t1.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId and a.ROOT_COMPANIES_ID=@rootId
			left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
	)t2
)t3
where rnk=1



-- Generate output for Rheumatoid Arthritis Opportunities
-- Insert into Registry Line
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
	select	
		EMPI
		,Payer
		,PayerId
		,MemberLastName
		,MemberFirstName
		,MemberMiddleName
		,MemberDOB
		,MEM_GENDER
		,EnrollmentStatus
		,PracticeName
		,AttributedProviderName
		,AttributedProviderSpecialty
		,AttributedProviderNPI
		,RegistryName
		,DiagnosisCode
		,ServiceDate
		,AttendingProviderName
		,AttendingProviderNPI
		,Case  
			when AttendingProviderSpecialty is null then PreferredConceptName 
			else AttendingProviderSpecialty 
		end as AttendingProviderSpecialty
		,DataSource as  DiagnosisDataSource
		,DENSE_RANK() over (partition by EMPI order by ServiceDate desc,AttendingProviderNPI desc,rn desc) as rnk 
	From(

		Select 
			a.AmbulatoryPCPPractice as PracticeName
			,a.AmbulatoryPCPName as AttributedProviderName
			,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
			,a.AmbulatoryPCPNPI as AttributedProviderNPI
			,a.MemberLastName
			,a.MemberFirstName
			,a.MemberMiddleName
			,a.DATA_SOURCE as Payer
			,a.PayerId
			,a.MemberDOB
			,a.MEM_GENDER
			,a.EnrollmentStatus
			,a.AmbulatoryPCPRecentVisit as RecentVisit
			,'Rheumatoid Arthritis Opportunities' as RegistryName
			,t1.*
			,t.PreferredConceptName
			,row_number() over(partition by t1.EMPI order by ServiceDate Desc) as rn
		from(
			select distinct 
				r.EMPI
				,r.Memid
				,r.ServiceDate
				,r.DiagnosisCode
				,r.AttendingNPI as AttendingProviderNPI
				,case 
					when n.NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
					else r.AttendingName 
				end as AttendingProviderName
				,r.AttendingName
				,r.AttendingSpecialty as AttendingProviderSpecialty
				,Case 
					when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
					when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
					when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
					when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
					when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
				end as PrimarySpecialtyCode
				,r.DataSource 
			from #rheumatoidlist r
			join #rheumatoidvstlist v on r.Claim_Id=v.Claim_id and r.DataSource=v.Data_Source
			left outer join KPI_ENGINE.dbo.RFT_NPI n on r.AttendingNPI=n.NPI
			where r.EMPI in (select EMPI from #rheumatoidwithoneoccurrence)
		)t1
		join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t1.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId and a.ROOT_COMPANIES_ID=@rootId
			left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
	)t2
)t3



--Insert into Registry
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
	select	
		EMPI
		,Payer
		,PayerId
		,MemberLastName
		,MemberFirstName
		,MemberMiddleName
		,MemberDOB
		,MEM_GENDER
		,EnrollmentStatus
		,PracticeName
		,AttributedProviderName
		,AttributedProviderSpecialty
		,AttributedProviderNPI
		,RegistryName
		,DiagnosisCode
		,ServiceDate
		,AttendingProviderName
		,AttendingProviderNPI
		,Case  
			when AttendingProviderSpecialty is null then PreferredConceptName 
			else AttendingProviderSpecialty 
		end as AttendingProviderSpecialty
		,DataSource as  DiagnosisDataSource
		,DENSE_RANK() over (partition by EMPI order by ServiceDate desc,AttendingProviderNPI desc,rn desc) as rnk 
	From(

		Select 
			a.AmbulatoryPCPPractice as PracticeName
			,a.AmbulatoryPCPName as AttributedProviderName
			,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
			,a.AmbulatoryPCPNPI as AttributedProviderNPI
			,a.MemberLastName
			,a.MemberFirstName
			,a.MemberMiddleName
			,a.DATA_SOURCE as Payer
			,a.PayerId
			,a.MemberDOB
			,a.MEM_GENDER
			,a.EnrollmentStatus
			,a.AmbulatoryPCPRecentVisit as RecentVisit
			,'Rheumatoid Arthritis Opportunities' as RegistryName
			,t1.*
			,t.PreferredConceptName
			,row_number() over(partition by t1.EMPI order by ServiceDate Desc) as rn
		from(
			select distinct 
				r.EMPI
				,r.Memid
				,r.ServiceDate
				,r.DiagnosisCode
				,r.AttendingNPI as AttendingProviderNPI
				,case 
					when n.NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
					else r.AttendingName 
				end as AttendingProviderName
				,r.AttendingName
				,r.AttendingSpecialty as AttendingProviderSpecialty
				,Case 
					when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
					when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
					when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
					when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
					when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
				end as PrimarySpecialtyCode
				,r.DataSource 
			from #rheumatoidlist r
			join #rheumatoidvstlist v on r.Claim_Id=v.Claim_id and r.DataSource=v.Data_Source
			left outer join KPI_ENGINE.dbo.RFT_NPI n on r.AttendingNPI=n.NPI
			where r.EMPI in (select EMPI from #rheumatoidwithoneoccurrence)
		)t1
		join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t1.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId and a.ROOT_COMPANIES_ID=@rootId
			left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
	)t2
)t3
where rnk=1


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
