SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
Create PROC RPT.LOAD_SCORECARD_FROM_HEDIS
AS
BEGIN

INSERT INTO [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
(
	  [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[SPECIALTY]
      ,[PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TITLE]
      ,[MEASURE_SUBTITLE]
      ,[MEASURE_TYPE]
      ,[NUM_COUNT]
      ,[DEN_COUNT]
      ,[EXCL_COUNT]
      ,[REXCL_COUNT]
      ,[GAPS]
      ,[RESULT]
      ,[TARGET]
      ,[TO_TARGET]
      ,[REPORT_ID]
      ,[ReportType]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT P.AmbulatoryPCPNPI [PROVIDER_ID]
      ,P.AmbulatoryPCPNPI [PCP_NPI]
      ,P.AmbulatoryPCPName [PCP_NAME]
      ,P.AmbulatoryPCPSpecialty [SPECIALTY]
      ,NULL [PRACTICE_NAME]
      ,H.MEASURE_ID [MEASURE_ID]
      ,H.Meas [MEASURE_NAME]
      ,NULL [MEASURE_TITLE]
      ,NULL [MEASURE_SUBTITLE]
      ,'HEDIS' [MEASURE_TYPE]
      ,SUM(convert(int,H.Num)) [NUM_COUNT]
      ,count(H.memID) [DEN_COUNT]
      ,SUM(convert(int,H.Excl)) [EXCL_COUNT]
      ,SUM(convert(int,H.RExcl)) [REXCL_COUNT]
      ,count(H.memID)-SUM(convert(int,H.Num)) [GAPS]
      ,ROUND(SUM(convert(int,H.Num)) * 100.0 / count(H.memID), 1) [RESULT]
      ,NULL [TARGET]
      ,NULL [TO_TARGET]
      ,R.ReportId [REPORT_ID]
      ,R.ReportType [ReportType]
      ,convert(date,R.ReportDateTime) [REPORT_RUN_DATE]
      ,YEAR(R.ReportDateTime)+DATEPART(QUARTER,R.ReportDateTime) [Report_Quarter]
      ,R.ReportStartDate [PERIOD_START_DATE]
      ,R.ReportEndDate [PERIOD_END_DATE]
      ,H.ROOT_COMPANIES_ID [ROOT_COMPANIES_ID]
---  FROM [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
From [RPT].[Report_Details] R
INNER JOIN [RPT].[PCP_ATTRIBUTION] P
ON R.ReportId=P.ReportId and R.ReportId=(select max(ReportId) from [RPT].[Report_Details])
LEFT JOIN HDS.HEDIS_MEASURE_OUTPUT H
ON P.EMPI=H.MEMID
where H.ROOT_COMPANIES_ID='159' and MEASURE_ID in ('ART','CHL')
group by 
	   P.AmbulatoryPCPNPI
      ,P.AmbulatoryPCPNPI
      ,P.AmbulatoryPCPName
      ,P.AmbulatoryPCPSpecialty
      ,H.MEASURE_ID
      ,H.Meas
	  ,R.ReportId
	  ,R.ReportType
	  ,convert(date,R.ReportDateTime)
      ,YEAR(R.ReportDateTime)+DATEPART(QUARTER,R.ReportDateTime)
      ,R.ReportStartDate
      ,R.ReportEndDate
      ,H.ROOT_COMPANIES_ID


END
GO
