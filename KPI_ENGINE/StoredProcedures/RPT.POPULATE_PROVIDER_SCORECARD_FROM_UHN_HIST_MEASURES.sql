SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [RPT].[POPULATE_PROVIDER_SCORECARD_FROM_UHN_HIST_MEASURES]
AS
BEGIN

EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE'
SELECT 
[Providers NPI] AS [PROVIDER_ID], LEFT(RIGHT(FTP_FILE_NAME,charindex('\',reverse(FTP_FILE_NAME),1)-1),4)+' - '+RIGHT(LTRIM(RTRIM(Measure)),2) AS QUARTER,
LTRIM(RTRIM(RIGHT(LEFT(Measure, charindex(' ', Measure, charindex(' ', Measure) + 1)),3))) AS Measure_ID,
LEFT(Measure, charindex(' ', Measure, charindex(' ', Measure) + 1)) AS Measure_NAME, 
REPLACE(LTRIM(RTRIM(Score)),'%','') AS Score
INTO #TEMP_HISTORICAL_MEASURE
FROM [KPI_ENGINE_SOURCE].DBO.[HISTORICAL_MEASURE_CITIZENSHIP]
UNPIVOT
(
	Score
	FOR Measure in ([Measure 1 2019Q1],
[Measure 1 2019Q2],
[Measure 1 2019Q3],
[Measure 1 2019Q4],
[Measure 52 2019Q1],
[Measure 52 2019Q2],
[Measure 52 2019Q3],
[Measure 52 2019Q4])
) AS Measure_UNpivot



INSERT INTO [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
(
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[SPECIALTY]
      ,[PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[NUM_COUNT]
      ,[DEN_COUNT]
      ,[EXCL_COUNT]
      ,[REXCL_COUNT]
      ,[GAPS]
      ,[RESULT]
      ,[ReportType]
	  ,[REPORT_ID]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT 
	   [PROVIDER_ID]
      ,[PROVIDER_ID] AS [PCP_NPI]
      ,NULL AS [PCP_NAME]
      ,NULL AS [SPECIALTY]
      ,NULL AS [PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,'UHN_HISTORICAL_CITIZENSHIP' [MEASURE_TYPE]
      ,NULL AS [NUM_COUNT]
      ,NULL AS [DEN_COUNT]
      ,NULL [EXCL_COUNT]
      ,NULL [REXCL_COUNT]
      ,NULL AS [GAPS]
      ,SCORE AS [RESULT]
	  ,'Physician' AS [ReportType]
      ,1 AS [REPORT_ID]
	  ,CONVERT(DATE,GETDATE()) AS [REPORT_RUN_DATE]
      ,QUARTER AS [Report_Quarter]
      ,convert(date,LEFT(QUARTER,4)+'-01-01') AS [PERIOD_START_DATE]
      ,DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),4) + '-' + CAST(RIGHT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),1)*3 AS VARCHAR) +'-01' AS DATE)))  AS [PERIOD_END_DATE]
      ,159 as [ROOT_COMPANIES_ID]
FROM #TEMP_HISTORICAL_MEASURE



INSERT INTO [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
(
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[SPECIALTY]
      ,[PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[NUM_COUNT]
      ,[DEN_COUNT]
      ,[EXCL_COUNT]
      ,[REXCL_COUNT]
      ,[GAPS]
      ,[RESULT]
      ,[ReportType]
	  ,[REPORT_ID]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT 
	   NULL AS [PROVIDER_ID]
      ,NULL AS [PCP_NPI]
      ,NULL AS [PCP_NAME]
      ,NULL AS [SPECIALTY]
      ,[Practice Name] AS [PRACTICE_NAME]
      ,Measure AS [Measure_ID ]
      ,'Measure '+Measure AS [MEASURE_NAME]
      ,'UHN_HISTORICAL_EOC' [MEASURE_TYPE]
      ,NULL AS [NUM_COUNT]
      ,NULL AS [DEN_COUNT]
      ,NULL [EXCL_COUNT]
      ,NULL [REXCL_COUNT]
      ,NULL AS [GAPS]
      ,CASE WHEN LTRIM(RTRIM([2019 Score]))='-' then '0.00' else REPLACE(LTRIM(RTRIM([2019 Score])),'%','') END AS [RESULT]
	  ,'Practice' AS [ReportType]
      ,1 AS [REPORT_ID]
	  ,CONVERT(DATE,GETDATE()) AS [REPORT_RUN_DATE]
      ,'2019 - Q4' AS [Report_Quarter]
      ,'2019-01-01' AS [PERIOD_START_DATE]
      ,'2019-12-31'  AS [PERIOD_END_DATE]
      ,159 as [ROOT_COMPANIES_ID]
from [KPI_ENGINE_SOURCE]..[HISTORICAL_MEASURE_EOC]


EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE_IP'
SELECT 
[Measure Name],Measure,QUARTER,
CASE WHEN LTRIM(RTRIM(Score))='ND' then '0.00' else LTRIM(RTRIM(Score)) END AS Score
into #TEMP_HISTORICAL_MEASURE_IP
FROM [KPI_ENGINE_SOURCE].[dbo].[HISTORICAL_MEASURE_IP_HOSPITAL]
UNPIVOT
(
	Score
	FOR QUARTER in (
[2017Q1]
,[2017Q2]
,[2017Q3]
,[2017Q4]
,[2018Q1]
,[2018Q2]
,[2018Q3]
,[2018Q4]
,[2019Q1]
,[2019Q2]
,[2019Q3]
,[2019Q4]
,[2020Q1]
,[2020Q2]
,[2020Q3]
,[2020Q4])
) AS Measure_UNpivot


INSERT INTO [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
(
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[SPECIALTY]
      ,[PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[NUM_COUNT]
      ,[DEN_COUNT]
      ,[EXCL_COUNT]
      ,[REXCL_COUNT]
      ,[GAPS]
      ,[RESULT]
      ,[ReportType]
	  ,[REPORT_ID]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT 
	   NULL AS [PROVIDER_ID]
      ,NULL AS [PCP_NPI]
      ,NULL AS [PCP_NAME]
      ,NULL AS [SPECIALTY]
      ,NULL AS [PRACTICE_NAME]
      ,Measure AS [Measure]
      ,[Measure Name] [MEASURE_NAME]
      ,'UHN_HISTORICAL_IP' [MEASURE_TYPE]
      ,NULL AS [NUM_COUNT]
      ,NULL AS [DEN_COUNT]
      ,NULL [EXCL_COUNT]
      ,NULL [REXCL_COUNT]
      ,NULL AS [GAPS]
      ,SCORE AS [RESULT]
	  ,'NETWORK' AS [ReportType]
      ,1 AS [REPORT_ID]
	  ,CONVERT(DATE,GETDATE()) AS [REPORT_RUN_DATE]
      ,LEFT(QUARTER,4)+' - '+ RIGHT(QUARTER,2) AS [Report_Quarter]
      ,convert(date,LEFT(QUARTER,4)+'-01-01') AS [PERIOD_START_DATE]
      ,DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),4) + '-' + CAST(RIGHT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),1)*3 AS VARCHAR) +'-01' AS DATE)))  AS [PERIOD_END_DATE]
      ,159 as [ROOT_COMPANIES_ID]
FROM #TEMP_HISTORICAL_MEASURE_IP


EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE_PatientSatisfaction'
SELECT 
Measure,QUARTER,
REPLACE(LTRIM(RTRIM(Score)),'%','') AS Score
into #TEMP_HISTORICAL_MEASURE_PatientSatisfaction
FROM [KPI_ENGINE_SOURCE].[dbo].[HISTORICAL_MEASURE_PatientSatisfaction]
UNPIVOT
(
	Score
	FOR QUARTER in (
[2017Q1]
,[2017Q2]
,[2017Q3]
,[2017Q4]
,[2018Q1]
,[2018Q2]
,[2018Q3]
,[2018Q4]
,[2019Q1]
,[2019Q2]
,[2019Q3]
,[2019Q4]
,[2020Q1]
,[2020Q2]
,[2020Q3])
) AS Measure_UNpivot


INSERT INTO [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
(
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[SPECIALTY]
      ,[PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[NUM_COUNT]
      ,[DEN_COUNT]
      ,[EXCL_COUNT]
      ,[REXCL_COUNT]
      ,[GAPS]
      ,[RESULT]
      ,[ReportType]
	  ,[REPORT_ID]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT 
	   NULL AS [PROVIDER_ID]
      ,NULL AS [PCP_NPI]
      ,NULL AS [PCP_NAME]
      ,NULL AS [SPECIALTY]
      ,NULL AS [PRACTICE_NAME]
      ,Measure AS [Measure]
      ,'Measure '+Measure AS [MEASURE_NAME]
      ,'UHN_HISTORICAL_PatientSatisfaction' [MEASURE_TYPE]
      ,NULL AS [NUM_COUNT]
      ,NULL AS [DEN_COUNT]
      ,NULL [EXCL_COUNT]
      ,NULL [REXCL_COUNT]
      ,NULL AS [GAPS]
      ,SCORE AS [RESULT]
	  ,'NETWORK' AS [ReportType]
      ,1 AS [REPORT_ID]
	  ,CONVERT(DATE,GETDATE()) AS [REPORT_RUN_DATE]
      ,LEFT(QUARTER,4)+' - '+ RIGHT(QUARTER,2) AS [Report_Quarter]
      ,convert(date,LEFT(QUARTER,4)+'-01-01') AS [PERIOD_START_DATE]
      ,DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),4) + '-' + CAST(RIGHT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),1)*3 AS VARCHAR) +'-01' AS DATE)))  AS [PERIOD_END_DATE]
      ,159 as [ROOT_COMPANIES_ID]
FROM #TEMP_HISTORICAL_MEASURE_PatientSatisfaction



EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE_90DaysReadmission'
SELECT 
Measure,QUARTER,
REPLACE(LTRIM(RTRIM(Score)),'%','') AS Score
into #TEMP_HISTORICAL_MEASURE_90DaysReadmission
FROM [KPI_ENGINE_SOURCE].[dbo].[HISTORICAL_MEASURE_90DaysReadmission]
UNPIVOT
(
	Score
	FOR QUARTER in (
[2018Q4]
,[2019Q1]
,[2019Q2]
,[2019Q3]
,[2019Q4]
,[2020Q1]
,[2020Q2]
,[2020Q3])
) AS Measure_UNpivot

INSERT INTO [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
(
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[SPECIALTY]
      ,[PRACTICE_NAME]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[NUM_COUNT]
      ,[DEN_COUNT]
      ,[EXCL_COUNT]
      ,[REXCL_COUNT]
      ,[GAPS]
      ,[RESULT]
      ,[ReportType]
	  ,[REPORT_ID]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT 
	   NULL AS [PROVIDER_ID]
      ,NULL AS [PCP_NPI]
      ,NULL AS [PCP_NAME]
      ,NULL AS [SPECIALTY]
      ,NULL AS [PRACTICE_NAME]
      ,Measure AS [Measure]
      ,'Measure '+Measure AS [MEASURE_NAME]
      ,'UHN_HISTORICAL_90DaysReadmission' [MEASURE_TYPE]
      ,NULL AS [NUM_COUNT]
      ,NULL AS [DEN_COUNT]
      ,NULL [EXCL_COUNT]
      ,NULL [REXCL_COUNT]
      ,NULL AS [GAPS]
      ,SCORE AS [RESULT]
	  ,'NETWORK' AS [ReportType]
      ,1 AS [REPORT_ID]
	  ,CONVERT(DATE,GETDATE()) AS [REPORT_RUN_DATE]
      ,LEFT(QUARTER,4)+' - '+ RIGHT(QUARTER,2) AS [Report_Quarter]
      ,convert(date,LEFT(QUARTER,4)+'-01-01') AS [PERIOD_START_DATE]
      ,DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(LEFT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),4) + '-' + CAST(RIGHT(LEFT(QUARTER,4)+RIGHT(QUARTER,1),1)*3 AS VARCHAR) +'-01' AS DATE)))  AS [PERIOD_END_DATE]
      ,159 as [ROOT_COMPANIES_ID]
FROM #TEMP_HISTORICAL_MEASURE_90DaysReadmission





EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE'
EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE_IP'
EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE_PatientSatisfaction'
EXEC SP_KPI_DROPTABLE '#TEMP_HISTORICAL_MEASURE_90DaysReadmission'

END
GO
