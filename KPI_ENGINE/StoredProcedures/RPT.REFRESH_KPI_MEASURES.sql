SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [RPT].[REFRESH_KPI_MEASURES]
AS
BEGIN
/******
Change Log: 
	2021-11-15 | KPISQL-524 | Cast EMPI to VARCHAR(MAX) wherever a table is joined with ConsolidatedAttribution_Snapshot based on EMPI
******/

DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY




IF OBJECT_ID(N'RPT.FINANCIALS_TEMP', N'U') IS NOT NULL 
drop table RPT.FINANCIALS_TEMP
Select EMPI,Year(FROM_DATE)*100+Month(FROM_DATE) as YEARMO,CL_DATA_SRC,SV_STAT,FORM_TYPE,COUNT(distinct CLAIM_ID) as CLAIMLINES,SUM(AMT_BILLED) AS BILLED,SUM(AMT_ALLOWED) AS ALLOWED,SUM(AMT_PAID) AS PAID,
SUM(AMT_COINS) COINSURANCE,SUM(AMT_COPAY) COPAY,SUM(AMT_DEDUCT) as DEDUCTIBLE,ROOT_COMPANIES_ID
INTO RPT.FINANCIALS_TEMP
FROM DBO.CLAIMLINE where CL_DATA_SRC in (select Distinct Payer from [dbo].[STAGING_TO_PROD_DEDUP_KEY] where PROD_TABLE='CLAIMLINE' and HEADER_LINE='LINE')
GROUP by EMPI,Year(FROM_DATE)*100+Month(FROM_DATE),CL_DATA_SRC,SV_STAT,FORM_TYPE,ROOT_COMPANIES_ID



INSERT INTO RPT.FINANCIALS_TEMP(EMPI,YEARMO,CL_DATA_SRC,SV_STAT,FORM_TYPE,CLAIMLINES,BILLED,ALLOWED,PAID,ROOT_COMPANIES_ID)
Select EMPI,Year(FROM_DATE)*100+Month(FROM_DATE) as YEARMO,CH_DATA_SRC,SV_STAT,'U' FORM_TYPE,COUNT(distinct CLAIM_ID) as CLAIMLINES,SUM(AMT_PAID) AS BILLED,0 as Allowed,SUM(AMT_PAID) AS PAID,ROOT_COMPANIES_ID
FROM DBO.CLAIMHEADER 
	where CH_DATA_SRC in (select Distinct Payer from [dbo].[STAGING_TO_PROD_DEDUP_KEY] where PROD_TABLE='CLAIMLINE' and HEADER_LINE='HEADER')
	-- adding this condition to only pull Institutional claim amounts from CLAIMHEADER
	and FORM_TYPE='U'
GROUP by EMPI,Year(FROM_DATE)*100+Month(FROM_DATE),CH_DATA_SRC,SV_STAT,ROOT_COMPANIES_ID

insert INTO RPT.FINANCIALS_TEMP(EMPI,YEARMO,CL_DATA_SRC,SV_STAT,FORM_TYPE,CLAIMLINES,BILLED,ALLOWED,PAID,COINSURANCE,COPAY,DEDUCTIBLE,ROOT_COMPANIES_ID)
Select EMPI,Year(FROM_DATE)*100+Month(FROM_DATE) as YEARMO,CL_DATA_SRC,SV_STAT,FORM_TYPE,COUNT(distinct CLAIM_ID) as CLAIMLINES,SUM(AMT_BILLED) AS BILLED,SUM(AMT_ALLOWED) AS ALLOWED,SUM(AMT_PAID) AS PAID,
SUM(AMT_COINS) COINSURANCE,SUM(AMT_COPAY) COPAY,SUM(AMT_DEDUCT) as DEDUCTIBLE,ROOT_COMPANIES_ID
FROM DBO.CLAIMLINE where FORM_TYPE!='U' and CL_DATA_SRC in (select Distinct Payer from [dbo].[STAGING_TO_PROD_DEDUP_KEY] where PROD_TABLE='CLAIMLINE' and HEADER_LINE='HEADER')
GROUP by EMPI,Year(FROM_DATE)*100+Month(FROM_DATE),CL_DATA_SRC,SV_STAT,FORM_TYPE,ROOT_COMPANIES_ID





IF OBJECT_ID(N'RPT.FINANCIALS_NEW', N'U') IS NOT NULL
drop table RPT.FINANCIALS_NEW
select 
a.AmbulatoryPCPPractice as PracticeName
,a.AmbulatoryPCPName as AttributedProviderName
,a.AmbulatoryPCPSpecialty as Specialty
,a.AmbulatoryPCPNPI as NPI 
,F.EMPI
,YEARMO
,CL_DATA_SRC
,SV_STAT
,FORM_TYPE
,CLAIMLINES
,BILLED
,ALLOWED
,PAID
,COINSURANCE
,COPAY
,DEDUCTIBLE
,F.ROOT_COMPANIES_ID
INTO RPT.FINANCIALS_NEW
from RPT.FINANCIALS_TEMP F
join RPT.PCP_ATTRIBUTION A
ON F.EMPI=A.EMPI and A.ReportId in (select max(ReportId) from RPT.PCP_ATTRIBUTION A)



CREATE NONCLUSTERED INDEX idx_FINANCIALS_NEW_CL_DATA_SRC_ROOT_COMPANIES_ID_YEARMO
ON [RPT].[FINANCIALS_NEW] ([CL_DATA_SRC],[ROOT_COMPANIES_ID],[YEARMO])
INCLUDE ([PAID],EMPI)

CREATE NONCLUSTERED INDEX idx_FINANCIALS_NEW_ROOT_COMPANIES_ID_YEARMO
ON [RPT].[FINANCIALS_NEW] ([ROOT_COMPANIES_ID],[YEARMO])
INCLUDE ([CL_DATA_SRC],[PAID])

CREATE NONCLUSTERED INDEX idx_FINANCIALS_NEW_FORM_TYPE_ROOT_COMPANIES_ID_YEARMO
ON [RPT].[FINANCIALS_NEW] ([FORM_TYPE],[ROOT_COMPANIES_ID],[YEARMO])
INCLUDE ([CL_DATA_SRC],[PAID],EMPI)



/*
IF OBJECT_ID(N'RPT.FINANCIALS', N'U') IS NOT NULL  
DROP TABLE RPT.FINANCIALS

Select Year(FROM_DATE)*100+Month(FROM_DATE) as YEARMO,CL_DATA_SRC,SV_STAT,FORM_TYPE,COUNT(distinct CLAIM_ID) as CLAIMLINES,SUM(AMT_BILLED) AS BILLED,SUM(AMT_ALLOWED) AS ALLOWED,SUM(AMT_PAID) AS PAID,
SUM(AMT_COINS) COINSURANCE,SUM(AMT_COPAY) COPAY,SUM(AMT_DEDUCT) as DEDUCTIBLE,ROOT_COMPANIES_ID
INTO RPT.FINANCIALS
FROM DBO.CLAIMLINE
GROUP by Year(FROM_DATE)*100+Month(FROM_DATE),CL_DATA_SRC,SV_STAT,FORM_TYPE,ROOT_COMPANIES_ID
*/



IF OBJECT_ID(N'RPT.MEMBER_MONTH', N'U') IS NOT NULL  
DROP TABLE RPT.MEMBER_MONTH

Select MEMBER_MONTH_START_DATE as START_DATE,MM_DATA_SRC,SUM(MM_UNITS) AS MEMBER_MONTH
Into RPT.MEMBER_MONTH
from dbo.MEMBER_MONTH
group by MEMBER_MONTH_START_DATE,MM_DATA_SRC






--Select YearMo,AVG(PMPM)
--FROM
--(
--Select YearMo,CL_DATA_SRC,PAID,MM.MEMBER_MONTH,PAID/MM.MEMBER_MONTH as PMPM
--FROM
--(
--select YearMo,CL_DATA_SRC,SUM(PAID) PAID 
--from RPT.FINANCIALS
--where SV_STAT IN ('O','A','P','R')
--group by YearMo,CL_DATA_SRC
--having YEARMO>=CASE WHEN CL_DATA_SRC='BCBS-UHS' THEN '202001' ELSE CASE WHEN CL_DATA_SRC in ('BRIGHT','CIGNA') THEN '201901' ELSE CASE WHEN CL_DATA_SRC='HUMANA' THEN '201701' ELSE CASE WHEN CL_DATA_SRC='MSSP' THEN '201501' END END END END
--and SUM(PAID)>0
--) CL
--Inner join 
--RPT.MEMBER_MONTH MM
--on CL.YEARMO=YEAR(MM.START_DATE)*100+month(MM.START_DATE) and Cl.CL_DATA_SRC=MM.MM_DATA_SRC
--) T
--group by YearMo
--order by YearMo

/*
*******************************************************************************************************
********************************************UTILIZATION--**********************************************
*******************************************************************************************************
*/


/*
IF OBJECT_ID(N'RPT.UHN_UTILZATION', N'U') IS NOT NULL  
DROP TABLE RPT.UHN_UTILZATION

select 'IP_PER_1K' as Metrix, LEFT(SV_DATE,4) as SV_YEAR,AVG(UTILS_PER_1K) UTILIZATION
INTO RPT.UHN_UTILZATION
FROM
(
Select 'IP_PER_1k' AS matrix,SV_DATE,(Utils*1000)/MEM_MNTHS AS UTILS_PER_1K FROM
(
Select Year(FROM_DATE)*100+month(from_date) as SV_DATE,COUNT(Distinct MEMBER_ID+CONVERT(CHAR(10),FROM_DATE)) AS Utils
from dbo.CLAIMLINE 
Where (CASE WHEN POS IN ('','  ','*','I','O','OH','XX') THEN 00 END In (13,14,21,25,26,31,32,33,34,35,51,55,56,61,65) Or 
(REPLACE(replace(REV_CODE,'',0000),'000*',0000) In (22,24,100,101,164,179,180,219) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 110 And 162)
Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 166 And 175) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 182 And 185)
Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 189 And 194) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 199 And 204)
Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 206 And 214) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 1000 And 1006)))
And root_companies_id='159'
group by Year(FROM_DATE)*100+month(from_date)
)T
Inner join
(
Select YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) as MM_DATE,SUM(MM_UNITS) as MEM_MNTHS
from dbo.MEMBER_MONTH
GROUP by YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) 
) T1 ON --T.CL_DATA_SRC=t1.MM_DATA_SRC AND 
T.SV_DATE=T1.MM_DATE
--order by SV_DATE
) O
group by LEFT(SV_DATE,4)
order by LEFT(SV_DATE,4)





INSERT INTO RPT.UHN_UTILZATION(Metrix, SV_YEAR,UTILIZATION)
select 'OP_PER_1K' as Metrix, LEFT(SV_DATE,4) as SV_YEAR,AVG(UTILS_PER_1K)
FROM
(
Select 'OP_PER_1k' AS matrix,SV_DATE,(Utils*1000)/MEM_MNTHS AS UTILS_PER_1K FROM
(
Select Year(FROM_DATE)*100+month(from_date) as SV_DATE,COUNT(Distinct MEMBER_ID+CONVERT(CHAR(10),FROM_DATE)) AS Utils
from dbo.CLAIMLINE 
Where (CASE when POS in ('  ','*' ,'I' ,'O' ,'OH','XX') then 00 end In (19,22,23,24,41,42,52,53) 
OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) IN ('523','526','539','569','650') OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 220 And 521)
OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 528 And 531) OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 550 And 552)
OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 559 And 562) OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 600 And 637)
OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 655 And 659) OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 670 And 953)
OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 990 And 999) OR (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 2100 And 3109)))
And root_companies_id='159'
group by Year(FROM_DATE)*100+month(from_date)
)T
Inner join
(
Select YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) as MM_DATE,SUM(MM_UNITS) as MEM_MNTHS
from dbo.MEMBER_MONTH
GROUP by YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) 
) T1 ON --T.CL_DATA_SRC=t1.MM_DATA_SRC AND 
T.SV_DATE=T1.MM_DATE
--order by SV_DATE
) O
group by LEFT(SV_DATE,4)
order by LEFT(SV_DATE,4)



--------------------------------------ED-------------------------------------------------------
INSERT INTO RPT.UHN_UTILZATION(Metrix, SV_YEAR,UTILIZATION)
select 'ED_PER_1K' as Metrix, LEFT(SV_DATE,4) as SV_YEAR,AVG(UTILS_PER_1K) Utilization
FROM
(
Select 'ED_PER_1k' AS matrix,SV_DATE,(Utils*1000)/MEM_MNTHS AS UTILS_PER_1K FROM
(
Select Year(FROM_DATE)*100+month(from_date) as SV_DATE,COUNT(Distinct MEMBER_ID+CONVERT(CHAR(10),FROM_DATE)) AS Utils
from dbo.CLAIMLINE 
Where (POS ='23' Or 
(REPLACE(replace(REV_CODE,'',0000),'000*',0000) in ('0450','0451','0452','0453','0454','0455','0456','0457','0458','0459','0981'))) 
And root_companies_id='159'
group by Year(FROM_DATE)*100+month(from_date)
)T
Inner join
(
Select YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) as MM_DATE,SUM(MM_UNITS) as MEM_MNTHS
from dbo.MEMBER_MONTH
GROUP by YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) 
) T1 ON --T.CL_DATA_SRC=t1.MM_DATA_SRC AND 
T.SV_DATE=T1.MM_DATE
--order by SV_DATE
) O
group by LEFT(SV_DATE,4)
order by LEFT(SV_DATE,4)

--------------------------------------UR-------------------------------------------------------
INSERT INTO RPT.UHN_UTILZATION(Metrix, SV_YEAR,UTILIZATION)
select 'UR_PER_1K' as Metrix,LEFT(SV_DATE,4) as SV_YEAR,AVG(UTILS_PER_1K)
FROM
(
Select 'UR_PER_1k' AS matrix,SV_DATE,(Utils*1000)/MEM_MNTHS AS UTILS_PER_1K FROM
(
Select Year(FROM_DATE)*100+month(from_date) as SV_DATE,COUNT(Distinct MEMBER_ID+CONVERT(CHAR(10),FROM_DATE)) AS Utils
from dbo.CLAIMLINE 
Where POS ='20'
--And root_companies_id='159'
group by Year(FROM_DATE)*100+month(from_date)
)T
Inner join
(
Select YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) as MM_DATE,SUM(MM_UNITS) as MEM_MNTHS
from dbo.MEMBER_MONTH
GROUP by YEAR(MEMBER_MONTH_START_DATE)*100+Month(MEMBER_MONTH_START_DATE) 
) T1 ON --T.CL_DATA_SRC=t1.MM_DATA_SRC AND 
T.SV_DATE=T1.MM_DATE
--order by SV_DATE
) O
group by LEFT(SV_DATE,4)
order by LEFT(SV_DATE,4)
*/

IF OBJECT_ID(N'RPT.UHN_UTILZATION_TEMP', N'U') IS NOT NULL  
DROP TABLE RPT.UHN_UTILZATION_TEMP

------------------------------------------------IP--------------------------------------------


--Get admissions

Drop table if exists #admissions;
Select
	*
into #admissions
From
(
		select 
			c.EMPI
			,REV_CODE as Code
			,coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
			, DIS_DATE
			,'Admission' as TypeofEncounter
			,ROW_NUMBER() over(Partition By c.EMPI,ADM_DATE order by DIS_DATE desc) as rn
			From CLAIMLINE c
		where
			ROOT_COMPANIES_ID=159 and
			ISNULL(POS,0)!='81' and
			REV_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay','Nonacute Inpatient Stay','Nonacute Inpatient Stay Other Than Behavioral Health Accommodations'/*,'Observation Stay'*/)
					-- Commenting observation stay on Heathers request on 07-15-2021
			)
				--and c.EMPI='1482A64B-E0E6-416C-9FC5-8C610388C829'
	)tbl
	Where rn=1


-- Identify Direct Transfers
drop table if exists #mergedstays
;with grp_starts as 
(
  select distinct
	EMPI
	,ADM_DATE
	,DIS_DATE
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #admissions

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Select 
	EMPI
	,AdmissionDate
	,DischargeDate
into #mergedstays
from	
(
	select 
		EMPI
		,min(ADM_DATE) as AdmissionDate
		,max(DIS_DATE) as DischargeDate
	from grps 
	group by 
		EMPI,grp
		
)t1
	


select 
	s.EMPI,
	Year(AdmissionDate)*100+month(AdmissionDate) as SV_DATE,
	'IP' AS Service_Line_Category,
	a.Payer as CL_DATA_SRC,
	COUNT(Distinct concat(s.EMPI,AdmissionDate)) AS Utils,
	159 as root_companies_id
INTO RPT.UHN_UTILZATION_TEMP
from #mergedstays s
Join RPT.ConsolidatedAttribution_Snapshot a on
-- KPISQL-524 | Cast EMPI to VARCHAR(MAX) wherever a table is joined with ConsolidatedAttribution_Snapshot based on EMPI
	--s.EMPI=a.EMPI and
	CAST(s.EMPI AS VARCHAR(MAX))=a.EMPI and
	a.Attribution_type='Ambulatory_PCP'
group by 
	s.EMPI,Year(AdmissionDate)*100+month(AdmissionDate),a.Payer
Order by 1 desc


/*
select 
	EMPI,
	Year(FROM_DATE)*100+month(from_date) as SV_DATE,
	'IP' AS Service_Line_Category,
	CL_DATA_SRC,
	COUNT(Distinct MEMBER_ID+CONVERT(CHAR(10),FROM_DATE)) AS Utils,
	root_companies_id
--INTO RPT.UHN_UTILZATION_TEMP
from dbo.CLAIMLINE
Where (CASE WHEN POS IN ('','  ','*','I','O','OH','XX') THEN 00 END In (13,14,21,25,26,31,32,33,34,35,51,55,56,61,65) Or 
(REPLACE(replace(REV_CODE,'',0000),'000*',0000) In (22,24,100,101,164,179,180,219) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 110 And 162)
Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 166 And 175) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 182 And 185)
Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 189 And 194) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 199 And 204)
Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 206 And 214) Or (REPLACE(replace(REV_CODE,'',0000),'000*',0000) Between 1000 And 1006)))
group by EMPI,Year(FROM_DATE)*100+month(from_date),CL_DATA_SRC,root_companies_id
*/


------------------------------------------------OP--------------------------------------------

Drop table if exists #outpatientvisits
Select distinct
	EMPI,
	PROC_START_DATE
into #outpatientvisits
From
(

	Select
		EMPI,
		PROC_START_DATE
	From GetProcedures('159','1900-01-01','3000-01-01','Ambulatory Outpatient Visits,Outpatient')

	Union all

	Select
		EMPI,
		FROM_DATE
	From CLAIMLINE
	Where
		ROOT_COMPANIES_ID=159 and
		ISNULL(POS,'0') in(Select code from HDS.Valueset_to_code where value_set_name='Outpatient POS')

	Union all

	Select
		EMPI,
		FROM_DATE
	FROM CLAIMLINE
	Where
		ROOT_COMPANIES_ID=159 and
		ISNULL(POS,'0')!='81' and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in ('Outpatient')
		)
)t1


INSERT INTO RPT.UHN_UTILZATION_TEMP(EMPI,SV_DATE,Service_Line_Category,CL_DATA_SRC,Utils,root_companies_id)
select 
	o.EMPI,
	Year(PROC_START_DATE)*100+month(PROC_START_DATE) as SV_DATE,
	'OP' AS Service_Line_Category,
	s.Payer as CL_DATA_SRC,
	COUNT(Distinct concat(o.EMPI,PROC_START_DATE)) AS Utils,
	s.root_companies_id
from #outpatientvisits o
Join RPT.ConsolidatedAttribution_Snapshot s on
-- KPISQL-524 | Cast EMPI to VARCHAR(MAX) wherever a table is joined with ConsolidatedAttribution_Snapshot based on EMPI
	--o.EMPI=s.EMPI and
	CAST(o.EMPI AS VARCHAR(MAX))=s.EMPI and
	s.Attribution_type='Ambulatory_PCP'
group by 
	o.EMPI,Year(PROC_START_DATE)*100+month(PROC_START_DATE),Payer,root_companies_id


------------------------------------------------ED--------------------------------------------

-- ED Visits Calculation

-- Get ED Visits list
Drop table if exists #edu_edvisits;
select distinct 
	EMPI
	,FROM_DATE
	,CLAIM_ID 
into #edu_edvisits
from
(
	
	Select
		c.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,p.PROC_CODE as Code
		,c.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on 
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		c.ROOT_COMPANIES_ID=159 and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,0)!='81' and
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		p.PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('ED')
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
	--	and p.EMPi=95013

	Union all
	

	Select
		c.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,c.POS as Code
		,c.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on 
		p.CLAIM_ID=c.CLAIM_ID and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=C.EMPI
	Where
		c.ROOT_COMPANIES_ID=159 and
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		ISNULL(c.POS,'0') in
		(
			select code from HDS.VALUESET_TO_CODE where value_set_name='ED POS'
		)
		and
		p.PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('ED Procedure Code')
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')

	Union all

	Select
		c.EMPI
		,c.FROM_DATE
		,REV_CODE as Code
		,c.CLAIM_ID
	From CLAIMLINE c	
	Where
		c.ROOT_COMPANIES_ID=159 and
		c.CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		c.REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='ED'
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
		--and EMPI=95014
	
)t1





-- Observation Stay and Inpatient Stay
Drop table if exists #edu_stayexclusionlist;
Create table #edu_stayexclusionlist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
)
insert into #edu_stayexclusionlist
Select distinct
	EMPI
	,FROM_DATE
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
From
(
	Select distinct
		EMPI
		,FROM_DATE
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
	From inpatientstays('159','1900-01-01','3000-01-01')
	Where
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) 
)t1



--Exclusion Set - Mental and Behavioral Disorders,	Psychiatry ,	Electroconvulsive therapy
Drop table if exists #edu_encounterexclusionlist;
Create table #edu_encounterexclusionlist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
)
Insert into #edu_encounterexclusionlist
Select distinct
	EMPI
	,FROM_DATE
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
From
(

	Select
		d.EMPI
		,d.DIAG_START_DATE as FROM_DATE
		,Coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
		,Coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
		,d.CLAIM_ID
	From diagnosis d
	Join CLAIMLINE c on
		d.CLAIM_ID=c.CLAIM_ID and
		d.DIAG_DATA_SRC=c.CL_DATA_SRC and
		d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		d.EMPI=c.EMPI
	Where
		d.ROOT_COMPANIES_ID=159 and
		d.DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		DIAG_SEQ_NO=1 and
		DIAG_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Mental and Behavioral Disorders'
		)


	Union all

	Select
		p.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,Coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
		,Coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
		,p.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		p.ROOT_COMPANIES_ID=159 and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		(
			PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Psychiatry','Electroconvulsive Therapy')
			)
			or
			ICDPCS_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Electroconvulsive Therapy')
			)
		)
)t1




-- Ed Visit Count by member
drop table if exists #EDvisits;
select distinct
	ed.EMPI,
	ed.FROM_DATE
into #EDvisits
from #edu_edvisits ed -- where memid=96248
left outer join #edu_encounterexclusionlist enx on 
	ed.EMPI=enx.EMPI and 
	ed.CLAIM_ID=enx.CLAIM_ID
left outer join #edu_stayexclusionlist stx on 
	ed.EMPI=stx.EMPI and 
	(
		ed.CLAIM_ID=stx.CLAIM_ID 
		or 
		ed.FROM_DATE between dateadd(day,-1,stx.ADM_DATE) and stx.DIS_DATE
	)
where 
	enx.EMPI is null and 
	stx.EMPI is null 
			--and ed.EMPI=96465
	



INSERT INTO RPT.UHN_UTILZATION_TEMP(EMPI,SV_DATE,Service_Line_Category,CL_DATA_SRC,Utils,root_companies_id)
select 
	e.EMPI,
	Year(FROM_DATE)*100+month(from_date) as SV_DATE,
	'ED' AS Service_Line_Category,
	a.Payer as CL_DATA_SRC,
	COUNT(Distinct concat(e.EMPI,FROM_DATE)) AS Utils,
	a.root_companies_id
from #EDvisits e
Join RPT.ConsolidatedAttribution_Snapshot a on
-- KPISQL-524 | Cast EMPI to VARCHAR(MAX) wherever a table is joined with ConsolidatedAttribution_Snapshot based on EMPI
	--e.EMPI=a.EMPI and
	CAST(e.EMPI AS VARCHAR(MAX))=a.EMPI and
	a.Attribution_Type='Ambulatory_PCP'
group by e.EMPI,Year(FROM_DATE)*100+month(from_date),a.Payer,a.root_companies_id

------------------------------------------------UR--------------------------------------------
INSERT INTO RPT.UHN_UTILZATION_TEMP(EMPI,SV_DATE,Service_Line_Category,CL_DATA_SRC,Utils,root_companies_id)
select 
	c.EMPI,
	Year(FROM_DATE)*100+month(from_date) as SV_DATE,
	'UR' AS Service_Line_Category,
	a.Payer as CL_DATA_SRC,
	COUNT(Distinct concat(c.EMPI,FROM_DATE)) AS Utils,
	c.root_companies_id
from dbo.CLAIMLINE c
Join RPT.ConsolidatedAttribution_Snapshot a on
-- KPISQL-524 | Cast EMPI to VARCHAR(MAX) wherever a table is joined with ConsolidatedAttribution_Snapshot based on EMPI
	--c.EMPI=a.EMPI and
	CAST(c.EMPI AS VARCHAR(MAX))=a.EMPI and
	a.Attribution_Type='Ambulatory_PCP'
Where 
	c.ROOT_COMPANIES_ID=159 and
	ISNULL(POS,'') ='20'
group by c.EMPI,Year(FROM_DATE)*100+month(from_date),a.Payer,c.root_companies_id

------------------------------------------------RX--------------------------------------------
INSERT INTO RPT.UHN_UTILZATION_TEMP(EMPI,SV_DATE,Service_Line_Category,CL_DATA_SRC,Utils,root_companies_id)
select 
	EMPI,
	Year(FROM_DATE)*100+month(from_date) as SV_DATE,
	'RX' AS Service_Line_Category,
	CL_DATA_SRC,
	COUNT(Distinct MEMBER_ID+CONVERT(CHAR(10),FROM_DATE)) AS Utils,
	root_companies_id
from dbo.CLAIMLINE
Where 
	(FORM_TYPE='D' or (CASE when POS in ('  ','*' ,'I' ,'O' ,'OH','XX') then 00 end In (01)))
group by EMPI,Year(FROM_DATE)*100+month(from_date),CL_DATA_SRC,root_companies_id



IF OBJECT_ID(N'RPT.UHN_UTILZATION_NEW', N'U') IS NOT NULL
drop table RPT.UHN_UTILZATION_NEW
select 
a.AmbulatoryPCPPractice as PracticeName
,a.AmbulatoryPCPName as AttributedProviderName
,a.AmbulatoryPCPSpecialty as Specialty
,a.AmbulatoryPCPNPI as NPI 
,F.EMPI
,F.SV_DATE
,F.Service_Line_Category
,F.CL_DATA_SRC
,Utils
,F.ROOT_COMPANIES_ID
INTO RPT.UHN_UTILZATION_NEW
from RPT.UHN_UTILZATION_TEMP F
join RPT.PCP_ATTRIBUTION A
ON F.EMPI=A.EMPI and A.ReportId in (select max(ReportId) from RPT.PCP_ATTRIBUTION A)


Create clustered index idx_RPT_UHN_UTILZATION_NEW_EMPI
on [RPT].[UHN_UTILZATION_NEW]
(EMPI)


Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_ROOT_COMPANIES_ID
on [RPT].[UHN_UTILZATION_NEW]
(ROOT_COMPANIES_ID)


Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_SV_DATE
on [RPT].[UHN_UTILZATION_NEW]
(SV_DATE)


Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_Service_Line_Category
on [RPT].[UHN_UTILZATION_NEW]
(Service_Line_Category)


Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_CL_DATA_SRC
on [RPT].[UHN_UTILZATION_NEW]
(CL_DATA_SRC)


---
Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_PracticeName
on [RPT].[UHN_UTILZATION_NEW]
(PracticeName)


Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_AttributedProviderName
on [RPT].[UHN_UTILZATION_NEW]
(AttributedProviderName)



Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_Specialty
on [RPT].[UHN_UTILZATION_NEW]
(Specialty)


Create nonclustered index idx_RPT_UHN_UTILZATION_NEW_NPI
on [RPT].[UHN_UTILZATION_NEW]
(NPI)



		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
