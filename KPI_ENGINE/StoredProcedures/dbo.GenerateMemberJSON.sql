SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Procedure [dbo].[GenerateMemberJSON] @empi varchar(100),@module varchar(50),@json nvarchar(max) OUTPUT
As

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


Declare @rootId INT=159;


/*
Drop table if exists #enrollment
;with grp_starts as 
	(
		Select
			*
			,case
				when datediff(day,lag([End Date]) over(partition by EMPI order by [Start Date], [End Date]),[End Date]) <=1 then 0 
				else 1
			end grp_start
		From
		(
			select
				EMPI
				,MEMBER_ID as [Payer Id]
				,EFF_DATE as [Start Date]
				,TERM_DATE as [End Date]
				,SUBSCRIBER_ID as [Subscriber Id]
				,Case
					When DRUG_BENEFIT=1 or RX_UNITS=1 Then 'Y'
					else 'N'
				end as [Rx Coverage]
				,rel.Relation_desc as [Insured Relationship]
				,EN_DATA_SRC as Payer
				,rank() over(Partition By EMPI order by Filedate Desc) as rn
			from ENROLLMENT en
			left outer join RFT.RelationshipCodes rel on
				en.Relation=rel.Relation
			where
				EN_DATA_SRC!='MEDCO' and
				EMPI=@empi

			
		)t1
	--	Where
	--		rn=1
	)
	, grps as 
	(
	  select 
		*
		,sum(grp_start) over(partition by EMPI order by [Start Date], [End Date]) grp
	  from grp_starts
	)
	
	Select
		*
	into #enrollment
	(
		Select
			* as json
		From
		(
			select 
				EMPI
				,[Payer Id]
				,Payer
				,min([Start Date]) as [Start Date]
				,max([End Date]) as [End Date]
				,[Rx Coverage]
				,[Insured Relationship]
	
			from grps 
			group by 
				EMPI
				,[Payer Id]
				,[Rx Coverage]
				,[Insured Relationship]
				,Payer
		)t1
		For JSON Path;
	)t2
*/


declare @enrollment nvarchar(max);
;with grp_starts as 
	(
		Select
			*
			,case
				when datediff(day,lag([End Date]) over(partition by EMPI order by [Start Date], [End Date]),[End Date]) <=1 then 0 
				else 1
			end grp_start
		From
		(
			select
				en.EMPI
				,MEMBER_ID as [Payer Id]
				,EFF_DATE as [Start Date]
				,TERM_DATE as [End Date]
				,SUBSCRIBER_ID as [Subscriber Id]
				,Case
					When DRUG_BENEFIT=1 or RX_UNITS=1 Then 'Y'
					else 'N'
				end as [Rx Coverage]
				,rel.Relation_desc as [Insured Relationship]
				,EN_DATA_SRC as Payer
				,rank() over(Partition By EMPI order by Filedate Desc) as rn
			from ENROLLMENT en
			left outer join RFT.RelationshipCodes rel on
				en.Relation=rel.Relation
			where
				EN_DATA_SRC!='MEDCO' and
				en.EMPI=@empi

			
		)t1
	--	Where
	--		rn=1
	)
	, grps as 
	(
	  select 
		*
		,sum(grp_start) over(partition by EMPI order by [Start Date], [End Date]) grp
	  from grp_starts
	)
	select @enrollment=(	
	Select * from(
		Select top 1000 
			t1.EMPI,
			[Payer Id],
			t1.Payer,
			[Start Date],
			Case
				when cs.Payer=t1.[Payer] and cs.Enrollmentstatus='Active' then 'Active'
				else cast([End Date] as varchar(20))
			end as [End Date],
			[Rx Coverage] ,
			[Insured Relationship]
		From
		(
			select 
				ISNULL(EMPI,'') as EMPI
				,ISNULL([Payer Id],'') as [Payer Id]
				,ISNULL(Payer,'') as Payer
				,ISNULL(min([Start Date]),'') as [Start Date]
				,ISNULL(max([End Date]),'') as [End Date]
				,ISNULL([Rx Coverage],'') as [Rx Coverage] 
				,ISNULL([Insured Relationship],'') as [Insured Relationship]
			from grps 
			group by 
				EMPI
				,[Payer Id]
				,[Rx Coverage]
				,[Insured Relationship]
				,Payer
		)t1
		Join rpt.ConsolidatedAttribution_Snapshot cs on cast(t1.EMPI as varchar(100))=cs.EMPI and cs.Attribution_Type='Ambulatory_PCP' and ReportId=(select top 1 ReportId from RPT.Report_Details order by ReportEndDate desc)
		Order by [Start Date] desc)t2
		For JSON PATH);
		

Set @json=Case
			When @module='Patient' Then (select 
			ISNULL(oem.EMPI_ID,'') as EMPI_ID,
			identifier=
			(
				Select DISTINCT
					ISNULL(value,'') as [identifier],ISNULL(Data_Source,'') as [Assigner]
				From
				(
					select
						EMPI_ID,
						EMPI_ID as value,
						'KPI Ninja' as Data_Source
					From open_empi
					Union all
					select
						EMPI_ID,
						Org_Patient_Extension_ID as value,
						Data_Source
					From open_empi
					Union all
					select
						EMPI_ID,
						Org_Patient_Alternative_ID as value
						,'Medicare' as Data_Source 
					From open_empi
					where 
						Org_Patient_Alternative_ID is not null
					Union all
					select
						EMPI_ID,
						Social_Security_Number as value,
						'SSN' as Data_Source
					From open_empi
					where 
						Social_Security_Number is not null
				)t1
				Where
					t1.EMPI_ID=oem.EMPI_ID
				For JSON PATH

		
			),
			ISNULL((
				select top 1
					*
				From
				(
					Select
						Case
							When Dateadd(m,-2,GETDATE()) between en.EFF_DATE and en.Term_date then 'Active'
							Else 'Inactive'
						end as Status
					from ENROLLMENT en
					Where
						en.EMPI=oem.EMPI_ID
						
				)t1
				order by 1 asc
			),'') as [activeStatus],
			ISNULL(oem.Last_Name,'') as [familyName],
			ISNULL(oem.First_Name,'') as [GivenName],
			ISNULL(oem.Middle_Name,'') as [MiddleName],
			ISNULL(oem.Email_Address,'') as [email],
			ISNULL(oem.phone_number_main,'') as [primaryContactNumber],
			ISNULL(oem.Phone_Number_Alternative,'') as [AlternateContactNumber],
			ISNULL(oem.Gender,'') as [gender],
			Format(Date_of_Birth,'MM-dd-yyyy') as [birthDate],
			Case	
				When ISNULL(oem.Date_of_Death_Flag,'N')='N' Then 'N'
				else 'Y'
			end as [deceasedBoolean],
			ISNULL(Home_Address_Line_1,'') as [AddressLine1],
			ISNULL(Home_Address_Line_2,'') as [AddressLine2],
			ISNULL(City,'') as [City],
			ISNULL(Zip_Code,'') as zipCode,
			ISNULL(coalesce(st.StateCode,oem.state),'') as [state],
			isnull(h.RiskScore,'') as [CMS-HCC Risk Score],
			ISNULL(cast(h.ClosureRate as varchar(10)),'') as [HCC Closure Rate],
			ISNULL(cs.Prov_Name,'') as PCP,
			ISNULL(cs.Payer,'') as Payer
		from Open_empi_master oem
		left outer join RFT.StateCodes st on
			oem.State=RIGHT('00'+CAST(st.NumericStateCode AS VARCHAR(2)),2)
		left outer join HCC_Patient_Summary h on
			h.EMPI=@empi
		Join RPT.ConsolidatedAttribution_Snapshot cs on oem.EMPI_ID=cs.EMPI and ReportId=(Select top 1 ReportId from RPT.Report_Details order by ReportEndDate Desc) and cs.Attribution_Type='Ambulatory_PCP'
		Where oem.EMPI_ID=@empi
		For JSON PATH)

	When @module='Enrollment' Then 
	(
		/*
		Select
			*
		From #enrollment		For JSON path*/
		
		select @enrollment
	)

	When @module='Claims' Then
	(
		Select cast((
		select distinct
			ISNULL(CLAIM_ID,'') as [Claim No],
			ISNULL(SV_LINE,'1') as [Claim Line],
			ISNULL(MEMBER_ID,'') as [Payer Id],
			Case
				When FORM_TYPE='H' Then 'Professional'
				When FORM_TYPE='U' Then 'Institutional'
				When FORM_TYPE='D' Then 'Pharmacy'
				else 'Unknown'
			end as [Claim Type],
			Case
				When ISNULL(ADM_DATE,'1900-01-01')!='1900-01-01' then cast(ADM_DATE as varchar(10))
				else ''
			End as [Admission Date],
			Case
				When ISNULL(DIS_DATE,'1900-01-01')!='1900-01-01' then cast(DIS_DATE as varchar(10))
				else ''
			End as [Discharge Date],
			ISNULL(DIS_STAT,'') as [Discharge Disposition],
			ISNULL(FROM_DATE,'') as [From Date],
			ISNULL(TO_DATE,'') as [To Date],
			ISNULL(NULLIF(cast(PAID_DATE as varchar(10)),'1900-01-01'),'') as [Paid Date],
			ISNULL(POS,'') as [POS],
			ISNULL(NULLIF(UB_BILL_TYPE,'0'),'') as [Bill Type],
			ISNULL(NULLIF(REV_CODE,'0000'),'') as [Revenue Code],
			ISNULL(ATT_NPI,'') as [Attending Provider NPI],
			Case
				WHEN na.Entity_Type_Code=1 then concat(na.Provider_Last_Name,' ',na.Provider_First_Name)
				WHEN na.Entity_Type_Code=2 then na.Provider_Organization_Name
				WHEN na.Entity_Type_Code is null and ATT_PROV_NAME is not null then ATT_PROV_NAME
				else ''
			end as [Attending Provider Name],
			Case
				When ATT_PROV_SPEC is not null then ATT_PROV_SPEC
				When na.PrimarySpecialty is not null Then na.PrimarySpecialty
				Else ''
			end as [Attending Provider Specialty],
			
			ISNULL(NULLIF(BILL_PROV,'0'),'') as [Billing NPI],
			Case
				WHEN nb.Entity_Type_Code=1 then concat(nb.Provider_Last_Name,' ',nb.Provider_First_Name)
				WHEN nb.Entity_Type_Code=2 then nb.Provider_Organization_Name
				WHEN nb.Entity_Type_Code is null and BILL_PROV_NAME is not null then BILL_PROV_NAME
				else ''
			end as [Billing Provider Name],
			Case
				When SV_STAT='P' Then 'Paid'
				When SV_STAT='D' Then 'Denied'
				When SV_STAT='O' Then 'Pending'
				When SV_STAT='R' Then 'Reversed'
				When SV_STAT='A' Then 'Adjusted'
				else 'Unknown'
			end as [Claim Status],
			ISNULL(AMT_BILLED,0) as [Billed Amount],
			ISNULL(AMT_ALLOWED,0) as [Allowed Amount],
			ISNULL(AMT_PAID,0) as [Paid Amount],
			ISNULL(AMT_DEDUCT,0) as [Deductible],
			ISNULL(AMT_COINS,0) as [Coinsurance],
			ISNULL(AMT_COPAY,0) as [Copay],
			ISNULL(AMT_COB,0) as [Cordination of Benefit],
			ISNULL(CL_DATA_SRC,'') as [Payer]
		From CLAIMLINE c
		Left outer join RFT_NPI na on
			ATT_NPI=na.NPI
		Left outer join RFT_NPI nb on
			BILL_PROV=nb.NPI
		Where 
			ROOT_COMPANIES_ID=@rootId and
			EMPI=@empi
		--	and CLAIM_ID='8956010105258B0' and SV_LINE=1
			order by 8 Desc

		For JSON Path
	) as nvarchar(max))
	)
	
	When @module='HCC' Then 
	(
		Select cast(
	(
			Select 
				ISNULL(HCC,'') as HCC
				,ISNULL(Description,'') as Description
				,ISNULL(HCCGroup,'') as [HCC Group]
				,ISNULL(HCCType,'') as [HCC Type]
				,ISNULL(HCCWeight,'') as [Coefficient]
				,ISNULL(Status,'') as Status
				,ISNULL(DiagnosisCode,'') as [Qualifying Diagnosis Code]
				,ISNULL(DiagnosisDate,'') as [Diagnosis Date]
				,ISNULL(ProviderName,'') as [Diagnosing Provider]
				,ISNULL(Specialty,'') as [Diagnosing Provider Specialty]
			From RPT.HCC_List
			where
				root_Companies_id=@rootId and
				ReportId=(Select top 1 ReportId from RPT.Report_Details where Root_Companies_id=@rootId order by ReportEndDate desc) and
				EMPI=@empi
			For JSON Path
	) as nvarchar(max))
	)

	When @module='Measures' Then
	(
		Select cast
	(
		(
			select 
				ISNULL(MEASURE_ID,'') as [Measure Id]
				,ISNULL(MEASURE_NAME,'') as [Measure Name]
				,ISNULL(ReportType,'') as [Report Type]
				,Case
					When MEASURE_ID in(select MEASURE_ID from RFT.UHN_MeasuresList where InverseMeasure=1) and MEASURE_ID!='36' and Num=1 and Excl=0 and Rexcl=0 Then 'Open'
					When MEASURE_ID not in(select MEASURE_ID from RFT.UHN_MeasuresList where InverseMeasure=1) and Measure_id!='36' and Num=0 and Excl=0 and Rexcl=0 Then 'Open'
					When MEASURE_ID ='36' and Encounters=0 Then 'Close'
					Else 'Close'
				end as [Gap]
				,Case
					When Excl=1 Then 'Y'
					else 'N'
				end as [Optional Exclusion]
				,Case
					When REXCL=1 Then 'Y'
					else 'N'
				end as [Required Exclusion]
				,ISNULL(Code,'') as [QualifyingCode]
				,ISNULL(Replace(Cast(DateofService as varchar(10)),'1900-01-01',''),'') as [Qualifying Date]
				,ISNULL(Replace(Cast(DischargeDate as varchar(10)),'1900-01-01',''),'') as [Qualifying Discharge Date]
				,Case
					When Measure_id='36' then cast(Encounters as varchar(10))
					else 'NA'
				end as [ED Visits]
				,ISNULL(Report_Quarter,'') as [Reported Quarter]

			from RPT.Measure_DetailLine 
			where
				EMPI=@empi
			For JSON Path
		) as nvarchar(max)
	)
	)

	When @module='Medication' Then 
	(
		Select	Cast(
		(
			Select 
				ISNULL(CLAIM_ID,'') as [Claim Id]
				,ISNULL(FILL_DATE,'') as [Fill Date]
				,Case
					When n.BrandName is not null Then UPPER(BrandName)
					When n.GenericName is not null Then Upper(GenericName)
					when m.MEDICATION_NAME is not null and m.MEDICATION_NAME!='' then Upper(m.MEDICATION_NAME)
					else ''
				End as [Medication Name]
				,ISNULL(MEDICATION_CODE,'') as [Medication Code]
				,ISNULL(Cast(Cast(QUANTITY as numeric) as INT),'') as Quantity
				,Case
					When n.LABELERNAME is not null Then Upper(n.LABELERNAME)
					When m.MANUFACTURER is not null and m.MANUFACTURER!='' then Upper(m.MANUFACTURER)
					else ''
				end as Manufacturer
				,Case
					When n.GenericFlag is not null and n.GenericFlag=1 then 'Generic'
					When n.GenericFlag is not null and n.GenericFlag=2 then 'Brand'
					When m.MED_TYPE is not null and m.MED_TYPE=2 then 'Brand'
					When m.MED_TYPE is not null and m.MED_TYPE=1 then 'Generic'
					When m.MED_TYPE is not null and m.MED_TYPE=3 then 'Other'
					else 'Unknown'
				End as [Generic Flag]
				,ISNULL(Cast(Cast(SUPPLY_DAYS as numeric) as INT),'') as [Days of Supply]
				,Case
					When DAW is not null and DAW!='U' then DAW
					else ''
				end as DAW
				,ISNULL(NULLIF(REFILLS,0),'') as Refills
				,ISNULL(NULLIF(Cast(PRESCRIPTION_DATE as varchar(10)),'1900-01-01'),'') as [Prescription Date]
				,ISNULL(PRESC_NPI,'') as [Prescribing Provider NPI]
				,Case
					When npi.Provider_Last_Name is not null then Concat(UPPER(npi.Provider_Last_Name),' ',Upper(npi.Provider_First_Name))
					When m.PRESC_NAME is not null and m.PRESC_NAME!='' then Upper(m.PRESC_NAME)
					else ''
				End as [Prescribing Provider]
				,ISNULL(PHARM_ID,'') as [Pharmacy NPI]
				,Case
					When ph.Provider_organization_Name is not null then Upper(ph.Provider_organization_Name)
					When m.Pharm_Name is not null and m.Pharm_Name!='' then Upper(m.Pharm_Name)
					else ''
				End as Pharmacy
				,Case
					When MED_COVERAGE=1 Then 'Y'
					When MED_COVERAGE=0 Then 'N'
					else 'Unknown'
				end as Formulary
				,case
					when n.DosageForm is not null then n.DosageForm
					else ''
				end as Formulation
				,case
					when n.RouteName is not null then n.RouteName
					else ''
				end as Route
				,case
					when n.ACTIVE_NUMERATOR_STRENGTH is not null then concat(n.ACTIVE_NUMERATOR_STRENGTH,' ',n.ACTIVE_INGRED_UNIT)
					when m.MED_STRENGTH is not null and m.MED_STRENGTH!='' then m.MED_STRENGTH
					else ''
				end as Strength
				,ISNULL(n.TherapeuticClass,'') as [Therapeutic Class]
				,ISNULL(MED_DATA_SRC,'') as [Data Source]
			From MEDICATION m
			left outer Join RFT.ndccodes n on
				m.MEDICATION_CODE=n.NDC
			Left outer join RFT_NPI npi on
				m.PRESC_NPI=npi.NPI
			Left outer join RFT_NPI ph on
				m.PHARM_ID=ph.NPI
			where
				EMPI=@empi
			For jSON Path
		) as nvarchar(max))

	)

	When @module='Results' Then
	(
		Select
			cast(
			(
				select 
					ISNULL(ResultId,'') as [Accession Id]
					,ISNULL(ResultDate,'') as [Result Date]
					,ISNULL(ResultCode,'') as [Result Code]
					,ISNULL(ResultName,'') as [Result Name]
					,ISNULL(Value,'') as Value 
					,ISNULL(Unit,'') as Unit
					,ISNULL(ReferenceRange,'') as ReferenceRange
					,ISNULL(Criticality,'') as Criticality
					,ISNULL(PerformingLab,'') as PerformingLab
					,ISNULL(OrderingProviderNPI,'') as [Ordering Provider NPI]
					,Case
						When n.Provider_Last_Name is not null then Concat(UPPER(n.Provider_Last_Name),' ',Upper(n.Provider_First_Name))
						else ''
					End as [Ordering Provider]
					,ISNULL(LAB_DATA_SRC,'') as [Data Source]
				from Lab 
				Left outer join RFT_NPI n on
					OrderingProviderNPI=NPI
				where 
					EMPI=@empi
				order by ResultDate Desc
				FOR JSON PATH
			) as nvarchar(Max))
	)

	When @module='Vitals' Then 
	(
		Select
			Cast(
			(
				select 
					ISNULL(VitalName,'') as [Vital]
					,ISNULL(VitalCode,'') as [Vital Code]
					,ISNULL(VitalDateTime,'') as [Vital Date]
					,ISNULL(Value,'') as Value
					,ISNULL(Unit,'') as Unit
					,ISNULL(VIT_DATA_SRC,'') as [Data Source] 
				from Vital 
				where 
					EMPi=@empi
				Order by
					VitalDateTime Desc
				For JSON PATH
			) as nvarchar(max))
	)

	When @module='Diagnosis' Then 
	(
						Select cast((
			Select
				[Claim Id],[Diagnosis Date],[Diagnosis Code],[Diagnosis Description],[Diagnosis Code Type],[Diagnosis Sequence],[Admitting Diagnosis],[Data Source],[Diagnosing Provider],[Diagnosing Provider Specialty]
			From
			(
				select 
					ISNULL(d.CLAIM_ID,'') as [Claim Id]
					,ISNULL(DIAG_START_DATE,'') as [Diagnosis Date]
					,ISNULL(DIAG_CODE,'') as [Diagnosis Code]
					,ISNULL(i.Description,'') as [Diagnosis Description]
					,ISNULL(DIAG_CODE_TYPE,'') as [Diagnosis Code Type]
					,ISNULL(DIAG_SEQ_NO,'') as [Diagnosis Sequence]
					,ISNULL(ADMIT_DIAG,0) as [Admitting Diagnosis]
					,ISNULL(DIAG_DATA_SRC,'') as [Data Source]
					,ISNULL(UPPER(Coalesce(concat(n.Provider_Last_Name,' ',n.Provider_First_Name),c.ATT_PROV_NAME)),'') as [Diagnosing Provider]
					,ISNULL(UPPER(Coalesce(n.PrimarySpecialty,c.ATT_PROV_SPEC)),'') as [Diagnosing Provider Specialty]
					,ROW_NUMBER() over(Partition By d.EMPI,DIAG_START_DATE,DIAG_CODE order by DIAG_SEQ_NO,n.Provider_Last_Name desc,d.Claim_id) as rn
				from Diagnosis d
				Left outer join CLAIMLINE c on
					d.EMPI=c.EMPI and
					d.CLAIM_ID=c.CLAIM_ID and
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
				Left outer join RFT_NPI n on
					c.ATT_NPI=n.NPI and n.Entity_Type_Code=1
				Left outer join RFT.ICDCodes i on
					d.DIAG_CODE=i.Code
				where
					d.ROOT_COMPANIES_ID=@rootId and
					d.EMPI=@empi
			)t1
			Where
				rn=1
			Order By [Diagnosis Date] desc,[Diagnosis Sequence]
			For JSON PATH) as nvarchar(max)
			)

	)

	When @module='Procedures' Then 
	(
			Select
			Cast(
			(

				Select distinct
					ISNULL(t1.CLAIM_ID,'') as [Claim Id]
					,ISNULL(t1.CLAIMLINE,'') as [Claim Line]
					,ISNULL(t1.ProcedureDate,'') as [Procedure Date]
					,ISNULL(t1.ProcedureCode,'') as [Code]
					,ISNULL(t1.ProcedureCodeType,'') as [Procedure Code Type]
					,ISNULL(Modifier1,'') as  Modifier1
					,ISNULL(Modifier2,'') as Modifier2
					,ISNULL(ServiceUnits,'') as [Service Units]
					,Case
						When n.Provider_Last_Name is not null then Concat(UPPER(n.Provider_Last_Name),' ',Upper(n.Provider_First_Name))
						When c.ATT_PROV_NAME is not null then UPPER(c.ATT_PROV_NAME)
						else ''
					End as [Attending Provider]
					,Case
						When n.PrimarySpecialty is not null then Upper(n.PrimarySpecialty)
						When c.ATT_PROV_SPEC is not null then UPPER(c.ATT_PROV_SPEC)
						else ''
					End as [Attending Provider Specialty]
					,ISNULL(DataSource,'') as [Data Source]
				From
				(

					select 
						p.EMPI
						,p.CLAIM_ID
						,p.SV_LINE as CLAIMLINE
						,PROC_START_DATE as ProcedureDate
						,PROC_CODE as ProcedureCode
						,ISNULL(PROC_CODE_TYPE,'') as ProcedureCodeType
						,MOD_1 as Modifier1
						,MOD_2 as Modifier2
						,SERVICE_UNIT as ServiceUnits
						,PROC_DATA_SRC as DataSource
					from Procedures p
					where
						ROOT_COMPANIES_ID=@rootId and
						p.PROC_CODE is not null and
						p.EMPI=@empi

					Union all

					select 
						p.EMPI
						,p.CLAIM_ID
						,p.SV_LINE as CLAIMLINE
						,PROC_START_DATE as ProcedureDate
						,ICDPCS_CODE as ProcedureCode
						,'ICDPCS10' as ProcedureCodeType
						,'' as Modifier1
						,'' as Modifier2
						,'' as ServiceUnits
						,PROC_DATA_SRC as DataSource
					from Procedures p
					where
						ROOT_COMPANIES_ID=@rootId and
						p.ICDPCS_CODE is not null and
						p.EMPI=@empi
				)t1
				Left outer join CLAIMLINE c on
					t1.EMPI=c.EMPI and
					t1.CLAIM_ID=c.CLAIM_ID and
					t1.DataSource=c.CL_DATA_SRC
				Left outer join RFT_NPI n on
					c.ATT_NPI=n.NPI
				Order by
					3 desc
				For JSON PATh
			) as nvarchar(max)
		)

	)

	When @module='Attribution' Then
	(
		Select
			Cast(
			(

				select
					Case 
						When Attribution_Type='Ambulatory_PCP' Then 'Ambulatory PCP' 
						When Attribution_Type='Hospitalist' Then 'Hospitalist PCP' 
						When Attribution_Type='Ambulatory_Specialist' Then 'Ambulatory Specialist' 
						When Attribution_Type='IP_Surg_Specialist' Then 'Inpatient Surgical Specialist' 
						else ''
					end as [Attribution Type]
					,ISNULL(NPI,'') as NPI
					,ISNULL(Prov_Name,'') as [Provider Name]
					,ISNULL(Specialty,'') as Specialty
					,ISNULL(Practice,'') as Practice
					,ISNULL(RecentVisit,'') as [Recent Visit Date]
					,Rank
				From RPT.ConsolidatedAttribution_Snapshot
				where
					ROOT_COMPANIES_ID=@rootId and
					NPI!='' and
					EMPI=@empi
				For JSON PATH
			) as nvarchar(max)
		)
	)

	When @module='Registries' Then 
	(
			Select cast((
			select
				ISNULL(RegistryName,'') as [Registry Name]
				,ISNULL(DiagnosisCode,'') as [Qualifying Diagnosis]
				,ISNULL(NULLIF(Cast(ServiceDate as varchar),'1900-01-01') ,'') as [Qualifying Date]
				,ISNULL(AttendingProviderNPI,'') as [Diagnosing Provider NPI]
				,ISNULL(AttendingProviderName,'') as [Diagnosing Provider]
				,ISNULL(AttendingProviderSpecialty,'') as [Diagnosing Provider Specialty]
				,ISNULL(DiagnosisDataSource,'') as [Data Source]
			From REG.Registry_output
			Where
				ROOT_COMPANIES_ID=@rootId and
				ReportId=(Select top 1 ReportId from KPI_ENGINE.RPT.Report_Details order by ReportEndDate Desc) and
				EMPI=@empi
				Order by RegistryName 
				For JSON PATH
				) as nvarchar(max))

	)

	When @module='Utilization' Then
	(
		Select cast((
					Select 
			ISNULL(TypeOfEncounter,'') as [Encounter Type],
			ISNULL(Code,'') as[Qualifying Code],
			ISNULL(DateofEncounter,'') as [Encounter Date],
			ISNULL(AdmissionDate,'') as [Admission Date],
			Case 
				when TypeOfEncounter not in ('Readmission 30','Readmission 60','Readmission 90') Then AdmissionFacilityNPI
				else ''
			End as [Admission Facility NPI],
			Case 
				when TypeOfEncounter not in ('Readmission 30','Readmission 60','Readmission 90') Then AdmissionFacility
				else ''
			End as [Admission Facility],
			ISNULL(DischargeDate,'') as DischargeDate,
			ISNULL(DischargeFacilityNPI,'') as [Discharge Facility NPI],
			ISNULL(DischargeFacility,'') as [Discharge Facility],
			ISNULL(ReadmissionDate,'') as [Readmission Date],
			ISNULL(ReadmissionDischargeDate,'') as [Readmission Discharge Date],
			Case 
				when TypeOfEncounter in ('Readmission 30','Readmission 60','Readmission 90') Then AdmissionFacilityNPI
				else ''
			End as [Readmission Facility NPI],
			Case 
				when TypeOfEncounter in ('Readmission 30','Readmission 60','Readmission 90') Then AdmissionFacility
				else ''
			End as [Readmission Facility]
		from RPT.UtilizationReport
		Where
			ROOT_COMPANIES_ID=@rootId and
			ReportId=(Select Top 1 ReportId from RPT.Report_Details where Root_Companies_id=@rootId order by ReportEndDate Desc) and
			EMPI=@empi
			Order by TypeOfEncounter
			FOR JSON PATH
		) as nvarchar(max))

	)

	when @module='Notifications' Then 
	(
		Select
		  ISNULL(count(Case when Gap = 'Open' Then 1 end),0) as [Open Measures],
	--	  count(Case when Gap = 'Close' Then 1 end) as [Closed Measures],
		  ISNULL((Select ISNULL(TotalRegistries,0) from PatientRegistries where EMPI=@empi),'') as [Total Registries],
		  ISNULL((Select ISNULL(OpenHCCcount,0) from HCC_Patient_Summary where EMPI=@empi),'') as [Open HCC Count]
	--	  (Select ChronicHCCCount from HCC_Patient_Summary where EMPI=@empi) as [Chronic HCC Count],
	--	  (Select ClosureRate from HCC_Patient_Summary where EMPI=@empi) as [Closure Rate]
		From
		  (
			select
			  EMPI,
			  MEASURE_ID,
			  MEASURE_NAME,
			  ReportType,Case
				When MEASURE_ID in(select MEASURE_ID from RFT.UHN_MeasuresList where InverseMeasure=1) and MEASURE_ID!='36'
				and Num = 1
				and Excl = 0
				and Rexcl = 0 Then 'Open'
				When MEASURE_ID not in(select MEASURE_ID from RFT.UHN_MeasuresList where InverseMeasure=1) and MEASURE_ID!='36'
				and Num = 0
				and Excl = 0
				and Rexcl = 0 Then 'Open'
				When MEASURE_ID = '36'
				and Encounters = 0 Then 'Close'
				Else 'Close'
			  end as [Gap],
			  Excl as [optionalExclusion],
			  REXCL as [RequiredExclusion],
			  ISNULL(Code, '') as [qualifyingCode],
			  ISNULL(
				Replace(
				  Cast(DateofService as varchar(10)),
				  '1900-01-01',
				  ''
				),
				''
			  ) as [qualifyingDate],
			  ISNULL(
				Replace(
				  Cast(DischargeDate as varchar(10)),
				  '1900-01-01',
				  ''
				),
				''
			  ) as [qualifyingDischargeDate],Case
				When Measure_id = '36' then cast(Encounters as varchar(10))
				else 'NA'
			  end as [EDVisits],
			  Report_Quarter as [ReportedQuarter]
			from
			  RPT.Measure_DetailLine_Snapshot
			where
			  root_companies_id = @rootId
			  and EMPI = @empi
		)t1
		For JSON PATH
	)
	End
	
	






		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

	Return

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
