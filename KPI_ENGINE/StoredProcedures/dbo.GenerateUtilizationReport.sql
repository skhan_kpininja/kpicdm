SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE Procedure [dbo].[GenerateUtilizationReport]
as

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


Declare @rundate date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @ce_startdt Date
Declare @ce_enddt Date;
Declare @reportId INT;
Declare @ce_dischstartdt Date;
Declare @ce_dischenddt Date

--report detail sp from kpi_engine
exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

--SET @ce_startdt=@startDate
--Set @ce_enddt=@enddate

SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_dischstartdt=concat(@meas_year,'-01-03');
Set @ce_dischenddt=concat(@meas_year,'-12-01');



-- identify follow up visits
Drop table if exists #uhn_followups
Select
	*
into #uhn_followups
From
(
	Select 
		p.EMPI,
		p.PROC_START_DATE,
		c.ATT_NPI,
		coalesce(NULLIF(concat(n.Provider_Last_Name,' ',n.Provider_First_Name),' '),c.ATT_PROV_NAME) as ATT_PROV_NAME,
		row_number() over(partition by p.EMPI,PROC_START_DATE order by n.Entity_Type_Code desc) as rn
	From PROCEDURES p
	Left outer join CLAIMLINE c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
	left outer Join RFT_NPI n on
		c.ATT_NPI=n.NPI and
		n.Entity_Type_Code=1
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(c.POS,'0')!='81' and
		p.PROC_START_DATE between @ce_startdt and DATEADD(day,30,@ce_enddt) and
		p.PROC_CODE in
		(
			select code from REG.REGISTRY_VALUESET where ValueSetName='Follow Ups' and ROOT_COMPANIES_ID=@rootId
		)
)t1
Where
	rn=1

	
-- Identify Home Health Visits
		
	Exec SP_KPI_DROPTABLE '#homehealth'
	select distinct
		EMPI
		,'06 - Discharge to Home Health' as Code
		,coalesce(DIS_DATE,TO_DATE) as DateofEncounter
		,'Home Health' as TypeofEncounter
		,BILL_PROV
		,BILL_PROV_NAME
		into #homehealth
	From CLAIMLINE
	where
		ROOT_COMPANIES_ID=@rootId and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt and @ce_enddt and
		REV_CODE in
		(
			Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Inpatient Stay'
		)
		and
		ISNULL(DIS_STAT,'0')='06'

	
		
		
-- ED Visits

Exec SP_KPI_DROPTABLE '#edvisits'
select distinct 
	EMPI
	,FROM_DATE
	,Code
	,CLAIM_ID
	,BILL_PROV
	,BILL_PROV_NAME
into #edvisits
from
(
	Select
		c.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,p.PROC_CODE as Code
		,c.CLAIM_ID
		,c.BILL_PROV
		,c.BILL_PROV_NAME
	From PROCEDURES p
	Join CLAIMLINE c on 
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		ISNULL(c.POS,0)!='81' and
		c.FROM_DATE between Dateadd(year,-1,@ce_startdt) and @ce_enddt and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		p.PROC_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('ED')
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
		


	Union all

	Select
		c.EMPI
		,p.PROC_START_DATE
		,c.POS as Code
		,c.CLAIM_ID
		,c.BILL_PROV
		,c.BILL_PROV_NAME
	From PROCEDURES p
	Join CLAIMLINE c on 
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0') in
		(
			select code from HDS.VALUESET_TO_CODE where value_set_name='ED POS'
		)
		and
		c.FROM_DATE between DATEADD(year,-1,@ce_startdt) and @ce_enddt and
		p.PROC_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('ED Procedure Code')
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
	

	Union all

	Select
		c.EMPI
		,c.FROM_DATE
		,c.REV_CODE as Code
		,c.CLAIM_ID
		,c.BILL_PROV
		,c.BILL_PROV_NAME
	From CLAIMLINE c	
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		c.CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		c.FROM_DATE between DATEADD(year,-1,@ce_startdt) and @ce_enddt and
		c.REV_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='ED'
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
		
)t1

--select count(distinct concat(EMPI,FROM_DATE)) from #edvisits where FROM_DATE between '2021-01-01' and '2021-12-31' 

--Exclusion Set - Mental and Behavioral Disorders,	Psychiatry ,	Electroconvulsive therapy
Drop table if exists #edu_encounterexclusionlist;
Create table #edu_encounterexclusionlist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
)
Insert into #edu_encounterexclusionlist
Select distinct
	EMPI
	,FROM_DATE
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
From
(

	Select
		d.EMPI
		,d.DIAG_START_DATE as FROM_DATE
		,Coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
		,Coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
		,d.CLAIM_ID
	From diagnosis d
	Join CLAIMLINE c on
		d.CLAIM_ID=c.CLAIM_ID and
		d.DIAG_DATA_SRC=c.CL_DATA_SRC and
		d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		d.EMPI=c.EMPI
	Where
		d.ROOT_COMPANIES_ID=@rootId and
		d.DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		d.DIAG_START_DATE between @ce_startdt and @ce_enddt and
		DIAG_SEQ_NO=1 and
		DIAG_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Mental and Behavioral Disorders'
		)


	Union all

	Select
		p.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,Coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
		,Coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
		,p.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		p.PROC_START_DATE between @ce_startdt and @ce_enddt and
		(
			PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Psychiatry','Electroconvulsive Therapy')
			)
			or
			ICDPCS_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Electroconvulsive Therapy')
			)
		)
)t1




-- Observation Stay and Inpatient Stay
Exec SP_KPI_DROPTABLE '#stayexclusionlist'
Select distinct
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,BILL_PROV
	,BILL_PROV_NAME
into #stayexclusionlist
From
(
	Select distinct
		EMPI
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
		,BILL_PROV
		,BILL_PROV_NAME
	From inpatientstays(@rootId,'1900-01-01',@ce_enddt)
	Where
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) 
	/*
	Union All

	Select distinct
		EMPI
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
		,BILL_PROV
		,BILL_PROV_NAME
	From observationstays(@rootId,DATEADD(year,-1,@ce_startdt),@ce_enddt)
	*/
)t1


	
	Exec SP_KPI_DROPTABLE '#ed_final'
	Select
		EMPI
		,Code
		,DateofEncounter
		,TypeofEncounter
		,BILL_PROV
		,BILL_PROV_NAME
		,VisitReasonCode
		,VisitReason
		into #ed_final
	From
	(
		select 
			ed.EMPi
			,ed.Code
			,ed.FROM_DATE as DateofEncounter
			,'ED Visit' as TypeofEncounter
			,ed.BILL_PROV
			,ed.BILL_PROV_NAME
			,d.DIAG_CODE as VisitReasonCode
			,i.description as VisitReason
			,ROW_NUMBER() over(partition by ed.EMPI,ed.FROM_DATE order by ed.FROM_DATE DESC) as rn
		from #edvisits ed -- where memid=96248
		left outer join DIAGNOSIS d on
			ed.EMPI=d.EMPI and
			ed.CLAIM_ID=d.CLAIM_ID and
			d.DIAG_SEQ_NO=1
		left outer join RFT_ICDCodes i on
			d.DIAG_CODE=i.code
		left outer join #stayexclusionlist stx on 
			ed.EMPI=stx.EMPI and 
			(
				ed.CLAIM_ID=stx.CLAIM_ID 
				or 
				ed.FROM_DATE between dateadd(day,-1,stx.ADM_DATE) and stx.DIS_DATE
			)
		left outer join #edu_encounterexclusionlist enx on
			ed.EMPI=enx.EMPI and 
			ed.CLAIM_ID=enx.CLAIM_ID
		where 
			stx.EMPI is null and
			enx.EMPI is null
	)t1
	Where
		rn=1

		

		-- ED Visit Summary
		Drop table if exists #ed_summary
		Select 
			EMPI
			,count(case when DateofEncounter between @ce_startdt and @ce_enddt then 1 end) as EDVisitCount
			,count(case when DateofEncounter between DATEADD(year,-1,@ce_startdt) and DATEADD(Year,-1,@ce_enddt) then 1 end) as EDVisitCount_Lastyear
			into #ed_summary
		from #ed_final
		Group by EMPI

		

	-- Primary or OP SPecialist Visit

	Drop table if exists #primarycarevisit;
	Select 
		EMPI,
		PROC_START_DATE,
		ATT_NPI as AttendingNPI,
		AttendingProviderName
	into #primarycarevisit
	From
	(
		Select
			*
			,ROW_NUMBER() over(Partition by EMPI order by PROC_START_DATE Desc,AttendingProviderName desc) as rn
		from
		(
			Select
				c.EMPI
				,PROC_START_DATE
				,c.ATT_NPI
				,Coalesce(NULLIF(Concat(Provider_Last_Name,' ',Provider_First_Name),' '),c.ATT_PROV_NAME) as AttendingProviderName
				,'Primary Care Visit' as Type
			From PROCEDURES p
			Join CLAIMLINE c on 
				p.CLAIM_ID=c.CLAIM_ID and
				ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
				p.PROC_DATA_SRC=c.CL_DATA_SRC and
				p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
				p.EMPI=c.EMPI
			Left Outer Join RFT_NPI n on
				c.ATT_NPI=n.NPI and
				n.Entity_Type_Code=1
			Where
				c.ROOT_COMPANIES_ID=159 and
				ISNULL(c.POS,'0') !='81' and
				PROC_START_DATE between DATEADD(year,-2,@ce_startdt) and @ce_enddt and
				p.PROC_CODE in
				(
					select code from REG.REGISTRY_VALUESET where ValueSetName in('Primary Care Services')
				)

			/*
			Union all

			Select
				EMPI,
				FROM_DATE,
				ATT_NPI,
				Coalesce(NULLIF(Concat(Provider_Last_Name,' ',Provider_First_Name),' '),ATT_PROV_NAME) as AttendingProviderName,
				'OP Specialist Visit' as Type
			From CLAIMLINE
			Left outer Join RFT_NPI n on 
				ATT_NPI=NPI and
				PrimarySpecialty not in(Select PreferredConceptName from RFT_Taxonomies where Conceptcode in('207Q00000X','207R00000X','208D00000X','208000000X'))
				and Entity_Type_Code=1
			Where
				ROOT_COMPANIES_ID=159 and
				FROM_DATE between DATEADD(year,-2,@ce_startdt) and @ce_enddt and
				ISNULL(POS,'0') in('19','20','21','22')
			*/
		)tbl
		Left outer join RFT_NPI n on
			ATT_NPI=n.NPI and 
			n.Entity_Type_Code=1
	)t2
	Where
		rn=1

		
	Drop table if Exists #ed;
	Select 
		e.*,
		p.PROC_START_DATE as VisitDate,
		AttendingNPI,
		AttendingProviderName,
		s.EDVisitCount_Lastyear 
		into #ed
	from #ed_final e
	left outer Join #primarycarevisit p on
		e.EMPI=p.EMPI
	Left outer join #ed_summary s on 
		e.EMPI=s.EMPI
	Where
		DateofEncounter between @ce_startdt and @ce_enddt

		
		

-- Admissions/Hospitalizations

-- Identify TCM visits

Drop table if exists #tcm;
Select EMPI,PROC_START_DATE,PROC_DATA_SRC,PROC_CODE into #tcm from PROCEDURES where PROC_CODE in('99495','99496')



-- Admissions

	
	Exec SP_KPI_DROPTABLE '#admissions'
	Drop table if exists #admissions;
	Select
		*
	into #admissions
	From
	(
			select 
				c.EMPI
				,REV_CODE as Code
				,ADM_DATE as ADM_DATE
				, DIS_DATE
				,'Admission' as TypeofEncounter
				,BILL_PROV as AdmissionFacilityNPI
				,coalesce(NULLIF(n.Provider_organization_name,''),BILL_PROV_NAME) as AdmissionFacility
				,MS_DRG
				,d.Description as DischargeDisposition
				,ROW_NUMBER() over(Partition By c.EMPI,ADM_DATE order by DIS_DATE desc,Entity_Type_Code desc) as rn
			From CLAIMLINE c
			Left Outer Join RFT.DispositionCodes d on
				c.DIS_STAT=d.Code
			Left outer Join dbo.RFT_NPI n on
				c.BILL_PROV=n.NPI
				and n.Entity_Type_Code=2
			where
				ROOT_COMPANIES_ID=@rootId and
				ISNULL(POS,0)!='81' and
				ADM_DATE between @ce_startdt and @ce_enddt and
				REV_CODE in
				(
					Select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay','Nonacute Inpatient Stay','Nonacute Inpatient Stay Other Than Behavioral Health Accommodations'/*,'Observation Stay'*/)
					-- Commenting observation stay on Heathers request on 07-15-2021
				)
				--and c.EMPI='1482A64B-E0E6-416C-9FC5-8C610388C829'
	)tbl
	Where rn=1

		



	
drop table if exists #mergedstays
;with grp_starts as 
	(
	  select 
		EMPI
		,Code
		,ADM_DATE
		,DIS_DATE
		,case
			when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
			or 
			(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
			then 0 
			else 1
		end grp_start
		,AdmissionFacilityNPI
		,AdmissionFacility
		,MS_DRG
		,DischargeDisposition
	  from #admissions

	)
	, grps as 
	(
	  select 
		EMPI
		,Code
		,ADM_DATE
		, DIS_DATE
		,AdmissionFacilityNPI
		,AdmissionFacility
		,MS_DRG
		,DischargeDisposition
		,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
	  from grp_starts
	)
	Select 
		EMPI
		,Code
		,AdmissionDate
		,DischargeDate
		,AdmissionFacilityNPI
		,AdmissionFacility
		,LOS
		,DischargeDisposition
		,DischargeFacilityNPI
		,DischargeFacility
		,PROC_START_DATE
		,PROC_CODE
		,MS_DRG
		,FollowUpVisitDate
		,FU_AttendingProviderNPI
		,FU_AttendingProvider
		,FollowUpAfterDays
		,rnt
		,rnf
	into #mergedstays
	From
	(
		select 
			t1.EMPI
			,t1.Code
			,AdmissionDate
			,DischargeDate
			,a.AdmissionFacilityNPI
			,a.AdmissionFacility
			,DATEDIFF(day,AdmissionDate,DischargeDate)+1 as LOS
			,d.DischargeDisposition
			,d.AdmissionFacilityNPI as DischargeFacilityNPI
			,d.AdmissionFacility as DischargeFacility
			,t.PROC_START_DATE
			,t.PROC_CODE
			,a.MS_DRG
			,f.PROC_START_DATE as FollowUpVisitDate
			,f.ATT_NPI as FU_AttendingProviderNPI
			,f.ATT_PROV_NAME as FU_AttendingProvider
			,DATEDIFF(day,t1.DischargeDate,f.PROC_START_DATE)+1 as FollowUpAfterDays
			,ROW_NUMBER() over(partition By t1.EMPI,AdmissionDate order by DischargeDate,t.Proc_Start_Date) as rnt
			,ROW_NUMBER() over(partition By t1.EMPI,AdmissionDate order by DischargeDate,f.Proc_Start_Date) as rnf
		from	
		(
			select 
				EMPI
				,min(Code) as Code
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
			from grps 
			
			group by 
				EMPI,grp
		
		)t1
		Join #admissions a on
			t1.EMPI=a.EMPI and
			t1.AdmissionDate=a.ADM_DATE
		left outer Join #admissions d on
			t1.EMPI=d.EMPI and
			t1.DischargeDate=d.DIS_DATE
		Left Outer Join #tcm t on
			t1.EMPI=t.EMPI and
			t.PROC_START_DATE between DischargeDate and DATEADD(day,30,DischargeDate)
		Left outer join #uhn_followups f on
			t1.EMPI=f.EMPI and
			f.PROC_START_DATE between t1.DischargeDate and DATEADD(day,30,t1.DischargeDate)
		
	)t2
	Where rnt=1 and rnf=1


	
		
--Skilled Nursing Stay

	Exec SP_KPI_DROPTABLE '#snf'
	select 
		EMPI
		,REV_CODE as Code
		,ADM_DATE
		,DIS_DATE
		,BILL_PROV 
		,BILL_PROV_NAME
	Into #snf
	From CLAIMLINE
	where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,0)!='81' and
		ADM_DATE between @ce_startdt and @ce_enddt and
		REV_CODE in
		(
			Select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Skilled Nursing Stay')
		)



	
	Exec SP_KPI_DROPTABLE '#snfmergedstays'
	;with grp_starts as 
	(
	  select 
		EMPI
		,Code
		,ADM_DATE
		,DIS_DATE
		,case
			when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
			or 
			(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
			then 0 
			else 1
		end grp_start
	  from #snf

	)
	, grps as 
	(
	  select 
		EMPI
		,Code
		,ADM_DATE
		, DIS_DATE
		,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
	  from grp_starts
	)
	Select
		*
		Into #snfmergedstays
	From
	(
		Select
			EMPI
			,Code
			,AdmissionDate as DateofEncounter
			,DischargeDate
			,'SNF' as TypeofEncounter
			,DATEDIFF(day,AdmissionDate,DischargeDate)+1 as LOS
			,AdmissionFacilityNPI
			,AdmissionFacility
			,DischargeFacilityNPI
			,DischargeFacility
			,MS_DRG
			,AnchorAdmissionDate
			,AnchorDischargeDate
			,AnchorDichargeFacilityNPI
			,AnchorDichargeFacility
			,FU_Visit_Date
			,FU_AttendingProviderNPI
			,FU_AttendingProvider
			,FU_AfterDays
			,ROW_NUMBER() over(Partition by EMPI,AdmissionDate order by AdmissionFacilityNPI Desc) as rn
			,ROW_NUMBER() over(Partition by EMPI,AdmissionDate order by FU_Visit_Date asc) as rnf
		From
		(
			select 
				t1.EMPI
				,s.Code
				,AdmissionDate
				,DischargeDate
				,s.BILL_PROV as AdmissionFacilityNPI
				,s.BILL_PROV_NAME as AdmissionFacility
				,d.BILL_PROV as DischargeFacilityNPI
				,d.BILL_PROV_NAME as DischargeFacility
				,c.MS_DRG
				,c.ADM_DATE as AnchorAdmissionDate
				,c.DIS_DATE as AnchorDischargeDate
				,c.BILL_PROV as AnchorDichargeFacilityNPI
				,c.BILL_PROV_NAME as AnchorDichargeFacility
				,f.PROC_START_DATE as FU_Visit_Date
				,f.ATT_NPI as FU_AttendingProviderNPI
				,f.ATT_PROV_NAME as FU_AttendingProvider
				,DATEDIFF(day,t1.DischargeDate,f.PROC_START_DATE)+1 as  FU_AfterDays
 			from	
			(
						select 
							EMPI
							,min(ADM_DATE) as AdmissionDate
							,max(DIS_DATE) as DischargeDate
						from grps 
						group by EMPI,grp
		
			)t1
			Join #snf s on
				t1.EMPI=s.EMPI and
				t1.AdmissionDate=s.ADM_DATE
			Join #snf d on
				t1.EMPI=d.EMPI and
				t1.DischargeDate=d.DIS_DATE
			left outer join CLAIMLINE c on 
				s.EMPI=c.EMPI and 
				datediff(day,c.DIS_DATE,t1.AdmissionDate) between 0 and 1 and 
				c.REV_CODE in(Select code from HDS.ValueSet_TO_Code where code_System='UBREV' and value_Set_Name='Inpatient Stay')
			left outer join #uhn_followups f on
				s.EMPI=f.EMPI and
				f.PROC_START_DATE between t1.DischargeDate and DATEADD(day,30,t1.DischargeDate)
		
		)t2
	)t3
	Where	
		rn=1 and rnf=1

		

	
		

	
	
-- Readmission

-- Exclusion

-- hospice Exclusion
drop table if exists #uhn_hospicemembers;
CREATE table #uhn_hospicemembers
(
	EMPI varchar(100)
		
)
Insert into #uhn_hospicemembers
select EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)



Exec SP_KPI_DROPTABLE '#readm_ipstaylist'
select distinct
	EMPI
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
	,BILL_PROV
	,BILL_PROV_NAME
	,MS_DRG
	into #readm_ipstaylist
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay')
	)

	


Exec SP_KPI_DROPTABLE '#readm_nonacutestays'
select distinct
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
into #readm_nonacutestays
From CLAIMLINE
where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(POS,'0')!='81' and 
	(
		RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in 
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
		) 
		or 
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
		)
	)

	

--Remove Non acute stays from ip to only identify acute stays
Exec SP_KPI_DROPTABLE '#readm_acutestays'
Select
	ip.*
Into #readm_acutestays
From #readm_ipstaylist ip
left outer join #readm_nonacutestays na on ip.EMPI=na.EMPI and ip.CLAIM_ID=na.CLAIM_ID
Where
	na.EMPI is null


-- Combine ip and observation and merge direct transfers
Exec SP_KPI_DROPTABLE '#readm_mergedstays'
;with grp_starts as 
(
	  select 
		EMPI
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
		,case
			when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
			or 
			(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
			then 0 
			else 1
		end grp_start
	  from #readm_acutestays
	  
)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Select 
	*
into #readm_mergedstays
From 
(
	select 
		t1.EMPI
		,AdmissionDate
		,DischargeDate
		,AdmissionClaimId
		,DischargeClaimId
		,grp_claimid
		,BILL_PROV
		,BILL_PROV_NAME
		,MS_DRG
		,ROW_NUMBER() Over(Partition by t1.EMPI,DischargeDate order by BILL_PROV desc) as rn
	from	
	(
				select 
					EMPI
					,min(ADM_DATE) as AdmissionDate
					,max(DIS_DATE) as DischargeDate
					,min(CLAIM_ID) as AdmissionClaimId
					,max(CLAIM_ID) as DischargeClaimId
					,STRING_AGG(CLAIM_ID,',') as grp_claimid
				from grps 
				group by EMPI,grp
		
	)t1
	Join #readm_acutestays r on
		t1.EMPI=r.EMPI and
		t1.AdmissionDate=r.ADM_DATE
)tbl
Where
	rn=1




-- Identify Planned and eligible exclusions
-- Identify Prgnacncy Visits
Exec SP_KPI_DROPTABLE '#pregvstlist'
Select distinct
	d.EMPI
	,d.CLAIM_ID
into #pregvstlist
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
Join open_empi_master m on d.EMPI=m.EMPI_ID and 
						   m.Gender='F' and
						   d.ROOT_COMPANIES_ID=m.Root_Companies_ID
						   
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'0')!='81' and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
	)
	and
	DIAG_SEQ_NO=1
	

-- Identify Deceased Members

drop table if exists #uhn_deceasedmembers;
CREATE table #uhn_deceasedmembers
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #uhn_deceasedmembers
select distinct
	EMPI
	,CLAIM_ID 
from CLAIMLINE 
where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(POS,'')!='81' and 
	DIS_STAT='20';




--Identify Perinatal Conditions
Exec SP_KPI_DROPTABLE '#perinatalvstlist'
Select distinct
	d.EMPI
	,d.CLAIM_ID
into #perinatalvstlist
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Perinatal Conditions'
	)
	and
	DIAG_SEQ_NO=1




--Idntifyinh Planned Admissions
-- Indetifying Chemotherapy
Exec SP_KPI_DROPTABLE '#chemovstlist'
Select distinct
	d.EMPI
	,d.CLAIM_ID
into #chemovstlist
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between Dateadd(year,-1,@ce_startdt) and @ce_enddt and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Chemotherapy Encounter'
	)
	and
	DIAG_SEQ_NO=1


-- Rehabilitation
Exec SP_KPI_DROPTABLE '#rehabvstlist'
Select distinct
	d.EMPI
	,d.CLAIM_ID
into #rehabvstlist
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between Dateadd(year,-1,@ce_startdt) and @ce_enddt and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Rehabilitation'
	)
	and
	DIAG_SEQ_NO=1



-- Organ Transplant
Exec SP_KPI_DROPTABLE '#organtransplanttlist'
Select distinct
	p.EMPI
	,p.CLAIM_ID
into #organtransplanttlist
From Procedures p
Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
					ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
					p.PROC_DATA_SRC=c.CL_DATA_SRC and
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					p.EMPI=c.EMPI
Where
	p.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between Dateadd(year,-1,@ce_startdt) and @ce_enddt and
	(
		PROC_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Organ Transplant Other Than Kidney')
		)
		or
		ICDPCS_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells')
		)
	)

	

--Potentially Planned Procedure
Exec SP_KPI_DROPTABLE '#plannedproctlist'
Select distinct
	p.EMPI
	,p.CLAIM_ID
into #plannedproctlist
From Procedures p
Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
				    ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
					p.PROC_DATA_SRC=c.CL_DATA_SRC and
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					p.EMPI=c.EMPI
JOIN DIAGNOSIS d on p.CLAIM_ID=d.CLAIM_ID and
					p.PROC_DATA_SRC=d.DIAG_DATA_SRC and
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					p.EMPI=d.EMPI and
					d.DIAG_SEQ_NO=1
where
	p.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE)  between Dateadd(year,-1,@ce_startdt) and @ce_enddt and
	d.DIAG_CODE not in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Acute Condition'
	)
	and
	ICDPCS_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Potentially Planned Procedures'
	)


-- Identify Stays for numerator based on admission date
Exec SP_KPI_DROPTABLE '#readm_num_ipstaylist'
select distinct
	c.CLAIM_ID
	,c.EMPI
	,REV_CODE as Code
	,ADM_DATE
	,DIS_DATE
	,BILL_PROV as AdmissionFacilityNPI
	,coalesce(NULLIF(n.Provider_organization_name,''),BILL_PROV_NAME) as AdmissionFacility
	,MS_DRG
	,CL_DATA_SRC
into #readm_num_ipstaylist
From CLAIMLINE c
Left outer Join dbo.RFT_NPI n on
	c.BILL_PROV=n.NPI
	and n.Entity_Type_Code=2
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay')
	)




-- Identify Nonacute Inpatient Stay
	Exec SP_KPI_DROPTABLE '#readm_num_naipstaylist'
	select
		EMPI
		,CLAIM_ID
		,CL_DATA_SRC
	into #readm_num_naipstaylist
	From CLAIMLINE
	Where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'')!='81' and
		(
			REV_CODE in 
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
			) 
			or 
			RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
			)
		)

-- Identify Acute and Observation for numerator
Exec SP_KPI_DROPTABLE '#readm_num_acutestalylist'
Select
	*
Into #readm_num_acutestalylist
From
(
	select
		ip.*
		,ROW_NUMBER() over(partition by ip.EMPI,ip.ADM_DATE order by DIS_DATE desc,AdmissionFacilityNPI desc) as rn
	From #readm_num_ipstaylist ip
	Left outer join #readm_num_naipstaylist na on 
		ip.EMPI=na.EMPI and 
		ip.CLAIM_ID=na.CLAIM_ID and
		ip.CL_DATA_SRC=na.CL_DATA_SRC
	Where
		na.EMPI is null
)tbl
Where
	rn=1



-- Combine ip and observation and merge direct transfers
Exec SP_KPI_DROPTABLE '#readm_num_mergedstays'
;with grp_starts as 
(
	  select distinct
		*
		,case
			when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
			then 0 
			else 1
		end grp_start
	  from #readm_num_acutestalylist
	  
)
, grps as 
(
  select 
	*
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
select distinct
	t1.EMPI
	,AdmissionDate as ReadmissionDate
	,DischargeDate as ReadmissionDischargeDate
	,DATEDIFF(day,AdmissionDate,DischargeDate)+1 as LOS
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid
	,AdmissionFacilityNPI
	,AdmissionFacility
	,MS_DRG
into #readm_num_mergedstays
from	
(
	select 
		EMPI
		,min(ADM_DATE) as AdmissionDate
		,max(DIS_DATE) as DischargeDate
		,min(CLAIM_ID) as AdmissionClaimId
		,max(CLAIM_ID) as DischargeClaimId
		,STRING_AGG(CLAIM_ID,',') as grp_claimid
	from grps 
	group by EMPI,grp
		
)t1
Join #readm_num_acutestalylist r on
	t1.EMPI=r.EMPI and
	t1.AdmissionDate=r.ADM_DATE





-- Exclude planned stays from base stay list
	Exec SP_KPI_DROPTABLE '#readm_basestaylist'
	select distinct
		b.* 
	into #readm_basestaylist
	from #readm_mergedstays b
	left outer join #pregvstlist pg on 
		b.EMPI=pg.EMPI and 
		pg.CLAIM_ID in(select value from string_split((select distinct b3.grp_claimid from #readm_mergedstays b3 where b3.grp_claimid=b.grp_claimid), ','))
	left outer join #perinatalvstlist pn on 
		b.EMPI=pn.EMPI and 
		pn.CLAIM_ID in(select value from string_split((select distinct b6.grp_claimid from #readm_mergedstays b6 where b6.grp_claimid=b.grp_claimid), ','))
	left outer join #uhn_hospicemembers h on 
		b.EMPI=h.EMPI
		
	left outer join #uhn_deceasedmembers d on 
		b.EMPI=d.EMPI and
		d.CLAIM_ID in(select value from string_split((select distinct b6.grp_claimid from #readm_mergedstays b6 where b6.grp_claimid=b.grp_claimid), ','))
	where 
		DischargeDate between @ce_startdt and @ce_enddt and 
		pg.EMPI is null and 
		pn.EMPI is null and
		h.EMPI is null and
		d.EMPI is null



-- Exclude planned stays from numerator stay list
	Exec SP_KPI_DROPTABLE '#readm_numstaylist'
	select distinct
		b.* 
	into #readm_numstaylist
	from #readm_num_mergedstays b
	left outer join #chemovstlist c on 
		b.EMPI=c.EMPI and 
		c.CLAIM_ID in(select value from string_split((select distinct b1.grp_claimid from #readm_num_mergedstays b1 where b1.grp_claimid=b.grp_claimid), ','))
	left outer join #rehabvstlist r on 
		b.EMPI=r.EMPI and 
		r.CLAIM_ID in(select value from string_split((select distinct b2.grp_claimid from #readm_num_mergedstays b2 where b2.grp_claimid=b.grp_claimid), ','))
	left outer join #pregvstlist pg on 
		b.EMPI=pg.EMPI and 
		pg.CLAIM_ID in(select value from string_split((select distinct b3.grp_claimid from #readm_num_mergedstays b3 where b3.grp_claimid=b.grp_claimid), ','))
	left outer join #organtransplanttlist o on 
		b.EMPI=o.EMPI and 
		o.CLAIM_ID in(select value from string_split((select distinct b4.grp_claimid from #readm_num_mergedstays b4 where b4.grp_claimid=b.grp_claimid), ','))
	left outer join #plannedproctlist p on 
		b.EMPI=p.EMPI and 
		p.CLAIM_ID in(select value from string_split((select distinct b5.grp_claimid from #readm_num_mergedstays b5 where b5.grp_claimid=b.grp_claimid), ','))
	left outer join #perinatalvstlist pn on 
		b.EMPI=pn.EMPI and 
		pn.CLAIM_ID in(select value from string_split((select distinct b6.grp_claimid from #readm_num_mergedstays b6 where b6.grp_claimid=b.grp_claimid), ','))
	left outer join #uhn_hospicemembers h on 
		b.EMPI=h.EMPI
	left outer join #uhn_deceasedmembers d on 
		b.EMPI=r.EMPI 
	where 
		ReadmissionDate between @ce_startdt and dateadd(month,3,@ce_enddt) and 
		c.EMPI is null and 
		r.EMPI is null and 
		o.EMPI is null and 
		p.EMPI is null and
		pg.EMPI is null and 
		pn.EMPI is null and
		h.EMPI is null and
		d.EMPI is null





	-- Identify 30 day readmissions
	Exec SP_KPI_DROPTABLE '#readmissionset'
	Select
		EMPI
		,'Readmission 30' as Code
		,AdmissionDate
		,DischargeDate
		,ReadmissionDate
		,ReadmissionDischargeDate
		,'Readmission 30' as TypeofEncounter
		,DischargeFacilityNPI
		,DischargeFacility
		,AdmissionFacilityNPI
		,AdmissionFacility
		,DRG
		,AnchorDRG
	into #readmissionset
	From
	(

		select 
			b.EMPI
			,b.DischargeDate
			,b.AdmissionDate
			,ReadmissionDate
			,ReadmissionDischargeDate
			,b.BILL_PROV as DischargeFacilityNPI
			,b.BILL_PROV_NAME as DischargeFacility
			,b.MS_DRG as AnchorDRG
			,ns.AdmissionFacilityNPI 
			,ns.AdmissionFacility
			,ns.MS_DRG as DRG
			,ROW_NUMBER() over(Partition by b.EMPI,b.DischargeDate order by ns.ReAdmissionDate) as rn
			,ROW_NUMBER() over(Partition by b.EMPI,ns.ReadmissionDate order by b.Dischargedate desc) as rn1
		from #readm_basestaylist b
		join #readm_numstaylist ns on 
			b.EMPI=ns.EMPI and 
			ns.ReadmissionDate between b.DischargeDate and dateadd(day,30,b.DischargeDate)
		where 
			b.AdmissionDate!=ns.ReadmissionDate
	)t1
	Where
		rn=1 and rn1=1
	order by EMPI

	

	-- Identify 90 day readmissions

	
	Exec SP_KPI_DROPTABLE '#readmissionset90'
	Select
		EMPI
		,'Readmission 90' as Code
		,DischargeDate
		,AdmissionDate
		,ReadmissionDate
		,ReadmissionDischargeDate
		,'Readmission 90' as TypeofEncounter
		,DischargeFacilityNPI
		,DischargeFacility
		,AdmissionFacilityNPI
		,AdmissionFacility
		,DRG
		,AnchorDRG
	into #readmissionset90
	From
	(

		select 
			b.EMPI
			,b.DischargeDate
			,b.AdmissionDate
			,ReadmissionDate
			,ReadmissionDischargeDate
			,b.BILL_PROV as DischargeFacilityNPI
			,b.BILL_PROV_NAME as DischargeFacility
			,ns.AdmissionFacilityNPI 
			,ns.AdmissionFacility
			,ns.MS_DRG as DRG
			,b.MS_DRG as AnchorDRG
			,ROW_NUMBER() over(Partition by b.EMPI,b.DischargeDate order by ns.ReAdmissionDate) as rn
			,ROW_NUMBER() over(Partition by b.EMPI,ns.ReadmissionDate order by b.Dischargedate desc) as rn1
		from #readm_basestaylist b
		join #readm_numstaylist ns on 
			b.EMPI=ns.EMPI and 
			ns.ReadmissionDate between b.DischargeDate and dateadd(day,90,b.DischargeDate)
		where 
			b.AdmissionDate!=ns.ReadmissionDate
	)t1
	Where
		rn=1 and rn1=1
	order by EMPI
	

	


	-- Identify 60 day readmissions

	
	Exec SP_KPI_DROPTABLE '#readmissionset60'
	Select
		EMPI
		,'Readmission 60' as Code
		,AdmissionDate
		,DischargeDate
		,ReadmissionDate
		,ReadmissionDischargeDate
		,'Readmission 60' as TypeofEncounter
		,DischargeFacilityNPI
		,DischargeFacility
		,AdmissionFacilityNPI
		,AdmissionFacility
		,DRG
		,AnchorDRG
	into #readmissionset60
	From
	(

		select 
			b.EMPI
			,b.DischargeDate
			,b.AdmissionDate
			,ReadmissionDate
			,ReadmissionDischargeDate
			,b.BILL_PROV as DischargeFacilityNPI
			,b.BILL_PROV_NAME as DischargeFacility
			,ns.AdmissionFacilityNPI 
			,ns.AdmissionFacility
			,ns.MS_DRG as DRG
			,b.MS_DRG as AnchorDRG
			,ROW_NUMBER() over(Partition by b.EMPI,b.DischargeDate order by ns.ReAdmissionDate) as rn
			,ROW_NUMBER() over(Partition by b.EMPI,ns.ReadmissionDate order by b.Dischargedate desc) as rn1
		from #readm_basestaylist b
		join #readm_numstaylist ns on 
			b.EMPI=ns.EMPI and 
			ns.ReadmissionDate between b.DischargeDate and dateadd(day,60,b.DischargeDate)
		where 
			b.AdmissionDate!=ns.ReadmissionDate
	)t1
	Where
		rn=1 and rn1=1
	order by EMPI
	


			



	Exec SP_KPI_DROPTABLE '#consolidatedencounters'
	Select 
		EMPI
		,Code
		,DateofEncounter
		,TypeofEncounter
		,DischargeFacilityNPI
		,DischargeFacility
		,AdmissionFacilityNPI
		,AdmissionFacility
		,DischargeDate
		,ReadmissionDate
		,ReadmissionDischargeDate
		,EDVisits_Prevyear
		,AttendingProviderNPI
		,AttendingProviderName
		,LastPrimaryVisit
		,AdmissionDate
		,DRG
		,DRGDescription
		,DischargeDisposition
		,LOS
		,TCMCode
		,AnchorDRG
		,AnchorDRGDescription
		,VisitReasonCode
		,VisitReason
		,FollowUpVisitDate
		,FU_AttendingProviderNPI
		,FU_AttendingProvider
		,FollowUpAfterDays
	into #consolidatedencounters	
	From
	(
		Select 
			t1.EMPI,
			t1.Code,
			t1.DateofEncounter,
			t1.TypeofEncounter,
			t1.DischargeFacilityNPI,
			UPPER(coalesce(NULLIF(n1.Provider_Organization_Name,''),DischargeFacility)) as DischargeFacility,
			t1.AdmissionFacilityNPI,
			UPPER(coalesce(NULLIF(n2.Provider_Organization_Name,''),DischargeFacility)) as AdmissionFacility,
			DischargeDate,
			ReadmissionDate,
			ReadmissionDischargeDate,
			EDVisits_Prevyear,
			AttendingProviderNPI,
			AttendingProviderName,
			LastPrimaryVisit,
			AdmissionDate,
			DRG,
			d.Description as DRGDescription,
			DischargeDisposition,
			LOS,
			TCMCode,
			AnchorDRG,
			ad.Description as AnchorDRGDescription,
			VisitReasonCode,
			VisitReason,
			FollowUpVisitDate,
			FU_AttendingProviderNPI,
			FU_AttendingProvider,
			FollowUpAfterDays,
			row_number() over(Partition by EMPI,TypeofEncounter,DateofEncounter order by DateofEncounter) as rnk
	
		from
		(
			Select distinct EMPI,Code,DateofEncounter,TypeofEncounter,Bill_prov as DischargeFacilityNPI,BILL_PROV_NAME as DischargeFacility,'' AdmissionFacilityNPI,'' as AdmissionFacility,null as DischargeDate,null as ReadmissionDate,null as ReadmissionDischargeDate,null as EDVisits_Prevyear,null as AttendingProviderNPI,null as AttendingProviderName,null as LastPrimaryVisit,null as AdmissionDate,null as DRG,'06' as DischargeDisposition,null as LOS,null as TCMCode,null as AnchorDRG,null as VisitReasonCode,null as VisitReason,null as  FollowUpVisitDate,null as FU_AttendingProviderNPI,null as FU_AttendingProvider,null as FollowUpAfterDays from #homehealth

			Union all

			Select distinct #ed.EMPI,Code,DateofEncounter,TypeofEncounter,'' as DischargeFacilityNPI,'' as DischargeFacility,Bill_prov as AdmissionFacilityNPI,UPPER(BILL_PROV_NAME) as AdmissionFacility,null as DischargeDate,null as ReadmissionDate,null as ReadmissionDischargeDate,EDVisitCount_Lastyear,AttendingNPI,AttendingProviderName,VisitDate,null as AdmissionDate,null as DRG,null as DischargeDisposition,null as LOS,null as TCMCode,null as AnchorDRG,VisitReasonCode,VisitReason,null as  FollowUpVisitDate,null as FU_AttendingProviderNPI,null as FU_AttendingProvider,null as FollowUpAfterDays from #ed
			

			

			Union all

			Select distinct EMPI,Code,AdmissionDate as DateofEncounter,'Admission' as TypeofEncounter,DischargeFacilityNPI,DischargeFacility,AdmissionFacilityNPI,AdmissionFacility,DischargeDate,null as ReadmissionDate,null as ReadmissionDischargeDate,null as EDVisits_Prevyear,null as AttendingProviderNPI,null as AttendingProviderName,PROC_START_DATE,AdmissionDate,MS_DRG,DischargeDisposition,LOS,PROC_CODE as TCMCode,null as AnchorDRG,null as VisitReasonCode,null as VisitReason,FollowUpVisitDate,FU_AttendingProviderNPI,FU_AttendingProvider,FollowUpAfterDays from #mergedstays
			
			

			Union all

			Select distinct EMPI,Code,DateofEncounter,TypeofEncounter,AnchorDichargeFacilityNPI as DichargeFacilityNPI,AnchorDichargeFacility as DischargeFacility,AdmissionFacilityNPI,AdmissionFacility,AnchorDischargeDate,DateofEncounter as ReadmissionDate,DischargeDate as ReadmissionDischargeDate,null as EDVisits_Prevyear,null as AttendingProviderNPI,null as AttendingProviderName,null as LastPrimaryVisit,AnchorAdmissionDate,null as DRG,null as DischargeDisposition,LOS,null as TCMCode,MS_DRG as AnchorDRG,null as VisitReasonCode,null as VisitReason,FU_Visit_Date,FU_AttendingProviderNPI,FU_AttendingProvider,FU_AfterDays from #snfmergedstays
			 
			 
			
			Union all 

			Select distinct EMPI,Code,ReadmissionDate,'Readmission 30' as TypeofEncounter,DischargeFacilityNPI,DischargeFacility,AdmissionFacilityNPI,AdmissionFacility,DischargeDate,ReadmissionDate,ReadmissionDischargeDate,null as EDVisits_Prevyear,null as AttendingProviderNPI,null as AttendingProviderName,null as LastPrimaryVisit,AdmissionDate,DRG,null as DischargeDisposition,null as LOS,null as TCMCode,AnchorDRG,null as VisitReasonCode,null as VisitReason,null as  FollowUpVisitDate,null as FU_AttendingProviderNPI,null as FU_AttendingProvider,null as FollowUpAfterDays from #readmissionset

			Union all 

			Select distinct EMPI,Code,ReadmissionDate,'Readmission 60' as TypeofEncounter,DischargeFacilityNPI,DischargeFacility,AdmissionFacilityNPI,AdmissionFacility,DischargeDate,ReadmissionDate,ReadmissionDischargeDate,null as EDVisits_Prevyear,null as AttendingProviderNPI,null as AttendingProviderName,null as LastPrimaryVisit,AdmissionDate,DRG,null as DischargeDisposition,null as LOS,null as TCMCode,AnchorDRG,null as VisitReasonCode,null as VisitReason,null as  FollowUpVisitDate,null as FU_AttendingProviderNPI,null as FU_AttendingProvider,null as FollowUpAfterDays from #readmissionset60
			
			Union all

			Select distinct EMPI,Code,ReadmissionDate,'Readmission 90' as TypeofEncounter,DischargeFacilityNPI,DischargeFacility,AdmissionFacilityNPI,AdmissionFacility,DischargeDate,ReadmissionDate,ReadmissionDischargeDate,null as EDVisits_Prevyear,null as AttendingProviderNPI,null as AttendingProviderName,null as LastPrimaryVisit,AdmissionDate,DRG,null as DischargeDisposition,null as LOS,null as TCMCode,AnchorDRG,null as VisitReasonCode,null as VisitReason,null as  FollowUpVisitDate,null as FU_AttendingProviderNPI,null as FU_AttendingProvider,null as FollowUpAfterDays from #readmissionset90
			
		)t1
		left outer Join RFT_NPI n1 on t1.DischargeFacilityNPI=n1.NPI and n1.Entity_Type_code=2
		left outer Join RFT_NPI n2 on t1.AdmissionFacilityNPI=n2.NPI and n2.Entity_Type_code=2
		left outer Join RFT.DRG d on t1.DRG=d.Code
		left outer Join RFT.DRG ad on t1.AnchorDRG=ad.Code
	)tbl
	Where
		rnk=1

		
	
	
	




-- Adding Loginc to create Patient Visit Summary Report

Drop table if exists #amb_pcp_procs;
select distinct
	p.CLAIM_ID
	,p.EMPI
	,p.MEMBER_ID
	,p.PROC_DATA_SRC 
into #amb_pcp_procs
from KPI_ENGINE.dbo.PROCEDURES p
where 
	p.root_companies_id=@rootId and
	p.proc_code in('G0402','G0438','G0439','G2010','G2061','G2062','G2063','99201','99202','99203','99204','99205',' 99211','99212','99213','99214','99215','99241','99242','99243','99244','99245','99304','99305','99306','99307','99308','99309','99310','99381','99382','99383','99384','99385','99386','99387','99391','99392','99393','99394','99395','99396','99397','99421','99422','99423')
	and 
	p.PROC_START_DATE between @ce_startdt and @ce_enddt

	
	Drop table if exists #amb_pcp_vsts;
	select distinct
		cl.CLAIM_ID
		,cl.EMPI
		,cl.MEMBER_ID
		,cl.CL_DATA_SRC
		,cl.ATT_NPI
		,cl.ATT_PROV_NAME
		,FROM_DATE 
		,cl.BILL_PROV
		,cl.BILL_PROV_NAME
		,cl.BILL_PROV_TIN
	into #amb_pcp_vsts
	from KPI_ENGINE.dbo.CLAIMLINE cl 
	where 
		cl.root_companies_id=@rootId and
		POS in('02','11','12','13','14','31','32','34','50') 
		and 
		cl.FROM_DATE between @ce_startdt and @ce_enddt

	
	Drop table if exists #pcpvisits
	select distinct 
		c.EMPI,
		c.FROM_DATE,
		c.ATT_NPI as AttendingProviderNPI,
		UPPER(coalesce(NULLIF(concat(n.Provider_Last_Name,' ',n.Provider_First_Name),' '),c.ATT_PROV_NAME)) as AttendingProvider,
		n.PrimarySpecialty as AttendingProviderSpecialty,
		ROW_NUMBER() over(Partition by c.EMPI order by FROM_DATE Desc) as rn 
	into #pcpvisits
	from #amb_pcp_vsts c
	join #amb_pcp_procs p on
		c.CLAIM_ID=p.CLAIM_ID and 
		c.CL_DATA_SRC=p.PROC_DATA_SRC and 
		p.EMPI=c.EMPI
	Join RFT_NPI n on c.ATT_NPI=n.NPI and n.PrimarySpecialty in('Family Medicine','General Practice','Internal Medicine','Pediatrics')
	
	


	Drop table if exists #AMB_SPEC_PROCS;
	select distinct
		p.EMPI
		,p.CLAIM_ID
		,p.MEMBER_ID
		,p.PROC_DATA_SRC
	into #AMB_SPEC_PROCS
	from KPI_ENGINE.dbo.PROCEDURES p
	where
		p.ROOT_COMPANIES_ID=@rootId and
		p.proc_code in('G0402','G0438','G0439','G2010','G2061','G2062','G2063','99201','99202','99203','99204','99205',' 99211','99212','99213','99214','99215','99304','99305','99306','99307','99308','99309','99310','99341','99342','99343','99344','99345','99347','99348','99349','99350','99381','99382','99383','99384','99385','99386','99387','99421','99422','99423')
	and
	p.PROC_START_DATE between @ce_startdt and @ce_enddt
	

	Drop table if exists #AMB_SPEC_VISITS;
	select distinct
		cl.CLAIM_ID
		,cl.EMPI
		,cl.MEMBER_ID
		,cl.CL_DATA_SRC
		,cl.ATT_NPI
		,FROM_DATE 
		,cl.ATT_PROV_NAME
	into #AMB_SPEC_VISITS
	from KPI_ENGINE.dbo.CLAIMLINE cl 
	where 
		cl.ROOT_COMPANIES_ID=@rootId and
		POS in('11','12','13','14','31','32','34') and 
		cl.FROM_DATE between @ce_startdt and @ce_enddt


	drop table if exists #specialistvisits;
	select distinct 
		c.EMPI,
		c.FROM_DATE,
		c.ATT_NPI as AttendingProviderNPI,
		UPPER(coalesce(NULLIF(concat(n.Provider_Last_Name,' ',n.Provider_First_Name),' '),c.ATT_PROV_NAME)) as AttendingProvider,
		n.PrimarySpecialty as AttendingProviderSpecialty,
		ROW_NUMBER() over(Partition by c.EMPI order by FROM_DATE Desc) as rn 
	into #specialistvisits
	from #AMB_SPEC_VISITS c
	join #AMB_SPEC_PROCS p on 
		c.CLAIM_ID=p.CLAIM_ID and 
		c.CL_DATA_SRC=p.PROC_DATA_SRC 
	Join RFT_NPI n on c.ATT_NPI=n.NPI and PrimarySpecialty in(select PreferredConceptName from RFT_TAXONOMIES where ConceptCode in('207RC0001X','207RC0000X','207RC0000X','207RG0100X','207VG0400X','207RI0011X','207RN0300X','207V00000X','207W00000X','152W00000X','207RP1001X','207RR0500X')) 
	

	Drop Table if exists #latestEDVisit
	Select EMPI,DateofEncounter into #latestEDVisit from(
	select EMPI,DateofEncounter,ROW_NUMBER() over(partition by EMPI order by DateofEncounter desc) as rn from RPT.UtilizationReport where TypeOfEncounter='ED Visit' and DateofEncounter<=@ce_enddt and ReportId=@reportId
	)t1
	where rn=1



Delete from RPT.UtilizationReport where ReportId=@reportId and ROOT_COMPANIES_ID=@rootId;
Insert into RPT.UtilizationReport(EMPI,Practice,ProviderName,Specialty,NPI,MemberLastName,MemberFirstName,MemberDOB,MemberGender,Payer,PayerId,LastVisitDate,EnrollmentStatus,TypeofEncounter,DateofEncounter,Code,DischargeFacilityNPI,DischargeFacility,AdmissionFacilityNPI,AdmissionFacility,DischargeDate,ReadmissionDate,ReadmissionDischargeDate,EDVisits_Prevyear,AttendingProviderNPI,AttendingProviderName,LastPrimaryVisit,AdmissionDate,DRG,DRGDescription,DischargeDisposition,LOS,TCMCode,AnchorDRG,AnchorDRGDescription,VisitReasonCode,VisitReason,FollowUpVisitDate,FollowUpAttendingProviderNPI,FollowUpAttendingProvider,FollowUpAfterDays,ReportId,ROOT_COMPANIES_ID,ReportStartDate,ReportEndDate,ReportQuarter,RefreshDate)
Select
	a.EMPI,a.Practice,Prov_Name,Specialty,NPI,MemberLastName,MemberFirstName,MemberDOB,Mem_Gender,Payer,PayerId,RecentVisit,EnrollmentStatus
	,TypeofEncounter
	,DateofEncounter
	,Code
	,DischargeFacilityNPI
	,DischargeFacility
	,AdmissionFacilityNPI
	,AdmissionFacility
	,DischargeDate
	,ReadmissionDate
	,ReadmissionDischargeDate
	,EDVisits_Prevyear
	,AttendingProviderNPI
	,AttendingProviderName
	,LastPrimaryVisit
	,AdmissionDate
	,DRG
	,DRGDescription
	,DischargeDisposition
	,LOS
	,TCMCode
	,AnchorDRG
	,AnchorDRGDescription
	,VisitReasonCode
	,VisitReason
	,FollowUpVisitDate
	,FU_AttendingProviderNPI
	,FU_AttendingProvider
	,FollowUpAfterDays
	,@reportId
	,@rootId
	,@startDate
	,@enddate
	,@quarter
	,GetDate()
From #consolidatedencounters e
join RPT.ConsolidatedAttribution_Snapshot a on 
	e.EMPI=a.EMPI and 
	a.Attribution_Type='Ambulatory_PCP'
Where
	a.AssignedStatus='Assigned'

	


	

drop table if exists #patientVisitSummary;
Select t1.*,ed.DateofEncounter as LastEDVisit,p.FROM_DATE as LastPCPVisitDate,s.FROM_DATE as LastSpecialistDate into #patientVisitSummary from(
	Select EMPI,sum(ED) as ED,sum(IP) as IP,sum(Readmission90) as Readmission90,sum(Readmission60) as Readmission60,sum(Readmission30) as Readmission30,sum(PCPVists)as PCPVists,sum(SpecialistVists) as SpecialistVists
From
(
	select EMPI,count(*) as ED,0 as IP,0 as Readmission90,0 as Readmission60,0 as Readmission30,0 as PCPVists,0 as SpecialistVists from RPT.UtilizationReport where TypeOfEncounter='ED Visit' and ReportId=@reportId Group by EMPI

	Union all

	select EMPI,0,count(*) as IP,0,0,0,0,0 from RPT.UtilizationReport where TypeOfEncounter='Admission' and ReportId=@reportId Group by EMPI

	

	Union all

	select EMPI,0,0,count(*) as Readmission90,0,0,0,0 from RPT.UtilizationReport where TypeOfEncounter='Readmission 90' and ReportId=@reportId Group by EMPI

	Union all

	select EMPI,0,0,0,count(*) as Readmission60,0,0,0 from RPT.UtilizationReport where TypeOfEncounter='Readmission 60' and ReportId=@reportId Group by EMPI

	Union all

	select EMPI,0,0,0,0,count(*) as Readmission30,0,0 from RPT.UtilizationReport where TypeOfEncounter='Readmission 30' and ReportId=@reportId Group by EMPI

	Union all

	select EMPI,0,0,0,0,0,count(*),0 from #pcpvisits  Group by EMPI

	Union all

	select EMPI,0,0,0,0,0,0,count(*) from #specialistvisits  Group by EMPI
)tbl
Group by EMPI
)t1
Left Outer Join #latestEDVisit ed on t1.EMPI=ed.EMPI
Left Outer Join #pcpvisits p on t1.EMPI=p.EMPI and p.rn=1
Left Outer Join #specialistvisits s on t1.EMPI=s.EMPI and s.rn=1


delete from KPI_ENGINE_MART.RPT.PatientVisitSummary where ROOT_COMPANIES_ID=@rootId and ReportId=@reportId
insert into KPI_ENGINE_MART.RPT.PatientVisitSummary(EMPI,ED,IP,Readmission90,Readmission60,Readmission30,PCP,Specialists,LastEDVisit,LastPCPVisit,LastSpecialistVisit,ReportId,ReportStartDate,ReportEndDate,ReportQuarter,ROOT_COMPANIES_ID)
select
	EMPI,
	ED,
	IP,
	Readmission90,
	Readmission60,
	Readmission30,
	PCPVists,
	SpecialistVists,
	LastEDVisit,
	LastPCPVisitDate,
	LastSpecialistDate,
	@reportId,
	@startdate,
	@enddate,
	@quarter,
	@rootId
from #patientVisitSummary




		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END







GO
