SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE Proc [dbo].[KPI_HCC_LIST]
AS


BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


Declare @rundate date=GetDate()
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159


Declare @ce_startdt Date
Declare @ce_enddt Date
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @reportId INT;

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

set @ce_startdt=concat(@meas_year-2,'-01-01');
set @ce_enddt=@enddate;


-- Get Report id , Start Date and Quarter

--Identify visits for risk scoring
Drop table if exists #hcc_diagvisits;
Create Table #hcc_diagvisits
(
	EMPI varchar(100),
	ServiceDate Date,
	ATT_NPI varchar(20),
	ATT_PROV_NAME varchar(200),
	ATT_PROV_SPEC varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #hcc_diagvisits
Select distinct
	EMPI
	,ServiceDate
	,ATT_NPI
	,ATT_PROV_NAME
	,ATT_PROV_SPEC
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		c.EMPI
		,c.FROM_DATE as ServiceDate
		,c.ATT_NPI
		,c.ATT_PROV_NAME
		,c.ATT_PROV_SPEC
		,CL_DATA_SRC as DATA_SRC
		,c.CLAIM_ID
	From CLAIMLINE c
	Join PROCEDURES p on
		c.EMPI=p.EMPI and
		c.CLAIM_ID=p.CLAIM_ID and
		ISNULL(c.SV_LINE,0)=ISNULL(p.SV_LINE,0) and
		c.ROOT_COMPANIES_ID=p.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		ISNULL(c.POS,'0')!='81' and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		p.PROC_START_DATE between @ce_startdt and @ce_enddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Observation','ED','Nonacute Inpatient','Acute Inpatient')
		)


	Union all

	Select
		c.EMPI
		,c.FROM_DATE as ServiceDate
		,c.ATT_NPI
		,c.ATT_PROV_NAME
		,c.ATT_PROV_SPEC
		,CL_DATA_SRC as DATA_SRC
		,c.CLAIM_ID
	From CLAIMLINE c
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		ISNULL(c.POS,'0')!='81' and
		c.FROM_DATE between @ce_startdt and @ce_enddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
		)


	Union All

	Select
		c.EMPI
		,coalesce(c.DIS_DATE,c.TO_DATE) as ServiceDate
		,c.ATT_NPI
		,c.ATT_PROV_NAME
		,c.ATT_PROV_SPEC
		,CL_DATA_SRC as DATA_SRC
		,c.CLAIM_ID
	From Procedures p
	Join Claimline c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(C.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		ISNULL(POS,'0')!='81' and
		coalesce(c.DIS_DATE,TO_DATE) between @ce_startdt and @ce_enddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
		)

	Union all


	Select
		c.EMPI
		,coalesce(c.DIS_DATE,c.TO_DATE) as ServiceDate
		,c.ATT_NPI
		,c.ATT_PROV_NAME
		,c.ATT_PROV_SPEC
		,CL_DATA_SRC as DATA_SRC
		,c.CLAIM_ID
	From CLAIMLINE c
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt and @ce_enddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		)



)t1



drop table if exists #hcc_diagnosislist;
Create table #hcc_diagnosislist
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT,
	DIAG_DATA_SRC varchar(50)

)
Insert into #hcc_diagnosislist
Select 
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO
	,DIAG_DATA_SRC
From Diagnosis
Where
	ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	DIAG_START_DATE between @ce_startdt and @ce_enddt


drop table if exists #hcc_diagnosis;
Create table #hcc_diagnosis
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	DIAG_DATA_SRC varchar(50),
	ATT_NPI varchar(20),
	ATT_PROV_NAME varchar(200),
	ATT_PROV_SPEC varchar(100)

)
Insert into #hcc_diagnosis
Select distinct
	d.EMPI
	,d.diagnosis
	,d.DiagnosisDate
	,d.DIAG_DATA_SRC
	,v.ATT_NPI
	,v.ATT_PROV_NAME
	,v.ATT_PROV_SPEC
From #hcc_diagnosislist d
Join CLAIMLINE v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DIAG_DATA_SRC=v.CL_DATA_SRC



	

	Drop table if exists #hcc_codes
	Create Table #hcc_codes
	(
		EMPI varchar(100),
		DiagnosisDate Date,
		NPI varchar(20),
		ProviderName varchar(200),
		Specialty varchar(100),
		DiagnosisCode varchar(10),
		hcc varchar(10),
		Description varchar(200),
		HCCGroup varchar(100),
		HCCType varchar(20),
		HCCWeight float

	)
	Insert into #hcc_codes
	select  
		EMPI
		,DiagnosisDate
		,NPI
		,ProviderName
		,Specialty
		,DiagnosisCode
		,HCC
		,Description
		,RankingGroup as HCCGroup
		,HCCType
		,HCCWeight
	from 
	(
		Select
			*
			,row_number() over(partition by EMPI,RankingGroup order by Rank,DiagnosisDate desc) as calcrank
			,row_number() over(partition by EMPI,HCC order by DiagnosisDate desc) as hccct
		From
		(
			select 
				d.EMPI
				,d.DiagnosisDate
				,d.diagnosis as DiagnosisCode
				,d.ATT_NPI as NPI
				,coalesce(concat(n.Provider_Last_Name,' ',n.Provider_First_Name),ATT_PROV_NAME) as ProviderName
				,coalesce(ATT_PROV_SPEC,t.PreferredConceptName) as Specialty
				,concat('HCC-',hcc.HCC) as HCC
				,coalesce(hccr.RankingGroup,dhcc.hccdescription) as RankingGroup
				,ISNULL(hccr.Rank,1) as Rank
				,coalesce(hccr.Description,hcc.description) as Description
				,dhcc.HCCType
				,dhcc.HCCWeight
			from #hcc_diagnosis d
			join RFT.HCCMapping hcc on d.diagnosis=hcc.diagnosiscode 
			left outer join HDS.HEDIS_HCC_RANK hccr on 
				concat('HCC-',hcc.HCC)=hccr.HCC and
				hccr.HCC_type='Shared'
			Left outer join RFT.HCC dhcc on
				concat('HCC-',hcc.HCC)=dhcc.hcc
			Left outer join  RFT_NPI n on 
				d.ATT_NPI=n.NPI and 
				n.Entity_type_code=1
			Left outer join RFT_Taxonomies t on 
				n.Healthcare_Provider_Taxonomy_Code_1=t.ConceptCode
		--	where EMPI='284D7540-62F5-4CB9-8583-D69994B142ED'

		)t1
	)t2 
	where 
		calcrank=1 
		-- added on 7-22-2021 to remove duplicate HCC
		and hccct=1
	Order by HCC
	
		-- HCC RANK COMB Identify if a Member Risk increases based on HCC Combinations
		drop table if exists #hcc_rankcmb;
		Create table #hcc_rankcmb
		(
			EMPI varchar(100),
			DiagnosisDate Date,
			NPI varchar(20),
			ProviderName varchar(200),
			Specialty varchar(100),
			DiagnosisCode varchar(10),
			hcc varchar(10),
			Description varchar(200),
			HCCGroup varchar(200),
			HCCType varchar(20),
			HCCWeight float
		)
		Insert into #hcc_rankcmb
		Select
			EMPI
			,DiagnosisDate
			,NPI
			,ProviderName
			,Specialty
			,DiagnosisCode
			,HCC
			,Description
			,HCCGroup
			,HCCType
			,HCCWeight
		From
		(
			select 
				h.EMPI
				,null as DiagnosisDate
				,null as NPI
				,null as ProviderName
				,null as Specialty
				,null as DiagnosisCode
				,cmb.HCCCombDescription as Description
				,cmb.hcccomb as HCC
				,h.HCCGroup
				,hcc.HCCType
				,hcc.HCCWeight
				,row_number() over(partition by EMPI,hcccomb order by DiagnosisDate Desc) as rn
			from #hcc_codes h 
			join HDS.HEDIS_HCC_COMB cmb on 
				cmb.ComorbidHCC1 in
				(
					select 
						HCC 
					from #hcc_codes t1 
					where 
						t1.EMPI=h.EMPI
				) 
				and 
				cmb.ComorbidHCC2 in
				(
					select 
						HCC 
					from #hcc_codes t1 
					where 
						t1.EMPI=h.EMPI
				)
				and 
				cmb.hcc_type='Shared'
			Left outer Join RFT.HCC hcc on
				cmb.HCCComb=hcc.HCC
		--	where EMPI='d638795a-006c-465b-b18f-77b7a845a0e8'
			
			

		)t1
		where
			rn=1


	-- Combining HCC and Comb

		drop table if exists #hcc_total;
		Create table #hcc_total
		(
			EMPI varchar(100),
			DiagnosisDate Date,
			NPI varchar(20),
			ProviderName varchar(200),
			Specialty varchar(100),
			DiagnosisCode varchar(10),
			hcc varchar(10),
			Description varchar(200),
			HCCGroup varchar(200),
			HCCType varchar(20),
			HCCWeight float
		)
		Insert into #hcc_total
		Select
			*
		From
		(
			Select
				*
			From #hcc_codes

			Union all

			Select 
				*
			From #hcc_rankcmb
		)t1



	Delete from RPT.HCC_LIST where ROOT_COMPANIES_ID=@rootId --ReportId=@reportId and ;

	Insert into RPT.HCC_LIST(EMPI,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,MemberFirstName,MemberLastName,MemberMiddleName,MemberDOB,MemberGender,Payer,PayerId,LastVisitDate,EnrollmentStatus,DiagnosisCode,hcc,Description,HCCGroup,HCCType,HCCWeight,DiagnosisDate,DiagnosingProviderNPI,ProviderName,Specialty,Status,ReportId,ROOT_COMPANIES_ID,ReportStartDate,ReportEndDate,ReportCreatedQuarter,ReportCreatedDate)
	Select
		*
	From
	(
		Select
			hcc.EMPI
			,a.Practice as PracticeName
			,a.Prov_Name as AttributedProviderName
			,a.Specialty as AttributedProviderSpecialty
			,a.NPI as AttributedProviderNPI
			,a.MemberFirstName
			,a.MemberLastName
			,a.MemberMiddleName
			,a.MemberDOB
			,a.MEM_GENDER
			,a.Payer
			,a.PayerId
			,a.RecentVisit as LastVisitDate
			,a.EnrollmentStatus
			,hcc.DiagnosisCode
			,hcc.hcc
			,hcc.Description
			,hcc.HCCGroup
			,HCCType
			,HCCWeight
			,hcc.DiagnosisDate
			,hcc.NPI
			,hcc.ProviderName
			,hcc.Specialty
			,Case 
				When Year(hcc.DiagnosisDate)=Year(@ce_enddt) Then 'Closed'
				When Year(hcc.DiagnosisDate)=Year(@ce_enddt)-1 Then 'Open'
				When HCCType='Interaction' Then 'Open'
				When Year(hcc.DiagnosisDate)=Year(@ce_enddt)-2 and HCCType='Chronic' Then 'Potential'
			End as Status
			,@reportId as ReportId
			,@rootId as ROOT_COMPANIES_ID
			,@ce_startdt as ReportStartDate
			,@ce_enddt as ReportEndDate
			,@quarter as ReportCreatedQuarter
			,GetDATE() as ReportCreatedDate
		From #hcc_total hcc
		Join RPT.ConsolidatedAttribution_Snapshot a on 
			hcc.EMPI=a.EMPI and
			a.Attribution_Type='Ambulatory_PCP'
	)tbl
	Where
		Status in('Potential','Closed','Open')
		 

		/* commented as we are using rick scores from ACG
		-- Generate Risk Score based on Age and Gender
		Drop table if exists #hcc_DemoRiskScore
		Create Table #hcc_DemoRiskScore
		(
			EMPI varchar(100),
			genagegroup varchar(20),
			reportingindicator varchar(20),
			productline varchar(20),
			Weight decimal(10,2)
		)
		Insert into #hcc_DemoRiskScore
		select
			EMPI_ID
			,genagegroup
			,t2.reportingindicator
			,t2.productline
			,ISNULL(Weight,0) as weight
		From
		(
			Select
				*
				,case 
					when age between 18 and 44 and gender='F' then 'F_18-44'
					when age between 45 and 54 and gender='F' then 'F_45-54'
					when age between 55 and 64 and gender='F' then 'F_55-64'
					when age between 65 and 74 and gender='F' then 'F_65-74'
					when age between 75 and 84 and gender='F' then 'F_75-84'
					when age >=85 and gender='F' then 'F_85'
					when age between 18 and 44 and gender='M' then 'M_18-44'
					when age between 45 and 54 and gender='M' then 'M_45-54'
					when age between 55 and 64 and gender='M' then 'M_55-64'
					when age between 65 and 74 and gender='M' then 'M_65-74'
					when age between 75 and 84 and gender='M' then 'M_75-84'
					when age >=85 and gender='M' then 'M_85'
				end as genagegroup
				,case 
					when payer in('MSSP','Humana') and age between 18 and 64 then 'Standard - 18-64'
					when payer in('MSSP','Humana') and age>=65 then 'Standard - 65+'
					else 'Standard - 18+'
				end as reportingindicator
				,case
					When payer in('MSSP','Humana') then 'Medicare'
					else 'Commercial'
				end as productline
			From
			(
				select 
					EMPI_ID
					,Gender
					,Data_Source as Payer
					,CASE 
						WHEN dateadd(year, datediff (year, Date_of_Birth,@enddate), Date_of_Birth) > @enddate THEN datediff(year, Date_of_Birth, @enddate) - 1
						ELSE datediff(year, Date_of_Birth,@enddate)
					END as Age
				From open_empi_master
			)t1
		)t2
		left outer Join HDS.Hedis_Risk_adjustment r on 
			t2.genagegroup=r.VariableName and
			t2.reportingindicator=r.reportingindicator and
			t2.productline=r.productline and
			VariableType='Demo' and
			Measure_ID='PCR' 






			-- Generate Comorbid Conditions Risk Adjustment Scores

			Drop Table if exists #hcc_conditionsRiskScore
			Create table #hcc_conditionsRiskScore
			(
				EMPI varchar(100),
				Weight Decimal(10,2)
			)
			Insert into #hcc_conditionsRiskScore
			select 
				d.EMPI 
				,sum(hcc.hccweight) as ComorbidRiskScore
			From #hcc_total d
			join #hcc_DemoRiskScore s on d.EMPI=s.EMPI
			join RFT.HCC hcc on
				d.hcc=hcc.HCC
			Group by
				d.EMPI



			-- Insert data into RPT.HCC_RiskScores
			Delete from RPT.HCC_RiskScores where ReportId=@reportId and ROOT_COMPANIES_ID=@rootId

			Insert into RPT.HCC_RiskScores(EMPI,DemoRiskScore,ComorbidRiskScore,ReportId,ROOT_COMPANIES_ID)
			select 
				a.EMPI
				,ISNULL(d.Weight,0) as DemoRiskScore
				,ISNULL(c.Weight,0) as ComorbidRiskScore
				,@reportId
				,@rootId
			from RPT.PCP_ATTRIBUTION a
			left outer join #hcc_DemoRiskScore d on a.EMPI=d.EMPI
			left outer join #hcc_conditionsRiskScore c on a.EMPI=c.EMPI
			where 
				a.ReportId=@reportId
				
*/
			

		
		-- Updated Status for Interactions
		Exec SP_KPI_DROPTABLE '#hcc_closedinteractions'
		Select 
			l1.EMPI,
			HCCComb
		into #hcc_closedinteractions
		from HDS.HEDIS_HCC_COMB c
		Join RPT.HCC_List l1 on
			c.ComorbidHCC1=l1.hcc and
			l1.Status='Closed' 
		Join RPT.HCC_List l2 on
			l1.EMPI=l2.EMPI and
			l2.Status='Closed' and
			c.ComorbidHCC2=l2.hcc
		Where
			c.HCC_type='Shared'


			Update l Set l.Status='Closed' from RPT.HCC_List l join #hcc_closedinteractions i on l.EMPI=i.EMPI and l.hcc=i.HCCComb





		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END
			
GO
