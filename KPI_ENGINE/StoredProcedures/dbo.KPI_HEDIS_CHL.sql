SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
/****** Object:  StoredProcedure [HDS].[CHL]    Script Date: 15-12-2020 08:33:46 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE proc [dbo].[KPI_HEDIS_CHL]
AS


BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY



declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

DECLARE @ce_startdt Date
DECLARE @ce_startdt1 Date
DECLARE @ce_enddt Date
DECLARE @ce_enddt1 Date
DECLARE @meas VARCHAR(10);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target float;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;
 

SET @meas='CHL';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
Set @measurename='Patients 16-24 years of age that had a chlamydia screening test in the last 12 months'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=38.06
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult & Pediatric Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='21'




drop table if exists #CHL_memlist; 
CREATE TABLE #CHL_memlist 
(
    EMPI varchar(100)    
);
insert into #CHL_memlist
SELECT DISTINCT 
	gm.EMPI_ID
FROM open_empi_master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=159 and 
	Gender='F' and 
	en.EFF_DATE<=@ce_enddt and
	TERM_DATE>=@ce_startdt	AND 
	YEAR(@ce_enddt)-YEAR(gm.Date_of_Birth) BETWEEN 16 AND 24




-- Create Temp Patient Enrollment
drop table if exists #CHL_tmpsubscriber;
CREATE TABLE #CHL_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #CHL_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,en.Payer_Type
	,en.EFF_DATE
	,en.TERM_DATE  
FROM Open_empi_Master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=159 and 
	Gender='F' and 
	en.EFF_DATE<=@ce_enddt and
	TERM_DATE>=@ce_startdt	AND 
	YEAR(@ce_enddt)-YEAR(gm.Date_of_Birth) BETWEEN 16 AND 24


Drop table if exists #CHLdataset;
CREATE TABLE #CHLdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  gender varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT 0,
) ;
Insert into #CHLdataset(EMPI,meas,Payer,Gender,Age)
Select distinct
	EMPI
	,'CHL'
	,Payer
	,Gender
	,Year(@ce_enddt)-Year(dob)
from #CHL_tmpsubscriber

-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #chl_contenroll;	
	CREATE table #chl_contenroll
	(
		EMPI varchar(100),
		CE INT
	);
	Insert into #chl_contenroll
	SELECT 
		EMPI
		,SUM(ceflag) AS CE 
	FROM
	(
	
		-- CE Check for 1st year
		SELECT 
			EMPI
			,SUM(coverage) AS coverage
			, 1 AS ceflag 
		FROM
		(
			SELECT 
				EMPI
				,PAYER_TYPE
				,start_date
				,end_date
				,DATEDIFF(day,start_date,end_date)+1 AS coverage 
			FROM
			(

				SELECT 
					s.EMPI
					,s.PAYER_TYPE
					,s.TERM_DATE
					,CASE
						WHEN EFF_DATE<@ce_startdt THEN @ce_startdt
						ELSE EFF_DATE
					END AS start_date
					,CASE
						WHEN TERM_DATE>@ce_enddt THEN @ce_enddt
						ELSE TERM_DATE
					END AS end_date
				FROM ENROLLMENT s
				JOIN 
				(
					SELECT 
						EMPI
						,SUM(diff) as diff 
					FROM
					(
						SELECT 
							EMPI
							,nextstartdate
							,TERM_DATE
							,CASE
								WHEN DATEDIFF(day,TERM_DATE,nextstartdate)<=1 THEN 0
								ELSE 1
							END AS diff
						 FROM
						 (
							 SELECT 
								EMPI
								,EFF_DATE
								,TERM_DATE
								,CASE
									WHEN nextstartdate IS NULL THEN TERM_DATE
									ELSE nextstartdate
								END AS nextstartdate FROM
								(
									SELECT 
										s.EMPI
										,EFF_DATE
										,TERM_DATE
										,(		
											SELECT Top 1 
												s1.EFF_DATE
											FROM ENROLLMENT s1
											WHERE 
												s1.ROOT_COMPANIES_ID=s.ROOT_COMPANIES_ID AND 
												s1.EMPI=s.EMPI AND 
												s.TERM_DATE<s1.EFF_DATE AND 
												Convert(date,s1.EFF_DATE)<=@ce_enddt AND 
												Convert(date,s1.TERM_DATE)>=@ce_startdt
											ORDER BY 
												s1.EMPI
												,s1.EFF_DATE
												,s1.TERM_DATE
										) AS nextstartdate
									 FROM ENROLLMENT s
									WHERE 
										ROOT_COMPANIES_ID=@rootId AND 
										s.EFF_DATE<=@ce_enddt AND 
										s.TERM_DATE>=@ce_startdt
								)t
							)t1 
						)t2 
						GROUP BY 
							EMPI 
						HAVING 
							SUM(diff)<=1
					)
					s1 ON 
						s.EMPI=s1.EMPI
					WHERE 
						s.ROOT_COMPANIES_ID=@rootId	AND 
						s.EFF_DATE<=@ce_enddt AND 
						s.TERM_DATE>=@ce_startdt
	
				)t2
			)t3 
			GROUP BY 
				EMPI 
			HAVING 
			sum(coverage)>=(DATEPART(dy, @ce_enddt)-45)

		Union All

		-- Anchor Date
	 
		SELECT 
			EMPI
			,SUM(DATEDIFF(day,start_date,end_date)) AS coverage
			, 1 AS ceflag 
		FROM
		(
			SELECT 
				EMPI
				,TERM_DATE
				,CASE
					WHEN EFF_DATE<@ce_startdt THEN @ce_startdt
					ELSE EFF_DATE
				END AS start_date
				,CASE
					WHEN TERM_DATE>@ce_enddt THEN @ce_enddt
					ELSE TERM_DATE
				END AS end_date
			FROM ENROLLMENT
			WHERE 
				ROOT_COMPANIES_ID=@rootId and
				 @ce_enddt BETWEEN EFF_DATE AND TERM_DATE
		)t4  
		GROUP BY 
			EMPI
 
	)cont_enroll 
	GROUP BY 
		EMPI  
	HAVING
		SUM(ceflag)=2 
	order by 
		EMPI;
	
	update #chldataset set CE=1 from #chldataset ds join #chl_contenroll ce on ds.EMPI=ce.EMPI;


-----Event Method 1:Claim/Encounter Data
	exec dbo.sp_kpi_droptable '#Event'
	CREATE table #Event
	(
		EMPI varchar(100)			
	);
	insert into #Event
	Select distinct
		EMPI
	From
	(
		
		Select 
			EMPI
		From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Pregnancy')

		Union all

		Select 
			EMPI
		From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Sexual Activity')

		Union all

		Select 
			EMPI
		From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Sexual Activity')

		--Event Method 2:Pharmacy Data
	
		union all

		select 
			EMPI
		from MEDICATION
		where 
			ROOT_COMPANIES_ID=@rootId  and 
			Fill_Date between @ce_startdt and @ce_enddt and 
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications'
			)
	)t1

update #chldataset set [Event]=1 from #chldataset ds join #Event E on ds.EMPI=E.EMPI;




-- Hospice Exclusion

	
	drop table if exists #hospicemembers;
	CREATE table #hospicemembers
	(
		EMPI varchar(100)
		
	);
	Insert into #hospicemembers
	Select distinct
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	
	update #CHLdataset set rexcl=1 from #CHLdataset ds join #hospicemembers hos on hos.EMPI=ds.EMPI;




drop table if exists #num;
CREATE table #num
(
	EMPi varchar(100),
	ServiceDate Date,
	Code varchar(20)
);
Insert into #num
Select distinct
	EMPI
	,ServiceDate
	,Code
From
(

	Select
		EMPI
		,ResultDate as ServiceDate
		,TestCode as Code
	From Lab
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		TestCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
		)

	Union All

	Select
		EMPI
		,Resultdate as ServiceDate
		,ResultCode as Code
	From Lab
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		ResultCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
		)

	Union all

	Select
		EMPI
		,Proc_start_date as ServiceDate
		,Proc_code as code
	From Procedures
	Where
		ROOT_COMPANIES_ID=@rootId and
		PROC_START_DATE between @ce_startdt and @ce_enddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
		)

)t1	
	
update #CHLdataset set num=1 from #CHLdataset ds join #num num on num.EMPI=ds.EMPI ;

-- Create Num Details
drop table if exists #chl_numdetails;
CREATE table #chl_numdetails
(
	EMPi varchar(100),
	ServiceDate Date,
	Code varchar(20)
);
Insert into #chl_numdetails
Select
	EMPI
	,ServiceDate
	,Code
From
(
	Select
		*
		,row_number() over(Partition by EMPI order by ServiceDate Desc) as rn
	From #num
)t1
Where
	rn=1



exec dbo.sp_kpi_droptable '#Pregnancy_Tests_Members'
CREATE table #Pregnancy_Tests_Members
(
	EMPI varchar(100)
);
Insert into #Pregnancy_Tests_Members
Select distinct
	EMPI
From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Pregnancy Tests')






-----COMPLUSORY2
exec dbo.sp_kpi_droptable '#Pregnancy_Tests_Exclusion'
CREATE table #Pregnancy_Tests_Exclusion
(
	EMPI varchar(100),
	Date_S  date
);
Insert into #Pregnancy_Tests_Exclusion
Select distinct
	EMPI
	,ServiceDate
From
(

	Select
		EMPI
		,ResultDate as ServiceDate
	From Lab
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		TestCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)

	Union All

	Select
		EMPI
		,ResultDate as ServiceDate
	From Lab
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		ResultCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
	From Procedures
	Where
		ROOT_COMPANIES_ID=@rootId and
		PROC_START_DATE between @ce_startdt and @ce_enddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)

)t1	




-----BIT 1.2
exec dbo.sp_kpi_droptable '#Medication_XRAY'
CREATE table #Medication_XRAY
(
	EMPI varchar(100),
	DATE_S Date
);
Insert into #Medication_XRAY
Select distinct
	EMPI
	,ServiceDate
From
(
	Select
		EMPI
		,FILL_DATE as ServiceDate
	From MEDICATION
	Where
		ROOT_COMPANIES_ID=@rootId and
		FILL_DATE between @ce_startdt and DATEADD(d,6,@ce_enddt) and
		MEDICATION_CODE in
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Retinoid Medications'
		)

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Diagnostic Radiology')
)t1





exec dbo.sp_kpi_droptable '#Exclusion_Window'
CREATE table #Exclusion_Window
(
	EMPI varchar(100)
);
insert into #Exclusion_Window
Select Distinct 
	A.EMPI 
from #Pregnancy_Tests_Exclusion A
Inner join #Medication_XRAY B on 
	A.EMPI=B.EMPI and  
	B.Date_S BETWEEN A.Date_S AND dateadd(d,6,A.Date_S)


/*

------EXcl_Preg_Test for optional Exclusion
	exec dbo.sp_kpi_droptable '#Preg_sexualActivity_Rx_Contraceptive'
	CREATE table #Preg_sexualActivity_Rx_Contraceptive
	(
		EMPI varchar(100)
			
	);
	insert into #Preg_sexualActivity_Rx_Contraceptive
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between @ce_startdt and @ce_enddt and Date_S!='' AND SuppData='N'
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	)
Union
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between @ce_startdt and @ce_enddt and Date_S!='' AND SuppData='N'
 AND( 
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	or 
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	or
	CPT2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	)
union
select distinct Memid from HDS.HEDIS_PHARM where Measure_id='CHL_TEST'  AND SuppData='N' and PrServDate!='' and  convert(date,PrServDate) between @ce_startdt and @ce_enddt and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications')




-----CREATE CLUSTERED INDEX idx_Excl_Preg_Test ON #Excl_Preg_Test ([memid]);
*/

------EXcl_Preg_Test for optional Exclusion
	exec dbo.sp_kpi_droptable '#EXCL'
	CREATE table #EXCL
	(
		EMPI varchar(100)
			
	);
	insert into #EXCL
	select  
		A.EMPI 
	from 
	(
		select Distinct 
			A.EMPI
			,1 as Excl 
		from #Pregnancy_Tests_Members A
		left join #Event B on 
			A.EMPI=B.EMPI
		inner join #Exclusion_Window C on 
			A.EMPI=C.EMPI 
		where 
			B.EMPI is null
) A



update #CHLdataset set EXCL=1 from #CHLdataset ds join #EXCL E on E.EMPI=ds.EMPI;




exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


Delete from HDS.HEDIS_MEASURE_OUTPUT where measure_id='CHL' and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
SELECT EMPI AS memid,meas,payer,CE, [EVENT],CASE WHEN CE='1'  AND EVENT='1' AND rexcl='0' and rexcld='0' AND PAYER not in ('MCS','MC','MCR','MP','MC','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,'CHL' as Measure_ID,@meas_year,@reportId,@rootId FROM #CHLdataset



-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0  and Event= 1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #chldataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #chl_numdetails nd on d.EMPI=nd.EMPI
	where 
		d.Event=1



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(SUM(Cast(DEN_Excl as Float)),1))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target


SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
