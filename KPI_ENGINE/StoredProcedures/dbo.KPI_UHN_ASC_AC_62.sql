SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [dbo].[KPI_UHN_ASC_AC_62]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

-- Declare Variables
declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @target float;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;

-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');



Set @reporttype='Network'
Set @measurename='Ambulatory Sensitive Conditions - Acute Composite (Dehydration, Bacterial Pneumonia, or UTI)'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=1.77
Set @domain='Utilization Management'
Set @subdomain='Ambulatory Sensitive Conditions'
Set @measuretype='Calculated'
Set @measure_id=62




-- Identify Deaceased Members
Drop table if exists #62_deceasedmembers
Create table #62_deceasedmembers
(
	EMPI varchar(100)
)
Insert into #62_deceasedmembers
select * from deceasedmembers(@rootId,@ce_startdt,@ce_enddt)

-- Identify Hospice exclusions
drop table if exists #62_hospicemembers;
CREATE table #62_hospicemembers
(
	EMPI varchar(50)
		
);
Insert into #62_hospicemembers
select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)



-- Identify members who are 12 years and older as on the Reporting Month
drop table if exists #62_denominatorset;
Create table #62_denominatorset
(
	EMPI varchar(100),
	MEMBER_ID varchar(100),
	Gender varchar(10),
	Age INT
)
Insert into #62_denominatorset
Select * from(
select distinct
	o.EMPI_ID
	,Org_Patient_Extension_ID as MEMBER_ID
	,Gender
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,eomonth(@ce_enddt)), Date_of_Birth) > eomonth(@ce_enddt) THEN datediff(year, Date_of_Birth, eomonth(@ce_enddt)) - 1
		ELSE datediff(year, Date_of_Birth, eomonth(@ce_enddt))
	END as Age
from open_empi_master o
left outer join #62_deceasedmembers d on EMPI_ID=d.EMPI
where o.Root_Companies_ID=@rootId and
	  d.EMPI is null
)t1 
where age >=18



-- Identify Members with Inpatient Stay in Measurement Year
Drop table if exists #62_inpatientStays;
Create Table #62_inpatientStays
(
	EMPI varchar(100),
	ServiceDate Date,
	ADM_DATE Date,
	DIS_DATE Date,
	Claim_id varchar(100),
	CL_DATA_SRC varchar(20)
	
)
Insert into #62_inpatientStays
Select
	*
From
(
	select distinct 
		EMPI
		,FROM_DATE
		,Case
			When ADM_DATE is null then FROM_DATE
			else ADM_DATE
		end as ADM_DATE
		,Case
			When DIS_DATE is null then TO_DATE
			Else DIS_DATE
		End as DIS_DATE
		,Claim_id
		,CL_DATA_SRC 
	from CLAIMLINE v 
	where ISNULL(v.POS,'')!='81' and 
		  v.DIS_DATE!='' and 
		  RIGHT('0000'+CAST(Trim(v.REV_CODE) AS VARCHAR(4)),4) in 
		  (
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		  )
)t1
where
DIS_DATE between @ce_startdt and @ce_enddt 


-- Identify Non Acute Inpatient Stays in Measurement Year
Drop table if exists #62_nonacutestays
Create Table #62_nonacutestays
(
	EMPI varchar(100),
	ServiceDate Date,
	ADM_DATE Date,
	DIS_DATE Date,
	Claim_id varchar(100),
	CL_DATA_SRC varchar(20)
	
)
Insert into #62_nonacutestays
Select * from
(
	select distinct 
		EMPI
		,FROM_DATE
		,ADM_DATE
		,Case
			When DIS_DATE is null then To_Date
			Else DIS_DATE
		end as DIS_DATE
		,Claim_id
		,CL_DATA_SRC 
	from CLAIMLINE 
	where ISNULL(POS,'')!='81' and 
		 
		 (
			REV_CODE in 
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
			) 
			or 
			RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
			)
		)
)t1
where DIS_DATE between @ce_startdt and @ce_enddt



-- Identify Acute Stays
Drop table if exists #62_acuteStays;
Create Table #62_acuteStays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	Claim_id varchar(100)

	
)
Insert into #62_acuteStays
select distinct
	inp.EMPI
	,inp.ADM_DATE
	,inp.DIS_DATE
	,inp.Claim_id
from #62_inpatientStays inp
left outer join #62_nonacutestays na on	inp.Claim_id=na.Claim_id and 
										inp.CL_DATA_SRC=na.CL_DATA_SRC
where na.EMPI is null




--identify Discharges with Dehydration,Pneumonia and UTI
Drop table if exists #62_dischargeconditions
Create Table #62_dischargeconditions
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	Code varchar(20),
	DateofService Date
)
Insert into #62_dischargeconditions
	select distinct 
		d.EMPI
		,d.CLAIM_ID
		,d.DIAG_CODE
		,d.DIAG_START_DATE
	from diagnosis d
	Join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
						d.DIAG_DATA_SRC=c.CL_DATA_SRC and
						d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
	where
		(
			 Diag_code in
			 (
				select code_new from KPI_ENGINE.HDS.VALUESET_TO_CODE where value_set_name in('Bacterial Pneumonia','Urinary Tract Infection')
			 )
			 or
			 Diag_code='E860'
		)
		 and 
		 diag_seq_no=1 and
		 ISNULL(c.POS,'')!='81'


--Identify Transfers
drop table if exists #62_directtransfers;
Create table #62_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid varchar(600)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #62_acuteStays

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #62_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1
Where DischargeDate between @ce_startdt and @ce_enddt
	  

-- Identify Numerator Set
Drop table if exists #62_numeratorset
Create table #62_numeratorset
(
	EMPI varchar(100),
	ADM_DATE DATE,
	DIS_DATE DATE,
	Code varchar(20),
	DateOfService Date
)
Insert into #62_numeratorset
Select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,Code
	,DateOfService
From
(
	Select 
		*
		--,DENSE_RANK() over(Partition by EMPI,DIS_DATE order by DateOfService Desc,rn desc) as rnk
		,DENSE_RANK() over(Partition by EMPI order by DateOfService Desc,rn desc) as rnk
	From
	(
		select distinct 
			s.EMPI
			,s.ADM_DATE
			,s.DIS_DATE
			,d.Code
			,d.DateofService
			,ROW_NUMBER() over(Partition By s.EMPI,s.DIS_DATE order by DateofService desc) as rn
		from #62_directtransfers s
		Join #62_dischargeconditions d on s.EMPI=d.EMPI and d.DateofService between s.ADM_DATE and s.DIS_DATE
	)t1
)t2
Where rnk=1



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



-- Create the output as required
Drop table if exists #62_dataset
Create Table #62_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	CE bit,
	Epop bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT,
	Code varchar(20),
	DateofService Date

)
Insert into #62_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,Case
		When n.EMPI is not null then 1
		else 0
	end as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,0 as CE
	,0 as Epop
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
	,n.Code
	,n.DIS_DATE as DateOfService
from #62_denominatorset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=@attributiondate
Left outer join #62_numeratorset n on d.EMPI=n.EMPI
where a.AssignedStatus='Assigned' 
--and	  a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Internal Medicine')




update ds Set Rexcl=1 from #62_dataset ds join #62_hospicemembers n on ds.EMPI=n.EMPI


-- Insert data into Measure Detailed Line
Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and Root_Companies_id=@rootId;
		
Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,Code,DateofService)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,code,DateofService
From #62_dataset d





-- Insert data into Provider Scorecard

Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and Root_Companies_id=@rootId;

/*
Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	@measure_id
	,@measurename
	,@domain
	,@subdomain
	,@measuretype
	,(select count(distinct concat(EMPI,DateOfService)) from #62_dataset where Enrollment_Status='Active' and Num=1 and Excl=0 and Rexcl=0) as NUM_Count
	,(select count(distinct concat(EMPI,DateOfService)) from #62_dataset where Enrollment_Status='Active') as DEN_Count
	,(select count(distinct concat(EMPI,DateOfService)) from #62_dataset where Enrollment_Status='Active' and Excl=1) as Excl_Count
	,(select count(distinct concat(EMPI,DateOfService)) from #62_dataset where Enrollment_Status='Active' and Rexcl=1) as Rexcl_Count
	,(select count(distinct concat(EMPI,DateOfService)) from #62_dataset where Enrollment_Status='Active' and Num=1 and Excl=0 and Rexcl=0) as Gaps
	,Case 
		when (select count(distinct concat(EMPI,DateOfService)) from #62_dataset where Enrollment_Status='Active') > 0 then Round((cast((select count(case when num=1 then 1 end) from #62_dataset where Enrollment_Status='Active') as float)/(select count(distinct EMPI) from #62_dataset where Enrollment_Status='Active'))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when Round(cast((select count(case when num=1 then 1 end) from #62_dataset where Enrollment_Status='Active') as float)/(select count(distinct EMPI) from #62_dataset where Enrollment_Status='Active'),2) > @target then @target-ROUND(cast((select count(case when num=1 then 1 end) from #62_dataset where Enrollment_Status='Active') as float)/(select count(distinct EMPI) from #62_dataset where Enrollment_Status='Active'),2)
		Else 0
	end as To_Target
	,@reportId
	,@reporttype
	,@quarter
	,@startDate
	,@enddate
	,@rootId


*/

	Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)

	Select
		Measure_id,
		Measure_Name,
		Measure_Title,
		MEASURE_SUBTITLE,
		Measure_Type,
		NUM_COUNT,
		DEN_COUNT,
		Excl_Count,
		Rexcl_Count,
		Gaps,
		Result,
		Target,
		Case
			when Result > Target then Floor((DEN_COUNT*Target)/100)-NUM_COUNT
			when Result <= Target then 0
		end as To_Target,
		Report_Id,
		ReportType,
		Report_Quarter,
		Period_Start_Date,
		Period_End_Date,
		Root_Companies_Id
	From
	(
		
			Select
				Measure_id,
				Measure_Name,
				Measure_Title,
				MEASURE_SUBTITLE,
				Measure_Type,
				SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT,
				SUM(Cast(Den_excl as INT)) as DEN_COUNT,
				SUM(Cast(Excl_Count as INT)) as Excl_Count,
				Sum(Cast(Rexcl_count as INT)) as Rexcl_Count,
				SUM(Cast(NUM_COUNT as INT))  as Gaps,
				Case
					when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
					else 0
				end as Result,
				Target,
				Report_Id,
				ReportType,
				Report_Quarter,
				Period_Start_Date,
				Period_End_Date,
				Root_Companies_Id
			From
			(
				Select distinct
					m.EMPI
					,a.NPI as Provider_id
					,a.NPI as PCP_NPI
					,a.Prov_Name as PCP_Name
					,a.Specialty
					,a.Practice as Practice_Name
					,m.Measure_id
					,m.Measure_Name
					,l.measure_Title
					,l.Measure_SubTitle
					,'Calculated' as Measure_Type
					,Case
						when NUM=1 and excl=0 and rexcl=0 Then 1
						else 0
					end as NUM_COUNT
					,DEN as DEN_COUNT
					,Case
						When DEN=1 and Excl=0 and Rexcl=0 Then 1
						else 0
					End as Den_excl
					,Excl as Excl_Count
					,Rexcl as Rexcl_count
					,Report_Id
					,l.ReportType
					,Report_Quarter
					,Period_Start_Date
					,Period_End_Date
					,m.Root_Companies_Id
					,l.Target
				From RPT.MEASURE_DETAILED_LINE m
				Join RPT.ConsolidatedAttribution_Snapshot a on
					m.EMPI=a.EMPI and
					a.Attribution_Type='Ambulatory_PCP'
				Join RFT.UHN_measuresList l on
					m.Measure_Id=l.measure_id
					/*
				Join RFT.UHN_MeasureSpecialtiesMapping s on
					case when a.Specialty not in(select Specialty from RFT.UHN_MeasureSpecialtiesMapping where Measure_id=@measure_id) Then 'Network' else a.Specialty =s.Specialty and
					m.MEASURE_ID=s.Measure_id
					*/
				where 
					Enrollment_Status='Active' and
					a.NPI!='' and
					m.MEASURE_ID=@measure_id and
					REPORT_ID=@reportId

			)tbl
			Group By
				Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,target
		
	)tbl2		
			
			

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END


GO
