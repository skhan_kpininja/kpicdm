SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


Create proc [KPI_UHN_GenericDispensing_46]
AS


-- Declare Variables
declare @meas_year varchar(4)=Year(Dateadd(month,-2,GetDate()))
declare @rootId INT=159


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id INT;
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @numerator float;
Declare @denominator float;

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;


-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');




Set @reporttype='Network'
Set @measurename='Generic Dispensing Rate: Cigna Data Only'
Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=90
Set @domain='Utilization Management'
Set @subdomain='Medication Management'
Set @measuretype='UHN'
Set @measure_id='46'


-- Create Denominator Set
Drop table if exists #46_denominatorset
Create Table #46_denominatorset
(
	EMPI varchar(100),
	FILL_DATE Date,
	MEDICATION_CODE varchar(20),
	MED_TYPE INT

)
Insert into #46_denominatorset
select distinct
	EMPI
	,FILL_DATE
	,MEDICATION_CODE
	,MED_TYPE
From MEDICATION
where MED_DATA_SRC='CIGNA' and
	  ROOT_COMPANIES_ID=@rootId and
	  FILL_DATE between @ce_startdt and @ce_enddt

/*
-- Calculate Numerator
select 
		@numerator=count(case when MED_TYPE=1 then 1 end)
from MEDICATION 
where MED_DATA_SRC='CIGNA' and
	  ROOT_COMPANIES_ID=@rootId and
	  FILL_DATE between @ce_startdt and @ce_enddt and
	  MED_TYPE=1

*/







-- Get ReportId from Report_Details

	Select @reportId=ReportId from KPI_ENGINE.RPT.Report_Details where ReportStartDate=@startDate and ReportEndDate=@enddate;

IF(@reportId>0)
	Begin

		Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId;
		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId;
		
	End
	Else
	Begin

		Insert into KPI_ENGINE.RPT.Report_Details(ReportStartDate,ReportEndDate,ReportQuarter,Root_Companies_id) values(@startDate,@enddate,@quarter,@rootId);

		Select @reportId=ReportId from KPI_ENGINE.RPT.Report_Details where ReportStartDate=@startDate and ReportEndDate=@enddate;

		
	End


-- Create the output as required
Drop table if exists #46_dataset
Create Table #46_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	CE bit,
	Epop bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT,
	Code varchar(20),
	DateofService Date

)
Insert into #46_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,Case
		when MED_TYPE=1 Then 1
		else 0
	End as NUM
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,0 as CE
	,0 as Epop
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
	,Case
		when d.MED_TYPE=1 Then 'G'
		when d.MED_TYPE=2 Then 'B'
		when d.MED_TYPE=3 Then 'O'
		else '4'
	end as Code
	,d.Fill_Date as DateofService
from #46_denominatorset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=DATEADD(month, DATEDIFF(month, 0, @enddate), 0)
where a.AssignedStatus='Assigned'



-- Insert data into Measure Detailed Line
Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,Code,DateofService)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,code,DateofService
From #46_dataset d



select @denominator=count(*) from #46_dataset;
select @numerator=count(case when NUM=1 then 1 end) from #46_dataset;

-- Insert data into Provider Scorecard
Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	
	@measure_id
	,@measurename
	,@domain
	,@subdomain
	,@measuretype
	,@numerator
	,@denominator
	,@denominator - @numerator as Gaps
	,Case
		when @denominator>0 Then Round(@numerator/@denominator*100,2)
		else 0
	end as Result
	,@target as Target
	,Case	
		when (@denominator*(cast(@target*0.01 as float))) - @numerator>0 Then ROUND((@denominator*(cast(@target*0.01 as float))) - @numerator,0)
		Else 0
	end as To_Target
	,@reportId
	,@reporttype
	,@quarter
	,@startDate
	,@enddate
	,@rootId



GO
