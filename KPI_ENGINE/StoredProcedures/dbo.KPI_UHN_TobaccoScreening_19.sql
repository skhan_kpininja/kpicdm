SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE proc [dbo].[KPI_UHN_TobaccoScreening_19]
AS


declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159


Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);


-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
Set @measurename='Tobacco Use: Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=60
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult & Pediatric Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id=19


-- Identify Deaceased Members
Drop table if exists #19_deceasedmembers
Create table #19_deceasedmembers
(
	EMPI varchar(100)
)
Insert into #19_deceasedmembers
select * from deceasedmembers(@rootId,@ce_startdt,@ce_enddt)


-- Generate Visit List based on eCQM definition
Drop table if exists #19_visitlist;
Create table #19_visitlist
(
	EMPI varchar(100)
)
Insert into #19_visitlist
Select distinct
	EMPI
From
(
	Select 
		EMPI
	From
	(
		Select 
			p.EMPI
			,p.PROC_START_DATE
			,count(*) as ct
		From Procedures p
		left outer Join ClaimLine c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,'')=ISNULL(c.SV_LINE,'') and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and 
									   p.EMPI=c.EMPI
		Where
			p.Root_Companies_Id=@rootId and
			ISNULL(c.POS,'')!='81' and
			p.PROC_START_DATE between @ce_startdt and @ce_enddt and
			p.PROC_CODE in('90791','90792','90832','90834','90837','90845','92002','92004','92012','92014','92521','92522','92523','92524','92540','92557','92625','96156','96158','97161','97162','97163','97165','97166','97167','97168','99202','99203','99204','99205','99212','99213','99214','99215','99341','99342','99343','99344','99345','99347','99348','99349','99350')
		group by P.EMPI,p.PROC_START_DATE having count(*)>1
	)t1

	Union all

	Select 
			p.EMPI
		From Procedures p
		left outer Join ClaimLine c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,'')=ISNULL(c.SV_LINE,'') and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and 
									   p.EMPI=c.EMPI
		Where
			p.Root_Companies_Id=@rootId and
			ISNULL(c.POS,'')!='81' and
			p.PROC_START_DATE between @ce_startdt and @ce_enddt and
			p.PROC_CODE in('99385','99386','99387','99395','99396','99397','99401','99402','99403','99404','99411','99412','99429','G0438','G0439')
)t2



-- Identify members who are 18 years and older as on the Reporting Month
drop table if exists #19_denominatorset;
Create table #19_denominatorset
(
	EMPI varchar(100),
	MEMBER_ID varchar(100),
	Gender varchar(10),
	Age INT
)
Insert into #19_denominatorset
Select * from(
select distinct
	o.EMPI_ID
	,Org_Patient_Extension_ID as MEMBER_ID
	,Gender
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,eomonth(@ce_enddt)), Date_of_Birth) > eomonth(@ce_enddt) THEN datediff(year, Date_of_Birth, eomonth(@ce_enddt)) - 1
		ELSE datediff(year, Date_of_Birth, eomonth(@ce_enddt))
	END as Age
from open_empi_master o
join #19_visitlist v on o.EMPI_ID=v.EMPI
left outer join #19_deceasedmembers d on EMPI_ID=d.EMPI
where 
	o.Root_Companies_ID=@rootId and
	d.EMPI is null
)t1 
where age >=18



--Identify exclusions
drop table if exists #19_exclusions
CREATE table #19_exclusions
(
	EMPI varchar(50)
		
)
Insert into #19_exclusions
select distinct
	EMPI
From
(
	select EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)

	Union all

	Select 
		p.EMPI
	From KPI_ENGINE.dbo.PROCEDURES p
	left outer Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
								   ISNULL(p.SV_LINE,'')=ISNULL(c.SV_LINE,'') and
								   p.PROC_DATA_SRC=c.CL_DATA_SRC and
								   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
								   p.EMPI=c.EMPI
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(c.POS,'')!='81' and
		PROC_START_DATE between @ce_startdt and @ce_enddt and 
		PROC_CODE in('G9904')
			
		
)t1



-- Identifying Members who were screened for Tobacco use
Drop table if exists #19_numerator;
Create table #19_numerator
(
	EMPI varchar(100)
)
Insert into #19_numerator
Select 
	distinct p.EMPI
From KPI_ENGINE.dbo.PROCEDURES p
left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
							   ISNULL(p.SV_LINE,'')=ISNULL(c.SV_LINE,'') and
							   p.PROC_DATA_SRC=c.CL_DATA_SRC and 
							   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
							   p.EMPI=c.EMPI
Where
	p.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	PROC_START_DATE between @ce_startdt and @ce_enddt and 
	PROC_CODE in('1036F','G9903','G9902','G9906','4004F') and 
	(
		(
			Mod_1 not in(select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where value_set_name in('CPT CAT II Modifier'))
		)
		or
		MOD_1 is null
	)


-- Get Numerator Details
Drop table if exists #19_num_detail
Create table #19_num_detail
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(50)
)
Insert into #19_num_detail
Select
	EMPI
	,ServiceDate
	,Code
From
(
	Select
		*
		,DENSE_RANK() over(partition by EMPI order by rn Desc) as rnk
	From
	(
		Select distinct 
			EMPI
			,Code
			,ServiceDate
			,ROW_NUMBER() over (partition by EMPI order by ServiceDate Desc) as rn
		from(

			Select 
					p.EMPI
					,p.PROC_CODE as Code
					,p.PROC_START_DATE as ServiceDate
			from KPI_ENGINE.dbo.PROCEDURES p
			left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
										   ISNULL(p.SV_LINE,'')=ISNULL(c.SV_LINE,'') and
										   p.PROC_DATA_SRC=c.CL_DATA_SRC and 
										   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
										   p.EMPI=c.EMPI
			Where
				p.ROOT_COMPANIES_ID=@rootId and
				ISNULL(c.POS,'')!='81' and
				PROC_START_DATE between @ce_startdt and @ce_enddt and 
				PROC_CODE in('1036F','G9903','G9902','G9906','4004F') and 
				(
					(
						Mod_1 not in(select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where value_set_name in('CPT CAT II Modifier'))
					)
					or
					MOD_1 is null
				)

		)t1
	)t2
)t3
Where rnk=1





-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=159,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output




-- Create the output as required
Drop table if exists #19_dataset
Create Table #19_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT

)
Insert into #19_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,0 as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
from #19_denominatorset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=DATEADD(month, DATEDIFF(month, 0, @enddate), 0)
where a.AssignedStatus='Assigned'


update ds Set num=1 from #19_dataset ds join #19_numerator n on ds.EMPI=n.EMPI
update ds Set Rexcl=1 from #19_dataset ds join #19_exclusions n on ds.EMPI=n.EMPI


Delete from RPT.MEASURE_DETAILED_LINE where measure_id=@measure_id and Report_id=@reportId;

-- Insert data into Measure Detailed Line
Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,Code,DateofService)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,Code,ServiceDate
From #19_dataset d
Left outer join #19_num_detail nd on d.EMPI=nd.EMPI
--where Specialty in('General Practice','Family Medicine','Hospitalists','Internal Medicine','Pediatrics')


Delete from RPT.PROVIDER_SCORECARD where measure_id=@measure_id and Report_id=@reportId;

-- Insert data into Provider Scorecard
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target as Target
	,Case	
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target


GO
