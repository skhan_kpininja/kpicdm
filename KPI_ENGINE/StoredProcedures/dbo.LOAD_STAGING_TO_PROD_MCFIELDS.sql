SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [dbo].[LOAD_STAGING_TO_PROD_MCFIELDS]
AS
BEGIN
	
	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 MC_DATA_SRC FROM [KPI_ENGINE].[dbo].[MCFIELDS] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='MCFIELDS') DEDUPKEY
ON MC_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR MCFIELDS: Select Distinct TOP 1 MC_DATA_SRC FROM [KPI_ENGINE].[dbo].[MCFIELDS] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''MCFIELDS'') DEDUPKEY
ON MC_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);


DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(500)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(M.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(M.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(M.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PM.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PM.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PM.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='MCFIELDS'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  


SET @SQL='
update [KPI_ENGINE].[dbo].[MCFIELDS]
SET
 
[MEMBER_ID]=LTRIM(RTRIM(M.[MEMBER_ID]))
,[Contract]=LTRIM(RTRIM(M.[Contract]))
,[PBP_Number]=LTRIM(RTRIM(M.[PBP_Number]))
,[MEM_MEDICARE]=LTRIM(RTRIM(M.[MEM_MEDICARE]))
,[Low_Income_Period_Start_Date]=LTRIM(RTRIM(M.[Low_Income_Period_Start_Date]))
,[Low_Income_Period_End_Date]=LTRIM(RTRIM(M.[Low_Income_Period_End_Date]))
,[Premium_LIS_Amount]=LTRIM(RTRIM(M.[Premium_LIS_Amount]))
,[Hospice]=LTRIM(RTRIM(M.[Hospice]))
,[OREC]=LTRIM(RTRIM(M.[OREC]))
,[LTI]=LTRIM(RTRIM(M.[LTI]))
,[PayDate]=LTRIM(RTRIM(M.[PayDate]))
,[RunDate]=LTRIM(RTRIM(M.[RunDate]))
,[MC_DATA_SRC]=LTRIM(RTRIM(M.[MC_DATA_SRC]))
,[ROOT_COMPANIES_ID]=LTRIM(RTRIM(M.[ROOT_COMPANIES_ID]))
,[FileName]=LTRIM(RTRIM(M.[FileName]))
,[FileDate]=LTRIM(RTRIM(M.[FileDate]))
,[LoadDateTime]=LTRIM(RTRIM(M.[LoadDateTime]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_MCFIELDS] M
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on M.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND M.[MC_DATA_SRC]=EMPI.[Data_Source] AND M.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.MCFIELDS PM
ON '+@KEYS+
' WHERE PM.MEMBER_ID IS not NULL AND EMPI.EMPI_ID IS NOT NULL AND M.MC_DATA_SRC='''+@PAYER+''' AND M.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @updatecount INT;
EXECUTE (@SQL);
SET @updatecount=@@ROWCOUNT;




SET @SQL='
INSERT INTO [KPI_ENGINE].[dbo].[MCFIELDS]
(
	   
      [MEMBER_ID]
	  ,EMPI
      ,[Contract]
      ,[PBP_Number]
      ,[MEM_MEDICARE]
      ,[Low_Income_Period_Start_Date]
      ,[Low_Income_Period_End_Date]
      ,[Premium_LIS_Amount]
      ,[Hospice]
      ,[OREC]
      ,[LTI]
      ,[PayDate]
      ,[RunDate]
      ,[MC_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[FileName]
      ,[FileDate]
      ,[LoadDateTime]
)
SELECT 
      LTRIM(RTRIM(M.[MEMBER_ID]))
	  ,LTRIM(RTRIM(EMPI.[EMPI_ID]))
      ,LTRIM(RTRIM(M.[Contract]))
      ,LTRIM(RTRIM(M.[PBP_Number]))
      ,LTRIM(RTRIM(M.[MEM_MEDICARE]))
      ,LTRIM(RTRIM(M.[Low_Income_Period_Start_Date]))
      ,LTRIM(RTRIM(M.[Low_Income_Period_End_Date]))
      ,LTRIM(RTRIM(M.[Premium_LIS_Amount]))
      ,LTRIM(RTRIM(M.[Hospice]))
      ,LTRIM(RTRIM(M.[OREC]))
      ,LTRIM(RTRIM(M.[LTI]))
      ,LTRIM(RTRIM(M.[PayDate]))
      ,LTRIM(RTRIM(M.[RunDate]))
      ,LTRIM(RTRIM(M.[MC_DATA_SRC]))
      ,LTRIM(RTRIM(M.[ROOT_COMPANIES_ID]))
      ,LTRIM(RTRIM(M.[FileName]))
      ,LTRIM(RTRIM(M.[FileDate]))
      ,LTRIM(RTRIM(M.[LoadDateTime]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_MCFIELDS] M
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on M.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND M.[MC_DATA_SRC]=EMPI.[Data_Source] AND M.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.MCFIELDS PM
ON '+@KEYS+
' WHERE PM.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NOT NULL AND M.MC_DATA_SRC='''+@PAYER+''' AND M.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @insertcount INT;
EXECUTE (@SQL);
SET @insertcount=@@ROWCOUNT;



SET @SQL='
INSERT INTO [KPI_ENGINE_STAGING].[dbo].[STAGING_MCFIELDS_ERROR]
(
	   [ST_MCF_ID]
      ,[MEMBER_ID]
      ,[Contract]
      ,[PBP_Number]
      ,[MEM_MEDICARE]
      ,[Low_Income_Period_Start_Date]
      ,[Low_Income_Period_End_Date]
      ,[Premium_LIS_Amount]
      ,[Hospice]
      ,[OREC]
      ,[LTI]
      ,[PayDate]
      ,[RunDate]
      ,[MC_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[FileName]
      ,[FileDate]
      ,[LoadDateTime]
)
SELECT LTRIM(RTRIM(M.[ST_MCF_ID]))
      ,LTRIM(RTRIM(M.[MEMBER_ID]))
      ,LTRIM(RTRIM(M.[Contract]))
      ,LTRIM(RTRIM(M.[PBP_Number]))
      ,LTRIM(RTRIM(M.[MEM_MEDICARE]))
      ,LTRIM(RTRIM(M.[Low_Income_Period_Start_Date]))
      ,LTRIM(RTRIM(M.[Low_Income_Period_End_Date]))
      ,LTRIM(RTRIM(M.[Premium_LIS_Amount]))
      ,LTRIM(RTRIM(M.[Hospice]))
      ,LTRIM(RTRIM(M.[OREC]))
      ,LTRIM(RTRIM(M.[LTI]))
      ,LTRIM(RTRIM(M.[PayDate]))
      ,LTRIM(RTRIM(M.[RunDate]))
      ,LTRIM(RTRIM(M.[MC_DATA_SRC]))
      ,LTRIM(RTRIM(M.[ROOT_COMPANIES_ID]))
      ,LTRIM(RTRIM(M.[FileName]))
      ,LTRIM(RTRIM(M.[FileDate]))
      ,LTRIM(RTRIM(M.[LoadDateTime]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_MCFIELDS] M
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on M.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND M.[MC_DATA_SRC]=EMPI.[Data_Source] AND M.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN [KPI_ENGINE_STAGING].[dbo].[STAGING_MCFIELDS_ERROR] PM
ON '+@KEYS+
' WHERE PM.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NULL AND M.MC_DATA_SRC='''+@PAYER+''' AND M.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @errorcount INT;
EXECUTE (@SQL);
Set @errorcount=@@ROWCOUNT


-- Shanawaz 10-21-2021 -- Adding this  for data flow trace
Insert into KPI_ENGINE_RECON.dbo.PROD_INGESTION_STATS(Dimension,Source,UpdateCount,InsertCount,ErrorCount) values(@PROD_TABLE,@PAYER,@updatecount,@insertcount,@errorcount);



FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C

			SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;


END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
