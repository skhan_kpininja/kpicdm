SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[LOAD_STAGING_TO_PROD_PROVIDER]

AS
BEGIN

	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 


BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 PROV_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].STAGING_PROVIDER M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='PROVIDER') DEDUPKEY
ON PROV_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR PROVIDERS: Select Distinct TOP 1 PROV_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].STAGING_PROVIDER M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''PROVIDER'') DEDUPKEY
ON MC_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);

DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(500)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(P.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(P.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(P.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PP.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PP.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PP.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='PROVIDER'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  

/*
SET @SQL='
UPDATE KPI_ENGINE.DBO.PROVIDER
SET
		 [PROV_START_DATE]=LTRIM(RTRIM(P.[PROV_START_DATE]))
		,[PROV_END_DATE]=LTRIM(RTRIM(P.[PROV_END_DATE]))
		,[PROV_ID]=LTRIM(RTRIM(P.[PROV_ID]))
		,[PROV_TYPE]=LTRIM(RTRIM(P.[PROV_TYPE]))
		,[PROV_NPI]=LTRIM(RTRIM(P.[PROV_NPI]))
		,[PROV_TIN]=LTRIM(RTRIM(P.[PROV_TIN]))
		,[PROV_TAXONOMY]=LTRIM(RTRIM(P.[PROV_TAXONOMY]))
		,[PROV_LIC]=LTRIM(RTRIM(P.[PROV_LIC]))
		,[PROV_MEDICAID]=LTRIM(RTRIM(P.[PROV_MEDICAID]))
		,[PROV_DEA]=LTRIM(RTRIM(P.[PROV_DEA]))
		,[PROV_NET_FLAG]=LTRIM(RTRIM(P.[PROV_NET_FLAG]))
		,[PROV_PAR_FLAG]=LTRIM(RTRIM(P.[PROV_PAR_FLAG]))
		,[PROV_LNAME]=LTRIM(RTRIM(P.[PROV_LNAME]))
		,[PROV_FNAME]=LTRIM(RTRIM(P.[PROV_FNAME]))
		,[PROV_MNAME]=LTRIM(RTRIM(P.[PROV_MNAME]))
		,[PROV_GENDER]=LTRIM(RTRIM(P.[PROV_GENDER]))
		,[PROV_DOB]=LTRIM(RTRIM(P.[PROV_DOB]))
		,[PROV_EMAIL]=LTRIM(RTRIM(P.[PROV_EMAIL]))
		,[PROV_CLINIC_ID]=LTRIM(RTRIM(P.[PROV_CLINIC_ID]))
		,[PROV_CLINIC_NAME]=LTRIM(RTRIM(P.[PROV_CLINIC_NAME]))
		,[PROV_Clinic_ADDR]=LTRIM(RTRIM(P.[PROV_Clinic_ADDR]))
		,[PROV_Clinic_ADDR2]=LTRIM(RTRIM(P.[PROV_Clinic_ADDR2]))
		,[PROV_Clinic_CITY]=LTRIM(RTRIM(P.[PROV_Clinic_CITY]))
		,[PROV_Clinic_STATE]=LTRIM(RTRIM(P.[PROV_Clinic_STATE]))
		,[PROV_Clinic_ZIP]=LTRIM(RTRIM(P.[PROV_Clinic_ZIP]))
		,[PROV_MAILING_ADDR]=LTRIM(RTRIM(P.[PROV_MAILING_ADDR]))
		,[PROV_MAILING_ADDR2]=LTRIM(RTRIM(P.[PROV_MAILING_ADDR2]))
		,[PROV_MAILING_CITY]=LTRIM(RTRIM(P.[PROV_MAILING_CITY]))
		,[PROV_MAILING_STATE]=LTRIM(RTRIM(P.[PROV_MAILING_STATE]))
		,[PROV_MAILING_ZIP]=LTRIM(RTRIM(P.[PROV_MAILING_ZIP]))
		,[PROV_PHONE]=LTRIM(RTRIM(P.[PROV_PHONE]))
		,[PROV_GRP_ID]=LTRIM(RTRIM(P.[PROV_GRP_ID]))
		,[PROV_GRP_NAME]=LTRIM(RTRIM(P.[PROV_GRP_NAME]))
		,[PROV_GRP_MAILING_ADDR]=LTRIM(RTRIM(P.[PROV_GRP_MAILING_ADDR]))
		,[PROV_GRP_MAILING_ADDR2]=LTRIM(RTRIM(P.[PROV_GRP_MAILING_ADDR2]))
		,[PROV_GRP_MAILING_CITY]=LTRIM(RTRIM(P.[PROV_GRP_MAILING_CITY]))
		,[PROV_GRP_MAILING_STATE]=LTRIM(RTRIM(P.[PROV_GRP_MAILING_STATE]))
		,[PROV_GRP_MAILING_ZIP]=LTRIM(RTRIM(P.[PROV_GRP_MAILING_ZIP]))
		,[PROV_GRP_PHONE]=LTRIM(RTRIM(P.[PROV_GRP_PHONE]))
		,[PROV_GRP_EMAIL]=LTRIM(RTRIM(P.[PROV_GRP_EMAIL]))
		,[PROV_SPEC_CODE]=LTRIM(RTRIM(P.[PROV_SPEC_CODE]))
		,[PROV_SPEC_DESC]=LTRIM(RTRIM(P.[PROV_SPEC_DESC]))
		,[PROV_SPEC_CODE2]=LTRIM(RTRIM(P.[PROV_SPEC_CODE2]))
		,[PROV_SPEC_DESC2]=LTRIM(RTRIM(P.[PROV_SPEC_DESC2]))
		,[PROV_SPEC_CODE3]=LTRIM(RTRIM(P.[PROV_SPEC_CODE3]))
		,[PROV_SPEC_DESC3]=LTRIM(RTRIM(P.[PROV_SPEC_DESC3]))
		,[PROV_PCP_FLAG]=LTRIM(RTRIM(P.[PROV_PCP_FLAG]))
		,[PROV_OB_FLAG]=LTRIM(RTRIM(P.[PROV_OB_FLAG]))
		,[PROV_MH_FLAG]=LTRIM(RTRIM(P.[PROV_MH_FLAG]))
		,[PROV_EYE_FLAG]=LTRIM(RTRIM(P.[PROV_EYE_FLAG]))
		,[PROV_DEN_FLAG]=LTRIM(RTRIM(P.[PROV_DEN_FLAG]))
		,[PROV_NEP_FLAG]=LTRIM(RTRIM(P.[PROV_NEP_FLAG]))
		,[PROV_CD_FLAG]=LTRIM(RTRIM(P.[PROV_CD_FLAG]))
		,[PROV_NP_FLAG]=LTRIM(RTRIM(P.[PROV_NP_FLAG]))
		,[PROV_PA_FLAG]=LTRIM(RTRIM(P.[PROV_PA_FLAG]))
		,[PROV_RX_FLAG]=LTRIM(RTRIM(P.[PROV_RX_FLAG]))
		,[PROV_GROUP_IPA_NAME]=LTRIM(RTRIM(P.[PROV_GROUP_IPA_NAME]))
		,[PROV_GROUP_ACO_NAME]=LTRIM(RTRIM(P.[PROV_GROUP_ACO_NAME]))
		,[PROV_DATA_SRC]=LTRIM(RTRIM(P.[PROV_DATA_SRC]))
		,[PROV_SRC_COMMON_ID]=LTRIM(RTRIM(P.[PROV_SRC_COMMON_ID]))
		,[PROV_CW_PRIORITY]=LTRIM(RTRIM(P.[PROV_CW_PRIORITY]))
		,[PROV_QUALPORT]=LTRIM(RTRIM(P.[PROV_QUALPORT]))
		,[PROV_ATTRIB]=LTRIM(RTRIM(P.[PROV_ATTRIB]))
		,[PROV_DOMESTIC]=LTRIM(RTRIM(P.[PROV_DOMESTIC]))
		,[PROV_HOSP_MEDICARE_ID]=LTRIM(RTRIM(P.[PROV_HOSP_MEDICARE_ID]))
		,[PROV_PHARM_FLAG]=LTRIM(RTRIM(P.[PROV_PHARM_FLAG]))
		,[PROV_HOSP_FLAG]=LTRIM(RTRIM(P.[PROV_HOSP_FLAG]))
		,[PROV_SNF_FLAG]=LTRIM(RTRIM(P.[PROV_SNF_FLAG]))
		,[PROV_SURG_FLAG]=LTRIM(RTRIM(P.[PROV_SURG_FLAG]))
		,[PROV_RN_FLAG]=LTRIM(RTRIM(P.[PROV_RN_FLAG]))
		,[PROV_DME_FLAG]=LTRIM(RTRIM(P.[PROV_DME_FLAG]))
		,[PROV_AMBULANCE_FLAG]=LTRIM(RTRIM(P.[PROV_AMBULANCE_FLAG]))
		,[_PROV_UDF_01_]=LTRIM(RTRIM(P.[_PROV_UDF_01_]))
		,[_PROV_UDF_02_]=LTRIM(RTRIM(P.[_PROV_UDF_02_]))
		,[_PROV_UDF_03_]=LTRIM(RTRIM(P.[_PROV_UDF_03_]))
		,[_PROV_UDF_04_]=LTRIM(RTRIM(P.[_PROV_UDF_04_]))
		,[_PROV_UDF_05_]=LTRIM(RTRIM(P.[_PROV_UDF_05_]))
		,[_PROV_UDF_06_]=LTRIM(RTRIM(P.[_PROV_UDF_06_]))
		,[_PROV_UDF_07_]=LTRIM(RTRIM(P.[_PROV_UDF_07_]))
		,[_PROV_UDF_08_]=LTRIM(RTRIM(P.[_PROV_UDF_08_]))
		,[_PROV_UDF_09_]=LTRIM(RTRIM(P.[_PROV_UDF_09_]))
		,[_PROV_UDF_10_]=LTRIM(RTRIM(P.[_PROV_UDF_10_]))
		,[_PROV_UDF_11_]=LTRIM(RTRIM(P.[_PROV_UDF_11_]))
		,[_PROV_UDF_12_]=LTRIM(RTRIM(P.[_PROV_UDF_12_]))
		,[_PROV_UDF_13_]=LTRIM(RTRIM(P.[_PROV_UDF_13_]))
		,[_PROV_UDF_14_]=LTRIM(RTRIM(P.[_PROV_UDF_14_]))
		,[_PROV_UDF_15_]=LTRIM(RTRIM(P.[_PROV_UDF_15_]))
		,[_PROV_UDF_16_]=LTRIM(RTRIM(P.[_PROV_UDF_16_]))
		,[_PROV_UDF_17_]=LTRIM(RTRIM(P.[_PROV_UDF_17_]))
		,[_PROV_UDF_18_]=LTRIM(RTRIM(P.[_PROV_UDF_18_]))
		,[_PROV_UDF_19_]=LTRIM(RTRIM(P.[_PROV_UDF_19_]))
		,[_PROV_UDF_20_]=LTRIM(RTRIM(P.[_PROV_UDF_20_]))
		,[ST_PROV_ID]=LTRIM(RTRIM(P.[ST_PROV_ID]))
		,[PROV_NAME]=LTRIM(RTRIM(P.[PROV_NAME]))
		,[ROOT_COMPANIES_ID]=LTRIM(RTRIM(P.[ROOT_COMPANIES_ID]))
		,[LOADDATETIME]=LTRIM(RTRIM(P.[LOADDATETIME]))
		,[FILENAME]=LTRIM(RTRIM(P.[FILENAME]))
		,[FileDate]=LTRIM(RTRIM(P.[FileDate]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROVIDER] P
LEFT JOIN [KPI_ENGINE].[dbo].[PROVIDER] PP
ON '+@KEYS+
' WHERE PP.PROV_ID IS NOT NULL AND P.PROV_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

EXECUTE (@SQL)
---PRINT(@SQL)



*/


SET @SQL='
Delete from [KPI_ENGINE].[dbo].[PROVIDER] where PROV_NPI in(
select pp.prov_npi
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROVIDER] P
LEFT JOIN [KPI_ENGINE].[dbo].[PROVIDER] PP
ON '+@KEYS+
' WHERE PP.PROV_ID IS NOT NULL AND P.PROV_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)+')'


EXECUTE (@SQL)
---PRINT(@SQL)



-- Adding Fully refresh logic for Provider Table
--Truncate Table KPI_ENGINE.DBO.PROVIDER;
SET @SQL='
INSERT INTO KPI_ENGINE.DBO.PROVIDER
(
	   [PROV_START_DATE]
      ,[PROV_END_DATE]
      ,[PROV_ID]
      ,[PROV_TYPE]
      ,[PROV_NPI]
      ,[PROV_TIN]
      ,[PROV_TAXONOMY]
      ,[PROV_LIC]
      ,[PROV_MEDICAID]
      ,[PROV_DEA]
      ,[PROV_NET_FLAG]
      ,[PROV_PAR_FLAG]
      ,[PROV_LNAME]
      ,[PROV_FNAME]
      ,[PROV_MNAME]
      ,[PROV_GENDER]
      ,[PROV_DOB]
      ,[PROV_EMAIL]
      ,[PROV_CLINIC_ID]
      ,[PROV_CLINIC_NAME]
      ,[PROV_Clinic_ADDR]
      ,[PROV_Clinic_ADDR2]
      ,[PROV_Clinic_CITY]
      ,[PROV_Clinic_STATE]
      ,[PROV_Clinic_ZIP]
      ,[PROV_MAILING_ADDR]
      ,[PROV_MAILING_ADDR2]
      ,[PROV_MAILING_CITY]
      ,[PROV_MAILING_STATE]
      ,[PROV_MAILING_ZIP]
      ,[PROV_PHONE]
      ,[PROV_GRP_ID]
      ,[PROV_GRP_NAME]
      ,[PROV_GRP_MAILING_ADDR]
      ,[PROV_GRP_MAILING_ADDR2]
      ,[PROV_GRP_MAILING_CITY]
      ,[PROV_GRP_MAILING_STATE]
      ,[PROV_GRP_MAILING_ZIP]
      ,[PROV_GRP_PHONE]
      ,[PROV_GRP_EMAIL]
      ,[PROV_SPEC_CODE]
      ,[PROV_SPEC_DESC]
      ,[PROV_SPEC_CODE2]
      ,[PROV_SPEC_DESC2]
      ,[PROV_SPEC_CODE3]
      ,[PROV_SPEC_DESC3]
      ,[PROV_PCP_FLAG]
      ,[PROV_OB_FLAG]
      ,[PROV_MH_FLAG]
      ,[PROV_EYE_FLAG]
      ,[PROV_DEN_FLAG]
      ,[PROV_NEP_FLAG]
      ,[PROV_CD_FLAG]
      ,[PROV_NP_FLAG]
      ,[PROV_PA_FLAG]
      ,[PROV_RX_FLAG]
      ,[PROV_GROUP_IPA_NAME]
      ,[PROV_GROUP_ACO_NAME]
      ,[PROV_DATA_SRC]
      ,[PROV_SRC_COMMON_ID]
      ,[PROV_CW_PRIORITY]
      ,[PROV_QUALPORT]
      ,[PROV_ATTRIB]
      ,[PROV_DOMESTIC]
      ,[PROV_HOSP_MEDICARE_ID]
      ,[PROV_PHARM_FLAG]
      ,[PROV_HOSP_FLAG]
      ,[PROV_SNF_FLAG]
      ,[PROV_SURG_FLAG]
      ,[PROV_RN_FLAG]
      ,[PROV_DME_FLAG]
      ,[PROV_AMBULANCE_FLAG]
      ,[_PROV_UDF_01_]
      ,[_PROV_UDF_02_]
      ,[_PROV_UDF_03_]
      ,[_PROV_UDF_04_]
      ,[_PROV_UDF_05_]
      ,[_PROV_UDF_06_]
      ,[_PROV_UDF_07_]
      ,[_PROV_UDF_08_]
      ,[_PROV_UDF_09_]
      ,[_PROV_UDF_10_]
      ,[_PROV_UDF_11_]
      ,[_PROV_UDF_12_]
      ,[_PROV_UDF_13_]
      ,[_PROV_UDF_14_]
      ,[_PROV_UDF_15_]
      ,[_PROV_UDF_16_]
      ,[_PROV_UDF_17_]
      ,[_PROV_UDF_18_]
      ,[_PROV_UDF_19_]
      ,[_PROV_UDF_20_]
      ,[ST_PROV_ID]
      ,[PROV_NAME]
      ,[ROOT_COMPANIES_ID]
      ,[LOADDATETIME]
      ,[FILENAME]
      ,[FileDate]
)
SELECT
		LTRIM(RTRIM(P.[PROV_START_DATE]))
		,LTRIM(RTRIM(P.[PROV_END_DATE]))
		,LTRIM(RTRIM(P.[PROV_ID]))
		,LTRIM(RTRIM(P.[PROV_TYPE]))
		,LTRIM(RTRIM(P.[PROV_NPI]))
		,LTRIM(RTRIM(P.[PROV_TIN]))
		,LTRIM(RTRIM(P.[PROV_TAXONOMY]))
		,LTRIM(RTRIM(P.[PROV_LIC]))
		,LTRIM(RTRIM(P.[PROV_MEDICAID]))
		,LTRIM(RTRIM(P.[PROV_DEA]))
		,LTRIM(RTRIM(P.[PROV_NET_FLAG]))
		,LTRIM(RTRIM(P.[PROV_PAR_FLAG]))
		,LTRIM(RTRIM(P.[PROV_LNAME]))
		,LTRIM(RTRIM(P.[PROV_FNAME]))
		,LTRIM(RTRIM(P.[PROV_MNAME]))
		,LTRIM(RTRIM(P.[PROV_GENDER]))
		,LTRIM(RTRIM(P.[PROV_DOB]))
		,LTRIM(RTRIM(P.[PROV_EMAIL]))
		,LTRIM(RTRIM(P.[PROV_CLINIC_ID]))
		,LTRIM(RTRIM(P.[PROV_CLINIC_NAME]))
		,LTRIM(RTRIM(P.[PROV_Clinic_ADDR]))
		,LTRIM(RTRIM(P.[PROV_Clinic_ADDR2]))
		,LTRIM(RTRIM(P.[PROV_Clinic_CITY]))
		,LTRIM(RTRIM(P.[PROV_Clinic_STATE]))
		,LTRIM(RTRIM(P.[PROV_Clinic_ZIP]))
		,LTRIM(RTRIM(P.[PROV_MAILING_ADDR]))
		,LTRIM(RTRIM(P.[PROV_MAILING_ADDR2]))
		,LTRIM(RTRIM(P.[PROV_MAILING_CITY]))
		,LTRIM(RTRIM(P.[PROV_MAILING_STATE]))
		,LTRIM(RTRIM(P.[PROV_MAILING_ZIP]))
		,LTRIM(RTRIM(P.[PROV_PHONE]))
		,LTRIM(RTRIM(P.[PROV_GRP_ID]))
		,LTRIM(RTRIM(P.[PROV_GRP_NAME]))
		,LTRIM(RTRIM(P.[PROV_GRP_MAILING_ADDR]))
		,LTRIM(RTRIM(P.[PROV_GRP_MAILING_ADDR2]))
		,LTRIM(RTRIM(P.[PROV_GRP_MAILING_CITY]))
		,LTRIM(RTRIM(P.[PROV_GRP_MAILING_STATE]))
		,LTRIM(RTRIM(P.[PROV_GRP_MAILING_ZIP]))
		,LTRIM(RTRIM(P.[PROV_GRP_PHONE]))
		,LTRIM(RTRIM(P.[PROV_GRP_EMAIL]))
		,LTRIM(RTRIM(P.[PROV_SPEC_CODE]))
		,LTRIM(RTRIM(P.[PROV_SPEC_DESC]))
		,LTRIM(RTRIM(P.[PROV_SPEC_CODE2]))
		,LTRIM(RTRIM(P.[PROV_SPEC_DESC2]))
		,LTRIM(RTRIM(P.[PROV_SPEC_CODE3]))
		,LTRIM(RTRIM(P.[PROV_SPEC_DESC3]))
		,LTRIM(RTRIM(P.[PROV_PCP_FLAG]))
		,LTRIM(RTRIM(P.[PROV_OB_FLAG]))
		,LTRIM(RTRIM(P.[PROV_MH_FLAG]))
		,LTRIM(RTRIM(P.[PROV_EYE_FLAG]))
		,LTRIM(RTRIM(P.[PROV_DEN_FLAG]))
		,LTRIM(RTRIM(P.[PROV_NEP_FLAG]))
		,LTRIM(RTRIM(P.[PROV_CD_FLAG]))
		,LTRIM(RTRIM(P.[PROV_NP_FLAG]))
		,LTRIM(RTRIM(P.[PROV_PA_FLAG]))
		,LTRIM(RTRIM(P.[PROV_RX_FLAG]))
		,LTRIM(RTRIM(P.[PROV_GROUP_IPA_NAME]))
		,LTRIM(RTRIM(P.[PROV_GROUP_ACO_NAME]))
		,LTRIM(RTRIM(P.[PROV_DATA_SRC]))
		,LTRIM(RTRIM(P.[PROV_SRC_COMMON_ID]))
		,LTRIM(RTRIM(P.[PROV_CW_PRIORITY]))
		,LTRIM(RTRIM(P.[PROV_QUALPORT]))
		,LTRIM(RTRIM(P.[PROV_ATTRIB]))
		,LTRIM(RTRIM(P.[PROV_DOMESTIC]))
		,LTRIM(RTRIM(P.[PROV_HOSP_MEDICARE_ID]))
		,LTRIM(RTRIM(P.[PROV_PHARM_FLAG]))
		,LTRIM(RTRIM(P.[PROV_HOSP_FLAG]))
		,LTRIM(RTRIM(P.[PROV_SNF_FLAG]))
		,LTRIM(RTRIM(P.[PROV_SURG_FLAG]))
		,LTRIM(RTRIM(P.[PROV_RN_FLAG]))
		,LTRIM(RTRIM(P.[PROV_DME_FLAG]))
		,LTRIM(RTRIM(P.[PROV_AMBULANCE_FLAG]))
		,LTRIM(RTRIM(P.[_PROV_UDF_01_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_02_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_03_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_04_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_05_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_06_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_07_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_08_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_09_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_10_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_11_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_12_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_13_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_14_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_15_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_16_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_17_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_18_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_19_]))
		,LTRIM(RTRIM(P.[_PROV_UDF_20_]))
		,LTRIM(RTRIM(P.[ST_PROV_ID]))
		,LTRIM(RTRIM(P.[PROV_NAME]))
		,LTRIM(RTRIM(P.[ROOT_COMPANIES_ID]))
		,LTRIM(RTRIM(P.[LOADDATETIME]))
		,LTRIM(RTRIM(P.[FILENAME]))
		,LTRIM(RTRIM(P.[FileDate]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROVIDER] P
LEFT JOIN [KPI_ENGINE].[dbo].[PROVIDER] PP
ON '+@KEYS+
' WHERE PP.PROV_ID IS NULL AND P.PROV_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

EXECUTE (@SQL)
---PRINT (@SQL)
--SELECT (@SQL)

/*
SET @SQL='
UPDATE KPI_ENGINE.DBO.PROVIDER
SET
		[_PROV_UDF_05_]=COALESCE(LTRIM(RTRIM(P.[_PROV_UDF_05_])),0)
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROVIDER] P
LEFT JOIN [KPI_ENGINE].[dbo].[PROVIDER] PP
ON '+@KEYS+
' WHERE P.PROV_ID IS NULL AND P.PROV_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

EXECUTE (@SQL)
--PRINT (@SQL)
*/


FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C

-- Map missing physicians to Provider_scorecard_Mail

Insert into RPT.Provider_Scorecard_Mail(Practice,Specialty,Provider,ProviderNPI,midlevel,ROOT_COMPANIES_ID)
select Prov_Grp_name,Prov_Spec_desc2,Prov_name,PROV_NPI,case when _Prov_UDF_01_='midlevel' then 1 else 0 end,159 from Provider where _PROV_UDF_02_='y' and _PROV_UDF_04_='Primary Address'
except
select Practice,Specialty,Provider,ProviderNPI,Midlevel,159 from RPT.Provider_Scorecard_Mail



-- SHanawaz 10-11-2021- Adding logic to handle Provider,Parctice and Specialty name changes 

Update psm Set Provider=Prov_Name,Practice=Prov_GRP_Name,Specialty=Prov_Spec_desc2,midlevel=case when _Prov_UDF_01_  in('midlevel','Mid-level') then 1 else 0 end from RPT.Provider_scorecard_mail psm Join Provider p on PROV_NPI=ProviderNPI where _Prov_udf_02_='Y' and _PROV_UDF_04_='Primary Address'

-- Logic to deduplicate the data from RPT.Provider_scorecard_mail

delete from RPT.Provider_Scorecard_mail where id in(  Select id from(
	select *,row_number() over(Partition By ProviderNPI,Email_id Order by id) as rn from RPT.Provider_Scorecard_Mail)t1 where rn>1)

	-- Shanawaz 10-12-2021 - Adding logic to update flags for NPI 1111111111

	update Provider Set _PROV_UDF_02_='Y',_PROV_UDF_04_='Primary Address' where Prov_NPI='1111111111';


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;


END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
