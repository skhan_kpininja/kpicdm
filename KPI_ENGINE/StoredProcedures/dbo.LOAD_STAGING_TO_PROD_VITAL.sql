SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [dbo].[LOAD_STAGING_TO_PROD_VITAL]
AS
BEGIN

	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 VIT_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='VITAL') DEDUPKEY
ON VIT_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR VITAL: Select Distinct TOP 1 VIT_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''VITAL'') DEDUPKEY
ON VIT_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);



DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(500)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR LOCAL FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(V.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(V.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(V.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PV.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PV.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PV.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='VITAL'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  

SET @SQL='
UPDATE [KPI_ENGINE].[dbo].[VITAL]
SET
	 [ST_VITAL_ID]=LTRIM(RTRIM(V.[ST_VITAL_ID]))
	,[MEMBER_ID]=LTRIM(RTRIM(V.[MEMBER_ID]))
	,[VitalCode]=LTRIM(RTRIM(V.[VitalCode]))
	,[VitalName]=LTRIM(RTRIM(V.[VitalName]))
	,[VitalDateTime]=LTRIM(RTRIM(V.[VitalDateTime]))
	,[Value]=LTRIM(RTRIM(V.[Value]))
	,[Unit]=LTRIM(RTRIM(V.[Unit]))
	,[AttendingProviderNPI]=LTRIM(RTRIM(V.[AttendingProviderNPI]))
	,[AttendingProviderName]=LTRIM(RTRIM(V.[AttendingProviderName]))
	,[AttendingProviderSpecialty]=LTRIM(RTRIM(V.[AttendingProviderSpecialty]))
	,[VIT_DATA_SRC]=LTRIM(RTRIM(V.[VIT_DATA_SRC]))
	,[FileDate]=LTRIM(RTRIM(V.[FileDate]))
	,[FileName]=LTRIM(RTRIM(V.[FileName]))
	,[LoadDateTime]=LTRIM(RTRIM(V.[LoadDateTime]))
	,[ROOT_COMPANIES_ID]=LTRIM(RTRIM(V.[ROOT_COMPANIES_ID]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL] V
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on V.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND V.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID ---and V.[VIT_DATA_SRC]=EMPI.[Data_Source]
LEFT JOIN KPI_ENGINE.dbo.VITAL PV
ON '+@KEYS+
' WHERE PV.MEMBER_ID IS NOT NULL AND EMPI.EMPI_ID IS NOT NULL AND V.VIT_DATA_SRC='''+@PAYER+''' AND V.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @updatecount INT;
EXECUTE (@SQL);
SET @updatecount=@@ROWCOUNT;




SET @SQL='
INSERT INTO [KPI_ENGINE].[dbo].[VITAL]
(
	   [ST_VITAL_ID]
      ,[MEMBER_ID]
	  ,EMPI
      ,[VitalCode]
      ,[VitalName]
      ,[VitalDateTime]
      ,[Value]
      ,[Unit]
      ,[AttendingProviderNPI]
      ,[AttendingProviderName]
      ,[AttendingProviderSpecialty]
      ,[VIT_DATA_SRC]
      ,[FileDate]
      ,[FileName]
      ,[LoadDateTime]
      ,[ROOT_COMPANIES_ID]
)
SELECT LTRIM(RTRIM(V.[ST_VITAL_ID]))
      ,LTRIM(RTRIM(V.[MEMBER_ID]))
	  ,LTRIM(RTRIM(EMPI.[EMPI_ID]))
      ,LTRIM(RTRIM(V.[VitalCode]))
      ,LTRIM(RTRIM(V.[VitalName]))
      ,LTRIM(RTRIM(V.[VitalDateTime]))
      ,LTRIM(RTRIM(V.[Value]))
      ,LTRIM(RTRIM(V.[Unit]))
      ,LTRIM(RTRIM(V.[AttendingProviderNPI]))
      ,LTRIM(RTRIM(V.[AttendingProviderName]))
      ,LTRIM(RTRIM(V.[AttendingProviderSpecialty]))
      ,LTRIM(RTRIM(V.[VIT_DATA_SRC]))
      ,LTRIM(RTRIM(V.[FileDate]))
      ,LTRIM(RTRIM(V.[FileName]))
      ,LTRIM(RTRIM(V.[LoadDateTime]))
      ,LTRIM(RTRIM(V.[ROOT_COMPANIES_ID]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL] V
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on V.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND V.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID ---and V.[VIT_DATA_SRC]=EMPI.[Data_Source]
LEFT JOIN KPI_ENGINE.dbo.VITAL PV
ON '+@KEYS+
' WHERE PV.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NOT NULL AND V.VIT_DATA_SRC='''+@PAYER+''' AND V.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @insertcount INT;
EXECUTE (@SQL);
SET @insertcount=@@ROWCOUNT;



SET @SQL='
INSERT INTO [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL_ERROR] 
(
	   [ST_VITAL_ID]
      ,[MEMBER_ID]
      ,[VitalCode]
      ,[VitalName]
      ,[VitalDateTime]
      ,[Value]
      ,[Unit]
      ,[AttendingProviderNPI]
      ,[AttendingProviderName]
      ,[AttendingProviderSpecialty]
      ,[VIT_DATA_SRC]
      ,[FileDate]
      ,[FileName]
      ,[LoadDateTime]
      ,[ROOT_COMPANIES_ID]
)
SELECT LTRIM(RTRIM(V.[ST_VITAL_ID]))
      ,LTRIM(RTRIM(V.[MEMBER_ID]))
      ,LTRIM(RTRIM(V.[VitalCode]))
      ,LTRIM(RTRIM(V.[VitalName]))
      ,LTRIM(RTRIM(V.[VitalDateTime]))
      ,LTRIM(RTRIM(V.[Value]))
      ,LTRIM(RTRIM(V.[Unit]))
      ,LTRIM(RTRIM(V.[AttendingProviderNPI]))
      ,LTRIM(RTRIM(V.[AttendingProviderName]))
      ,LTRIM(RTRIM(V.[AttendingProviderSpecialty]))
      ,LTRIM(RTRIM(V.[VIT_DATA_SRC]))
      ,LTRIM(RTRIM(V.[FileDate]))
      ,LTRIM(RTRIM(V.[FileName]))
      ,LTRIM(RTRIM(V.[LoadDateTime]))
      ,LTRIM(RTRIM(V.[ROOT_COMPANIES_ID]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL] V
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on V.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND V.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID ----and V.[VIT_DATA_SRC]=EMPI.[Data_Source]
LEFT JOIN [KPI_ENGINE_STAGING].[dbo].[STAGING_VITAL_ERROR] PV
ON '+@KEYS+
' WHERE PV.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NULL AND V.VIT_DATA_SRC='''+@PAYER+''' AND V.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)



Declare @errorcount INT;
EXECUTE (@SQL);
Set @errorcount=@@ROWCOUNT


-- Shanawaz 10-21-2021 -- Adding this  for data flow trace
Insert into KPI_ENGINE_RECON.dbo.PROD_INGESTION_STATS(Dimension,Source,UpdateCount,InsertCount,ErrorCount) values(@PROD_TABLE,@PAYER,@updatecount,@insertcount,@errorcount);



FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C

			SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;


END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
