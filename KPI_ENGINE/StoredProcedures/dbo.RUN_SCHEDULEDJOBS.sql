SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
Create proc RUN_SCHEDULEDJOBS as

Declare @jobcount INT=0;
Declare @i INT=0;
Declare @procname varchar(50);

-- Get Job Count
Select @jobcount=count(*)  from KPI_scheduledJobs where EnabledFlag=1 and DeleteFlag=0 ;

-- Loop through all procedures
While(@i<@jobcount and @jobcount>0)
Begin

	Select @procname=StoredProcedure from KPI_scheduledJobs where EnabledFlag=1 and DeleteFlag=0 order by SeqNo Asc OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY;

	Exec @procname;

End
GO
