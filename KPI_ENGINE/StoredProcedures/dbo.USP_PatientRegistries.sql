SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Procedure [dbo].[USP_PatientRegistries]
As
BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


	-- Get Registries Dynamically
	DECLARE @cols AS NVARCHAR(MAX),
			@query AS NVARCHAR(MAX)

	Select @cols = STUFF((SELECT ',' + QUOTENAME(RegistryName) 
                    From REG.REGISTRY_OUTPUT
					Group By RegistryName
					Order By RegistryName Asc
					FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')

	IF OBJECT_ID('tempdb.dbo.##Temp_AllRegitries', 'U') IS NOT NULL
	DROP TABLE ##Temp_AllRegitries; 

	Set @query = N'Select * Into ##Temp_AllRegitries From (SELECT EMPI,' + @cols + N' From 
             (
                Select Distinct EMPI,RegistryName
                From REG.REGISTRY_OUTPUT
				Where ReportId=(Select Top 1 ReportId from RPT.Report_Details where root_companies_id=root_companies_id order by 1 desc)
			 ) x
             Pivot 
             (
               Count(RegistryName)
               For RegistryName In (' + @cols + N')
             ) p ) tbl'

	Exec sp_executesql @query

	
	-- Creating table dynamically with patient details and registries
	IF OBJECT_ID('dbo.PatientRegistries', 'U') IS NOT NULL 
	DROP TABLE dbo.PatientRegistries; 

	Declare @SQL Varchar(Max)

	SET @SQL = 'CREATE TABLE PatientRegistries (
	[root_companies_id] int NOT NULL,
	[EMPI] [varchar](100) NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](100) NULL,
	[DateofBirth] [date] NULL,
	[Gender] [varchar](10) NULL,
	[HomeAddress] [varchar](500) NULL,
	[AddressState] [varchar](64) NULL,
	[City] [varchar](64) NULL,
	[ZipCode] [varchar](16) NULL,
	[PhoneNumber] [varchar](24) NULL,
	[EmailId] [varchar](100) NULL,
	[Employer] [varchar](250) NULL,
	[Payer] [varchar](64) NULL,
	[PracticeName] [varchar](200) NULL,
	[ProviderName] [varchar](200) NULL,
	[Specialty] [varchar](100) NULL,
	[RecentVisit] [date] NULL,
	[Temp_EMPI] [varchar](100) NULL,'

	SELECT @SQL = @SQL + '['+name +'] [smallint] Null Default 0'  + ','
					FROM tempdb.sys.columns 
					WHERE [object_id] = OBJECT_ID(N'tempdb..##Temp_AllRegitries')
					And name != 'EMPI';

	--Remove trailing comma
	SET @SQL = SUBSTRING(@SQL,0,LEN(@SQL))    
	SET @SQL =  @SQL +',[TotalRegistries] [int] NULL Default 0,[EnrollmentStatus] [varchar](10) NULL)'

	Execute( @SQL)

	IF OBJECT_ID('tempdb.dbo.#Temp_TotalRegistries', 'U') IS NOT NULL
	DROP TABLE #Temp_TotalRegistries;

	 Select Distinct EMPI,Count(Distinct RegistryName) As TotalRegistries Into #Temp_TotalRegistries
     From REG.REGISTRY_OUTPUT
	 Where ReportId=(Select Top 1 ReportId from RPT.Report_Details where root_companies_id=root_companies_id order by 1 desc)
	 Group By EMPI



	 CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
ON [dbo].[##Temp_AllRegitries] ([EMPI])
INCLUDE ([Adult Wellness],[Advanced Illness And Frailty],[Advanced Illness Opportunities],[Advanced Illness Opportunities - Single Diagnosis],[AMI],[CAD],[CKD],[COPD],[Depression],[Diabetes],[ESRD],[Frailty Opportunities],[Heart Failure],[Hypertension],[Oncology],[Pediatric Wellness],[Pregnancy],[Rheumatoid Arthritis],[Rheumatoid Arthritis Opportunities])


CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
ON [dbo].[#Temp_TotalRegistries] ([EMPI])
INCLUDE ([TotalRegistries])



	-- Inserting the patient details initially to the table
	Insert Into dbo.PatientRegistries
	Select Distinct 
		159
		,EMPI_ID
		, First_Name
		, Last_Name
		, Date_of_Birth, Gender
		,IsNull(Home_Address_Line_1,'') + IsNull(Home_Address_Line_2,'') As HomeAddress
		,[State]
		,City
		,Case 
			When Substring(Zip_Code, Patindex('%[^0]%',Zip_Code), 10) = '' Then Null
			Else Substring(Zip_Code, Patindex('%[^0]%',Zip_Code), 10) 
		End As Zipcode
		,IsNull(Phone_Number_Main,Phone_Number_Alternative) As PhoneNumber
		,Email_Address
		,Employer
		,RPT.PCP_ATTRIBUTION.DATA_SOURCE As Payer
		,AmbulatoryPCPPractice
		,AmbulatoryPCPName
		,AmbulatoryPCPSpecialty
		,AmbulatoryPCPRecentVisit
		,temp.*
		,tempTR.TotalRegistries
		,PCP_ATTRIBUTION.EnrollmentStatus
	From dbo.open_empi_master 
	Join RPT.PCP_ATTRIBUTION On open_empi_master.EMPI_ID = PCP_ATTRIBUTION.EMPI
	And PCP_ATTRIBUTION.ReportId=(Select Top 1 ReportId from RPT.Report_Details where root_companies_id=root_companies_id order by 1 desc)
	Left Join ##Temp_AllRegitries temp On open_empi_master.EMPI_ID = temp.EMPI
	Left Join #Temp_TotalRegistries tempTR On open_empi_master.EMPI_ID = tempTR.EMPI
	

	DROP TABLE ##Temp_AllRegitries;
	DROP TABLE #Temp_TotalRegistries; 


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END


GO
