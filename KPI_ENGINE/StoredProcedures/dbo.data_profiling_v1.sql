SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE proc [dbo].[data_profiling_v1] @tablename varchar(100),@schemaname varchar(20)
as


Declare @col_ct INT;
Declare @colname varchar(100);
declare @col_type varchar(50);
declare @col_len varchar(20);
declare @minval varchar(50);
declare @maxval nvarchar(max);
declare @top10val nvarchar(max);
declare @rowct INT;
declare @fillct INT;
declare @nullct INT;
declare @uniquect INT;
declare @i INT=0;
declare @isnumber INT;
declare @Sql nvarchar(max);
declare @ttablename varchar(200);

Set @ttablename=concat(@schemaname,'.',@tablename);

-- Delete any existing records

SET @Sql = 'Delete from KPI_ENGINE_SOURCE.dbo.raw_data_profiling where TableName='''+@tablename+''''
EXECUTE sp_executesql @Sql
-- Print 'Delete Records'

-- Get total column count of the table

SET @Sql = 'SELECT @col_ct=count(*) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @tablename+ '''and TABLE_SCHEMA='''+@schemaname+''''

EXECUTE sp_executesql @Sql,N'@col_ct INT OUTPUT',
@col_ct = @col_ct OUTPUT
-- Print 'Get Column Count'

-- Run a loop to get stats for each column
while(@i<@col_ct)
Begin
	
	Set @minval='';
	Set @maxval='';


	-- Get the column name

	SET @Sql = 'SELECT @colname=COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' +@tablename+ '''and TABLE_SCHEMA='''+@schemaname+''' Order by Column_Name ASC OFFSET @ctr ROWS FETCH NEXT 1 ROWS ONLY'
	
	EXECUTE sp_executesql @Sql, N'@ctr INT,@colname varchar(100) OUTPUT',@i,@colname = @colname OUTPUT;
	-- Print 'Get Column Name'
	-- Print @colname



	-- Load data in temp table

	drop table if exists #colinmem;
	create Table #colinmem
	(
		data varchar(600)
		
	)
	create nonclustered index idx_data on #colinmem(data);

	Set @Sql='Insert into #colinmem	select '+@colname+' from '+@ttablename
	EXECUTE sp_executesql @Sql

	-- Check if the Column is numeric


	SELECT @isnumber=Min(ISNUMERIC(data)) from #colinmem where data is not null;
		
	-- Get total Row count
	select @rowct=count(*) from #colinmem;

	
	-- Get Unique value count
	
	select @uniquect=count(distinct data) from #colinmem;

	-- Get FIll rate

	select @fillct=count(*) from #colinmem where len(data)>0 and data is not null

	-- Get Null value count

	select @nullct=count(*) from #colinmem where data is null;

	-- Get top 10 values by occurence

	select @top10val=string_agg(concat(data,':',occurrence),',') from(
	select top 10 data,count(*) as occurrence from #colinmem group by data order by 2 desc
	)t1 

	-- Get min and max value if number else get length of the string
	If(@isnumber=1)
	Begin
		
		select top 1 @minval=min(data) from #colinmem;

	End
	Else
	Begin
		
		select top 1 @minval=data from #colinmem where len(data)=(select min(len(data)) from #colinmem);

	End
------

	If(@isnumber=1)
	Begin
		
		select top 1 @maxval=max(data) from #colinmem;
	End
	Else
	Begin
		
		select top 1 @maxval=data from #colinmem where len(data)=(select max(len(data)) from #colinmem)
	End	

	
--
	Insert into KPI_ENGINE_SOURCE.[dbo].[raw_data_profiling](TableName,ColumnName,RowCt,FillCt,FillPercentage,UniqueValuesCt,NullCt,Minvalue,MaxValue,Top10Values) values(@tablename,@colname,@rowct,@fillct,concat(round((cast(@fillct as float)/cast(@rowct as float))*100,2),'%'),@uniquect,@nullct,@minval,@maxval,@top10val)

	Set @i=@i+1;

	
END

	drop table if exists #colinmem;


GO
