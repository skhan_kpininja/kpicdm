SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE 
  PROCEDURE [dbo].[sp_IndexMaintenance]
AS
BEGIN

--** This SP is created to perform the Indexe Rebuild
--** Created on 2021-09-07

--**Declare Variables	
	DECLARE 
		@counter INT = 1
		,@loop int = 1
		,@query varchar(max) = ''
		;

--**Creating this table for Logging
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Idx_Maintenance_Log')
	BEGIN
		CREATE TABLE dbo.Idx_Maintenance_Log
		(
			Id int not null identity(1,1)
			,LogType varchar(10)
			,LogDesc varchar(max)
			,LogTime datetime
		)
	END

--**SP Start Log
	INSERT INTO dbo.Idx_Maintenance_Log
	VALUES ('INFO', 'SP Execution Started: sp_IndexMaintenance', getdate());

--**Temp table for creating the query for fragmented Indexes
	DROP TABLE IF EXISTS #Temp_Index_Maintenance;
	CREATE TABLE #Temp_Index_Maintenance
	(
		IdxId int not null identity(1,1)
		,SchemaName varchar(10) not null
		,TableName varchar(100) not null
		,IdxName varchar(1000) not null
		,AvgFrag float not null
		,PgeCount bigint not null
		--,Query varchar(max) null
	);

	INSERT INTO #Temp_Index_Maintenance
	SELECT
		S.name as 'Schema',
		T.name as 'Table',
		I.name as 'Index',
		DDIPS.avg_fragmentation_in_percent,
		DDIPS.page_count
	FROM 
		sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS DDIPS
	INNER JOIN 
		sys.tables T on T.object_id = DDIPS.object_id
	INNER JOIN 
		sys.schemas S on T.schema_id = S.schema_id
	INNER JOIN 
		sys.indexes I ON I.object_id = DDIPS.object_id AND DDIPS.index_id = I.index_id
	WHERE 
		DDIPS.database_id = DB_ID()
		and I.name is not null
		AND DDIPS.avg_fragmentation_in_percent > 10					--**considering indexes those are fragmented more than 10%
	ORDER BY DDIPS.avg_fragmentation_in_percent desc;

--**Setting the Loop Variable to number of Indexes to be re-built
	SET @loop = (SELECT MAX(IdxId) FROM #Temp_Index_Maintenance);

--**Index Rebuild Loop Start
	WHILE (@counter <= @loop)
	BEGIN
	
	--**Creating Dynamic Query for Index Rebuild
		SET @query = 
		(
			SELECT 'ALTER INDEX '+IdxName+' ON ['+SchemaName+'].['+TableName+'] REBUILD;'
			FROM #Temp_Index_Maintenance
			WHERE IdxId = @counter
		);
	
	--**Running the Dynamic Query in Try Block. Success Log in case Query Execution is Successful
		BEGIN TRY
			EXEC (@query);

			INSERT INTO dbo.Idx_Maintenance_Log
			VALUES ('INFO', 'COMPLETED EXECUTION OF: '+@query, GETDATE())
		END TRY

	--**Catch Block to log error in case the Rebuild Fails. Rollback is not required as Rebuild automatically rolls back if the command fails
		BEGIN CATCH
			IF ERROR_MESSAGE() IS NOT NULL
			BEGIN
				INSERT INTO dbo.Idx_Maintenance_Log
				VALUES ('ERROR', ERROR_MESSAGE()+' | WHILE EXECUTING QUERY | '+@query, GETDATE())
			END
		END CATCH

	--**Incrementing the Counter
		SET @counter += 1;
	END
--**Index Rebuild Loop End


--**SP End Log
	INSERT INTO dbo.Idx_Maintenance_Log
	VALUES ('INFO', 'SP Execution Completed: sp_IndexMaintenance', getdate());

END
GO
