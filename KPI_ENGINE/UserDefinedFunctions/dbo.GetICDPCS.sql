SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE FUNCTION [dbo].[GetICDPCS]
(
	@rootId INT
	,@startdate Date
	,@enddate Date
	,@valuesetname varchar(300)
)
RETURNS TABLE AS
RETURN 
(
		select 
			p.EMPI
			,p.PROC_START_DATE
			,p.CLAIM_ID
			,p.ICDPCS_CODE
			,PROC_DATA_SRC
			,c.BILL_PROV
		from PROCEDURES p
		left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			ISNULL(c.POS,'')!='81' and 
			PROC_START_DATE between @startdate and @enddate	and
			ICDPCS_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name=@valuesetname
			)
	


)

GO
