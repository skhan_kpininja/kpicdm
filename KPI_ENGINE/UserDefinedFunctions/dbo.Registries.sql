SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE FUNCTION dbo.Registries (@EMPI VARCHAR(250))
RETURNS VARCHAR(MAX)
AS BEGIN
    DECLARE @Registries VARCHAR(MAX)

    Select @Registries = (STUFF((SELECT ', ' + REGISTRY_OUTPUT.RegistryName 
		        FROM REG.REGISTRY_OUTPUT
			    WHERE ReportId=(Select Max(reportId) From KPI_ENGINE.REG.REGISTRY_OUTPUT Where EnrollmentStatus = 'Active')
				And REGISTRY_OUTPUT.EMPI = @EMPI
				FOR XML PATH('')), 1, 1, ''))

    RETURN @Registries
END
GO
