SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE FUNCTION [dbo].[hospicemembers]
(
	@rootId varchar(10)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	select distinct t1.* from
	(
		Select 
			EMPI,
			REV_CODE,
			FROM_DATE,
			BILL_PROV
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and
			FROM_DATE!='' and 
			FROM_DATE between @startdate and @enddate and 
			REV_CODE IN
			(
				select code from HDS.VALUESET_TO_CODE where code_System='UBREV' and Value_Set_Name in('Hospice Encounter')
			)
	
		Union all

		Select 
			p.EMPI,
			p.PROC_CODE,
			p.PROC_START_DATE,
			c.BILL_PROV
		from PROCEDURES p
		left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and 
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE!='' and 
			PROC_START_DATE between @startdate and @enddate and 
			PROC_CODE IN
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention')
			)

		Union all

		Select 
			d.EMPI,
			d.DIAG_CODE,
			d.DIAG_START_DATE,
			c.BILL_PROV
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and
			DIAG_START_DATE between @startdate and @enddate and 
			DIAG_CODE IN
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention')
			)

		Union all

		select 
			EMPI,
			null,
			null,
			null
		from MCFIELDS 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			Rundate!='' and 
			Rundate between @startdate and @enddate and 
			Hospice=1

		Union all
				
		Select
			EMPI,
			VisitTypeCode,
			VisitDate,
			null
		From Visit
		where
			ROOT_COMPANIES_ID=@rootId and 
			VisitDate between @startdate and @enddate and 
			VisitTypeCode IN
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention')
			)
	)t1

)

GO
