SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE VIEW  [RFT].[vw_RFT_NPI]
AS
select NPI,
Case when ISNULL(Provider_Last_Name+Provider_First_Name,'') ='' then '' ELSE Provider_Credential_Text+' '+Provider_Last_Name+', '+Provider_First_Name END as PROV_NAME,
COALESCE(Provider_Organization_Name,Provider_other_Organization_Name) as Practice,
PrimarySpecialty as Specialty
from dbo.RFT_NPI
where NPI in 
(
select ATT_PROV from rpt.FINANCIALS_TEST
union
select BILL_PROV from rpt.FINANCIALS_TEST
)

GO
