SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE view [RPT].[PROV_SCORECARD]
AS

SELECT 
	*
 FROM [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD]
Where
	REPORT_ID=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159  order by ReportEndDate Desc)



GO
