SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE view [RPT].[Scorecard_Network]
AS


			Select distinct
				 ml.MEASURE_TITLE
				,ml.MEASURE_SUBTITLE + ' (' + ml.ReportType + ')' as MEASURE_SUBTITLE
				,s.Measure_id
				,ml.Measure_name
				,null as Denominator
				,null as NUM_Count
				,null as Calc_DEN_COUNT
				,null as Exclusions
				,null as EDVisits
				,null as Gaps
				,null as Target
				,null as Result
				,NetworkResult
			From RPT.PROV_SCORECARD s
			Join RFT.UHN_MeasuresList ml on
				s.MEASURE_ID=ml.Measure_id
			Where
				root_companies_id=159 and
				s.ReportType<>'Network' and
				ml.UHNCalculated=0

				
				

GO
