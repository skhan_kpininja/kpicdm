SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE view [RPT].[Scorecard_Network_Score]
AS
		Select
		
			MEASURE_TITLE
			,MEASURE_SUBTITLE
			,MEASURE_ID
			,MEASURE_NAME
			,Sum(Denominator) as Denominator
			,sum(NUM_COUNT) as Numerator
			,SUM(CALC_DEN_COUNT) as Calc_Denominator
--			,0 as Exclusions
--			,0 as EDVisits
			,SUM(Gaps) as Gaps
			,sum(Result) as Result
			,sum(NetworkResult) as NetworkResult
			,Sum(TARGET) as Target
		From
		(

			Select
				*
				,Case
					When Measure_id not in('5','39','40','41','62','36') then CALC_DEN_COUNT-NUM_COUNT 
					When Measure_id in('5','39','40','41','62') then NUM_COUNT 
					When Measure_id in('36') then EDVisits 
				end as Gaps
				,Case
					when Measure_id!='36' and CALC_DEN_COUNT>0 Then Round(((cast(NUM_COUNT as float)/Cast(CALC_DEN_COUNT as float))*100),2)
					when Measure_id!='36' and CALC_DEN_COUNT<=0 Then 0
					When Measure_id in('36') then cast((Cast(EDVisits as float)/(Select count(distinct EMPI) from RPT.Measure_DetailLine_Snapshot where MEASURE_ID='36'))*1000 as INT)
				end as Result
				,Case
					when Measure_id!='36' and CALC_DEN_COUNT>0 Then Round(((cast(NUM_COUNT as float)/Cast(CALC_DEN_COUNT as float))*100),2)
					when Measure_id!='36' and CALC_DEN_COUNT<=0 Then 0
					When Measure_id in('36') then cast((Cast(EDVisits as float)/(Select count(distinct EMPI) from RPT.Measure_DetailLine_Snapshot where MEASURE_ID='36'))*1000 as INT)
				end as NetworkResult
			From
			(

				select
					
					ps.MEASURE_TITLE
					,ps.MEASURE_SUBTITLE
					,t1.MEASURE_ID
					,MEASURE_NAME
					,Sum(Den) as Denominator
					,SUM(Cast(Calc_NUM as INT)) as NUM_COUNT
					,SUM(Cast(Calc_DEN as INT)) as CALC_DEN_COUNT
					,SUM(EXCL) + SUM(REXCL) as Exclusions
					,sum(Encounters) as EDVisits
					,TARGET
				From
				(
					select distinct 
						MEASURE_ID
						,MEASURE_NAME
						,a.EMPI
						,PayerId
						,MEM_FNAME
						,MEM_MNAME
						,MEM_LNAME
						,MEM_DOB
						,MEM_GENDER
						,ENROLLMENT_STATUS
						,Den
						,Case
							When Num=1 and Excl=0 and Rexcl=0 Then 1
							Else 0
						end as Calc_NUM
						,Case
							When DEN=1 and Excl=0 and Rexcl=0 Then 1
							Else 0
						end as Calc_DEN
						,Excl
						,Rexcl
						,encounters
						,DischargeDate
					From RPT.Measure_DetailLine_Snapshot a
					inner join
					(
                    	
						Select distinct EMPI from RPT.ConsolidatedAttribution_Snapshot 
						--root_companies_id=root_companies_id
                    	--and @global_ConsolidatedAttribution_Practice
						--and @global_ConsolidatedAttribution_Specialty
						--and @global_ConsolidatedAttribution_Prov_Name					
                  ) CA on a.EMPI=CA.EMPI
					Where
				  root_companies_id='159'		
                  and ENROLLMENT_STATUS='Active'
				  and a.ReportType='Network'
				)t1
				Join 
				(
					select
						Measure_id
						,MEASURE_TITLE
						,MEASURE_SUBTITLE+' ('+ReportType+')' as MEASURE_SUBTITLE
						,TARGET
					from RPT.RFT_MEASURE_LIST
				)ps on 
					t1.MEASURE_ID=ps.MEASURE_ID
				Group By
					t1.MEASURE_ID
					,MEASURE_NAME
					,MEASURE_SUBTITLE
					,MEASURE_TITLE
					,TARGET
			)t2
			)t3
		Group by
				MEASURE_ID
				,MEASURE_NAME
				,MEASURE_SUBTITLE
				,MEASURE_TITLE




GO
