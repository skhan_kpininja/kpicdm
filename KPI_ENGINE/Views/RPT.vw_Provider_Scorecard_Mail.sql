SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE view [RPT].[vw_Provider_Scorecard_Mail] as 


SELECT 
	P.PROV_GRP_NAME AS Practice
	,Prov_Spec_desc2 as Specialty
	,Prov_Name as Provider
	,NULL as Report_Type
	,NULL as Report_Quarter
	,EmailAddress as Email_Id
	,UserFullName as UserName
	,PROV_NPI as ProviderNPI
	,159 as ROOT_COMPANIES_ID
	,case when _PROV_UDF_01_='mid-level' then 1 else 0 end AS midlevel
FROM 
	[KPI_ENGINE].[RPT].[Provider_Scorecard_Users] U
	JOIN KPI_ENGINE..PROVIDER P ON U.Practice = P.PROV_GRP_NAME
WHERE 1=1
	AND role like '%PRactice%'
	AND _PROV_UDF_02_='y' 
	AND _PROV_UDF_04_='Primary Address' 
	AND U.ISDELETED = 0

UNION ALL

SELECT 
	P.PROV_GRP_NAME AS Practice
	,Prov_Spec_desc2 as Specialty
	,Prov_Name as Provider
	,NULL as Report_Type
	,NULL as Report_Quarter
	,EmailAddress as Email_Id
	,UserFullName as UserName
	,PROV_NPI as ProviderNPI
	,159 as ROOT_COMPANIES_ID
	,case when _PROV_UDF_01_='mid-level' then 1 else 0 end AS midlevel
FROM 
	[KPI_ENGINE].[RPT].[Provider_Scorecard_Users] U
	JOIN KPI_ENGINE..PROVIDER P ON U.ProviderNPI = P.PROV_NPI
WHERE 1=1
	AND role like '%Provider%' 
	AND _PROV_UDF_02_='y' 
	AND _PROV_UDF_04_='Primary Address' 
	AND U.ISDELETED = 0

UNION ALL

SELECT
	P.PROV_GRP_NAME AS Practice
	,P.Prov_Spec_desc2 as Specialty
	,P.PROV_NAME as Provider
	,NULL as Report_Type
	,NULL as Report_Quarter
	,NULL AS Email_Id
	,NULL as UserName
	,P.PROV_NPI as ProviderNPI
	,159 as ROOT_COMPANIES_ID
	,case when P._PROV_UDF_01_='mid-level' then 1 else 0 end AS midlevel
FROM
	KPI_ENGINE..PROVIDER P


GO
