SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE   PROC [RPT].[SP_AWVGapReport]
AS
BEGIN

/****** Object:  StoredProcedure [RPT].[SP_AWVGapReport]    Script Date: 23-09-2021 ******/
/****** Author: Sameer Gawde ******/
/****** Purpose: This Stored Procedure gets the members with gaps for AWV (Annual Wellness Visit) measure and evaluates the gap reason for the members with the prior AWV Date as additional details ******/

--Declaring and initializing all the required variables
	Declare @rundate date=GetDate();
	DECLARE @meas_year varchar(4) = Year(Dateadd(month,-2,@rundate));
	DECLARE @rootId varchar(3) = '159';
	DECLARE @ce_startdt Date = concat(@meas_year,'-01-01');
	DECLARE @ce_enddt Date = concat(@meas_year,'-12-31');
	DECLARE @ce_startdt1 Date = concat(@meas_year-1,'-01-01');
	DECLARE @ce_enddt1 Date = concat(@meas_year-1,'-12-31');
	DECLARE @measureid varchar(2) = '20'; --20 is the measureID for AWV measure
	DECLARE @startdt Date;
	DECLARE @enddt Date;
	DECLARE @reportId int;
	DECLARE @quarter varchar(5);

--Calling the Stored Procedure to get details of Latest ReportID, Start Date, End Date and Quarter details
	EXEC KPI_ENGINE.DBO.GetReportDetail @rundate,@rootId,@startDate=@startdt Output,@enddate=@enddt output,@reportId=@reportId output,@quarter=@quarter output

--Deleting existing records from Gap Details table for AWV measure
	DELETE KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
	WHERE root_companies_id = @rootId AND MeasureId = @measureid AND REPORTID = @reportId;

--Inserting all the gap members from KPI_ENGINE.RPT.MEASURE_DETAILED_LINE into the Gap Details table
	INSERT INTO KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS (EMPI, root_companies_id, MeasureId, ReportId)
	SELECT DISTINCT
		EMPI
		,ROOT_COMPANIES_ID
		,MEASURE_ID
		,@reportId
	FROM KPI_ENGINE.RPT.MEASURE_DETAILED_LINE HMO
	WHERE 1=1
		AND ROOT_COMPANIES_ID = @rootId
		AND MEASURE_ID = @measureid
		AND NUM = 0
		AND REXCL = 0
		AND EXCL = 0
		AND REPORT_ID = @reportId

--Checking/Marking any members qualifying in the numerator condition. MArking them as Not-A-Gap in the Gap Details table
	UPDATE QGD
	SET 
		NotAGap = 1
		,AddDetails1 = 'Annual Wellness Visit Date'
		,AddValue1 = p.ServiceDate
		,AddDetails2 = 'Annual Wellness Visit Code'
		,AddValue2 = P.Code
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
	JOIN
	(
		Select 
			p.EMPI,
			p.PROC_CODE as Code,
			p.PROC_START_DATE as ServiceDate
		from KPI_ENGINE.dbo.PROCEDURES p
		left outer join KPI_ENGINE.DBO.CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
									ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
									p.PROC_DATA_SRC=c.CL_DATA_SRC and 
									p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
									p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID='159' and 
			c.POS!='81' and
			PROC_START_DATE between '2021-01-01' and '2021-12-31' and 
			PROC_CODE in('G0438','G0439','G0468','G0402')
	) P ON QGD.EMPI = P.EMPI
	where QGD.ROOT_COMPANIES_ID = @rootId AND QGD.MEASUREID = @measureid AND QGD.ReportID = @reportId

--Checking the Last Annual Wellness Visit code for the members still having gaps
	UPDATE QGD
	SET 
		Gap_Reason = 'No Wellness Visit in MY'
		,ImpactDate = @ce_enddt
		,AddDetails1 = 'Last Annual Wellness Visit Date'
		,AddValue1 = p2.ServiceDate
		,AddDetails2 = 'Last Annual Wellness Visit Code'
		,AddValue2 = P2.Code
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
	JOIN 
	(
		Select 
			p.EMPI,
			p.PROC_CODE as Code,
			p.PROC_START_DATE as ServiceDate
		from KPI_ENGINE.dbo.PROCEDURES p
		left outer join KPI_ENGINE.DBO.CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
									ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
									p.PROC_DATA_SRC=c.CL_DATA_SRC and 
									p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
									p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID='159' and 
			c.POS!='81' 
			and PROC_CODE in('G0438','G0439','G0468','G0402')
	) P2 ON QGD.EMPI = P2.EMPI
	WHERE ROOT_COMPANIES_ID = @rootId AND MEASUREID = @measureid AND QGD.ReportID = @reportId AND NotAGap = 0

--Marking the members not having any Annual Wellness Visit till now
	UPDATE QGD
	SET 
		Gap_Reason = 'No Wellness Visit in MY'
		,ImpactDate = @ce_enddt
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
	WHERE ROOT_COMPANIES_ID = @rootId AND MEASUREID = @measureid AND QGD.ReportID = @reportId AND NotAGap = 0 AND Gap_Reason IS NULL

END
GO
