SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE   PROC [RPT].[SP_CBPGapReport]
AS
BEGIN

/****** Object:  StoredProcedure [RPT].[SP_CBPGapReport]    Script Date: 23-09-2021 ******/
/****** Author: Sameer Gawde ******/
/****** Purpose: This Stored Procedure gets the members with gaps for CBP measure and evaluates the gap reason ******/
/****** KPISQL-502 | Change done on 2021-11-03 by Sameer Gawde ******/

	BEGIN TRY
	--Declaring and initializing all the required variables
		DECLARE 
			@SPName varchar(100) = 'KPI_ENGINE_MART.RPT.SP_CBPGapReport'
			,@CodeBlockName varchar(max) = 'Declaring and initializing variables';
		DECLARE 
			@rootId int = 159
			,@measureId varchar(5) = '12'
			,@rundate Date=GetDate()
			,@startDate Date
			,@enddate date
			,@quarter varchar(20)
			,@reportId INT
			;
		DECLARE @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate));
		DECLARE
			@ce_startdt DATE = concat(@meas_year,'-01-01')
			,@ce_middt DATE = concat(@meas_year,'-06-30')
			,@ce_enddt DATE = concat(@meas_year,'-12-31')
			,@ce_startdt1 DATE = concat(@meas_year-1,'-01-01')
			;

	--Calling the Stored Procedure to get details of Latest ReportID, Start Date, End Date and Quarter details
		SET @CodeBlockName = 'Calling KPI_ENGINE..GetReportDetail SP'
		exec KPI_ENGINE..GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

		BEGIN TRAN

		--Deleting existing records from Gap Details table for AWV measure
			SET @CodeBlockName = 'Deleting existing records from KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS table'
			DELETE KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
			WHERE 1=1
				AND ROOT_COMPANIES_ID = @rootId
				AND MeasureId = @measureId
				AND REPORTID = @reportId;

		--Inserting all the gap members from KPI_ENGINE.RPT.MEASURE_DETAILED_LINE into the Gap Details table
			SET @CodeBlockName = 'Populating members in KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS table'
			INSERT INTO KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS (EMPI, root_companies_id, ReportId, MeasureId, ImpactDate)
			SELECT 
				EMPI
				,ROOT_COMPANIES_ID
				,REPORT_ID
				,MEASURE_ID
				--,@enddate
				,@ce_enddt
--Change Log: 2021-11-03: (SG) As per Stef's email, she was seeing 2021-07-31 as the Impact Date which should ideally be the year end date for CBP measure. In this case @enddate variable gives the quarter end date that the report ran for. SO changed the variable from @enddate to @ce_enddate
			FROM KPI_ENGINE.RPT.MEASURE_DETAILED_LINE
			WHERE 1=1
				AND ROOT_COMPANIES_ID = @rootId
				AND MEASURE_ID = @measureId
				AND NUM = 0
				AND EXCL = 0
				AND REXCL = 0
				AND REPORT_ID = @reportId

		--CBP Measure Denominator Conditions
			SET @CodeBlockName = 'CBP Measure Denominator Conditions'
			drop table if exists #cbp_hypertension;
			CREATE table #cbp_hypertension
			(
				EMPI varchar(100),
				servicedate Date,
				CLAIM_ID varchar(100)
			)				
			insert into #cbp_hypertension
			Select distinct
				EMPI
				,DIAG_START_DATE
				,CLAIM_ID
			From KPI_ENGINE.DBO.GetDiagnosis(@rootId,@ce_startdt1,@ce_middt,'Essential Hypertension')
			Where
				DIAG_DATA_SRC not in(select Data_Source from KPI_ENGINE.DBO.Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)

			drop table if exists #cbp_hypertensionvstlist;				
			CREATE table #cbp_hypertensionvstlist
			(
				EMPI varchar(100),
				servicedate Date,
				CLAIM_ID varchar(100)
			)
			insert into #cbp_hypertensionvstlist
			Select distinct *
			From
			(
				Select
					EMPI
					,PROC_START_DATE
					,CLAIM_ID
				From KPI_ENGINE.DBO.GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Outpatient Without UBREV')
				Where
					PROC_DATA_SRC not in(select Data_Source from KPI_ENGINE.DBO.[Data_source] where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
				Union all
				Select
					EMPI
					,PROC_START_DATE
					,CLAIM_ID
				From KPI_ENGINE.DBO.GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Online Assessments')
				Where
					PROC_DATA_SRC not in(select Data_Source from KPI_ENGINE.DBO.Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
				Union all
				Select
					EMPI
					,PROC_START_DATE
					,CLAIM_ID
				From KPI_ENGINE.DBO.GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Telephone Visits')
				Where
					PROC_DATA_SRC not in(select Data_Source from KPI_ENGINE.DBO.Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
			)t1	

			IF EXISTS (SELECT * FROM tempdb.sys.OBJECTS WHERE [NAME] LIKE '%CBPEvents%')
			BEGIN
				DROP TABLE #CBPEvents
			END

			SELECT
				EMPI
				,DiagnosisDate
				,DiagnosisClaimID
				,DiagnosisCode
				,VisitProcCode
			INTO #CBPEvents
			FROM
			(
				SELECT
					QGD.EMPI
					,HYP.DIAG_START_DATE AS DiagnosisDate
					,HYP.CLAIM_ID AS DiagnosisClaimID
					,HYP.DIAG_CODE AS DiagnosisCode
					,vst.PROC_CODE AS VisitProcCode
					,MAX(DIAG_START_DATE) OVER (PARTITION BY QGD.EMPI) AS MostRecentDiagnosis
					,COUNT(HYP.DIAG_CODE) OVER (PARTITION BY QGD.EMPI) AS NumberofDIagnosis
					,ROW_NUMBER() OVER (PARTITION BY QGD.EMPI ORDER BY HYP.DIAG_START_DATE DESC) AS RN
				FROM
					KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
				LEFT JOIN
				(	
					Select distinct
						EMPI
						,DIAG_START_DATE
						,CLAIM_ID
						,DIAG_CODE
					From KPI_ENGINE.DBO.GetDiagnosis(@rootId,@ce_startdt1,@ce_middt,'Essential Hypertension')
					Where
						DIAG_DATA_SRC not in (select [Data_Source] from KPI_ENGINE.DBO.[Data_source] where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
				) HYP ON QGD.EMPI = HYP.EMPI
				JOIN
				(
					Select distinct 
						*
					From
					(
						Select
							EMPI
							,PROC_START_DATE
							,CLAIM_ID
							,PROC_CODE
						From KPI_ENGINE.DBO.GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Outpatient Without UBREV')
						Where
							PROC_DATA_SRC not in(select [Data_Source] from KPI_ENGINE.DBO.[Data_source] where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
						Union all
						Select
							EMPI
							,PROC_START_DATE
							,CLAIM_ID
							,PROC_CODE
						From KPI_ENGINE.DBO.GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Online Assessments')
						Where
							PROC_DATA_SRC not in(select [Data_Source] from KPI_ENGINE.DBO.[Data_source] where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
						Union all
						Select
							EMPI
							,PROC_START_DATE
							,CLAIM_ID
							,PROC_CODE
						From KPI_ENGINE.DBO.GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Telephone Visits')
						Where
							PROC_DATA_SRC not in(select [Data_Source] from KPI_ENGINE.DBO.[Data_source] where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
					) UN
				) VST ON HYP.EMPI = VST.EMPI AND HYP.CLAIM_ID = VST.CLAIM_ID
				WHERE 1=1
					AND QGD.ROOT_COMPANIES_ID = @rootId
					AND QGD.MeasureId = @measureId
					AND QGD.ReportId = @reportId
			) A
			WHERE 1=1
				AND A.RN = 1
				AND A.NumberofDIagnosis > 1

		--Update KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS with Denominator details
			SET @CodeBlockName = 'Update KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS with Denominator details'
			UPDATE QGD
			SET
				QGD.AdditionalDetails = CBPEO.AdditionalData
			FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
			CROSS APPLY
			(
				SELECT TOP 1 DIAGNOSISDATE AS "ServiceDate", DiagnosisCode AS "DiagCode", VisitProcCode AS "ProcCode"
				FROM #CBPEvents CBPEI
				WHERE CBPEI.EMPI = QGD.EMPI
				FOR JSON PATH, ROOT('Denominator')
			) CBPEO(AdditionalData)	
			WHERE 1=1
				AND QGD.root_companies_id = @rootId AND QGD.ReportId = @reportId AND QGD.MeasureId = @measureId


		----**** Members not having BP reading

			SET @CodeBlockName = 'Calculating Members not having BP reading'
			drop table if exists #numvstlist;				
			CREATE table #numvstlist
			(
			EMPI varchar(100),
			servicedate Date,
			code varchar(20),
			Concept varchar(20)
			)	

			Insert into #numvstlist
			Select distinct
				*
			From
			(
				Select 
					EMPI
					,PROC_START_DATE
					,PROC_CODE
					,'Procedure' AS Concept
				From KPI_ENGINE.DBO.PROCEDURES 
				where
					ROOT_COMPANIES_ID=159 and
					ISNULL(PROC_STATUS,'EVN')!='INT' AND
					PROC_START_DATE between '2021-01-01' and '2021-12-31' and
					PROC_CODE in
					(
						select Code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
					)
				Union all
				Select 
					EMPI
					,VisitDate
					,VisitTypeCode
					,'Visit' AS Concept
				From KPI_ENGINE.DBO.VISIT 
				where
					ROOT_COMPANIES_ID=@rootId and
					VisitDate between @ce_startdt and @ce_enddt and
					VisitTypeCode in
					(
						select Code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
					)
				Union all
				Select 
					EMPI
					,DIAG_START_DATE
					,DIAG_CODE
					,'Diagnosis' AS Concept
				From KPI_ENGINE.DBO.Diagnosis 
				where
					ROOT_COMPANIES_ID=@rootId and
					DIAG_START_DATE between @ce_startdt and @ce_enddt and
					DIAG_CODE in
					(
						select Code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
					)	
			)t1

		-- Numerator compliant members
			SET @CodeBlockName = 'Calculating Numerator compliant members'
			drop table if exists #numcompliantmemlist;				
			CREATE table #numcompliantmemlist
			(
				EMPI varchar(100),
				servicedate Date,
				Code varchar(20),
				[value] float		
			)	

			insert into #numcompliantmemlist
			select distinct 
				EMPI
				,servicedate 
				,Code
				,[value]
			from
			(
				select distinct 
					s.EMPI
					,s.servicedate 
					,s.Code
					,s.[value]
				from
				(
					Select 
						EMPI
						,Servicedate
						,Code
						,[value]
					from
					(
						Select
							EMPI
							,Cast(ResultDate as Date) as ServiceDate
							,ResultCode as Code
							,Try_convert(decimal,SUBSTRING(value, PATINDEX('%[0-9]%', value), LEN(value))) as value
						From KPI_ENGINE.DBO.LAB
						Where
							ROOT_COMPANIES_ID=@rootId and
							ResultDate between @ce_startdt and @ce_enddt and
							(
								TestCode in
								(
									select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
								)
								or
								ResultCode in
								(
									select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
								)
							)
					)t1
					Union ALL
					Select
						EMPI
						,Cast(VitalDateTime as Date) as ServiceDate
						,VitalCode as Code
						,[value]
					From KPI_ENGINE.DBO.VITAL
					Where
						ROOT_COMPANIES_ID=@rootId and
						VitalDateTime between @ce_startdt and @ce_enddt and
						VitalCode in
						(
							select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
						)
				
				)s
				join
				(
					Select distinct
						EMPI
						,Servicedate
						,Code
						,[value]
					from
					(
						Select 
							EMPI
							,Cast(ResultDate as Date) as ServiceDate
							,ResultCode as Code
							,Try_convert(decimal,SUBSTRING(value, PATINDEX('%[0-9]%', value), LEN(value))) as value
						From KPI_ENGINE.DBO.LAB
						Where
							ROOT_COMPANIES_ID=@rootId and
							ResultDate between @ce_startdt and @ce_enddt and
							(
								TestCode in
								(
									select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
								)
								or
								ResultCode in
								(
									select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
								)
							)
					)t1
					Union ALL
					Select 
						EMPI
						,Cast(VitalDateTime as Date) as ServiceDate
						,VitalCode as Code
						,[value]
					From KPI_ENGINE.DBO.VITAL
					Where
						ROOT_COMPANIES_ID=@rootId and
						VitalDateTime between @ce_startdt and @ce_enddt and
						VitalCode in
						(
							select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
						)
				)d on 
					d.EMPI=s.EMPI and 
					d.servicedate=s.servicedate
				Union all
				Select
					s.EMPI
					,s.ServiceDate
					,s.Code
					,null as [value]
				From
				(
					Select distinct
						EMPI
						,Proc_start_date as ServiceDate
						,Proc_Code as Code
					From KPI_ENGINE.DBO.GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Systolic Less Than 140,Systolic Greater Than or Equal To 140','CPT CAT II Modifier')		
				)s
				Join
				( 
					Select distinct
						EMPI
						,Proc_start_date as ServiceDate
						,Proc_Code as Code
					From KPI_ENGINE.DBO.GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diastolic 80-89,Diastolic Less Than 80,Diastolic Greater Than or Equal To 90','CPT CAT II Modifier')
				)d on 
					s.EMPI=d.EMPI and 
					s.servicedate=d.servicedate
			)numcompmemlist

		--Checking the Gaps
		----------------------********************** No BP Reading
			SET @CodeBlockName = 'Calculating Members with No BP Reading'

			UPDATE QGD
			SET QGD.Gap_Reason = 'No BP Reading in MY'
			--DECLARE @rootId INT = 159, @measureId VARCHAR(5) = '12', @REPORTID INT = 31, @ce_startdt DATE = '2021-01-01', @ce_enddt DATE = '2021-12-31';
			--SELECT *
			FROM 
				KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
				LEFT JOIN #numcompliantmemlist NUM ON QGD.EMPI = NUM.EMPI 
				WHERE 1=1
					AND QGD.ROOT_COMPANIES_ID = @rootId AND QGD.MeasureId = @measureId AND QGD.REPORTID = @reportId AND NUM.Code IS NULL;

		----------------------********************** High Result by CPT Code

			SET @CodeBlockName = 'Calculating Members with High Result by CPT Code'
			UPDATE QGD
			SET
				QGD.Gap_Reason = 'Most Recent Uncontrolled BP'
				,QGD.AddDetails1 = 'Most Recent BP Date'
				,QGD.AddValue1 = CAST(NUM.servicedate AS VARCHAR(20))
				,QGD.AddDetails2 = 'Most Recent BP Code'
				,QGD.AddValue2 = NUM.Code
			FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
			JOIN 
			(
				SELECT DISTINCT
					EMPI
					,servicedate
					,Code
				FROM 
				(
					SELECT 
						EMPI
						,servicedate
						,Code
						,ROW_NUMBER() OVER (PARTITION BY EMPI ORDER BY servicedate DESC) RN
					FROM #numcompliantmemlist
					WHERE [value] IS NULL
					AND Code IN (SELECT DISTINCT CODE FROM KPI_ENGINE.HDS.VALUESET_TO_CODE WHERE Value_Set_Name IN ('Diastolic Greater Than or Equal To 90','Systolic Greater Than or Equal To 140'))
				) A
				WHERE A.RN = 1
			) NUM ON QGD.EMPI = NUM.EMPI AND QGD.ROOT_COMPANIES_ID = @rootId AND QGD.MeasureId = @measureId AND QGD.REPORTID = @reportId AND QGD.Gap_Reason IS NULL
			WHERE 1=1

		----------------------********************** High Result by Result Value

			SET @CodeBlockName = 'Calculating Members with High Result by Result Value'
			UPDATE QGD
			SET
				QGD.Gap_Reason = 'Most Recent Uncontrolled BP'
				,QGD.AddDetails1 = 'Most Recent BP Date'
				,QGD.AddValue1 = CAST(NUM.servicedate AS VARCHAR(20))
				,QGD.AddDetails2 = 'Most Recent BP Code'
				,QGD.AddValue2 = NUM.Code
				,QGD.AddDetails3 = 'BP Reading'
				,QGD.AddValue3 = CAST(NUM.[value]  AS VARCHAR(10))
			FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
			JOIN 
			(
				SELECT 
					EMPI
					,servicedate
					,Code
					,[value]
				FROM #numcompliantmemlist
				WHERE [value] >=140
			) NUM ON QGD.EMPI = NUM.EMPI AND QGD.ROOT_COMPANIES_ID = @rootId AND QGD.MeasureId = @measureId AND QGD.REPORTID = @reportId AND QGD.Gap_Reason IS NULL

		----------------------********************** Incomplete Result

			SET @CodeBlockName = 'Calculating Members with Incomplete Result'
			UPDATE QGD
			SET
				QGD.Gap_Reason = 'Incomplete BP Reading'
				,QGD.AddDetails1 = 'Incomplete Reading Date'
				,QGD.AddValue1 = CAST(NUM.servicedate AS VARCHAR(20))
			FROM 
				KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS QGD
			LEFT JOIN #numcompliantmemlist NUM ON QGD.EMPI = NUM.EMPI 
			WHERE 1=1
				AND QGD.ROOT_COMPANIES_ID = @rootId AND QGD.MeasureId = @measureId AND QGD.REPORTID = @reportId AND QGD.Gap_Reason IS NULL
			;
		COMMIT TRAN;
	END TRY
	BEGIN CATCH
		INSERT INTO KPI_ENGINE_MART.RPT.SPErrorLog
		SELECT 
			@SPName
			,@CodeBlockName
			,ERROR_MESSAGE() AS ERROR
			,GETDATE() AS DATEENTERED
			,NULL AS OPTIONALPARAMETERS

		ROLLBACK;

	END CATCH

END
GO
