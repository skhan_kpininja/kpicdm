SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [RPT].[SP_CDC4GapReport]
AS
BEGIN

/****** Object:  StoredProcedure [RPT].[SP_CDC4GapReport]    Script Date: 22-09-2021 19:53:37 ******/
/****** Author: Sameer Gawde ******/
/****** Purpose: This Stored Procedure gets the members with gaps for CDC4 (Eye Care) measure and evaluates the gap reason for the members ******/

	Declare @rundate date=GetDate();
	DECLARE @meas_year varchar(4) = Year(Dateadd(month,-2,@rundate));
	DECLARE @rootId int = 159;
	DECLARE @ce_startdt Date = concat(@meas_year,'-01-01');
	DECLARE @ce_enddt Date = concat(@meas_year,'-12-31');
	DECLARE @ce_startdt1 Date = concat(@meas_year-1,'-01-01');
	DECLARE @ce_enddt1 Date = concat(@meas_year-1,'-12-31');
	DECLARE @measureid char(1) = '8';
	DECLARE @startdate Date;
	DECLARE @enddate Date;
	DECLARE @quarter varchar(20);
	DECLARE @reportId INT;


	EXEC KPI_ENGINE.[dbo].[GetReportDetail] @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	DELETE KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
	WHERE root_companies_id = @rootId AND MeasureId = @measureid AND ReportId = @reportId

	INSERT INTO KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS (EMPI, root_companies_id, MeasureId, ReportId)
	SELECT DISTINCT 
		EMPI
		,ROOT_COMPANIES_ID
		,MEASURE_ID
		,REPORT_ID
	FROM
		KPI_ENGINE.RPT.MEASURE_DETAILED_LINE HMO
	WHERE 1=1
		AND ROOT_COMPANIES_ID = @rootId
		AND MEASURE_ID = @measureid
		AND NUM = 0
		AND EXCL = 0
		AND REXCL = 0
		AND REPORT_ID = @reportId
	 
	-------****** Condition 1

	UPDATE
		CGL
	SET
		Gap_Reason = 
			CASE 
				WHEN P.EMPI IS NULL THEN 'Diabetic Retinal Screening not performed in Measurement year'
				WHEN P.EMPI = CGL.EMPI THEN 'Member has valid Diabetic Retinal Screening code in Measurement year'
			END
	FROM 
		KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CGL
	LEFT JOIN
	(
		SELECT * 
		FROM
		(
			SELECT DISTINCT 
				*
				,ROW_NUMBER() OVER (PARTITION BY EMPI ORDER BY PROC_START_DATE DESC) AS RN
			FROM
			(
				select 
					p.EMPI
					,p.PROC_START_DATE
					,PROC_CODE
				from KPI_ENGINE.DBO.PROCEDURES p
				JOIN KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CDC4 ON P.EMPI = CDC4.EMPI
				Left outer join KPI_ENGINE.DBO.CLAIMLINE c on 
					p.CLAIM_ID=c.CLAIM_ID and 
					ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
					p.PROC_DATA_SRC=c.CL_DATA_SRC and
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					p.EMPI=c.EMPI
				Join KPI_ENGINE.DBO.PROVIDER_FLAGS prv on
						c.ATT_NPI=prv.ProvId and
						p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
						EyeCareProv='Y'
				where 
					p.ROOT_COMPANIES_ID=@rootId and
					ISNULL(PROC_STATUS,'EVN')!='INT' and
					ISNULL(c.POS,'')!='81' and
					p.PROC_START_DATE between @ce_startdt and @ce_enddt and 
					PROC_Code in
					(
						select CODE from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
					)

				Union All

				select 
					p.EMPI
					,p.PROC_START_DATE
					,PROC_CODE
				from KPI_ENGINE.DBO.PROCEDURES p
				JOIN KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CDC4 ON P.EMPI = CDC4.EMPI
				join KPI_ENGINE.DBO.Visit v on 
					p.EMPI=v.EMPI and 
					p.PROC_DATA_SRC=v.Visit_DATA_SRC and
					p.ROOT_COMPANIES_ID=v.ROOT_COMPANIES_ID and
					p.PROC_START_DATE=v.VisitDate
				Join KPI_ENGINE.DBO.PROVIDER_FLAGS prv on
						v.VisitProviderNPI=prv.ProvId and
						p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
						EyeCareProv='Y'
				where 
					p.ROOT_COMPANIES_ID=@rootId and
					ISNULL(PROC_STATUS,'EVN')!='INT' and
					p.PROC_START_DATE between @ce_startdt and @ce_enddt and 
					PROC_Code in
					(
						select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
					)
			) A
		) B 
		WHERE RN = 1
	) P ON CGL.EMPI = P.EMPI 
	WHERE 1=1
		AND CGL.ROOT_COMPANIES_ID = @rootId 
		AND CGL.MeasureId = @measureid 
		AND CGL.ReportId = @reportId


	UPDATE KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS
	SET NotAGap = 1
	WHERE Gap_Reason = 'Member has valid Diabetic Retinal Screening code in Measurement year'
	AND ROOT_COMPANIES_ID = @rootId AND MeasureId = @measureid AND ReportId = @reportId

	-------******


	-------******Condition 2


	drop table if exists #cdc_memberswdbmwoc1
	CREATE table #cdc_memberswdbmwoc1
	(
		EMPI varchar(100),
		servicedate Date,
		DATA_SRC varchar(50)
						
	)
	Insert into #cdc_memberswdbmwoc1
	Select
		EMPI
		,DIAG_START_DATE
		,DIAG_DATA_SRC
	From KPI_ENGINE.DBO.GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes Mellitus Without Complications')


	-- Members with Diabetic Retinal Screening prior year
	drop table if exists #cdc_mem_w_retinalscreening1;
	CREATE table #cdc_mem_w_retinalscreening1
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20),
		DATA_SRC varchar(50)
		,NPI varchar(20)				
	)	
	insert into #cdc_mem_w_retinalscreening1
	Select distinct
		EMPI
		,PROC_START_DATE
		,PROC_CODE
		,PROC_DATA_SRC
		,NPI
	From
	(
		select 
			p.EMPI
			,p.PROC_START_DATE
			,PROC_CODE
			,PROC_DATA_SRC
			,ATT_NPI AS NPI
		from KPI_ENGINE.DBO.PROCEDURES p
		join KPI_ENGINE.DBO.CLAIMLINE c on 
			p.CLAIM_ID=c.CLAIM_ID and
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
			p.PROC_DATA_SRC=c.CL_DATA_SRC and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.EMPI=c.EMPI
		Join KPI_ENGINE.DBO.PROVIDER_FLAGS prv on
			c.ATT_NPI=prv.ProvId and
			p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
			EyeCareProv='Y'

		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			ISNULL(c.POS,'0')!='81' and
			p.PROC_START_DATE between @ce_startdt1 and @ce_enddt1 and 
			PROC_CODE in
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)
			and 		p.CLAIM_ID is not null
			
		
		Union all

		select 
			p.EMPI
			,p.PROC_START_DATE
			,PROC_CODE
			,PROC_DATA_SRC
			,visitProviderNPI
		from KPI_ENGINE.DBO.PROCEDURES p
		Join KPI_ENGINE.DBO.Visit v on 
			p.EMPI=v.EMPI and
			p.ROOT_COMPANIES_ID=v.ROOT_COMPANIES_ID and
			p.PROC_START_DATE=v.VisitDate and
			p.PROC_DATA_SRC=v.VISIT_DATA_SRC
		Join KPI_ENGINE.DBO.PROVIDER_FLAGS prv on
			v.visitProviderNPI=prv.ProvId and
			p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
			EyeCareProv='Y'
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			p.PROC_START_DATE between @ce_startdt1 and @ce_enddt1 and 
			PROC_CODE in
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)

	) t1
	
	drop table if exists #CDC4_3_1
	CREATE table #CDC4_3_1
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20),
		ProvNPI varchar(20),
		DBMWOC_ServiceDate Date,
		DBMWOC_DS varchar(20)
	)	
	Insert into #CDC4_3_1
	select distinct 
		t1.EMPI 
		,t1.servicedate
		,t1.Code
		,T1.NPI
		,t2.servicedate
		,t2.DATA_SRC

	from #cdc_mem_w_retinalscreening1 t1
	left join #cdc_memberswdbmwoc1 t2 on 
		t1.EMPI=t2.EMPI and 
		t1.servicedate=t2.servicedate and
		t1.DATA_SRC=t2.DATA_SRC

	UPDATE CGL
	SET
		CGL.Gap_Reason = 'Diabetic Retinal Screening in Prior Year without diagnosis of Diabetes Mellitus Without Complications'
		,CGL.AddDetails1 = 'Service Date of Diabetic Retinal Screening in Prior Year'
		,CGL.AddValue1 = P.ServiceDate
		,CGL.AddDetails2 =  'Code of Diabetic Retinal Screening in Prior Year'
		,CGL.AddValue2 = P.Code
		,CGL.AddDetails3 =  'Provider NPI'
		,CGL.AddValue3 = P.ProvNPI
	--SELECT *
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CGL
	JOIN
	(
		SELECT *
		FROM
		(
			SELECT 
				*
				,ROW_NUMBER() OVER (PARTITION BY EMPI ORDER BY ServiceDate DESC) AS RN
			FROM #CDC4_3_1
		) A
		WHERE RN = 1
	) P ON CGL.EMPI = P.EMPI AND CGL.ROOT_COMPANIES_ID = @rootId AND CGL.MeasureId = @measureid AND CGL.ReportId = @reportId AND CGL.NOTAGAP = 0 AND DBMWOC_ServiceDate IS NULL



	-------******


	-------******Condition 3

	drop table if exists #CDC4_4_1;
	CREATE table #CDC4_4_1
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)	
	insert into #CDC4_4_1
	Select distinct
		*
	From
	(
		Select
			EMPI
			,PROC_START_DATE
			,PROC_CODE
		From KPI_ENGINE.DBO.GetPROCEDURESWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Eye Exam With Evidence of Retinopathy','CPT CAT II Modifier')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,PROC_CODE
		From KPI_ENGINE.DBO.GetPROCEDURESWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Eye Exam Without Evidence of Retinopathy','CPT CAT II Modifier')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,PROC_CODE
		From KPI_ENGINE.DBO.GetPROCEDURES(@rootId,@ce_startdt,@ce_enddt,'Automated Eye Exam')
	)t1


	UPDATE CGL
	SET 
		NOTAGAP = 1
		,Gap_Reason = 'Member has valid Eye Exam with or without Evidence of Retinopathy in MY'
		,CGL.AddDetails1 = 'Service Date of Eye Exam With/Without Evidence of Retinopathy'
		,CGL.AddValue1 = C4.ServiceDate
		,CGL.AddDetails2 = 'Code of Eye Exam With/Without Evidence of Retinopathy'
		,CGL.AddValue2 = C4.Code
	FROM 
		KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CGL
		LEFT JOIN 
		(
			SELECT *
			FROM
			(
				SELECT 
					*
					,ROW_NUMBER() OVER (PARTITION BY EMPI ORDER BY ServiceDate DESC) RN
				FROM #CDC4_4_1
			) A
			WHERE RN = 1
		) C4
		ON CGL.EMPI = C4.EMPI
	WHERE NOTAGAP = 0 AND CGL.ROOT_COMPANIES_ID = @rootId AND CGL.MeasureId = @measureid AND CGL.ReportId = @reportId AND AddDetails1 IS NULL AND C4.EMPI IS NOT NULL

	-------******


	-------******Condition 4

	drop table if exists #CDC4_5_1;
	CREATE table #CDC4_5_1
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)
	insert into #CDC4_5_1
	Select
		EMPI
		,PROC_START_DATE
		,PROC_CODE
	From KPI_ENGINE.DBO.GetPROCEDURESWithOutMods(@rootId,@ce_startdt1,@ce_enddt1,'Eye Exam Without Evidence of Retinopathy','CPT CAT II Modifier')


	UPDATE CGL
	SET
		NOTAGAP = 1
		,Gap_Reason = 'Member has valid Eye Exam Without Evidence of Retinopathy in prior year'
		,CGL.AddDetails1 = 'Service Date of Eye Exam Without Evidence of Retinopathy in prior year'
		,CGL.AddValue1 = C5.ServiceDate
		,CGL.AddDetails2 = 'Code of Eye Exam Without Evidence of Retinopathy in prior year'
		,CGL.AddValue2 = C5.Code
	--SELECT * 
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CGL
	LEFT JOIN
	(
		SELECT *
		FROM
		(
			SELECT 
				*
				,ROW_NUMBER() OVER (PARTITION BY EMPI ORDER BY ServiceDate DESC) AS RN
			FROM #CDC4_5_1
		) A
		WHERE RN = 1
	) C5 ON CGL.EMPI = C5.EMPI
	WHERE CGL.NOTAGAP = 0 AND CGL.ROOT_COMPANIES_ID = @rootId AND CGL.MeasureId = @measureid AND CGL.ReportId = @reportId AND CGL.AddDetails1 IS NOT NULL AND C5.EMPI IS NOT NULL

	-------******



	-------******Condition 5

	drop table if exists #CDC4_6_1;
	CREATE table #CDC4_6_1
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)	
	insert into #CDC4_6_1
	Select
		EMPI
		,PROC_START_DATE
		,PROC_CODE
	From KPI_ENGINE.DBO.GetPROCEDURESWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diabetic Retinal Screening Negative In Prior Year','CPT CAT II Modifier')

	UPDATE CGL
	SET
		NOTAGAP = 1
		,Gap_Reason = 'Member has valid Eye Exam Without Evidence of Retinopathy in prior year'
		,CGL.AddDetails1 = 'Service Date of Eye Exam Without Evidence of Retinopathy in prior year'
		,CGL.AddValue1 = C6.ServiceDate
		,CGL.AddDetails2 = 'Code of Eye Exam Without Evidence of Retinopathy in prior year'
		,CGL.AddValue2 = C6.Code
	--SELECT * 
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CGL
	LEFT JOIN
	(
		SELECT *
		FROM
		(
			SELECT 
				*
				,ROW_NUMBER() OVER (PARTITION BY EMPI ORDER BY ServiceDate DESC) AS RN
			FROM #CDC4_6_1
		) A
		WHERE RN = 1
	) C6 ON CGL.EMPI = C6.EMPI
	WHERE CGL.NOTAGAP = 0 AND CGL.ROOT_COMPANIES_ID = @rootId AND CGL.MeasureId = @measureid AND CGL.ReportId = @reportId AND CGL.AddDetails1 IS NOT NULL AND C6.EMPI IS NOT NULL;


	-------******

	UPDATE CGL
	SET CGL.Gap_Reason = 
		CASE
			WHEN CGL.Gap_Reason = 'Diabetic Retinal Screening not performed in Measurement year' THEN 'No Test'
			WHEN CGL.Gap_Reason = 'Diabetic Retinal Screening in Prior Year without diagnosis of Diabetes Mellitus Without Complications' THEN 'Test in Prior Year without required diagnosis'
		ELSE CGL.Gap_Reason
		END
		,CGL.ImpactDate = @ce_enddt
	FROM KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS CGL
	WHERE 1=1
		AND CGL.root_companies_id = @rootId 
		AND CGL.ReportId = @reportId
		AND CGL.MeasureId = @measureid

END

GO
