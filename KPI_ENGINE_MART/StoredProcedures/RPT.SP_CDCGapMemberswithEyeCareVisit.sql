SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE   PROCEDURE [RPT].[SP_CDCGapMemberswithEyeCareVisit]
AS
BEGIN

/****** Object:  StoredProcedure CREATE OR ALTER PROCEDURE [RPT].[SP_CDCGapMemberswithEyeCareVisit]    Script Date: 18-10-2021 ******/
/****** Author: Sameer Gawde ******/
/****** Purpose: This Stored Procedure gets list of eye care visits for the members having gap ******/

	Declare @rundate date=GetDate();
	DECLARE @meas_year varchar(4) = Year(Dateadd(month,-2,@rundate));
	DECLARE @rootId int = 159;
	DECLARE @ce_startdt Date = concat(@meas_year,'-01-01');
	DECLARE @ce_enddt Date = concat(@meas_year,'-12-31');
	DECLARE @ce_startdt1 Date = concat(@meas_year-1,'-01-01');
	DECLARE @ce_enddt1 Date = concat(@meas_year-1,'-12-31');
	DECLARE @measureid char(1) = '8';
	DECLARE @startdate Date;
	DECLARE @enddate Date;
	DECLARE @quarter varchar(20);
	DECLARE @reportId INT;


	EXEC KPI_ENGINE.[dbo].[GetReportDetail] @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output;

	drop table IF EXISTS #CDCGapMemberswithEyeCareVisit;


	DELETE KPI_ENGINE_MART.RPT.GapSupportingReport
	WHERE root_companies_id = @rootId AND ReportId = @reportId AND Measure_Id = @measureId;

	SELECT
 		A.EMPI
		,M.MEM_FNAME+' '+M.MEM_LNAME as MEMBER_NAME
		,M.MEM_GENDER
		,M.MEM_DOB
		,[Service Date]
		,[Billing Provider NPI]
		,[Billing Provider]
		,[Billing Provider TIN]
		,[Attending Provider NPI]
		,[Attending Provider]
		,[Attending Provider Specialty]
		,CL_DATA_SRC AS [PAYER]
		,@rootId as root_companies_id
		,@reportId as ReportId
		,@measureId as Measure_id
	INTO #CDCGapMemberswithEyeCareVisit
	FROM
	  (
		SELECT  
			CL.EMPI,
			CL.FROM_DATE as [Service Date],
			CL.BILL_PROV as [Billing Provider NPI],
			UPPER(coalesce(NULLIF(concat(BNPI.Provider_Last_Name,' ',BNPI.Provider_First_Name),' '),BNPI.Provider_Organization_Name,BILL_PROV_NAME)) as [Billing Provider],
			CL.BILL_PROV_TIN as [Billing Provider TIN],
			CL.ATT_NPI as [Attending Provider NPI],
			coalesce(NULLIF(concat(NPI.Provider_Last_Name,' ',NPI.Provider_First_Name),' '),NULLIF(ATT_PROV_NAME,'')) as [Attending Provider],
			NPI.PrimarySpecialty as [Attending Provider Specialty],
			CL.CL_DATA_SRC,
			ROW_NUMBER() OVER (PARTITION BY CL.EMPI ORDER BY CL.FROM_DATE DESC) AS ROW_NUM
		FROM
		  KPI_ENGINE.dbo.CLAIMLINE CL (NOLOCK)
		  LEFT JOIN KPI_ENGINE.dbo.rft_npi NPI (NOLOCK) ON CL.ATT_NPI = NPI.NPI and NPI.Entity_Type_Code=1
		  LEFT JOIN KPI_ENGINE.dbo.rft_npi BNPI (NOLOCK) ON CL.BILL_PROV = BNPI.NPI
		  JOIN KPI_ENGINE.dbo.Provider_Flags PF (NOLOCK) ON CL.ATT_NPI = PF.ProvID
		  AND EyeCareProv = 'Y'
		  JOIN KPI_ENGINE.RPT.Measure_DetailLine_Snapshot HMO (NOLOCK) ON CL.EMPI = HMO.EMPI
		WHERE
		  CL.root_companies_id=@rootId
		  AND HMO.MEASURE_ID = '8'
		  AND HMO.NUM = 0
		  AND HMO.EXCL = 0
		  AND HMO.REXCL = 0
		  AND CL.FROM_DATE between Dateadd(yy,-1,DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
		  and DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, -1)
	  ) A
	  JOIN KPI_ENGINE.DBO.MEMBER M ON A.EMPI = M.EMPI
	WHERE
	  ROW_NUM = 1;


	INSERT INTO KPI_ENGINE_MART.RPT.GapSupportingReport
	(EMPI,ServiceDate,BillingProvider,BillingProviderNPI,BillingProviderTIN,ROOT_COMPANIES_ID,ReportId,Measure_Id,AdditionalDetails)
	SELECT 
		EMPI
		,[Service Date]
		,[Billing Provider]
		,[Billing Provider NPI]
		,[Billing Provider TIN]
		,root_companies_id
		,ReportId
		,Measure_id
		,CGWEVI.AdditionalData
	FROM #CDCGapMemberswithEyeCareVisit CGWEV
	CROSS APPLY
	(
		SELECT TOP 1 
			MEMBER_NAME AS "Memberdetail.MemberName"
			,MEM_GENDER AS "Memberdetail.Gender"
			,mem_dob AS "Memberdetail.DOB"
			,[Attending Provider NPI] AS "VisitDetail.AttnProvNPI"
			,[Attending Provider] AS "VisitDetail.AttnProvName"
			,[Attending Provider Specialty] AS "VisitDetail.AttnProvSpec"
			,PAYER AS "VisitDetail.PayerBilled"
		FROM #CDCGapMemberswithEyeCareVisit CGWEVI
		WHERE CGWEVI.EMPI = CGWEV.EMPI
		FOR JSON PATH, ROOT('CDC4EyeExam')
	) CGWEVI(AdditionalData)	
	WHERE 1=1
		AND CGWEV.root_companies_id = @rootId AND CGWEV.ReportId = @reportId AND CGWEV.Measure_Id = @measureId



END
GO
