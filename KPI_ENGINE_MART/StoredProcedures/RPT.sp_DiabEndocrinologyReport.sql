SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE   PROC [RPT].[sp_DiabEndocrinologyReport]
AS
BEGIN

/****** Object:  StoredProcedure [RPT].[sp_DiabEndocrinologyReport]    Script Date: 01-10-2021 ******/
/****** Author: Sameer Gawde ******/
/****** Purpose: This Stored Procedure gets the members with Diabetes from the Registry table and checks if those patients have encounter with Providers having Endocrinology Taxanomy code. Further log this information in KPI_ENGINE_MART.RPT.GapSupportingReport table ******/

--Declaring and initializing all the required variables
	DECLARE
		@rootid int = 159
		,@rundate date = getdate()
		,@startDate date
		,@enddate date
		,@quarter varchar(5)
		,@reportid int
		,@measureid varchar(5) = '4'

--Calling the Stored Procedure to get details of Latest ReportID, Start Date, End Date and Quarter details
	EXEC KPI_ENGINE.DBO.GETREPORTDETAIL @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

--Deleting existing records from Gap Details table for AWV measure
	DELETE KPI_ENGINE_MART.RPT.GapSupportingReport
	WHERE 1=1
		AND ROOT_COMPANIES_ID = @rootid AND ReportId = @reportid AND Measure_Id = @measureid

--Populating Temp table with All Diabetic Patients from registry and their Endocrinologist visit details
	DROP TABLE IF EXISTS #DiabEndocrinologist
	select 
		diab.EMPI
		,CL.ATT_NPI
		,NPI.Provider_First_Name
		,NPI.Provider_Last_Name
		,PR.PROC_START_DATE
		,PR.PROC_CODE
		,CL.CL_DATA_SRC
		,ROW_NUMBER() OVER (PARTITION BY DIAB.EMPI ORDER BY PR.PROC_START_DATE DESC) RN
	INTO
		#DiabEndocrinologist
	FROM 
	(
		SELECT DISTINCT EMPI FROM KPI_ENGINe.REG.REGISTRY_OUTPUT
		WHERE 1=1
			AND registryname = 'Diabetes'
			AND ReportId = @reportid
			AND ROOT_COMPANIES_ID = @rootid
	) DIAB
	JOIN KPI_ENGINE.DBO.PROCEDURES PR ON DIAB.EMPI = PR.EMPI
	JOIN KPI_ENGINE.DBO.CLAIMLINE CL ON CL.EMPI = PR.EMPI AND CL.CLAIM_ID = PR.CLAIM_ID AND CL.SV_LINE = PR.SV_LINE
	JOIN 
	(
		SELECT * FROM KPI_ENGINE..RFT_NPI
		WHERE 1=1
			AND
			(
				Healthcare_Provider_Taxonomy_Code_1 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_2 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_3 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_4 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_5 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_6 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_7 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_8 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_9 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_10 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_11 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_12 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_13 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_14 = '207RE0101X'
				OR Healthcare_Provider_Taxonomy_Code_15 = '207RE0101X'
			)
	) NPI ON CL.ATT_NPI = NPI.NPI
	WHERE 1=1
		AND PR.PROC_START_DATE BETWEEN '2020-01-01' AND '2021-12-31'
		AND CL.BILL_PROV <> '1538164090'

--Populating all the records related to Endocrinology in the KPI_ENGINE_MART.RPT.GapSupportingReport table from temp table
	INSERT INTO KPI_ENGINE_MART.RPT.GapSupportingReport (ROOT_COMPANIES_ID, ReportId, Measure_Id, EMPI, BillingProviderNPI, BillingProvider, ServiceDate, Code, AdditionalDetails)
	SELECT 
		@rootid
		,@reportid
		,@measureid
		,DIAB.EMPI
		,DIABE.ATT_NPI AS PROVIDER_NPI
		,ISNULL(DIABE.Provider_First_Name,'')+' '+ISNULL(DIABE.Provider_Last_Name,'') AS PROVIDER_NAME
		,DIABE.PROC_START_DATE AS SERVICE_DATE
		,DIABE.PROC_CODE AS CODE
		,DIABE2.AdditionalData
	FROM
		(
			SELECT DISTINCT 
				REG.EMPI
			FROM KPI_ENGINe.REG.REGISTRY_OUTPUT REG
			WHERE 1=1
				AND registryname = 'Diabetes'
				AND ReportId = @reportid
				AND ROOT_COMPANIES_ID = @rootid
		) DIAB
		LEFT JOIN #DiabEndocrinologist DIABE ON DIAB.EMPI = DIABE.EMPI AND DIABE.RN = 1
		CROSS APPLY
		(
			SELECT TOP 1 ISNULL(DIABEA.PROC_START_DATE,'') AS "ServiceDate", ISNULL(DIABEA.PROC_CODE,'') AS "Code", ISNULL(DIABEA.ATT_NPI,'') AS "ProviderNPI", ISNULL(DIABEA.Provider_First_Name+' '+DIABEA.Provider_Last_Name,'') AS "ProviderName",ISNULL(DIABEA.CL_DATA_SRC,'') AS "Payer"
			FROM #DiabEndocrinologist AS DIABEA
			WHERE DIABE.EMPI = DIABEA.EMPI
			FOR JSON PATH, ROOT('Endocrinology')
		) DIABE2(AdditionalData)

END
GO
