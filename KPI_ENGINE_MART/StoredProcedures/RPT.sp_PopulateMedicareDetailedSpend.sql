SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE   PROCEDURE [RPT].[sp_PopulateMedicareDetailedSpend]
AS
BEGIN
	
	CREATE TABLE #MedicareDetailedSpend
	(
		[PAYER] [varchar](20) NULL,
		[MEDICARE_PART] [varchar](20) NULL,
		[EMPI] [uniqueidentifier] NULL,
		[PAT_NAME] [varchar](100) NULL,
		[PAT_GENDER] [varchar](10) NULL,
		[PAT_DOB] [date] NULL,
		[CLAIM_ID] [varchar](100) NULL,
		[SV_LINE] [int] NULL,
		[FROM_DATE] [date] NULL,
		[PAID_DATE] [date] NULL,
		[SPEND] [decimal](20, 2) NULL,
		[ATT_PR_NPI] [varchar](20) NULL,
		[ATT_PR_NAME] [varchar](100) NULL,
		[BILL_PROV_NPI] [varchar](20) NULL,
		[BILL_PR_NAME] [varchar](100) NULL,
		[ATTRPracticeName] [varchar](100) NULL,
		[AttributedProviderName] [varchar](100) NULL,
		[AttributedProviderSpecialty] [varchar](100) NULL,
		[AttributedNPI] [varchar](20) NULL,
		[SPEND_MNTH] [varchar](7) NULL,
		[SPEND_YR] [varchar](4) NULL,
		[AMT_FLAG] [varchar](2) NULL
	) 

	INSERT INTO #MedicareDetailedSpend
	(
		PAYER, MEDICARE_PART, ATT_PR_NPI, BILL_PROV_NPI, EMPI, CLAIM_ID, SV_LINE, FROM_DATE, PAID_DATE, SPEND_MNTH, SPEND_YR, SPEND, AMT_FLAG
		, PAT_NAME, PAT_GENDER, PAT_DOB, ATT_PR_NAME, BILL_PR_NAME, ATTRPracticeName, AttributedNPI, AttributedProviderName, AttributedProviderSpecialty
	)
	SELECT 
		CL_DATA_SRC
		,CASE
			WHEN _Claim_UDF_10_ = 'PRTA' THEN 'PART-A'
			WHEN _Claim_UDF_10_ = 'PRTB' THEN 'PART-B'
			WHEN _Claim_UDF_10_ = 'PRTD' THEN 'PART-D'
			WHEN _Claim_UDF_10_ = 'PART-B DME' THEN 'PART-B'
			WHEN _Claim_UDF_10_ = 'PART-B PHYSICIAN' THEN 'PART-B'
			ELSE _Claim_UDF_10_
		END AS MEDICARE_PART
		,CL.ATT_NPI
		,CL.BILL_PROV
		,CL.EMPI
		,CL.CLAIM_ID
		,SV_LINE
		,CL.FROM_DATE
		,CL.PAID_DATE
		,SUBSTRING(CAST(CL.PAID_DATE AS VARCHAR(15)),1,7)
		,CAST(DATEPART(YEAR,CL.PAID_DATE) AS VARCHAR(5))
		,SUM(CL.AMT_PAID) AS SPEND
		,CASE
			WHEN CL.AMT_PAID = -(CH.AMT_PAID) AND CL.AMT_PAID <> 0 AND CH.SV_STAT = 'R' THEN '-H'
			WHEN CL.AMT_PAID <> CH.AMT_PAID AND CH.SV_STAT <> 'R' THEN 'L'
			WHEN CL.AMT_PAID = CH.AMT_PAID THEN 'H'
			ELSE 'L'
		END AS AMT_FLAG
		,M.MEM_FNAME+' '+M.MEM_LNAME
		,M.MEM_GENDER
		,M.MEM_DOB
		,PRA.Provider_First_Name+' '+PRA.Provider_Last_Name
		,PRB.Provider_First_Name+' '+PRB.Provider_Last_Name
		,A.AmbulatoryPCPPractice
		,A.AmbulatoryPCPNPI
		,A.AmbulatoryPCPName
		,A.AmbulatoryPCPSpecialty
	FROM KPI_ENGINE.DBO.CLAIMLINE CL
		LEFT JOIN [KPI_ENGINE].dbo.CLAIMHEADER CH ON CL.CLAIM_ID = CH.CLAIM_ID
		JOIN KPI_ENGINE.RPT.PCP_ATTRIBUTION A ON CL.EMPI=A.EMPI and A.ReportId IN (select max(ReportId) from KPI_ENGINE.RPT.PCP_ATTRIBUTION A)
		JOIN KPI_ENGINE.DBO.RFT_NPI PRA ON PRA.NPI = CL.ATT_NPI
		JOIN KPI_ENGINE.DBO.RFT_NPI PRB ON PRB.NPI = CL.BILL_PROV
		JOIN KPI_ENGINE.DBO.MEMBER M ON M.EMPI = CL.EMPI
	WHERE CL_DATA_SRC IN ('HUMANA','MSSP')
	AND CL.PAID_DATE >= '2019-01-01'
	GROUP BY CL_DATA_SRC, _Claim_UDF_10_, CL.ATT_NPI, CL.BILL_PROV, CL.EMPI, CL.CLAIM_ID, CH.SV_STAT, SV_LINE, CL.FROM_DATE, CL.TO_DATE, CL.PAID_DATE, CL.AMT_PAID, CH.AMT_PAID,M.MEM_FNAME, M.MEM_LNAME, M.MEM_GENDER, M.MEM_DOB, PRA.Provider_First_Name, PRA.Provider_Last_Name, PRB.Provider_First_Name, PRB.Provider_Last_Name, A.AmbulatoryPCPPractice, A.AmbulatoryPCPNPI, A.AmbulatoryPCPName, A.AmbulatoryPCPSpecialty

--**Header Claims
	DROP TABLE IF EXISTS #HEADERCLAIMS

	SELECT DISTINCT
		PAYER
		,MEDICARE_PART
		,EMPI
		,CLAIM_ID
		,PAID_DATE
		,SPEND_YR
		,SPEND_MNTH
		,SPEND
		,AMT_FLAG
	INTO #HEADERCLAIMS
	FROM #MedicareDetailedSpend
	WHERE AMT_FLAG = 'H'

	DELETE #MedicareDetailedSpend
	WHERE AMT_FLAG = 'H'

	INSERT INTO #MedicareDetailedSpend
	(PAYER,MEDICARE_PART,EMPI,CLAIM_ID,PAID_DATE,SPEND_YR,SPEND_MNTH,SPEND,AMT_FLAG)
	SELECT * FROM #HEADERCLAIMS

--**Reverse Header Claims
	DROP TABLE IF EXISTS #RHEADERCLAIMS

	SELECT DISTINCT
		PAYER
		,MEDICARE_PART
		,EMPI
		,CLAIM_ID
		,PAID_DATE
		,SPEND_YR
		,SPEND_MNTH
		,SPEND
		,AMT_FLAG
	INTO #RHEADERCLAIMS
	FROM #MedicareDetailedSpend
	WHERE AMT_FLAG = '-H'

	DELETE #MedicareDetailedSpend
	WHERE AMT_FLAG = '-H'

	INSERT INTO #MedicareDetailedSpend
	(PAYER,MEDICARE_PART,EMPI,CLAIM_ID,PAID_DATE,SPEND_YR,SPEND_MNTH,SPEND,AMT_FLAG)
	SELECT * FROM #RHEADERCLAIMS

	TRUNCATE TABLE KPI_ENGINE_MART.RPT.MedicareDetailedSpend

	INSERT INTO KPI_ENGINE_MART.RPT.MedicareDetailedSpend
	SELECT * FROM #MedicareDetailedSpend

END
GO
