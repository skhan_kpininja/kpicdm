SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
create procedure sp_INCOMING_FILES_DEFINITION
 (
@Payer_Name varchar(100),
@File_Type varchar(100),
@Eligibilty varchar(100),
@Claims varchar(100), 
@Provider varchar(100),
@Naming_Pattern   varchar(100), 
@SSIS_SETUP varchar(100),
@Target_Table varchar(100) ,
@ACTIVE bit
)
as
begin 

set nocount on;

insert into DBO.INCOMING_FILES_DEFINITION(Payer_Name,File_Type,Eligibilty,Claims,
 Provider,Naming_Pattern,SSIS_SETUP,Target_Table,ACTIVE)

 values(@Payer_Name,
@File_Type,
@Eligibilty ,
@Claims , 
@Provider ,
@Naming_Pattern , 
@SSIS_SETUP,
@Target_Table ,
@ACTIVE )

 End
GO
