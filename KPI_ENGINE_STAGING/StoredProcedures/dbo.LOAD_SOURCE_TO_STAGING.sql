SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[LOAD_SOURCE_TO_STAGING]
AS
BEGIN

	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

/*Shanawaz 10-25-2021 : Calling [dbo].[usp_GetSourceIngestionStats] to update the KPI_ENGINE_RECON.EVN.Source_Ingstion_Stats table with counts for logging.*/

	Exec KPI_ENGINE_RECON.[dbo].[usp_GetSourceIngestionStats]



DECLARE @SRC_TO_STG_PROCS varchar(500)
DECLARE @ROOT_COMPANIES_ID int
DECLARE @SQL VARCHAR(500)




DECLARE C CURSOR
FOR
SELECT SRC_TO_STG_PROCS,ROOT_COMPANIES_ID
FROM KPI_ENGINE_STAGING.DBO.SOURCE_TO_STAGING
WHERE ACTIVE=1 and Execution_STATUS=0 AND ISNULL(SRC_TO_STG_PROCS,'')<>''
ORDER BY P_ID


OPEN C
FETCH NEXT FROM C INTO @SRC_TO_STG_PROCS,@ROOT_COMPANIES_ID

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SQL='EXEC '+@SRC_TO_STG_PROCS

EXEC (@SQL)
--PRINT (@SQL)
--PRINT @SRC_TO_STG_PROCS
--PRINT @ROOT_COMPANIES_ID

FETCH NEXT FROM C INTO @SRC_TO_STG_PROCS,@ROOT_COMPANIES_ID
END

CLOSE C
DEALLOCATE C


/*Shanawaz 10-22-2021 : Calling [dbo].[usp_GetStagingIngestionStats] to update the KPI_ENGINE_RECON.EVN.Staging_Ingstion_Stats table with counts for logging.*/

	Exec KPI_ENGINE_RECON.[dbo].[usp_GetStagingIngestionStats]


-- Exec [KPI_ENGINE_RECON].[dbo].[IngestionTracker];
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;


END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  
END
GO
