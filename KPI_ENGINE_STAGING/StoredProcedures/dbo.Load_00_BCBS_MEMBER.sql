SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_00_BCBS_MEMBER]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

	-- Update FileDate in BCBS_Eligibility
	update KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY set FileDate=convert(date,substring(Filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',Filename),8),103) where FileDate is null

	-- BCBS_Eligibility to STAGING_MEMBER

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEMBER(MEMBER_ID,MEMBER_QUAL,MEM_GENDER,MEM_SSN,MEM_LNAME,MEM_FNAME,MEM_MNAME,MEM_DOB,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_COUNTY,MEM_STATE,MEM_ZIP,MEM_PHONE,MEM_RACE,MEM_LANGUAGE,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	Select MEMBER_ID,MEMBER_QUAL,MEM_GENDER,MEM_SSN,MEM_LNAME,MEM_FNAME,MEM_MNAME,MEM_DOB,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_COUNTY,MEM_STATE,MEM_ZIP,MEM_PHONE,MEM_RACE,MEM_LANGUAGE,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			dense_rank() over(partition by concat(O_MBR_NBR,O_MBR_SFX) order by FileDate desc,RecordNo Desc) as rnk,
			concat(O_MBR_NBR,O_MBR_SFX) as MEMBER_ID
			,O_MBR_SFX as MEMBER_QUAL
			,O_GENDER as MEM_GENDER
			,O_MBR_SSN_NO as MEM_SSN
			,O_LAST_NAME as MEM_LNAME
			,O_FIRST_NAME as MEM_FNAME
			,O_MID_NAME as MEM_MNAME
			,convert(varchar,cast(O_DOB as date),23) as MEM_DOB
			,O_ADDR_1 as MEM_ADDR1
			,O_ADDR_2 as MEM_ADDR2
			,O_CITY as MEM_CITY
			,O_COUNTY as MEM_COUNTY
			,O_STATE as MEM_STATE
			,Substring(cast(O_ZIP as varchar),1,5) as MEM_ZIP
			,O_HOME_PHONE as MEM_PHONE
			,O_RACE as MEM_RACE
			,O_LANGUAGE_IND as MEM_LANGUAGE
			,1 as _MEM_UDF_03_
			,'BCBS-UHS' as MEM_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY
	)t1 where rnk=1

	-- BCBS_ELIGIBILITY to STAGING_ENROLLMENT

	
Insert into KPI_ENGINE_STAGING.dbo.STAGING_ENROLLMENT(MEMBER_ID,MEMBER_QUAL,EFF_DATE,TERM_DATE,SUBSCRIBER_ID,SUBSCRIBER_QUAL,RELATION,GRP_ID,PCP_PROV,CONTRACT,BEN_PKG_ID,PROD_TYPE,MM_UNITS,RX_UNITS,DRUG_BENEFIT,ENR_PRIMARY,PAYER_TYPE,EN_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	Select MEMBER_ID,MEMBER_QUAL,EFF_DATE,TERM_DATE,SUBSCRIBER_ID,SUBSCRIBER_QUAL,RELATION,GRP_ID,PCP_PROV,CONTRACT,BEN_PKG_ID,PROD_TYPE,MM_UNITS,RX_UNITS,DRUG_BENEFIT,ENR_PRIMARY,PAYER_TYPE,EN_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			dense_rank() over(partition by concat(O_MBR_NBR,O_MBR_SFX),O_CURR_ELIG_BEGIN_DT,O_CURR_ELIG_END_DT,O_BEN_PLAN order by FileDate desc,RecordNo Desc) as rnk,
			concat(O_MBR_NBR,O_MBR_SFX) as MEMBER_ID
			,O_MBR_SFX as MEMBER_QUAL
			,convert(varchar,cast(O_CURR_ELIG_BEGIN_DT as date),23) as EFF_DATE
			,convert(varchar,cast(O_CURR_ELIG_END_DT as date),23) as TERM_DATE
			,O_MBR_NBR as SUBSCRIBER_ID
			,O_MBR_SFX as SUBSCRIBER_QUAL
			,Case
				when TRIM(O_SBR_REL)='SUBSCRIBER' Then 18
				when TRIM(O_SBR_REL)='SON' Then 2
				when TRIM(O_SBR_REL)='DAUGHTER' Then 2
				when TRIM(O_SBR_REL)='HUSBAND' Then 1
				when TRIM(O_SBR_REL)='WIFE' Then 1
			end as RELATION
			,O_GROUP_NO as GRP_ID
			,O_PCP as PCP_PROV
			,O_CONTRACT_TYPE as CONTRACT
			,O_BEN_PLAN as BEN_PKG_ID
			,'MEDICAL' as PROD_TYPE
			,1 as MM_UNITS
			,1 as RX_UNITS
			,1 as DRUG_BENEFIT
			,Case 
				when O_COB='S' Then 1
				else 0
			end as ENR_PRIMARY
			,'EPO' as PAYER_TYPE
			,'BCBS-UHS' as EN_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY
	)t1 where rnk=1

	-- Move Source table BCBS_ELIGIBILITY data to BCBS_ELIGIBILITY_ARCHIVE

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY_ARCHIVE ON;  

	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[BCBS_ELIGIBILITY_ARCHIVE]
           ([O_MBR_NBR]
           ,[O_MBR_SFX]
           ,[O_MBR_SSN_NO]
           ,[O_GENDER]
           ,[O_RACE]
           ,[O_FIRST_NAME]
           ,[O_MID_NAME]
           ,[O_LAST_NAME]
           ,[O_ADDR_1]
           ,[O_ADDR_2]
           ,[O_CITY]
           ,[O_STATE]
           ,[O_ZIP]
           ,[O_COUNTY]
           ,[O_HOME_PHONE]
           ,[O_WORK_PHONE]
           ,[O_DOB]
           ,[O_SBR_REL]
           ,[O_CURR_ELIG_BEGIN_DT]
           ,[O_CURR_ELIG_END_DT]
           ,[O_PREV_ELIG_BEGIN_DT]
           ,[O_PREV_ELIG_END_DT]
           ,[O_PLAN_TYPE_W]
           ,[O_LOB]
           ,[O_COB]
           ,[O_CONTRACT_TYPE]
           ,[O_SBR_EMPL_DEPT_NO]
           ,[O_BEN_PLAN]
           ,[O_TIER]
           ,[O_REGION]
           ,[O_PCP]
           ,[O_EMPLOYER]
           ,[O_EMPLOYER_GROUP_ID]
           ,[O_SBR_EMP_NO]
           ,[O_LANGUAGE_IND]
           ,[O_GROUP_NO]
           ,[O_DETAIL_REC_TYPE]
           ,[FileName]
           ,[FileDate]
           ,[LoadDateTime]
		    ,[RecordNo])
   SELECT [O_MBR_NBR]
      ,[O_MBR_SFX]
      ,[O_MBR_SSN_NO]
      ,[O_GENDER]
      ,[O_RACE]
      ,[O_FIRST_NAME]
      ,[O_MID_NAME]
      ,[O_LAST_NAME]
      ,[O_ADDR_1]
      ,[O_ADDR_2]
      ,[O_CITY]
      ,[O_STATE]
      ,[O_ZIP]
      ,[O_COUNTY]
      ,[O_HOME_PHONE]
      ,[O_WORK_PHONE]
      ,[O_DOB]
      ,[O_SBR_REL]
      ,[O_CURR_ELIG_BEGIN_DT]
      ,[O_CURR_ELIG_END_DT]
      ,[O_PREV_ELIG_BEGIN_DT]
      ,[O_PREV_ELIG_END_DT]
      ,[O_PLAN_TYPE_W]
      ,[O_LOB]
      ,[O_COB]
      ,[O_CONTRACT_TYPE]
      ,[O_SBR_EMPL_DEPT_NO]
      ,[O_BEN_PLAN]
      ,[O_TIER]
      ,[O_REGION]
      ,[O_PCP]
      ,[O_EMPLOYER]
      ,[O_EMPLOYER_GROUP_ID]
      ,[O_SBR_EMP_NO]
      ,[O_LANGUAGE_IND]
      ,[O_GROUP_NO]
      ,[O_DETAIL_REC_TYPE]
      ,[FileName]
      ,[FileDate]
      ,[LoadDateTime]
      ,[RecordNo]
  FROM [KPI_ENGINE_SOURCE].[UHN].[BCBS_ELIGIBILITY]

  SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY_ARCHIVE OFF;
   -- Delete data from BCBS_ELIGIBILITY

	delete from KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY

	  

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
