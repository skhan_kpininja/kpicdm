SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE PROC [dbo].[Load_00_BRIGHT_MEMBER]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

	-- Update ELigibility.FileDate column based on Filename

	Update KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES set FileDate=Case when ISDATE(substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8))=1 Then convert(varchar,Cast(substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8) as date),23) else convert(varchar,Cast(concat(substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',filename),6),'01') as date),23) end where filedate is null

	-- BRIGHT_ELIGIBILITY_FILES to STAGING_MEMBER

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEMBER(MEMBER_ID,MEM_FNAME,MEM_MNAME,MEM_LNAME,MEM_GENDER,MEM_DOB,MEM_DOD,MEM_EMAIL,MEM_PHONE,MEM_LANGUAGE,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_STATE,MEM_ZIP,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,filename)
	select MEMBER_ID,MEM_FNAME,MEM_MNAME,MEM_LNAME,MEM_GENDER,MEM_DOB,MEM_DOD,MEM_EMAIL,MEM_PHONE,MEM_LANGUAGE,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_STATE,MEM_ZIP,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,filename 
	from(
		select
			 Dense_rank() over(partition by BRIGHT_MEMBER_ID order by FileDate desc,RecordNo Desc) as rnk,
			 BRIGHT_MEMBER_ID as MEMBER_ID
			,FIRST_NAME as MEM_FNAME
			,MIDDLE_NAME as MEM_MNAME
			,LAST_NAME as MEM_LNAME
			,Case	
				When GENDER ='Male' Then 'M'
				When GENDER ='Female' Then 'F'
				Else 'U'
			end as MEM_GENDER
			,convert(varchar,cast(DOB as Date),23) as MEM_DOB
			,ISNULL(NULLIF(convert(varchar,cast(DOD as Date),23),'1900-01-01'),'') as MEM_DOD
			,EMAIL_ADDRESS as MEM_EMAIL
			,ISNULL(PHONE_NUMBER,'') as MEM_PHONE
			,Case 
				when LANGUAGE_SPOKEN ='English' Then 1
				when LANGUAGE_SPOKEN ='Spanish' Then 2
				else 9
			end as MEM_LANGUAGE
			,MAILING_ADDR_LINE_1 as MEM_ADDR1
			,MAILING_ADDR_LINE_2 as MEM_ADDR2
			,MAILING_CITY as MEM_CITY
			,MAILING_STATE_CD as MEM_STATE
			,MAILING_ZIP as MEM_ZIP
			,1 as _MEM_UDF_03_
			,'BRIGHT' as MEM_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,filename
		from KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES
	)t1 where rnk=1



	
	-- Move Data to enrollment

	
	-- -- BRIGHT_ELIGIBILITY_FILES to STAGING_ENROLLMENT

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_ENROLLMENT(MEMBER_ID,EFF_DATE,TERM_DATE,SUBSCRIBER_ID,PCP_PROV,PROD_TYPE,MM_UNITS,RX_UNITS,DRUG_BENEFIT,EN_DATA_SRC,PAYER_TYPE,ROOT_COMPANIES_ID,FileDate,filename)
	select MEMBER_ID,EFF_DATE,TERM_DATE,SUBSCRIBER_ID,PCP_PROV,PROD_TYPE,MM_UNITS,RX_UNITS,DRUG_BENEFIT,EN_DATA_SRC,PAYER_TYPE,ROOT_COMPANIES_ID,FileDate,filename from(
		select
			 Dense_rank() over(partition by BRIGHT_MEMBER_ID,EFF_START_DT,EFF_END_DT order by FileDate desc,RecordNo Desc) as rnk,
			 BRIGHT_MEMBER_ID as MEMBER_ID
			,convert(varchar,cast(EFF_START_DT as date),23) as EFF_DATE
			,convert(varchar,cast(EFF_END_DT as date),23) as TERM_DATE
			,BRIGHT_MEMBER_ID as SUBSCRIBER_ID
			,ISNULL(PCP_NPI,'') as PCP_PROV
			,'Medical' as PROD_TYPE
			,1 as MM_UNITS 
			,1 as RX_UNITS
			,1 as DRUG_BENEFIT
			,'BRIGHT' as EN_DATA_SRC
			,'PPO' as PAYER_TYPE
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,filename
		from KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES
	)t1 where rnk=1


	

	-- Move Source table data to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES_ARCHIVE ON;  

	INSERT INTO [KPI_ENGINE_SOURCE].UHN.[BRIGHT_ELIGIBILITY_FILES_ARCHIVE]
           ([RECORD_ID]
           ,[BRIGHT_MEMBER_ID]
           ,[FIRST_NAME]
           ,[MIDDLE_NAME]
           ,[LAST_NAME]
           ,[GENDER]
           ,[DOB]
           ,[DOD]
           ,[EMAIL_ADDRESS]
           ,[PHONE_NUMBER]
           ,[LANGUAGE_SPOKEN]
           ,[LANGUAGE_WRITTEN]
           ,[LOB]
           ,[MARKET]
           ,[EFF_START_DT]
           ,[EFF_END_DT]
           ,[PHYSICAL_ADDR_LINE_1]
           ,[PHYSICAL_ADDR_LINE_2]
           ,[PHYSICAL_CITY]
           ,[PHYSICAL_STATE_CD]
           ,[PHYSICAL_ZIP]
           ,[PHYSICAL_ZIP_PLUS_4]
           ,[MAILING_ADDR_LINE_1]
           ,[MAILING_ADDR_LINE_2]
           ,[MAILING_CITY]
           ,[MAILING_STATE_CD]
           ,[MAILING_ZIP]
           ,[MAILING_ZIP_PLUS_4]
           ,[PCP_NPI]
           ,[PCP_FIRST_NAME]
           ,[PCP_LAST_NAME]
           ,[PCP_ADDR_LINE_1]
           ,[PCP_ADDR_LINE_2]
           ,[PCP_CITY]
           ,[PCP_STATE_CD]
           ,[PCP_ZIP]
           ,[PCP_ZIP_PLUS_4]
           ,[LoadDateTime]
           ,[filename]
           ,[FileDate]
		   ,RecordNo)
		select 
			[RECORD_ID]
           ,[BRIGHT_MEMBER_ID]
           ,[FIRST_NAME]
           ,[MIDDLE_NAME]
           ,[LAST_NAME]
           ,[GENDER]
           ,[DOB]
           ,[DOD]
           ,[EMAIL_ADDRESS]
           ,[PHONE_NUMBER]
           ,[LANGUAGE_SPOKEN]
           ,[LANGUAGE_WRITTEN]
           ,[LOB]
           ,[MARKET]
           ,[EFF_START_DT]
           ,[EFF_END_DT]
           ,[PHYSICAL_ADDR_LINE_1]
           ,[PHYSICAL_ADDR_LINE_2]
           ,[PHYSICAL_CITY]
           ,[PHYSICAL_STATE_CD]
           ,[PHYSICAL_ZIP]
           ,[PHYSICAL_ZIP_PLUS_4]
           ,[MAILING_ADDR_LINE_1]
           ,[MAILING_ADDR_LINE_2]
           ,[MAILING_CITY]
           ,[MAILING_STATE_CD]
           ,[MAILING_ZIP]
           ,[MAILING_ZIP_PLUS_4]
           ,[PCP_NPI]
           ,[PCP_FIRST_NAME]
           ,[PCP_LAST_NAME]
           ,[PCP_ADDR_LINE_1]
           ,[PCP_ADDR_LINE_2]
           ,[PCP_CITY]
           ,[PCP_STATE_CD]
           ,[PCP_ZIP]
           ,[PCP_ZIP_PLUS_4]
           ,[LoadDateTime]
           ,[filename]
           ,[FileDate]
		   ,RecordNo
		from KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES

	-- Delete data from BRIGHT_ELIGIBILITY_FILES

	delete from KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES;


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
