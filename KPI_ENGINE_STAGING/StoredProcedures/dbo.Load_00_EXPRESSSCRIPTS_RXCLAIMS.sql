SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE PROC [dbo].[Load_00_EXPRESSSCRIPTS_RXCLAIMS]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Delete Trailer Records and header records 

delete from KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS where PatientIDCarrier is null and FileDate = '2021-01-01'

-- Move data to Member Table


Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEMBER(MEMBER_ID,MEM_GENDER,MEM_LNAME,MEM_FNAME,MEM_DOB,MEM_DATA_SRC,_MEM_UDF_02_,_MEM_UDF_03_,ROOT_COMPANIES_ID,FileName,FileDate)
Select MEMBER_ID,MEM_GENDER,MEM_LNAME,MEM_FNAME,MEM_DOB,MEM_DATA_SRC,_MEM_UDF_02_,_MEM_UDF_03_,ROOT_COMPANIES_ID,FileName,FileDate  from(
select 
	row_number() over(partition by TRIM(PatientIDCarrier) order by FileDate Desc) as rnk,
	TRIM(PatientIDCarrier) as MEMBER_ID
	,Case
		When PatientGender='Male' Then 'M'
		When PatientGender='Female' Then 'F'
		else 'U'
	end as MEM_GENDER
	,TRIM(PatientLastName) as MEM_LNAME
	,TRIM(PatientFirstName) as MEM_FNAME
	,convert(varchar,cast(PatientDateofBirth as Date),23) as MEM_DOB
	,'EXPRESSSCRIPTS' as MEM_DATA_SRC
	,TRIM(PatientIDClient) as _MEM_UDF_02_
	,'1' as _MEM_UDF_03_
	,159 as ROOT_COMPANIES_ID
	,TRIM(filename) as FileName
	,FileDate
from KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS
where PatientIDCarrier is not null and FileDate = '2021-01-01'
)t1 where rnk=1 


-- Move Data to ClaimLine


Insert into KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE(CLAIM_ID,SV_LINE,FORM_TYPE,SV_STAT,FROM_DATE,TO_DATE,MEMBER_ID,ATT_PROV,ATT_PROV_SPEC,BILL_PROV,RX_INGR_COST,AMT_BILLED,AMT_COPAY,CL_DATA_SRC,ATT_PROV_NAME,ATT_NPI,BILL_PROV_NAME,Filename,Filedate,ROOT_COMPANIES_ID)
Select CLAIM_ID,SV_LINE,FORM_TYPE,SV_STAT,FROM_DATE,TO_DATE,MEMBER_ID,ATT_PROV,ATT_PROV_SPEC,BILL_PROV,RX_INGR_COST,AMT_BILLED,AMT_COPAY,CL_DATA_SRC,ATT_PROV_NAME,ATT_NPI,BILL_PROV_NAME,Filename,Filedate,ROOT_COMPANIES_ID from(
Select 
	Dense_rank() over(partition by PatientIDCarrier,RxNumber,NDC11,DateServiced,s.RecordNo order by s.FileDate Desc,s.RecordNo Desc) as rnk,
	concat(RxNumber,NDC11,convert(varchar(8),cast(PatientDateofBirth as date),112)) as CLAIM_ID
	,Case 
        When s.RecordNo is null Then '1'
        else s.RecordNo 
		end as SV_LINE
	,'D' as FORM_TYPE
	,Case 
	    When cast(Copay as decimal(18,2)) <0 Then 'R'
		else 'P'
		end as SV_STAT
	,convert(varchar,cast(DateServiced as Date),23) as FROM_DATE
	,convert(varchar,cast(DateServiced as Date),23) as TO_DATE
	,TRIM(PatientIDCarrier) as MEMBER_ID
	,TRIM(PrescriberNPINumber) as ATT_PROV
	,p.PrimarySpecialty as ATT_PROV_SPEC
	,PharmacyNPINumber as BILL_PROV
	,cast(IngredientCost as decimal(18,2)) as RX_INGR_COST
	,cast(PlanCost as decimal(18,2)) as AMT_BILLED
	,cast(Copay as decimal(18,2)) as AMT_COPAY
	,'EXPRESSSCRIPTS' as CL_DATA_SRC
	,concat(coalesce(nullif(p.Provider_Last_Name,s.PrescriberLastName),s.PrescriberLastName),' ',coalesce(nullif(p.Provider_First_Name,s.PrescriberFirstName),s.PrescriberFirstName)) as ATT_PROV_NAME
	,PrescriberNPINumber as ATT_NPI
	,coalesce(p1.Provider_Organization_Name,s.PharmacyName) as BILL_PROV_NAME
	,s.filename as Filename
	,FileDate
	,159 as ROOT_COMPANIES_ID
from KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS s
left outer join KPI_ENGINE.dbo.RFT_NPI p on s.PrescriberNPINumber = p.NPI
left outer join KPI_ENGINE.dbo.RFT_NPI p1 on s.PharmacyNPINumber = p1.NPI
where PatientIDCarrier is not null and FileDate = '2021-01-01'
)t1 where rnk=1



-- Move data to Staging Medication



Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEDICATION(CLAIM_ID,MEMBER_ID,FILL_DATE,MEDICATION_NAME,MEDICATION_CODE,MEDICATION_CODE_TYPE,QUANTITY,MANUFACTURER,MED_TYPE,SUPPLY_DAYS,PRESC_NPI,PRESC_NAME,PHARM_ID,PHARM_NAME,MED_ROUTE,MED_FORM,MED_STRENGTH,ROOT_COMPANIES_ID,FileDate,Filename,THERAPEUTIC_CLASS,MED_DATA_SRC)
select CLAIM_ID,MEMBER_ID,FILL_DATE,MEDICATION_NAME,MEDICATION_CODE,MEDICATION_CODE_TYPE,QUANTITY,MANUFACTURER,MED_TYPE,SUPPLY_DAYS,PRESC_NPI,PRESC_NAME,PHARM_ID,PHARM_NAME,MED_ROUTE,MED_FORM,MED_STRENGTH,ROOT_COMPANIES_ID,FileDate,Filename,THERAPEUTIC_CLASS,MED_DATA_SRC from(
select 
	Dense_rank() over(partition by TRIM(PatientIDCarrier),RxNumber,DateServiced,NDC11,s.RecordNo order by FileDate Desc) as rnk,
	concat(RxNumber,NDC11,convert(varchar(8),cast(PatientDateofBirth as date),112)) as CLAIM_ID
	,TRIM(PatientIDCarrier) as MEMBER_ID
	,convert(varchar,cast(DateServiced as Date),23) as FILL_DATE
	,coalesce(p.GenericName,p.BrandName,s.DrugName) as MEDICATION_NAME
	,TRIM(NDC11) as MEDICATION_CODE
	,case	
		when ClaimDrugQualifier ='Unknown' then 'U'
		when ClaimDrugQualifier ='National Drug Code - NDC' then 'NDC'
	end as MEDICATION_CODE_TYPE
	,Quantity as QUANTITY
	,p.LABELERNAME as MANUFACTURER
	,p.GenericFlag as MED_TYPE
	,DaysofTherapy as SUPPLY_DAYS
	,PrescriberNPINumber as PRESC_NPI
	,concat(coalesce(nullif(p1.Provider_Last_Name,s.PrescriberLastName),s.PrescriberLastName),' ',coalesce(nullif(p1.Provider_First_Name,s.PrescriberFirstName),s.PrescriberFirstName)) as PRESC_NAME
	,TRIM(PharmacyNPINumber) as PHARM_ID
	,coalesce(case when p2.Provider_Organization_Name='' then null else p2.Provider_Organization_Name end,s.PharmacyName) as PHARM_NAME
	,p.RouteName as MED_ROUTE
	,p.DosageForm as MED_FORM
	,DrugStrength as MED_STRENGTH
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,s.filename as Filename
	,p.TherapeuticClass as THERAPEUTIC_CLASS
	,'EXPRESSSCRIPTS' as MED_DATA_SRC
from KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS s
left outer join KPI_ENGINE.rft.ndccodes p on s.NDC11=p.NDC
left outer join KPI_ENGINE.dbo.RFT_NPI p1 on s.PrescriberNPINumber = p1.NPI
left outer join KPI_ENGINE.dbo.RFT_NPI p2 on s.PrescriberNPINumber = p2.NPI
where PatientIDCarrier is not null and FileDate = '2021-01-01'
)t1 where rnk=1



-- Moving data to ARCHIVE





INSERT INTO KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS_ARCHIVE(PatientIDCarrier
      ,PatientIDClient
      ,PatientFirstName
      ,PatientLastName
      ,PatientGender
      ,PatientDateofBirth
      ,DateServiced
      ,RxNumber
      ,NDC11
      ,DrugName
      ,DrugStrength
      ,Quantity
      ,DaysofTherapy
      ,IngredientCost
      ,Copay
      ,PlanCost
      ,PrescriberNPINumber
      ,PrescriberFirstName
      ,PrescriberLastName
      ,PharmacyName
      ,PharmacyNPINumber
      ,ClaimDrugQualifier
      ,filename
      ,FileDate
      ,LOADDATETIME
	  ,RecordNo)
SELECT PatientIDCarrier
      ,PatientIDClient
      ,PatientFirstName
      ,PatientLastName
      ,PatientGender
      ,PatientDateofBirth
      ,DateServiced
      ,RxNumber
      ,NDC11
      ,DrugName
      ,DrugStrength
      ,Quantity
      ,DaysofTherapy
      ,IngredientCost
      ,Copay
      ,PlanCost
      ,PrescriberNPINumber
      ,PrescriberFirstName
      ,PrescriberLastName
      ,PharmacyName
      ,PharmacyNPINumber
      ,ClaimDrugQualifier
      ,filename
      ,FileDate
      ,LOADDATETIME
	  ,RecordNo
FROM KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS where  FileDate = '2021-01-01' and PatientIDCarrier is not null

-- Delete Data from EXPRESSSCRIPTS

Delete from KPI_ENGINE_SOURCE.UHN.EXPRESSSCRIPTS where  FileDate = '2021-01-01';




	  

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END











GO
