SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE PROC [dbo].[Load_00_FLATFILES_EMR]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update File Date

	Update KPI_ENGINE_SOURCE.UHN.FLATFILES_EMR set FileDate=convert(varchar,substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8),112)
	update KPI_ENGINE_SOURCE.UHN.FLATFILES_EMR set FileDate=Concat(SUBSTRING(Filedate,5,4),'-',SUBSTRING(Filedate,1,2),'-',SUBSTRING(Filedate,3,2))

	
-- Move data to Staging Member
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEMBER(MEMBER_ID,_MEM_UDF_02_,MEM_MEDICARE,MEM_LNAME,MEM_FNAME,MEM_DOB,MEM_GENDER,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select MEMBER_ID,_MEM_UDF_02_,MEM_MEDICARE,MEM_LNAME,MEM_FNAME,MEM_DOB,MEM_GENDER,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			DENSE_RANK() over(partition by cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)) order by Filedate desc,RecordNo desc) as rnk,
			--KPI_ENGINE_STAGING.dbo.StringtoINT(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID)) as MEMBER_ID,
			
			cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)) as MEMBER_ID,
			PayerId as _MEM_UDF_02_
			,MBI as MEM_MEDICARE
			,LastName as MEM_LNAME
			,FirstName as MEM_FNAME
			,convert(varchar,DOB,23) as MEM_DOB
			,Case	
				when Gender='Unknown' Then 'U'
				when Gender='Male' Then 'M'
				when Gender='Female' Then 'F'
				else Gender
			end as MEM_GENDER
			,1 as _MEM_UDF_03_
			,'FlatFiles' as MEM_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.FLATFILES_EMR
		--where PayerID='100590063'
	)t1 where  rnk=1


	
-- Insert data into labs

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_LAB(MEMBER_ID,ResultCode,ResultDate,ResultName,Value,Unit,LAB_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select MEMBER_ID,ResultCode,ResultDate,ResultName,Value,Unit,LAB_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			Dense_rank() over(partition by cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)),LabId,ServiceDate order by FileDate Desc,RecordNo Desc) as rnk,
			cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)) as MEMBER_ID
			,case
				when LabID in('390000000.0','71957.0','12777.0','13652.0','3.90302617E8') then '14957-5'
				when LabId in('13968.0','13962.0') then '2887-8'
				when LabId in('13719.0') then '14959-1'
				when LabId in('391000000.0') then '13801-6'
				when LabId in('256000000.0','83523.0','256000000') then '4548-4'
				when LabId in('13966.0') then '2890-2'
				when LabId in('258000000.0','13963.0','2.57636619E8') then '5804-0'
				when LabId in('3.91430571E8','52729.0') then '40486-3'
			end as ResultCode
			,convert(varchar,cast(ServiceDate as Date),23) as ResultDate
			,LabName as ResultName
			,cast(TestResult as nvarchar(50)) as Value
			,ResultUnits as Unit
			,'FlatFiles' as LAB_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.FLATFILES_EMR
		where BPsystolic is NULL and BPdiastolic is NULL
	)t1 where rnk=1

-- Insert data into Vital

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_VITAL(MEMBER_ID,VitalCode,VitalDateTime,VitalName,Value,Unit,VIT_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	Select MEMBER_ID,VitalCode,VitalDateTime,VitalName,Value,Unit,VIT_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			Dense_rank() over(partition by cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)),ServiceDate order by FileDate Desc,RecordNo Desc) as rnk,
			cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)) as MEMBER_ID
			,'8480-6' as VitalCode
			,convert(varchar,cast(ServiceDate as Date),23) as VitalDateTime
			,'BP Systolic' as VitalName
			,cast(BPsystolic as nvarchar(50)) as Value
			,'mmHg' as Unit
			,'FlatFiles' as VIT_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.FLATFILES_EMR
		where BPsystolic is not null
	)t1 where rnk=1

-- Insert BP Diastolic into Vital

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_VITAL(MEMBER_ID,VitalCode,VitalDateTime,VitalName,Value,Unit,VIT_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	Select MEMBER_ID,VitalCode,VitalDateTime,VitalName,Value,Unit,VIT_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			Dense_rank() over(partition by cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)),ServiceDate order by FileDate Desc,RecordNo Desc) as rnk,
			cast(cast(Hashbytes('SHA1',CAST(concat(LastName,FirstName,DOB,Gender,'FlatFiles',PayerID) AS VARBINARY(max))) as uniqueidentifier) as varchar(100)) as MEMBER_ID
			,'8462-4' as VitalCode
			,convert(varchar,cast(ServiceDate as Date),23) as VitalDateTime
			,'BP Diastolic' as VitalName
			,cast(BPDiastolic as nvarchar(50)) as Value
			,'mmHg' as Unit
			,'FlatFiles' as VIT_DATA_SRC
			,159 as  ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.FLATFILES_EMR
		where BPdiastolic is not null
	)t1 where rnk=1


	
	-- Move Source data to Archive from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX to KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX_Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.[FLATFILES_EMR_ARCHIVE] ON;  

	
	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[FLATFILES_EMR_ARCHIVE]
			   ([PayerID]
			   ,[MBI]
			   ,[LastName]
			   ,[FirstName]
			   ,[DOB]
			   ,[Gender]
			   ,[ServiceDate]
			   ,[LabID]
			   ,[LabName]
			   ,[TestResult]
			   ,[ResultUnits]
			   ,[BPsystolic]
			   ,[BPdiastolic]
			   ,[FileDate]
			   ,[FileName]
			   ,[LoadDateTime]
			   ,[RecordNo]
			   ,Payer)
	SELECT [PayerID]
		  ,[MBI]
		  ,[LastName]
		  ,[FirstName]
		  ,[DOB]
		  ,[Gender]
		  ,[ServiceDate]
		  ,[LabID]
		  ,[LabName]
		  ,[TestResult]
		  ,[ResultUnits]
		  ,[BPsystolic]
		  ,[BPdiastolic]
		  ,[FileDate]
		  ,[FileName]
		  ,[LoadDateTime]
		  ,[RecordNo]
		  ,Payer
	  FROM [KPI_ENGINE_SOURCE].[UHN].[FLATFILES_EMR]



	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].UHN.FLATFILES_EMR

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.[FLATFILES_EMR_ARCHIVE] OFF;  

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
