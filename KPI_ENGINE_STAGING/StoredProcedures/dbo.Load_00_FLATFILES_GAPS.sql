SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE PROC [dbo].[Load_00_FLATFILES_GAPS]
AS

BEGIN
--PK:2022-02-18: Added Upsert logic for OptionalExclusions table and commented out A1c insert logic into procedures table
--PK:2022-02-21:Added logic for error handling of rows and made logic changes for merge SOURCE query
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update File Date

	Update KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS set FileDate=convert(varchar,substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8),112)
	update KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS set FileDate=Concat(SUBSTRING(Filedate,5,4),'-',SUBSTRING(Filedate,1,2),'-',SUBSTRING(Filedate,3,2))



-- Insert data into labs


Insert into KPI_ENGINE_STAGING.dbo.STAGING_LAB(MEMBER_ID,ResultDate,ResultCode,ResultName,Value,Unit,LAB_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename)
select MEMBER_ID,ResultDate,ResultCode,ResultName,Value,Unit,LAB_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename from(
select
	DENSE_RANK() over(partition by PayerId,A1cTestDate order by g.FileDate Desc,RecordNo Desc) as rnk
	,Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
	,convert(varchar,cast(A1cTestDate as Date),23) as ResultDate
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='HbA1c Lab Test' and Code_System='LOINC') as ResultCode
	,'Hgb A1c' as ResultName
	,A1cResult as Value
	,'%' as Unit
	,'FlatFiles-Gaps' as LAB_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,g.FileDate
	,g.FileName
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS g
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE 
)c1 on PayerId =MEMBR_AMI  and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna' and m.ROOT_COMPANIES_ID=159
where A1cTestDate is not null
)t1 where rnk=1



-- Insert into Procedures - CPT,HCPCS,CPT-CAT-II
Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(MEMBER_ID,PROC_START_DATE,PROC_CODE,MOD_1,PROC_CODE_TYPE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
select MEMBER_ID,PROC_START_DATE,PROC_CODE,MOD_1,PROC_CODE_TYPE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
Select 
	DENSE_RANK() over(partition by PayerId,PROC_START_DATE,PROC_CODE order by t1.FileDate Desc,t1.RecordNo) as rnk,
	
	Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
	,PROC_START_DATE
	,PROC_CODE
	,MOD_1
	,PROC_CODE_TYPE
	,PROC_DATA_SRC
	,t1.ROOT_COMPANIES_ID
	,t1.FileDate
	,t1.FileName
	,t1.RecordNo
	,t1.Payer
from(
-- ART Medication
select
	PayerId
	,convert(varchar,cast(ARTMedicationDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='DMARD' and Code_System='HCPCS') as PROC_CODE
	,'' as MOD_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ARTMedicationDate is not null


Union all
--PK:2022-02-18: Commenting out the A1c logic based on OptionalExclusion changes
/*-- Does not Have A1c

select
	PayerId
	,convert(varchar,cast(FileDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='HbA1c Level Less Than 7.0' and Code_System='CPT-CAT-II') as PROC_CODE
	,'' as MOD_1
	,'CPT-CAT-II' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where A1cDoesNotHaveDM is not null



Union all 
*/
-- Bilateral Eye Removal
select
	PayerId
	,convert(varchar,cast(BilateralEyeRemovalDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Unilateral Eye Enucleation' and Code_System='CPT') as PROC_CODE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Bilateral Modifier' and Code_System='Modifier') as MOD_1 
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where BilateralEyeRemovalDate is not null

Union all
-- BMD Test

select
	PayerId
	,convert(varchar,cast(BMDTestDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Bone Mineral Density Tests' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where BMDTestDate is not null


Union all
-- Influenza Vaccination

select
	PayerId
	,convert(varchar,cast(FLUDateofVaccine as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Adult Influenza Vaccine Procedure' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FLUDateofVaccine is not null

Union all
-- CBP Exclusions

select
	PayerId 
	,convert(varchar,cast(CBPExclusionDate as Date),23) as PROC_START_DATE
	,Case
		when CBPExclusion='Kidney Transplant' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Kidney Transplant' and Code_System='CPT')
		when CBPExclusion='Dialysis' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Dialysis Procedure' and Code_System='CPT')
		when CBPExclusion='Nephrectomy' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Nephrectomy' and Code_System='CPT') 
	end as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where CBPExclusionDate is not null and CBPExclusion in('Nephrectomy','Dialysis','Kidney Transplant')

Union all
-- Chlamydia
select
	PayerId
	,convert(varchar,cast(ChlamydiaDateofScreening as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Chlamydia Tests' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ChlamydiaDateofScreening is not null

Union all

-- Colectomy

select
	PayerId
	,convert(varchar,cast(ColectomyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Total Colectomy' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ColectomyDate is not null

Union all

-- Cologuard

select
	PayerId
	,convert(varchar,cast(CologuardDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where CologuardDate is not null

Union all

-- Colonoscopy

select
	PayerId
	,convert(varchar,cast(ColonoscopyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Colonoscopy' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ColonoscopyDate is not null

Union all

--Colorectal 

select
	PayerId
	,convert(varchar,cast(ColorectalCancerDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Colorectal Cancer' and Code_System='HCPCS') as PROC_CODE
	,'' as MOD_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ColorectalCancerDate is not null

Union all

--CT Colonography

select
	PayerId
	,convert(varchar,cast(CTColonographyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where CTColonographyDate is not null

Union all

--AWC

select
	PayerId
	,convert(varchar,cast(DateofAWC as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Well-Care' and Code_System='CPT') as PROC_CODE
	,'' as MOD_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where DateofAWC is not null

Union all

--AWv

select
	PayerId
	,convert(varchar,cast(DateofAWV as Date),23) as PROC_START_DATE
	,AWV_Code as PROC_CODE
	,'' as MOD_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where DateofAWV is not null



Union all 
--ACP

select
PayerId
,convert(varchar,cast(ACP_Date as Date),23) as PROC_START_DATE
,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning' and Code_System='CPT-CAT-II') as PROC_CODE
,'' as MOD_1
,'CPT' as PROC_CODE_TYPE
,'FlatFiles-Gaps' as PROC_DATA_SRC
,159 as ROOT_COMPANIES_ID
,FileDate
,FileName
,RecordNo
,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ACP_Date is not null

union all
--Depression Screening

select
	PayerId
	,convert(varchar,cast(DepressionDateofScreening as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.REG.REGISTRY_VALUESET where Measure_id=18 and ValueSetName='Depression Screening') as PROC_CODE
	,'' as MOD_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where DepressionDateofScreening is not null

Union all

--Depression Exclusion

select
	PayerId
	,convert(varchar,cast(DepressionExclusionDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.REG.REGISTRY_VALUESET where Measure_id=18 and ValueSetName='Depression Screening Exclusion' and CodeSystem='HCPCS') as PROC_CODE
	,'' as MOD_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where DepressionExclusionDate is not null

Union all

-- Evidence of Nephropathy

select
	PayerId
	,convert(varchar,cast(EvidenceofNephrologyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Urine Protein Tests' and Code_System='CPT-CAT-II') as PROC_CODE
	,'' as MOD_1
	,'CPT-CAT-II' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where EvidenceofNephrologyDate is not null

Union all

-- Eye Exam
select
	PayerId
	,convert(varchar,cast(EyeExamTestDate as Date),23) as PROC_START_DATE
	,Case
		when RetinopathyResult='Positive' then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Eye Exam With Evidence of Retinopathy' and Code_System='CPT-CAT-II')
		else (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Eye Exam Without Evidence of Retinopathy' and Code_System='CPT-CAT-II')
	end as PROC_CODE
	,'' as MOD_1
	,'CPT-CAT-II' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where EyeExamTestDate is not null

Union all

-- Falls Exclusion
select
	PayerId
	,convert(varchar,cast(FallsExclusionDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.REG.REGISTRY_VALUESET where Measure_id=27 and ValueSetName='Fall Screening Exclusion' and CodeSystem='CPT-CAT-II') as PROC_CODE
	,'1P' as Mod_1
	,'CPT-CAT-II' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FallsExclusionDate is not null

Union all

-- Flexible Sigmoidoscopy
select
	PayerId
	,convert(varchar,cast(FlexSigDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Flexible Sigmoidoscopy' and Code_System='CPT') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FlexSigDate is not null

Union all

-- Flu Patient Exclusion
select
	PayerId
	,convert(varchar,cast(FLUPatientExclusionDate as Date),23) as PROC_START_DATE
	,(select code from KPI_ENGINE.REG.REGISTRY_VALUESET where Measure_id=17 and ValueSetName='Refusal to Vaccine' and CodeSystem='HCPCS') as PROC_CODE
	,'' as Mod_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FLUPatientExclusionDate is not null

Union all
-- FOBT
select
	PayerId
	,convert(varchar,cast(FOBTDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='FOBT Lab Test' and Code_System='CPT') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FOBTDate is not null

Union all
-- Functional Assessment
select
	PayerId
	,convert(varchar,cast(FSAAssessmentDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Functional Status Assessment' and Code_System='CPT') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FSAAssessmentDate is not null

Union all

-- Mammography
select
	PayerId
	,convert(varchar,cast(MammogramDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Mammography' and Code_System='CPT') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where MammogramDate is not null

Union all
-- Osteoporosis Medication Therapy
select
	PayerId
	,convert(varchar,cast(OMWDrugDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Osteoporosis Medication Therapy' and Code_System='HCPCS') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where OMWDrugDate is not null

Union all

-- Renal Transplant
select
	PayerId
	,convert(varchar,cast(RenalTransplantDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Kidney Transplant' and Code_System='HCPCS') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where RenalTransplantDate is not null

Union all

-- SPC - Procedure Code
select
	PayerId
	,convert(varchar,cast(SPCExclusionDate as Date),23) as PROC_START_DATE
	,Case
		when SPCExclusion='In Vitro Fertilization' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='IVF' and Code_System='HCPCS')
	end as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where SPCExclusionDate is not null and SPCExclusion='In Vitro Fertilization'

Union all

-- Tobacco Screening
select
	PayerId
	,convert(varchar,cast(TobaccoDateofScreening as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.REG.REGISTRY_VALUESET where Measure_id=19 and ValueSetName='Tobacco Screening' and CodeSystem='HCPCS') as PROC_CODE
	,'' as Mod_1
	,'CPT-CAT-II' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where TobaccoDateofScreening is not null

Union all

-- Urine Tests
select
	PayerId
	,convert(varchar,cast(UrineTestDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Urine Protein Tests' and Code_System='CPT') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where UrineTestDate is not null

Union all

-- Falls
select
	PayerId
	,convert(varchar,cast(FallsDateofScreening as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.REG.REGISTRY_VALUESET where ValueSetName='Fall Screening Exclusion' and CodeSystem='CPT-CAT-II') as PROC_CODE
	,'' as Mod_1
	,'CPT-CAT-II' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FallsDateofScreening is not null

Union all


-- Hospice
select
	PayerId
	,convert(varchar,cast(Global_Exclusion_Date as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Hospice Intervention' and Code_System='CPT') as PROC_CODE
	,'' as Mod_1
	,'CPT' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where GlobalExclusion='Hospice'

Union all

-- Palliative care

select
	PayerId
	,convert(varchar,cast(Global_Exclusion_Date as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Palliative Care Encounter' and Code_System='HCPCS') as PROC_CODE
	,'' as Mod_1
	,'HCPCS' as PROC_CODE_TYPE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where GlobalExclusion='Palliative Care'


)t1
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on t1.PayerId =MEMBR_AMI  and t1.Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when t1.Payer='BCBS UHS' then concat(SUBSTRING(t1.PayerID,1,9),right(t1.PayerID,1)) else t1.PayerID end)=m.MEMBER_ID and t1.Payer!='Cigna' and m.ROOT_COMPANIES_ID=159

)t2 where rnk=1


-- Insert ICDPCS Codes
Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(MEMBER_ID,PROC_START_DATE,ICDPCS_CODE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
select MEMBER_ID,PROC_START_DATE,ICDPCS_CODE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
Select 
	dense_rank() over(partition by PayerId,PROC_START_DATE,ICDPCS_CODE order by t1.FileDate,t1.RecordNo) as rnk,
	Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
	,PayerId
	,PROC_START_DATE
	,ICDPCS_CODE
	,PROC_DATA_SRC
	,t1.ROOT_COMPANIES_ID
	,t1.FileDate
	,t1.FileName
	,t1.Payer
from(
-- Bilateral Mastectomy

select
	PayerId
	,convert(varchar,cast(BiLateralMastectomyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Bilateral Mastectomy' and Code_System='ICD10PCS') as ICDPCS_CODE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where BiLateralMastectomyDate is not null

Union all

-- Left Eye Removal
select
	PayerId
	,convert(varchar,cast(LeftEyeRemovalDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Unilateral Eye Enucleation Left' and Code_System='ICD10PCS') as ICDPCS_CODE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where LeftEyeRemovalDate is not null

Union all

-- Right Eye Removal
select
	PayerId
	,convert(varchar,cast(RightEyeRemovalDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Unilateral Eye Enucleation Right' and Code_System='ICD10PCS') as ICDPCS_CODE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where RightEyeRemovalDate is not null

Union all

-- UnilateralLeftMastectomyDate
select
	PayerId
	,convert(varchar,cast(UnilateralLeftMastectomyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Unilateral Mastectomy Left' and Code_System='ICD10PCS') as ICDPCS_CODE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where UnilateralLeftMastectomyDate is not null

Union all

-- UnilateralRightMastectomyDate
select
	PayerId
	,convert(varchar,cast(UnilateralRightMastectomyDate as Date),23) as PROC_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Unilateral Mastectomy Right' and Code_System='ICD10PCS') as ICDPCS_CODE
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where UnilateralRightMastectomyDate is not null

)t1
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on t1.PayerId =MEMBR_AMI  and t1.Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and t1.Payer!='Cigna'
)t2 where rnk=1



-- Insert into Medication
Insert Into KPI_ENGINE_STAGING.dbo.STAGING_MEDICATION(MEMBER_ID,Fill_Date,Medication_code,Medication_code_type,MED_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
Select MEMBER_ID,FillDate,MedicationCode,Medication_code_type,MED_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
Select
	dense_rank()  over(partition by PayerId,FillDate,MedicationCode order by t1.FileDate desc,t1.RecordNo Desc) as rnk,
	Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
	,FillDate
	,MedicationCode
	,Medication_code_type
	,t1.MED_DATA_SRC
	,t1.ROOT_COMPANIES_ID
	,t1.FileDate
	,t1.FileName
	,t1.RecordNo
	,t1.Payer

from(
-- ACE & ARB Medication Ingestion

select
	PayerId
	,convert(varchar,cast(ACE_ARBDate as Date),23) as FillDate
	,(select top 1 code from KPI_ENGINE.HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='ACE Inhibitor and ARB Medications' and Code_System='NDC') as MedicationCode
	,'NDC' as Medication_code_type
	,'FlatFiles-Gaps' as MED_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where ACE_ARBDate is not null

Union all

-- SPC Medication
select
	PayerId
	,convert(varchar,cast(SPCDateofMed as Date),23) as FillDate
	,(select top 1 code from KPI_ENGINE.HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications')) as MedicationCode
	,'NDC' as Medication_code_type
	,'FlatFiles-Gaps' as MED_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where SPCDateofMed is not null

Union all

-- SPD Medication

select
	PayerId
	,convert(varchar,cast(SUPDDateofMed as Date),23) as FillDate
	,(select top 1 code from KPI_ENGINE.HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications')) as MedicationCode
	,'NDC' as Medication_code_type
	,'FlatFiles-Gaps' as MED_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where SUPDDateofMed is not null
)t1
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on t1.PayerId =MEMBR_AMI  and t1.Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and t1.Payer!='Cigna'
)t2 where rnk=1



-- Insert into Diagnosis
Insert into KPI_ENGINE_STAGING.dbo.STAGING_DIAGNOSIS(MemBer_id,Diag_start_Date,Diag_code,Diag_Seq_No,Diag_code_type,Diag_data_src,Root_companies_id,FileDate,FileName)
Select MemBer_id,Diag_start_Date,Diag_code,Diag_Seq_No,Diag_code_type,Diag_data_src,Root_companies_id,FileDate,FileName from(
Select 
	dense_rank()  over(partition by PayerId,Diag_Start_date,Diag_code order by t1.FileDate desc,t1.RecordNo Desc) as rnk,
	Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
	,Diag_Start_date
	,Diag_code
	,Diag_Seq_No
	,t1.Diag_Code_type
	,t1.DIAG_DATA_SRC
	,t1.ROOT_COMPANIES_ID
	,t1.FileDate
	,t1.FileName
	,t1.RecordNo
	,t1.Payer

from(
-- CBP Exclusion
select
	PayerId
	,convert(varchar,cast(CBPExclusionDate as Date),23) as DIAG_START_DATE
	,Case
		When CBPExclusion='Pregnancy' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy' and Code_System='ICD10CM')
	
	End as DIAG_CODE
	,2 as DIAG_SEQ_NO
	,'ICD-10' as DIAG_code_type
	,'FlatFiles-Gaps' as DIAG_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where CBPExclusionDate is not null and CBPExclusion='Pregnancy'

Union all


-- SPC Exclusion - Diagnosis
select
	PayerId as MemberId
	,convert(varchar,cast(SPCExclusionDate as Date),23) as DIAG_START_DATE
	,Case
		When SPCExclusion='Pregnancy' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy' and Code_System='ICD10CM')
		When SPCExclusion='ESRD' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis' and Code_System='ICD10CM')
		When SPCExclusion='Cirrhosis' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Cirrhosis' and Code_System='ICD10CM')
		When SPCExclusion='Muscular Disease' Then (select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease' and Code_System='ICD10CM')
	End as DIAG_CODE
	,2 as DIAG_SEQ_NO
	,'ICD-10' as DIAG_code_type
	,'FlatFiles-Gaps' as DIAG_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where SPCExclusionDate is not null and SPCExclusion in('Pregnancy','ESRD','Cirrhosis','Muscular Disease')


Union all

-- Flu Medical Exclusion
select
	PayerId as MemberId
	,convert(varchar,cast(FLUMedicalExclusionDate as Date),23) as DIAG_START_DATE
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Encephalopathy Due To Vaccination' and Code_System='ICD10CM') as Diag_Code
	,2 as DIAG_SEQ_NO
	,'ICD-10' as DIAG_CODE_TYPE
	,'FlatFiles-Gaps' as DIAG_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where FLUMedicalExclusionDate is not null
)t1
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on t1.PayerId =MEMBR_AMI  and t1.Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and t1.Payer!='Cigna'
)t2 where rnk=1





-- Insert data into Vital


-- CBP
Insert into KPI_ENGINE_STAGING.dbo.STAGING_VITAL(MEMBER_ID,VitalDateTime,VitalCode,VitalName,Value,Unit,VIT_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
Select MEMBER_ID,VitalDateTime,VitalCode,VitalName,Value,Unit,VIT_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
Select 
	dense_rank()  over(partition by PayerId,VitalDateTime,VitalCode order by t1.FileDate desc,t1.RecordNo Desc) as rnk,
	Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
	,VitalDateTime
	,VitalCode
	,VitalName
	,Value
	,Unit
	,VIT_DATA_SRC
	,t1.ROOT_COMPANIES_ID
	,t1.FileDate
	,t1.FileName
	,t1.RecordNo
	,t1.Payer

from(
select
	PayerId
	,convert(varchar,cast(CBPDateofReading as Date),23) as VitalDateTime
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Systolic Blood Pressure' and Code_System='LOINC') as VitalCode
	,'BP Systolic' as VitalName
	,Systolic as Value
	,'mmHg' as Unit
	,'FlatFiles-Gaps' as VIT_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where CBPDateofReading is not null

Union all

select
	PayerId
	,convert(varchar,cast(CBPDateofReading as Date),23) as VitalDateTime
	,(select top 1 code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Diastolic Blood Pressure' and Code_System='LOINC') as VitalCode
	,'BP Diastolic' as VitalName
	,Diastolic as Value
	,'mmHg' as Unit
	,'FlatFiles-Gaps' as PROC_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
	,RecordNo
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
where CBPDateofReading is not null
)t1
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on t1.PayerId =MEMBR_AMI  and t1.Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and t1.Payer!='Cigna'
)t2 where rnk=1

-- Insert into claimline

-- Nephrologist Visit
Insert into KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE(Claim_id,SV_LINE,MEMBER_ID,FROM_DATE,TO_DATE,ATT_PROV,CL_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
Select Claim_id,SV_LINE,MEMBER_ID,FROM_DATE,TO_DATE,ATT_PROV,CL_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
Select
	dense_rank()  over(partition by PayerId,NephrologistVisitDate order by g.FileDate desc,g.RecordNo Desc) as rnk,
	g.RecordNo as Claim_id
	,1 as SV_LINE
	,Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
		,CONVERT(varchar,cast(NephrologistVisitDate as date),23) as FROM_DATE
	,CONVERT(varchar,cast(NephrologistVisitDate as date),23) as TO_DATE
	,'NEPH1' as ATT_PROV  
	,'FlatFiles-Gaps' as CL_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,g.FileDate
	,g.FileName
	,Payer
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS g
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on PayerId =MEMBR_AMI  and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna'
where NephrologistVisitDate is not null
)t1 where rnk=1




--PK:2022-02-18: Added Upsert logic for OptionalExclusions table

--UPSERT KPI_ENGINE.DBO.OptionalExclusions

    
MERGE KPI_ENGINE.DBO.OptionalExclusions AS Target
USING ( select EMPI.EMPI_ID as EMPI,
               MEMBER_ID      ,
               ExclusionDate  ,
               ExclusionCode  ,
               ExclusionReason,
               FileName       ,
               FileDate       ,
               Case When t1.DATA_SOURCE = 'BCBS UHS' Then 'BCBS-UHS'
               else t1.DATA_SOURCE
               end as DATA_SOURCE,
               EMPI.ROOT_COMPANIES_ID as ROOT_COMPANIES_ID
		from (
-- RA
select 
       Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	   end as MEMBER_ID,
	   concat(cast(cast(DoesNotHaveRA as numeric)as INT),'-01-01') as ExclusionDate,
	   'Excl-RA' as ExclusionCode,
	   'Does Not Have Rheumatiod Arthritis' as ExclusionReason,
	   FFG.FileName as FileName,
	   FFG.FileDate as FileDate,
	   FFG.Payer as DATA_SOURCE
	   
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS FFG
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on PayerId =MEMBR_AMI  and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna'

Where FFG.DoesNotHaveRA is not null

union 

-- A1c
select 
       Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	   end as MEMBER_ID,
	   concat(cast(cast(A1cDoesNotHaveDM as numeric)as INT),'-01-01') as ExclusionDate,
	   'Excl-DM' as ExclusionCode,
	   'Does Not Have Diabetes' as ExclusionReason,
	   FFG.FileName as FileName,
	   FFG.FileDate as FileDate,
	   FFG.Payer as DATA_SOURCE
	   
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS FFG
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on FFG.PayerID =MEMBR_AMI
and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna'

Where FFG.A1cDoesNotHaveDM is not null 


union

-- Eye Exam

select 
       Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	   end as MEMBER_ID,
		concat(cast(cast(EyeExamDoesNotHaveDM as numeric)as INT),'-01-01') as ExclusionDate,
	   'Excl-DM' as ExclusionCode,
	   'Does Not Have Diabetes' as ExclusionReason,
	   FFG.FileName as FileName,
	   FFG.FileDate as FileDate,
	   FFG.Payer as DATA_SOURCE
	   
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS FFG
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on FFG.PayerID =MEMBR_AMI
and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna'

Where FFG.EyeExamDoesNotHaveDM is not null	



union

-- Nephropathy

select
       Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	   end as MEMBER_ID,
	   concat(cast(cast(NephropathyDoesNotHaveDM as numeric)as INT),'-01-01') as ExclusionDate,
	   'Excl-DM' as ExclusionCode,
	   'Does Not Have Diabetes' as ExclusionReason,
	   FFG.FileName as FileName,
	   FFG.FileDate as FileDate,
	   FFG.Payer as DATA_SOURCE
	   
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS FFG
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on FFG.PayerID =MEMBR_AMI
and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna'

Where FFG.NephropathyDoesNotHaveDM is not null 



union


-- SUPD

select 
       Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	   end as MEMBER_ID,
	  	concat(cast(cast(SUPDDoesNotHaveDM as numeric)as INT),'-01-01') as ExclusionDate,
	   'Excl-DM' as ExclusionCode,
	   'Does Not Have Diabetes' as ExclusionReason,
	   FFG.FileName as FileName,
	   FFG.FileDate as FileDate,
	   FFG.Payer as DATA_SOURCE
	   
from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS FFG
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on FFG.PayerID =MEMBR_AMI
and Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when Payer='BCBS UHS' then concat(SUBSTRING(PayerID,1,9),right(PayerID,1)) else PayerID end)=m.MEMBER_ID and Payer!='Cigna'
Where FFG.SUPDDoesNotHaveDM is not null 

)t1   join KPI_ENGINE.DBO.open_empi EMPI on EMPI.Org_Patient_Extension_ID = t1.MEMBER_ID and case When t1.DATA_SOURCE = 'BCBS UHS' Then 'BCBS-UHS'
                                                                                             else t1.DATA_SOURCE end= EMPI.Data_Source)	AS Source  
ON  Source.EMPI = Target.EMPI 
and Source.MEMBER_ID = Target.MEMBER_ID
and Source.ExclusionDate = Target.ExclusionDate
and Source.ExclusionCode = Target.ExclusionCode
and Source.ExclusionReason = Target.ExclusionReason
and Source.DATA_SOURCE = Target.DATA_SOURCE
and Source.ROOT_COMPANIES_ID = Target.ROOT_COMPANIES_ID
    


-- For Updates
WHEN MATCHED THEN UPDATE SET

Target.EMPI				  = Source.EMPI,
Target.Member_id		  = Source.Member_id	,
Target.ExclusionDate	  = Source.ExclusionDate,
Target.ExclusionCode	  = Source.ExclusionCode,
Target.ExclusionReason	  = Source.ExclusionReason,
Target.LoadDate			  = cast(getdate() as date),
Target.FileName			  = Source.FileName,
Target.FileDate			  = Source.FileDate	,
Target.DATA_SOURCE		  = Source.DATA_SOURCE,
Target.ROOT_COMPANIES_ID  = Source.ROOT_COMPANIES_ID 




-- For Inserts
WHEN NOT MATCHED BY Target THEN INSERT (EMPI          ,
                                       MEMBER_ID      ,
                                       ExclusionDate  ,
                                       ExclusionCode  ,
                                       ExclusionReason,
                                       LoadDate       ,
                                       FileName       ,
                                       FileDate       ,
                                       DATA_SOURCE    ,
                                       ROOT_COMPANIES_ID) 
VALUES (Source.EMPI,
        Source.MEMBER_ID,
        Source.ExclusionDate,
        Source.ExclusionCode,
        Source.ExclusionReason,
        cast(getdate() as date),
        Source.FileName,
        Source.FileDate,
        Source.DATA_SOURCE,
        Source.ROOT_COMPANIES_ID);
    

 -- OE_Error flag setup

 DECLARE @Filename VARCHAR(1000)
 SET @Filename = (select top 1 Filename from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS)



	
	-- Move Source data to Archive from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX to KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX_Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS_ARCHIVE ON;  

	Insert into KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS_ARCHIVE(
	[LastName]
      ,[FirstName]
      ,[DOB]
      ,[Gender]
      ,[PayerID]
      ,[Payer]
      ,[GlobalExclusion]
      ,[ColonoscopyDate]
      ,[FlexSigDate]
      ,[FOBTDate]
      ,[CTColonographyDate]
      ,[CologuardDate]
      ,[ColectomyDate]
      ,[ColorectalCancerDate]
      ,[MammogramDate]
      ,[BiLateralMastectomyDate]
      ,[UnilateralLeftMastectomyDate]
      ,[UnilateralRightMastectomyDate]
      ,[BMDTestDate]
      ,[OMWDrugDate]
      ,[NoFracture]
      ,[ARTMedicationDate]
      ,[DoesNotHaveRA]
      ,[A1cTestDate]
      ,[A1cResult]
      ,[A1cDoesNotHaveDM]
      ,[EyeExamTestDate]
      ,[RetinopathyResult]
      ,[BilateralEyeRemovalDate]
      ,[LeftEyeRemovalDate]
      ,[RightEyeRemovalDate]
      ,[EyeExamDoesNotHaveDM]
      ,[NephrologistVisitDate]
      ,[RenalTransplantDate]
      ,[UrineTestDate]
      ,[ACE_ARBDate]
      ,[EvidenceofNephrologyDate]
      ,[NephropathyDoesNotHaveDM]
      ,[FSAAssessmentDate]
      ,[SPCDateofMed]
      ,[SPCExclusion]
      ,[SPCExclusionDate]
      ,[CBPDateofReading]
      ,[Systolic]
      ,[Diastolic]
      ,[CBPExclusion]
      ,[CBPExclusionDate]
      ,[SUPDDateofMed]
      ,[ESRD]
      ,[SUPDDoesNotHaveDM]
      ,[FLUDateofVaccine]
      ,[FLUPatientExclusionDate]
      ,[FLUMedicalExclusionDate]
      ,[DepressionDateofScreening]
      ,[DepressionExclusionDate]
      ,[TobaccoDateofScreening]
      ,[DateofAWV]
      ,[ChlamydiaDateofScreening]
      ,[FallsDateofScreening]
      ,[FallsExclusionDate]
      ,[DateofAWC]
      ,[FileName]
      ,[FileDate]
      ,[LoadDateTime]
      ,[RecordNo]
	)
SELECT [LastName]
      ,[FirstName]
      ,[DOB]
      ,[Gender]
      ,[PayerID]
      ,[Payer]
      ,[GlobalExclusion]
      ,[ColonoscopyDate]
      ,[FlexSigDate]
      ,[FOBTDate]
      ,[CTColonographyDate]
      ,[CologuardDate]
      ,[ColectomyDate]
      ,[ColorectalCancerDate]
      ,[MammogramDate]
      ,[BiLateralMastectomyDate]
      ,[UnilateralLeftMastectomyDate]
      ,[UnilateralRightMastectomyDate]
      ,[BMDTestDate]
      ,[OMWDrugDate]
      ,[NoFracture]
      ,[ARTMedicationDate]
      ,[DoesNotHaveRA]
      ,[A1cTestDate]
      ,[A1cResult]
      ,[A1cDoesNotHaveDM]
      ,[EyeExamTestDate]
      ,[RetinopathyResult]
      ,[BilateralEyeRemovalDate]
      ,[LeftEyeRemovalDate]
      ,[RightEyeRemovalDate]
      ,[EyeExamDoesNotHaveDM]
      ,[NephrologistVisitDate]
      ,[RenalTransplantDate]
      ,[UrineTestDate]
      ,[ACE_ARBDate]
      ,[EvidenceofNephrologyDate]
      ,[NephropathyDoesNotHaveDM]
      ,[FSAAssessmentDate]
      ,[SPCDateofMed]
      ,[SPCExclusion]
      ,[SPCExclusionDate]
      ,[CBPDateofReading]
      ,[Systolic]
      ,[Diastolic]
      ,[CBPExclusion]
      ,[CBPExclusionDate]
      ,[SUPDDateofMed]
      ,[ESRD]
      ,[SUPDDoesNotHaveDM]
      ,[FLUDateofVaccine]
      ,[FLUPatientExclusionDate]
      ,[FLUMedicalExclusionDate]
      ,[DepressionDateofScreening]
      ,[DepressionExclusionDate]
      ,[TobaccoDateofScreening]
      ,[DateofAWV]
      ,[ChlamydiaDateofScreening]
      ,[FallsDateofScreening]
      ,[FallsExclusionDate]
      ,[DateofAWC]
      ,[FileName]
      ,[FileDate]
      ,[LoadDateTime]
      ,[RecordNo]
  FROM [KPI_ENGINE_SOURCE].[UHN].[FLATFILES_GAPS]


-- UPDATE OE_Error flag
UPDATE FFGA 
SET FFGA.OE_Error = 'OE-1'
from [KPI_ENGINE_SOURCE].UHN.[FLATFILES_GAPS_ARCHIVE] FFGA
left join KPI_ENGINE.DBO.MEMBER M on FFGA.PayerID = M.MEMBER_ID and case when FFGA.Payer  = 'BCBS UHS' THEN 'BCBS-UHS'
                                                                         else FFGA.Payer
																    end= M.MEM_DATA_SRC
where M.EMPI is null and FFGA.FileName = @Filename


-- Adding logic to handle deceased member
Update m Set m.MEM_DOD=t2.MEM_DOD from KPI_ENGINE.dbo.Member m 
Join
(
Select
	t1.*,
	Case
		when Payer='Cigna' and INDIV_ENTERPRISE_ID is not null then cast(INDIV_ENTERPRISE_ID as varchar(100))
		when m.Member_id is not null then m.Member_id
		else PayerId
	end as MEMBER_ID
From
(
	select
		PayerId
		,convert(varchar,cast(Global_Exclusion_Date as Date),23) as MEM_DOD
		,Payer
	from KPI_ENGINE_SOURCE.UHN.FLATFILES_GAPS
	where GlobalExclusion='Deceased'
)t1
left outer Join 
(
	select distinct INDIV_ENTERPRISE_ID,MEMBR_AMI from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE
)c1 on t1.PayerId =MEMBR_AMI  and t1.Payer='CIGNA'
left outer Join KPI_ENGINE.dbo.MEMBER m on (case when t1.Payer='BCBS UHS' then concat(SUBSTRING(t1.PayerID,1,9),right(t1.PayerID,1)) else t1.PayerID end)=m.MEMBER_ID and t1.Payer!='Cigna' and m.ROOT_COMPANIES_ID=159
)t2 on m.MEMBER_ID=t2.MEMBER_ID and m.MEM_DATA_SRC=t2.Payer
	


	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].UHN.[FLATFILES_GAPS]

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.[FLATFILES_GAPS_ARCHIVE] OFF;  

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
