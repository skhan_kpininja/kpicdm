SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




CREATE PROC [dbo].[Load_00_HUMANA_MEMBER]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update REMBX.FileDate column based on Filename

	Update KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX set FileDate=convert(varchar,Cast(substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8) as date),23) where filedate is null

	-- Script to Move data from KPI_Engine_Source.UHN.HUMANA_REMBX to KPI_ENGINE_STAGING.dbo.STAGING_MEMBER

	
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEMBER(MEMBER_ID,_MEM_UDF_02_,MEM_GENDER,MEM_LNAME,MEM_FNAME,MEM_MNAME,MEM_DOB,MEM_DOD,MEM_MEDICARE,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_COUNTY,MEM_STATE,MEM_ZIP,MEM_PHONE,MEM_LANGUAGE,MEM_DATA_SRC,Root_Companies_id,Filedate,Filename,_MEM_UDF_03_)
	Select MEMBER_ID,_MEM_UDF_02_,MEM_GENDER,MEM_LNAME,MEM_FNAME,MEM_MNAME,MEM_DOB,MEM_DOD,MEM_MEDICARE,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_COUNTY,MEM_STATE,MEM_ZIP,MEM_PHONE,MEM_LANGUAGE,MEM_DATA_SRC,Root_Companies_id,Filedate,Filename,1 as _MEM_UDF_03_ from(
		select 
			dense_rank() over(partition by m.MBR_PID order by Filedate desc,recordNo desc) as rnk,
			 m.MBR_PID as MEMBER_ID
			,substring(m.MBR_PID,1,9) as _MEM_UDF_02_
			,m.MBR_SEX as MEM_GENDER
			,SUBSTRING(MBR_NM,1,case when CHARINDEX(',',MBR_NM)=0 then len(MBR_NM) else CHARINDEX(',',MBR_NM)-1 end) as MEM_LNAME
			,Case
				when CHARINDEX(' ',substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+1,len(MBR_NM)),2)>0 then Trim(substring(substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+1,len(MBR_NM)),1,CHARINDEX(' ',substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+1,len(MBR_NM)),2)))
				when CHARINDEX(' ',substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+1,len(MBR_NM)),2)=0 then Trim(substring(substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+1,len(MBR_NM)),1,len(substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+1,len(MBR_NM)))))
			end as MEM_FNAME
			--,TRIM(SUBSTRING(SUBSTRING(MBR_NM,CHARINDEX(',',MBR_NM)+2,len(MBR_NM)),CHARINDEX(' ',SUBSTRING(MBR_NM,CHARINDEX(',',MBR_NM)+2,len(MBR_NM))),len(MBR_NM))) as MEM_MNAME
			,CASE
				WHEN CHARINDEX(' ',substring(MBR_NM,CHARINDEX(',',MBR_NM,1)+2,100),1)>0
				THEN TRIM(SUBSTRING(SUBSTRING(MBR_NM,CHARINDEX(',',MBR_NM)+2,len(MBR_NM)),CHARINDEX(' ',SUBSTRING(MBR_NM,CHARINDEX(',',MBR_NM)+2,len(MBR_NM))),len(MBR_NM)))
				ELSE NULL
			END AS MEM_MNAME
--** 2021-11-01: (SG) Changed the logic to calculate the Member Middle Name as it was copying the member firstname in middle name field when there was no middle name provided
			,convert(varchar,cast(MBR_BTH as date),23) as MEM_DOB
			,NULLIF(convert(varchar,cast(DECEA_DT as date),23),'1900-01-01') as MEM_DOD
			,TRIM(MCARE_ID) as MEM_MEDICARE
			,ISNULL(MBR_ADDR1,'') as MEM_ADDR1
			,ISNULL(MBR_ADDR2,'') as MEM_ADDR2
			,MBR_CITY as MEM_CITY
			,MBR_CNTYNM as MEM_COUNTY
			,MBR_STATE as MEM_STATE
			,Substring(cast(MBR_ZIP as varchar),1,5) as MEM_ZIP
			,MBR_PHONE as MEM_PHONE
			,Case
				when WRIT_LAN ='ENG' Then 1
				when WRIT_LAN ='SPA' Then 2
				when WRIT_LAN ='OTH' Then 8
				when WRIT_LAN ='CHI' Then 4
				else ISNULL(WRIT_LAN,'')
			end as MEM_LANGUAGE
			,'HUMANA' as MEM_DATA_SRC	
			,159 as Root_Companies_id
			,Filedate
			,filename
		from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX m
		where MBR_BTH is not null
	)t1 where rnk=1

 -- Script to move data from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX to KPI_ENGINE_STAGING.dbo.STAGING_ENROLLMENT

 
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_ENROLLMENT(MEMBER_ID,EFF_DATE,TERM_DATE,SUBSCRIBER_ID,RELATION,GRP_ID,PCP_PROV,CONTRACT,BEN_PKG_ID,PROD_TYPE,MM_UNITS,RX_UNITS,DN_UNITS,VS_UNITS,PAYER_TYPE,DENTAL_BENEFIT,DRUG_BENEFIT,EN_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename)
	select MEMBER_ID,EFF_DATE,TERM_DATE,SUBSCRIBER_ID,RELATION,GRP_ID,PCP_PROV,CONTRACT,BEN_PKG_ID,PROD_TYPE,MM_UNITS,RX_UNITS,DN_UNITS,VS_UNITS,PAYER_TYPE,DENTAL_BENEFIT,DRUG_BENEFIT,EN_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename from(
		select 
			dense_rank() over(partition by MBR_PID,PROD_BD,PRD_PLNOPT order by FileDate desc,recordno desc) as rnk,
			MBR_PID as MEMBER_ID
			,NULLIF(convert(varchar,cast(PROD_BD as Date),23),'1900-01-01') as EFF_DATE
			,eomonth(convert(varchar,cast(RPT_PE as Date),23))  as TERM_DATE
			,MBR_ID as SUBSCRIBER_ID
			,case
				when MBR_REL_CD in('01','1') then '18'
				when MBR_REL_CD in('02','2') then '2'
				when MBR_REL_CD in('4') then 'G8'
				when MBR_REL_CD in('3') then '76'
				else ISNULL(MBR_REL_CD,'')
			end as RELATION
			,MBR_CUS_ID as GRP_ID
			,NPI as PCP_PROV
			,MCO_NBR as CONTRACT
			,PRD_PLNOPT as BEN_PKG_ID
			,'Medical' as PROD_TYPE
			,1 as MM_UNITS
			,1 as RX_UNITS
			,case
				when DNTL_PLANI='Yes' Then 1
				else 0
			end as DN_UNITS
			,case
				when VISNPLANI='Yes' Then 1
				else 0
			end as VS_UNITS
			,Case
				when PROD_LOB='MEP' Then 'MP'
				when PROD_LOB='MER' Then 'MR'
				when PROD_LOB='MRO' Then 'MCS'
			End as PAYER_TYPE
			,case
				when DNTL_PLANI='Yes' Then 1
				else 0
			end as DENTAL_BENEFIT
			,1 as DRUG_BENEFIT
			,'HUMANA' as EN_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,Filedate
			,Filename
	
	

		from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX
		where MBR_BTH is not null
	)t1 where rnk=1

	-- Move Source data to Archive from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX to KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX_Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX_ARCHIVE ON;  

	
	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REMBX_ARCHIVE]
           ([RPT_PE]
           ,[LOB_CLASS]
           ,[RPT_MKT]
           ,[PROV_CTRCT]
           ,[PYMT_CTRCT]
           ,[GK_CTRCT]
           ,[CENTER_NM]
           ,[GK_TY]
           ,[MBR_NM]
           ,[MBR_ID]
           ,[MBR_PID]
           ,[MBR_ADDR1]
           ,[MBR_ADDR2]
           ,[MBR_CITY]
           ,[MBR_STATE]
           ,[MBR_ZIP]
           ,[MBR_CNTYCD]
           ,[MBR_CNTYNM]
           ,[MBR_PHONE]
           ,[MBR_BTH]
           ,[MBR_AGE]
           ,[MBR_SEX]
           ,[MBR_REL_CD]
           ,[MBR_COB_SW]
           ,[GK_SERV_TY]
           ,[MBR_GK_BD]
           ,[MBR_GK_ED]
           ,[GK_END_REA]
           ,[GK_ADJ_CD]
           ,[SPC_ADJ_CD]
           ,[GRP_ADJ_CD]
           ,[CI_CUST_ID]
           ,[MBR_CUS_ID]
           ,[CUST_NM]
           ,[MAXSTUDAGE]
           ,[MAXDEPAGE]
           ,[OV_COPAY]
           ,[ER_COPAY]
           ,[RX_COPAY_G]
           ,[DNTL_PLANI]
           ,[DNTL_PLAN]
           ,[VISNPLANI]
           ,[VISN_PLAN]
           ,[CUST_LEDGR]
           ,[PROD_LOB]
           ,[PRD_PLNOPT]
           ,[GRP_LEDGR]
           ,[GROUPER]
           ,[RX_COPAY_B]
           ,[ENROL_DATE]
           ,[MCAID_ID]
           ,[MCARE_ID]
           ,[HER_PLANI]
           ,[HER_PLAN]
           ,[PROD_BD]
           ,[DUAL_ELIG]
           ,[MEDI_FLAG]
           ,[MCAID_IND]
           ,[CUR_MEDI_S]
           ,[DUAL_EFF]
           ,[DUAL_END]
           ,[DECEA_DT]
           ,[NPI]
           ,[MCO_NBR]
           ,[PBP_ID]
           ,[PBP_NAME]
           ,[SEG_ID]
           ,[GEO_MKTID]
           ,[CYC_NBR]
           ,[PCP_GRP]
           ,[VERB_LANG]
           ,[WRIT_LAN]
           ,[RES_ADDR1]
           ,[RES_ADDR2]
           ,[RES_CITY]
           ,[RES_STATE]
           ,[RES_ZIP]
           ,[RES_CNTYCD]
           ,[RES_CNTRY]
           ,[RES_CNTYNM]
           ,[RES_ADMNCD]
           ,[RES_CITYNU]
           ,[EMP_ENR_DT]
           ,[PRD_EFF_DT]
           ,[PNL_REASON]
           ,[filename]
           ,[LOADDATETIME]
           ,[LST_CHG]
           ,[FileDate]
		   ,[RecordNo])
		SELECT [RPT_PE]
      ,[LOB_CLASS]
      ,[RPT_MKT]
      ,[PROV_CTRCT]
      ,[PYMT_CTRCT]
      ,[GK_CTRCT]
      ,[CENTER_NM]
      ,[GK_TY]
      ,[MBR_NM]
      ,[MBR_ID]
      ,[MBR_PID]
      ,[MBR_ADDR1]
      ,[MBR_ADDR2]
      ,[MBR_CITY]
      ,[MBR_STATE]
      ,[MBR_ZIP]
      ,[MBR_CNTYCD]
      ,[MBR_CNTYNM]
      ,[MBR_PHONE]
      ,[MBR_BTH]
      ,[MBR_AGE]
      ,[MBR_SEX]
      ,[MBR_REL_CD]
      ,[MBR_COB_SW]
      ,[GK_SERV_TY]
      ,[MBR_GK_BD]
      ,[MBR_GK_ED]
      ,[GK_END_REA]
      ,[GK_ADJ_CD]
      ,[SPC_ADJ_CD]
      ,[GRP_ADJ_CD]
      ,[CI_CUST_ID]
      ,[MBR_CUS_ID]
      ,[CUST_NM]
      ,[MAXSTUDAGE]
      ,[MAXDEPAGE]
      ,[OV_COPAY]
      ,[ER_COPAY]
      ,[RX_COPAY_G]
      ,[DNTL_PLANI]
      ,[DNTL_PLAN]
      ,[VISNPLANI]
      ,[VISN_PLAN]
      ,[CUST_LEDGR]
      ,[PROD_LOB]
      ,[PRD_PLNOPT]
      ,[GRP_LEDGR]
      ,[GROUPER]
      ,[RX_COPAY_B]
      ,[ENROL_DATE]
      ,[MCAID_ID]
      ,[MCARE_ID]
      ,[HER_PLANI]
      ,[HER_PLAN]
      ,[PROD_BD]
      ,[DUAL_ELIG]
      ,[MEDI_FLAG]
      ,[MCAID_IND]
      ,[CUR_MEDI_S]
      ,[DUAL_EFF]
      ,[DUAL_END]
      ,[DECEA_DT]
      ,[NPI]
      ,[MCO_NBR]
      ,[PBP_ID]
      ,[PBP_NAME]
      ,[SEG_ID]
      ,[GEO_MKTID]
      ,[CYC_NBR]
      ,[PCP_GRP]
      ,[VERB_LANG]
      ,[WRIT_LAN]
      ,[RES_ADDR1]
      ,[RES_ADDR2]
      ,[RES_CITY]
      ,[RES_STATE]
      ,[RES_ZIP]
      ,[RES_CNTYCD]
      ,[RES_CNTRY]
      ,[RES_CNTYNM]
      ,[RES_ADMNCD]
      ,[RES_CITYNU]
      ,[EMP_ENR_DT]
      ,[PRD_EFF_DT]
      ,[PNL_REASON]
      ,[filename]
      ,[LOADDATETIME]
      ,[LST_CHG]
      ,[FileDate]
      ,[RecordNo]
	FROM [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REMBX]

	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].UHN.[HUMANA_REMBX]


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
