SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_00_MEDCO_RXCLAIMS]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

	 -- Update FileDate

update KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS set FileDate=convert(date,substring(Filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',Filename),8),103) where FileDate is null

-- Delete Trailer Records and header records

delete from KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS where recordcode in('8','0')

-- Move data to Member Table


Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEMBER(MEMBER_ID,MEM_GENDER,MEM_SSN,MEM_LNAME,MEM_FNAME,MEM_MNAME,MEM_DOB,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_STATE,MEM_ZIP,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
Select MEMBER_ID,MEM_GENDER,MEM_SSN,MEM_LNAME,MEM_FNAME,MEM_MNAME,MEM_DOB,MEM_ADDR1,MEM_ADDR2,MEM_CITY,MEM_STATE,MEM_ZIP,_MEM_UDF_03_,MEM_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName  from(
select 
	DENSE_RANK() over(partition by TRIM(PatientId) order by FileDate Desc,RecordNo desc) as rnk,
	TRIM(PatientId) as MEMBER_ID
	,Case
		When PatientGender=1 Then 'M'
		When PatientGender=2 Then 'F'
		When PatientGender=0 Then 'U'
	end as MEM_GENDER
	,TRIM(PatientId) as MEM_SSN
	,TRIM(PatientLastName) as MEM_LNAME
	,TRIM(PatientFirstName) as MEM_FNAME
	,TRIM(PatientMiddleName) as MEM_MNAME
	,convert(varchar,cast(PatientDateofBirth as Date),23) as MEM_DOB
	,TRIM(CardholderAddress1) as MEM_ADDR1
	,TRIM(CardholderAddress2) as MEM_ADDR2
	,TRIM(CardholderCity) as MEM_CITY
	,TRIM(CardholderState) as MEM_STATE
	,Substring(CardholderZipCode,1,5) as MEM_ZIP
	,1 as _MEM_UDF_03_
	,'MEDCO' as MEM_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,FileName
from KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS
where PatientId>0
)t1 where rnk=1

-- Move Data to ClaimLine


Insert into KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE(CLAIM_ID,CLAIM_SFX_OR_PARENT,FORM_TYPE,SV_STAT,FROM_DATE,TO_DATE,MEMBER_ID,RELATION,GRP_ID,CLAIM_REC_DATE,PAID_DATE,CHK_NUM,BEN_PKG_ID,RX_INGR_COST,RX_DISP_FEE,AMT_BILLED,AMT_DEDUCT,AMT_COB,CLAIM_IN_NETWORK,ATT_PROV,ATT_NPI,ATT_PROV_NAME,BILL_PROV,BILL_PROV_NAME,BILL_PROV_TIN,CL_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
Select CLAIM_ID,CLAIM_SFX_OR_PARENT,FORM_TYPE,SV_STAT,FROM_DATE,TO_DATE,MEMBER_ID,RELATION,GRP_ID,CLAIM_REC_DATE,PAID_DATE,CHK_NUM,BEN_PKG_ID,RX_INGR_COST,RX_DISP_FEE,AMT_BILLED,AMT_DEDUCT,AMT_COB,CLAIM_IN_NETWORK,ATT_PROV,ATT_NPI,ATT_PROV_NAME,BILL_PROV,BILL_PROV_NAME,BILL_PROV_TIN,CL_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
Select 
	Dense_rank() over(partition by TransactionID,TRIM(PatientId),DateOfService,DirectClaimPaymentReleaseDate,TransactionStatus order by c.FileDate Desc,c.RecordNo Desc) as rnk,
	Trim(TransactionID) as CLAIM_ID
	,Trim(TransactionIDcross_reference) as CLAIM_SFX_OR_PARENT
	,'D' as FORM_TYPE
	,Case
		when TransactionStatus='01' then 'R'
		when TransactionStatus='04' then 'A'
		else 'O'
	end as SV_STAT
	,convert(varchar,cast(DateOfService as Date),23) as FROM_DATE
	,convert(varchar,cast(DateOfService as Date),23) as TO_DATE
	,TRIM(PatientId) as MEMBER_ID
	,case 
		when PatientRelationshipCode=1 then '18'
		when PatientRelationshipCode=2 then '1'
		when PatientRelationshipCode=3	 then '3'
		when PatientRelationshipCode=4 then 'G8'
	end as RELATION
	,TRIM(GroupId) as GRP_ID
	,NULLIF(convert(varchar,cast(OriginalClaimReceivedDate as Date),23),'1900-01-01') as CLAIM_REC_DATE
	,Case 
		when DirectClaimPaymentReleaseDate!='00000000' Then NULLIF(convert(varchar,cast(DirectClaimPaymentReleaseDate as Date),23),'1900-01-01')  
	end as PAID_DATE
	,TRIM(CheckNumber) as CHK_NUM
	,TRIM(PlanID) as BEN_PKG_ID
	,Case
		when TransactionStatus='04' then cast(SUBSTRING(IngredientCost,1,7) as float)  * -1
		else cast(SUBSTRING(IngredientCost,1,7) as float) 
	end as RX_INGR_COST
	,cast(SUBSTRING(DispensingFee,1,7) as float) as RX_DISP_FEE
	,Case
		when TransactionStatus='04' then cast(SUBSTRING(PharmacySubmittedAmount,1,7) as float) * -1
		else cast(SUBSTRING(PharmacySubmittedAmount,1,7) as float)
	end as AMT_BILLED
	,cast(SUBSTRING(Deductible,1,7) as float) as AMT_DEDUCT
	,cast(SUBSTRING(Copay_Coinsurance,1,7) as float) as AMT_COPAY
	,cast(SUBSTRING(OtherPayerAmountPaid,1,7) as float) as AMT_COB
	,Case
		when InNetworkIndicator='Y' then  'Y'
		else 'N'
	end as CLAIM_IN_NETWORK
	,PrescriberID as ATT_PROV
	,case
		when PrescriberIDQualifier='01' then PrescriberID
	end ATT_NPI
	,case
		when PrescriberIDQualifier='01' then concat(n.Provider_last_name,',',n.Provider_first_Name,' ',n.Provider_Middle_name)
	end ATT_PROV_NAME
	,TRIM(PharmacyNPI) as BILL_PROV
	,TRIM(PharmacyName) as BILL_PROV_NAME
	,TRIM(PharmacyTaxIDNumber) as BILL_PROV_TIN
	,'MEDCO' as CL_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,c.FileName
from KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS c
left outer join KPI_ENGINE.dbo.RFT_NPI n on PrescriberID=n.NPI
where PatientId>0
)t1 where rnk=1

-- Move data to Staging Medication



Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEDICATION(CLAIM_ID,MEMBER_ID,FILL_DATE,MEDICATION_NAME,MEDICATION_CODE,MEDICATION_CODE_TYPE,QUANTITY,MED_TYPE,SUPPLY_DAYS,REFILLS,DAW,PRESCRIPTION_DATE,PRESC_NPI,PRESC_NAME,PHARM_ID,PHARM_NAME,PHARM_ADDR,PHARM_CITY,PHARM_ST,PHARM_ZIP,MED_STRENGTH,THERAPEUTIC_CLASS,MED_COVERAGE,MED_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
select CLAIM_ID,MEMBER_ID,FILL_DATE,MEDICATION_NAME,MEDICATION_CODE,MEDICATION_CODE_TYPE,QUANTITY,MED_TYPE,SUPPLY_DAYS,REFILLS,DAW,PRESCRIPTION_DATE,PRESC_NPI,PRESC_NAME,PHARM_ID,PHARM_NAME,PHARM_ADDR,PHARM_CITY,PHARM_ST,PHARM_ZIP,MED_STRENGTH,THERAPEUTIC_CLASS,MED_COVERAGE,MED_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
select 
	Dense_rank() over(partition by TRIM(PatientId),DateOfService,Product order by FileDate Desc,c.RecordNo desc) as rnk,
	TransactionID as CLAIM_ID
	,TRIM(PatientId) as MEMBER_ID
	,convert(varchar,cast(DateOfService as Date),23) as FILL_DATE
	,TRIM(ProductDescription) as MEDICATION_NAME
	,TRIM(Product) as MEDICATION_CODE
	,case	
		when ProductQualifier='01' then 'UPC'
		when ProductQualifier='02' then 'HRI'
		when ProductQualifier='03' then 'NDC'
	end as MEDICATION_CODE_TYPE
	,cast(SUBSTRING(QuantityDispensed,1,9) as float) as QUANTITY
	,Case 
		when GenericProductIndicator not in('1','2','3') Then 0
		else GenericProductIndicator
	end as MED_TYPE
	,substring(DaysSupply,1,2) as SUPPLY_DAYS
	,Trim(NumberofRefillsAuthorized) as REFILLS
	,Trim(DispenseAsWritten) as DAW
	,Case
		when DatePrescriptionWritten!='00000000' Then convert(varchar,cast(DatePrescriptionWritten as Date),23) 
	end as PRESCRIPTION_DATE
	,case
		when PrescriberIDQualifier='01' then PrescriberID
	end PRESC_NPI
	,case
		when PrescriberIDQualifier='01' then concat(n.Provider_last_name,',',n.Provider_first_Name,' ',n.Provider_Middle_name)
	end PRESC_NAME
	,TRIM(PharmacyNPI) as PHARM_ID
	,TRIM(PharmacyName) as PHARM_NAME
	,TRIM(PharmacyAddress) as PHARM_ADDR
	,TRIM(PharmacyCity) as PHARM_CITY
	,TRIM(PharmacyState) as PHARM_ST
	,SUBSTRING(PharmacyZip,1,5) as PHARM_ZIP
	,Trim(DrugStrength) as MED_STRENGTH
	,Trim(TherapeuticClassCode_Standard) as THERAPEUTIC_CLASS
	,Case
		When FORMULARYFLAG='Y' Then 1
		When FORMULARYFLAG='N' Then 0
		else 2
	end as MED_COVERAGE
	,'MEDCO' as MED_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,c.FileName
from KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS c
left outer join KPI_ENGINE.dbo.RFT_NPI n on PrescriberID=n.NPI
where PatientId>0
)t1 where rnk=1


-- Moving Data to Diagnosis



Insert into KPI_ENGINE_STAGING.dbo.STAGING_DIAGNOSIS(CLAIM_ID,MEMBER_ID,DIAG_START_DATE,DIAG_SEQ_NO,DIAG_CODE,DIAG_CODE_TYPE,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
select CLAIM_ID,MEMBER_ID,DIAG_START_DATE,DX_Order as DIAG_SEQ_NO,DX as DIAG_CODE,DIAG_CODE_TYPE,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
select 
	Dense_rank() over(partition by TRIM(PatientId),DatePrescriptionWritten,DX,DX_Order order by FileDate Desc,c.RecordNo desc) as rnk,
	TransactionID as CLAIM_ID
	,TRIM(PatientId) as MEMBER_ID
	,convert(varchar,cast(DatePrescriptionWritten as Date),23) as DIAG_START_DATE
	,DX_Order
	,replace(DX,'.','') as DX
	,Case	
		when DiagnosisCodeQualifier1='02' Then 'ICD-10'
		when DiagnosisCodeQualifier1='01' Then 'ICD-9'
	end as DIAG_CODE_TYPE
	,'MEDCO' as DIAG_DATA_SRC
	,159 as ROOT_COMPANIES_ID
	,FileDate
	,c.FileName
from KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS c
  Cross apply (values (DiagnosisCode1,1),(DiagnosisCode2,2),(DiagnosisCode3,3),(DiagnosisCode4,4),(DiagnosisCode5,5)) cs (DX, DX_Order)
  where DX!='' and  PatientId>0
)t1 where rnk=1


-- Moving data to ARCHIVE

SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS_ARCHIVE ON;  


INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[MEDCO_RXCLAIMS_ARCHIVE]
           ([RecordCode]
           ,[BatchNumber]
           ,[TransactionID]
           ,[TransactionIDcross_reference]
           ,[VendorBatchCode]
           ,[VendorBatchCodecross_reference]
           ,[MediaTypeCode]
           ,[RecordTypeCode]
           ,[NCPDPVersionNumber]
           ,[TransactionStatus]
           ,[CarrierNumber]
           ,[ContractNumber]
           ,[GroupId]
           ,[GroupPassThru]
           ,[EligibilityGroupID]
           ,[ClaimSubmissionType]
           ,[PlanID]
           ,[InsulinPlan]
           ,[SpecialtyClaimIndicator]
           ,[DCRSSuperID]
           ,[ShortCycleFillCode]
           ,[ReservedforFutureUse1]
           ,[AdjustmentType]
           ,[AdjustmentReasonCode]
           ,[AdjustmentCode]
           ,[AdjustmentIndicator]
           ,[ReversalStatus]
           ,[PairedAdjustmentSequenceNumber]
           ,[ReservedforFutureUse2]
           ,[PrescriptionNumber]
           ,[MMRXRxNumber]
           ,[FillNumber]
           ,[FillNumberCalculated]
           ,[NumberofRefillsAuthorized]
           ,[DispenseAsWritten]
           ,[CutbackIndicator]
           ,[DispensingStatus]
           ,[DatePrescriptionWritten]
           ,[DirectClaimPaymentReleaseDate]
           ,[ClaimReceivedDate]
           ,[TimeReceived]
           ,[AdjudicationDate]
           ,[DateOfService]
           ,[DateOfService_Julian]
           ,[InvoiceDate]
           ,[PrescriptionOriginCode]
           ,[IntermediaryCommunicationCode]
           ,[RejectCode1]
           ,[RejectCode2]
           ,[RejectCode3]
           ,[RejectCode4]
           ,[RejectCode5]
           ,[ClaimOverrideCode1]
           ,[ClaimOverrideCode2]
           ,[ClaimOverrideCode3]
           ,[RejectOverrideCode]
           ,[SubmissionClarificationCode]
           ,[CommunicationTypeIdentifier]
           ,[ReservedforFutureUse3]
           ,[DiagnosisCodeQualifier1]
           ,[DiagnosisCodeQualifier2]
           ,[DiagnosisCodeQualifier3]
           ,[DiagnosisCodeQualifier4]
           ,[DiagnosisCodeQualifier5]
           ,[DiagnosisCode1]
           ,[DiagnosisCode2]
           ,[DiagnosisCode3]
           ,[DiagnosisCode4]
           ,[DiagnosisCode5]
           ,[ProfessionalServiceCode1]
           ,[ProfessionalServiceCode2]
           ,[ProfessionalServiceCode3]
           ,[ProfessionalServiceCode4]
           ,[ProfessionalServiceCode5]
           ,[ProfessionalServiceCode6]
           ,[ProfessionalServiceCode7]
           ,[ProfessionalServiceCode8]
           ,[ProfessionalServiceCode9]
           ,[ReasonforServiceCode1]
           ,[ReasonforServiceCode2]
           ,[ReasonforServiceCode3]
           ,[ReasonforServiceCode4]
           ,[ReasonforServiceCode5]
           ,[ReasonforServiceCode6]
           ,[ReasonforServiceCode7]
           ,[ReasonforServiceCode8]
           ,[ReasonforServiceCode9]
           ,[ResultofServiceCode1]
           ,[ResultofServiceCode2]
           ,[ResultofServiceCode3]
           ,[ResultofServiceCode4]
           ,[ResultofServiceCode5]
           ,[ResultofServiceCode6]
           ,[ResultofServiceCode7]
           ,[ResultofServiceCode8]
           ,[ResultofServiceCode9]
           ,[ReservedforFutureUse4]
           ,[ProductQualifier]
           ,[Product]
           ,[UnitOfMeasure]
           ,[UnitDoseIndicator]
           ,[ProductDescription]
           ,[QuantityIntendedtobedispensed]
           ,[QuantityDispensed]
           ,[PrescribedDaysSupply]
           ,[DaysSupplyIntendedtobedispensed]
           ,[DaysSupply]
           ,[DrugStrength]
           ,[DosageDescription]
           ,[GCNNumber]
           ,[CompoundIndicator]
           ,[MaintenanceDrugFlag]
           ,[DrugIndicator]
           ,[HCFAIndicator]
           ,[SingleSourceGenericCode]
           ,[Filler]
           ,[TherapeuticClassCode_Specific]
           ,[TherapeuticClassCode_AHFS]
           ,[TherapeuticClassCode_Generic]
           ,[TherapeuticClassCode_Standard]
           ,[USCCode]
           ,[GenericProductIndicator]
           ,[GenericSequenceNumber]
           ,[RouteofAdministration]
           ,[DEAClassofDrug]
           ,[FDADrugEfficacyIndicator]
           ,[DrugClass]
           ,[DrugCategoryCode]
           ,[TherapeuticChapter]
           ,[ClientAccountID]
           ,[CardholderID]
           ,[CardholderFirstName]
           ,[CardholderMiddleName]
           ,[CardholderLastName]
           ,[CardholderGender]
           ,[CardholderDateofBirth]
           ,[CardholderAddress1]
           ,[CardholderAddress2]
           ,[CardholderCity]
           ,[CardholderState]
           ,[CardholderZipCode]
           ,[CoverageCode]
           ,[PatientIdQualifier]
           ,[PatientId]
           ,[PatientFirstName]
           ,[PatientMiddleName]
           ,[PatientLastName]
           ,[PatientGender]
           ,[PatientDateofBirth]
           ,[PatientAge]
           ,[PatientRelationshipCode]
           ,[PersonCode]
           ,[EligibilityClarificationCode]
           ,[EligibilityRelationCode]
           ,[PatientLocation]
           ,[ReservedforFutureUse5]
           ,[PrescriberIDQualifier]
           ,[PrescriberID]
           ,[PrescriberLastName]
           ,[PrescriberTelephoneNumber]
           ,[ServiceProviderIDQualifier]
           ,[ServiceProviderID]
           ,[PharmacyChainID]
           ,[PharmacyName]
           ,[PharmacyAddress]
           ,[PharmacyCity]
           ,[PharmacyState]
           ,[PharmacyZip]
           ,[PharmacyPhoneNumber]
           ,[PharmacyTaxIDNumber]
           ,[PharmacyCountyCode]
           ,[PharmacyClassCode]
           ,[LevelofService]
           ,[InNetworkIndicator]
           ,[NetworkType]
           ,[PharmacyNPIQualifier]
           ,[PharmacyNPI]
           ,[ReservedforFutureUse6]
           ,[BasisOfCostDetermination]
           ,[AdministrativeFeeAmount]
           ,[AdminEffectIndicator]
           ,[UsualandCustomary]
           ,[IngredientCostSubmitted]
           ,[PharmacySubmittedAmount]
           ,[IngredientCost]
           ,[DispensingFee]
           ,[IncentivePaid]
           ,[ProfessionalFee]
           ,[TotalSalesTax]
           ,[GrossCost]
           ,[ClientAmountDue]
           ,[OtherPayerAmountPaid]
           ,[IngredientCostDifferenceAmount]
           ,[AdditionalPatientDiscountApplied]
           ,[CopayWaiverAmount]
           ,[ReservedforFutureUse7]
           ,[Copay_Coinsurance]
           ,[AmountExceedingBenefitMaximum]
           ,[Deductible]
           ,[AmountAttributedtoProductSelection]
           ,[AmountAttributedtoSalesTax]
           ,[PatientPaidAmount]
           ,[ExcessCopayAmount]
           ,[OutOfPocketApplyAmount]
           ,[OutOfPocketRemainingAmount]
           ,[CAPApplyAmount]
           ,[CAPRemainingAmount]
           ,[HoldHarmlessCode]
           ,[HoldHarmlessAmount]
           ,[MemberSubmitAmount]
           ,[DeductibleRemainingAmount]
           ,[AmountAppliedtoTransitionalAssistance]
           ,[ReservedforFutureUse8]
           ,[PercentageTaxRate]
           ,[PercentageTaxBasis]
           ,[FULUnitPrice]
           ,[AGPUnitPrice]
           ,[ACQUnitPrice]
           ,[AWPUnitPrice]
           ,[MACPrice]
           ,[ReservedforFutureUse9]
           ,[OtherCoverageCode]
           ,[COBPrimaryClaimType]
           ,[COBIndicator]
           ,[OriginalClaimReceivedDate]
           ,[ClaimSequenceNumber]
           ,[ClaimDirectIndicator]
           ,[CheckNumber]
           ,[CheckDate]
           ,[FormularyAdminType]
           ,[ClientFormularyFlag]
           ,[FormularyFlag]
           ,[CopayModifierID]
           ,[FormularyFileID]
           ,[PreferredAlternativeFileID]
           ,[CopayProductDrugList]
           ,[CopayProductCopayLevel]
           ,[ReservedforFutureUse10]
           ,[MedicareRecoveryIndicator]
           ,[MedicareIndicator]
           ,[PriorAuthorizationIndicator]
           ,[PriorAuthorizationNumber]
           ,[PriorAuthorizationTypeCode]
           ,[ClaimID]
           ,[MemoItemIndicator]
           ,[PriorAuthCopayOverrideIndicator]
           ,[PatientLevelAuthorizationIndicator]
           ,[ReservedforFutureUse11]
           ,[UserSequence_Locator]
           ,[InsuranceCode]
           ,[PayrollClass]
           ,[ClientPassThrough]
           ,[BillableProductQualifier]
           ,[BillableProduct]
           ,[CopayModifierIndicator]
           ,[RRAPenalty]
           ,[EnhancedPrescriptionNumber]
           ,[AmountAttributedtoProcessorFee]
           ,[AmountAttributedtoProviderNetworkSelection]
           ,[AmountAttributedtoProductSelection_Non_PreferredFormularySelection]
           ,[AmountAttributedtoProductSelection_BrandNon_PreferredFormularySelection]
           ,[AmountAttributedtoCoverageGap]
           ,[HealthPlan_fundedAssistanceAmount]
           ,[CoverageGapDiscountIndicator]
           ,[PaymentReferenceNumber]
           ,[PreferredPharmacyStatusCode]
           ,[ReservedforClientSpecificCustomization]
           ,[FileDate]
           ,[FileName]
           ,[LoadDateTime]
		   ,[RecordNo])
SELECT [RecordCode]
      ,[BatchNumber]
      ,[TransactionID]
      ,[TransactionIDcross_reference]
      ,[VendorBatchCode]
      ,[VendorBatchCodecross_reference]
      ,[MediaTypeCode]
      ,[RecordTypeCode]
      ,[NCPDPVersionNumber]
      ,[TransactionStatus]
      ,[CarrierNumber]
      ,[ContractNumber]
      ,[GroupId]
      ,[GroupPassThru]
      ,[EligibilityGroupID]
      ,[ClaimSubmissionType]
      ,[PlanID]
      ,[InsulinPlan]
      ,[SpecialtyClaimIndicator]
      ,[DCRSSuperID]
      ,[ShortCycleFillCode]
      ,[ReservedforFutureUse1]
      ,[AdjustmentType]
      ,[AdjustmentReasonCode]
      ,[AdjustmentCode]
      ,[AdjustmentIndicator]
      ,[ReversalStatus]
      ,[PairedAdjustmentSequenceNumber]
      ,[ReservedforFutureUse2]
      ,[PrescriptionNumber]
      ,[MMRXRxNumber]
      ,[FillNumber]
      ,[FillNumberCalculated]
      ,[NumberofRefillsAuthorized]
      ,[DispenseAsWritten]
      ,[CutbackIndicator]
      ,[DispensingStatus]
      ,[DatePrescriptionWritten]
      ,[DirectClaimPaymentReleaseDate]
      ,[ClaimReceivedDate]
      ,[TimeReceived]
      ,[AdjudicationDate]
      ,[DateOfService]
      ,[DateOfService_Julian]
      ,[InvoiceDate]
      ,[PrescriptionOriginCode]
      ,[IntermediaryCommunicationCode]
      ,[RejectCode1]
      ,[RejectCode2]
      ,[RejectCode3]
      ,[RejectCode4]
      ,[RejectCode5]
      ,[ClaimOverrideCode1]
      ,[ClaimOverrideCode2]
      ,[ClaimOverrideCode3]
      ,[RejectOverrideCode]
      ,[SubmissionClarificationCode]
      ,[CommunicationTypeIdentifier]
      ,[ReservedforFutureUse3]
      ,[DiagnosisCodeQualifier1]
      ,[DiagnosisCodeQualifier2]
      ,[DiagnosisCodeQualifier3]
      ,[DiagnosisCodeQualifier4]
      ,[DiagnosisCodeQualifier5]
      ,[DiagnosisCode1]
      ,[DiagnosisCode2]
      ,[DiagnosisCode3]
      ,[DiagnosisCode4]
      ,[DiagnosisCode5]
      ,[ProfessionalServiceCode1]
      ,[ProfessionalServiceCode2]
      ,[ProfessionalServiceCode3]
      ,[ProfessionalServiceCode4]
      ,[ProfessionalServiceCode5]
      ,[ProfessionalServiceCode6]
      ,[ProfessionalServiceCode7]
      ,[ProfessionalServiceCode8]
      ,[ProfessionalServiceCode9]
      ,[ReasonforServiceCode1]
      ,[ReasonforServiceCode2]
      ,[ReasonforServiceCode3]
      ,[ReasonforServiceCode4]
      ,[ReasonforServiceCode5]
      ,[ReasonforServiceCode6]
      ,[ReasonforServiceCode7]
      ,[ReasonforServiceCode8]
      ,[ReasonforServiceCode9]
      ,[ResultofServiceCode1]
      ,[ResultofServiceCode2]
      ,[ResultofServiceCode3]
      ,[ResultofServiceCode4]
      ,[ResultofServiceCode5]
      ,[ResultofServiceCode6]
      ,[ResultofServiceCode7]
      ,[ResultofServiceCode8]
      ,[ResultofServiceCode9]
      ,[ReservedforFutureUse4]
      ,[ProductQualifier]
      ,[Product]
      ,[UnitOfMeasure]
      ,[UnitDoseIndicator]
      ,[ProductDescription]
      ,[QuantityIntendedtobedispensed]
      ,[QuantityDispensed]
      ,[PrescribedDaysSupply]
      ,[DaysSupplyIntendedtobedispensed]
      ,[DaysSupply]
      ,[DrugStrength]
      ,[DosageDescription]
      ,[GCNNumber]
      ,[CompoundIndicator]
      ,[MaintenanceDrugFlag]
      ,[DrugIndicator]
      ,[HCFAIndicator]
      ,[SingleSourceGenericCode]
      ,[Filler]
      ,[TherapeuticClassCode_Specific]
      ,[TherapeuticClassCode_AHFS]
      ,[TherapeuticClassCode_Generic]
      ,[TherapeuticClassCode_Standard]
      ,[USCCode]
      ,[GenericProductIndicator]
      ,[GenericSequenceNumber]
      ,[RouteofAdministration]
      ,[DEAClassofDrug]
      ,[FDADrugEfficacyIndicator]
      ,[DrugClass]
      ,[DrugCategoryCode]
      ,[TherapeuticChapter]
      ,[ClientAccountID]
      ,[CardholderID]
      ,[CardholderFirstName]
      ,[CardholderMiddleName]
      ,[CardholderLastName]
      ,[CardholderGender]
      ,[CardholderDateofBirth]
      ,[CardholderAddress1]
      ,[CardholderAddress2]
      ,[CardholderCity]
      ,[CardholderState]
      ,[CardholderZipCode]
      ,[CoverageCode]
      ,[PatientIdQualifier]
      ,[PatientId]
      ,[PatientFirstName]
      ,[PatientMiddleName]
      ,[PatientLastName]
      ,[PatientGender]
      ,[PatientDateofBirth]
      ,[PatientAge]
      ,[PatientRelationshipCode]
      ,[PersonCode]
      ,[EligibilityClarificationCode]
      ,[EligibilityRelationCode]
      ,[PatientLocation]
      ,[ReservedforFutureUse5]
      ,[PrescriberIDQualifier]
      ,[PrescriberID]
      ,[PrescriberLastName]
      ,[PrescriberTelephoneNumber]
      ,[ServiceProviderIDQualifier]
      ,[ServiceProviderID]
      ,[PharmacyChainID]
      ,[PharmacyName]
      ,[PharmacyAddress]
      ,[PharmacyCity]
      ,[PharmacyState]
      ,[PharmacyZip]
      ,[PharmacyPhoneNumber]
      ,[PharmacyTaxIDNumber]
      ,[PharmacyCountyCode]
      ,[PharmacyClassCode]
      ,[LevelofService]
      ,[InNetworkIndicator]
      ,[NetworkType]
      ,[PharmacyNPIQualifier]
      ,[PharmacyNPI]
      ,[ReservedforFutureUse6]
      ,[BasisOfCostDetermination]
      ,[AdministrativeFeeAmount]
      ,[AdminEffectIndicator]
      ,[UsualandCustomary]
      ,[IngredientCostSubmitted]
      ,[PharmacySubmittedAmount]
      ,[IngredientCost]
      ,[DispensingFee]
      ,[IncentivePaid]
      ,[ProfessionalFee]
      ,[TotalSalesTax]
      ,[GrossCost]
      ,[ClientAmountDue]
      ,[OtherPayerAmountPaid]
      ,[IngredientCostDifferenceAmount]
      ,[AdditionalPatientDiscountApplied]
      ,[CopayWaiverAmount]
      ,[ReservedforFutureUse7]
      ,[Copay_Coinsurance]
      ,[AmountExceedingBenefitMaximum]
      ,[Deductible]
      ,[AmountAttributedtoProductSelection]
      ,[AmountAttributedtoSalesTax]
      ,[PatientPaidAmount]
      ,[ExcessCopayAmount]
      ,[OutOfPocketApplyAmount]
      ,[OutOfPocketRemainingAmount]
      ,[CAPApplyAmount]
      ,[CAPRemainingAmount]
      ,[HoldHarmlessCode]
      ,[HoldHarmlessAmount]
      ,[MemberSubmitAmount]
      ,[DeductibleRemainingAmount]
      ,[AmountAppliedtoTransitionalAssistance]
      ,[ReservedforFutureUse8]
      ,[PercentageTaxRate]
      ,[PercentageTaxBasis]
      ,[FULUnitPrice]
      ,[AGPUnitPrice]
      ,[ACQUnitPrice]
      ,[AWPUnitPrice]
      ,[MACPrice]
      ,[ReservedforFutureUse9]
      ,[OtherCoverageCode]
      ,[COBPrimaryClaimType]
      ,[COBIndicator]
      ,[OriginalClaimReceivedDate]
      ,[ClaimSequenceNumber]
      ,[ClaimDirectIndicator]
      ,[CheckNumber]
      ,[CheckDate]
      ,[FormularyAdminType]
      ,[ClientFormularyFlag]
      ,[FormularyFlag]
      ,[CopayModifierID]
      ,[FormularyFileID]
      ,[PreferredAlternativeFileID]
      ,[CopayProductDrugList]
      ,[CopayProductCopayLevel]
      ,[ReservedforFutureUse10]
      ,[MedicareRecoveryIndicator]
      ,[MedicareIndicator]
      ,[PriorAuthorizationIndicator]
      ,[PriorAuthorizationNumber]
      ,[PriorAuthorizationTypeCode]
      ,[ClaimID]
      ,[MemoItemIndicator]
      ,[PriorAuthCopayOverrideIndicator]
      ,[PatientLevelAuthorizationIndicator]
      ,[ReservedforFutureUse11]
      ,[UserSequence_Locator]
      ,[InsuranceCode]
      ,[PayrollClass]
      ,[ClientPassThrough]
      ,[BillableProductQualifier]
      ,[BillableProduct]
      ,[CopayModifierIndicator]
      ,[RRAPenalty]
      ,[EnhancedPrescriptionNumber]
      ,[AmountAttributedtoProcessorFee]
      ,[AmountAttributedtoProviderNetworkSelection]
      ,[AmountAttributedtoProductSelection_Non_PreferredFormularySelection]
      ,[AmountAttributedtoProductSelection_BrandNon_PreferredFormularySelection]
      ,[AmountAttributedtoCoverageGap]
      ,[HealthPlan_fundedAssistanceAmount]
      ,[CoverageGapDiscountIndicator]
      ,[PaymentReferenceNumber]
      ,[PreferredPharmacyStatusCode]
      ,[ReservedforClientSpecificCustomization]
      ,[FileDate]
      ,[FileName]
      ,[LoadDateTime]
      ,[RecordNo]
  FROM KPI_ENGINE_SOURCE.[UHN].[MEDCO_RXCLAIMS]



-- Delete Data from MEDCO_RXCLAIMS

Delete from KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS;

SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.MEDCO_RXCLAIMS_ARCHIVE OFF;  

	  

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
