SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_00_UHN_PHYSICIAN_ROSTER]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update FileDate column based on Filename

	update KPI_ENGINE_SOURCE.UHN.PHYSICIAN_ROSTER set FileDate=SUBSTRING(substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',filename),6),3,4) + SUBSTRING(substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',filename),6),1,2) + '01' where FileDate is null
	  
	
	-- Script to Move data from KPI_Engine_Source.UHN.HUMANA_RELAB  to KPI_ENGINE_STAGING.dbo.STAGING_LAB

	
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROVIDER(PROV_ID,PROV_NPI,PROV_LNAME,PROV_FNAME,PROV_NAME,PROV_TAXONOMY,PROV_SPEC_DESC,PROV_GRP_NAME,PROV_GRP_ID,PROV_SPEC_DESC2,PROV_SPEC_CODE2,_PROV_UDF_01_,_PROV_UDF_02_,_PROV_UDF_03_,_PROV_UDF_04_,_PROV_UDF_05_,PROV_DATA_SRC,ROOT_COMPANIES_ID,LOADDATETIME,Filename,FileDate)
	Select PROV_ID,PROV_NPI,PROV_LNAME,PROV_FNAME,PROV_NAME,PROV_TAXONOMY,PROV_SPEC_DESC,PROV_GRP_NAME,PROV_GRP_ID,PROV_SPEC_DESC2,PROV_SPEC_CODE2,_PROV_UDF_01_,_PROV_UDF_02_,_PROV_UDF_03_,_PROV_UDF_04_,_PROV_UDF_05_,PROV_DATA_SRC,ROOT_COMPANIES_ID,LOADDATETIME,Filename,FileDate from(
		select 
			 ProvidersNPI as PROV_ID
			,ProvidersNPI as PROV_NPI
			,ProvidersLastName as PROV_LNAME
			,ProvidersFirstName as PROV_FNAME
			,ProvidersFullName as PROV_NAME
			,ISNULL(NULLIF(ProviderSpecialtiesTaxonomyCode,'[None or N/A]'),'') as PROV_TAXONOMY
			,CactusSpecialty as PROV_SPEC_DESC
			,GroupPracticeName as PROV_GRP_NAME
			,GroupsTaxid as PROV_GRP_ID
			,ISNULL(IncentiveSpecialty,'') as PROV_SPEC_DESC2
			,ISNULL(IncentiveSpecialtyTaxonomyCode,'') as PROV_SPEC_CODE2
			,ProviderRole as _PROV_UDF_01_
			,Incentive as _PROV_UDF_02_
			,ProvidersDisplayDegreesShort as _PROV_UDF_03_
			,ProviderAddressType as _PROV_UDF_04_
			,1 as _PROV_UDF_05_
			,'UHN' as PROV_DATA_SRC
			,159 as ROOT_COMPANIES_ID	
			,LOADDATETIME
			,filename
			,FileDate
			,DENSE_RANK() over(partition by ProvidersNPI,GroupPracticeName order by LOADDATETIME DESC) as rn
		from KPI_ENGINE_SOURCE.UHN.PHYSICIAN_ROSTER
	)t1 where rn=1

	-- Move data from KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.PHYSICIAN_ROSTER_ARCHIVE ON;  

	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[PHYSICIAN_ROSTER_ARCHIVE]
           ([ProvidersNPI]
           ,[ProvidersLastName]
           ,[ProvidersFirstName]
           ,[ProvidersFullName]
           ,[ProviderAddressType]
           ,[GroupPracticeName]
           ,[Incentive]
           ,[GroupsTaxid]
           ,[IncentiveSpecialty]
           ,[CactusSpecialty]
           ,[ProviderSpecialtiesTaxonomyCode]
           ,[ProvidersDisplayDegreesShort]
           ,[filename]
           ,[LOADDATETIME]
		   ,[RecordNo]
           ,[IncentiveSpecialtyTaxonomyCode]
           ,[FileDate]
		   ,ProviderRole)
	SELECT [ProvidersNPI]
      ,[ProvidersLastName]
      ,[ProvidersFirstName]
      ,[ProvidersFullName]
      ,[ProviderAddressType]
      ,[GroupPracticeName]
      ,[Incentive]
      ,[GroupsTaxid]
      ,[IncentiveSpecialty]
      ,[CactusSpecialty]
      ,[ProviderSpecialtiesTaxonomyCode]
      ,[ProvidersDisplayDegreesShort]
      ,[filename]
      ,[LOADDATETIME]
      ,[RecordNo]
      ,[IncentiveSpecialtyTaxonomyCode]
      ,[FileDate]
	  ,ProviderRole
	FROM [KPI_ENGINE_SOURCE].[UHN].[PHYSICIAN_ROSTER]


	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].[UHN].[PHYSICIAN_ROSTER]

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.PHYSICIAN_ROSTER_ARCHIVE OFF;  

	
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
