SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_01_BRIGHT_MEDCLAIMS]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

	-- Update FileDate based of Filename

	update KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES set FileDate=convert(date,substring(Filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',Filename),8),103) where FileDate is null

	-- Move MEDCLAIMS to Claimline

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE(CLAIM_ID,SV_LINE,MEMBER_ID,FROM_DATE,TO_DATE,ADM_DATE,DIS_DATE,CLIENT_LOS,PAID_DATE,SV_STAT,FORM_TYPE,POS,BEN_PKG_ID,RELATION,CLAIM_REC_DATE,REF_PROV_NAME,REF_PROV,ATT_PROV_NAME,ATT_PROV,ATT_NPI,REV_CODE,UB_BILL_TYPE,AMT_ALLOWED,AMT_BILLED,AMT_COINS,AMT_COPAY,AMT_DEDUCT,AMT_DISALLOWED,AMT_COB,AMT_PAID,BILL_PROV_NAME,BILL_PROV,BILL_PROV_TIN,MS_DRG,SERV_LOC_NAME,SERV_LOC_NPI,DIS_STAT,CL_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename)
	select CLAIM_ID,SV_LINE,MEMBER_ID,FROM_DATE,TO_DATE,ADM_DATE,DIS_DATE,case when ADM_DATE!='' and DIS_DATE!='' then Datediff(day,ADM_DATE,DIS_DATE)+1 else '' end as CLIENT_LOS,PAID_DATE,SV_STAT,FORM_TYPE,POS,BEN_PKG_ID,RELATION,CLAIM_REC_DATE,REF_PROV_NAME,REF_PROV,ATT_PROV_NAME,ATT_PROV,ATT_NPI,REV_CODE,UB_BILL_TYPE,AMT_ALLOWED,AMT_BILLED,AMT_COINS,AMT_COPAY,AMT_DEDUCT,AMT_DISALLOWED,AMT_COB,AMT_PAID,BILL_PROV_NAME,BILL_PROV,BILL_PROV_TIN,MS_DRG,SERV_LOC_NAME,SERV_LOC_NPI,DIS_STAT,CL_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename from(
		select 
			dense_rank() over(partition by ORIGINAL_BRIGHT_CLAIM_ID,CLAIM_LINE,BRIGHT_MEMBER_ID,PAID_DT,CLAIM_STATUS order by Filedate desc,recordNo desc) as rnk,
			 ORIGINAL_BRIGHT_CLAIM_ID as CLAIM_ID
			 ,CLAIM_LINE as SV_LINE
			 ,BRIGHT_MEMBER_ID as MEMBER_ID
			 ,convert(varchar,cast(SERVICE_START_DT as date),23) as FROM_DATE
			 ,convert(varchar,cast(SERVICE_END_DT as date),23) as TO_DATE
			 ,NULLIF(convert(varchar,cast(ADMISSION_DT as date),23),'1900-01-01') as ADM_DATE
			 ,NULLIF(convert(varchar,cast(DISCHARGE_DT as Date),23),'1900-01-01') as DIS_DATE
			 ,NULLIF(convert(varchar,cast(PAID_DT as date),23),'1900-01-01') as PAID_DATE
			 ,case
				when CLAIM_STATUS='PAID' then 'P'
				when CLAIM_STATUS='DENIED' then 'D'
				when CLAIM_STATUS='REVERSED' then 'R'
			End as SV_STAT
			,case
				When CLAIM_TYPE='Institutional' then 'U'
				When CLAIM_TYPE='Professional' then 'H'
			End as FORM_TYPE
			,Case
				when PLACE_OF_SERVICE_CODE='BD' Then '11'
				when PLACE_OF_SERVICE_CODE='BO' Then '11'
				when PLACE_OF_SERVICE_CODE='IL' Then '81'
				else PLACE_OF_SERVICE_CODE 
			end as POS
			,PLAN_ID as BEN_PKG_ID
			,case
				when POLICY_ROLE='Member' Then '18'
				when POLICY_ROLE='Spouse' Then '1'
				when POLICY_ROLE='Child' Then '2'
				when POLICY_ROLE='Other' Then 'G8'
			end as RELATION
			,CONVERT(varchar,cast(RECEIVED_DT as date),23) as CLAIM_REC_DATE
			,CONCAT(REFER_PROV_LAST_NAME,' ',REFER_PROV_FIRST_NAME) as REF_PROV_NAME
			,REFER_PROV_NPI as REF_PROV
			,Concat(RENDER_PROV_LAST_NAME,' ',RENDER_PROV_FIRST_NAME) as ATT_PROV_NAME
			,RENDER_PROV_NPI as ATT_PROV
			,RENDER_PROV_NPI as ATT_NPI
			,RIGHT(CONCAT('0000', REVENUE_CODE), 4) as REV_CODE
			,BILL_TYPE as UB_BILL_TYPE
			,ALLOWED_AMT as AMT_ALLOWED
			,BILLED_AMT as AMT_BILLED 
			,COINSURANCE_AMT as AMT_COINS
			,COPAY_AMT as AMT_COPAY
			,DEDUCTIBLE_AMT as AMT_DEDUCT
			,NON_COVERED_AMT as AMT_DISALLOWED
			,OTHER_INSURANCE_AMT as AMT_COB
			,PAID_AMT as AMT_PAID
			,coalesce(NULLIF(concat(BILLING_PROV_LAST_NAME,' ',BILLING_PROV_FIRST_NAME),' '),SERVICE_LOC_NAME) as BILL_PROV_NAME
			,coalesce(NULLIF(BILLING_PROV_NPI,''),replace(SERVICE_LOC_NPI,'\\','')) as BILL_PROV
			,BILLING_PROV_TIN as BILL_PROV_TIN
			,ISNULL(DRG_CODE,'') as MS_DRG
			,SERVICE_LOC_NAME as SERV_LOC_NAME
			,replace(SERVICE_LOC_NPI,'\\','') as SERV_LOC_NPI		
			,DISCHARGE_STATUS_CODE as DIS_STAT		
			,'BRIGHT' as CL_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,convert(varchar,cast(FileDate as Date),23) as FileDate
			,filename
		from KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES c
	)t1 where rnk=1


	
	-- BRIGHT_MEDCLAIMS to STAGING_DIAGNOSIS


	Insert into KPI_ENGINE_STAGING.dbo.STAGING_DIAGNOSIS(CLAIM_ID,MEMBER_ID,DIAG_START_DATE,DIAG_CODE,DIAG_SEQ_NO,DIAG_CODE_TYPE,ADMIT_DIAG,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename)
	select CLAIM_ID,MEMBER_ID,Diag_Start_DATE,DX as DIAG_CODE,DX_Order as DIAG_SEQ_NO,ISNULL(DIAG_CODE_TYPE,'') as  DIAG_CODE_TYPE,ADMIT_DIAG,DIAG_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename from(
			SELECT 
				DENSE_RANK() over(partition by ORIGINAL_BRIGHT_CLAIM_ID,BRIGHT_MEMBER_ID,SERVICE_START_DT,DX,DX_ORDER order by Filedate desc,recordNo desc) as rn
				,ORIGINAL_BRIGHT_CLAIM_ID as CLAIM_ID
				,BRIGHT_MEMBER_ID as MEMBER_ID
				,convert(varchar,cast(SERVICE_START_DT as Date),23) as Diag_Start_DATE
				,Case	
					when ICD_TYPE='ICD10' Then 'ICD-10'
					else ICD_TYPE
				end as DIAG_CODE_TYPE
				,case when DX_Order=31 then 1 else 0 end as ADMIT_DIAG
				,'BRIGHT' as DIAG_DATA_SRC
				,FileDate
				,Filename
				,159 as ROOT_COMPANIES_ID
				,DX_Order
				,replace(DX,'.','') as DX
			  FROM KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES
			  Cross apply (values (DIAGNOSIS_CODE_1,1),(DIAGNOSIS_CODE_2,2),(DIAGNOSIS_CODE_3,3),(DIAGNOSIS_CODE_4,4),(DIAGNOSIS_CODE_5,5),(DIAGNOSIS_CODE_6,6),(DIAGNOSIS_CODE_7,7),(DIAGNOSIS_CODE_8,8),(DIAGNOSIS_CODE_9,9),(DIAGNOSIS_CODE_10,10),(DIAGNOSIS_CODE_11,11),(DIAGNOSIS_CODE_12,12),(DIAGNOSIS_CODE_13,13),(DIAGNOSIS_CODE_14,14),(DIAGNOSIS_CODE_15,15),(DIAGNOSIS_CODE_16,16),(DIAGNOSIS_CODE_17,17),(DIAGNOSIS_CODE_18,18),(DIAGNOSIS_CODE_19,19),(DIAGNOSIS_CODE_20,20),(DIAGNOSIS_CODE_21,21),(DIAGNOSIS_CODE_22,22),(DIAGNOSIS_CODE_23,23),(DIAGNOSIS_CODE_24,24),(DIAGNOSIS_CODE_25,25),(DIAGNOSIS_CODE_26,26),(DIAGNOSIS_CODE_27,27),(DIAGNOSIS_CODE_28,28),(DIAGNOSIS_CODE_29,29),(DIAGNOSIS_CODE_30,30),(ADMITTING_DIAGNOSIS
				,31)) cs (DX, DX_Order)
			  Where DX!='' and BRIGHT_MEMBER_ID is not null
	)t1 where rn=1

	
	

	-- BRIGHT_MEDCLAIMS_FILES to STAGING_PROCEDURES move ICD - PCS codes

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,PROC_START_DATE,PROC_END_DATE,ICDPCS_CODE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename)
	select CLAIM_ID,SV_LINE,MEMBER_ID,Proc_Start_DATE,Proc_END_DATE,Proc_code as ICDPCS_CODE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename from(
		SELECT 
			DENSE_RANK() over(partition by ORIGINAL_BRIGHT_CLAIM_ID,BRIGHT_MEMBER_ID,SERVICE_START_DT,Proc_code order by Filedate desc,recordNo desc) as rn
			,ORIGINAL_BRIGHT_CLAIM_ID as CLAIM_ID
			,CLAIM_LINE as SV_LINE
			,BRIGHT_MEMBER_ID as MEMBER_ID
			,convert(varchar,cast(SERVICE_START_DT as Date),23) as Proc_Start_DATE
			,convert(varchar,cast(SERVICE_END_DT as Date),23) as Proc_END_DATE
			,'BRIGHT' as PROC_DATA_SRC
			,FileDate
			,Filename
			,159 as ROOT_COMPANIES_ID
			,Proc_Order
			,Proc_code
		  FROM KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES
		  Cross apply (values (SURGICAL_PROC_CODE_1,1),(SURGICAL_PROC_CODE_2,2),(SURGICAL_PROC_CODE_3,3),(SURGICAL_PROC_CODE_4,4),(SURGICAL_PROC_CODE_5,5)) cs (Proc_code,Proc_Order)
		  Where Proc_code not in('') and BRIGHT_MEMBER_ID is not null
		)t1 where rn=1


	-- -- BRIGHT_MEDCLAIMS_FILES to STAGING_PROCEDURES move CPT,HCPCS


	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,Proc_Start_DATE,Proc_END_DATE,Proc_code,MOD_1,MOD_2,MOD_3,MOD_4,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename)
	select  CLAIM_ID,SV_LINE,MEMBER_ID,Proc_Start_DATE,Proc_END_DATE,Proc_code,MOD_1,MOD_2,MOD_3,MOD_4,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename from(
		SELECT 
			DENSE_RANK() over(partition by ORIGINAL_BRIGHT_CLAIM_ID,CLAIM_LINE,BRIGHT_MEMBER_ID,SERVICE_START_DT,PROCEDURE_CODE order by Filedate desc,recordNo desc) as rn
				,ORIGINAL_BRIGHT_CLAIM_ID as CLAIM_ID
				,CLAIM_LINE as SV_LINE
				,BRIGHT_MEMBER_ID as MEMBER_ID
				,convert(varchar,cast(SERVICE_START_DT as Date),23) as Proc_Start_DATE
				,convert(varchar,cast(SERVICE_END_DT as Date),23) as Proc_END_DATE
				,PROCEDURE_CODE as PROC_CODE
				,MODIFIER_CODE_1 as MOD_1
				,MODIFIER_CODE_2 as MOD_2
				,MODIFIER_CODE_3 as MOD_3
				,MODIFIER_CODE_4 as MOD_4
				,UNITS_BILLED as SERVICE_UNIT
				,'BRIGHT' as PROC_DATA_SRC
				,FileDate
				,Filename
				,159 as ROOT_COMPANIES_ID
			  FROM KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES
			  Where PROCEDURE_CODE not in('') and BRIGHT_MEMBER_ID is not null
		)t1 where rn=1
	
	-- Move Source table data to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES_ARCHIVE ON;  
	
	INSERT INTO [KPI_ENGINE_SOURCE].UHN.[BRIGHT_MEDCLAIMS_FILES_ARCHIVE]
           ([BRIGHT_CLAIM_ID]
           ,[REVERSAL_CLAIM_KEY]
           ,[ORIGINAL_BRIGHT_CLAIM_ID]
           ,[BRIGHT_SUBSCRIBER_ID]
           ,[BRIGHT_MEMBER_ID]
           ,[PATIENT_FIRST_NAME]
           ,[PATIENT_MIDDLE_NAME]
           ,[PATIENT_LAST_NAME]
           ,[PATIENT_DOB]
           ,[PATIENT_GENDER]
           ,[POLICY_ROLE]
           ,[LOB]
           ,[PLAN_ID]
           ,[CLAIM_TYPE]
           ,[ADMISSION_DT]
           ,[DISCHARGE_DT]
           ,[CLAIM_LINE]
           ,[CLAIM_STATUS]
           ,[BILL_TYPE]
           ,[PLACE_OF_SERVICE_CODE]
           ,[DRG_CODE]
           ,[SERVICE_START_DT]
           ,[SERVICE_END_DT]
           ,[ICD_TYPE]
           ,[ADMITTING_DIAGNOSIS]
           ,[DIAGNOSIS_CODE_1]
           ,[DIAGNOSIS_CODE_2]
           ,[DIAGNOSIS_CODE_3]
           ,[DIAGNOSIS_CODE_4]
           ,[DIAGNOSIS_CODE_5]
           ,[DIAGNOSIS_CODE_6]
           ,[DIAGNOSIS_CODE_7]
           ,[DIAGNOSIS_CODE_8]
           ,[DIAGNOSIS_CODE_9]
           ,[DIAGNOSIS_CODE_10]
           ,[DIAGNOSIS_CODE_11]
           ,[DIAGNOSIS_CODE_12]
           ,[DIAGNOSIS_CODE_13]
           ,[DIAGNOSIS_CODE_14]
           ,[DIAGNOSIS_CODE_15]
           ,[DIAGNOSIS_CODE_16]
           ,[DIAGNOSIS_CODE_17]
           ,[DIAGNOSIS_CODE_18]
           ,[DIAGNOSIS_CODE_19]
           ,[DIAGNOSIS_CODE_20]
           ,[DIAGNOSIS_CODE_21]
           ,[DIAGNOSIS_CODE_22]
           ,[DIAGNOSIS_CODE_23]
           ,[DIAGNOSIS_CODE_24]
           ,[DIAGNOSIS_CODE_25]
           ,[DIAGNOSIS_CODE_26]
           ,[DIAGNOSIS_CODE_27]
           ,[DIAGNOSIS_CODE_28]
           ,[DIAGNOSIS_CODE_29]
           ,[DIAGNOSIS_CODE_30]
           ,[PROCEDURE_CODE]
           ,[MODIFIER_CODE_1]
           ,[MODIFIER_CODE_2]
           ,[MODIFIER_CODE_3]
           ,[MODIFIER_CODE_4]
           ,[REVENUE_CODE]
           ,[SURGICAL_PROC_CODE_1]
           ,[SURGICAL_PROC_CODE_2]
           ,[SURGICAL_PROC_CODE_3]
           ,[SURGICAL_PROC_CODE_4]
           ,[SURGICAL_PROC_CODE_5]
           ,[UNITS_BILLED]
           ,[UNITS_PROCESSED]
           ,[BILLED_AMT]
           ,[DISCOUNT_AMT]
           ,[ALLOWED_AMT]
           ,[PROCESSED_AMT]
           ,[DEDUCTIBLE_AMT]
           ,[COINSURANCE_AMT]
           ,[COPAY_AMT]
           ,[NON_COVERED_AMT]
           ,[OTHER_INSURANCE_AMT]
           ,[MEMBER_RESP_AMT]
           ,[PAID_AMT]
           ,[TOTAL_BILLED_AMT]
           ,[TOTAL_DISCOUNT_AMT]
           ,[TOTAL_ALLOWED_AMT]
           ,[TOTAL_PROCESSED_AMT]
           ,[TOTAL_DEDUCTIBLE_AMT]
           ,[TOTAL_COINSURANCE_AMT]
           ,[TOTAL_COPAY_AMT]
           ,[TOTAL_NON_COVERED_AMT]
           ,[TOTAL_OTHER_INSURANCE_AMT]
           ,[TOTAL_MEMBER_RESP_AMT]
           ,[TOTAL_PAID_AMT]
           ,[REMARK_CODE_1]
           ,[REMARK_DESCR_1]
           ,[REMARK_CODE_2]
           ,[REMARK_DESCR_2]
           ,[REMARK_CODE_3]
           ,[REMARK_DESCR_3]
           ,[REMARK_CODE_4]
           ,[REMARK_DESCR_4]
           ,[BILLING_PROV_TIN]
           ,[BILLING_PROV_GROUP_NAME]
           ,[BILLING_PROV_NPI]
           ,[BILLING_PROV_FIRST_NAME]
           ,[BILLING_PROV_LAST_NAME]
           ,[RENDER_PROV_NPI]
           ,[RENDER_PROV_FIRST_NAME]
           ,[RENDER_PROV_LAST_NAME]
           ,[REFER_PROV_NPI]
           ,[REFER_PROV_FIRST_NAME]
           ,[REFER_PROV_LAST_NAME]
           ,[SERVICE_LOC_NPI]
           ,[SERVICE_LOC_NAME]
           ,[SERVICE_LOC_ADDR_1]
           ,[SERVICE_LOC_ADDR_2]
           ,[SERVICE_LOC_CITY]
           ,[SERVICE_LOC_STATE_CD]
           ,[SERVICE_LOC_ZIP]
           ,[PPO_TYPE]
           ,[CLAIM_NOTE]
           ,[RECEIVED_DT]
           ,[PROCESSED_DT]
           ,[PAID_DT]
           ,[RECORD_TYPE]
           ,[filename]
           ,[LOADDATETIME]
           ,[FileDate]
		   ,RecordNo
		   ,PAYMENT_TYPE
		   ,DISCHARGE_STATUS_CODE)
		select 
			[BRIGHT_CLAIM_ID]
           ,[REVERSAL_CLAIM_KEY]
           ,[ORIGINAL_BRIGHT_CLAIM_ID]
           ,[BRIGHT_SUBSCRIBER_ID]
           ,[BRIGHT_MEMBER_ID]
           ,[PATIENT_FIRST_NAME]
           ,[PATIENT_MIDDLE_NAME]
           ,[PATIENT_LAST_NAME]
           ,[PATIENT_DOB]
           ,[PATIENT_GENDER]
           ,[POLICY_ROLE]
           ,[LOB]
           ,[PLAN_ID]
           ,[CLAIM_TYPE]
           ,[ADMISSION_DT]
           ,[DISCHARGE_DT]
           ,[CLAIM_LINE]
           ,[CLAIM_STATUS]
           ,[BILL_TYPE]
           ,[PLACE_OF_SERVICE_CODE]
           ,[DRG_CODE]
           ,[SERVICE_START_DT]
           ,[SERVICE_END_DT]
           ,[ICD_TYPE]
           ,[ADMITTING_DIAGNOSIS]
           ,[DIAGNOSIS_CODE_1]
           ,[DIAGNOSIS_CODE_2]
           ,[DIAGNOSIS_CODE_3]
           ,[DIAGNOSIS_CODE_4]
           ,[DIAGNOSIS_CODE_5]
           ,[DIAGNOSIS_CODE_6]
           ,[DIAGNOSIS_CODE_7]
           ,[DIAGNOSIS_CODE_8]
           ,[DIAGNOSIS_CODE_9]
           ,[DIAGNOSIS_CODE_10]
           ,[DIAGNOSIS_CODE_11]
           ,[DIAGNOSIS_CODE_12]
           ,[DIAGNOSIS_CODE_13]
           ,[DIAGNOSIS_CODE_14]
           ,[DIAGNOSIS_CODE_15]
           ,[DIAGNOSIS_CODE_16]
           ,[DIAGNOSIS_CODE_17]
           ,[DIAGNOSIS_CODE_18]
           ,[DIAGNOSIS_CODE_19]
           ,[DIAGNOSIS_CODE_20]
           ,[DIAGNOSIS_CODE_21]
           ,[DIAGNOSIS_CODE_22]
           ,[DIAGNOSIS_CODE_23]
           ,[DIAGNOSIS_CODE_24]
           ,[DIAGNOSIS_CODE_25]
           ,[DIAGNOSIS_CODE_26]
           ,[DIAGNOSIS_CODE_27]
           ,[DIAGNOSIS_CODE_28]
           ,[DIAGNOSIS_CODE_29]
           ,[DIAGNOSIS_CODE_30]
           ,[PROCEDURE_CODE]
           ,[MODIFIER_CODE_1]
           ,[MODIFIER_CODE_2]
           ,[MODIFIER_CODE_3]
           ,[MODIFIER_CODE_4]
           ,[REVENUE_CODE]
           ,[SURGICAL_PROC_CODE_1]
           ,[SURGICAL_PROC_CODE_2]
           ,[SURGICAL_PROC_CODE_3]
           ,[SURGICAL_PROC_CODE_4]
           ,[SURGICAL_PROC_CODE_5]
           ,[UNITS_BILLED]
           ,[UNITS_PROCESSED]
           ,[BILLED_AMT]
           ,[DISCOUNT_AMT]
           ,[ALLOWED_AMT]
           ,[PROCESSED_AMT]
           ,[DEDUCTIBLE_AMT]
           ,[COINSURANCE_AMT]
           ,[COPAY_AMT]
           ,[NON_COVERED_AMT]
           ,[OTHER_INSURANCE_AMT]
           ,[MEMBER_RESP_AMT]
           ,[PAID_AMT]
           ,[TOTAL_BILLED_AMT]
           ,[TOTAL_DISCOUNT_AMT]
           ,[TOTAL_ALLOWED_AMT]
           ,[TOTAL_PROCESSED_AMT]
           ,[TOTAL_DEDUCTIBLE_AMT]
           ,[TOTAL_COINSURANCE_AMT]
           ,[TOTAL_COPAY_AMT]
           ,[TOTAL_NON_COVERED_AMT]
           ,[TOTAL_OTHER_INSURANCE_AMT]
           ,[TOTAL_MEMBER_RESP_AMT]
           ,[TOTAL_PAID_AMT]
           ,[REMARK_CODE_1]
           ,[REMARK_DESCR_1]
           ,[REMARK_CODE_2]
           ,[REMARK_DESCR_2]
           ,[REMARK_CODE_3]
           ,[REMARK_DESCR_3]
           ,[REMARK_CODE_4]
           ,[REMARK_DESCR_4]
           ,[BILLING_PROV_TIN]
           ,[BILLING_PROV_GROUP_NAME]
           ,[BILLING_PROV_NPI]
           ,[BILLING_PROV_FIRST_NAME]
           ,[BILLING_PROV_LAST_NAME]
           ,[RENDER_PROV_NPI]
           ,[RENDER_PROV_FIRST_NAME]
           ,[RENDER_PROV_LAST_NAME]
           ,[REFER_PROV_NPI]
           ,[REFER_PROV_FIRST_NAME]
           ,[REFER_PROV_LAST_NAME]
           ,[SERVICE_LOC_NPI]
           ,[SERVICE_LOC_NAME]
           ,[SERVICE_LOC_ADDR_1]
           ,[SERVICE_LOC_ADDR_2]
           ,[SERVICE_LOC_CITY]
           ,[SERVICE_LOC_STATE_CD]
           ,[SERVICE_LOC_ZIP]
           ,[PPO_TYPE]
           ,[CLAIM_NOTE]
           ,[RECEIVED_DT]
           ,[PROCESSED_DT]
           ,[PAID_DT]
           ,[RECORD_TYPE]
           ,[filename]
           ,[LOADDATETIME]
           ,[FileDate]
		   ,RecordNo
		   ,PAYMENT_TYPE
		   ,DISCHARGE_STATUS_CODE
		from KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES

	-- Delete data from CIGNA_MEDCLAIMS_FILES

	delete from KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES;


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
