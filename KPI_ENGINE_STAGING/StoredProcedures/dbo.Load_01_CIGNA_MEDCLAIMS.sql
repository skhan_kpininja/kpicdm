SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_01_CIGNA_MEDCLAIMS]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

	-- Update FileDate based of Filename

	update KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES set FileDate=convert(date,substring(Filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',Filename),8),23) where FileDate is null


	-- Move Date from CIGNA_MEDCLAIMS to STAGING_CLAIMLINE
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE(CLAIM_ID,SV_LINE,FORM_TYPE,MEMBER_ID,FROM_DATE,TO_DATE,PAID_DATE,ADM_DATE,DIS_DATE,CLIENT_LOS,CLAIM_IN_NETWORK,BEN_PKG_ID,BILL_PROV,BILL_PROV_NAME,BILL_PROV_TIN,GRP_ID,MS_DRG,AP_DRG,POS,REV_CODE,UB_BILL_TYPE,AMT_BILLED,DIS_STAT,SV_STAT,RELATION,REF_PROV,REF_PROV_NAME,ATT_PROV,ATT_NPI,ATT_PROV_NAME,ATT_PROV_SPEC,TOS,_Claim_UDF_01_,_Claim_UDF_02_,_Claim_UDF_03_,_Claim_UDF_04_,_Claim_UDF_05_,_Claim_UDF_06_,ROOT_COMPANIES_ID,Filename,FileDate,CL_DATA_SRC)
	Select CLAIM_ID,SV_LINE,FORM_TYPE,MEMBER_ID,FROM_DATE,TO_DATE,PAID_DATE,ADM_DATE,DIS_DATE,(datediff(day,ADM_DATE,DIS_DATE)+1) as CLIENT_LOS,CLAIM_IN_NETWORK,BEN_PKG_ID,BILL_PROV,BILL_PROV_NAME,BILL_PROV_TIN,GRP_ID,MS_DRG,AP_DRG,POS,REV_CODE,UB_BILL_TYPE,AMT_BILLED,DIS_STAT,SV_STAT,RELATION,REF_PROV,REF_PROV_NAME,ATT_PROV,ATT_NPI,ATT_PROV_NAME,ATT_PROV_SPEC,TOS,_Claim_UDF_01_,_Claim_UDF_02_,_Claim_UDF_03_,_Claim_UDF_04_,_Claim_UDF_05_,_Claim_UDF_06_,ROOT_COMPANIES_ID,Filename,FileDate,CL_DATA_SRC from(
		select 
			CLM_SYS_CLM_ID as CLAIM_ID
			,ACCT_SUBTY_CD as _Claim_UDF_04_
			,ACCT_TY_CD as _Claim_UDF_03_
			,NULLIF(convert(varchar,cast(ADMSN_DT as Date),23),'1900-01-01') as ADM_DATE
			,Case
				When BEN_NT_LVL_CD='INNT' Then 'Y'
				When BEN_NT_LVL_CD='UNK' Then 'U'
				Else 'N'
			end as CLAIM_IN_NETWORK
			,BEN_PLAN_NUM as BEN_PKG_ID
			,BILL_PROV_NPI as BILL_PROV
			,BILL_PROV_NM as BILL_PROV_NAME
			,BILL_PROV_EIN as BILL_PROV_TIN
			,CLIENT_ACCT_NUM as GRP_ID
			,CLM_EVENT_DRG_CD as MS_DRG
			,DERV_CHRG_AMT as AMT_BILLED
			,DERV_POS_CD as POS
			,ISNULL(DISCHRG_STAT_CD,'') as DIS_STAT
			,DRG_CD as AP_DRG
			,NULLIF(convert(varchar,cast(DISCHRG_DT as Date),23),'1900-01-01') as DIS_DATE
			--,DATEDIFF(day,ADMSN_DT,DISCHRG_DT) as CLIENT_LOS
			,ISNULL(INDIV_ENTPR_ID,'') as MEMBER_ID
			,Case
				when Ln_Procs_Event_Cd in('ORIGINAL','1') Then 'P'
				when Ln_Procs_Event_Cd in('REVERSAL','4','5') Then 'R'
				when Ln_Procs_Event_Cd in('ADJUSTMT','R') Then 'A'
				Else Ln_Procs_Event_Cd
			 End as SV_STAT
			,MDC_CD as _Claim_UDF_01_
			,Case
				When RSN_NOT_COVRD_CD='PD' Then 'Provider Discount'
				When RSN_NOT_COVRD_CD='AI' Then 'Additional Info'
				When RSN_NOT_COVRD_CD='CC' Then 'Claim Check'
				When RSN_NOT_COVRD_CD='DN' Then 'Denied'
				When RSN_NOT_COVRD_CD='DP' Then 'Duplicate'
				When RSN_NOT_COVRD_CD='HA' Then 'Hospital Audit'
				When RSN_NOT_COVRD_CD='ID' Then 'Inelig Dep'
				When RSN_NOT_COVRD_CD='IE' Then 'Inelig Emp'
				When RSN_NOT_COVRD_CD='MC' Then 'Medicare'
				When RSN_NOT_COVRD_CD='OC' Then 'Other Carrier'
				When RSN_NOT_COVRD_CD='OT' Then 'Other'
				When RSN_NOT_COVRD_CD='PC' Then 'Pending'
				When RSN_NOT_COVRD_CD='PD' Then 'Provider Discount'
				When RSN_NOT_COVRD_CD='PE' Then 'Pre-Existing'
				When RSN_NOT_COVRD_CD='PM' Then 'Plan Max'
				When RSN_NOT_COVRD_CD='PP' Then 'Prompt Pay'
				When RSN_NOT_COVRD_CD='PX' Then 'Plan Excl'
				When RSN_NOT_COVRD_CD='SB' Then 'Subrogation'
				When RSN_NOT_COVRD_CD='UN' Then 'Unclassified'
				When RSN_NOT_COVRD_CD='UR' Then 'Util Review'
				Else RSN_NOT_COVRD_CD
			 end as _Claim_UDF_02_
			,ISNULL(relm.KPI_MAPPING_ID,21) as RELATION
			,convert(varchar,cast(PD_DT as Date),23) as PAID_DATE
			,PRODT_TY_CD as _Claim_UDF_05_
			,REFRL_PROV_NPI as REF_PROV
			,REFRL_PROV_NM as REF_PROV_NAME
			,RIGHT('0000'+CAST(Trim(REVNU_CD) AS VARCHAR(4)),4) as REV_CODE
			,RNDR_PROV_ID as ATT_PROV
			,RNDR_PROV_NPI as ATT_NPI
			,RNDR_PROV_NM as ATT_PROV_NAME
			,ISNULL(dspec.Description,'') as ATT_PROV_SPEC
			,ISNULL(dtos.Description,'') as TOS
			,Case 
				When SUBSCRBR_STAT_CD ='A' then 'Active'
				When SUBSCRBR_STAT_CD ='C' then 'Cobra-Active'
				When SUBSCRBR_STAT_CD ='D' then 'Disabled'
				When SUBSCRBR_STAT_CD ='R' then 'Retired'
				When SUBSCRBR_STAT_CD ='S' then 'Spouse'
				Else ''
			End as _Claim_UDF_06_
			,convert(varchar,cast(SVC_BEG_DT as Date),23) as FROM_DATE 
			,convert(varchar,cast(SVC_END_DT as Date),23) as TO_DATE
			,SVC_LN_NUM as SV_LINE
			,RIGHT('000'+CAST(TY_OF_BILL_CD AS VARCHAR(3)),3) as UB_BILL_TYPE
			,'M' as FORM_TYPE
			,'CIGNA' as CL_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,Filename
			,Convert(varchar,cast(FileDate as date),23) as FileDate
			,DENSE_RANK() over(partition by CLM_SYS_CLM_ID,SVC_LN_NUM,INDIV_ENTPR_ID,SVC_BEG_DT,SVC_END_DT,PD_DT,Ln_Procs_Event_Cd order by filedate desc,recordno desc) as rn
			from KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES m
			left outer join KPI_ENGINE_STAGING.dbo.SOURCE_TO_STAGING_ENTITY_MAPPING relm on m.PATNT_RELSHP_CD=relm.SOURCE_VALUE and relm.SOURCE_ENTITY='CIGNA' and relm.EntityType='Relationship'
			left outer join KPI_ENGINE_SOURCE.UHN.CIGNA_SOURCE_DICTIONARY dtos on m.SRC_TY_OF_SVC_CD=dtos.code and dtos.Type='ServiceType'
			left outer join KPI_ENGINE_SOURCE.UHN.CIGNA_SOURCE_DICTIONARY dspec on m.RNDR_PROV_SPECLTY_CD=dspec.code and dspec.Type='Specialty'
	)t1 where rn=1 and MEMBER_ID!=0

	-- Move date from CIGNA Medclaims to STAGING_DIAGNOSIS

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_DIAGNOSIS(CLAIM_ID,MEMBER_ID,DIAG_START_DATE,DIAG_CODE,DIAG_SEQ_NO,DIAG_CODE_TYPE,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename)
select CLAIM_ID,MEMBER_ID,Diag_Start_DATE,DX as DIAG_CODE,DX_Order as DIAG_SEQ_NO,ISNULL(DIAG_CODE_TYPE,'') as  DIAG_CODE_TYPE,DIAG_DATA_SRC,ROOT_COMPANIES_ID,Filedate,Filename from(
		SELECT 
			DENSE_RANK() over(partition by CLM_SYS_CLM_ID,INDIV_ENTPR_ID,SVC_BEG_DT,DX,DX_ORDER order by Filedate desc,recordNo desc) as rn
			,CLM_SYS_CLM_ID as CLAIM_ID
			,INDIV_ENTPR_ID as MEMBER_ID
			,convert(varchar,cast(SVC_BEG_DT as Date),23) as Diag_Start_DATE
			,Case	
				when ICD_VRSN_CD=10 Then 'ICD-10'
				when ICD_VRSN_CD=10 Then 'ICD-9'
				
			end as DIAG_CODE_TYPE
			,'CIGNA' as DIAG_DATA_SRC
			,FileDate
			,Filename
			,159 as ROOT_COMPANIES_ID
			,DX_Order
			,replace(DX,'.','') as DX
		  FROM KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES
		  Cross apply (values (CLM_DIAG_CD_1,1),(CLM_DIAG_CD_2,2),(CLM_DIAG_CD_3,3),(CLM_DIAG_CD_4,4),(CLM_DIAG_CD_5,5),(CLM_DIAG_CD_6,6),(CLM_DIAG_CD_7,7),(CLM_DIAG_CD_8,8),(CLM_DIAG_CD_9,9),(CLM_DIAG_CD_10,10)) cs (DX, DX_Order)
		  Where DX!='' and INDIV_ENTPR_ID is not null
	)t1 where rn=1

	
	
	-- Move ICD-PCS codes from UHN Medclaims to Staging Procedures
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,PROC_START_DATE,PROC_END_DATE,ICDPCS_CODE,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename)
	select CLAIM_ID,SV_LINE,MEMBER_ID,Proc_Start_DATE,Proc_END_DATE,Proc_code as ICDPCS_CODE,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename from(
		SELECT 
			DENSE_RANK() over(partition by CLM_SYS_CLM_ID,INDIV_ENTPR_ID,SVC_BEG_DT,Proc_code order by Filedate desc,recordNo desc) as rn
			,CLM_SYS_CLM_ID as CLAIM_ID
			,SVC_LN_NUM as SV_LINE
			,INDIV_ENTPR_ID as MEMBER_ID
			,convert(varchar,cast(SVC_BEG_DT as Date),23) as Proc_Start_DATE
			,convert(varchar,cast(SVC_END_DT as Date),23) as Proc_END_DATE
			,'CIGNA' as DIAG_DATA_SRC
			,FileDate
			,Filename
			,159 as ROOT_COMPANIES_ID
			,Proc_Order
			,Proc_code
		  FROM KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES
		  Cross apply (values (ICD_PROC_CD_1,1),(ICD_PROC_CD_2,2),(ICD_PROC_CD_3,3),(ICD_PROC_CD_4,4),(ICD_PROC_CD_5,5)) cs (Proc_code,Proc_Order)
		  Where Proc_code not in('','*') and INDIV_ENTPR_ID is not null
	)t1 where rn=1


	-- Move CPT,HCPCS from UHN Medclaims to Staging Procedures

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,Proc_Start_DATE,Proc_END_DATE,Proc_code,PROC_CODE_TYPE,MOD_1,MOD_2,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename)
	select  CLAIM_ID,SV_LINE,MEMBER_ID,Proc_Start_DATE,Proc_END_DATE,Proc_code,PROC_CODE_TYPE,MOD_1,MOD_2,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,Filename from(
		SELECT 
			DENSE_RANK() over(partition by CLM_SYS_CLM_ID,SVC_LN_NUM,INDIV_ENTPR_ID,SVC_BEG_DT,Proc_CD order by Filedate desc,recordNo desc) as rn
			,CLM_SYS_CLM_ID as CLAIM_ID
			,SVC_LN_NUM as SV_LINE
			,INDIV_ENTPR_ID as MEMBER_ID
			,convert(varchar,cast(SVC_BEG_DT as Date),23) as Proc_Start_DATE
			,convert(varchar,cast(SVC_END_DT as Date),23) as Proc_END_DATE
			,PROC_CD as PROC_CODE
			,PROC_CD_MOD_CD_1 as MOD_1
			,PROC_CD_MOD_CD_2 as MOD_2
			,Case
				When PROC_TY_CD='CP' Then 'CPT'
				When PROC_TY_CD='HC' Then 'HCPCS'
				When PROC_TY_CD='RV' Then 'REV'
				When PROC_TY_CD='AD' Then 'ADA'
				When PROC_TY_CD='DG' Then 'DRG'
			End as PROC_CODE_TYPE
			,DERV_SU_CNT as SERVICE_UNIT
			,'CIGNA' as PROC_DATA_SRC
			,FileDate
			,Filename
			,159 as ROOT_COMPANIES_ID
		  FROM KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES
		  Where PROC_CD not in('','*') and INDIV_ENTPR_ID is not null
	)t1 where rn=1

	
	
	-- Move Source table data to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES_ARCHIVE ON;  

	INSERT INTO KPI_ENGINE_SOURCE.[UHN].[CIGNA_MEDCLAIMS_FILES_ARCHIVE]
	         ([CLIENT_ACCT_NUM]
           ,[BASE_HMO_CD]
           ,[ELGBTY_BRNCH_NUM]
           ,[BEN_PLAN_NUM]
           ,[ACCT_TY_CD]
           ,[ACCT_SUBTY_CD]
           ,[PRODT_TY_CD]
           ,[CHNL_SRC_CD]
           ,[INDIV_ENTPR_ID]
           ,[AMI]
           ,[LGCY_MBR_NUM]
           ,[MEMBR_FIRST_NM]
           ,[MEMBR_LAST_NM]
           ,[MEMBR_BRTH_DT]
           ,[MEMBR_GENDER]
           ,[MEMBR_ZIP_CD]
           ,[MEMBR_AGE]
           ,[PATNT_RELSHP_CD]
           ,[SUBSCRBR_STAT_CD]
           ,[RNDR_PROV_ID]
           ,[RNDR_PROV_NPI]
           ,[BILL_PROV_EIN]
           ,[RNDR_PROV_NM]
           ,[RNDR_PROV_ADDR_LN_1]
           ,[RNDR_PROV_CITY_NM]
           ,[RNDR_PROV_ST_CD]
           ,[RNDR_PROV_POSTL_CD]
           ,[RNDR_PROV_TY_CD]
           ,[RNDR_PROV_SPECLTY_CD]
           ,[PROV_PARTCPNT_AGRMT_CD]
           ,[PD_DT]
           ,[SVC_BEG_DT]
           ,[SVC_END_DT]
           ,[DERV_CHRG_AMT]
           ,[CLM_SYS_CLM_ID]
           ,[SVC_LN_NUM]
           ,[CLM_DIAG_CD_1]
           ,[CLM_DIAG_CD_2]
           ,[CLM_DIAG_CD_3]
           ,[PROC_TY_CD]
           ,[PROC_CD]
           ,[REVNU_CD]
           ,[PROC_CD_MOD_CD_2]
           ,[BEN_NT_LVL_CD]
           ,[MDC_CD]
           ,[DERV_SU_CNT]
           ,[PROC_CD_MOD_CD_1]
           ,[SAV_RSN]
           ,[RMK_CD]
           ,[LN_PROCS_EVENT_CD]
           ,[NT_ID]
           ,[RSN_NOT_COVRD_CD]
           ,[SRC_TY_OF_SVC_CD]
           ,[DERV_POS_CD]
           ,[ENCNTR_TY_CD]
           ,[ACTL_LOS_DAYS_NUM]
           ,[DRG_CD]
           ,[DISCHRG_STAT_CD]
           ,[ICD_VRSN_CD]
           ,[CLM_EVENT_DRG_CD]
           ,[CLM_DIAG_CD_4]
           ,[CLM_DIAG_CD_5]
           ,[CLM_DIAG_CD_6]
           ,[CLM_DIAG_CD_7]
           ,[CLM_DIAG_CD_8]
           ,[CLM_DIAG_CD_9]
           ,[CLM_DIAG_CD_10]
           ,[REFRL_PROV_NPI]
           ,[BILL_PROV_NPI]
           ,[ICD_PROC_CD_1]
           ,[ICD_PROC_CD_2]
           ,[ICD_PROC_CD_3]
           ,[ICD_PROC_CD_4]
           ,[ICD_PROC_CD_5]
           ,[REFRL_PROV_NM]
           ,[BILL_PROV_NM]
           ,[TY_OF_BILL_CD]
           ,[ADMSN_DT]
           ,[DISCHRG_DT]
           ,[RPT_SEG_CD]
           ,[Filename]
           ,[LOADDATETIME]
           ,[FileDate]
		   ,[RecordNo]
		   )
		select * from KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES
	
	-- Delete data from CIGNA_MEDCLAIMS_FILES

	delete from KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES;


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
