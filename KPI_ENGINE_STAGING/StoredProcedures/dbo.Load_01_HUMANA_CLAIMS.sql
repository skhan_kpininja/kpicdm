SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_01_HUMANA_CLAIMS]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update FileDate column based on Filename

	update KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP set FileDate=convert(varchar,substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8),112);
	  
	update KPI_ENGINE_SOURCE.UHN.HUMANA_REPRX set FileDate=convert(varchar,	substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8),112);	

	-- Script to Move data from KPI_Engine_Source.UHN.HUMANA_RECLMEXP and KPI_Engine_Source.UHN.HUMANA_REPRX to KPI_ENGINE_STAGING.dbo.STAGING_MEDICATION

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_MEDICATION(CLAIM_ID,MEMBER_ID,FILL_DATE,MEDICATION_CODE,MEDICATION_NAME,MEDICATION_CODE_TYPE,QUANTITY,MANUFACTURER,MED_TYPE,SUPPLY_DAYS,REFILLS,DAW,PRESCRIPTION_DATE,PRESC_NPI,PHARM_ID,PHARM_NAME,MED_ROUTE,MED_FORM,MED_STRENGTH,THERAPEUTIC_CLASS,MED_COVERAGE,MED_DATA_SRC,ROOT_COMPANIES_ID,FileDate,filename)
	select CLAIM_ID,MEMBER_ID,FILL_DATE,MEDICATION_CODE,MEDICATION_NAME,MEDICATION_CODE_TYPE,QUANTITY,MANUFACTURER,MED_TYPE,SUPPLY_DAYS,REFILLS,DAW,PRESCRIPTION_DATE,PRESC_NPI,PHARM_ID,PHARM_NAME,MED_ROUTE,MED_FORM,MED_STRENGTH,THERAPEUTIC_CLASS,MED_COVERAGE,MED_DATA_SRC,ROOT_COMPANIES_ID,FileDate,filename from(
		select 
			DENSE_RANK() over(partition by CLAIM_NBR,MBR_PID,SRVC_IN_DT order by c.FileDate Desc,c.RecordNo Desc) as rnk,
			CLAIM_NBR as CLAIM_ID
			,MBR_PID as MEMBER_ID
			,NULLIF(CONVERT(varchar,cast(SRVC_IN_DT as Date),23),'1900-01-01') as FILL_DATE
			,NDC_NBR as MEDICATION_CODE
			,PROC_DESC as MEDICATION_NAME
			,case 
				when NDC_NBR!='' and NDC_NBR is not null then 'NDC' 
			end as MEDICATION_CODE_TYPE
			,RX_QTY as QUANTITY
			,ISNULL(p.MANUFACTUR,'') as MANUFACTURER
			,case
				when GEN_IND='G' Then 1
				when GEN_IND='N' Then 4
				else GEN_IND
			End as MED_TYPE
			,ISNULL(p.PLAN_DS,'') as SUPPLY_DAYS
			,REFIL_STAT as REFILLS
			,ISNULL(p.DAW_CD,'') as DAW
			,NULLIF(convert(varchar,cast(ISNULL(NULLIF(p.PRESTN_DT,'00000000'),'') as Date),23),'1900-01-01') as PRESCRIPTION_DATE
			,Coalesce(NULLIF(PRES_NPI_N,''),PRES_ID) as PRESC_NPI
			,PHAR_NPI as PHARM_ID
			,PHAR_NM as PHARM_NAME
			,ISNULL(RT_ADMIN,'') as MED_ROUTE
			,ISNULL(DOSAGE_DES,'') as MED_FORM
			,ISNULL(DRG_STRENG,'') as MED_STRENGTH
			,ISNULL(STC_THER_C,'') as THERAPEUTIC_CLASS
			,Case
				When FORM_STAT in('1','Y') Then 1
				When FORM_STAT in('2','N') Then 0
				else '2'
			end as MED_COVERAGE
			,'HUMANA' as MED_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,c.FileDate
			,c.FileName
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP c
		left outer join KPI_ENGINE_SOURCE.UHN.HUMANA_REPRX p on c.CLAIM_NBR=p.CAS_CLMNBR
		where c.CLAIM_TYPE='R' 
	)t1 where rnk=1

 -- Script to move Surgery data from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP to KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES

	 Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,ICDPCS_CODE,PROC_START_DATE,Proc_Seq,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select CLAIM_ID,SV_LINE,MEMBER_ID,ICDPCS_CODE,PROC_START_DATE,Proc_Seq,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			DENSE_RANK() over(partition by CLAIM_NBR,MBR_PID,Proc_Dt,Proc_code order by Filedate desc,RecordNo desc) as rnk,
			CLAIM_NBR as CLAIM_ID
			,REL_LINE_N as SV_LINE
			,MBR_PID as MEMBER_ID
			,Proc_code as ICDPCS_CODE
			,case when Proc_Dt='00000000' then Convert(varchar,cast(SRVC_IN_DT as Date),23)
				else Convert(varchar,cast(Proc_Dt as Date),23)
			end as PROC_START_DATE
			,Proc_Order as Proc_Seq
			,'HUMANA' as PROC_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,filename
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP
		  Cross apply (values (PROC_CODE1,PROC_CDDT1,1),(PROC_CODE2,PROC_CDDT2,2),(PROC_CODE3,PROC_CDDT3,3),(PROC_CODE4,PROC_CDDT4,4),(PROC_CODE5,PROC_CDDT5,5),(PROC_CODE6,PROC_CDDT6,6)) cs (Proc_code,Proc_Dt,Proc_Order)
				  Where Proc_code not in('') 
	)t1 where rnk=1

	-- Move Source Procedures data from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP to KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,PROC_CODE,PROC_CODE_TYPE,PROC_START_DATE,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select CLAIM_ID,SV_LINE,MEMBER_ID,PROC_CODE,PROC_CODE_TYPE,PROC_START_DATE,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			DENSE_RANK() over(partition by CLAIM_NBR,MBR_PID,SRVC_IN_DT,TYPE order by Filedate desc,RecordNo desc) as rnk,
			CLAIM_NBR as CLAIM_ID
			,REL_LINE_N as SV_LINE
			,MBR_PID as MEMBER_ID
			,SUBSTRING(TYPE,1,5) as PROC_CODE
			,case 
				when SRV_CD_TYP='C' then 'CPT'
				when SRV_CD_TYP='H' then 'HCPCS'
			end as PROC_CODE_TYPE
			,PROC_Desc as PROC_DESC
			,CPT4_MOD as MOD_1
			,Type_Mod as MOD_2
			,Convert(varchar,cast(SRVC_IN_DT as Date),23) as PROC_START_DATE
			,SERV_UNITS as SERVICE_UNIT
			,'HUMANA' as PROC_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,filename
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP
		where SRV_CD_TYP in('H','C')
 
	)t1 where rnk=1

	-- Script to move Procedures from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP to STAGING_PROCEDURES, this is for secondary procedures

	
	Insert into KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES(CLAIM_ID,SV_LINE,MEMBER_ID,PROC_CODE,PROC_CODE_TYPE,PROC_START_DATE,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select CLAIM_ID,SV_LINE,MEMBER_ID,PROC_CODE,PROC_CODE_TYPE,PROC_START_DATE,SERVICE_UNIT,PROC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			DENSE_RANK() over(partition by CLAIM_NBR,MBR_PID,SRVC_IN_DT,TYPE_2 order by Filedate desc,RecordNo desc) as rnk,
			CLAIM_NBR as CLAIM_ID
			,REL_LINE_N as SV_LINE
			,MBR_PID as MEMBER_ID
			,SUBSTRING(TYPE_2,1,5) as PROC_CODE
			,case 
				when SRV_CD_TYP='C' then 'CPT'
				when SRV_CD_TYP='H' then 'HCPCS'
			end as PROC_CODE_TYPE
			,PROC_Desc as PROC_DESC
			,CPT4_MOD as MOD_1
			,Type_Mod as MOD_2
			,Convert(varchar,cast(SRVC_IN_DT as Date),23) as PROC_START_DATE
			,SERV_UNITS as SERVICE_UNIT
			,'HUMANA' as PROC_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,filename
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP
		where TYPE_2!=''
 
	)t1 where rnk=1


	-- Script to move diagnosis data from KPI_ENGINE_SOURCE_RECLMEXP to STAGING_DIAGNOSIS

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_DIAGNOSIS(CLAIM_ID,MEMBER_ID,DIAG_START_DATE,DIAG_CODE,DIAG_SEQ_NO,DIAG_CODE_TYPE,ADMIT_DIAG,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select CLAIM_ID,MEMBER_ID,DIAG_START_DATE,DIAG_CODE,DIAG_SEQ_NO,DIAG_CODE_TYPE,ADMIT_DIAG,DIAG_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			DENSE_RANK() over(partition by CLAIM_NBR,MBR_PID,SRVC_IN_DT,DX,DX_Order Order by FileDate Desc,RecordNo Desc) as rnk,
			CLAIM_NBR as CLAIM_ID
			,MBR_PID as MEMBER_ID
			,convert(varchar,cast(SRVC_IN_DT as date),23) as DIAG_START_DATE
			,Replace(DX,'.','') as DIAG_CODE
			,DX_Order as DIAG_SEQ_NO
			,case
				when ICD_IND='A' then 'ICD-10'
				when ICD_IND='9' then 'ICD-9'
				else ICD_IND
			End as DIAG_CODE_TYPE
			,case 
				when DX_Order=10 then 1
				else 0
			End as ADMIT_DIAG
			,'HUMANA' as DIAG_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP
		 Cross apply (values (DIAG_CODE1,1),(DIAG_CODE2,2),(DIAG_CODE3,3),(DIAG_CODE4,4),(DIAG_CODE5,5),(DIAG_CODE6,6),(DIAG_CODE7,7),(DIAG_CODE8,8),(DIAG_CODE9,9),(ADM_DX,10)) cs (DX, DX_Order)
		 where DX!=''
	 )t1 where rnk=1

	
	
	-- Script to move Claims data from KPI_ENGINE_SOURCE.UHN.RECLMEXP to STAGING_CLAIMLINE

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE(CLAIM_ID,SV_LINE,MEMBER_ID,FORM_TYPE,SV_STAT,FROM_DATE,TO_DATE,ADM_DATE,DIS_DATE,PAID_DATE,POS,ADM_SRC,AMT_ALLOWED,AMT_BILLED,AMT_COB,AMT_COINS,AMT_COPAY,AMT_DEDUCT,AMT_PAID,AMT_WITHHOLD,ATT_PROV,AUTH_ID,BEN_PKG_ID,CHK_NUM,DIS_STAT,GRP_ID,MS_DRG,REF_PROV,RELATION,REV_CODE,UB_BILL_TYPE,ATT_NPI,BILL_PROV,CL_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select CLAIM_ID,SV_LINE,MEMBER_ID,FORM_TYPE,SV_STAT,FROM_DATE,TO_DATE,ADM_DATE,DIS_DATE,PAID_DATE,POS,ADM_SRC,AMT_ALLOWED,AMT_BILLED,AMT_COB,AMT_COINS,AMT_COPAY,AMT_DEDUCT,AMT_PAID,AMT_WITHHOLD,ATT_PROV,AUTH_ID,BEN_PKG_ID,CHK_NUM,DIS_STAT,GRP_ID,MS_DRG,REF_PROV,RELATION,REV_CODE,UB_BILL_TYPE,ATT_NPI,BILL_PROV,CL_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			dense_rank() over(partition by CLAIM_NBR,REL_LINE_N,MBR_PID,SRV_FRM_DT,SERV_TO_DT,TRAN_CODE,CHECK_DATE order by Filedate desc,Recordno desc) as rnk,
			CLAIM_NBR as CLAIM_ID
			,REL_LINE_N as SV_LINE
			,MBR_PID as MEMBER_ID
			,Case
				when CLAIM_TYPE ='H' Then 'U'
				when CLAIM_TYPE ='R' Then 'D'
				when CLAIM_TYPE ='P' Then 'H'
			end as FORM_TYPE
			,Case
				When TRAN_CODE in ('E','F') Then 'A'
				When TRAN_CODE in ('C','D') or Override in('9','09') Then 'R'
				When TRAN_CODE in ('A') Then 'V'
				else 'O'
			end as SV_STAT
			,case 
				when SRV_FRM_DT!='00000000' then convert(varchar,cast(SRV_FRM_DT as Date),23) 
			end as FROM_DATE
			,case 
				when SERV_TO_DT!='00000000' then convert(varchar,cast(SERV_TO_DT as Date),23) 
			end as TO_DATE
			,case 
				when ADM_DATE!='00000000' then convert(varchar,cast(ADM_DATE as Date),23) 
			end as ADM_DATE
			,case
				when DISCHRG_DT!='00000000' then convert(varchar,cast(DISCHRG_DT as Date),23) 
			end as DIS_DATE
			,case
				when CHECK_DATE not in('00000000','0') then convert(varchar,cast(CHECK_DATE as Date),23) 
			end as PAID_DATE
			,CMS_POT as POS
			,ADM_SRCE as ADM_SRC
			,BSCHED_AMT as AMT_ALLOWED
			,CHRG_AMT as AMT_BILLED
			,OI_AMT as AMT_COB
			,COINS_AMT as AMT_COINS
			,BASIC_DED as AMT_COPAY
			,Cast(Replace(AMT_TO_DE,'+','') as float) as AMT_DEDUCT
			,ACT_PD_AMT as AMT_PAID
			,WHOLD_AMT as AMT_WITHHOLD
			,SERV_PROV as ATT_PROV
			,ADM_AUTH as AUTH_ID
			,PRD_PLNOPT as BEN_PKG_ID
			,CHECK_NBR as CHK_NUM
			,DISCHRG_ST as DIS_STAT
			,CUS_GRP as GRP_ID
			,DRG_NBR as MS_DRG
			,REFER_PROV as REF_PROV
			,case
				when PAT_REL='AD' then 76
				when PAT_REL='CH' then 19
				when PAT_REL='EE' then 20
				when PAT_REL='HN' then 22
				when PAT_REL='SP' then 1
				when PAT_REL='SS' then 1
				when PAT_REL='ST' then 27
				Else PAT_REL
			End as RELATION
			,case 
				when SRV_CD_TYP='R' then RIGHT('0000'+CAST(Trim(Type) AS VARCHAR(4)),4)
				else ''
			end as REV_CODE
		--	,FACILITY as SERV_LOC_NPI
			,ISNULL(BILL_TYPE,'') as UB_BILL_TYPE
			,CLMSRC_NPI as ATT_NPI
			,NPI as BILL_PROV
			,'HUMANA' as CL_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,convert(varchar,cast(FileDate as Date),23) as FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP
	)t1 where rnk=1

	-- Move data from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP and KPI_ENGINE_SOURCE.UHN.HUMANA_REPRX to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_REPRX_ARCHIVE ON;  

	
	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REPRX_ARCHIVE]
           ([RPT_PE]
           ,[CYC_NBR]
           ,[GROUPER]
           ,[GRPR_NM]
           ,[PROV_CTRCT]
           ,[PROV_NM]
           ,[LDGR_NBR]
           ,[PROD_MKT_N]
           ,[SF_PROC_DT]
           ,[MBR_ID]
           ,[MBR_SUFFIX]
           ,[MBR_LST_NM]
           ,[MBR_FST_NM]
           ,[MBR_MID]
           ,[MBR_DOB]
           ,[MBR_AGE]
           ,[MBR_GEN]
           ,[PRES_DEA_I]
           ,[PRES_ID_Q]
           ,[PRES_NPI_N]
           ,[PRES_LSTNM]
           ,[PRES_FSTNM]
           ,[PHAR_QUAL]
           ,[PHAR_NABP]
           ,[PHAR_NPI_N]
           ,[CAS_CLMNBR]
           ,[PRESTN_NBR]
           ,[PRESTN_DT]
           ,[SERVICE_DT]
           ,[REFILL_NBR]
           ,[DRUG_NAME]
           ,[MANUFACTUR]
           ,[PROD_ID]
           ,[GEN_PRD_IN]
           ,[BRND_GEN_I]
           ,[FORM_STAT]
           ,[PART_BVSD]
           ,[INTNDED_DS]
           ,[PLAN_DS]
           ,[COMP_CODE]
           ,[SRC_IND]
           ,[GCN]
           ,[DRG_CLS_IN]
           ,[DRG_STRENG]
           ,[LEG_STAT_C]
           ,[TRANS_FLAG]
           ,[DIAG_CD_1]
           ,[DG_CD_QL_1]
           ,[DIAG_CD_2]
           ,[DG_CD_QL_2]
           ,[DIAG_CD_3]
           ,[DG_CD_QL_3]
           ,[DIAG_CD_4]
           ,[DG_CD_QL_4]
           ,[DIAG_CD_5]
           ,[DG_CD_QL_5]
           ,[COST_TYP_C]
           ,[UNC_AMT]
           ,[AWP_UNTCOS]
           ,[INGRED_AMT]
           ,[PHR_MAC_UC]
           ,[DISPNS_FEE]
           ,[SLS_TX_AMT]
           ,[PROF_AMT]
           ,[INCENTVE_A]
           ,[PAT_PAY_AM]
           ,[OTHR_PYR_A]
           ,[TOT_PAID_A]
           ,[PYMT_TYP_C]
           ,[CGD_AMT]
           ,[PRV_SPCCD1]
           ,[SPEC_DESC1]
           ,[PRV_SPCCD2]
           ,[SPEC_DESC2]
           ,[HOSPICE_IN]
           ,[MCO_CTRCT]
           ,[PBP_ID]
           ,[GEO_MKT]
           ,[PROD_LOB]
           ,[PHAR_IND]
           ,[CLAIM_TIER]
           ,[BRAND_NM]
           ,[GENERIC_NM]
           ,[SPEC_DRFLG]
           ,[HUM_CLDESC]
           ,[GPI_CLASS]
           ,[GPI_GROUP]
           ,[AHFS_CD]
           ,[SPC_THER_C]
           ,[STC_THER_C]
           ,[GEN_THER_C]
           ,[DAW_CD]
           ,[DAYS_DIS]
           ,[DOSAGE_DES]
           ,[RT_ADMIN]
           ,[CLM_STAT_C]
           ,[MET_DEC_QT]
           ,[AMT_SUB]
           ,[ORIG_PLN_A]
           ,[COINS_AMT]
           ,[DEDUCT_AMT]
           ,[IN_OUT_NTW]
           ,[PRT_FILL_D]
           ,[PRT_FILL_C]
           ,[DOC_KEY]
           ,[PREAUTH_NO]
           ,[filename]
           ,[FileDate]
		   ,[RecordNo]
           ,[LoadDateTime])
	SELECT [RPT_PE]
      ,[CYC_NBR]
      ,[GROUPER]
      ,[GRPR_NM]
      ,[PROV_CTRCT]
      ,[PROV_NM]
      ,[LDGR_NBR]
      ,[PROD_MKT_N]
      ,[SF_PROC_DT]
      ,[MBR_ID]
      ,[MBR_SUFFIX]
      ,[MBR_LST_NM]
      ,[MBR_FST_NM]
      ,[MBR_MID]
      ,[MBR_DOB]
      ,[MBR_AGE]
      ,[MBR_GEN]
      ,[PRES_DEA_I]
      ,[PRES_ID_Q]
      ,[PRES_NPI_N]
      ,[PRES_LSTNM]
      ,[PRES_FSTNM]
      ,[PHAR_QUAL]
      ,[PHAR_NABP]
      ,[PHAR_NPI_N]
      ,[CAS_CLMNBR]
      ,[PRESTN_NBR]
      ,[PRESTN_DT]
      ,[SERVICE_DT]
      ,[REFILL_NBR]
      ,[DRUG_NAME]
      ,[MANUFACTUR]
      ,[PROD_ID]
      ,[GEN_PRD_IN]
      ,[BRND_GEN_I]
      ,[FORM_STAT]
      ,[PART_BVSD]
      ,[INTNDED_DS]
      ,[PLAN_DS]
      ,[COMP_CODE]
      ,[SRC_IND]
      ,[GCN]
      ,[DRG_CLS_IN]
      ,[DRG_STRENG]
      ,[LEG_STAT_C]
      ,[TRANS_FLAG]
      ,[DIAG_CD_1]
      ,[DG_CD_QL_1]
      ,[DIAG_CD_2]
      ,[DG_CD_QL_2]
      ,[DIAG_CD_3]
      ,[DG_CD_QL_3]
      ,[DIAG_CD_4]
      ,[DG_CD_QL_4]
      ,[DIAG_CD_5]
      ,[DG_CD_QL_5]
      ,[COST_TYP_C]
      ,[UNC_AMT]
      ,[AWP_UNTCOS]
      ,[INGRED_AMT]
      ,[PHR_MAC_UC]
      ,[DISPNS_FEE]
      ,[SLS_TX_AMT]
      ,[PROF_AMT]
      ,[INCENTVE_A]
      ,[PAT_PAY_AM]
      ,[OTHR_PYR_A]
      ,[TOT_PAID_A]
      ,[PYMT_TYP_C]
      ,[CGD_AMT]
      ,[PRV_SPCCD1]
      ,[SPEC_DESC1]
      ,[PRV_SPCCD2]
      ,[SPEC_DESC2]
      ,[HOSPICE_IN]
      ,[MCO_CTRCT]
      ,[PBP_ID]
      ,[GEO_MKT]
      ,[PROD_LOB]
      ,[PHAR_IND]
      ,[CLAIM_TIER]
      ,[BRAND_NM]
      ,[GENERIC_NM]
      ,[SPEC_DRFLG]
      ,[HUM_CLDESC]
      ,[GPI_CLASS]
      ,[GPI_GROUP]
      ,[AHFS_CD]
      ,[SPC_THER_C]
      ,[STC_THER_C]
      ,[GEN_THER_C]
      ,[DAW_CD]
      ,[DAYS_DIS]
      ,[DOSAGE_DES]
      ,[RT_ADMIN]
      ,[CLM_STAT_C]
      ,[MET_DEC_QT]
      ,[AMT_SUB]
      ,[ORIG_PLN_A]
      ,[COINS_AMT]
      ,[DEDUCT_AMT]
      ,[IN_OUT_NTW]
      ,[PRT_FILL_D]
      ,[PRT_FILL_C]
      ,[DOC_KEY]
      ,[PREAUTH_NO]
      ,[filename]
      ,[FileDate]
      ,[RecordNo]
      ,[LoadDateTime]
  FROM [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REPRX]

	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REPRX]

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_REPRX_ARCHIVE OFF;

	-- Move data from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP to KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP_ARCHIVE

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP_ARCHIVE ON;

	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[HUMANA_RECLMEXP_ARCHIVE]
			   ([EXP_GRP_NO]
			   ,[PYMT_CTRCT]
			   ,[CLM_DEC_CD]
			   ,[IPA_LN_DEC]
			   ,[ADJ_LN_DEC]
			   ,[ADJUSTR_ID]
			   ,[FUND_TYPE]
			   ,[FUND_NBR]
			   ,[LEDGER]
			   ,[CLAIM_NBR]
			   ,[CLAIM_TYPE]
			   ,[OVERRIDE]
			   ,[PAYEE_CODE]
			   ,[RFR_ATHNBR]
			   ,[SUB_ID]
			   ,[SUB_SUF]
			   ,[SUB_DOB]
			   ,[SUB_NAME]
			   ,[PAT_NAME]
			   ,[PAT_SEX]
			   ,[PAT_DOB]
			   ,[PAT_REL]
			   ,[PAT_ACCT]
			   ,[ADM_SRCE]
			   ,[ADM_DATE]
			   ,[ADM_HOUR]
			   ,[ADM_AUTH]
			   ,[ADM_DX]
			   ,[DISCHRG_DT]
			   ,[DISCHRG_ST]
			   ,[NDC_NBR]
			   ,[RX_QTY]
			   ,[DEA_NBR]
			   ,[PROC_DATE]
			   ,[SUSP_DATE]
			   ,[ACT_DATE]
			   ,[RWORK_DATE]
			   ,[RESP_DATE]
			   ,[CHECK_DATE]
			   ,[CHECK_NBR]
			   ,[CUS_GRP]
			   ,[CUS_CL]
			   ,[PRIM_PROV]
			   ,[SERV_PROV]
			   ,[SPROV_NAME]
			   ,[SPROV_SPEC]
			   ,[SPROV_ADR1]
			   ,[SPROV_ADR2]
			   ,[SPROV_ADR3]
			   ,[SPROV_ADR4]
			   ,[SPROV_CITY]
			   ,[SPROV_STAT]
			   ,[SPROV_ZIP]
			   ,[REFER_PROV]
			   ,[ATEND_PROV]
			   ,[FACILITY]
			   ,[DIAG_PTR]
			   ,[DIAG_CODE1]
			   ,[DIAG_CODE2]
			   ,[DIAG_CODE3]
			   ,[DIAG_CODE4]
			   ,[DIAG_CODE5]
			   ,[DIAG_CODE6]
			   ,[DIAG_CODE7]
			   ,[DIAG_CODE8]
			   ,[DIAG_CODE9]
			   ,[CAUSE_CD1]
			   ,[CAUSE_CD2]
			   ,[CAUSE_CD3]
			   ,[CAUSE_CD4]
			   ,[CAUSE_CD5]
			   ,[CAUSE_CD6]
			   ,[CAUSE_CD7]
			   ,[CAUSE_CD8]
			   ,[CAUSE_CD9]
			   ,[PROC_CODE1]
			   ,[PROC_CODE2]
			   ,[PROC_CODE3]
			   ,[PROC_CODE4]
			   ,[PROC_CODE5]
			   ,[PROC_CODE6]
			   ,[PROC_CDDT1]
			   ,[PROC_CDDT2]
			   ,[PROC_CDDT3]
			   ,[PROC_CDDT4]
			   ,[PROC_CDDT5]
			   ,[PROC_CDDT6]
			   ,[SRV_FRM_DT]
			   ,[SERV_TO_DT]
			   ,[DRG_NBR]
			   ,[SERV_UNITS]
			   ,[POT]
			   ,[CHRG_AMT]
			   ,[CALC_AMT]
			   ,[RWORK_AMT]
			   ,[ACT_PD_AMT]
			   ,[BSCHED_AMT]
			   ,[BASC_WDPAY]
			   ,[BEN_SAV]
			   ,[BASIC_DED]
			   ,[BASIC_AMT]
			   ,[DISCNT_AMT]
			   ,[OI_AMT]
			   ,[SUB_RSPAMT]
			   ,[WHOLD_AMT]
			   ,[MCAID_NBR]
			   ,[DCN_NBR]
			   ,[COB_IND]
			   ,[TYPE]
			   ,[TYPE_MOD]
			   ,[HCFA_SV_TY]
			   ,[EX_CODE]
			   ,[DED_SAT]
			   ,[BDED_SAT]
			   ,[CPT4_MOD]
			   ,[AIB_APPL]
			   ,[ADJ_REASON]
			   ,[CLM_CHK]
			   ,[MULT_SRG]
			   ,[WCOMP_NBR]
			   ,[OOP_SAT]
			   ,[OOP_APPL]
			   ,[FUND_EXP]
			   ,[FND_EXP_DT]
			   ,[DISC_GRPR]
			   ,[DISC_GPRNM]
			   ,[PRD_PLNOPT]
			   ,[PLAN_LOB]
			   ,[AFF_CCOM1]
			   ,[AFF_CCOM2]
			   ,[ADJ_CLM_ID]
			   ,[ADJ_CCOM1]
			   ,[ADJ_CCOM2]
			   ,[AFF_LCOM1]
			   ,[AFF_LCOM2]
			   ,[ADJ_LNE_ID]
			   ,[ADJ_LCOM1]
			   ,[ADJ_LCOM2]
			   ,[TRAN_CODE]
			   ,[GK_PROV_ID]
			   ,[GK_PROV_NM]
			   ,[DPNT_CODE]
			   ,[BILL_TYPE]
			   ,[RVS_UNITS]
			   ,[PROV_TYPE]
			   ,[MBR_PID]
			   ,[GK_CTRCT]
			   ,[PE_DATE]
			   ,[FUN_PER_BD]
			   ,[FUN_PER_ED]
			   ,[CLAIM_IND]
			   ,[CAUSE_CD]
			   ,[PRES_NBR]
			   ,[GEN_IND]
			   ,[REFIL_STAT]
			   ,[PRTB_D_IND]
			   ,[DEN_RIDR]
			   ,[VIS_RIDR]
			   ,[HER_RIDR]
			   ,[PROCSR_ID]
			   ,[PHAR_NPI]
			   ,[PHAR_NM]
			   ,[PYT_GRP_NO]
			   ,[PYT_GRP_NM]
			   ,[EXP_GRP_NM]
			   ,[LR_IND]
			   ,[CGD_AMT]
			   ,[ICD_IND]
			   ,[GCN]
			   ,[CYC_NBR]
			   ,[PROD_MKT_N]
			   ,[MBR_ST_ADD]
			   ,[MBR_AGE]
			   ,[LIN_BUS_CD]
			   ,[ORIG_PR_DT]
			   ,[PRV_AC_ASG]
			   ,[SRVC_IN_DT]
			   ,[REL_LINE_N]
			   ,[BASIC_DAYS]
			   ,[PLUS_DAYS]
			   ,[MJR_DI_CAT]
			   ,[SRV_CD_SFX]
			   ,[PROC_DESC]
			   ,[OTH_TY]
			   ,[TYPE_2]
			   ,[ANES_MIN_Q]
			   ,[UCR_AMT]
			   ,[OTH_I_CV_A]
			   ,[OTH_INS]
			   ,[COINS_AMT]
			   ,[SRC_EN_CD]
			   ,[FEE_SCHED]
			   ,[POT_DESC]
			   ,[SRV_TY_DES]
			   ,[SRV_SP_DES]
			   ,[SRV_PRV_TI]
			   ,[PRV_PAR_CD]
			   ,[VOUCH_CD]
			   ,[ORI_PE_CD]
			   ,[ORI_PE_DT]
			   ,[PP_PRV_IND]
			   ,[BANK_CD]
			   ,[CT_CIS_SID]
			   ,[REFLM_N]
			   ,[SRV_CD_TYP]
			   ,[PRES_ID]
			   ,[MBR_REL]
			   ,[MBR_GEN]
			   ,[AMT_TO_DE]
			   ,[NPI]
			   ,[CLMSRC_NPI]
			   ,[CMS_POT]
			   ,[NTWK]
			   ,[SUPNWNBR]
			   ,[CTRT_CATNM]
			   ,[ECN_CLM]
			   ,[ECN_CLMEXP]
			   ,[ECF_ECN_ID]
			   ,[UNQ_FIELDS]
			   ,[filename]
			   ,[FileDate]
			   ,[LoadDateTime]
			   ,[RecordNo])
	SELECT [EXP_GRP_NO]
		  ,[PYMT_CTRCT]
		  ,[CLM_DEC_CD]
		  ,[IPA_LN_DEC]
		  ,[ADJ_LN_DEC]
		  ,[ADJUSTR_ID]
		  ,[FUND_TYPE]
		  ,[FUND_NBR]
		  ,[LEDGER]
		  ,[CLAIM_NBR]
		  ,[CLAIM_TYPE]
		  ,[OVERRIDE]
		  ,[PAYEE_CODE]
		  ,[RFR_ATHNBR]
		  ,[SUB_ID]
		  ,[SUB_SUF]
		  ,[SUB_DOB]
		  ,[SUB_NAME]
		  ,[PAT_NAME]
		  ,[PAT_SEX]
		  ,[PAT_DOB]
		  ,[PAT_REL]
		  ,[PAT_ACCT]
		  ,[ADM_SRCE]
		  ,[ADM_DATE]
		  ,[ADM_HOUR]
		  ,[ADM_AUTH]
		  ,[ADM_DX]
		  ,[DISCHRG_DT]
		  ,[DISCHRG_ST]
		  ,[NDC_NBR]
		  ,[RX_QTY]
		  ,[DEA_NBR]
		  ,[PROC_DATE]
		  ,[SUSP_DATE]
		  ,[ACT_DATE]
		  ,[RWORK_DATE]
		  ,[RESP_DATE]
		  ,[CHECK_DATE]
		  ,[CHECK_NBR]
		  ,[CUS_GRP]
		  ,[CUS_CL]
		  ,[PRIM_PROV]
		  ,[SERV_PROV]
		  ,[SPROV_NAME]
		  ,[SPROV_SPEC]
		  ,[SPROV_ADR1]
		  ,[SPROV_ADR2]
		  ,[SPROV_ADR3]
		  ,[SPROV_ADR4]
		  ,[SPROV_CITY]
		  ,[SPROV_STAT]
		  ,[SPROV_ZIP]
		  ,[REFER_PROV]
		  ,[ATEND_PROV]
		  ,[FACILITY]
		  ,[DIAG_PTR]
		  ,[DIAG_CODE1]
		  ,[DIAG_CODE2]
		  ,[DIAG_CODE3]
		  ,[DIAG_CODE4]
		  ,[DIAG_CODE5]
		  ,[DIAG_CODE6]
		  ,[DIAG_CODE7]
		  ,[DIAG_CODE8]
		  ,[DIAG_CODE9]
		  ,[CAUSE_CD1]
		  ,[CAUSE_CD2]
		  ,[CAUSE_CD3]
		  ,[CAUSE_CD4]
		  ,[CAUSE_CD5]
		  ,[CAUSE_CD6]
		  ,[CAUSE_CD7]
		  ,[CAUSE_CD8]
		  ,[CAUSE_CD9]
		  ,[PROC_CODE1]
		  ,[PROC_CODE2]
		  ,[PROC_CODE3]
		  ,[PROC_CODE4]
		  ,[PROC_CODE5]
		  ,[PROC_CODE6]
		  ,[PROC_CDDT1]
		  ,[PROC_CDDT2]
		  ,[PROC_CDDT3]
		  ,[PROC_CDDT4]
		  ,[PROC_CDDT5]
		  ,[PROC_CDDT6]
		  ,[SRV_FRM_DT]
		  ,[SERV_TO_DT]
		  ,[DRG_NBR]
		  ,[SERV_UNITS]
		  ,[POT]
		  ,[CHRG_AMT]
		  ,[CALC_AMT]
		  ,[RWORK_AMT]
		  ,[ACT_PD_AMT]
		  ,[BSCHED_AMT]
		  ,[BASC_WDPAY]
		  ,[BEN_SAV]
		  ,[BASIC_DED]
		  ,[BASIC_AMT]
		  ,[DISCNT_AMT]
		  ,[OI_AMT]
		  ,[SUB_RSPAMT]
		  ,[WHOLD_AMT]
		  ,[MCAID_NBR]
		  ,[DCN_NBR]
		  ,[COB_IND]
		  ,[TYPE]
		  ,[TYPE_MOD]
		  ,[HCFA_SV_TY]
		  ,[EX_CODE]
		  ,[DED_SAT]
		  ,[BDED_SAT]
		  ,[CPT4_MOD]
		  ,[AIB_APPL]
		  ,[ADJ_REASON]
		  ,[CLM_CHK]
		  ,[MULT_SRG]
		  ,[WCOMP_NBR]
		  ,[OOP_SAT]
		  ,[OOP_APPL]
		  ,[FUND_EXP]
		  ,[FND_EXP_DT]
		  ,[DISC_GRPR]
		  ,[DISC_GPRNM]
		  ,[PRD_PLNOPT]
		  ,[PLAN_LOB]
		  ,[AFF_CCOM1]
		  ,[AFF_CCOM2]
		  ,[ADJ_CLM_ID]
		  ,[ADJ_CCOM1]
		  ,[ADJ_CCOM2]
		  ,[AFF_LCOM1]
		  ,[AFF_LCOM2]
		  ,[ADJ_LNE_ID]
		  ,[ADJ_LCOM1]
		  ,[ADJ_LCOM2]
		  ,[TRAN_CODE]
		  ,[GK_PROV_ID]
		  ,[GK_PROV_NM]
		  ,[DPNT_CODE]
		  ,[BILL_TYPE]
		  ,[RVS_UNITS]
		  ,[PROV_TYPE]
		  ,[MBR_PID]
		  ,[GK_CTRCT]
		  ,[PE_DATE]
		  ,[FUN_PER_BD]
		  ,[FUN_PER_ED]
		  ,[CLAIM_IND]
		  ,[CAUSE_CD]
		  ,[PRES_NBR]
		  ,[GEN_IND]
		  ,[REFIL_STAT]
		  ,[PRTB_D_IND]
		  ,[DEN_RIDR]
		  ,[VIS_RIDR]
		  ,[HER_RIDR]
		  ,[PROCSR_ID]
		  ,[PHAR_NPI]
		  ,[PHAR_NM]
		  ,[PYT_GRP_NO]
		  ,[PYT_GRP_NM]
		  ,[EXP_GRP_NM]
		  ,[LR_IND]
		  ,[CGD_AMT]
		  ,[ICD_IND]
		  ,[GCN]
		  ,[CYC_NBR]
		  ,[PROD_MKT_N]
		  ,[MBR_ST_ADD]
		  ,[MBR_AGE]
		  ,[LIN_BUS_CD]
		  ,[ORIG_PR_DT]
		  ,[PRV_AC_ASG]
		  ,[SRVC_IN_DT]
		  ,[REL_LINE_N]
		  ,[BASIC_DAYS]
		  ,[PLUS_DAYS]
		  ,[MJR_DI_CAT]
		  ,[SRV_CD_SFX]
		  ,[PROC_DESC]
		  ,[OTH_TY]
		  ,[TYPE_2]
		  ,[ANES_MIN_Q]
		  ,[UCR_AMT]
		  ,[OTH_I_CV_A]
		  ,[OTH_INS]
		  ,[COINS_AMT]
		  ,[SRC_EN_CD]
		  ,[FEE_SCHED]
		  ,[POT_DESC]
		  ,[SRV_TY_DES]
		  ,[SRV_SP_DES]
		  ,[SRV_PRV_TI]
		  ,[PRV_PAR_CD]
		  ,[VOUCH_CD]
		  ,[ORI_PE_CD]
		  ,[ORI_PE_DT]
		  ,[PP_PRV_IND]
		  ,[BANK_CD]
		  ,[CT_CIS_SID]
		  ,[REFLM_N]
		  ,[SRV_CD_TYP]
		  ,[PRES_ID]
		  ,[MBR_REL]
		  ,[MBR_GEN]
		  ,[AMT_TO_DE]
		  ,[NPI]
		  ,[CLMSRC_NPI]
		  ,[CMS_POT]
		  ,[NTWK]
		  ,[SUPNWNBR]
		  ,[CTRT_CATNM]
		  ,[ECN_CLM]
		  ,[ECN_CLMEXP]
		  ,[ECF_ECN_ID]
		  ,[UNQ_FIELDS]
		  ,[filename]
		  ,[FileDate]
		  ,[LoadDateTime]
		  ,[RecordNo]
	  FROM [KPI_ENGINE_SOURCE].[UHN].[HUMANA_RECLMEXP]

	-- Delete data from [KPI_ENGINE_SOURCE].[UHN].[HUMANA_RECLMEXP]

	Delete from [KPI_ENGINE_SOURCE].[UHN].[HUMANA_RECLMEXP];


	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP_ARCHIVE OFF;

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
