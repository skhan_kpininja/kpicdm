SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




CREATE PROC [dbo].[Load_02_HUMANA_MCFIELDS]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update FileDate column based on Filename

	update KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF set FileDate=convert(varchar,substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%',filename),8),112)
	  
	
	-- Script to Move data from KPI_Engine_Source.UHN.HUMANA_REHCF  to KPI_ENGINE_STAGING.dbo.STAGING_MCFIELDS

	Insert into KPI_ENGINE_STAGING.dbo.STAGING_MCFIELDS(RunDate,CONTRACT,PBP_Number,MEM_MEDICARE,MEMBER_ID,Premium_LIS_Amount,Hospice,OREC,LTI,PayDate,MC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select RunDate,CONTRACT,PBP_Number,MEM_MEDICARE,MEMBER_ID,Premium_LIS_Amount,Hospice,OREC,LTI,PayDate,MC_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			DENSE_RANK() over(partition by MBR_UMID,Pay_date,RECORD_TY order by FileDate Desc,RecordNo Desc) as rnk,
			convert(varchar,cast(RPT_PE as date),23) as RunDate
			,MCO_CTRCT as CONTRACT
			,PBP as PBP_Number
			,MCARE_ID as MEM_MEDICARE
			,MBR_UMID as MEMBER_ID
			,Cast(replace(LIS_PRMAMT,'+','') as float) as Premium_LIS_Amount
			,Case	
				When HSPC_IND='Y' then 1
				else 0
			end as Hospice
			,OREC_CD as OREC
			,Case	
				When LTI_FLAG='Y' then 1
				else 0
			end as LTI
			,Pay_date as PayDate
			,'HUMANA' as MC_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF
	)t1 where rnk=1


	-- Move data from KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF_ARCHIVE ON;  

	INSERT INTO [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REHCF_ARCHIVE]
           ([RPT_PE]
           ,[RECORD_TY]
           ,[FIN_LEDGR]
           ,[MAIL_PRVID]
           ,[CENTER_NM]
           ,[FND_BEG_DT]
           ,[GRP_IPA_ID]
           ,[GRP_NAME]
           ,[GRP_FIN_LR]
           ,[CUST_ID]
           ,[SUB_GRP_ID]
           ,[MBR_ID]
           ,[MBR_NAME]
           ,[PAY_DATE]
           ,[FND_END_DT]
           ,[GK_ID]
           ,[CUST_LEDGR]
           ,[PROD_LOB]
           ,[PROD_PLAN]
           ,[PROD_OPT]
           ,[CYC_NO]
           ,[CTR_GRP_ID]
           ,[DOB_DATE]
           ,[MBR_SEX]
           ,[MBR_STATE]
           ,[MBR_CNTY]
           ,[PIPP_SCORE]
           ,[PRTA_AMT]
           ,[PRTB_AMT]
           ,[PRTD_AMT]
           ,[TOT_AMT]
           ,[ADJ_A_AMT]
           ,[ADJ_B_AMT]
           ,[ADJ_D_AMT]
           ,[ADJ_T_AMT]
           ,[HCC_A]
           ,[HCC_B]
           ,[HCC_D]
           ,[DFLT_IND]
           ,[LAG_IND]
           ,[L_INC_IND]
           ,[L_INC_SCR]
           ,[DIR_SUBAMT]
           ,[REI_SUBAMT]
           ,[LIS_CSRAMT]
           ,[LIS_PRMAMT]
           ,[MBR_UMID]
           ,[SEG_ID]
           ,[MCO_CTRCT]
           ,[MCARE_ID]
           ,[AGE_GROUP]
           ,[OOA_IND]
           ,[PRTA_IND]
           ,[PRTB_IND]
           ,[HSPC_IND]
           ,[ESRD_IND]
           ,[AGED_MSP]
           ,[INST_IND]
           ,[NHC]
           ,[MEDI_FLAG]
           ,[LTI_FLAG]
           ,[MCAID_IND]
           ,[ESRD_MSP]
           ,[CURR_MEDI]
           ,[RAAG]
           ,[PRDIB]
           ,[PBP]
           ,[RACE_CODE]
           ,[RA_FACT]
           ,[FRAIL_IND]
           ,[OREC_CD]
           ,[ENROLL_S]
           ,[EGHP_IND]
           ,[INST_MULT]
           ,[PRTD_INST]
           ,[BRD_DISC_A]
           ,[DUAL_ELIG]
           ,[PD_RA_FAC]
           ,[ACC_A_AMT]
           ,[ACC_B_AMT]
           ,[ACC_D_AMT]
           ,[ACC_T_AMT]
           ,[MA_RISK_NB]
           ,[RX_RISK_NB]
           ,[FileName]
           ,[FileDate]
		   ,[RecordNo]
           ,[LoadDateTime]
           ,[LOW_SUBAMT])
	SELECT [RPT_PE]
      ,[RECORD_TY]
      ,[FIN_LEDGR]
      ,[MAIL_PRVID]
      ,[CENTER_NM]
      ,[FND_BEG_DT]
      ,[GRP_IPA_ID]
      ,[GRP_NAME]
      ,[GRP_FIN_LR]
      ,[CUST_ID]
      ,[SUB_GRP_ID]
      ,[MBR_ID]
      ,[MBR_NAME]
      ,[PAY_DATE]
      ,[FND_END_DT]
      ,[GK_ID]
      ,[CUST_LEDGR]
      ,[PROD_LOB]
      ,[PROD_PLAN]
      ,[PROD_OPT]
      ,[CYC_NO]
      ,[CTR_GRP_ID]
      ,[DOB_DATE]
      ,[MBR_SEX]
      ,[MBR_STATE]
      ,[MBR_CNTY]
      ,[PIPP_SCORE]
      ,[PRTA_AMT]
      ,[PRTB_AMT]
      ,[PRTD_AMT]
      ,[TOT_AMT]
      ,[ADJ_A_AMT]
      ,[ADJ_B_AMT]
      ,[ADJ_D_AMT]
      ,[ADJ_T_AMT]
      ,[HCC_A]
      ,[HCC_B]
      ,[HCC_D]
      ,[DFLT_IND]
      ,[LAG_IND]
      ,[L_INC_IND]
      ,[L_INC_SCR]
      ,[DIR_SUBAMT]
      ,[REI_SUBAMT]
      ,[LIS_CSRAMT]
      ,[LIS_PRMAMT]
      ,[MBR_UMID]
      ,[SEG_ID]
      ,[MCO_CTRCT]
      ,[MCARE_ID]
      ,[AGE_GROUP]
      ,[OOA_IND]
      ,[PRTA_IND]
      ,[PRTB_IND]
      ,[HSPC_IND]
      ,[ESRD_IND]
      ,[AGED_MSP]
      ,[INST_IND]
      ,[NHC]
      ,[MEDI_FLAG]
      ,[LTI_FLAG]
      ,[MCAID_IND]
      ,[ESRD_MSP]
      ,[CURR_MEDI]
      ,[RAAG]
      ,[PRDIB]
      ,[PBP]
      ,[RACE_CODE]
      ,[RA_FACT]
      ,[FRAIL_IND]
      ,[OREC_CD]
      ,[ENROLL_S]
      ,[EGHP_IND]
      ,[INST_MULT]
      ,[PRTD_INST]
      ,[BRD_DISC_A]
      ,[DUAL_ELIG]
      ,[PD_RA_FAC]
      ,[ACC_A_AMT]
      ,[ACC_B_AMT]
      ,[ACC_D_AMT]
      ,[ACC_T_AMT]
      ,[MA_RISK_NB]
      ,[RX_RISK_NB]
      ,[FileName]
      ,[FileDate]
      ,[RecordNo]
      ,[LoadDateTime]
      ,[LOW_SUBAMT]
  FROM [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REHCF]



		
	
	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].[UHN].[HUMANA_REHCF]

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF_ARCHIVE OFF;  

	
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
