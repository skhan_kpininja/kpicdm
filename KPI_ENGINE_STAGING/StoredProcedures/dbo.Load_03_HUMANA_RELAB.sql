SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROC [dbo].[Load_03_HUMANA_RELAB]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY
	
	-- Update FileDate column based on Filename

	update KPI_ENGINE_SOURCE.UHN.HUMANA_RELAB set FileDate=concat(convert(varchar,substring(filename,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',filename),6),112),'01');
	  
	
	-- Script to Move data from KPI_Engine_Source.UHN.HUMANA_RELAB  to KPI_ENGINE_STAGING.dbo.STAGING_LAB

	


	Insert into KPI_ENGINE_STAGING.dbo.STAGING_LAB(CollectionDateTime,OrderDate,TestCode,Criticality,Comments,ResultId,value,PerformingLab,ResultName,ResultCode,MEMBER_ID,ReferenceRange,OrderingProviderNPI,Unit,ResultDate,Specimen,LAB_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName)
	select CollectionDateTime,OrderDate,TestCode,Criticality,Comments,ResultId,value,PerformingLab,ResultName,ResultCode,MEMBER_ID,ReferenceRange,OrderingProviderNPI,Unit,ResultDate,Specimen,LAB_DATA_SRC,ROOT_COMPANIES_ID,FileDate,FileName from(
		select 
			Dense_rank() over(partition by MBR_PID,SER_DTE_KE,LAB_RESULT,COMPON_CPT,LOINC_CODE order by FileDate desc,RecordNo desc) as rnk,
			case when COLLEC_DAT!='000000000000000' then convert(varchar,cast(STUFF(STUFF(STUFF(convert(char(14),COLLEC_DAT),13,0,':'),11,0,':'),9,0,' ') as datetime),120) end as CollectionDateTime
			,case when COLLEC_DAT!='000000000000000' then convert(varchar,cast(STUFF(STUFF(STUFF(convert(char(14),COLLEC_DAT),13,0,':'),11,0,':'),9,0,' ') as datetime),120) end as OrderDate
			,COMPON_CPT as TestCode
			,LAB_RES_AB as Criticality
			,Concat(LAB_COMM_1,',',LAB_COMM_2,',',LAB_COMM_3,',',LAB_COMM_4,',',LAB_COMM_5,',',LAB_COMM_6,',',LAB_COMM_7) as Comments
			,LAB_RESULT as ResultId
			,LAB_VALUE as value
			,LAB_VEN_NA as PerformingLab
			,LC_SRT_NM as ResultName
			,LOINC_CODE as ResultCode
			,MBR_PID as MEMBER_ID
			,NORMAL_ALP as ReferenceRange
			,ISNULL(ORD_PROV_N,'') as OrderingProviderNPI
			,RESUL_UNIT as Unit	
			,convert(varchar,cast(SER_DTE_KE as date),23) as ResultDate
			,SPECI_SOUR as Specimen
			,'HUMANA' as LAB_DATA_SRC
			,159 as ROOT_COMPANIES_ID
			,FileDate
			,FileName
		from KPI_ENGINE_SOURCE.UHN.HUMANA_RELAB
	)t1 where rnk=1

	-- Move data from KPI_ENGINE_SOURCE.UHN.HUMANA_REHCF to Archive

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_RELAB_ARCHIVE ON;  

	INSERT INTO KPI_ENGINE_SOURCE.[UHN].[HUMANA_RELAB_ARCHIVE]
           ([RPT_PE]
           ,[MAIL_PRVID]
           ,[CENTER_NM]
           ,[FND_BEG_DT]
           ,[FND_END_DT]
           ,[MBR_PID]
           ,[FIN_LEDGR]
           ,[GRP_IPA_ID]
           ,[GRP_NAME]
           ,[GRP_FIN_LR]
           ,[CUST_ID]
           ,[MEMBER_SSN]
           ,[GK_ID]
           ,[CUST_LEDGR]
           ,[MCO_CTRCT]
           ,[MCARE_ID]
           ,[LAST_NAME]
           ,[FIRST_NAME]
           ,[INIT]
           ,[BIRTH_DATE]
           ,[SEX_CODE]
           ,[LAB_RESULT]
           ,[SER_DTE_KE]
           ,[SRC_PLATFO]
           ,[SRC_MBR_ID]
           ,[LOINC_CODE]
           ,[LAB_VALUE]
           ,[LAB_VEN_TX]
           ,[LAB_VEN_NA]
           ,[COLLEC_DAT]
           ,[LAB_RES_CO]
           ,[RESUL_UNIT]
           ,[PROCES_DAT]
           ,[RECIEV_DAT]
           ,[LAB_RES_AB]
           ,[NORMAL_ALP]
           ,[COMPON_CPT]
           ,[LOCAL_LAB]
           ,[LAB_PARTNE]
           ,[HOS_INP_IN]
           ,[PATI_RACE]
           ,[SPECI_SOUR]
           ,[TEST_METHO]
           ,[ORD_PROV_N]
           ,[CLASS_DESC]
           ,[HEDIS_TYPE]
           ,[LC_SRT_NM]
           ,[LAB_COMM_1]
           ,[LAB_COMM_2]
           ,[LAB_COMM_3]
           ,[LAB_COMM_4]
           ,[LAB_COMM_5]
           ,[LAB_COMM_6]
           ,[LAB_COMM_7]
		   ,[PerformingLab]
           ,[filename]
           ,[FileDate]
		   ,[RecordNo]
           ,[LoadDateTime])
	SELECT [RPT_PE]
      ,[MAIL_PRVID]
      ,[CENTER_NM]
      ,[FND_BEG_DT]
      ,[FND_END_DT]
      ,[MBR_PID]
      ,[FIN_LEDGR]
      ,[GRP_IPA_ID]
      ,[GRP_NAME]
      ,[GRP_FIN_LR]
      ,[CUST_ID]
      ,[MEMBER_SSN]
      ,[GK_ID]
      ,[CUST_LEDGR]
      ,[MCO_CTRCT]
      ,[MCARE_ID]
      ,[LAST_NAME]
      ,[FIRST_NAME]
      ,[INIT]
      ,[BIRTH_DATE]
      ,[SEX_CODE]
      ,[LAB_RESULT]
      ,[SER_DTE_KE]
      ,[SRC_PLATFO]
      ,[SRC_MBR_ID]
      ,[LOINC_CODE]
      ,[LAB_VALUE]
      ,[LAB_VEN_TX]
      ,[LAB_VEN_NA]
      ,[COLLEC_DAT]
      ,[LAB_RES_CO]
      ,[RESUL_UNIT]
      ,[PROCES_DAT]
      ,[RECIEV_DAT]
      ,[LAB_RES_AB]
      ,[NORMAL_ALP]
      ,[COMPON_CPT]
      ,[LOCAL_LAB]
      ,[LAB_PARTNE]
      ,[HOS_INP_IN]
      ,[PATI_RACE]
      ,[SPECI_SOUR]
      ,[TEST_METHO]
      ,[ORD_PROV_N]
      ,[CLASS_DESC]
      ,[HEDIS_TYPE]
      ,[LC_SRT_NM]
      ,[LAB_COMM_1]
      ,[LAB_COMM_2]
      ,[LAB_COMM_3]
      ,[LAB_COMM_4]
      ,[LAB_COMM_5]
      ,[LAB_COMM_6]
      ,[LAB_COMM_7]
	  ,PerformingLab
      ,[filename]
      ,[FileDate]
      ,[RecordNo]
      ,[LoadDateTime]
  FROM KPI_ENGINE_SOURCE.[UHN].[HUMANA_RELAB]

	-- Delete data from UHN.HUMANA_REMBX post data move

	delete from [KPI_ENGINE_SOURCE].[UHN].[HUMANA_RELAB]

	SET IDENTITY_INSERT KPI_ENGINE_SOURCE.UHN.HUMANA_RELAB_ARCHIVE OFF;  

	
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
