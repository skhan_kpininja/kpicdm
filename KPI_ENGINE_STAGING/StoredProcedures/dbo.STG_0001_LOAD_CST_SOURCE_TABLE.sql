SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE procedure dbo.[STG_0001_LOAD_CST_SOURCE_TABLE]
as
begin


exec ('Use KPI_ENGINE_SOURCE

if Not exists(select 1 from sys.schemas where name=''CST'')
Begin
	exec(''CREATE schema CST'')
End

if Not exists(select 1 from sys.procedures where name =''SP_KPI_DROPTABLE'')
Begin
EXEC (''
CREATE PROCEDURE [dbo].[SP_KPI_DROPTABLE]
(@TABLENAME VARCHAR(100)) AS

DECLARE @SQL VARCHAR(1000)
SET @SQL= ''''IF EXISTS (SELECT * FROM ''''
+case when LEFT(@TABLENAME,1)=''''#'''' THEN ''''TEMPDB.DBO.'''' ELSE '''''''' END+
''''sysobjects WHERE id = OBJECT_ID(N''''''''''''+
+case when LEFT(@TABLENAME,1)=''''#'''' THEN ''''TEMPDB.DBO.'''' ELSE '''''''' END+
@TABLENAME+'''''''''''') AND type in (N''''''''U''''''''))
DROP TABLE ''''+@TABLENAME
EXEC (@SQL)'')

End')

exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF1_CLAIMHEADER'
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF2_REVENUECODE'	
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF3_PROCCODE'	
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF4_DIAGCODE'		
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF5_Physician'	
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF6_DME'	
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLF7_PART_D'
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLFA_ENHANCEMENT'
exec KPI_ENGINE_SOURCE.dbo.SP_KPI_DROPTABLE 'cst.UHN_CCLFB_ENHANCEMENT'


	

Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF1_CLAIMHEADER	from KPI_ENGINE_SOURCE.dbo.UHN_CCLF1_ClaimHeader A LEFT JOIN  KPI_ENGINE_SOURCE.dbo.UHN_CCLF1_ClaimHeader B ON 1=0 ;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF2_REVENUECODE	from KPI_ENGINE_SOURCE.dbo.UHN_CCLF2_REVENUECODE A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLF2_REVENUECODE B on 1=0;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF3_PROCCODE	from KPI_ENGINE_SOURCE.dbo.UHN_CCLF3_PROCCODE A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLF3_PROCCODE B on 1=0;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF4_DIAGCODE	from KPI_ENGINE_SOURCE.dbo.UHN_CCLF4_DIAGCODE A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLF4_DIAGCODE B on 1=0;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF5_Physician	from KPI_ENGINE_SOURCE.dbo.UHN_CCLF5_Physician A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLF5_Physician B on 1=0;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF6_DME			from KPI_ENGINE_SOURCE.dbo.UHN_CCLF6_DME A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLF6_DME B on 1=0;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLF7_PART_D		from KPI_ENGINE_SOURCE.dbo.UHN_CCLF7_PART_D A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLF7_PART_D B on 1=0;

if exists(select * from KPI_ENGINE_SOURCE.information_schema.tables where table_name='UHN_CCLFB_ENHANCEMENT')
Begin
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLFA_ENHANCEMENT	from KPI_ENGINE_SOURCE.dbo.UHN_CCLFA_ENHANCEMENT A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLFA_ENHANCEMENT B ON 1=0;
Select top 0 A.* into KPI_ENGINE_SOURCE.cst.UHN_CCLFB_ENHANCEMENT	from KPI_ENGINE_SOURCE.dbo.UHN_CCLFB_ENHANCEMENT A LEFT JOIN KPI_ENGINE_SOURCE.dbo.UHN_CCLFB_ENHANCEMENT B ON 1=0;
END


insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF1_CLAIMHEADER with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF1_ClaimHeader;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF2_REVENUECODE with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF2_REVENUECODE;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF3_PROCCODE	with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF3_PROCCODE;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF4_DIAGCODE	with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF4_DIAGCODE;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF5_Physician	with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF5_Physician;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF6_DME			with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF6_DME;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLF7_PART_D		with (TABLOCK) select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLF7_PART_D;

if exists(select * from KPI_ENGINE_SOURCE.information_schema.tables where table_name='UHN_CCLFB_ENHANCEMENT')
Begin
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLFA_ENHANCEMENT with (TABLOCK)	select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLFA_ENHANCEMENT;
insert into KPI_ENGINE_SOURCE.cst.UHN_CCLFB_ENHANCEMENT with (TABLOCK) 	select * from KPI_ENGINE_SOURCE.dbo.UHN_CCLFB_ENHANCEMENT;
END


--CCLF1
;with cclf1 as (
select CUR_CLM_UNIQ_ID,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF1_CLAIMHEADER)
delete from cclf1 where Row_Num>1;


--CCLF2
with cclf2 as (
select CUR_CLM_UNIQ_ID,convert(int,CLM_LINE_NUM)CLM_LINE_NUM,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,convert(int,CLM_LINE_NUM) order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF2_REVENUECODE)
delete from cclf2 where Row_Num>1;

--CCLF3
with cclf3 as (
select CUR_CLM_UNIQ_ID,convert(int,CLM_VAL_SQNC_NUM)CLM_LINE_NUM,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,convert(int,CLM_VAL_SQNC_NUM) order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF3_PROCCODE)
delete from cclf3 where Row_Num>1;

--CCLF4
with cclf4 as (
select CUR_CLM_UNIQ_ID,convert(int,CLM_VAL_SQNC_NUM)CLM_LINE_NUM,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,convert(int,CLM_VAL_SQNC_NUM),CLM_PROD_TYPE_CD order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF4_DIAGCODE)
delete from cclf4 where Row_Num>1;

--CCLF5
with cclf5 as (
select CUR_CLM_UNIQ_ID,convert(int,clm_line_num)CLM_LINE_NUM,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,convert(int,clm_line_num) order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF5_Physician )
delete from cclf5 where Row_Num>1;

--CCLF6
with cclf6 as (
select CUR_CLM_UNIQ_ID,convert(int,clm_line_num)CLM_LINE_NUM,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,convert(int,clm_line_num) order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF6_DME )
delete from cclf6 where Row_Num>1;

--cclf7
with cclf7 as (
select CUR_CLM_UNIQ_ID,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,CLM_LINE_NDC_CD order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLF7_PART_D)
delete from cclf7 where Row_Num>1

if exists(select * from KPI_ENGINE_SOURCE.information_schema.tables where table_name='UHN_CCLFA_ENHANCEMENT')
Begin
--CCLFA
;with cclfa as (
select CUR_CLM_UNIQ_ID,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.cst.UHN_CCLFA_ENHANCEMENT)
delete from cclfa where Row_Num>1;
End

if exists(select * from KPI_ENGINE_SOURCE.information_schema.tables where table_name='UHN_CCLFB_ENHANCEMENT')
Begin
--CCLFB
;with cclfb as (
select CUR_CLM_UNIQ_ID,convert(int,clm_line_num)CLM_LINE_NUM,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+1,7) FilterString,convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) YearMo,FTP_FILE_NAME,load_date 
,ROW_NUMBER()over(partition by CUR_CLM_UNIQ_ID,convert(int,clm_line_num) order by convert(char(6),CONVERT(date,SUBSTRING(FTP_FILE_NAME,CHARINDEX('.D',FTP_FILE_NAME)+2,6)),112) desc) Row_Num
from KPI_ENGINE_SOURCE.CST.UHN_CCLFB_ENHANCEMENT)
delete from cclfb where Row_Num>1;
End

End
GO
