SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE  PROCEDURE [DBO].[STG_0400_FINAL_STAGING_LOAD_MSSP]

AS
BEGIN
--SET @schema = 'DBO'

EXEC SP_KPI_DROPTABLE '#temp_tables'


CREATE TABLE #temp_tables
(
table_name VARCHAR(50),
tmp_table_name VARCHAR(50),
)


INSERT INTO #temp_tables(table_name, tmp_table_name)
SELECT table_name = 'DBO'+'.'+'STAGING_ENROLLMENT',	tmp_table_name = 'DBO'+'.'+'TEMP_STAGING_ENROLLMENT' UNION ALL
SELECT table_name = 'DBO'+'.'+'STAGING_MEMBER',		tmp_table_name = 'DBO'+'.'+'TEMP_STAGING_MEMBER'     UNION ALL
SELECT table_name = 'DBO'+'.'+'STAGING_CLAIMLINE',	tmp_table_name = 'DBO'+'.'+'TEMP_STAGING_CLAIMLINE'	 UNION ALL
SELECT table_name = 'DBO'+'.'+'STAGING_PROVIDER',	tmp_table_name = 'DBO'+'.'+'TEMP_STAGING_PROVIDER'	 --UNION ALL
--SELECT table_name = 'DBO'+'.'+'STAGING_BENEFIT',	tmp_table_name = 'DBO'+'.'+'TEMP_STAGING_BENEFIT'		

--SELECT * FROM #temp_tables

DECLARE @stg_table VARCHAR(50)
		,@tmp_stg_table VARCHAR(50)
		,@stg_col_list VARCHAR(MAX)
		,@query VARCHAR(MAX)
		
WHILE (SELECT TOP 1 1 FROM #temp_tables) IS NOT NULL
BEGIN
	SELECT TOP 1 @stg_table = table_name,	@tmp_stg_table = tmp_table_name FROM #temp_tables


	print(@stg_table)
	print(@tmp_stg_table)

	-- Truncate Staging tables
	--SET @query = 'TRUNCATE TABLE '+ @stg_table
	--EXEC(@query)



	SET @query = ''
	SELECT @stg_col_list = (SELECT stuff((SELECT ', ' + cast(COLUMN_NAME as varchar(255))
											FROM INFORMATION_SCHEMA.COLUMNS 
											WHERE TABLE_NAME = REPLACE(@stg_table, 'DBO'+'.','') AND COLUMN_NAME not like 'ST_%_ID'
											AND TABLE_SCHEMA = 'DBO'
											FOR XML PATH('')
											), 1, 2, ''));

	--SELECT @stg_col_list
	--PRINT(LEN(@stg_col_list))

	SET @query = ''


	SET @query = N'--SET IDENTITY_INSERT '+@stg_table+' ON;
					INSERT INTO '+ @stg_table + '('+ @stg_col_list + ')
				'
	SET @query = @query+'SELECT '+ @stg_col_list + ' FROM '+ @tmp_stg_table+'
						
						--SET IDENTITY_INSERT '+@stg_table+' OFF;

						'
	EXEC(@query)  ------Commented
	Print @query


	SET @query = '--SET IDENTITY_INSERT '+@stg_table+' OFF;'
	EXEC(@query)  ------Commented
	Print @query


	--SET @query = 'SELECT COUNT(1) FROM '+ @stg_table

	--EXEC(@query)

	DELETE FROM #temp_tables WHERE table_name = @stg_table

END
END

GO
