SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [dbo].[STG_0400_LOAD_STAGING_PROCEDURE_MSSP]
AS
BEGIN

INSERT INTO [KPI_ENGINE_STAGING].[dbo].[STAGING_PROCEDURES]
(
	   [CLAIM_ID]
      ,[SV_LINE]
      ,[MEMBER_ID]
      ,[PROC_START_DATE]
      ,[PROC_END_DATE]
      ,[PROC_CODE]
      ,[PROC_DESC]
      ,[PROC_CODE_TYPE]
      ,[MOD_1]
      ,[MOD_2]
      ,[MOD_3]
      ,[MOD_4]
      ,[SERVICE_UNIT]
      ,[ICDPCS_CODE]
      ,[PROC_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[LOAD_DATE_TIME]
      ,[Filename]
      ,[FileDate]
)
SELECT Distinct
	   [CLAIM_ID]
      ,[SV_LINE]
      ,[MEMBER_ID]
      ,FROM_DATE
      ,TO_DATE
      ,[PROC_CODE]
      ,'' [PROC_DESC]
      ,'' [PROC_CODE_TYPE]
      ,[CPT_MOD_1]
      ,CPT_MOD_2
      ,NULL AS [MOD_3]
      ,NULL AS [MOD_4]
      ,[SV_UNITS]
      ,NULL [ICDPCS_CODE]
      ,'MSSP' [PROC_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,Filedate
      ,[Filename]
      ,[FileDate]
FROM DBO.TEMP_STAGING_CLAIMLINE WHERE  ISNULL(PROC_CODE,'')<>''


INSERT INTO [KPI_ENGINE_STAGING].[dbo].[STAGING_PROCEDURES](CLAIM_ID,
	SV_LINE,
	Member_ID,
	PROC_START_DATE,
	ICDPCS_CODE,
	PROC_DESC,
	PROC_SEQ,
	PROC_CODE_TYPE,
	Filename,
	Filedate,
	ROOT_COMPANIES_ID,
	PROC_DATA_SRC)
Select
	CLAIM_ID,
	SV_LINE,
	Member_ID,
	PROC_START_DATE,
	ICDPCS_CODE,
	PROC_DESC,
	PROC_SEQ,
	PROC_CODE_TYPE,
	Filename,
	Filedate,
	ROOT_COMPANIES_ID,
	'MSSP' [PROC_DATA_SRC]
from
(
	Select
		row_number() over(Partition by coalesce(p.BENE_MBI_ID,b.MBI,BENE_HIC_NUM),CLM_PRCDR_PRFRM_DT,CLM_PRCDR_CD order by CLM_VAL_SQNC_NUM asc) as rn,
		CUR_CLM_UNIQ_ID as CLAIM_ID,
		CONVERT(INT,NULLIF(LTRIM(RTRIM(CLM_VAL_SQNC_NUM)),'~')) AS SV_LINE,
		coalesce(p.BENE_MBI_ID,b.MBI,BENE_HIC_NUM) as Member_ID,
		CLM_PRCDR_PRFRM_DT as PROC_START_DATE,
		CLM_PRCDR_CD as ICDPCS_CODE,
		r.description as PROC_DESC,
		CONVERT(INT,NULLIF(LTRIM(RTRIM(CLM_VAL_SQNC_NUM)),'~')) as PROC_SEQ,
		Case 
			when DGNS_PRCDR_ICD_IND=9 Then 'ICD-9'
			when DGNS_PRCDR_ICD_IND=0 Then 'ICD-10'
		end as PROC_CODE_TYPE,
		p.ftp_file_name AS Filename,
		p.Load_date AS Filedate,
		159 as ROOT_COMPANIES_ID
	From KPI_ENGINE_SOURCE.dbo.UHN_CCLF3_PROCCODE p
	Left Outer JOIN KPI_ENGINE_SOURCE.dbo.UHN_BOTC_ARCHIVED b on
		p.BENE_HIC_NUM=b.HICN
	Left Outer Join KPI_ENGINE.RFT.ICDPCSCODES r on
		p.CLM_PRCDR_CD=r.Code
)t1
Where
	rn=1


END
GO
