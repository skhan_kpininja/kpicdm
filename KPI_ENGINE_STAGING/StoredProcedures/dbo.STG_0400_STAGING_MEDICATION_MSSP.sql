SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE PROCEDURE [dbo].[STG_0400_STAGING_MEDICATION_MSSP]      
     
AS 
BEGIN
	
	SELECT *,ROW_NUMBER() OVER(PARTITION BY CLM_LINE_FROM_DT,PRVDR_SRVC_ID_QLFYR_CD,CLM_SRVC_PRVDR_GNRC_ID_NUM,CLM_DSPNSNG_STUS_CD,CLM_LINE_RX_SRVC_RFRNC_NUM,
	CLM_LINE_RX_FILL_NUM ORDER BY CLM_IDR_LD_DT desc,CLM_ADJSMT_TYPE_CD desc)ROWNUMBER 
	INTO #TEMP FROM KPI_ENGINE_SOURCE.DBO.UHN_CCLF7_PART_D

	DELETE FROM #TEMP WHERE ROWNUMBER<>1



	-----------------LOAD STAGING DATA--------------------------------------   

-- Part D file (Pharmacy claims)

	INSERT INTO [KPI_ENGINE_STAGING].[dbo].[STAGING_MEDICATION]
	(		    
		[CLAIM_ID]
      ,[MEMBER_ID]
      ,[FILL_DATE]
      ,[MEDICATION_NAME]
      ,[MEDICATION_CODE]
      ,[MEDICATION_CODE_TYPE]
      ,[QUANTITY]
      ,[MANUFACTURER]
      ,[MED_TYPE]
      ,[SUPPLY_DAYS]
      ,[REFILLS]
      ,[DAW]
      ,[PRESCRIPTION_DATE]
      ,[PRESC_NPI]
      ,[PRESC_NAME]
      ,[PHARM_ID]
      ,[PHARM_NAME]
      ,[PHARM_ADDR]
      ,[PHARM_CITY]
      ,[PHARM_ST]
      ,[PHARM_ZIP]
      ,[MED_ROUTE]
      ,[MED_SIG]
      ,[MED_FORM]
      ,[MED_STRENGTH]
      ,[MED_DOSE]
      ,[ROOT_COMPANIES_ID]
      ,[LOADDATETIME]
      ,[FileDate]
      ,[Filename]
      ,[THERAPEUTIC_CLASS]
      ,[MED_DATA_SRC]

	)    
	       
SELECT 
	   LTRIM(RTRIM(CUR_CLM_UNIQ_ID)) AS	CLAIM_ID
      ,COALESCE(NULLIF(LTRIM(RTRIM(BENE_MBI_ID)),''),NULLIF(LTRIM(RTRIM(BENE_HIC_NUM)),'')) AS [MEMBER_ID]
      ,NULL AS [FILL_DATE]
      ,NULL AS [MEDICATION_NAME]
      ,ltrim(rtrim(CLM_LINE_NDC_CD)) AS [MEDICATION_CODE]
      ,'NDC' AS [MEDICATION_CODE_TYPE]
      ,convert(money,ltrim(rtrim(CLM_LINE_SRVC_UNIT_QTY))) AS [QUANTITY]
      ,NULL AS [MANUFACTURER]
      ,NULL AS [MED_TYPE]
      ,convert(smallint,ltrim(rtrim(CLM_LINE_DAYS_SUPLY_QTY))) AS [SUPPLY_DAYS]
      ,NULL AS [REFILLS]
      ,CASE WHEN ltrim(rtrim(CLM_DAW_PROD_SLCTN_CD))='' THEN 'U' ELSE ltrim(rtrim(CLM_DAW_PROD_SLCTN_CD)) END AS [DAW]
      ,convert(date,CLM_LINE_FROM_DT) AS [PRESCRIPTION_DATE]
      ,NULLIF(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),'') AS [PRESC_NPI]
      ,NULL AS [PRESC_NAME]
      ,NULLIF(LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM)),'') AS [PHARM_ID]
      ,NULL AS [PHARM_NAME]
      ,NULL AS [PHARM_ADDR]
      ,NULL AS [PHARM_CITY]
      ,NULL AS [PHARM_ST]
      ,NULL AS [PHARM_ZIP]
      ,NULL AS [MED_ROUTE]
      ,NULL AS [MED_SIG]
      ,NULL AS [MED_FORM]
      ,NULL AS [MED_STRENGTH]
      ,NULL AS [MED_DOSE]
      ,'159' AS [ROOT_COMPANIES_ID]
      ,CONVERT(DATE,LOAD_DATE) AS [LOADDATETIME]
      ,NULL AS [FileDate]
      ,FTP_FILE_NAME AS [Filename]
      ,NULL AS [THERAPEUTIC_CLASS]
      ,'MSSP' AS [MED_DATA_SRC]
  FROM #TEMP 

	
	------------------------ENDS HERE------------------------------------------------------
END
GO
