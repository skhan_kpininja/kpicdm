SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE PROCEDURE [DBO].[STG_0400_STAGING_PROVIDER_MSSP]
AS
BEGIN

	TRUNCATE  TABLE [DBO].[TEMP_STAGING_PROVIDER]


	-----------------LOAD STAGING DATA--------------------------------------


	EXEC SP_KPI_DROPTABLE #TEMP_PROVIDER


	-------------************** PROVIDER****************************-----------------------

	--PART A BILLING PROVIDER
	SELECT   DISTINCT 
			 CONVERT(DATE,COALESCE( NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_LINE_FROM_DT)),'~'),'1000-01-01'),NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_FROM_DT)),'~'),'1000-01-01'),
			 NULLIF(NULLIF(LTRIM(RTRIM(B.[CLM_LINE_THRU_DT])),'~'),'1000-01-01'),NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_THRU_DT)),'~'),'1000-01-01'),
			 NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_THRU_DT)),'~'),'1000-01-01'),CONVERT(DATE,NULLIF(CLM_EFCTV_DT,'~')))) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,LTRIM(RTRIM(FAC_PRVDR_NPI_NUM))  AS PROV_ID
			,NULL AS PROV_TYPE
			,CASE WHEN LEN(LTRIM(RTRIM(FAC_PRVDR_NPI_NUM)))=10 THEN LTRIM(RTRIM(FAC_PRVDR_NPI_NUM)) ELSE NULL END AS PROV_NPI
			,NULL AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,NULL AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,NULL AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL AS PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULL AS PROV_SPEC_CODE
			,NULL AS PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC 
			,B.FTP_FILE_NAME AS FILENAME
			,B.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
	INTO #TEMP_PROVIDER	    
	FROM  
	KPI_ENGINE_SOURCE.CST.UHN_CCLF1_CLAIMHEADER A
	LEFT JOIN   KPI_ENGINE_SOURCE.CST.UHN_CCLF2_REVENUECODE  AS B 
	ON LTRIM(RTRIM(A.CUR_CLM_UNIQ_ID)) = LTRIM(RTRIM(B.CUR_CLM_UNIQ_ID))

	UNION
	--PART A ATTENDING PROVIDER
	SELECT   DISTINCT
			 CONVERT(DATE,COALESCE( NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_LINE_FROM_DT)),'~'),'1000-01-01'),NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_FROM_DT)),'~'),'1000-01-01'),
			 NULLIF(NULLIF(LTRIM(RTRIM(B.[CLM_LINE_THRU_DT])),'~'),'1000-01-01'),NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_THRU_DT)),'~'),'1000-01-01'),
			 NULLIF(NULLIF(LTRIM(RTRIM(B.CLM_THRU_DT)),'~'),'1000-01-01'),CONVERT(DATE,NULLIF(CLM_EFCTV_DT,'~')))) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,REPLACE(LTRIM(RTRIM(ATNDG_PRVDR_NPI_NUM)),'~','') AS PROV_ID
			,NULL AS PROV_TYPE
			,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM(ATNDG_PRVDR_NPI_NUM)),'~'),''))=10 THEN NULLIF(NULLIF(LTRIM(RTRIM(ATNDG_PRVDR_NPI_NUM)),'~'),'') ELSE NULL END AS PROV_NPI
			,NULL AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,NULL AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,NULL AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL AS PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULL AS PROV_SPEC_CODE
			,NULL AS PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG 
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC
			,B.FTP_FILE_NAME AS FILENAME
			,B.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
	FROM  KPI_ENGINE_SOURCE.CST.UHN_CCLF1_CLAIMHEADER A
	LEFT JOIN   KPI_ENGINE_SOURCE.CST.UHN_CCLF2_REVENUECODE  AS B 
	ON LTRIM(RTRIM(A.CUR_CLM_UNIQ_ID)) = LTRIM(RTRIM(B.CUR_CLM_UNIQ_ID))

	UNION
	-- PHYSICIAN ATTENDING PROVIDER ID
	SELECT DISTINCT 
			 CONVERT(DATE,CLM_LINE_FROM_DT) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,REPLACE(LTRIM(RTRIM(RNDRG_PRVDR_NPI_NUM)),'~','')  AS PROV_ID
			,NULL AS PROV_TYPE	
			,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM(RNDRG_PRVDR_NPI_NUM)),'~'),''))=10 THEN NULLIF(NULLIF(LTRIM(RTRIM(RNDRG_PRVDR_NPI_NUM)),'~'),'') ELSE NULL END AS PROV_NPI
			,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM(CLM_RNDRG_PRVDR_TAX_NUM)),'~'),''))=9 THEN NULLIF(NULLIF(LTRIM(RTRIM(CLM_RNDRG_PRVDR_TAX_NUM)),'~'),'') ELSE NULL END AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,NULL AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,NULL AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULLIF(LTRIM(RTRIM(CLM_PRVDR_SPCLTY_CD)),'') AS PROV_SPEC_CODE
			,NULL PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC
			,A.FTP_FILE_NAME AS FILENAME
			,A.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
			 FROM  KPI_ENGINE_SOURCE.CST.UHN_CCLF5_PHYSICIAN A
		  
	UNION
	-- DME BILLING PROVIDER ID
	SELECT  DISTINCT 
			 CONVERT(DATE,A.CLM_LINE_FROM_DT) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,REPLACE(LTRIM(RTRIM(PAYTO_PRVDR_NPI_NUM)),'~','') AS PROV_ID
			,NULL AS PROV_TYPE	
			,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM(PAYTO_PRVDR_NPI_NUM)),'~'),''))=10 THEN NULLIF(NULLIF(LTRIM(RTRIM(PAYTO_PRVDR_NPI_NUM)),'~'),'') ELSE NULL END AS PROV_NPI
			,NULL AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,NULL AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,NULL AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL AS PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULL AS PROV_SPEC_CODE
			,NULL AS PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG 
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC
			,A.FTP_FILE_NAME AS FILENAME
			,A.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
	FROM  KPI_ENGINE_SOURCE.CST.UHN_CCLF6_DME A
		 
	UNION
		-- DME ATTENDING PROVIDER ID
	SELECT DISTINCT 
			 CONVERT(DATE,A.CLM_LINE_FROM_DT) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,REPLACE(LTRIM(RTRIM(ORDRG_PRVDR_NPI_NUM)),'~','') AS PROV_ID
			,NULL AS PROV_TYPE	
			,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM(ORDRG_PRVDR_NPI_NUM)),'~'),''))=10 THEN NULLIF(NULLIF(LTRIM(RTRIM(ORDRG_PRVDR_NPI_NUM)),'~'),'') ELSE NULL END AS PROV_NPI
			,NULL AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,NULL AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,NULL AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL AS PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULL AS PROV_SPEC_CODE
			,NULL AS PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC
			,A.FTP_FILE_NAME AS FILENAME
			,A.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
	FROM  KPI_ENGINE_SOURCE.CST.UHN_CCLF6_DME A
		  
	UNION
	-- PHARMACY BILLING PROVIDER ID
	SELECT DISTINCT 
			 CONVERT(DATE,CLM_LINE_FROM_DT) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM))AS PROV_ID
			,NULL AS PROV_TYPE
			,(CASE WHEN LTRIM(RTRIM([PRVDR_SRVC_ID_QLFYR_CD]))IN('01') AND LEN(NULLIF(LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM)),''))=10 THEN NULLIF(LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM)),'') ELSE NULL END) AS PROV_NPI
			,(CASE WHEN LTRIM(RTRIM([PRVDR_SRVC_ID_QLFYR_CD]))='11' AND LEN(NULLIF(LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM)),''))=9 THEN NULLIF(LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM)),'') ELSE NULL END) AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,(CASE WHEN LTRIM(RTRIM([PRVDR_SRVC_ID_QLFYR_CD]))='08'  THEN NULLIF(LTRIM(RTRIM(CLM_SRVC_PRVDR_GNRC_ID_NUM)),'') ELSE NULL END) AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,NULL AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL AS PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULL AS PROV_SPEC_CODE
			,NULL AS PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG --
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC	
			,A.FTP_FILE_NAME AS FILENAME
			,A.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
	FROM  KPI_ENGINE_SOURCE.CST.UHN_CCLF7_PART_D A
		
	UNION
	-- PHARMACY ATTENDING PROVIDER ID
	SELECT  DISTINCT 
			 CONVERT(DATE,CLM_LINE_FROM_DT) AS FROM_DATE
			,NULL AS PROV_START_DATE
			,NULL AS PROV_END_DATE
			,LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)) AS PROV_ID
			,NULL AS PROV_TYPE	
			,(CASE WHEN LTRIM(RTRIM([PRVDR_PRSBNG_ID_QLFYR_CD]))IN('01') AND LEN(NULLIF(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),''))=10 THEN NULLIF(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),'') ELSE NULL END)  AS PROV_NPI
			,(CASE WHEN LTRIM(RTRIM([PRVDR_PRSBNG_ID_QLFYR_CD]))='11' AND LEN(NULLIF(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),''))=9 THEN NULLIF(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),'') ELSE NULL END) AS PROV_TIN
			,NULL AS PROV_TAXONOMY
			,(CASE WHEN LTRIM(RTRIM([PRVDR_PRSBNG_ID_QLFYR_CD]))='08' THEN NULLIF(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),'') ELSE NULL END) AS PROV_LIC
			,NULL AS PROV_MEDICAID
			,(CASE WHEN LTRIM(RTRIM([PRVDR_PRSBNG_ID_QLFYR_CD]))='12' THEN NULLIF(LEFT(LTRIM(RTRIM(CLM_PRSBNG_PRVDR_GNRC_ID_NUM)),12),'') ELSE NULL END) AS PROV_DEA
			,NULL AS PROV_NET_FLAG
			,NULL AS PROV_PAR_FLAG
			,NULL AS PROV_LNAME
			,NULL AS PROV_FNAME
			,NULL AS PROV_MNAME
			,NULL AS PROV_GENDER
			,NULL AS PROV_DOB
			,NULL AS PROV_EMAIL
			,NULL AS PROV_CLINIC_ID
			,NULL AS PROV_CLINIC_NAME
			,NULL AS PROV_CLINIC_ADDR
			,NULL AS PROV_CLINIC_ADDR2
			,NULL AS PROV_CLINIC_CITY
			,NULL AS PROV_CLINIC_STATE
			,NULL AS PROV_CLINIC_ZIP
			,NULL AS PROV_MAILING_ADDR
			,NULL AS PROV_MAILING_ADDR2
			,NULL AS PROV_MAILING_CITY
			,NULL AS PROV_MAILING_STATE
			,NULL AS PROV_MAILING_ZIP		
			,NULL AS PROV_PHONE
			,NULL AS PROV_GRP_ID
			,NULL AS PROV_GRP_NAME
			,NULL AS PROV_GRP_MAILING_ADDR
			,NULL AS PROV_GRP_MAILING_ADDR2
			,NULL AS PROV_GRP_MAILING_CITY
			,NULL AS PROV_GRP_MAILING_STATE
			,NULL AS PROV_GRP_MAILING_ZIP
			,NULL AS PROV_GRP_PHONE
			,NULL AS PROV_GRP_EMAIL
			,NULL AS PROV_SPEC_CODE
			,NULL AS PROV_SPEC_DESC
			,NULL AS PROV_SPEC_CODE2
			,NULL AS PROV_SPEC_DESC2
			,NULL AS PROV_SPEC_CODE3
			,NULL AS PROV_SPEC_DESC3
			,NULL AS PROV_PCP_FLAG 
			,NULL AS PROV_OB_FLAG
			,NULL AS PROV_MH_FLAG
			,NULL AS PROV_EYE_FLAG
			,NULL AS PROV_DEN_FLAG
			,NULL AS PROV_NEP_FLAG
			,NULL AS PROV_CD_FLAG
			,NULL AS PROV_NP_FLAG
			,NULL AS PROV_PA_FLAG
			,NULL AS PROV_RX_FLAG
			,NULL AS PROV_GROUP_IPA_NAME
			,NULL AS PROV_GROUP_ACO_NAME
			,'MSSP' AS PROV_DATA_SRC
			,NULL AS PROV_SRC_COMMON_ID
			,NULL AS PROV_QUALPORT
			,NULL AS PROV_ATTRIB
			,NULL AS _PROV_UDF_01_
			,NULL AS _PROV_UDF_02_
			,NULL AS _PROV_UDF_03_
			,NULL AS _PROV_UDF_04_
			,NULL AS _PROV_UDF_05_
			,NULL AS _PROV_UDF_06_
			,NULL AS _PROV_UDF_07_
			,NULL AS _PROV_UDF_08_
			,NULL AS _PROV_UDF_09_
			,NULL AS _PROV_UDF_10_
			,'U' AS PROV_DOMESTIC
			,A.FTP_FILE_NAME AS FILENAME
			,A.LOAD_DATE AS FILEDATE
			,159 AS ROOT_COMPANIES_ID
	FROM  KPI_ENGINE_SOURCE.CST.UHN_CCLF7_PART_D A

	--incaseof any provider crosswalk use this one

	--union 

	---- PCP PROVIDER ID CROSSWALK
	--		SELECT DISTINCT 
	--		 GETDATE() AS FROM_DATE
	--		 ,NULL AS PROV_START_DATE
	--		,NULL AS PROV_END_DATE
	--		,REPLACE(LTRIM(RTRIM([Provider NPI])),'~','') AS PROV_ID
	--		,NULL AS PROV_TYPE	
	--		,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM([Provider NPI])),'~'),''))=10 THEN NULLIF(NULLIF(LTRIM(RTRIM([Provider NPI])),'~'),'') ELSE NULL END AS PROV_NPI
	--		,CASE WHEN LEN(NULLIF(NULLIF(LTRIM(RTRIM([Provider TIN])),'~'),''))=9 THEN NULLIF(NULLIF(LTRIM(RTRIM([Provider TIN])),'~'),'') ELSE NULL END AS PROV_TIN
	--		,NULL AS PROV_TAXONOMY
	--		,NULL AS PROV_LIC
	--		,NULL AS PROV_MEDICAID
	--		,NULL AS PROV_DEA
	--		,NULL AS PROV_NET_FLAG
	--		,NULL AS PROV_PAR_FLAG
	--		,NULLIF(LTRIM(RTRIM([provider Last Name])),'') AS PROV_LNAME
	--		,NULLIF(LTRIM(RTRIM([provider First Name])),'') AS PROV_FNAME
	--		,NULL AS PROV_MNAME
	--		,NULL AS PROV_GENDER
	--		,NULL AS PROV_DOB
	--		,NULL AS PROV_EMAIL
	--		,NULL AS PROV_CLINIC_ID
	--		,NULL AS PROV_CLINIC_NAME
	--		,NULL AS PROV_CLINIC_ADDR
	--		,NULL AS PROV_CLINIC_ADDR2
	--		,NULL AS PROV_CLINIC_CITY
	--		,NULL AS PROV_CLINIC_STATE
	--		,NULL AS PROV_CLINIC_ZIP
	--		,NULL AS PROV_MAILING_ADDR
	--		,NULL AS PROV_MAILING_ADDR2
	--		,NULL AS PROV_MAILING_CITY
	--		,NULL AS PROV_MAILING_STATE
	--		,NULL AS PROV_MAILING_ZIP		
	--		,NULL AS PROV_PHONE
	--		,NULL AS PROV_GRP_ID
	--		,NULL AS PROV_GRP_NAME
	--		,NULL AS PROV_GRP_MAILING_ADDR
	--		,NULL AS PROV_GRP_MAILING_ADDR2
	--		,NULL AS PROV_GRP_MAILING_CITY
	--		,NULL AS PROV_GRP_MAILING_STATE
	--		,NULL AS PROV_GRP_MAILING_ZIP
	--		,NULL AS PROV_GRP_PHONE
	--		,NULL AS PROV_GRP_EMAIL
	--		,NULL AS PROV_SPEC_CODE
	--		,NULL AS PROV_SPEC_DESC
	--		,NULL AS PROV_SPEC_CODE2
	--		,NULL AS PROV_SPEC_DESC2
	--		,NULL AS PROV_SPEC_CODE3
	--		,NULL AS PROV_SPEC_DESC3
	--		,'Y' AS PROV_PCP_FLAG --
	--		,NULL AS PROV_OB_FLAG
	--		,NULL AS PROV_MH_FLAG
	--		,NULL AS PROV_EYE_FLAG
	--		,NULL AS PROV_DEN_FLAG
	--		,NULL AS PROV_NEP_FLAG
	--		,NULL AS PROV_CD_FLAG
	--		,NULL AS PROV_NP_FLAG
	--		,NULL AS PROV_PA_FLAG
	--		,NULL AS PROV_RX_FLAG
	--		,NULL AS PROV_GROUP_IPA_NAME
	--		,NULL AS PROV_GROUP_ACO_NAME
	--		,'MSSP' AS PROV_DATA_SRC
	--		,NULL AS PROV_SRC_COMMON_ID
	--		,NULL AS PROV_QUALPORT
	--		,NULL AS PROV_ATTRIB
	--		,NULL AS _PROV_UDF_01_
	--		,NULL AS _PROV_UDF_02_
	--		,NULL AS _PROV_UDF_03_
	--		,NULL AS _PROV_UDF_04_
	--		,NULL AS _PROV_UDF_05_
	--		,NULL AS _PROV_UDF_06_
	--		,NULL AS _PROV_UDF_07_
	--		,NULL AS _PROV_UDF_08_
	--		,NULL AS _PROV_UDF_09_
	--		,NULL AS _PROV_UDF_10_,
	--		'U' AS PROV_DOMESTIC		
	--		 FROM KPI_ENGINE_SOURCE.[dbo].CLIENTNAME_MemberMSSPProvider
		  


	DELETE FROM #TEMP_PROVIDER WHERE PROV_ID=''
	DELETE FROM #TEMP_PROVIDER WHERE PROV_ID IS NULL

	SELECT B.*, ROW_NUMBER() OVER(PARTITION BY B.PROV_ID ORDER  BY FROM_DATE DESC) ROW_NUM INTO #TEMP_PROV_REM_DUPLICATE
	FROM
	#TEMP_PROVIDER B
		


	INSERT INTO  [DBO].[TEMP_STAGING_PROVIDER] 
			(   
			 PROV_START_DATE
			,PROV_END_DATE
			,PROV_ID
			,PROV_TYPE
			,PROV_NPI
			,PROV_TIN
			,PROV_TAXONOMY
			,PROV_LIC
			,PROV_MEDICAID
			,PROV_DEA
			,PROV_NET_FLAG
			,PROV_PAR_FLAG
			,PROV_LNAME
			,PROV_FNAME
			,PROV_MNAME
			,PROV_GENDER
			,PROV_DOB
			,PROV_EMAIL
			,PROV_CLINIC_ID
			,PROV_CLINIC_NAME
			,PROV_CLINIC_ADDR
			,PROV_CLINIC_ADDR2
			,PROV_CLINIC_CITY
			,PROV_CLINIC_STATE
			,PROV_CLINIC_ZIP
			,PROV_MAILING_ADDR
			,PROV_MAILING_ADDR2
			,PROV_MAILING_CITY
			,PROV_MAILING_STATE
			,PROV_MAILING_ZIP
			,PROV_PHONE
			,PROV_GRP_ID
			,PROV_GRP_NAME
			,PROV_GRP_MAILING_ADDR
			,PROV_GRP_MAILING_ADDR2
			,PROV_GRP_MAILING_CITY
			,PROV_GRP_MAILING_STATE
			,PROV_GRP_MAILING_ZIP
			,PROV_GRP_PHONE
			,PROV_GRP_EMAIL
			,PROV_SPEC_CODE
			,PROV_SPEC_DESC
			,PROV_SPEC_CODE2
			,PROV_SPEC_DESC2
			,PROV_SPEC_CODE3
			,PROV_SPEC_DESC3
			,PROV_PCP_FLAG
			,PROV_OB_FLAG
			,PROV_MH_FLAG
			,PROV_EYE_FLAG
			,PROV_DEN_FLAG
			,PROV_NEP_FLAG
			,PROV_CD_FLAG
			,PROV_NP_FLAG
			,PROV_PA_FLAG
			,PROV_RX_FLAG
			,PROV_GROUP_IPA_NAME
			,PROV_GROUP_ACO_NAME
			,PROV_DATA_SRC
			,PROV_SRC_COMMON_ID
			,PROV_QUALPORT
			,PROV_ATTRIB
			,_PROV_UDF_01_
			,_PROV_UDF_02_
			,_PROV_UDF_03_
			,_PROV_UDF_04_
			,_PROV_UDF_05_
			,_PROV_UDF_06_
			,_PROV_UDF_07_
			,_PROV_UDF_08_
			,_PROV_UDF_09_
			,_PROV_UDF_10_
			,PROV_DOMESTIC
			,FILENAME
			,LOADDATETIME
			,ROOT_COMPANIES_ID
		  ) 

	SELECT 
			 NULL
			,NULL
			,PROV_ID
			,PROV_TYPE
			,LEFT(PROV_NPI,10)
			,LEFT(PROV_TIN,9)
			,PROV_TAXONOMY
			,PROV_LIC
			,PROV_MEDICAID
			,LEFT(PROV_DEA,12)
			,PROV_NET_FLAG
			,PROV_PAR_FLAG
			,LEFT(PROV_LNAME,100)
			,LEFT(PROV_FNAME,50)
			,LEFT(PROV_MNAME,1)
			,LEFT(PROV_GENDER,1)
			,NULL
			,PROV_EMAIL
			,PROV_CLINIC_ID
			,PROV_CLINIC_NAME
			,PROV_CLINIC_ADDR
			,PROV_CLINIC_ADDR2
			,PROV_CLINIC_CITY
			,PROV_CLINIC_STATE
			,PROV_CLINIC_ZIP
			,LEFT(PROV_MAILING_ADDR,60)
			,LEFT(PROV_MAILING_ADDR2,60)
			,LEFT(PROV_MAILING_CITY,40)
			,LEFT(PROV_MAILING_STATE,2)
			,LEFT(PROV_MAILING_ZIP,5)
			,PROV_PHONE
			,PROV_GRP_ID
			,PROV_GRP_NAME
			,PROV_GRP_MAILING_ADDR
			,PROV_GRP_MAILING_ADDR2
			,PROV_GRP_MAILING_CITY
			,LEFT(PROV_GRP_MAILING_STATE,2)
			,LEFT(PROV_GRP_MAILING_ZIP,5)
			,PROV_GRP_PHONE
			,PROV_GRP_EMAIL
			,LEFT(PROV_SPEC_CODE,80)
			,LEFT(PROV_SPEC_DESC,80)
			,LEFT(PROV_SPEC_CODE2,80)
			,LEFT(PROV_SPEC_DESC2,80)
			,PROV_SPEC_CODE3
			,PROV_SPEC_DESC3
			,LEFT(PROV_PCP_FLAG,1)
			,PROV_OB_FLAG
			,PROV_MH_FLAG
			,PROV_EYE_FLAG
			,PROV_DEN_FLAG
			,PROV_NEP_FLAG
			,PROV_CD_FLAG
			,PROV_NP_FLAG
			,PROV_PA_FLAG
			,PROV_RX_FLAG
			,PROV_GROUP_IPA_NAME
			,PROV_GROUP_ACO_NAME
			,PROV_DATA_SRC
			,PROV_SRC_COMMON_ID
			,PROV_QUALPORT
			,PROV_ATTRIB
			,_PROV_UDF_01_
			,_PROV_UDF_02_
			,_PROV_UDF_03_
			,_PROV_UDF_04_
			,_PROV_UDF_05_
			,_PROV_UDF_06_
			,_PROV_UDF_07_
			,_PROV_UDF_08_
			,_PROV_UDF_09_
			,_PROV_UDF_10_
			,PROV_DOMESTIC
			,FILENAME
			,FILEDATE
			,ROOT_COMPANIES_ID
	FROM #TEMP_PROV_REM_DUPLICATE WHERE ROW_NUM=1


	------------------------ENDS HERE------------------------------------------------------

	
EXEC SP_KPI_DROPTABLE  '#TEMP_PROVIDER'
EXEC SP_KPI_DROPTABLE '#TEMP_PROV_REM_DUPLICATE '

END
GO
