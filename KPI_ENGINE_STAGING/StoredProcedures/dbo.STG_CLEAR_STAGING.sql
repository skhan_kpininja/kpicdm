SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [dbo].[STG_CLEAR_STAGING]
AS
BEGIN

delete from dbo.STAGING_CLAIMLINE
delete from dbo.STAGING_CLAIMHEADER
delete from dbo.STAGING_DIAGNOSIS
delete from dbo.STAGING_ENROLLMENT
delete from dbo.STAGING_LAB
delete from dbo.STAGING_MCFIELDS
delete from dbo.STAGING_MEDICATION
delete from dbo.STAGING_MEMBER
delete from dbo.STAGING_PROCEDURES
delete from dbo.STAGING_PROVIDER
delete from dbo.STAGING_VITAL

END
GO
