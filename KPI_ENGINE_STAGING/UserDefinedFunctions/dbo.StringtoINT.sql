SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[StringtoINT]
(
	-- Add the parameters for the function here
	@string nvarchar(600)
)
RETURNS  varchar(100)
AS
BEGIN
	-- Declare the return variable here
	--DECLARE @string nvarchar(600)='FlatFiles';
	Declare @i INT=1
	Declare @sum varchar(100) ='F'
	Declare @v INT

	
	
	
	While(@i<=len(@string))
	Begin
	--	Print 'In while'
	--	Print @i
		Set @v=ascii(Substring(@string,@i,1))
	--	Print @v
		Set @sum=concat(@sum,@v)
		Set @i=@i+1;
	End

	-- Return the result of the function
	--Select @sum
	RETURN @sum
	
END

GO
