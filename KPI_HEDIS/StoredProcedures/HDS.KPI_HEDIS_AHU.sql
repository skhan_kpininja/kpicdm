SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE proc [HDS].[KPI_HEDIS_AHU]
AS


--Declare vaiable
Declare @rundate date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='AHU-S'
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @ce_startdt Date
Declare @ce_startdt1 Date
Declare @ce_startdt2 Date
Declare @ce_dischstartdt Date
Declare @ce_enddt Date
Declare @ce_enddt1 Date
Declare @snf_enddt Date
Declare @ce_dischenddt Date
Declare @meas varchar(10);
Declare @runid INT=0;
Declare @reportId INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);



Set @meas='AHU';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_dischstartdt=concat(@meas_year,'-01-03');
SET @ce_enddt=concat(@meas_year,'-12-31');
Set @snf_enddt=concat(@meas_year,'-12-02');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_dischenddt=concat(@meas_year,'-12-01');

/*
Set @reporttype='Network'
Set @measurename='30 Day Readmission Rate ? All Cause'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=12
Set @domain='Utilization Management'
Set @subdomain='Readmission Rate'
Set @measuretype='UHN'
 Set @measure_id='40'

 */

 -- Identify Eligible Population

 -- Indetify members who are Continuous Enrolled in measurement year and year prior to it
Drop table if exists #ahu_contenroll
Create Table #ahu_contenroll
(
	EMPI varchar(100)
)
Insert into #ahu_contenroll
Select
	t1.EMPI
From GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0) t1
Join
(
	Select
		t1.EMPI
	From GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1) t1
	
)t2 on 
	t1.EMPI=t2.EMPI


-- Identify members who qualify for Hospice exclusion
drop table if exists #ahu_hospicemembers;
CREATE table #ahu_hospicemembers
(
	EMPI varchar(100)
		
)
Insert into #ahu_hospicemembers
select EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)


-- Identify members who are enrolled during the continuous enrollment period and are 18+
 -- Create Temp Patient Enrollment
drop table if exists #ahu_tmpsubscriber;
CREATE TABLE #ahu_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #ahu_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
Join #ahu_contenroll ce on
	gm.EMPI_ID=ce.EMPI
left outer join #ahu_hospicemembers h on
	gm.EMPI_ID=h.EMPI	
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1 AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) >=18 and
	h.EMPI is null
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;




--Identify member productline which needs to be reported
drop table if exists #ahu_payermap
Create table #ahu_payermap
(
	EMPI varchar(100),
	Payer varchar(50),
	Age INT,
	Gender varchar(10)
)
Insert into #ahu_payermap
Select distinct
	EMPI
	,pm.PayerMapping
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #ahu_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='AHU'
Order by 1


-- Create the base dataset
Drop table if exists #ahudataset
Create Table #ahudataset
(
	EMPI varchar(100),
	meas varchar(20),
	Payer varchar(50),
	Epop tinyint default 1,
	Discharges Int default 0,
	Outlier tinyint default 0,
	PPDComorbidWt float default 0,
	PPDAgeGenWt float default 0,
	PUCDComorbidWt float default 0,
	PUCDAgeGenWt float default 0,
	Age INT,
	Gender varchar(10)
)
Insert into #ahudataset(EMPI,meas,Payer,Age,Gender)
Select 
	*
From
(
	Select
		EMPI
		,'AHUS'  as meas
		,Payer
		,Age
		,Gender
	From #ahu_payermap
	where
		Payer in('CEP','HMO','POS','PPO','MCR','MCS','MP','MR','EPO')

	Union all

	Select
		EMPI
		,'AHUM'  as meas
		,Payer
		,Age
		,Gender
	From #ahu_payermap
	where
		Payer in('CEP','HMO','POS','PPO','MCR','MCS','MP','MR','EPO')


	Union all

	Select
		EMPI
		,'AHUT' as meas
		,Payer
		,Age
		,Gender
	From #ahu_payermap
	where
		Payer in('CEP','HMO','POS','PPO','MCR','MCS','MP','MR','EPO')

)t1



 -- Step 1 - Identify all acute inpatient and observation discharges during the measurement year. To identify acute inpatient and observation discharges
Drop table if exists #ahu_observationstays
Create table #ahu_observationstays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50),
	SV_STAT varchar(20)
)
Insert into #ahu_observationstays
Select
	*
From observationstays(@rootId,@ce_startdt,@ce_enddt)
where 
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


Drop table if exists #acu_inpatientstays
Create table #acu_inpatientstays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50),
	SV_STAT varchar(20)
)
Insert into #acu_inpatientstays
Select
	*
From inpatientstays(@rootId,@ce_startdt,@ce_enddt)
where 
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


--Identify all non-acute stays
Drop table if exists #ahu_nonacutestays
Create table #ahu_nonacutestays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #ahu_nonacutestays
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt,@ce_enddt)
where 
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


-- Step 2 and 3 - Identify Direct Transfers , Select discharges between 1st Jan and 1st Dec , Exlcude same day stays
drop table if exists #ahu_directtransfers;
Create table #ahu_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	t1.EMPI
	,t1.ADM_DATE
	,DIS_DATE
	,t1.CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by t1.EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by t1.EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
	From
	(
		Select
			*
		From #ahu_observationstays
		Where 
			ISNULL(SV_STAT,'O') in('P','A','O','')
	
		Union all

		Select
			*
		From #acu_inpatientstays
		Where 
			ISNULL(SV_STAT,'O') in('P','A','O','')
	)t1
	left outer join #ahu_nonacutestays na on
		t1.EMPI=na.EMPI and
		t1.CLAIM_ID=na.CLAIM_ID and
		t1.CL_DATA_SRC=na.CL_DATA_SRC
	where
		na.EMPI is null
	--	and t1.EMPI=95075
	
)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #ahu_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1



-- Create a normalized transfer table to improve lookup
drop table if exists #ahu_directtransfers_normalized;
Create table #ahu_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #ahu_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #ahu_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p
	


-- Step 3 For the remaining observation and inpatient discharges, exclude inpatient and observation discharges

-- Step 3.1 - ?	A principal diagnosis of mental health or chemical dependency (Mental and Behavioral Disorders Value Set). 
Drop table if exists #ahu_behavioral_excl;
Create Table #ahu_behavioral_excl
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ahu_behavioral_excl
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Mental and Behavioral Disorders')
Where
	DIAG_SEQ_NO=1 and
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)



-- Step 3.2 - ?	A principal diagnosis of live-born infant (Deliveries Infant Record Value Set).
Drop table if exists #ahu_deliveries_excl;
Create Table #ahu_deliveries_excl
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ahu_deliveries_excl
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Deliveries Infant Record')
Where
	DIAG_SEQ_NO=1 and
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


-- Step 3.3 - ?	A maternity-related principal diagnosis (Maternity Diagnosis Value Set).
Drop table if exists #ahu_maternitydiagnosis_excl;
Create Table #ahu_maternitydiagnosis_excl
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ahu_maternitydiagnosis_excl
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Maternity Diagnosis')
Where
	DIAG_SEQ_NO=1 and
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)



-- Step 3.4 - ?	A maternity-related stay (Maternity Value Set).
Drop table if exists #ahu_maternity_excl;
Create Table #ahu_maternity_excl
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ahu_maternity_excl
Select
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	ISNULL(POS,'0')!=81 and
	DIS_DATE between @ce_startdt1 and @ce_enddt and
	(
		REV_CODE in
		(
			Select code from HDS.valueset_to_code where code_system='UBREV' and value_set_Name='Maternity'
		)
		or
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
		(
			Select code from HDS.valueset_to_code where code_system='UBTOB' and value_set_Name='Maternity'
		)
	)

-- Step 3.5 - ?	Inpatient and observation stays with a discharge for death.
Drop table if exists #ahu_deceased_excl;
Create Table #ahu_deceased_excl
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ahu_deceased_excl
Select
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	ISNULL(POS,'0')!=81 and
	DIS_DATE between @ce_startdt1 and @ce_enddt and
	ISNULL(DIS_STAT,0)='20'


-- Exclude stays with above conditions
drop table if exists #ahu_stays_withexcl;
Create table #ahu_stays_withexcl
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #ahu_stays_withexcl
Select
	s.*
From #ahu_directtransfers_normalized s
left outer join #ahu_behavioral_excl be on
	s.EMPI=be.EMPI and
	be.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #ahu_deliveries_excl de on
	s.EMPI=de.EMPI and
	de.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #ahu_maternitydiagnosis_excl mde on
	s.EMPI=mde.EMPI and
	mde.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #ahu_maternity_excl me on
	s.EMPI=me.EMPI and
	me.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #ahu_deceased_excl  dce on
	s.EMPI=dce.EMPI and
	dce.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
Where
	be.EMPI is null and
	de.EMPI is null and
	mde.EMPI is null and
	me.EMPI is null and
	dce.EMPI is null






-- step 4  - Identify outliers
Drop table if exists #ahu_outliers
Create table #ahu_outliers
(
	EMPI varchar(100),
	Dischargecount INT,
	outlier INT
)
Insert into #ahu_outliers
Select
	t1.EMPI
	,Dischargecount
	,case
		When Payer in('MCR','MCS','MP','MR') and dischargecount>=4 then 1
		When Payer in('CEP','HMO','POS','PPO','EPO') and dischargecount>=3 then 1
		else 0
	end as outlier
From
(
	select
		EMPI
		,count(*) as Dischargecount
	From #ahu_stays_withexcl s
	Group by
		EMPI
)t1
Join #ahu_payermap pm on
	t1.EMPI=pm.EMPI



Update ds set
	ds.Outlier=o.outlier
From #ahudataset ds
Join #ahu_outliers o on
	ds.EMPI=o.EMPI

Update ds set
	ds.Discharges=o.Dischargecount
From #ahudataset ds
Join #ahu_outliers o on
	ds.EMPI=o.EMPI and
	ds.Outlier=0 and
	ds.meas='AHUT'


--Step 6 - Identify Surgery Discharges	
Drop table if exists #ahu_surgerystays
Create Table #ahu_surgerystays
(
	EMPI varchar(100),
	Surgerycount INT
	
)
Insert into #ahu_surgerystays
Select
	EMPI
	,count(distinct DIS_DATE) as Surgerycount
From
(
	Select 
		t1.EMPI
		,CLAIM_ID
		,CL_DATA_SRC
		,s.DIS_DATE
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt1,@ce_enddt,'Surgery') t1
	join #ahu_directtransfers_normalized s on
		s.EMPI=t1.EMPI and
		t1.CLAIM_ID in(s.CLAIM1,s.CLAIM2,s.CLAIM3,s.CLAIM4,s.CLAIM5)
	Where
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(SV_STAT,'O') in('P','A','O','')
)t1
Group by
	EMPI
	


Update ds Set 
	ds.Discharges=s.Surgerycount
From #ahudataset ds
Join #ahu_surgerystays s on
	ds.EMPI=s.EMPI and
	ds.meas='AHUS' and
	ds.Outlier=0

--Step 7 - Identify Medicine Discharges	
Drop table if exists #ahu_medicinestays
Create Table #ahu_medicinestays
(
	EMPI varchar(100),
	Medicinecount INT
	
)
Insert into #ahu_medicinestays
Select
	EMPI
	,count(distinct DIS_DATE) as medicinecount
From
(
	Select
		s.EMPI
		,s.DIS_DATE
	From #ahu_stays_withexcl s
	left outer join 
	(
		Select 
			EMPI
			,CLAIM_ID
			,CL_DATA_SRC
		From GetRecordsByRevenueCodes(@rootId,@ce_startdt1,@ce_enddt,'Surgery')
		Where
			CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
			ISNULL(SV_STAT,'O') in('P','A','O','')
	)t1 on 
		s.EMPI=t1.EMPI and
		t1.CLAIM_ID in (s.CLAIM1,s.CLAIM2,s.CLAIM3,s.CLAIM4,s.CLAIM5)
	Where 
		t1.EMPI is null


	

)t1
Group by
	EMPI
	


Update ds Set 
	ds.Discharges=s.Medicinecount
From #ahudataset ds
Join #ahu_medicinestays s on
	ds.EMPI=s.EMPI and
	ds.meas='AHUM' and
	ds.Outlier=0




-- Identifying all diagnosis


--Identify visits for risk scoring
Drop table if exists #ahu_diagvisits;
Create Table #ahu_diagvisits
(
	EMPI varchar(100),
	ServiceDate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ahu_diagvisits
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Outpatient')

	Union All 

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Telephone Visits')

	Union all 

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Observation')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'ED')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Nonacute Inpatient')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Acute Inpatient')

	Union All

	Select
		EMPI
		,FROM_DATE as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		FROM_DATE between @ce_startdt1 and @ce_enddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
		)

	Union All

	Select
		p.EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,p.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From Procedures p
	Join Claimline c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(C.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		ISNULL(POS,'0')!='81' and
		coalesce(c.DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_enddt1 and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
		)

	Union all


	Select
		EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_enddt1 and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		)



)t1



drop table if exists #ahu_diagnosislist;
Create table #ahu_diagnosislist
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT,
	DIAG_DATA_SRC varchar(50)

)
Insert into #ahu_diagnosislist
Select distinct
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO
	,DIAG_DATA_SRC
From Diagnosis
Where
	ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	DIAG_START_DATE between @ce_startdt1 and @ce_enddt1


drop table if exists #ahu_diagnosis;
Create table #ahu_diagnosis
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT

)
Insert into #ahu_diagnosis
Select
	d.EMPI
	,d.diagnosis
	,v.Servicedate
	,d.CLAIM_ID
	,d.DIAG_SEQ_NO
From #ahu_diagnosislist d
Join #ahu_diagvisits v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DIAG_DATA_SRC=v.DATA_SRC



-- HCC RANK
drop table if exists #ahu_hccrank;
Create table #ahu_hccrank
(
	EMPI varchar(100),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	meas varchar(20)
	
)
Insert into #ahu_hccrank
select 
	t2.EMPI
	,t2.payer
	,case 
		when age between 18 and 24 and gender='F' then 'F_18-24'
		when age between 25 and 34 and gender='F' then 'F_25-34'
		when age between 35 and 44 and gender='F' then 'F_35-44'
		when age between 45 and 54 and gender='F' then 'F_45-54'
		when age between 55 and 64 and gender='F' then 'F_55-64'
		when age between 65 and 74 and gender='F' then 'F_65-74'
		when age between 75 and 84 and gender='F' then 'F_75-84'
		when age >=85 and gender='F' then 'F_85'
		when age between 18 and 24 and gender='M' then 'M_18-24'
		when age between 25 and 34 and gender='M' then 'M_25-34'
		when age between 35 and 44 and gender='M' then 'M_35-44'
		when age between 45 and 54 and gender='M' then 'M_45-54'
		when age between 55 and 64 and gender='M' then 'M_55-64'
		when age between 65 and 74 and gender='M' then 'M_65-74'
		when age between 75 and 84 and gender='M' then 'M_75-84'
		when age >=85 and gender='M' then 'M_85'
	
	end as genagegroup
	,case 
		when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUM' then 'Medicine - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUM' then 'Medicine - 65+'
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUS' then 'Surgery - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUS' then 'Surgery - 65+'
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUT' then 'Total - 18-64'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUT' then 'Total - 18+'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUS' then 'Surgery - 18+'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUM' then 'Medicine - 18+'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUT' then 'Total - 65+'
	end as reportingindicator
	,case
		When payer in('CEP','HMO','POS','PPO','EPO') then 'Commercial'
		else 'Medicare'
	end as productline
	,hcc
	,meas
	
from
(
	select distinct 
		EMPI
		,HCC
		,cast(age as INT) as age
		,gender
		,payer
		,meas
	from 
	(
		select 
			d.EMPI
			,cc.Comorbid_CC
			,hcc.RankingGroup
			,isnull(hcc.Rank,1) as Rank
			,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC
			,Rank() over(partition by d.EMPI,RankingGroup order by Rank) as calcrank
			,ds.age
			,ds.gender
			,ds.payer 
			,ds.meas
		from #ahu_diagnosis d
		left outer join HDS.HEDIS_TABLE_CC cc on 
			d.diagnosis=cc.diagnosiscode and 
			cc.cc_type='Shared'
		left outer join HDS.HEDIS_HCC_RANK hcc on 
			cc.Comorbid_CC=hcc.CC and 
			hcc.hcc_type='Shared'
		join #ahudataset ds on 
			ds.EMPI=d.EMPI and 
			ds.outlier=0
		where 
			Comorbid_CC is not null
	)t1 
	where 
		calcrank=1
)t2


-- HCC RANK COMB
drop table if exists #ahu_hccrankcmb;
Create table #ahu_hccrankcmb
(
	EMPI varchar(100),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	meas varchar(20),
	hcccmb  varchar(10)
	
)
Insert into #ahu_hccrankcmb
select 
	h.*
	,cmb.hcccomb 
from #ahu_hccrank h 
left outer join HDS.HEDIS_HCC_COMB cmb on 
	cmb.ComorbidHCC1 in
	(
		select 
			HCC 
		from #ahu_hccrank t1 
		where 
			t1.EMPI=h.EMPI
	) 
	and 
	cmb.ComorbidHCC2 in
	(
		select 
			HCC 
		from #ahu_hccrank t1 
		where 
			t1.EMPI=h.EMPI
	)
	and 
	cmb.hcc_type='Shared'



-- PPD Model
drop table if exists #ahu_ppdagegenwt;
Create table #ahu_ppdagegenwt
(
	EMPI varchar(100),
	payer varchar(10),
	PPDAgeGenWt float,
	meas varchar(20)
	
)
Insert into #ahu_ppdagegenwt
select distinct 
	t1.EMPI
	,t1.payer
	,r1.Weight as PPDAgeGenWt 
	,meas
from
(
	select 
		EMPI
		,payer
		,case 
			when age between 18 and 24 and gender='F' then 'F_18-24'
			when age between 25 and 34 and gender='F' then 'F_25-34'
			when age between 35 and 44 and gender='F' then 'F_35-44'
			when age between 45 and 54 and gender='F' then 'F_45-54'
			when age between 55 and 64 and gender='F' then 'F_55-64'
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 18 and 24 and gender='M' then 'M_18-24'
			when age between 25 and 34 and gender='M' then 'M_25-34'
			when age between 35 and 44 and gender='M' then 'M_35-44'
			when age between 45 and 54 and gender='M' then 'M_45-54'
			when age between 55 and 64 and gender='M' then 'M_55-64'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
	
		end as genagegroup
		,case 
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUM' then 'Medicine - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUM' then 'Medicine - 65+'
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUS' then 'Surgery - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUS' then 'Surgery - 65+'
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUT' then 'Total - 18-64'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUT' then 'Total - 18+'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUS' then 'Surgery - 18+'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUM' then 'Medicine - 18+'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUT' then 'Total - 65+'
		end as reportingindicator
		,case
			When payer in('CEP','HMO','POS','PPO','EPO') then 'Commercial'
			else 'Medicare'
		end as productline
		,meas
	from #ahudataset 
	where 
		outlier=0
		--and EMPi=95061
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and
	r1.ReportingIndicator=t1.reportingindicator and
	t1.productline=r1.ProductLine and 
	r1.variabletype='DEMO' and 
	r1.Model='PPD' and
	r1.Measure_id='AHU'

	


drop table if exists #ahu_ppdcomorbidwt;
Create table #ahu_ppdcomorbidwt
(
	EMPI varchar(100),
	payer varchar(10),
	meas varchar(20),
	PPDComorbidWt float
	
)
Insert into #ahu_ppdcomorbidwt
select
	EMPI
	,payer
	,meas
	,sum(weight) as PPDComorbidWt 
from
(
	select distinct 
		h.EMPI
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcc as hcc
		,h.meas
		,r1.Weight 
	from #ahu_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcc and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='PPD' and
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='AHU'
--	where EMPi=153328

	Union all

	select distinct 
		h.EMPI
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcccmb as hcc
		,h.meas
		,r1.Weight 
	from #ahu_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcccmb and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='PPD' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='AHU'

)t1 
group by 
	EMPI
	,payer
	,meas


update #ahudataset set PPDAgeGenWt=0,PPDComorbidWt=0
update #ahudataset set PPDAgeGenWt=p.PPDAgeGenWt from #ahudataset ds join #ahu_ppdagegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;
update #ahudataset set PPDComorbidWt=p.PPDComorbidWt from #ahudataset ds join #ahu_ppdcomorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;




-- PUCD Model

drop table if exists #ahu_pucdagegenwt;
Create table #ahu_pucdagegenwt
(
	EMPI varchar(50),
	payer varchar(10),
	meas varchar(20),
	PUCDAgeGenWt float
	
)
Insert into #ahu_pucdagegenwt
select distinct 
	t1.EMPI
	,t1.payer
	,t1.meas
	,r1.Weight as PUCDAgeGenWt 
from
(
	select 
		EMPI
		,payer
		,case 
			when age between 18 and 24 and gender='F' then 'F_18-24'
			when age between 25 and 34 and gender='F' then 'F_25-34'
			when age between 35 and 44 and gender='F' then 'F_35-44'
			when age between 45 and 54 and gender='F' then 'F_45-54'
			when age between 55 and 64 and gender='F' then 'F_55-64'
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 18 and 24 and gender='M' then 'M_18-24'
			when age between 25 and 34 and gender='M' then 'M_25-34'
			when age between 35 and 44 and gender='M' then 'M_35-44'
			when age between 45 and 54 and gender='M' then 'M_45-54'
			when age between 55 and 64 and gender='M' then 'M_55-64'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
		end as genagegroup
		,case 
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUM' then 'Medicine - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUM' then 'Medicine - 65+'
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUS' then 'Surgery - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUS' then 'Surgery - 65+'
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 and meas='AHUT' then 'Total - 18-64'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUT' then 'Total - 18+'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUS' then 'Surgery - 18+'
			when payer not in('MCR','MP','MCS','MC','MR') and meas='AHUM' then 'Medicine - 18+'
			when payer in('MCR','MP','MCS','MC','MR') and age >=65 and meas='AHUT' then 'Total - 65+'
		end as reportingindicator
		,case
			When payer in('CEP','HMO','POS','PPO','EPO') then 'Commercial'
			else 'Medicare'
		end as productline
		,meas
	from #ahudataset 
	where 
		outlier=0
	--	and EMPI=145387
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	t1.productline=r1.ProductLine and
	t1.reportingindicator=r1.ReportingIndicator and
	r1.variabletype='DEMO' and
	r1.Model='PUCD'  and 
	r1.Measure_ID='AHU'



drop table if exists #ahu_pucdcomorbidwt;
Create table #ahu_pucdcomorbidwt
(
	EMPI varchar(100),
	payer varchar(10),
	meas varchar(20),
	PUCDComorbidWt float
	
)
Insert into #ahu_pucdcomorbidwt
select 
	EMPI
	,payer
	,meas
	,case 
		when decimalct>10 then round(PUCDComorbidWt,10,1) 
		else PUCDComorbidWt 
	end as PUCVomorbidWt 
from
(
	select 
		EMPI
		,payer
		,meas
		,PUCDComorbidWt
		,CASE Charindex('.',PUCDComorbidWt) 
			WHEN 0 THEN 0  
			ELSE Len (Cast(Cast(Reverse(CONVERT(VARCHAR(50),PUCDComorbidWt, 128)) AS FLOAT) AS BIGINT)) 
		END as decimalct 
	from
	(
		select 
			EMPI
			,payer
			,meas
			,EXP(SUM(LOG(weight))) as PUCDComorbidWt 
		from
		(
			select distinct 
				h.EMPI
				,h.payer
				,h.meas
				,h.genagegroup
				,h.reportingindicator
				,h.hcc as hcc
				,r1.Weight as weight 
			from #ahu_hccrankcmb h
			join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
				r1.VariableName=h.hcc and 
				h.productline=r1.ProductLine and 
				r1.variabletype='HCC' and 
				r1.Model='PUCD' and 
				r1.ReportingIndicator=h.reportingindicator  and 
				r1.Measure_ID='AHU'
				--and memid=97968
			Union all

			select distinct 
				h.EMPI
				,h.payer
				,h.meas
				,h.genagegroup
				,h.reportingindicator
				,h.hcccmb as hcc
				,r1.Weight 
			from #ahu_hccrankcmb h
			join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
				r1.VariableName=h.hcccmb and
				h.productline=r1.ProductLine and 
				r1.variabletype='HCC' and
				r1.Model='PUCD' and 
				r1.ReportingIndicator=h.reportingindicator  and 
				r1.Measure_ID='AHU'
		)t1 
		group by 
			EMPI
			,payer
			,meas
	)t2
)t3


Update #ahudataset set PUCDAgeGenWt=0,PUCDComorbidWt=0
update #ahudataset set PUCDAgeGenWt=p.PUCDAgeGenWt from #ahudataset ds join #ahu_pucdagegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;
update #ahudataset set PUCDComorbidWt=p.PUCDComorbidWt from #ahudataset ds join #ahu_pucdcomorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;
update #ahudataset set PUCDComorbidWt=1 where Outlier=0 and PUCDComorbidWt=0



-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	INSERT INTO HDS.HEDIS_MEASURE_OUTPUT(Memid,Meas,Payer,Epop,Discharges,Outlier,PPDComorbidWt,PPDAgeGenWt,PUCDComorbidWt,PUCDAgeGenWt,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	select EMPI,Meas,Payer,Epop,Discharges,Outlier,PPDComorbidWt,PPDAgeGenWt,PUCDComorbidWt,PUCDAgeGenWt,Age,Gender,@meas,@meas_year,@reportId,@rootId from #ahudataset


	/*

	-- Insert data into Measure Detailed Line

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService,DischargeDate)
	Select distinct
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id
		,@measurename
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,0 as Excl
		,outlier as Rexcl
		,0 as CE
		,0 as Event
		,Epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,'Readmission' as Code
		,re.ReadmissionDate as DateofService
		,b.DIS_DATE as DischargeDate
	From #pcrdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on 
		d.EMPI=a.EMPI and 
		a.Reportid=@reportId
	Left outer Join #pcr_readmissionset re on
		d.EMPI=re.EMPI and 
		SUBSTRING(d.meas,1,4)=concat('PCR',char(64+re.staynumber))
	Left outer join #pcr_base1 b on
		d.EMPI=b.EMPI and
		SUBSTRING(d.meas,1,4)=b.meas

	

	
		-- Insert data into Provider Scorecard
		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select Distinct
		Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,NUM_COUNT
		,DEN_COUNT
		,Excl_Count
		,Rexcl_Count
		,Gaps
		,Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	,Case
		when Result > Target then Floor((Den_excl*Target)/100)-NUM_COUNT
		when Result <= Target then 0
	end as To_Target
From
(
	Select 
		Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,NUM_COUNT as NUM_COUNT
		,DEN_COUNT as DEN_COUNT
		,EXCL_COUNT as Excl_Count
		,Rexcl_count as Rexcl_Count
		,NUM_COUNT as Gaps
		,Case
			when Den_excl>0 Then Round((cast(NUM_COUNT as decimal)/Den_excl)*100,2)
			else 0
		end as Result
		,@target as Target
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,DEN_EXCL
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,count(case when NUM=1 and Rexcl=0 Then 1 end) as NUM_COUNT
			,count(case when Den=1 Then 1 end) as DEN_COUNT
			,count(case when Den=1 and Rexcl=0 Then 1 end) Den_excl
			,count(case when excl=1 Then 1 end) as EXCL_COUNT
			,count(case when rexcl=1 Then 1 end) as Rexcl_count
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From RPT.MEASURE_DETAILED_LINE
		where Enrollment_Status='Active' and
			  MEASURE_ID=@measure_id and
			  REPORT_ID=@reportId
		Group by Measure_id,Measure_Name,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id
	)t1
	
)t2


	
GO


*/

GO
