SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




CREATE proc [HDS].[KPI_HEDIS_COA] 
AS


-- Declare Variables

Declare @rundate date='2022-02-01'

declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='COA-S'


Declare @meas varchar(10)='COA';
Declare @ce_startdt Date


Declare @ce_startdt1 Date
Declare @ce_novdt Date
Declare @ce_enddt Date
Declare @ce_enddt1 Date
Declare @runid INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;


-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
--Set @measurename='Disease-Modifying Anti-Rheumatic Drug Therapy for Rheumatoid Arthritis'
--Set @startDate='2020-01-01';
--Set @enddate='2020-12-31';
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--set @target=84
Set @domain='Clinical Quality / Wellness and Prevention'
--Set @subdomain='Rheumatoid Arthritis'
Set @measuretype='UHN'
--Set @measure_id='16'

-- Eligible Patient List

drop table if exists #coa_memlist; 
CREATE TABLE #coa_memlist (
    EMPI varchar(100)
    
)
insert into #coa_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) >=66



-- Create Temp Patient Enrollment
drop table if exists #coa_tmpsubscriber;
CREATE TABLE #coa_tmpsubscriber 
(
    EMPI varchar(100),
    dob Date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #coa_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth)
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE 
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) >=66
	ORDER BY 
		en.EMPI
		,en.EFF_DATE
		,en.TERM_DATE;



-- Create artdataset
drop table if exists #coadataset;
CREATE TABLE #coadataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  gender varchar(45) NOT NULL,
  [age] INT DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
  
  
) ;
Insert into #coadataset(EMPI,meas,payer,age,gender)
Select distinct
	EMPI
	,'COA1'
	,pm.PayerMapping
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','MR'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				when (Payer in('MDE') and nxtpayer in('MCR'))  then 'MDE-MCR'
				When (Payer in('MDE') and prvpayer in('MCR'))  then 'MDE-MCR'
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #coa_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
					--and EMPI in(100559,100732)
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='COA'
Order by 1


Insert into #coadataset
Select EMPI,'COA2',payer,gender,age,orec,lis,rexcl,rexcld,CE,excl,num,Event from #coadataset where meas='COA1'

Insert into #coadataset
Select EMPI,'COA3',payer,gender,age,orec,lis,rexcl,rexcld,CE,excl,num,Event from #coadataset where meas='COA1'

Insert into #coadataset
Select EMPI,'COA4',payer,gender,age,orec,lis,rexcl,rexcld,CE,excl,num,Event from #coadataset where meas='COA1'


-- Continuous Enrollment
	
	
DROP TABLE IF EXISTS #coa_contenroll;
CREATE table #coa_contenroll
(
	EMPI varchar(100)
);
Insert into #coa_contenroll
Select
	EMPI
From GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0)

update #coadataset set CE=1 from #coadataset ds join #coa_contenroll ce on ds.EMPI=ce.EMPI;
	




-- Numerator Conditions
-- Advance Care Planning

DROP TABLE IF EXISTS #coa_acplist;
CREATE table #coa_acplist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
	

);
Insert into #coa_acplist
select distinct 
	EMPI 
	,ServiceDate
	,Code
from
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedureswithoutMods(@rootId,@ce_startdt,@ce_enddt,'Advance Care Planning','CPT CAT II Modifier')
	

	Union all

	Select 
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Advance Care Planning')

)t1


update #coadataset set num=1 from #coadataset ds join #coa_acplist ce on ds.EMPI=ce.EMPI and ds.meas='COA1'




-- Medication Review

DROP TABLE IF EXISTS #coa_medicationreviewlist;
CREATE table #coa_medicationreviewlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20),
	DATA_SRC varchar(50),
	CLAIM_ID varchar(100),
	SV_LINE varchar(20)
);

Insert into #coa_medicationreviewlist
Select
	EMPI
	,PROC_START_DATE as ServiceDate
	,PROC_CODE as Code
	,PROC_DATA_SRC as DATA_SRC
	,CLAIM_ID
	,SV_LINE
From GetProcedureswithoutMods(@rootId,@ce_startdt,@ce_enddt,'Medication Review','CPT CAT II Modifier')




DROP TABLE IF EXISTS #coa_medicationlist;
CREATE table #coa_medicationlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50),
	SV_LINE varchar(50)
);
Insert into #coa_medicationlist
Select
	EMPI
	,CLAIM_ID
	,PROC_DATA_SRC as DATA_SRC
	,SV_LINE
From GetProcedureswithoutMods(@rootId,@ce_startdt,@ce_enddt,'Medication List','CPT CAT II Modifier')




drop table if exists #coa_TCMSlist
CREATE table #coa_TCMSlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20),
	DATA_SRC varchar(50),
	CLAIM_ID varchar(100),
	SV_LINE varchar(50)
	
);
Insert into #coa_TCMSlist
Select
	EMPI
	,PROC_START_DATE as ServiceDate
	,PROC_CODE as Code
	,PROC_DATA_SRC as DATA_SRC
	,CLAIM_ID
	,SV_LINE
From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Transitional Care Management Services')




drop table if exists #coa_acuteiplist;
CREATE table #coa_acuteiplist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	servicedate date,
	DATA_SRC varchar(50),
	SV_LINE varchar(50)
);
Insert into #coa_acuteiplist
select distinct 
	EMPI
	,CLAIM_ID
	,ServiceDate 
	,DATA_SRC
	,SV_LINE
from
(
	Select
		EMPI
		,CLAIM_ID
		,PROC_START_DATE as ServiceDate
		,PROC_DATA_SRC as DATA_SRC
		,SV_LINE
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Acute Inpatient')

	Union ALL

	Select
		EMPI
		,CLAIM_ID
		,FROM_DATE as ServiceDate
		,CL_DATA_SRC as DATA_SRC
		,SV_LINE
	From CLAIMLINE
	Where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0') in 
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient POS'
		)

	Union all

	Select
		EMPI
		,NULL as CLAIM_ID
		,VisitDate as ServiceDate
		,Visit_DATA_SRC as DATA_SRC
		,0 as SV_LINE
	From Visit
	Where
		ROOT_COMPANIES_ID=@rootId and
		VisitDate Between @ce_startdt and @ce_enddt and
		VisitTypeCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient'
		)
)t1




drop table if exists #coa_mrnumlist;
CREATE table #coa_mrnumlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
);
Insert into #coa_mrnumlist
Select distinct
	*
From
(

	select 
		t3.EMPI 
		,t3.ServiceDate
		,t3.Code
	from
	(
		select 
			t1.EMPI
			,t1.CLAIM_ID
			,t1.ServiceDate
			,t1.Code
			,t1.DATA_SRC
			,t1.SV_LINE
		from #coa_medicationreviewlist t1
		join #coa_medicationlist t2 on 
			t2.EMPI=t1.EMPI and 
			t1.CLAIM_ID=t2.CLAIM_ID and
			t1.DATA_SRC=t2.DATA_SRC
			
	--	where t1.EMPI=101584
	
		Union all
	
		Select 
			EMPI
			,CLAIM_ID
			,ServiceDate
			,Code
			,DATA_SRC
			,SV_LINE
		from #coa_TCMSlist
	--	where EMPI=101584
		
	)t3
	left outer Join #coa_acuteiplist ip on 
		t3.EMPI=ip.EMPI and 
		t3.CLAIM_ID=ip.CLAIM_ID and
		t3.DATA_SRC=ip.DATA_SRC
	where 
		ip.EMPI is null and
		t3.CLAIM_ID is not null 


	Union all

	select 
		t3.EMPI 
		,t3.ServiceDate
		,t3.Code
		
	from
	(
		select 
			t1.EMPI
			,t1.CLAIM_ID
			,t1.ServiceDate
			,t1.Code
			,t1.DATA_SRC
		from #coa_medicationreviewlist t1
		join #coa_medicationlist t2 on 
			t2.EMPI=t1.EMPI and 
			t1.CLAIM_ID=t2.CLAIM_ID and
			t1.DATA_SRC=t2.DATA_SRC 
	--	Where t1.EMPI=101584
			
		
		
		
	
		Union all
	
		Select 
			EMPI
			,CLAIM_ID
			,ServiceDate
			,Code
			,DATA_SRC
		from #coa_TCMSlist
	--	where EMPI=101584
		
	)t3
	left outer Join #coa_acuteiplist ip on 
		t3.EMPI=ip.EMPI and 
		t3.ServiceDate=ip.ServiceDate and
		t3.DATA_SRC=ip.DATA_SRC and
		ip.CLAIM_ID is null and
		t3.CLAIM_ID is null
	where 
		ip.EMPI is null and
		t3.CLAIM_ID is null and
		ip.CLAIM_ID is null
		

)f1

update #coadataset set num=0 where meas='COA2'
update #coadataset set num=1 from #coadataset ds join #coa_mrnumlist n2 on ds.EMPI=n2.EMPI and ds.meas='COA2'



-- Functional Status Assessment

DROP TABLE IF EXISTS #coa_FSAlist;
CREATE table #coa_FSAlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20),
	DATA_SRC varchar(50),
	CLAIM_ID varchar(100)

);
Insert into #coa_FSAlist
select distinct 
	*
from
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
		,PROC_DATA_SRC as DATA_SRC
		,CLAIM_ID
	From GetProcedureswithoutMods(@rootId,@ce_startdt,@ce_enddt,'Functional Status Assessment','CPT CAT II Modifier')

	Union all
	
		Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		,DIAG_DATA_SRC as DATA_SRC
		,CLAIM_ID
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Functional Status Assessment')

		
)t1


drop table if exists #coa_coa3numlist
CREATE table #coa_coa3numlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
);
Insert into #coa_coa3numlist
Select distinct
	*
From
(

	select 
		t3.EMPI 
		,t3.ServiceDate
		,t3.Code
		
	from
	(
		Select
			*
		from #coa_FSAlist
		
		
	)t3
	left outer Join #coa_acuteiplist ip on 
		t3.EMPI=ip.EMPI and 
		t3.CLAIM_ID=ip.CLAIM_ID and
		t3.DATA_SRC=ip.DATA_SRC 
	where 
		ip.EMPI is null and
		t3.CLAIM_ID is not null 



	Union all

	select 
		t3.EMPI 
		,t3.ServiceDate
		,t3.Code
		
	from
	(
		Select
			*
		from #coa_FSAlist
		
		
		
	)t3
	left outer Join #coa_acuteiplist ip on 
		t3.EMPI=ip.EMPI and 
		t3.ServiceDate=ip.ServiceDate and
		t3.DATA_SRC=ip.DATA_SRC 
	where 
		ip.EMPI is null and
		t3.CLAIM_ID is null and
		ip.CLAIM_ID is null
		

)f1


update #coadataset set num=1 from #coadataset ds join #coa_coa3numlist n3 on ds.EMPI=n3.EMPI and ds.meas='COA3'


-- Pain Management
DROP TABLE IF EXISTS #coa_painmanagementlist;
CREATE table #coa_painmanagementlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20),
	DATA_SRC varchar(50),
	CLAIM_ID varchar(100)

);
Insert into #coa_painmanagementlist
select distinct 
	*
from
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
		,PROC_DATA_SRC as DATA_SRC
		,CLAIM_ID
	From GetProcedureswithoutMods(@rootId,@ce_startdt,@ce_enddt,'Pain Assessment','CPT CAT II Modifier')

	Union all
	
		Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		,DIAG_DATA_SRC as DATA_SRC
		,CLAIM_ID
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Pain Assessment')

		
)t1




DROP TABLE IF EXISTS #coa_coa4numlist;
CREATE table #coa_coa4numlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
);
Insert into #coa_coa4numlist
Select distinct
	*
From
(

	select 
		t3.EMPI 
		,t3.ServiceDate
		,t3.Code
	
	from
	(
		Select
			*
		from #coa_painmanagementlist
	
	)t3
	left outer Join #coa_acuteiplist ip on 
		t3.EMPI=ip.EMPI and 
		t3.CLAIM_ID=ip.CLAIM_ID and
		t3.DATA_SRC=ip.DATA_SRC 
	where 
		ip.EMPI is null and
		t3.CLAIM_ID is not null 



	Union all

	select 
		t3.EMPI 
		,t3.ServiceDate
		,t3.Code
		
	from
	(
		Select
			*
		from #coa_painmanagementlist
		
		
	)t3
	left outer Join #coa_acuteiplist ip on 
		t3.EMPI=ip.EMPI and 
		t3.ServiceDate=ip.ServiceDate and
		t3.DATA_SRC=ip.DATA_SRC 
	where 
		ip.EMPI is null and
		t3.CLAIM_ID is null and
		ip.CLAIM_ID is null
		

)f1

update #coadataset set num=1 from #coadataset ds join #coa_coa4numlist n4 on ds.EMPI=n4.EMPI and ds.meas='COA4'




-- Create Numerator details
Drop table if exists #coa_numdetails
Create Table #coa_numdetails
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20),
	meas varchar(10),
	rownumber INT
)
Insert into #coa_numdetails
Select
	*
From
(
	select 
		EMPI
		,ServiceDate
		,Code
		,'COA1' as meas
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
	from #coa_acplist

	Union all

	select 
		EMPI
		,ServiceDate
		,Code
		,'COA2' as meas
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
	from #coa_mrnumlist

	Union all

	select 
		EMPI
		,ServiceDate
		,Code
		,'COA3' as meas
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
	from #coa_coa3numlist

	Union all

	select 
		EMPI
		,ServiceDate
		,Code
		,'COA4' as meas
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
	from #coa_coa4numlist
)t1
Where
	rn=1




-- Hospice Exclusion

	drop table if exists #coa_hospicemembers;
	CREATE table #coa_hospicemembers
	(
		EMPI varchar(100)		
	);
	Insert into #coa_hospicemembers
	Select distinct
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)


	update #coadataset set rexcl=1 from #coadataset ds join #coa_hospicemembers hos on hos.EMPI=ds.EMPI;
	



	-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startdate=@startdate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 AND rexcl=0 and rexcld=0 and payer in('SN1','SN2','SN3','MMP','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #coadataset


/*

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID in('28','59','COA2','COA4') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
	
	-- Insert data into Measure Detailed Line
	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,Case
			When d.meas='COA1' then '59'
			When d.meas='COA3' then '28'
			else d.meas
		end as Measure_id
		,Case
			When d.meas='COA1' Then 'Completion of Advanced Care Planning for all patients >=65 years of age'
		--	when d.meas='COA2' Then 'Care of Older Adults: Medication Review'
			when d.meas='COA3' then 'Care of Older Adults: Functional Status'
		--	when d.meas='COA4' Then 'Care of Older Adults: Pain Assessment'
		end as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,Event
		,CASE 
			WHEN CE=1 AND rexcl=0 and rexcld=0 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #coadataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #coa_numdetails nd on d.EMPI=nd.EMPI and d.meas=nd.meas
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')




	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID in('28','59','COA2','COA4') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	-- Insert data into Provider Scorecard
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,Case
			When MEASURE_ID='28' Then 'Adult Wellness and Prevention'
			When MEASURE_ID='59' Then 'Advanced Care Planning'
		--	When MEASURE_ID='COA2' Then 'Medication Management'
		--	When MEASURE_ID='COA4' Then 'Adult Wellness and Prevention'
		end as MEASURE_SUBTITLE
		,Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,Case
			When MEASURE_ID='28' Then 96
			When MEASURE_ID='59' Then 80
		--	When MEASURE_ID='COA2' Then 50
		--	When MEASURE_ID='COA4' Then 50
		end as Target
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID in('28','59') and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target


*/
GO
