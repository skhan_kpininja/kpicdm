SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE proc [HDS].[KPI_HEDIS_HFS]
AS


--Declare vaiable
Declare @rundate date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='HFS-S'


Declare @ce_startdt Date
Declare @ce_startdt1 Date
Declare @ce_startdt2 Date
Declare @ce_dischstartdt Date
Declare @ce_enddt Date
Declare @ce_enddt1 Date
Declare @snf_enddt Date
Declare @ce_dischenddt Date
Declare @meas varchar(10);
Declare @runid INT=0;
Declare @reportId INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);



Set @meas='HFS';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_dischstartdt=concat(@meas_year,'-01-03');
SET @ce_enddt=concat(@meas_year,'-12-31');
Set @snf_enddt=concat(@meas_year,'-12-02');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_dischenddt=concat(@meas_year,'-12-01');

/*
Set @reporttype='Network'
Set @measurename='30 Day Readmission Rate ? All Cause'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=12
Set @domain='Utilization Management'
Set @subdomain='Readmission Rate'
Set @measuretype='UHN'
 Set @measure_id='40'

 */


-- Identify hospice exlcusion
 drop table if exists #hfs_hospicemembers;
CREATE table #hfs_hospicemembers
(
	EMPI varchar(100)
		
);
Insert into #hfs_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)






-- Identify ELigible Population

-- Step 1 - Identify all skilled nursing facility discharges on or between January 1 and November 1 of the measurement year
Drop table if exists #hfs_snfstays
Create Table #hfs_snfstays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50),
	SV_STAT varchar(10)
)
Insert into #hfs_snfstays
Select distinct
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
	,SV_STAT
From snfstays(@rootId,@ce_startdt,'3000-01-01')
Where
	CL_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1)



-- Step 2 Skilled nursing-to-skilled nursing direct transfers
drop table if exists #hfs_directtransfers;
Create table #hfs_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #hfs_snfstays
  Where
	ISNULL(SV_STAT,'O') in('P','A','O','')

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #hfs_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1
Where
	DischargeDate<=@ce_dischenddt and
	AdmissionDate!=DischargeDate



-- Create a normalized transfer table to improve lookup
drop table if exists #hfs_directtransfers_normalized;
Create table #hfs_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #hfs_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #hfs_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p

	

-- Step 4 Exclude skilled nursing facility discharges due to an acute hospital transfer
-- 4.1 Identify observation stays and inpatient stays
Drop table if exists #hfs_inpatientandobservationstays
Create Table #hfs_inpatientandobservationstays
(
	EMPI varchar(100),
	ADM_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #hfs_inpatientandobservationstays
Select distinct
	*
From
(
	Select
		EMPI
		,ADM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From inpatientstays(@rootId,@ce_startdt,@ce_enddt)
	Where
		CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)

	Union all

	Select
		EMPI
		,ADM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From observationstays(@rootId,@ce_startdt,@ce_enddt)
	Where
		CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)
)t1


--4.2 Identify Non acute stays
Drop table if exists #hfs_nonacutestays
Create Table #hfs_nonacutestays
(
	EMPI varchar(100),
	ADM_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #hfs_nonacutestays
Select
		EMPI
		,ADM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt,@ce_enddt)
Where
	CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)




--4.3 Exclude non acute stays from acute and observation
Drop table if exists #hfs_acutestays
Create Table #hfs_acutestays
(
	EMPI varchar(100),
	ADM_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #hfs_acutestays
Select
	i.*
From #hfs_inpatientandobservationstays i
left outer Join #hfs_nonacutestays na on
	i.EMPI=na.EMPI and
	i.CLAIM_ID=na.CLAIM_ID and
	i.CL_DATA_SRC=na.CL_DATA_SRC
Where
	na.EMPI is null


--4.4 exclude snfstays which results in transfer
drop table if exists #hfs_snfstayswithexclusions
Create table #hfs_snfstayswithexclusions
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
	StayNumber INT
)
Insert into #hfs_snfstayswithexclusions
Select
	s.*
	,ROW_NUMBER() over(Partition by s.EMPI order by DIS_DATE)
From #hfs_directtransfers_normalized s
left outer join #hfs_acutestays a on
	s.EMPI=a.EMPI and
	DATEDIFF(Day,s.DIS_DATE,a.ADM_DATE) between 0 and 1
Left outer join #hfs_hospicemembers h on
	s.EMPI=h.EMPI
Where 
	h.EMPI is null and
	a.EMPI is null


--Step 5 COntinuous Enrollment
-- Check for Continuous Enrollment from -365 to Dischargedate
drop table if exists #hfs_contenroll1;
create table #hfs_contenroll1
(
	EMPI varchar(50),
	staynumber INT,
	anchor INT
);
--Check for continuous enrollment
--CTE1 to check covergae during last 365 days
With coverage_CTE1(EMPI,DIS_DATE,staynumber,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select 
		t1.EMPI
		,DIS_DATE
		,staynumber
		,isnull(lag(TERM_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE desc),dateadd(day,-365,DIS_DATE)) as lastcoveragedate
		,Case 
			when EFF_DATE<dateadd(day,-365,DIS_DATE) then dateadd(day,-365,DIS_DATE)
			else EFF_DATE 
		end as Startdate
		,case 
			when TERM_DATE>DIS_DATE then DIS_DATE 
			else TERM_DATE 
		end as Finishdate
		,isnull(lead(EFF_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE),DIS_DATE) as nextcoveragedate
	from
	(
			Select distinct 
				s.EMPI
				,s.DIS_DATE
				,s.StayNumber
				,EFF_DATE
				,TERM_DATE
				,Rank() over(partition by s.EMPI,s.staynumber order by EFF_Date,Term_Date) as rn
			From #hfs_snfstayswithexclusions s
			join ENROLLMENT en on 
				s.EMPI=en.EMPI and 
				en.ROOT_COMPANIES_ID=@rootId
			where  
				en.EFF_DATE<=s.DIS_DATE and 
				en.TERM_DATE>=dateadd(day,-365,s.DIS_DATE)
	)t1					        
)
Insert into #hfs_contenroll1
select 
	t3.EMPI
	,t3.staynumber 
	,anchor
from
(
	select 
		t2.EMPI
		,t2.staynumber
		,sum(anchor) as anchor 
	from
	(
	
		select 
			*
			,case 
				when rn=1 and startdate>dateadd(day,-365,DIS_DATE) then 1 
				else 0 
			end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
			end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when DIS_DATE between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
		from
		(
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,staynumber order by Startdate,FinishDate),DIS_DATE) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by EMPI,staynumber order by Startdate,Finishdate) as rn 
			from coverage_CTE1
		)t1           
	)t2  
	group by 
		EMPI
		,DIS_DATE
		,staynumber 
	having
	(sum(gaps)+sum(startgap))<=1 and 
	sum(coveragedays)>=((datediff(day,dateadd(day,-365,DIS_DATE),DIS_DATE)+1)-45) 

)t3


-- Calculate Continuous enrollment from Discharge Date to +60 days
drop table if exists #hfs_contenroll2;
create table #hfs_contenroll2
(
	EMPI varchar(50),
	staynumber INT,
	Anchor INT
);
--Check for continuous enrollment
--CTE1 to check covergae during last 365 days
With coverage_CTE1(EMPI,DIS_DATE,staynumber,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select 
		t1.EMPI
		,DIS_DATE
		,staynumber
		,isnull(lag(TERM_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE desc),DIS_DATE) as lastcoveragedate
		,Case 
			when EFF_DATE<DIS_DATE then DIS_DATE
			else EFF_DATE 
		end as Startdate
		,case 
			when TERM_DATE>DATEADD(day,60,DIS_DATE) then DATEADD(day,60,DIS_DATE) 
			else TERM_DATE 
		end as Finishdate
		,isnull(lead(EFF_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE),DATEADD(day,60,DIS_DATE)) as nextcoveragedate
	from
	(
			Select distinct 
				s.EMPI
				,s.DIS_DATE
				,s.StayNumber
				,EFF_DATE
				,TERM_DATE
				,Rank() over(partition by s.EMPI,s.staynumber order by EFF_Date,Term_Date) as rn
			From #hfs_snfstayswithexclusions s
			join ENROLLMENT en on 
				s.EMPI=en.EMPI and 
				en.ROOT_COMPANIES_ID=@rootId
			where  
				en.EFF_DATE<=dateadd(day,60,s.DIS_DATE) and 
				en.TERM_DATE>=DIS_DATE
			
	)t1					        
)
Insert into #hfs_contenroll2
select 
	t3.EMPI
	,t3.staynumber 
	,t3.anchor
from
(
	select 
		t2.EMPI
		,t2.staynumber
		,sum(anchor) as anchor 
	from
	(
	
		select 
			*
			,case 
				when rn=1 and startdate>DIS_DATE then 1 
				else 0 
			end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
			end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when DIS_DATE between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
		from
		(
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,staynumber order by Startdate,FinishDate),DIS_DATE) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by EMPI,staynumber order by Startdate,Finishdate) as rn 
			from coverage_CTE1
		)t1           
	)t2  
	group by 
		EMPI
		,DIS_DATE
		,staynumber 
	having
	(sum(gaps)+sum(startgap))<=1 and 
	sum(coveragedays)>=(datediff(day,DIS_DATE,dateadd(day,60,DIS_DATE))+1) 

)t3


--Identify LTI members
Drop table if exists #hfs_lti
Create Table #hfs_lti
(
	EMPI varchar(100)
)
Insert into #hfs_lti
Select
	EMPI
From LTImembers(@rootId,@ce_startdt,@ce_enddt)



-- 5.3 Remove stays which do no qualify for continuous enrollment
-- Remove members whose age is less than 65
Drop table if exists #hfs_baselist
Create Table #hfs_baselist
(
	EMPI varchar(100),
	DIS_DATE Date,
	Age INT,
	Gender varchar(10),
	StayNumber INT
)
Insert into #hfs_baselist
Select
	EMPI
	,DIS_DATE
	,Age
	,Gender
	,StayNumber
From
(
	Select
		s.*
		,o.Gender
		,CASE 
			WHEN dateadd(year, datediff (year, Date_of_Birth,s.DIS_DATE), Date_of_Birth) > s.DIS_DATE THEN datediff(year, Date_of_Birth, s.DIS_DATE) - 1
			ELSE datediff(year, Date_of_Birth, s.DIS_DATE)
		END as Age
	From #hfs_snfstayswithexclusions s
	join open_empi_master o on
		s.EMPI=o.EMPI_ID and
		o.Root_Companies_ID=@rootId
	Join #hfs_contenroll2 ce2 on
		s.EMPI=ce2.EMPI and
		s.StayNumber=ce2.staynumber
	Join #hfs_contenroll1 ce1 on
		s.EMPI=ce1.EMPI and
		s.StayNumber=ce1.staynumber
	left outer join #hfs_lti l on
		s.EMPI=l.EMPI
	Where
		ce2.Anchor+ce1.anchor>=1 and
		l.EMPI is null
)t1
Where
	age>=65


-- Apply Payer Mapping Logic for PPlan
-- generating the ouput in required format
drop table if exists #hfsdataset;
CREATE TABLE #hfsdataset (
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [Epop] bit DEFAULT '0',
  [Num] bit DEFAULT '0',
  [DCC1] INT DEFAULT '0',
  [DCCWt1] float DEFAULT '0',
  [DCC2] INT DEFAULT '0',
  [DCCWt2] float DEFAULT '0',
  [DCC3] INT DEFAULT '0',
  [DCCWt3] float DEFAULT '0',
  [ComorbidWt] float DEFAULT '0',
  [AgeGenWt] float DEFAULT '0',
  [Age] Int DEFAULT NULL,
  [Gender] varchar(45) NULL,
  LIS INT DEFAULT 0,
  OREC INT DEFAULT NULL
    
) 
Insert into #hfsdataset(EMPI,meas,Payer,Age,Gender)
Select distinct
	EMPI
	,Concat('HFS',StayNumber,'A')
	,pm.PayerMapping
	,Age
	,Gender
From
(
	Select distinct
		EMPI
		,DIS_DATE
		,StayNumber
		,Age
		,Gender
		,case 
			when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB','MDE')) and staynumber=nxtstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB','MDE')) and staynumber=prvstaynumber then Payer
			When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=prvstaynumber then prvpayer
			when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=nxtstaynumber then Payer
			When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=prvstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')) and staynumber=prvstaynumber then prvpayer
			else Payer 
		end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer,1) over(partition by EMPI order by EMPI,staynumber),Payer) as nxtpayer
			,isnull(lag(Payer,1) over(partition by EMPI order by EMPI,staynumber),Payer) as prvpayer
			,isnull(lead(Staynumber,1) over(partition by EMPI order by EMPI,staynumber),Staynumber) as nxtStaynumber
			,isnull(lag(Staynumber,1) over(partition by EMPI order by EMPI,staynumber),Staynumber) as prvStaynumber
			
		From 
		(
			Select
				EMPI
				,DIS_DATE
				,StayNumber
				,PAYER_TYPE as Payer
				,Age
				,Gender
			From
			(
				Select
					*
					,RANK() over(partition by EMPI,DIS_DATE order by TERM_DATE Desc,EFF_DATE Desc) as rnk
				From
				(
					select 
						d.EMPI
						,d.DIS_DATE
						,d.StayNumber
						,en.PAYER_TYPE
						,EFF_DATE
						,TERM_DATE
						,Age
						,Gender
					From #hfs_baselist d
					join ENROLLMENT en on
						d.EMPI=en.EMPI and
						en.ROOT_COMPANIES_ID=@rootId
					Where
						EFF_DATE<=DATEADD(day,60,d.DIS_DATE) and
						TERM_DATE>=DATEADD(day,-365,d.DIS_DATE) 
					--	and d.EMPI=99551
						
				)t1	
			)t2
			Where
				rnk=1
		)t3
		
	)t4
)t5
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t5.newpayer=pm.payer and 
	pm.Measure_id='HFS'
Order by 1

Insert into #hfsdataset(EMPI,meas,Payer,Age,Gender)
Select distinct
	EMPI
	,Concat(substring(meas,1,4),'B')
	,Payer
	,Age
	,Gender
From #hfsdataset




--HFS Dicaharge cc

-- Create discharge cc base table
Drop table if exists #hfs_dischargelist
Create Table #hfs_dischargelist
(
	EMPI varchar(100),
	meas varchar(10),
	Payer varchar(20),
	Age INT,
	Gender varchar(10),
	DIS_DATE Date,
	StayNumber INT,
	DischargeClaimid varchar(100)
)
Insert into #hfs_dischargelist
Select
	ds.EMPI
	,ds.Meas
	,ds.Payer
	,ds.Age
	,ds.Gender
	,b.DIS_DATE
	,b.StayNumber
	,s.DischargeClaimId
From #hfsdataset ds
Join #hfs_baselist b on
	ds.EMPI=b.EMPI and
	substring(ds.meas,4,1)=b.StayNumber
join #hfs_snfstayswithexclusions s on
	b.EMPI=s.EMPI and 
	b.DIS_DATE=s.DIS_DATE




-- Discharge Condition
drop table if exists #hfs_dischcc;
create table #hfs_dischcc
(
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  DCC INT,
  dccct INT,
  weight float
  
)
insert into #hfs_dischcc
select 
	f2.EMPI
	,f2.Meas
	,f2.Payer
	,DCC
	,row_number() over(partition by EMPI,meas order by meas,DCC) as dccct
	,weight 
from
(
	select distinct 
		f1.*
		,isnull(r.Weight,0) as weight
		,Cast(replace(Comorbid_CC,'CC-','') as INT) as DCC 
	from
	(
		select 
			t2.EMPI
			,t2.Meas
			,t2.Payer
			,t2.Age
			,t2.Gender
			,cc.Comorbid_CC
			,'Medicare' as productline
			,case
				when substring(t2.meas,5,1)='A' Then '30 Day Hospitalization'
				else '60 Day Hospitalization'
			end as reportingindicator
		from
		(
				select distinct
					b.*
					,DIAG_CODE
				From #hfs_dischargelist b
				join diagnosis d on 
					b.EMPI=d.EMPI and
					d.DIAG_SEQ_NO=1 and
					d.ROOT_COMPANIES_ID=@rootId and
					d.CLAIM_ID=DischargeClaimId and
					d.DIAG_SEQ_NO=1
			--	where b.EMPI=97796

								
		)t2
		join HDS.HEDIS_TABLE_CC cc on 
			t2.DIAG_CODE=cc.DiagnosisCode and 
			cc.CC_type='Shared'
		
)f1
left outer join HDS.HEDIS_RISK_ADJUSTMENT r on f1.Comorbid_CC=r.VariableName and 
											   f1.productline=r.ProductLine and 
											   f1.reportingindicator=r.ReportingIndicator and 
											   r.Measure_ID='HFS' and 
											   r.VariableType='DCC' and 
											   r.Model='Logistic'

)f2




update ds set ds.DCCWt1=o.weight,ds.DCC1=o.DCC from #hfsdataset ds join #hfs_dischcc o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and o.dccct=1
update ds set ds.DCCWt2=o.weight,ds.DCC2=o.DCC from #hfsdataset ds join #hfs_dischcc o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and o.dccct=2
update ds set ds.DCCWt3=o.weight,ds.DCC3=o.DCC from #hfsdataset ds join #hfs_dischcc o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and o.dccct=3


-- Comorbidities

--Identify visits for risk scoring
Drop table if exists #hfs_diagvisits;
Create Table #hfs_diagvisits
(
	EMPI varchar(100),
	ServiceDate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #hfs_diagvisits
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'Outpatient,Telephone Visits,Observation,ED,Nonacute Inpatient,Acute Inpatient')

	Union All 

	
	Select
		EMPI
		,FROM_DATE as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!=81 and
		FROM_DATE between @ce_startdt1 and @ce_dischenddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
		)

	Union All

	Select
		p.EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,p.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From Procedures p
	Join Claimline c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(C.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		ISNULL(POS,'0')!=81 and
		coalesce(c.DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_dischenddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
		)

	Union all


	Select
		EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!=81 and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_dischenddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		)

)t1



drop table if exists #hfs_diagnosislist;
Create table #hfs_diagnosislist
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT,
	DIAG_DATA_SRC varchar(50)

)
Insert into #hfs_diagnosislist
Select distinct
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO
	,DIAG_DATA_SRC
From Diagnosis
Where
	ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	DIAG_START_DATE between @ce_startdt2 and @ce_enddt


drop table if exists #hfs_diagnosis;
Create table #hfs_diagnosis
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT

)
Insert into #hfs_diagnosis
Select
	d.EMPI
	,d.diagnosis
	,v.Servicedate
	,d.CLAIM_ID
	,d.DIAG_SEQ_NO
From #hfs_diagnosislist d
Join #hfs_diagvisits v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DIAG_DATA_SRC=v.DATA_SRC



-- HCC RANK
drop table if exists #hfs_hccrank;
Create table #hfs_hccrank
(
	EMPI varchar(100),
	Meas varchar(20),
	payer varchar(20),
	genagegroup varchar(20),
	reportingindicator varchar(60),
	productline  varchar(20),
	hcc  varchar(10)
	
)
Insert into #hfs_hccrank
select 
	t3.EMPI
	,t3.Meas
	,t3.payer
	,case 
		when age between 65 and 74 and gender='F' then 'F_65-74'
		when age between 75 and 84 and gender='F' then 'F_75-84'
		when age >=85 and gender='F' then 'F_85+'
		when age between 65 and 74 and gender='M' then 'M_65-74'
		when age between 75 and 84 and gender='M' then 'M_75-84'
		when age >=85 and gender='M' then 'M_85+'
	end as genagegroup
	,case
		when substring(meas,5,1)='A' Then '30 Day Hospitalization'
		else '60 Day Hospitalization'
	end as reportingindicator
	,'Medicare' as productline,
	hcc
from
(
	select distinct 
		EMPI
		,Meas
		,payer
		,HCC
		,Age
		,gender
		,calcrank 
	from 
	(
		select 
			EMPI
			,Meas
			,payer
			,cc.Comorbid_CC
			,hcc.RankingGroup
			,isnull(hcc.Rank,1) as Rank
			,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC
			,Rank() over(partition by EMPI,meas,RankingGroup order by RankingGroup,Rank) as calcrank
			,age
			,gender  
		from
		(
			select 
				b.*
				,d.diagnosis
				,'Shared' as producttype 
			from #hfs_diagnosis d
			Join #hfs_dischargelist b on 
				d.EMPI=b.EMPI and
				d.DiagnosisDate between dateadd(day,-365,b.DIS_DATE) and b.DIS_DATE
			Where
				--b.empi=132109 and
				(Case when b.DIS_DATE=DiagnosisDate and DIAG_SEQ_NO=1 and dischargeClaimid=d.CLAIM_ID then 1 else 0 end)!=1
			
		)t1
		left outer join HDS.HEDIS_TABLE_CC cc on 
			t1.diagnosis=cc.diagnosiscode and 
			t1.producttype=cc.CC_type
		left outer join HDS.HEDIS_HCC_RANK hcc on 
			cc.Comorbid_CC=hcc.CC and 
			t1.producttype=hcc.hcc_type
		where 
			Comorbid_CC is not null
	)t2 
	where 
		calcrank=1
 --memid=100035 and meas='PCRA'
)t3






-- HCC RANK COMB
drop table if exists #hfs_hccrankcmb;
Create table #hfs_hccrankcmb
(
	EMPI varchar(50),
	Meas varchar(10),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(60),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #hfs_hccrankcmb
select distinct 
	h.*
	,cmb.hcccomb 
from #hfs_hccrank h 
left outer join HDS.HEDIS_HCC_COMB cmb on 
	cmb.ComorbidHCC1 in
	(
		select 
			HCC 
		from #hfs_hccrank t1 
		where 
			t1.EMPI=h.EMPI and 
			t1.productline=h.productline and 
			t1.reportingindicator=h.reportingindicator and 
			t1.Meas=h.Meas and 
			t1.payer=h.payer
	) 
	and 
	cmb.ComorbidHCC2 in
	(
		select 
			HCC 
		from #hfs_hccrank t1 
		where 
			t1.EMPI=h.EMPI and 
			t1.productline=h.productline and 
			t1.reportingindicator=h.reportingindicator and 
			t1.Meas=h.Meas and 
			t1.payer=h.payer
	)




drop table if exists #hfs_agegenwt;
Create table #hfs_agegenwt
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	AgeGenWt float
)
Insert into #hfs_agegenwt
select distinct 
	t1.EMPI
	,t1.Meas
	,t1.payer
	,r1.Weight as AgeGenWt 
from
(
	select 
		EMPI
		,Meas
		,payer
		,case 
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85+'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85+'
		end as genagegroup
		,case
			when substring(meas,5,1)='A' Then '30 Day Hospitalization'
			else '60 Day Hospitalization'
		end as reportingindicator
		,'Medicare' as productline
	from #hfsdataset 
	
		--and memid=100021          
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	t1.productline=r1.ProductLine and 
	r1.variabletype='DEMO' and 
	r1.Model='Logistic' and 
	r1.Measure_id='HFS' and 
	r1.reportingindicator=t1.reportingindicator


 
drop table if exists #hfs_comorbidwt;
Create table #hfs_comorbidwt
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ComorbidWt float
	
)
Insert into #hfs_comorbidwt
select 
	EMPI
	,Meas
	,payer
	,Round(sum(weight),4) as ComorbidWt 
from
(
	select distinct 
		h.EMPI
		,h.Meas
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcc as hcc
		,r1.Weight 
	from #hfs_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcc and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='Logistic' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='HFS'

	Union all

	select distinct 
		h.EMPI
		,h.meas
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcccmb as hcc
		,r1.Weight 
	from #hfs_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcccmb and
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='Logistic' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='HFS'

)t1 
group by 
	EMPI,Meas,payer


Update #hfsdataset set AgeGenWt=0,ComorbidWt=0
update ds set ds.AgeGenWt=p.AgeGenWt from #hfsdataset ds join #hfs_agegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.Meas=ds.Meas
update ds set ds.ComorbidWt=p.ComorbidWt from #hfsdataset ds join #hfs_comorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.Meas=ds.Meas


-- Numerator 

-- Step 1.1 -- Identify Inpatient and Observation Stay 
drop table if exists #hfs_num_staylist;
CREATE table #hfs_num_staylist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
				
)
Insert into #hfs_num_staylist
Select distinct
	EMPI
	,FROM_DATE
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
	,SV_STAT
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	ISNULL(SV_STAT,'O') in('P','A','O','') and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay','Observation Stay')
	)


--Identify non-acute stays
Drop table if exists #hfs_num_nonacutestays
Create Table #hfs_num_nonacutestays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #hfs_num_nonacutestays
Select
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,'1900-01-01','3000-01-01')


-- Step 1.3 -- Identify only Acute and observation Discharges and remove non acute stays
drop table if exists #hfs_num_acuteandobservationstays;
CREATE table #hfs_num_acuteandobservationstays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
		
)
-- removing non acute inpatient stays from total Inpatient stays
Insert into #hfs_num_acuteandobservationstays
select 
	t1.EMPI
	,t1.FROM_DATE
	,t1.ADM_DATE
	,t1.DIS_DATE
	,t1.CLAIM_ID
	,t1.SV_STAT
from #hfs_num_staylist t1
left outer join #hfs_num_nonacutestays t2 on 
	t1.EMPI=t2.EMPI and 
	t1.CLAIM_ID=t2.CLAIM_ID
Where 
	t2.EMPI is null




-- identify redamission within 30,60 days
Drop table if exists #hfs_readmissions
Create Table #hfs_readmissions
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
)
Insert into #hfs_readmissions
Select distinct
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
From
(
	Select
		r.EMPI
		,meas
		,Payer
		,r.CLAIM_ID
		,r.ADM_DATE
		,r.DIS_DATE
		,rank() over(partition by s.EMPI,s.DIS_DATE order by r.ADM_DATE) as r1
		,rank() over(partition by s.EMPI,r.ADM_DATE order by s.DIS_DATE desc) as r2
	From #hfs_dischargelist s
	Join #hfs_num_acuteandobservationstays r on
		s.EMPI=r.EMPI and
		DATEDIFF(day,s.DIS_DATE,r.ADM_DATE) between 0 and 60
)t1
Where
	r1=1 and
	r2=1



drop table if exists #hfs_num_directtransfers;
Create table #hfs_num_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #hfs_readmissions
  

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #hfs_num_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1



-- Create a normalized transfer table to improve lookup
drop table if exists #hfs_num_directtransfers_normalized;
Create table #hfs_num_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #hfs_num_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #hfs_num_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p


-- Step 3 Exclusions

--3.5.1 Identify visits with Acute Condition diagnosis
Drop table if exists #hfs_acuteconditions
Create Table #hfs_acuteconditions
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)

)
Insert into #hfs_acuteconditions
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Acute Condition')
Where
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	DIAG_SEQ_NO=1


-- Step 3.1 ?	Female members with a principal diagnosis of pregnancy (Pregnancy Value Set).
-- Step 3.2 ?	A principal diagnosis for a condition originating in the perinatal period (Perinatal Conditions Value Set). 
-- Step 3.3 ?	A principal diagnosis of maintenance chemotherapy (Chemotherapy Encounter Value Set). 
-- Step 3.4 ?	A principal diagnosis of rehabilitation (Rehabilitation Value Set). 
-- Stpe 3.5 ?	A potentially planned procedure (Potentially Planned Procedures Value Set, Potentially Planned Post-Acute Care Hospitalization Value Set) without a principal acute diagnosis (Acute Condition Value Set).
Drop table if exists #hfs_plannedstays
Create Table #hfs_plannedstays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)

)
Insert into #hfs_plannedstays
Select distinct
	*
From
(
	Select 
		EMPI
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Chemotherapy Encounter,Rehabilitation,Kidney Transplant,Bone Marrow Transplant,Organ Transplant Other Than Kidney,Perinatal Conditions')
	Where
		DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		DIAG_SEQ_NO=1

	Union all

	Select 
		EMPI
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Pregnancy') t
	Join open_empi_master o on
		t.EMPI=EMPI_ID and
		o.Gender='F' and
		o.Root_Companies_ID=@rootId
	Where
		DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		DIAG_SEQ_NO=1
		

	Union all

	Select 
		EMPI
		,CLAIM_ID
		,PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Kidney Transplant,Organ Transplant Other Than Kidney')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

	Union all

	Select 
		EMPI
		,CLAIM_ID
		,PROC_DATA_SRC
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Kidney Transplant,Bone Marrow Transplant,Organ Transplant Other Than Kidney,Introduction of Autologous Pancreatic Cells')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


	Union all

	Select 
		t.EMPI
		,t.CLAIM_ID
		,PROC_DATA_SRC
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Potentially Planned Procedures,Potentially Planned Post-Acute Care Hospitalization') t
	left outer join #hfs_acuteconditions a on
		t.EMPI=a.EMPI and
		t.CLAIM_ID=a.CLAIM_ID and
		t.PROC_DATA_SRC=a.DATA_SRC
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		a.EMPI is null
	
)t1



-- Exclude Planned stays from stays list
drop table if exists #hfs_num_readmissionlist;
Create table #hfs_num_readmissionlist
(
	EMPI varchar(100),
	ADM_DATE Date
)
Insert into #hfs_num_readmissionlist
Select distinct
	s.EMPI
	,ADM_DATE
From #hfs_num_directtransfers_normalized s
Left outer join #hfs_plannedstays p on
	s.EMPI=p.EMPI and
	p.CLAIM_ID in(s.CLAIM1,s.CLAIM2,s.CLAIM3,s.CLAIM4,s.CLAIM5)
Where
	p.EMPI is null


-- identify redamission within 30 days
Drop table if exists #hfs_num_30dayreadmissions
Create Table #hfs_num_30dayreadmissions
(
	EMPI varchar(100),
	meas varchar(10)
)
Insert into #hfs_num_30dayreadmissions
Select
	EMPI
	,meas
From
(
	Select
		s.* 
		,rank() over(partition by s.EMPI,s.DIS_DATE order by ADM_DATE) as r1
		,rank() over(partition by s.EMPI,ADM_DATE order by s.DIS_DATE desc) as r2
	From #hfs_dischargelist s
	Join #hfs_num_readmissionlist r on
		s.EMPI=r.EMPI and
		DATEDIFF(day,s.DIS_DATE,r.ADM_DATE) between 0 and 30
	Where
		substring(s.Meas,5,1)='A'
)t1
Where
	r1=1 and
	r2=1

update #hfsdataset set Num=0
Update ds
	set ds.Num=1
From #hfsdataset ds
Join #hfs_num_30dayreadmissions r on
	ds.EMPI=r.EMPI and
	ds.Meas=r.meas



-- identify redamission within 60 days
Drop table if exists #hfs_num_60dayreadmissions
Create Table #hfs_num_60dayreadmissions
(
	EMPI varchar(100),
	meas varchar(10)
)
Insert into #hfs_num_60dayreadmissions
Select
	EMPI
	,meas
From
(
	Select
		s.* 
		,rank() over(partition by s.EMPI,s.DIS_DATE order by ADM_DATE) as r1
		,rank() over(partition by s.EMPI,ADM_DATE order by s.DIS_DATE desc) as r2
	From #hfs_dischargelist s
	Join #hfs_num_readmissionlist r on
		s.EMPI=r.EMPI and
		DATEDIFF(day,s.DIS_DATE,r.ADM_DATE) between 0 and 60
	Where
		substring(s.Meas,5,1)='B'
)t1
Where
	r1=1 and
	r2=1


Update ds
	set ds.Num=1
From #hfsdataset ds
Join #hfs_num_60dayreadmissions r on
	ds.EMPI=r.EMPI and
	ds.Meas=r.meas



/*
	select * from #hfs_readmissions

Select EMPI,meas,Payer,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,AgeGenWt,ComorbidWt,Num from #hfsdataset where substring(Meas,5,1)='B'
except
Select Memid,meas,Payer,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,AgeGenWt,ComorbidWt,Num from HEDIS_SCORE_PCR where ROOT_COMPANIES_ID=@rootId and substring(Meas,5,1)='B'


Select Memid,meas,Payer,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,AgeGenWt,ComorbidWt from HEDIS_SCORE_PCR where ROOT_COMPANIES_ID=@rootId
except
Select EMPI,meas,Payer,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,AgeGenWt,ComorbidWt from #hfsdataset




Select EMPI,meas,Payer,DCC1,DCCWt1,DCC2,DCCWt2,AgeGenWt,ComorbidWt,Num from #hfsdataset where EMPI=102423 order by 2
Select Memid,meas,Payer,DCC1,DCCWt1,DCC2,DCCWt2,AgeGenWt,ComorbidWt,Num from HEDIS_SCORE_PCR where ROOT_COMPANIES_ID=@rootId and MemID=102423 order by 2

select * from #hfs_snfstayswithexclusions where EMPi=108127
select * from #hfs_num_acuteandobservationstays where empi=171515
select * from #hfs_num_directtransfers_normalized where empi=102423
select * from #hfs_directtransfers_normalized where empi=102423
Select * from #hfs_plannedstays where EMPI=102423
Select * from #hfs_readmissions where EMPI=144145
select * from #hfs_contenroll1 where EMPI=104505
select * from #hfs_contenroll2 where EMPI=104505
select * from #hfs_dischargelist where empi=95106
select * from #hfs_snfstays where empi=95106
select * from #hfs_acutestays where EMPi=95798
Select EMPI,CLAIM_ID,ADM_DATE,DIS_DATE,Rev_Code,UB_BILL_TYPE,SV_STAT,CL_DATA_SRC from claimline where root_Companies_id=@rootId and EMPi=95798
select * from ENROLLMENT where ROOT_COMPANIES_ID=@rootId and EMPi=95798
select * from #hfs_hospicemembers where EMPi=104505
select * from HDS.ValueSet_To_Code where code='0170'
select DATEADD(day,-365,'2021-03-15'),DATEADD(day,60,'2021-03-15')
select * from #hfs_lti where EMPI=95798
select * from MCFIELDS where ROOT_COMPANIES_ID=@rootId and EMPi=95798

*/

-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



	Delete from HDS.HEDIS_MEASURE_OUTPUT_PCR  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	INSERT INTO HDS.HEDIS_MEASURE_OUTPUT_PCR(MemID,Meas,Payer,Epop,Num,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,ComorbidWt,AgeGenWt,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	select EMPI,Meas,Payer,1,Num,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,ComorbidWt,AgeGenWt,Age,Gender,@meas,@meas_year,@reportId,@rootId from #hfsdataset


	/*

	-- Insert data into Measure Detailed Line

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService,DischargeDate)
	Select distinct
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id
		,@measurename
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,0 as Excl
		,outlier as Rexcl
		,0 as CE
		,0 as Event
		,Epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,'Readmission' as Code
		,re.ReadmissionDate as DateofService
		,b.DIS_DATE as DischargeDate
	From #pcrdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on 
		d.EMPI=a.EMPI and 
		a.Reportid=@reportId
	Left outer Join #pcr_readmissionset re on
		d.EMPI=re.EMPI and 
		SUBSTRING(d.meas,1,4)=concat('PCR',char(64+re.staynumber))
	Left outer join #pcr_base1 b on
		d.EMPI=b.EMPI and
		SUBSTRING(d.meas,1,4)=b.meas

	

	
		-- Insert data into Provider Scorecard
		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select Distinct
		Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,NUM_COUNT
		,DEN_COUNT
		,Excl_Count
		,Rexcl_Count
		,Gaps
		,Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	,Case
		when Result > Target then Floor((Den_excl*Target)/100)-NUM_COUNT
		when Result <= Target then 0
	end as To_Target
From
(
	Select 
		Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,NUM_COUNT as NUM_COUNT
		,DEN_COUNT as DEN_COUNT
		,EXCL_COUNT as Excl_Count
		,Rexcl_count as Rexcl_Count
		,NUM_COUNT as Gaps
		,Case
			when Den_excl>0 Then Round((cast(NUM_COUNT as decimal)/Den_excl)*100,2)
			else 0
		end as Result
		,@target as Target
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,DEN_EXCL
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,count(case when NUM=1 and Rexcl=0 Then 1 end) as NUM_COUNT
			,count(case when Den=1 Then 1 end) as DEN_COUNT
			,count(case when Den=1 and Rexcl=0 Then 1 end) Den_excl
			,count(case when excl=1 Then 1 end) as EXCL_COUNT
			,count(case when rexcl=1 Then 1 end) as Rexcl_count
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From RPT.MEASURE_DETAILED_LINE
		where Enrollment_Status='Active' and
			  MEASURE_ID=@measure_id and
			  REPORT_ID=@reportId
		Group by Measure_id,Measure_Name,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id
	)t1
	
)t2


	
GO


*/

GO
