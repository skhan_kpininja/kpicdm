SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE proc [HDS].[KPI_HEDIS_HPC]
AS


--Declare vaiable
Declare @rundate date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='HPC-S'


Declare @ce_startdt Date
Declare @ce_startdt1 Date
Declare @ce_startdt2 Date
Declare @ce_enddt Date
Declare @ce_enddt1 Date
Declare @meas varchar(10);
Declare @reportId INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);



Set @meas='HPC';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');

/*
Set @reporttype='Network'
Set @measurename='30 Day Readmission Rate ? All Cause'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=12
Set @domain='Utilization Management'
Set @subdomain='Readmission Rate'
Set @measuretype='UHN'
 Set @measure_id='40'

 */


-- Identify hospice exlcusion
drop table if exists #hpc_hospicemembers;
CREATE table #hpc_hospicemembers
(
	EMPI varchar(100)
		
);
Insert into #hpc_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)


-- Create Temp Patient Enrollment
drop table if exists #hpc_tmpsubscriber;
CREATE TABLE #hpc_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #hpc_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1 AND 
	YEAR(@ce_enddt)-YEAR(Date_of_birth) >=67
ORDER BY 
	en.EMPI,en.EFF_DATE,en.TERM_DATE;


-- Apply Continuous Enrollment
-- Continuous Enrollment
Drop table if exists #hpc_contenroll
Create table #hpc_contenroll
(
	EMPI varchar(100)
)
Insert into #hpc_contenroll
select 
	t1.EMPI
from GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0) t1
join
(
	select * from GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)
)t2 on t1.EMPI=t2.EMPI


-- LTI Exclusion
drop table if exists #hpc_LTImembers;
CREATE table #hpc_LTImembers
(
	EMPI varchar(100)
)
Insert into #hpc_LTImembers
SELECT DISTINCT EMPI FROM MCFIELDS WHERE ROOT_COMPANIES_ID=@rootId AND RunDate BETWEEN @ce_startdt AND @ce_enddt AND LTI=1;


-- Members with Institutinal SNP
drop table if exists #hpc_iSNPmembers;
CREATE table #hpc_iSNPmembers
(
	EMPI varchar(100)
)
Insert into #hpc_iSNPmembers
Select distinct
	EMPI
From #hpc_tmpsubscriber
Where
	StartDate<=@ce_enddt and
	EndDate>=@ce_startdt and
	payer='SN2'


-- Create dataset
drop table if exists #hpcdataset;
CREATE TABLE #hpcdataset (
  [EMPI] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [gender] varchar(45) NOT NULL,
  [age] INT,
  [discharges] smallint DEFAULT '0',
  [Epop] smallint DEFAULT '1',
  [Outlier] smallint DEFAULT '0',
  [PPDComorbidWt] float DEFAULT '0',
  [PPDAgeGenWt] float DEFAULT '0',
  [PUCDComorbidWt] float DEFAULT '1',
  [PUCDAgeGenWt] float DEFAULT '0',
    
) 
Insert into #hpcdataset(EMPI,meas,payer,gender,age)
Select distinct
	t4.EMPI
	,'HPCT'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #hpc_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='HPC'
join #hpc_contenroll ce on
	t4.EMPI=ce.EMPI
Left outer join #hpc_hospicemembers h on
	t4.EMPI=h.EMPI
Left outer join #hpc_iSNPmembers i on
	t4.EMPI=i.EMPI
Left outer join #hpc_LTImembers l on
	t4.EMPI=l.EMPI
Where
	h.EMPI is null and
	i.EMPI is null and
	l.EMPI is null
Order by 1

-- Add HPCA and HPCC
Insert into #hpcdataset(EMPI,meas,payer,gender,age)
Select EMPI,'HPCC',payer,gender,age from #hpcdataset where meas='HPCT'

Insert into #hpcdataset(EMPI,meas,payer,gender,age)
Select EMPI,'HPCA',payer,gender,age from #hpcdataset where meas='HPCT'


-- Chornic ACSC
-- Step 1.1 Identify all acute inpatient and observation stay discharges during the 
Drop table if exists #hpc_inpatientandobservationstays
Create Table #hpc_inpatientandobservationstays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50),
	SV_STAT varchar(10)
)
Insert into #hpc_inpatientandobservationstays
Select distinct
	*
From
(
	Select
		EMPI
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
		,CL_DATA_SRC
		,SV_STAT
	From inpatientstays(@rootId,@ce_startdt,@ce_enddt)
	Where
		CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)

	Union all

	Select
		EMPI
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
		,CL_DATA_SRC
		,SV_STAT
	From observationstays(@rootId,@ce_startdt,@ce_enddt)
	Where
		CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)
)t1


--1.2 Identify Non acute stays
Drop table if exists #hpc_nonacutestays
Create Table #hpc_nonacutestays
(
	EMPI varchar(100),
	ADM_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #hpc_nonacutestays
Select
		EMPI
		,ADM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt,@ce_enddt)
Where
	CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)




--1.3 Exclude non acute stays from acute and observation
Drop table if exists #hpc_acutestays
Create Table #hpc_acutestays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50),
	SV_STAT varchar(10)
)
Insert into #hpc_acutestays
Select
	i.*
From #hpc_inpatientandobservationstays i
left outer Join #hpc_nonacutestays na on
	i.EMPI=na.EMPI and
	i.CLAIM_ID=na.CLAIM_ID and
	i.CL_DATA_SRC=na.CL_DATA_SRC
Where
	na.EMPI is null


-- Step 2 Direct transfers: For discharges with one or more direct transfers, use the last discharge
drop table if exists #hpc_directtransfers;
Create table #hpc_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #hpc_acutestays
  Where
	ISNULL(SV_STAT,'O') in('P','A','O','')

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #hpc_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1




-- Create a normalized transfer table to improve lookup
drop table if exists #hpc_directtransfers_normalized;
Create table #hpc_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #hpc_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #hpc_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p

	

-- Step 3 For the remaining acute inpatient and observation stay discharges
-- Step 3.1 ?	Primary diagnosis of diabetes short-term complications (ketoacidosis, hyperosmolarity or coma; Diabetes Short Term Complications Value Set).
-- Step 3.2 ?	Primary diagnosis of diabetes with long-term complications (renal, eye, neurological, circulatory or unspecified complications; Diabetes Long Term Complications Value Set).
-- Step 3.3 ?	Primary diagnosis of uncontrolled diabetes (Uncontrolled Diabetes Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_diabetesevent';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
	INTO #hpc_diabetesevent
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes Short Term Complications,Diabetes Long Term Complications,Uncontrolled Diabetes')
Where
	DIAG_SEQ_NO=1

	
-- Step 3.4 ?	A procedure code for lower extremity amputation (Lower Extremity Amputation Procedures Value Set) with any diagnosis of diabetes (Diabetes Diagnosis Value Set). 
-- Step 3.4.1 -- Identify ?	A procedure code for lower extremity amputation (Lower Extremity Amputation Procedures Value Set)

EXEC SP_KPI_DROPTABLE '#hpc_lowerextremityamputation';
Select
	EMPI
	,CLAIM_ID
	,PROC_DATA_SRC
into #hpc_lowerextremityamputation
From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Lower Extremity Amputation Procedures')	


-- 	-- Step 3.4.2-- Identify ?	any diagnosis of diabetes (Diabetes Diagnosis Value Set). 
EXEC SP_KPI_DROPTABLE '#hpc_diabetesdiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_diabetesdiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes Diagnosis')


-- Step 3.4.3 ?	Exclude any discharge with a diagnosis of traumatic amputation of the lower extremity (Traumatic Amputation of Lower Extremity Value Set). 
EXEC SP_KPI_DROPTABLE '#hpc_traumaticamputaion';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_traumaticamputaion
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Traumatic Amputation of Lower Extremity')





-- Step 3.4.4 -- Identify Amputation event
EXEC SP_KPI_DROPTABLE '#hpc_amputationevent';
Select
	l.*
Into #hpc_amputationevent
From #hpc_lowerextremityamputation l
join #hpc_diabetesdiagnosis d on
	l.EMPI=d.EMPI and
	l.CLAIM_ID=d.CLAIM_ID and
	l.PROC_DATA_SRC=d.DIAG_DATA_SRC
Join #hpc_directtransfers_normalized dt on
	l.EMPI=dt.EMPI and
	l.CLAIM_ID in (CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)

Left Outer Join #hpc_traumaticamputaion t on
	l.EMPI=t.EMPI and
	t.CLAIM_ID in (CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5) and
	l.PROC_DATA_SRC=t.DIAG_DATA_SRC
Where 
	t.EMPI is null
	
	



-- Step 3.5 
-- Step 3.5.1 ?	?	Primary diagnosis of COPD (COPD Diagnosis Value Set)
EXEC SP_KPI_DROPTABLE '#hpc_copddiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
	INTO #hpc_copddiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'COPD Diagnosis')
Where
	DIAG_SEQ_NO=1

-- Step 3.5.2 ?	Exclude any discharge with a diagnosis of cystic fibrosis or anomaly of the respiratory system (Cystic Fibrosis and Respiratory System Anomalies Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_cysticfibrosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
	INTO #hpc_cysticfibrosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Cystic Fibrosis and Respiratory System Anomalies')


-- Step 3.5.3
EXEC SP_KPI_DROPTABLE '#hpc_copdevent';
Select
	d.*
into #hpc_copdevent
From #hpc_copddiagnosis d
Join #hpc_directtransfers_normalized dt on
	d.EMPI=dt.EMPI and
	d.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #hpc_cysticfibrosis c on
	d.EMPI=c.EMPI and
	c.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5) and
	d.DIAG_DATA_SRC=c.DIAG_DATA_SRC
Where
	c.EMPI is null


-- STep 3.6
-- Step 3.6.1 - ?	Primary diagnosis of asthma (Asthma Diagnosis Value Set). 
EXEC SP_KPI_DROPTABLE '#hpc_asthmadiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
	INTO #hpc_asthmadiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Asthma Diagnosis')
Where
	DIAG_SEQ_NO=1

-- Step 3.6.2 -- ?	Exclude any discharge with a diagnosis of cystic fibrosis or anomaly of the respiratory system (Cystic Fibrosis and Respiratory System Anomalies Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_asthmaevent';
Select
	d.*
into #hpc_asthmaevent
From #hpc_asthmadiagnosis d
Join #hpc_directtransfers_normalized dt on
	d.EMPI=dt.EMPI and
	d.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #hpc_cysticfibrosis c on
	d.EMPI=c.EMPI and
	c.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5) and
	d.DIAG_DATA_SRC=c.DIAG_DATA_SRC
Where
	c.EMPI is null


-- Step 3.7
-- Step 3.7.1 - ?	Primary diagnosis of heart failure (Heart Failure Diagnosis Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_heartfailurediagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
	INTO #hpc_heartfailurediagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Heart Failure Diagnosis')
Where
	DIAG_SEQ_NO=1


-- Step 3.7.2.1 ?	?	Exclude any discharge with a cardiac procedure (Cardiac Procedure Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_cardiacprocedure';
Select
	EMPI
	,CLAIM_ID
	,PROC_DATA_SRC
	INTO #hpc_cardiacprocedure
From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Cardiac Procedure')




-- Step 3.7.2.2 Exclude any discharge with a cardiac procedure (Cardiac Procedure Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_heartfailureevent';
Select
	d.*
into #hpc_heartfailureevent
From #hpc_heartfailurediagnosis d
Join #hpc_directtransfers_normalized dt on
	d.EMPI=dt.EMPI and
	d.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #hpc_cardiacprocedure c on
	d.EMPI=c.EMPI and
	c.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5) and
	d.DIAG_DATA_SRC=c.PROC_DATA_SRC
Where
	c.EMPI is null



-- Step 3.8
-- STep 3.8.1 - ?	Primary diagnosis of hypertension (Hypertension Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_hypertensiondiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
	INTO #hpc_hypertensiondiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Hypertension')
Where
	DIAG_SEQ_NO=1


-- Step 3.8.2.1 - ?	Exclude any discharge with a diagnosis of Stage I-IV kidney disease (Stage I-IV Kidney Disease Value Set) with a dialysis procedure (Dialysis Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_dialysis';
Select
	EMPI
	,CLAIM_ID
	,PROC_DATA_SRC
	into #hpc_dialysis
From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Dialysis')


-- STep 3.8.2.2 ?	Exclude any discharge with a diagnosis of Stage I-IV kidney disease (Stage I-IV Kidney Disease Value Set) with a dialysis procedure (Dialysis Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_kidneydisease';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
into #hpc_kidneydisease
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Stage I-IV Kidney Disease')


-- Step 3.8.2.3 - Exclude any discharge with a diagnosis of Stage I-IV kidney disease (Stage I-IV Kidney Disease Value Set) with a dialysis procedure (Dialysis Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_kidneywithdialysis';
Select
	d.*
Into #hpc_kidneywithdialysis
From #hpc_dialysis d
join #hpc_kidneydisease k on
	d.EMPI=k.EMPI and
	d.CLAIM_ID=k.CLAIM_ID and
	d.PROC_DATA_SRC=k.DIAG_DATA_SRC


-- Step 3.8.3 ?	Exclude any discharge with a cardiac procedure (Cardiac Procedure Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_hypertensionevent';
Select
	d.*
into #hpc_hypertensionevent
From #hpc_hypertensiondiagnosis d
Join #hpc_directtransfers_normalized dt on
	d.EMPI=dt.EMPI and
	d.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
left outer join #hpc_cardiacprocedure c on
	d.EMPI=c.EMPI and
	c.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5) and
	d.DIAG_DATA_SRC=c.PROC_DATA_SRC
Left outer join #hpc_kidneywithdialysis k on
	d.EMPI=k.EMPI and
	k.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5) and
	d.DIAG_DATA_SRC=k.PROC_DATA_SRC
Where --d.EMPI=117670 and
	c.EMPI is null and
	k.EMPI is null




-- Step 3.10 identify discharges with ACSC consitions
EXEC SP_KPI_DROPTABLE '#hpc_chronicacsc';
Select distinct
	*
into #hpc_chronicacsc
From
(
	Select * from #hpc_diabetesevent 

	Union all

	Select * from #hpc_amputationevent 

	Union all

	Select * from #hpc_copdevent 

	Union all

	Select * from #hpc_asthmaevent 

	Union all

	Select * from #hpc_heartfailureevent 

	Union all 

	Select * from #hpc_hypertensionevent 
)t1	




-- Step 3.10.2
EXEC SP_KPI_DROPTABLE '#hpc_chronicacscdischarges';
Select
	d.*
into #hpc_chronicacscdischarges
From #hpc_directtransfers_normalized d
join #hpc_chronicacsc c on
	d.EMPI=c.EMPI and
	c.CLAIM_ID in (d.CLAIM1,d.CLAIM2,d.CLAIM3,d.CLAIM4,d.CLAIM5)


-- Step 4 - Remove discharges for members with any three or more chronic ACSC discharges during the measurement year and report these members as chronic ACSC outliers. 
EXEC SP_KPI_DROPTABLE '#hpc_chronicacsc_outliers';
Select 
	EMPI 
	,count(distinct dis_date) as discharges
Into #hpc_chronicacsc_outliers
from #hpc_chronicacscdischarges 
group by 
	EMPI 



Update ds
	Set ds.Outlier=1
From #hpcdataset ds
join #hpc_chronicacsc_outliers o on
	ds.EMPI=o.EMPI and
	ds.meas='HPCC'
Where
	o.discharges>=3



Update ds
	Set ds.discharges=o.discharges
From #hpcdataset ds
join #hpc_chronicacsc_outliers o on
	ds.EMPI=o.EMPI and
	ds.meas='HPCC' and 
	ds.Outlier=0



-- Logic Acute ACSC 
--Step 3 For the remaining acute inpatient and observation stay discharges, identify discharges with any of the following on the discharge claim:
-- Step 3.1.1 --?	Primary diagnosis of bacterial pneumonia (Bacterial Pneumonia Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_pneumoniadiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_pneumoniadiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Bacterial Pneumonia')
Where
	DIAG_SEQ_NO=1

-- Step 3.1.2 ?	Exclude any discharge with a diagnosis of sickle cell anemia, HB S disease (Sickle Cell Anemia and HB S Disease Value Set
EXEC SP_KPI_DROPTABLE '#hpc_anemiadiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_anemiadiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Sickle Cell Anemia and HB S Disease')


-- Step 3.1.3 - ?	Exclude any discharge with a diagnosis of kidney/urinary tract disorder (Kidney and Urinary Tract Disorders Value Set). 
EXEC SP_KPI_DROPTABLE '#hpc_Immunocompromiseddiagnosis';
Select distinct
	*
Into #hpc_Immunocompromiseddiagnosis
From
(

	Select
		EMPI
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Immunocompromised State')

	Union all

	Select
		EMPI
		,CLAIM_ID
		,PROC_DATA_SRC
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Immunocompromised State')
)t1

-- Step 3.1 Exclude discharges with Immunocompromised , Sickle Cell Anemia and HB S Disease
EXEC SP_KPI_DROPTABLE '#hpc_pneumoniaevent';
Select
	d.*
Into #hpc_pneumoniaevent
From #hpc_pneumoniadiagnosis d
left outer Join #hpc_anemiadiagnosis a on
	d.EMPI=a.EMPI and
	d.CLAIM_ID=a.CLAIM_ID and
	d.DIAG_DATA_SRC=a.DIAG_DATA_SRC
Left Outer Join #hpc_Immunocompromiseddiagnosis i on
	d.EMPI=i.EMPI and
	d.CLAIM_ID=i.CLAIM_ID and
	d.DIAG_DATA_SRC=i.DIAG_DATA_SRC
Where
	a.EMPI is null and
	i.EMPI is null


-- Step 3.2 ?	Primary diagnosis of urinary tract infection (Urinary Tract Infection Value Set).
EXEC SP_KPI_DROPTABLE '#hpc_urinarytractdiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_urinarytractdiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Urinary Tract Infection')
Where
	DIAG_SEQ_NO=1

-- Step 3.2.1 ?	Exclude any discharge with a diagnosis of kidney/urinary tract disorder (Kidney and Urinary Tract Disorders Value Set). 
EXEC SP_KPI_DROPTABLE '#hpc_kidneyandurinarydiagnosis';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_kidneyandurinarydiagnosis
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Kidney and Urinary Tract Disorders')


-- Step 3.2 - Exclude Kidney and urinary Tract disorders and Immunocompromised conditions
EXEC SP_KPI_DROPTABLE '#hpc_urinarytractevent';
Select
	d.*
Into #hpc_urinarytractevent
From #hpc_urinarytractdiagnosis d
Left outer join #hpc_kidneyandurinarydiagnosis k on
	d.EMPI=k.EMPI and
	d.CLAIM_ID=k.CLAIM_ID and
	d.DIAG_DATA_SRC=k.DIAG_DATA_SRC
Left Outer Join #hpc_Immunocompromiseddiagnosis i on
	d.EMPI=i.EMPI and
	d.CLAIM_ID=i.CLAIM_ID and
	d.DIAG_DATA_SRC=i.DIAG_DATA_SRC
Where
	k.EMPI is null and
	i.EMPI is null


-- Step 3.3  ?	Primary diagnosis of cellulitis (Cellulitis Value Set).
-- Step 3.4 ?	Primary diagnosis of pressure ulcer (Pressure Ulcer Value Set). 
EXEC SP_KPI_DROPTABLE '#hpc_cellulitisandpressureulcerevent';
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #hpc_cellulitisandpressureulcerevent
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Cellulitis,Pressure Ulcer')
Where
	DIAG_SEQ_NO=1



-- Step 3.5 - Combine acute acsc events
EXEC SP_KPI_DROPTABLE '#hpc_acuteacsc';
Select distinct
	*
into  #hpc_acuteacsc
From
(
	Select * From #hpc_pneumoniaevent

	Union all

	Select * from #hpc_urinarytractevent

	Union all 

	Select * from #hpc_cellulitisandpressureulcerevent
)t1


-- Step 3.6
EXEC SP_KPI_DROPTABLE '#hpc_acuteacscdischarges';
Select
	d.*
Into #hpc_acuteacscdischarges
From #hpc_directtransfers_normalized d
Join #hpc_acuteacsc a on
	d.EMPI=a.EMPI and
	a.CLAIM_ID in (d.CLAIM1,d.CLAIM2,d.CLAIM3,d.CLAIM4,d.CLAIM5)



-- Step 4 - Remove discharges for members with any three or more acute ACSC discharges during the measurement year and report these members as acute ACSC outliers. 
EXEC SP_KPI_DROPTABLE '#hpc_acuteacsc_outliers';
Select 
	EMPI 
	,count(distinct dis_date) as discharges
Into #hpc_acuteacsc_outliers
from #hpc_acuteacscdischarges 
group by 
	EMPI 



Update ds
	Set ds.Outlier=1
From #hpcdataset ds
join #hpc_acuteacsc_outliers o on
	ds.EMPI=o.EMPI and
	ds.meas='HPCA'
Where
	o.discharges>=3



Update ds
	Set ds.discharges=o.discharges
From #hpcdataset ds
join #hpc_acuteacsc_outliers o on
	ds.EMPI=o.EMPI and
	ds.meas='HPCA' and 
	ds.Outlier=0


EXEC SP_KPI_DROPTABLE '#hpc_totalacsc_outliers';
Select 
	EMPI
	,Case
		When sum(Outlier)>0 Then 1
		else 0
	end as outlier
	,Case	
		when sum(Outlier)>0 Then 0
		else sum(discharges)
	end	 as discharges 
into #hpc_totalacsc_outliers
from #hpcdataset 
group by 
	EMPI

Update ds
	Set ds.Outlier=o.Outlier,ds.discharges=o.discharges
From #hpcdataset ds
Join #hpc_totalacsc_outliers o on
	ds.EMPI=o.EMPI and
	ds.meas='HPCT'



-- Applying Risk Adjustment logic

--Identify visits for risk scoring
Drop table if exists #hpc_diagvisits;
Create Table #hpc_diagvisits
(
	EMPI varchar(100),
	ServiceDate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #hpc_diagvisits
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Outpatient,Telephone Visits,Observation,ED,Nonacute Inpatient,Acute Inpatient')

	Union All

	Select
		EMPI
		,FROM_DATE as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		FROM_DATE between @ce_startdt1 and @ce_enddt1 and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
		)

	Union All

	Select
		p.EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,p.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From Procedures p
	Join Claimline c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(C.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		ISNULL(POS,'0')!='81' and
		coalesce(c.DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_enddt1 and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
		)

	Union all


	Select
		EMPI
		,DIS_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From inpatientstays(@rootId,@ce_startdt1,@ce_enddt1)
)t1



drop table if exists #hpc_diagnosislist;
Create table #hpc_diagnosislist
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT,
	DIAG_DATA_SRC varchar(50)

)
Insert into #hpc_diagnosislist
Select distinct
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO
	,DIAG_DATA_SRC
From Diagnosis
Where
	ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	DIAG_START_DATE between @ce_startdt2 and @ce_enddt1


drop table if exists #hpc_diagnosis;
Create table #hpc_diagnosis
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT

)
Insert into #hpc_diagnosis
Select
	d.EMPI
	,d.diagnosis
	,v.Servicedate
	,d.CLAIM_ID
	,d.DIAG_SEQ_NO
From #hpc_diagnosislist d
Join #hpc_diagvisits v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DIAG_DATA_SRC=v.DATA_SRC



-- HCC RANK
drop table if exists #hpc_hccrank;
Create table #hpc_hccrank
(
	EMPI varchar(100),
	meas varchar(10),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)
Insert into #hpc_hccrank
select 
	t2.EMPI
	,meas
	,t2.payer
	,case 
		when age between 67 and 74 and gender='F' then 'F_67-74'
		when age between 75 and 84 and gender='F' then 'F_75-84'
		when age >=85 and gender='F' then 'F_85'
		when age between 67 and 74 and gender='M' then 'M_67-74'
		when age between 75 and 84 and gender='M' then 'M_75-84'
		when age >=85 and gender='M' then 'M_85'
	end as genagegroup
	,case 
		When meas='HPCC' Then 'Chronic ACSC'
		When meas='HPCA' Then 'Acute ACSC'
		When meas='HPCT' Then 'Total ACSC'
	end as reportingindicator
	,'Medicare' as productline
	,hcc
from
(
	select distinct 
		EMPI
		,meas
		,HCC
		,cast(age as INT) as age
		,gender
		,payer 
	from 
	(
		select 
			d.EMPI
			,ds.meas
			,cc.Comorbid_CC
			,hcc.RankingGroup
			,isnull(hcc.Rank,1) as Rank
			,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC
			,Rank() over(partition by d.EMPI,RankingGroup order by Rank) as calcrank
			,ds.age
			,ds.gender
			,ds.payer 
		from #hpc_diagnosis d
		left outer join HDS.HEDIS_TABLE_CC cc on 
			d.diagnosis=cc.diagnosiscode and 
			cc.cc_type='Shared'
		left outer join HDS.HEDIS_HCC_RANK hcc on 
			cc.Comorbid_CC=hcc.CC and 
			hcc.hcc_type='Shared'
		join #hpcdataset ds on 
			ds.EMPI=d.EMPI and 
			ds.outlier=0
		where 
			Comorbid_CC is not null
	)t1 
	where 
		calcrank=1
)t2




-- HCC RANK COMB
drop table if exists #hpc_hccrankcmb;
Create table #hpc_hccrankcmb
(
	EMPI varchar(100),
	meas varchar(10),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #hpc_hccrankcmb
select 
	h.*
	,cmb.hcccomb 
from #hpc_hccrank h 
left outer join HDS.HEDIS_HCC_COMB cmb on 
	cmb.ComorbidHCC1 in
	(
		select 
			HCC 
		from #hpc_hccrank t1 
		where 
			t1.EMPI=h.EMPI
	) 
	and 
	cmb.ComorbidHCC2 in
	(
		select 
			HCC 
		from #hpc_hccrank t1 
		where 
			t1.EMPI=h.EMPI
	)
	and 
	cmb.hcc_type='Shared'

	

-- PPD Model
drop table if exists #hpc_ppdagegenwt;
Create table #hpc_ppdagegenwt
(
	EMPI varchar(100),
	meas varchar(10),
	payer varchar(10),
	PPDAgeGenWt float
	
)
Insert into #hpc_ppdagegenwt
select distinct 
	t1.EMPI
	,t1.meas
	,t1.payer
	,r1.Weight as PPDAgeGenWt 
from
(
	select 
		EMPI
		,meas
		,payer
		,case 
			when age between 67 and 74 and gender='F' then 'F_67-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 67 and 74 and gender='M' then 'M_67-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
			end as genagegroup
		,case 
			When meas='HPCC' Then 'Chronic ACSC'
			When meas='HPCA' Then 'Acute ACSC'
			When meas='HPCT' Then 'Total ACSC'
		end as reportingindicator
		,'Medicare' as productline
	from #hpcdataset 
	where 
		outlier=0 
	--	and EMPI=95012
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	t1.productline=r1.ProductLine and 
	t1.reportingindicator=r1.reportingindicator and 
	r1.variabletype='DEMO' and 
	r1.Model='PPD' and
	r1.Measure_id='HPC'




drop table if exists #hpc_ppdcomorbidwt;
Create table #hpc_ppdcomorbidwt
(
	EMPI varchar(100),
	meas varchar(10),
	payer varchar(10),
	PPDComorbidWt float
	
)
Insert into #hpc_ppdcomorbidwt
select
	EMPI
	,meas
	,payer
	,sum(weight) as PPDComorbidWt 
from
(
	select distinct 
		h.EMPI
		,h.meas
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcc as hcc
		,r1.Weight 
	from #hpc_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcc and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='PPD' and
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='HPC'

	Union all

	select distinct 
		h.EMPI
		,h.meas
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcccmb as hcc
		,r1.Weight 
	from #hpc_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcccmb and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='PPD' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='HFS'

)t1 
group by 
	EMPI
	,meas
	,payer

	
update #hpcdataset set PPDAgeGenWt=0
update #hpcdataset set PPDAgeGenWt=p.PPDAgeGenWt from #hpcdataset ds join #hpc_ppdagegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas
update #hpcdataset set PPDComorbidWt=p.PPDComorbidWt from #hpcdataset ds join #hpc_ppdcomorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;


-- PUCD Model

drop table if exists #hpc_pucdagegenwt;
Create table #hpc_pucdagegenwt
(
	EMPI varchar(50),
	meas varchar(10),
	payer varchar(10),
	PUCDAgeGenWt float
	
)
Insert into #hpc_pucdagegenwt
select distinct 
	t1.EMPI
	,t1.meas
	,t1.payer
	,r1.Weight
from
(
	select 
		EMPI
		,meas
		,payer
		,case 
			when age between 67 and 74 and gender='F' then 'F_67-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 67 and 74 and gender='M' then 'M_67-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
			end as genagegroup
		,case 
			When meas='HPCC' Then 'Chronic ACSC'
			When meas='HPCA' Then 'Acute ACSC'
			When meas='HPCT' Then 'Total ACSC'
		end as reportingindicator
		,'Medicare' as productline
	from #hpcdataset 
	where 
		outlier=0
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	r1.ReportingIndicator=t1.reportingindicator and
	t1.productline=r1.ProductLine and 
	r1.variabletype='DEMO' and
	r1.Model='PUCD'  and 
	r1.Measure_ID='HPC'



drop table if exists #hpc_pucdcomorbidwt;
Create table #hpc_pucdcomorbidwt
(
	EMPI varchar(100),
	meas varchar(10),
	payer varchar(10),
	PUCDComorbidWt float
	
)
Insert into #hpc_pucdcomorbidwt
select 
	EMPI
	,meas
	,payer
	,case 
		when decimalct>10 then round(PUCDomorbidWt,10,1) 
		else PUCDomorbidWt 
	end as PUCDomorbidWt 
from
(
	select 
		EMPI
		,meas
		,payer
		,PUCDomorbidWt
		,CASE Charindex('.',PUCDomorbidWt) 
			WHEN 0 THEN 0  
			ELSE Len (Cast(Cast(Reverse(CONVERT(VARCHAR(50),PUCDomorbidWt, 128)) AS FLOAT) AS BIGINT)) 
		END as decimalct 
	from
	(
		select 
			EMPI
			,meas
			,payer
			,EXP(SUM(LOG(weight))) as PUCDomorbidWt 
		from
		(
			select distinct 
				h.EMPI
				,h.meas
				,h.payer
				,h.genagegroup
				,h.reportingindicator
				,h.hcc as hcc
				,r1.Weight as weight 
			from #hpc_hccrankcmb h
			join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
				r1.VariableName=h.hcc and 
				h.productline=r1.ProductLine and 
				r1.variabletype='HCC' and 
				r1.Model='PUCD' and 
				r1.ReportingIndicator=h.reportingindicator  and 
				r1.Measure_ID='HPC'

			Union all

			select distinct 
				h.EMPI
				,h.meas
				,h.payer
				,h.genagegroup
				,h.reportingindicator
				,h.hcccmb as hcc
				,r1.Weight 
			from #hpc_hccrankcmb h
			join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
				r1.VariableName=h.hcccmb and
				h.productline=r1.ProductLine and 
				r1.variabletype='HCC' and
				r1.Model='PUCD' and 
				r1.ReportingIndicator=h.reportingindicator  and 
				r1.Measure_ID='HPC'
		)t1 
		group by 
			EMPI
			,meas
			,payer
	)t2
)t3



update #hpcdataset set PUCDAgeGenWt=p.PUCDAgeGenWt from #hpcdataset ds join #hpc_pucdagegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;
update #hpcdataset set PUCDComorbidWt=p.PUCDComorbidWt from #hpcdataset ds join #hpc_pucdcomorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.meas=ds.meas;;
update #hpcdataset set PUCDComorbidWt=0 where Outlier=1;


-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	INSERT INTO HDS.HEDIS_MEASURE_OUTPUT(MemID,Meas,Payer,Epop,Discharges,Outlier,PPDComorbidWt,PPDAgeGenWt,PUCDComorbidWt,PUCDAgeGenWt,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	select EMPI,Meas,Payer,Epop,Discharges,Outlier,PPDComorbidWt,PPDAgeGenWt,PUCDComorbidWt,PUCDAgeGenWt,Age,Gender,@meas,@meas_year,@reportId,@rootId from #hpcdataset


	/*

	-- Insert data into Measure Detailed Line

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService,DischargeDate)
	Select distinct
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id
		,@measurename
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,0 as Excl
		,outlier as Rexcl
		,0 as CE
		,0 as Event
		,Epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,'Readmission' as Code
		,re.ReadmissionDate as DateofService
		,b.DIS_DATE as DischargeDate
	From #pcrdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on 
		d.EMPI=a.EMPI and 
		a.Reportid=@reportId
	Left outer Join #pcr_readmissionset re on
		d.EMPI=re.EMPI and 
		SUBSTRING(d.meas,1,4)=concat('PCR',char(64+re.staynumber))
	Left outer join #pcr_base1 b on
		d.EMPI=b.EMPI and
		SUBSTRING(d.meas,1,4)=b.meas

	

	
		-- Insert data into Provider Scorecard
		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select Distinct
		Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,NUM_COUNT
		,DEN_COUNT
		,Excl_Count
		,Rexcl_Count
		,Gaps
		,Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	,Case
		when Result > Target then Floor((Den_excl*Target)/100)-NUM_COUNT
		when Result <= Target then 0
	end as To_Target
From
(
	Select 
		Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,NUM_COUNT as NUM_COUNT
		,DEN_COUNT as DEN_COUNT
		,EXCL_COUNT as Excl_Count
		,Rexcl_count as Rexcl_Count
		,NUM_COUNT as Gaps
		,Case
			when Den_excl>0 Then Round((cast(NUM_COUNT as decimal)/Den_excl)*100,2)
			else 0
		end as Result
		,@target as Target
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,DEN_EXCL
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,count(case when NUM=1 and Rexcl=0 Then 1 end) as NUM_COUNT
			,count(case when Den=1 Then 1 end) as DEN_COUNT
			,count(case when Den=1 and Rexcl=0 Then 1 end) Den_excl
			,count(case when excl=1 Then 1 end) as EXCL_COUNT
			,count(case when rexcl=1 Then 1 end) as Rexcl_count
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From RPT.MEASURE_DETAILED_LINE
		where Enrollment_Status='Active' and
			  MEASURE_ID=@measure_id and
			  REPORT_ID=@reportId
		Group by Measure_id,Measure_Name,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id
	)t1
	
)t2


	
GO


*/

GO
