SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE proc [HDS].[KPI_HEDIS_PCR]
AS


--Declare vaiable
Declare @rundate date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='PCR-A'
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @ce_startdt Date
Declare @ce_startdt1 Date
Declare @ce_startdt2 Date
Declare @ce_dischstartdt Date
Declare @ce_enddt Date
Declare @ce_enddt1 Date
Declare @snf_enddt Date
Declare @ce_dischenddt Date
Declare @meas varchar(10);
Declare @runid INT=0;
Declare @reportId INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);



Set @meas='PCR';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_dischstartdt=concat(@meas_year,'-01-03');
SET @ce_enddt=concat(@meas_year,'-12-31');
Set @snf_enddt=concat(@meas_year,'-12-02');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_dischenddt=concat(@meas_year,'-12-01');

Set @reporttype='Network'
Set @measurename='30 Day Readmission Rate ? All Cause'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=12
Set @domain='Utilization Management'
Set @subdomain='Readmission Rate'
Set @measuretype='UHN'
 Set @measure_id='40'




 

 -- Identify Plan Population
  Drop table if exists #pcr_memlist
 Create Table #pcr_memlist
 (
	EMPI varchar(100),
    dob Date,
	gender varchar(1),
	Age INT,
	payer varchar(50),
	StartDate Date,
	EndDate Date
)
Insert into #pcr_memlist
Select
	EMPI
	,Date_of_Birth
	,Gender
	,Age
	,Payer_type
	,EFF_DATE
	,TERM_DATE
From
(

	Select
		*
		,RANK() over(partition by EMPI order by yrrank desc,Year(EFF_DATE) desc,EFF_DATE asc,TERM_DATE asc) as rn 
	From
	(
		 Select distinct
			en.EMPI
			,Date_of_Birth
			,Gender
			,Year(@ce_startdt)-Year(Date_of_Birth) as age
			,Payer_Type
			,EFF_DATE
			,TERM_DATE
			,Case
				When @ce_startdt1 between EFF_DATE and Term_Date Then 2
				When @ce_startdt between EFF_DATE and Term_Date Then 1
				else 0
			end as yrrank
		From ENROLLMENT en
		Join open_empi_master oem on 
			en.EMPI=oem.EMPI_ID and
			en.ROOT_COMPANIES_ID=oem.Root_Companies_ID
		Where
			en.ROOT_COMPANIES_ID=@rootId and
			en.EFF_DATE<=@ce_dischenddt and
			en.TERM_DATE>=@ce_startdt1
			--and EMPI=141540
	)t1
	Where
		age>=18
)t2
Where
	rn=1


-- Apply Payer Mapping Logic for PPlan
Drop Table if exists #pcr_pplanpayermap
Create Table #pcr_pplanpayermap
(
	EMPI varchar(100),
	PPlan varchar(50)
)
Insert into #pcr_pplanpayermap
Select distinct
	EMPI
	,pm.PayerMapping
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From #pcr_memlist
		--	where EMPI=147936
		
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='PCR'
Order by 1



-- hospice Exclusion
drop table if exists #pcr_hospicemembers;
CREATE table #pcr_hospicemembers
(
	EMPI varchar(100)
		
)
Insert into #pcr_hospicemembers
select EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)


-- Step 1.1 -- Identify Inpatient and Observation Stay 
drop table if exists #pcr_staylist;
CREATE table #pcr_staylist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
				
)
Insert into #pcr_staylist
Select distinct
	EMPI
	,FROM_DATE
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
	,SV_STAT
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay','Observation Stay')
	)




-- Step 1.2 -- Identify Nonacute Inpatient Stay
drop table if exists #pcr_nonacutestays
CREATE table #pcr_nonacutestays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
				
)
-- Identify non acute Inpatient Stays
Insert into #pcr_nonacutestays
Select distinct
	EMPI
	,CLAIM_ID
From CLAIMLINE
where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(POS,'0')!='81' and 
	(
		RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in 
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
		) 
		or 
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
		)
	)



-- Step 1.3 -- Identify only Acute and observation Discharges and remove non acute stays
drop table if exists #pcr_acuteandobservationstays;
CREATE table #pcr_acuteandobservationstays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
		
)
-- removing non acute inpatient stays from total Inpatient stays
Insert into #pcr_acuteandobservationstays
select 
	t1.EMPI
	,t1.FROM_DATE
	,t1.ADM_DATE
	,t1.DIS_DATE
	,t1.CLAIM_ID
	,t1.SV_STAT
	from #pcr_staylist t1
	left outer join #pcr_nonacutestays t2 on 
		t1.EMPI=t2.EMPI and 
		t1.CLAIM_ID=t2.CLAIM_ID
	Where 
		t2.EMPI is null


-- Step 2 and 3 - Identify Direct Transfers , Select discharges between 1st Jan and 1st Dec , Exlcude same day stays
drop table if exists #pcr_directtransfers;
Create table #pcr_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #pcr_acuteandobservationstays
  Where
	ISNULL(SV_STAT,'O') in('P','A','O','')

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #pcr_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1



-- Create a normalized transfer table to improve lookup
drop table if exists #pcr_directtransfers_normalized;
Create table #pcr_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #pcr_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #pcr_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p
	



-- Step 4 - Identify Exclusions

-- Step 4.1 - Identify members who were dead on discharge
drop table if exists #pcr_deceasedmembers;
CREATE table #pcr_deceasedmembers
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #pcr_deceasedmembers
select distinct
	EMPI
	,CLAIM_ID 
from CLAIMLINE 
where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(POS,'')!='81' and 
	DIS_STAT='20';

-- Step 4.2  Identify Prgnacncy Visits
drop table if exists #pcr_pregvstlist;
CREATE table #pcr_pregvstlist
(
	EMPI varchar(100),
	FROM_DATE DATE,
	ADM_DATE DATE,
	DIS_DATE DATE,
	CLAIM_ID varchar(100)
);
Insert into #pcr_pregvstlist
Select distinct
	d.EMPI
	,d.DIAG_START_DATE		
	,coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
	,coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
Join open_empi_master m on d.EMPI=m.EMPI_ID and 
						   m.Gender='F' and
						   d.ROOT_COMPANIES_ID=m.Root_Companies_ID
						   
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'0')!='81' and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
	)
	and
	DIAG_SEQ_NO=1
	

-- Step 4.3 -- Identify Perinatal Conditions
drop table if exists #pcr_perinatalvstlist;
CREATE table #pcr_perinatalvstlist
(
	EMPI varchar(100),
	FROM_DATE DATE,
	ADM_DATE DATE,
	DIS_DATE DATE,
	CLAIM_ID varchar(100)
);
Insert into #pcr_perinatalvstlist
Select distinct
	d.EMPI
	,d.DIAG_START_DATE		
	,coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
	,coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Perinatal Conditions'
	)
	and
	DIAG_SEQ_NO=1

	
-- Step 4.4 -- Exclude stays identified in step 4.1,4.2 and 4.3
drop table if exists #pcr_directtransferswithexcl;
Create table #pcr_directtransferswithexcl
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
	StayNumber INT,
	Age INT
)
Insert into #pcr_directtransferswithexcl
Select distinct
	dt.*
	,RANK() over(partition by dt.EMPI order by dt.DIS_DATE) as StayNumber
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,dt.DIS_DATE), Date_of_Birth) >dt.DIS_DATE THEN datediff(year, Date_of_Birth, dt.DIS_DATE) - 1
		ELSE datediff(year, Date_of_Birth, dt.DIS_DATE)
	END as Age
From #pcr_directtransfers_normalized dt
Join open_empi_master oem on 
	dt.EMPI=oem.EMPI_ID and
	oem.Root_Companies_ID=@rootId
Left outer join #pcr_deceasedmembers dm on 
	dt.EMPI=dm.EMPI and 
	dm.CLAIM_ID in(dt.CLAIM1,dt.CLAIM2,dt.CLAIM3,dt.CLAIM4,dt.CLAIM5)
Left outer join #pcr_perinatalvstlist prn on 
	dt.EMPI=prn.EMPI and 
	prn.CLAIM_ID in(dt.CLAIM1,dt.CLAIM2,dt.CLAIM3,dt.CLAIM4,dt.CLAIM5)
Left outer join #pcr_pregvstlist prg on 
	dt.EMPI=prg.EMPI and 
	prg.CLAIM_ID in(dt.CLAIM1,dt.CLAIM2,dt.CLAIM3,dt.CLAIM4,dt.CLAIM5)
left outer join #pcr_hospicemembers hspexcl on dt.EMPI=hspexcl.EMPI
Where
	dm.EMPI is null and
	prn.EMPI is null and
	prg.EMPI is null and
	hspexcl.EMPI is null and
	dt.DIS_DATE between @ce_startdt and @ce_dischenddt and
	dt.ADM_DATE!=dt.DIS_DATE
	


-- Check for Continuous Enrollment from -365 to Dischargedate
drop table if exists #pcr_contenroll1;
create table #pcr_contenroll1
(
	EMPI varchar(50),
	staynumber INT,
	anchor INT
);
--Check for continuous enrollment
--CTE1 to check covergae during last 365 days
With coverage_CTE1(EMPI,DIS_DATE,staynumber,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select 
		t1.EMPI
		,DIS_DATE
		,staynumber
		,isnull(lag(TERM_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE desc),dateadd(day,-365,DIS_DATE)) as lastcoveragedate
		,Case 
			when EFF_DATE<dateadd(day,-365,DIS_DATE) then dateadd(day,-365,DIS_DATE)
			else EFF_DATE 
		end as Startdate
		,case 
			when TERM_DATE>DIS_DATE then DIS_DATE 
			else TERM_DATE 
		end as Finishdate
		,isnull(lead(EFF_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE),DIS_DATE) as nextcoveragedate
	from
	(
			Select distinct 
				s.EMPI
				,s.DIS_DATE
				,s.StayNumber
				,EFF_DATE
				,TERM_DATE
				,Rank() over(partition by s.EMPI,s.staynumber order by EFF_Date,Term_Date) as rn
			From #pcr_directtransferswithexcl s
			join ENROLLMENT en on 
				s.EMPI=en.EMPI and 
				en.ROOT_COMPANIES_ID=@rootId
			where  
				en.EFF_DATE<=s.DIS_DATE and 
				en.TERM_DATE>=dateadd(day,-365,s.DIS_DATE)
	)t1					        
)
Insert into #pcr_contenroll1
select 
	t3.EMPI
	,t3.staynumber 
	,anchor
from
(
	select 
		t2.EMPI
		,t2.staynumber
		,sum(anchor) as anchor 
	from
	(
	
		select 
			*
			,case 
				when rn=1 and startdate>dateadd(day,-365,DIS_DATE) then 1 
				else 0 
			end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
			end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when DIS_DATE between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
		from
		(
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,staynumber order by Startdate,FinishDate),DIS_DATE) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by EMPI,staynumber order by Startdate,Finishdate) as rn 
			from coverage_CTE1
		)t1           
	)t2  
	group by 
		EMPI
		,DIS_DATE
		,staynumber 
	having
	(sum(gaps)+sum(startgap))<=1 and 
	sum(coveragedays)>=((datediff(day,dateadd(day,-365,DIS_DATE),DIS_DATE)+1)-45) 

)t3


-- Calculate Continuous enrollment from Discharge Date to +30 days
drop table if exists #pcr_contenroll2;
create table #pcr_contenroll2
(
	EMPI varchar(50),
	staynumber INT,
	Anchor INT
);
--Check for continuous enrollment
--CTE1 to check covergae during last 365 days
With coverage_CTE1(EMPI,DIS_DATE,staynumber,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select 
		t1.EMPI
		,DIS_DATE
		,staynumber
		,isnull(lag(TERM_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE desc),DIS_DATE) as lastcoveragedate
		,Case 
			when EFF_DATE<DIS_DATE then DIS_DATE
			else EFF_DATE 
		end as Startdate
		,case 
			when TERM_DATE>DATEADD(day,30,DIS_DATE) then DATEADD(day,30,DIS_DATE) 
			else TERM_DATE 
		end as Finishdate
		,isnull(lead(EFF_DATE,1) over(partition by t1.EMPI,staynumber order by EFF_DATE,TERM_DATE),DATEADD(day,30,DIS_DATE)) as nextcoveragedate
	from
	(
			Select distinct 
				s.EMPI
				,s.DIS_DATE
				,s.StayNumber
				,EFF_DATE
				,TERM_DATE
				,Rank() over(partition by s.EMPI,s.staynumber order by EFF_Date,Term_Date) as rn
			From #pcr_directtransferswithexcl s
			join ENROLLMENT en on 
				s.EMPI=en.EMPI and 
				en.ROOT_COMPANIES_ID=@rootId
			where  
				en.EFF_DATE<=dateadd(day,30,s.DIS_DATE) and 
				en.TERM_DATE>=DIS_DATE
			
	)t1					        
)
Insert into #pcr_contenroll2
select 
	t3.EMPI
	,t3.staynumber 
	,t3.anchor
from
(
	select 
		t2.EMPI
		,t2.staynumber
		,sum(anchor) as anchor 
	from
	(
	
		select 
			*
			,case 
				when rn=1 and startdate>DIS_DATE then 1 
				else 0 
			end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
			end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when DIS_DATE between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
		from
		(
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,staynumber order by Startdate,FinishDate),DIS_DATE) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by EMPI,staynumber order by Startdate,Finishdate) as rn 
			from coverage_CTE1
		)t1           
	)t2  
	group by 
		EMPI
		,DIS_DATE
		,staynumber 
	having
	(sum(gaps)+sum(startgap))<=1 and 
	sum(coveragedays)>=(datediff(day,DIS_DATE,dateadd(day,30,DIS_DATE))+1) 

)t3



-- Identify Payer
-- Apply Payer Mapping Logic for PPlan
Drop Table if exists #pcr_payermap
Create Table #pcr_payermap
(
	EMPI varchar(100),
	DIS_DATE Date,
	StayNumber INT,
	Payer varchar(50)
)
Insert into #pcr_payermap
Select distinct
	EMPI
	,DIS_DATE
	,StayNumber
	,pm.PayerMapping
From
(
	Select distinct
		EMPI
		,DIS_DATE
		,StayNumber
		,case 
			when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB','MDE')) and staynumber=nxtstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB','MDE')) and staynumber=prvstaynumber then Payer
			When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=prvstaynumber then prvpayer
			when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=nxtstaynumber then Payer
			When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO')) and staynumber=prvstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')) and staynumber=prvstaynumber then prvpayer
			else Payer 
		end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer,1) over(partition by EMPI order by EMPI,staynumber),Payer) as nxtpayer
			,isnull(lag(Payer,1) over(partition by EMPI order by EMPI,staynumber),Payer) as prvpayer
			,isnull(lead(Staynumber,1) over(partition by EMPI order by EMPI,staynumber),Staynumber) as nxtStaynumber
			,isnull(lag(Staynumber,1) over(partition by EMPI order by EMPI,staynumber),Staynumber) as prvStaynumber
			
		From 
		(
			Select
				EMPI
				,DIS_DATE
				,StayNumber
				,PAYER_TYPE as Payer
			From
			(
				Select
					*
					,RANK() over(partition by EMPI,DIS_DATE order by TERM_DATE Desc,EFF_DATE Desc) as rnk
				From
				(
					select 
						d.EMPI
						,d.DIS_DATE
						,d.StayNumber
						,en.PAYER_TYPE
						,EFF_DATE
						,TERM_DATE
					From #pcr_directtransferswithexcl d
					join ENROLLMENT en on
						d.EMPI=en.EMPI and
						en.ROOT_COMPANIES_ID=@rootId
					Where
						EFF_DATE<=DATEADD(day,30,d.DIS_DATE) and
						TERM_DATE>=DATEADD(day,-365,d.DIS_DATE) 
					--	and d.EMPI=99551
						
				)t1	
			)t2
			Where
				rnk=1
		)t3
		
	)t4
)t5
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t5.newpayer=pm.payer and 
	pm.Measure_id='PCR'
Order by 1



-- Map Payer and PPayer
Drop table if exists #pcr_payerandpppayer
Create table #pcr_payerandpppayer
(
	EMPI varchar(100),
	DIS_DATE DATE,
	StayNumber INT,
	Payer varchar(10),
	PPayer varchar(10)
)
Insert into #pcr_payerandpppayer
Select 
	* 
from
(
	Select 
		p.*
		,pp.PPlan
	from #pcr_payermap p
	Join #pcr_pplanpayermap pp on 
		p.EMPI=pp.EMPI and
		p.Payer=pp.PPlan

	Union all


	Select
		t1.*
		,pp.PPlan
	From
	(
		Select 
			p.*
		from #pcr_payermap p
		left Outer Join #pcr_pplanpayermap pp on 
			p.EMPI=pp.EMPI and
			p.Payer=pp.PPlan
		Where
			pp.EMPI is null
	)t1
	Join #pcr_pplanpayermap pp on
		t1.EMPI=pp.EMPI
)t2




-- Create the Base data set
Drop Table if exists #pcrbaselist
Create Table #pcrbaselist
(
	EMPI varchar(100),
	Meas varchar(20),
	Payer varchar(20),
	PPayer varchar(20),
	Age INT,
	Gender varchar(10),
	StayNumber INT,
	DIS_DATE Date
)
Insert into #pcrbaselist
Select distinct 
	EMPI
	,Meas
	,Payer
	,PPayer
	,age
	,Gender
	,StayNumber
	,DIS_DATE
From
(

	select 
		s.EMPI
		,s.DIS_DATE
		,s.StayNumber
		,concat('PCR',char(64+s.StayNumber)) as Meas
		,Payer
		,PPayer
		,CASE 
			WHEN DATEADD(year,DATEDIFF(year, Date_of_Birth  ,s.DIS_DATE) , Date_of_Birth)> s.DIS_DATE THEN DATEDIFF(year, Date_of_Birth  ,s.DIS_DATE) -1
			ELSE DATEDIFF(year, Date_of_Birth  ,s.DIS_DATE)
		END as age 
		,Gender
	From #pcr_directtransferswithexcl s
	Join open_empi_master oem on
		s.EMPI=oem.EMPI_ID and
		oem.Root_Companies_ID=@rootId
	Join #pcr_payerandpppayer p on 
		s.EMPI=p.EMPI and
		s.DIS_DATE=p.DIS_DATE
	Join #pcr_contenroll1 ce1 on
		s.EMPI=ce1.EMPI and
		s.StayNumber=ce1.staynumber
	Join #pcr_contenroll2 ce2 on
		s.EMPI=ce2.EMPI and
		s.StayNumber=ce2.staynumber
	Where
		ce1.anchor+ce2.anchor>=1
	--	and s.EMPI=146932
)t1
where 
	(
		(
			payer in('MMP','SN3','SN2','SN1','MCR','MCS','MP','MR') and 
			age>=18
		) 
		or 
		(
			payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO','EPO') and 
			age between 18 and 64
		)
		or 
		(
			payer in('MD','MLI','MRB','MCD') and 
			age between 18 and 64
		)
	)
	

-- generating the ouput in required format
drop table if exists #pcrdataset;
CREATE TABLE #pcrdataset (
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  [Epop] bit DEFAULT '0',
  [Ppop] bit DEFAULT '1',
  [Num] bit DEFAULT '0',
  [Outlier] bit DEFAULT '0',
  [SNF] bit DEFAULT '0',
  [ObsWt] float DEFAULT '0',
  [SurgWt] float DEFAULT '0',
  [DCC1] INT DEFAULT '0',
  [DCCWt1] float DEFAULT '0',
  [DCC2] INT DEFAULT '0',
  [DCCWt2] float DEFAULT '0',
  [DCC3] INT DEFAULT '0',
  [DCCWt3] float DEFAULT '0',
  [ComorbidWt] float DEFAULT '0',
  [AgeGenWt] float DEFAULT '0',
  [ObsWtSNF] float DEFAULT '0',
  [SurgWtSNF] float DEFAULT '0',
  [DCC1SNF] INT DEFAULT '0',
  [DCCWt1SNF] float DEFAULT '0',
  [DCC2SNF] INT DEFAULT '0',
  [DCCWt2SNF] float DEFAULT '0',
  [DCC3SNF] INT DEFAULT '0',
  [DCCWt3SNF] float DEFAULT '0',
  [ComorbidWtSNF] float DEFAULT '0',
  [AgeGenWtSNF] float DEFAULT '0',
  [Age] Int DEFAULT NULL,
  [Gender] varchar(45) NULL,
  LIS INT DEFAULT 0,
  OREC INT DEFAULT NULL
    
) 
Insert into #pcrdataset(EMPI,meas,Payer,PPayer,Age,Gender)
select 
	EMPI
	,meas
	,Payer
	,PPayer
	,Age
	,Gender
from #pcrbaselist 








-- Step 6 - Calculating Outliers
drop table if exists #pcr_outliers;
create table #pcr_outliers
(
	EMPI varchar(100) NOT NULL,
	Meas varchar(20) DEFAULT NULL,
	[Payer] varchar(100) DEFAULT NULL,
	[PPayer] varchar(100) DEFAULT NULL,
	outlier INT,
	epop INT
  
);
Insert into #pcr_outliers
select 
	t1.EMPI
	,t1.Meas
	,t1.Payer
	,t1.Ppayer
	,case 
		when payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MCD','MR','MC') and staycount>=4 then 1 
		when payer in('CEP','HMO','POS','PPO','MOS','MEP','MPO','MMO','EPO') and staycount>=3 then 1
		else 0 
	end as outlier
	,case 
		when payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MCD','MR','MC') and staycount>=4 then 0 
		when payer in('CEP','HMO','POS','PPO','MOS','MEP','MPO','MMO','EPO') and staycount>=3 then 0
		else 1 
	end as epop
from #pcrbaselist t1
	join
	(
		select 
			EMPI
			,count(distinct Meas) as Staycount 
		from #pcrbaselist 
		group by 
			EMPI
	)t2 on t1.EMPI=t2.EMPI


update ds set ds.Outlier=o.outlier,ds.epop=o.epop from #pcrdataset ds join #pcr_outliers o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer


 

-- Calculating Observation Stay weight

-- Identify Observation Stays
Drop table if exists #pcr_observationstays;
Create Table #pcr_observationstays
(
	EMPI varchar(100),
	DIS_DATE Date,
	CLAIM_ID varchar(100)
)
Insert into #pcr_observationstays
Select distinct
	EMPI
	,coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Observation Stay')
	)


-- Get the Observation Weight	
drop table if exists #pcr_obswtlist
create table #pcr_obswtlist
(
	EMPI varchar(100) NOT NULL,
	[Meas] varchar(20) DEFAULT NULL,
	[Payer] varchar(100) DEFAULT NULL,
	[PPayer] varchar(100) DEFAULT NULL,
	obswt float
);
Insert into #pcr_obswtlist
select distinct 
	EMPI
	,Meas
	,Payer
	,ppayer
	,sum(obswt) as obswt
from
(
	select 
		b.EMPI
		,b.Meas
		,b.Payer
		,b.PPayer
		,isNull(weight,0) as obswt 
	from #pcr_directtransferswithexcl dt
	Join #pcr_observationstays obs on dt.EMPI=obs.EMPI and obs.CLAIM_ID=dt.DischargeClaimId
	Join #pcrbaselist b on
		b.EMPI=dt.EMPI and
		b.DIS_DATE=dt.DIS_DATE	Join HDS.HEDIS_RISK_ADJUSTMENT hra on hra.VariableName='OBS' and
									  hra.Measure_ID='PCR' and 
									  b.Payer in('MCD') and 
									  hra.ProductLine='Medicaid'

	Union all

	select 
		b.EMPI
		,b.Meas
		,b.Payer
		,b.PPayer
		,isNull(weight,0) as obswt 
	from #pcr_directtransferswithexcl dt
	Join #pcr_observationstays obs on dt.EMPI=obs.EMPI and obs.CLAIM_ID=dt.DischargeClaimId
	Join #pcrbaselist b on
		b.EMPI=dt.EMPI and
		b.DIS_DATE=dt.DIS_DATE
		Join HDS.HEDIS_RISK_ADJUSTMENT hra on hra.VariableName='OBS' and
									  hra.Measure_ID='PCR' and 
									  b.Payer in('HMO','CEP','POS','PPO','MEP','MMO','MOS','MPO','EPO') and 
									  hra.ProductLine='Commercial'
									  

)t1 
Group by
	EMPI
	,Meas
	,Payer
	,ppayer


Update #pcrdataset set ObsWt=0
update ds set ds.ObsWt=o.obswt from #pcrdataset ds join #pcr_obswtlist o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0

	




-- Surgery Weight
-- Identify Surgical Procedures
Drop table if exists #pcr_surgeries
Create table #pcr_surgeries
(
	EMPI varchar(100),
	PROC_START_DATE Date
)
Insert into #pcr_surgeries
Select
	EMPI
	,PROC_START_DATE
From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Surgery Procedure')
Where
	PROC_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)


drop table if exists #pcr_surgwtlist;
create table #pcr_surgwtlist
(
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  surgwt float
  
);
Insert into #pcr_surgwtlist
select distinct 
	t1.EMPI
	,t1.Meas
	,t1.Payer
	,t1.Ppayer
	,r.Weight 
from
(
	select distinct
		b.EMPI
		,b.Meas
		,b.Payer
		,b.PPayer
		,b.age
		,'Surg' as staytype
		,case 
			when  b.payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO','EPO') then 'Commercial'
			When b.payer='MCD' then 'Medicaid'
			When b.payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MC','MR') then 'Medicare'
		end as productline
		,case
			when b.age between 18 and 64 then 'Standard - 18-64'
			when b.age>=65 then 'Standard - 65+'
		end as reportingindicator
	from #pcr_directtransferswithexcl d
	Join #pcrbaselist b on 
		b.EMPI=d.EMPI and
		b.DIS_DATE=d.DIS_DATE
	join #pcr_surgeries s on 
		b.EMPI=s.EMPI and 
		s.PROC_START_DATE between d.ADM_DATE and d.DIS_DATE
		

)t1
join HDS.HEDIS_RISK_ADJUSTMENT r on 
	t1.staytype=r.VariableName and 
	r.Measure_ID='PCR' and 
	t1.productline=r.ProductLine and 
	t1.reportingindicator=r.ReportingIndicator


Update #pcrdataset set SurgWt=0;
update ds set ds.SurgWt=o.surgwt from #pcrdataset ds join #pcr_surgwtlist o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0;



-- Discharge Condition
drop table if exists #pcr_dischcc;
create table #pcr_dischcc
(
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  DCC INT,
  dccct INT,
  weight float
  
)
insert into #pcr_dischcc
select 
	f2.EMPI
	,f2.Meas
	,f2.Payer
	,f2.PPayer
	,DCC
	,row_number() over(partition by EMPI,meas,payer order by meas,payer,DCC) as dccct
	,weight 
from
(
	select distinct 
		f1.*
		,isnull(r.Weight,0) as weight
		,Cast(replace(Comorbid_CC,'CC-','') as INT) as DCC 
	from
	(

		select 
			t2.EMPI
			,t2.Meas
			,t2.Payer
			,t2.PPayer
			,t2.Age
			,t2.Gender
			,cc.Comorbid_CC
			,case 
				when t2.payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO','EPO') then 'Commercial'
				When t2.payer='MCD' then 'Medicaid'
				When t2.payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MC','MR') then 'Medicare'
			end as productline
			,case
				when t2.age between 18 and 64 then 'Standard - 18-64'
				when t2.age>=65 then 'Standard - 65+'
			end as reportingindicator
		from
		(

			select distinct
				b.*
				,DIAG_CODE
			From #pcr_directtransferswithexcl dt
			Join #pcrbaselist b on 
				dt.EMPI=b.EMPI and
				dt.DIS_DATE=b.DIS_DATE
			Join diagnosis d on b.EMPI=d.EMPI and
								d.DIAG_SEQ_NO=1 and
								d.ROOT_COMPANIES_ID=@rootId and
								d.CLAIM_ID=DischargeClaimId and
								d.DIAG_SEQ_NO=1
		)t2
		join HDS.HEDIS_TABLE_CC cc on t2.DIAG_CODE=cc.DiagnosisCode and 
									  t2.payer ='MCD' and 
									  cc.CC_type='Medicaid'
									  

	Union all
	

	select 
		t2.EMPI
		,t2.Meas
		,t2.Payer
		,t2.PPayer
		,t2.Age
		,t2.Gender
		,cc.Comorbid_CC
		,case 
			when t2.payer in('CEP','HMO','POS','PPO','MEP','MMO','MOS','MPO','EPO') then 'Commercial'
			When t2.payer='MCD' then 'Medicaid'
			When t2.payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MC','MR') then 'Medicare'
		end as productline
		,case
			when t2.age between 18 and 64 then 'Standard - 18-64'
			when t2.age>=65 then 'Standard - 65+'
		end as reportingindicator
	from
	(
		select distinct
				b.*
				,DIAG_CODE
			From #pcr_directtransferswithexcl dt
			Join #pcrbaselist b on 
				dt.EMPI=b.EMPI and
				dt.DIS_DATE=b.DIS_DATE
			Join diagnosis d on b.EMPI=d.EMPI and
								d.DIAG_SEQ_NO=1 and
								d.ROOT_COMPANIES_ID=@rootId and
								d.CLAIM_ID=DischargeClaimId and
								d.DIAG_SEQ_NO=1
								
	)t2
	join HDS.HEDIS_TABLE_CC cc on 
		t2.DIAG_CODE=cc.DiagnosisCode and 
		t2.payer !='MCD' and 
		cc.CC_type='Shared'
		
)f1
left outer join HDS.HEDIS_RISK_ADJUSTMENT r on f1.Comorbid_CC=r.VariableName and 
											   f1.productline=r.ProductLine and 
											   f1.reportingindicator=r.ReportingIndicator and 
											   r.Measure_ID='PCR' and 
											   r.VariableType='DCC' and 
											   r.Model='Logistic'

)f2



update #pcrdataset set DCCWt1=0,DCCWt2=0,DCCWt3=0,DCC1=0,DCC2=0,DCC3=0
update ds set ds.DCCWt1=o.weight,ds.DCC1=o.DCC from #pcrdataset ds join #pcr_dischcc o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=1
update ds set ds.DCCWt2=o.weight,ds.DCC2=o.DCC from #pcrdataset ds join #pcr_dischcc o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=2
update ds set ds.DCCWt3=o.weight,ds.DCC3=o.DCC from #pcrdataset ds join #pcr_dischcc o on ds.EMPI=o.EMPI and ds.Meas=o.meas and ds.Payer=o.payer and ds.ppayer=o.ppayer and ds.outlier=0 and o.dccct=3


-- Comorbidities

--Identify visits for risk scoring
Drop table if exists #pcr_diagvisits;
Create Table #pcr_diagvisits
(
	EMPI varchar(100),
	ServiceDate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #pcr_diagvisits
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'Outpatient')

	Union All 

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'Telephone Visits')

	Union all 

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'Observation')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'ED')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'Nonacute Inpatient')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_dischenddt,'Acute Inpatient')

	Union All

	Select
		EMPI
		,FROM_DATE as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!=81 and
		FROM_DATE between @ce_startdt1 and @ce_dischenddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
		)

	Union All

	Select
		p.EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,p.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From Procedures p
	Join Claimline c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(C.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		ISNULL(POS,'0')!=81 and
		coalesce(c.DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_dischenddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
		)

	Union all


	Select
		EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!=81 and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_dischenddt and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		)



)t1



drop table if exists #pcr_diagnosislist;
Create table #pcr_diagnosislist
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT,
	DIAG_DATA_SRC varchar(50)

)
Insert into #pcr_diagnosislist
Select distinct
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO
	,DIAG_DATA_SRC
From Diagnosis
Where
	ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	DIAG_START_DATE between @ce_startdt2 and @ce_enddt


drop table if exists #pcr_diagnosis;
Create table #pcr_diagnosis
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT

)
Insert into #pcr_diagnosis
Select
	d.EMPI
	,d.diagnosis
	,v.Servicedate
	,d.CLAIM_ID
	,d.DIAG_SEQ_NO
From #pcr_diagnosislist d
Join #pcr_diagvisits v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DIAG_DATA_SRC=v.DATA_SRC




drop table if exists #pcr_diagnosis_old;
Create table #pcr_diagnosis_old
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT

)
Insert into #pcr_diagnosis_old
Select distinct
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO

From
(
	Select 
		d.EMPI
		,d.DIAG_CODE
		,d.DIAG_START_DATE
		,d.CLAIM_ID
		,d.DIAG_SEQ_NO

	From DIAGNOSIS d 
	join CLAIMLINE c on	d.CLAIM_ID=c.CLAIM_ID and
						d.DIAG_DATA_SRC=c.CL_DATA_SRC and
						d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
						d.EMPI=c.EMPI
	left outer Join PROCEDURES p on c.CLAIM_ID=p.CLAIM_ID and
						 c.CL_DATA_SRC=p.PROC_DATA_SRC and
						 c.ROOT_COMPANIES_ID=p.ROOT_COMPANIES_ID and
						 ISNULL(c.SV_LINE,0)=ISNULL(p.SV_LINE,0) and
						 c.EMPI=p.EMPI and
						 ISNULL(p.PROC_STATUS,'EVN')!='INT'
	Where
		d.ROOT_COMPANIES_ID=@rootId and
		d.DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
		ISNULL(c.POS,'')!='81' and
		d.DIAG_START_DATE between @ce_startdt1 and @ce_dischenddt and
		(
			p.PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Observation','ED','Nonacute Inpatient','Acute Inpatient')
			)
			or
			c.REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
			)
		)
	--	and d.EMPI=104414
		

	Union All

	Select 
		d.EMPI
		,d.DIAG_CODE
		,coalesce(c.DIS_DATE,c.TO_DATE) as DIAG_START_DATE
		,d.CLAIM_ID
		,d.DIAG_SEQ_NO
	From DIAGNOSIS d 
	join CLAIMLINE c on	d.CLAIM_ID=c.CLAIM_ID and
						d.DIAG_DATA_SRC=c.CL_DATA_SRC and
						d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
						d.EMPI=c.EMPI
	left outer Join PROCEDURES p on c.CLAIM_ID=p.CLAIM_ID and
						 c.CL_DATA_SRC=p.PROC_DATA_SRC and
						 c.ROOT_COMPANIES_ID=p.ROOT_COMPANIES_ID and
						 ISNULL(c.SV_LINE,0)=ISNULL(p.SV_LINE,0) and
						 c.EMPI=p.EMPI and
						 ISNULL(p.PROC_STATUS,'EVN')!='INT'
	Where
		d.ROOT_COMPANIES_ID=@rootId and
		d.DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'')!='81' and
		coalesce(c.DIS_DATE,c.TO_DATE) between @ce_startdt1 and @ce_dischenddt and
		(
			p.PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
			)
			or
			c.REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
			)
		)		
	--	and  d.EMPI=104414
	
)t1







-- HCC RANK
drop table if exists #pcr_hccrank;
Create table #pcr_hccrank
(
	EMPI varchar(100),
	Meas varchar(20),
	payer varchar(20),
	ppayer varchar(20),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)
Insert into #pcr_hccrank
select 
	t3.EMPI
	,t3.Meas
	,t3.payer
	,t3.ppayer
	,case 
		when age between 18 and 44 and gender='F' then 'F_18-44'
		when age between 45 and 54 and gender='F' then 'F_45-54'
		when age between 55 and 64 and gender='F' then 'F_55-64'
		when age between 65 and 74 and gender='F' then 'F_65-74'
		when age between 75 and 84 and gender='F' then 'F_75-84'
		when age >=85 and gender='F' then 'F_85'
		when age between 18 and 44 and gender='M' then 'M_18-44'
		when age between 45 and 54 and gender='M' then 'M_45-54'
		when age between 55 and 64 and gender='M' then 'M_55-64'
		when age between 65 and 74 and gender='M' then 'M_65-74'
		when age between 75 and 84 and gender='M' then 'M_75-84'
		when age >=85 and gender='M' then 'M_85'
	end as genagegroup
	,case 
		when age between 18 and 64 then 'Standard - 18-64'
		when age>=65 then 'Standard - 65+'
		end as reportingindicator
	,case
		When payer in('CEP','HMO','POS','PPO','MEP','MPO','MOS','MMO','EPO') then 'Commercial'
		When payer='MCD' then 'Medicaid'
		else 'Medicare'
	end as productline,
	hcc
from
(
	select distinct 
		EMPI
		,Meas
		,payer
		,ppayer
		,HCC
		,Age
		,gender
		,calcrank 
	from 
	(
		select 
			EMPI
			,Meas
			,payer
			,ppayer
			,cc.Comorbid_CC
			,hcc.RankingGroup
			,isnull(hcc.Rank,1) as Rank
			,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC
			,Rank() over(partition by EMPI,meas,RankingGroup order by RankingGroup,Rank) as calcrank
			,age
			,gender  
		from
		(
			select 
				b.*
				,d.diagnosis
				,case 
					when payer in('MCD','MLI','MRB','MD') then 'Medicaid' 
					else 'Shared'
				end as producttype 
			from #pcr_diagnosis d
			Join #pcrbaselist b on 
				d.EMPI=b.EMPI and
				d.DiagnosisDate between dateadd(day,-365,b.DIS_DATE) and b.DIS_DATE
			join #pcr_directtransferswithexcl dte on
				b.EMPI=dte.EMPI and
				b.DIS_DATE=dte.DIS_DATE
			Where
				--b.empi=132109 and
				(Case when b.DIS_DATE=DiagnosisDate and DIAG_SEQ_NO=1 and dte.dischargeClaimid=d.CLAIM_ID then 1 else 0 end)!=1
			
		)t1
		left outer join HDS.HEDIS_TABLE_CC cc on t1.diagnosis=cc.diagnosiscode and 
												 t1.producttype=cc.CC_type
		left outer join HDS.HEDIS_HCC_RANK hcc on cc.Comorbid_CC=hcc.CC and 
												  t1.producttype=hcc.hcc_type
		where 
			Comorbid_CC is not null
	)t2 
	where 
		calcrank=1
 --memid=100035 and meas='PCRA'
)t3






-- HCC RANK COMB
drop table if exists #pcr_hccrankcmb;
Create table #pcr_hccrankcmb
(
	EMPI varchar(50),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #pcr_hccrankcmb
select distinct 
	h.*
	,cmb.hcccomb 
from #pcr_hccrank h 
left outer join HDS.HEDIS_HCC_COMB cmb on 
	cmb.ComorbidHCC1 in
	(
		select 
			HCC 
		from #pcr_hccrank t1 
		where 
			t1.EMPI=h.EMPI and 
			t1.productline=h.productline and 
			t1.reportingindicator=h.reportingindicator and 
			t1.Meas=h.Meas and 
			t1.payer=h.payer
	) 
	and 
	cmb.ComorbidHCC2 in
	(
		select 
			HCC 
		from #pcr_hccrank t1 
		where 
			t1.EMPI=h.EMPI and 
			t1.productline=h.productline and 
			t1.reportingindicator=h.reportingindicator and 
			t1.Meas=h.Meas and 
			t1.payer=h.payer
	)


drop table if exists #pcr_agegenwt;
Create table #pcr_agegenwt
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	AgeGenWt float
	
)
Insert into #pcr_agegenwt
select distinct 
	t1.EMPI
	,t1.Meas
	,t1.payer
	,t1.Ppayer
	,r1.Weight as AgeGenWt 
from
(
	select 
		EMPI
		,Meas
		,payer
		,ppayer
		,case 
			when age between 18 and 44 and gender='F' then 'F_18-44'
			when age between 45 and 54 and gender='F' then 'F_45-54'
			when age between 55 and 64 and gender='F' then 'F_55-64'
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 18 and 44 and gender='M' then 'M_18-44'
			when age between 45 and 54 and gender='M' then 'M_45-54'
			when age between 55 and 64 and gender='M' then 'M_55-64'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
		end as genagegroup
		,case 
			when payer in('MCR','MP','MCS','SN1','SN2','SN3','MMP','MC','MR') and age between 18 and 64 then 'Standard - 18-64'
			when payer in('MCR','MP','MCS','SN1','SN2','SN3','MMP','MC','MR') and age>=65 then 'Standard - 65+'
			else 'Standard - 18-64'
		end as reportingindicator
		,case
			When payer in('CEP','HMO','POS','PPO','MEP','MPO','MOS','MMO','EPO') then 'Commercial'
			When payer='MCD' then 'Medicaid'
			else 'Medicare'
		end as productline
	from #pcrdataset 
	where 
		outlier=0 
		--and memid=100021          
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	t1.productline=r1.ProductLine and 
	r1.variabletype='DEMO' and 
	r1.Model='Logistic' and 
	r1.Measure_id='PCR' and 
	r1.reportingindicator=t1.reportingindicator


 
drop table if exists #pcr_comorbidwt;
Create table #pcr_comorbidwt
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	ComorbidWt float
	
)
Insert into #pcr_comorbidwt
select 
	EMPI
	,Meas
	,payer
	,Ppayer
	,sum(weight) as ComorbidWt 
from
(
	select distinct 
		h.EMPI
		,h.Meas
		,h.payer
		,h.Ppayer
		,h.genagegroup
		,h.reportingindicator
		,h.hcc as hcc
		,r1.Weight 
	from #pcr_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcc and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='Logistic' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='PCR'

	Union all

	select distinct 
		h.EMPI
		,h.meas
		,h.payer
		,h.ppayer
		,h.genagegroup
		,h.reportingindicator
		,h.hcccmb as hcc
		,r1.Weight 
	from #pcr_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcccmb and
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='Logistic' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='PCR'

)t1 
group by 
	EMPI,Meas,payer,PPayer


Update #pcrdataset set AgeGenWt=0,ComorbidWt=0
update ds set ds.AgeGenWt=p.AgeGenWt from #pcrdataset ds join #pcr_agegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.Meas=ds.Meas and p.ppayer=ds.PPayer
update ds set ds.ComorbidWt=p.ComorbidWt from #pcrdataset ds join #pcr_comorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer and p.Meas=substring(ds.Meas,1,4) and p.ppayer=ds.PPayer and ds.Outlier=0


-- Numerator Logic


-- Step 1.1 -- Identify Inpatient and Observation Stay 
drop table if exists #pcr_num_staylist;
CREATE table #pcr_num_staylist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
				
)
Insert into #pcr_num_staylist
Select distinct
	EMPI
	,FROM_DATE
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
	,SV_STAT
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay','Observation Stay')
	)
	





-- Step 1.3 -- Identify only Acute and observation Discharges and remove non acute stays
drop table if exists #pcr_num_acuteandobservationstays;
CREATE table #pcr_num_acuteandobservationstays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
		
)
-- removing non acute inpatient stays from total Inpatient stays
Insert into #pcr_num_acuteandobservationstays
select 
	t1.EMPI
	,t1.FROM_DATE
	,t1.ADM_DATE
	,t1.DIS_DATE
	,t1.CLAIM_ID
	,t1.SV_STAT
from #pcr_num_staylist t1
left outer join #pcr_nonacutestays t2 on 
	t1.EMPI=t2.EMPI and 
	t1.CLAIM_ID=t2.CLAIM_ID
Where 
	t2.EMPI is null


	
-- Step 2 and 3 - Identify Direct Transfers , Select discharges between 1st Jan and 1st Dec , Exlcude same day stays
drop table if exists #pcr_num_directtransfers;
Create table #pcr_num_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #pcr_num_acuteandobservationstays
  Where
	ISNULL(SV_STAT,'O') in('P','A','O','')

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #pcr_num_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1



-- Create a normalized transfer table to improve lookup
drop table if exists #pcr_num_directtransfers_normalized;
Create table #pcr_num_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #pcr_num_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #pcr_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p
	



--Idntifying Planned Admissions
-- Indetifying Chemotherapy
drop table if exists #pcr_chemovstlist;
CREATE table #pcr_chemovstlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
	
);
Insert into #pcr_chemovstlist
Select distinct
	d.EMPI
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between @ce_dischstartdt and @ce_enddt and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Chemotherapy Encounter'
	)
	and
	DIAG_SEQ_NO=1


-- Rehabilitation
drop table if exists #pcr_rehabvstlist;
CREATE table #pcr_rehabvstlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
	
);

Insert into #pcr_rehabvstlist
Select distinct
	d.EMPI
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between @ce_dischstartdt and @ce_enddt and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Rehabilitation'
	)
	and
	DIAG_SEQ_NO=1



-- Organ Transplant
drop table if exists #pcr_organtransplanttlist;
CREATE table #pcr_organtransplanttlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
	
);
Insert into #pcr_organtransplanttlist
Select distinct
	p.EMPI
	,p.CLAIM_ID
From Procedures p
Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
					ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
					p.PROC_DATA_SRC=c.CL_DATA_SRC and
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					ISNULL(p.PROC_STATUS,'EVN')!='INT' and
					p.EMPI=c.EMPI
Where
	p.ROOT_COMPANIES_ID=@rootId and
	p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between @ce_dischstartdt and @ce_enddt and
	(
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Organ Transplant Other Than Kidney')
		)
		or
		ICDPCS_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells')
		)
	)

	

--Potentially Planned Procedure
-- Identify Planned Procedures
drop table if exists #pcr_plannedprocedures
Create Table #pcr_plannedprocedures
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #pcr_plannedprocedures
select distinct
	EMPI
	,CLAIM_ID
From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Potentially Planned Procedures')
Where
	PROC_DATA_SRC not in(Select Data_source from Data_Source where supplemental=1)

-- Identify Acute COndition
drop table if exists #pcr_acuteconditions
Create Table #pcr_acuteconditions
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #pcr_acuteconditions
select distinct
	EMPI
	,CLAIM_ID
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Acute Condition')
Where
	DIAG_DATA_SRC not in(Select Data_source from Data_Source where supplemental=1) and
	DIAG_SEQ_NO=1


--Identify Planned procedures with acute conditions
drop table if exists #pcr_plannedprocedurewithoutacutecondition
Create Table #pcr_plannedprocedurewithoutacutecondition
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #pcr_plannedprocedurewithoutacutecondition
Select
	p.EMPI
	,p.CLAIM_ID
From #pcr_plannedprocedures p
left outer join #pcr_acuteconditions a on
	p.EMPI=a.EMPI and
	p.CLAIM_ID=a.CLAIM_ID
Where
	a.EMPI is null
	



-- Identifying Valid Numerator stays after excluding stays as per definition
	drop table if exists #pcr_numstaylist;
	create table #pcr_numstaylist
	(
		EMPI varchar(100),
		ADM_DATE Date,
		DIS_DATE Date
		
	)
	Insert into #pcr_numstaylist
	select distinct
		b.EMPI
		,b.ADM_DATE
		,b.DIS_DATE
	from #pcr_num_directtransfers_normalized b
	left outer join #pcr_chemovstlist c on 
		b.EMPI=c.EMPI and 
		c.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #pcr_hospicemembers h on 
		b.EMPI=h.EMPI
	left outer join #pcr_rehabvstlist r on 
		b.EMPI=r.EMPI and 
		r.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #pcr_pregvstlist pg on 
		b.EMPI=pg.EMPI and 
		pg.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #pcr_organtransplanttlist o on 
		b.EMPI=o.EMPI and 
		o.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #pcr_plannedprocedurewithoutacutecondition p on 
		b.EMPI=p.EMPI and 
		p.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #pcr_perinatalvstlist pn on 
		b.EMPI=pn.EMPI and 
		pn.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	where 
		b.ADM_DATE between @ce_dischstartdt and @ce_enddt and 
		c.EMPI is null and 
		r.EMPI is null and 
		o.EMPI is null and 
		p.EMPI is null and
		h.EMPI is null and
		pg.EMPI is null and 
		pn.EMPI is null


	drop table if exists #pcr_readmissionset;
	create table #pcr_readmissionset
	(
		EMPI varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
		StayNumber INT,
		ReadmissionDate Date
	)
	Insert into #pcr_readmissionset
	select 
		b.EMPI
		,b.Meas
		,b.Payer
		,b.PPayer
		,b.Staynumber
		,ns.ADM_DATE 
	from #pcrbaselist b
	join #pcr_numstaylist ns on 
		b.EMPI=ns.EMPI and 
		ns.ADM_DATE between b.DIS_DATE and dateadd(day,30,b.DIS_DATE)
	order by 
		b.EMPI,b.Meas

	
	drop table if exists #pcr_readmissionnumlist;
	create table #pcr_readmissionnumlist
	(
		EMPI varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
	
	)
	Insert into #pcr_readmissionnumlist
	select distinct 
		EMPI
		,Meas
		,Payer
		,PPayer 
	from #pcr_readmissionset t1 
	where 
		StayNumber=
		(
			select 
				max(StayNumber) 
			from #pcr_readmissionset t2 
			where 
				t1.EMPI=t2.EMPI and 
				t1.Meas=t2.Meas and 
				t1.Payer=t2.Payer and 
				t1.PPayer=t2.PPayer and 
				t1.ReadmissionDate=t2.ReadmissionDate
		)



	Update #pcrdataset set num=0
	update ds set ds.num=1 from #pcrdataset ds join #pcr_readmissionnumlist n on n.EMPI=ds.EMPI and n.Meas=ds.Meas and n.Payer=ds.Payer and n.PPayer=ds.PPayer and ds.Outlier=0




	

-- Identifying Skilled Nursing Discharge and Transfers
drop table if exists #pcr_SNFStays;
create table #pcr_SNFStays
(
	EMPI varchar(100),
	ADM_DATE DATE,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
)
Insert into #pcr_SNFStays
select 
	EMPI
	,coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,CLAIM_ID
	,SV_STAT
from CLAIMLINE 
where 
	ROOT_COMPANIES_ID=@rootId and 
	Coalesce(ADM_DATE,FROM_DATE) between @ce_startdt and @snf_enddt and 
	CL_DATA_SRC not in(select Data_source from Data_Source where supplemental=1) and 
	ISNULL(POS,'0')!='81' and	
	(
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Skilled Nursing Stay'
		) 
		or 
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Skilled Nursing Stay'
		)
	)
	



drop table if exists #pcr_snfdischargeortransfers;
create table #pcr_snfdischargeortransfers
(
	EMPI varchar(100),
	Meas varchar(10),
	Payer varchar(10),
	PPayer varchar(10),
		
)
Insert into #pcr_snfdischargeortransfers
Select 
	b.EMPI
	,b.Meas
	,b.Payer
	,b.PPayer 
from #pcrbaselist b
join #pcr_SNFStays s on 
	b.EMPI=s.EMPI and 
	DATEDIFF(day,b.DIS_DATE,s.ADM_DATE) between 0 and 1
where 
	payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR') and
	ISNULL(SV_STAT,'O') in('P','A','O','')


update ds 
	set ds.SNF=1 
from #pcrdataset ds 
join #pcr_snfdischargeortransfers n on 
	n.EMPI=ds.EMPI and 
	n.Meas=ds.Meas and 
	n.Payer=ds.Payer and 
	n.PPayer=ds.PPayer and 
	ds.Outlier=0
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')



-- SNF Calculations

-- Surgery Weight - SNF
drop table if exists #pcr_snf_surgwtlist;
create table #pcr_snf_surgwtlist
(
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  surgwt float
  
);
Insert into #pcr_snf_surgwtlist
select distinct 
	t1.EMPI
	,t1.Meas
	,t1.Payer
	,t1.Ppayer
	,r.Weight 
from
(
	select 
		b.EMPI
		,b.Meas
		,b.Payer
		,b.PPayer
		,b.age
	from #pcr_directtransferswithexcl d
	Join #pcrbaselist b on
		b.EMPI=d.EMPI and
		b.DIS_DATE=d.DIS_DATE
	join #pcr_SNFStays s on 
		b.EMPI=s.EMPI and 
		DATEDIFF(day,b.DIS_DATE,s.ADM_DATE) between 0 and 1 and 
		b.payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')
	Join #pcr_surgeries p on
		b.EMPI=p.EMPI and
		p.PROC_START_DATE between d.ADM_DATE and d.DIS_DATE
	Where
		ISNULL(s.SV_STAT,'O') in('P','A','O','')
	
)t1
join HDS.HEDIS_RISK_ADJUSTMENT r on 
	r.Measure_ID='PCR' and 
	r.ProductLine='Medicare' and 
	r.reportingindicator='SNF - 65+' and
	r.VariableName='Surg'


	
	
update ds 
	set ds.SurgWtSNF=o.surgwt 
from #pcrdataset ds 
join #pcr_snf_surgwtlist o on 
	ds.EMPI=o.EMPI and 
	ds.Meas=o.meas and 
	ds.Payer=o.payer and 
	ds.ppayer=o.ppayer and 
	ds.outlier=0
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')


	
-- Discharge Condition - SNF
drop table if exists #pcr_snf_dischcc;
create table #pcr_snf_dischcc
(
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [PPayer] varchar(100) DEFAULT NULL,
  DCC INT,
  dccct INT,
  weight float
  
)
insert into #pcr_snf_dischcc
select 
	f2.EMPI
	,f2.Meas
	,f2.Payer
	,f2.PPayer
	,DCC
	,row_number() over(partition by EMPI,meas,payer order by meas,payer,DCC) as dccct
	,weight 
from
(
	select distinct 
		f1.*
		,isnull(r.Weight,0) as weight
		,Cast(replace(Comorbid_CC,'CC-','') as INT) as DCC 
	from
	(

		select 
			t2.EMPI
			,t2.Meas
			,t2.Payer
			,t2.PPayer
			,t2.Age
			,t2.Gender
			,cc.Comorbid_CC
		from
		(
			select 
				b.*
				,d.DIAG_CODE
			from #pcrbaselist b
			join #pcr_directtransferswithexcl dte on
				dte.EMPI=b.EMPI and
				dte.DIS_DATE=b.DIS_DATE
			join #pcr_SNFStays s on 
					b.EMPI=s.EMPI and 
					DATEDIFF(day,b.DIS_DATE,s.ADM_DATE) between 0 and 1
			join Diagnosis d on 
				d.EMPI=b.EMPI and 
				d.CLAIM_ID=dte.DischargeClaimId and
				d.ROOT_COMPANIES_ID=@rootId and
				d.DIAG_SEQ_NO=1
			where
				ISNULL(s.SV_STAT,'O') in('P','A','O','')
		)t2
		join HDS.HEDIS_TABLE_CC cc on 
			t2.DIAG_CODE=cc.DiagnosisCode and 
			cc.CC_type='Shared'
		
	)f1
	left outer join HDS.HEDIS_RISK_ADJUSTMENT r on 
		f1.Comorbid_CC=r.VariableName and 
		r.productline='Medicare' and 
		r.reportingindicator='SNF - 65+' and 
		r.Measure_ID='PCR' and 
		r.VariableType='DCC' and 
		r.Model='Logistic'

)f2



update ds 
	set ds.DCCWt1SNF=o.weight
	,ds.DCC1SNF=o.DCC 
from #pcrdataset ds 
join #pcr_snf_dischcc o on 
	ds.EMPI=o.EMPI and 
	ds.Meas=o.meas and 
	ds.Payer=o.payer and 
	ds.ppayer=o.ppayer and 
	ds.outlier=0 and 
	o.dccct=1
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')


update ds set 
	ds.DCCWt2SNF=o.weight
	,ds.DCC2SNF=o.DCC 
from #pcrdataset ds 
join #pcr_snf_dischcc o on 
	ds.EMPI=o.EMPI and 
	ds.Meas=o.meas and 
	ds.Payer=o.payer and 
	ds.ppayer=o.ppayer and 
	ds.outlier=0 and 
	o.dccct=2
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')


update ds set
	 ds.DCCWt3SNF=o.weight
	,ds.DCC3SNF=o.DCC 
from #pcrdataset ds 
join #pcr_snf_dischcc o on 
	ds.EMPI=o.EMPI and 
	ds.Meas=o.meas and 
	ds.Payer=o.payer and 
	ds.ppayer=o.ppayer and 
	ds.outlier=0 and 
	o.dccct=3
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')






-- AgeGenWt-SNF
drop table if exists #pcr_snf_agegenwt;
Create table #pcr_snf_agegenwt
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	AgeGenWt float
	
)
Insert into #pcr_snf_agegenwt
select distinct 
	t1.EMPI
	,t1.Meas
	,t1.payer
	,t1.Ppayer
	,r1.Weight as AgeGenWt 
from
(
	select 
		b.EMPI
		,Meas
		,payer
		,ppayer
		,case 
			when age between 18 and 54 and gender='F' then 'F_18-54'
			when age between 55 and 64 and gender='F' then 'F_55-64'
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 18 and 54 and gender='M' then 'M_18-54'
			when age between 55 and 64 and gender='M' then 'M_55-64'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
		end as genagegroup
    from #pcrbaselist b
	join #pcr_SNFStays s on 
		b.EMPI=s.EMPI and 
		DATEDIFF(day,b.DIS_DATE,s.ADM_DATE) between 0 and 1 and 
		ISNULL(s.SV_STAT,'O') in('P','A','O','')

 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	r1.productline='Medicare' and 
	r1.variabletype='DEMO' and 
	r1.Model='Logistic' and 
	r1.Measure_id='PCR' and 
	r1.reportingindicator='SNF - 65+'


update ds set 
	ds.AgeGenWtSNF=o.AgeGenWt 
from #pcrdataset ds 
join #pcr_snf_agegenwt o on 
	ds.EMPI=o.EMPI and
	ds.Meas=o.meas and 
	ds.Payer=o.payer and 
	ds.ppayer=o.ppayer and 
	ds.outlier=0
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')



-- SNF- ComorbidWt
drop table if exists #pcr_snf_diagnosiswexcl;
Create table #pcr_snf_diagnosiswexcl
(
	EMPI varchar(100),
	Meas varchar(10),
	Payer varchar(8),
	PPayer varchar(8),
	CLAIM_ID varchar(100),
	diagnosis varchar(50),
	StayNumber INT,
	Age INT,
	Gender varchar(10)
	
)
Insert into #pcr_snf_diagnosiswexcl
select distinct 
	t1.EMPI
	,t1.Meas
	,t1.Payer
	,t1.PPayer
	,t1.CLAIM_ID
	,t1.diagnosis
	,t1.Staynumber
	,t1.Age
	,t1.Gender 
from
(
	select 
		b.EMPI
		,b.Meas
		,b.Payer
		,b.PPayer
		,d.CLAIM_ID
		,d.diagnosis
		,b.Staynumber
		,b.Age
		,b.Gender 
	from #pcrbaselist b
	join #pcr_directtransferswithexcl dte on
		b.EMPI=dte.EMPI and
		b.DIS_DATE=dte.DIS_DATE
	join #pcr_diagnosis d on 
		b.EMPI=d.EMPI and 
		d.DiagnosisDate between dateadd(day,-365,b.DIS_DATE) and b.DIS_DATE
	join #pcr_SNFStays s on 
		b.EMPI=s.EMPI and 
		DATEDIFF(day,b.DIS_DATE,s.ADM_DATE) between 0 and 1 
	Where
		(Case when b.DIS_DATE=DiagnosisDate and DIAG_SEQ_NO=1 and dte.dischargeClaimid=d.CLAIM_ID then 1 else 0 end)!=1
		and	ISNULL(s.SV_STAT,'O') in('P','A','O','')

)t1



-- HCC RANK
drop table if exists #pcr_snf_hccrank;
Create table #pcr_snf_hccrank
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)
Insert into #pcr_snf_hccrank
select 
	t3.EMPI
	,t3.Meas
	,t3.payer
	,t3.ppayer
	,case 
		when age between 18 and 44 and gender='F' then 'F_18-44'
		when age between 45 and 54 and gender='F' then 'F_45-54'
		when age between 55 and 64 and gender='F' then 'F_55-64'
		when age between 65 and 74 and gender='F' then 'F_65-74'
		when age between 75 and 84 and gender='F' then 'F_75-84'
		when age >=85 and gender='F' then 'F_85'
		when age between 18 and 44 and gender='M' then 'M_18-44'
		when age between 45 and 54 and gender='M' then 'M_45-54'
		when age between 55 and 64 and gender='M' then 'M_55-64'
		when age between 65 and 74 and gender='M' then 'M_65-74'
		when age between 75 and 84 and gender='M' then 'M_75-84'
		when age >=85 and gender='M' then 'M_85'
	end as genagegroup
	,'SNF - 65+' as reportingindicator
	,case
		When payer in('CEP','HMO','POS','PPO','MEP','MPO','MOS','MMO','EPO') then 'Commercial'
		When payer in('MCD','MLI','MRB','MD') then 'Medicaid'
		else 'Medicare'
	end as productline
	,hcc
from
(
	select distinct 
		EMPI
		,Meas
		,payer
		,ppayer
		,HCC
		,cast(age as INT) as age
		,gender
		,calcrank 
	from 
	(

		select 
			EMPI
			,Meas
			,payer
			,ppayer
			,cc.Comorbid_CC
			,hcc.RankingGroup
			,isnull(hcc.Rank,1) as Rank
			,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC
			,Rank() over(partition by EMPI,meas,RankingGroup order by RankingGroup,Rank) as calcrank
			,age
			,gender  
		from
		(
			select 
				*
				,case 
					when payer in('MCD','MLI','MRB','MD') then 'Medicaid' 
					else 'Shared' 
				end as producttype 
			from #pcr_snf_diagnosiswexcl d
		
		)t1
		left outer join HDS.HEDIS_TABLE_CC cc on 
			t1.diagnosis=cc.diagnosiscode and 
			t1.producttype=cc.CC_type
		left outer join HDS.HEDIS_HCC_RANK hcc on 
			cc.Comorbid_CC=hcc.CC and 
			t1.producttype=hcc.hcc_type
		where 
			Comorbid_CC is not null
	)t2 
	where 
		calcrank=1
 )t3



-- HCC RANK COMB
drop table if exists #pcr_snf_hccrankcmb;
Create table #pcr_snf_hccrankcmb
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #pcr_snf_hccrankcmb
select distinct 
	h.*
	,cmb.hcccomb 
from #pcr_snf_hccrank h 
left outer join HDS.HEDIS_HCC_COMB cmb on 
	cmb.ComorbidHCC1 in
	(
		select 
			HCC 
		from #pcr_snf_hccrank t1 
		where 
			t1.EMPI=h.EMPI and 
			t1.productline=h.productline and 
			t1.reportingindicator=h.reportingindicator and 
			t1.Meas=h.Meas and 
			t1.payer=h.payer and 
			t1.ppayer=h.ppayer
	) 
	and 
	cmb.ComorbidHCC2 in
	(
		select 
			HCC 
		from #pcr_snf_hccrank t1 
		where 
			t1.EMPI=h.EMPI and 
			t1.productline=h.productline and 
			t1.reportingindicator=h.reportingindicator and 
			t1.Meas=h.Meas and 
			t1.payer=h.payer and 
			t1.ppayer=h.ppayer
	)



drop table if exists #pcr_snf_comorbidwt;
Create table #pcr_snf_comorbidwt
(
	EMPI varchar(100),
	Meas varchar(10),
	payer varchar(10),
	ppayer varchar(10),
	ComorbidWt float
	
)
Insert into #pcr_snf_comorbidwt
select 
	EMPI
	,Meas
	,payer
	,Ppayer
	,sum(weight) as ComorbidWt 
from
(
	select distinct 
		h.EMPI
		,h.Meas
		,h.payer
		,h.Ppayer
		,h.genagegroup
		,h.reportingindicator
		,h.hcc as hcc
		,r1.Weight 
	from #pcr_snf_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcc and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='Logistic' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='PCR'

	Union all

	select distinct 
		h.EMPI
		,h.meas
		,h.payer
		,h.ppayer
		,h.genagegroup
		,h.reportingindicator
		,h.hcccmb as hcc
		,r1.Weight 
	from #pcr_snf_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcccmb and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='Logistic' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='PCR'

)t1 
group by 
	EMPI,Meas,payer,PPayer


update ds set 
	ds.ComorbidWtSNF=p.ComorbidWt 
from #pcrdataset ds 
join #pcr_snf_comorbidwt p on 
	p.EMPI=ds.EMPI and 
	p.payer=ds.payer and 
	p.Meas=ds.Meas and 
	p.ppayer=ds.PPayer and 
	ds.Outlier=0
Where
	ds.age>=65 and
	ds.Payer in('MCS','MCR','MP','SN1','SN2','SN3','MMP','MC','MR')




--SES Startification

-- Update Lis flag from LisHist	
update #pcrdataset 
	set lis=1 
from #pcrdataset ds 
Join #pcrbaselist b on
	ds.EMPI=b.EMPI and
	ds.Meas=b.Meas and
	ds.Payer=b.Payer and 
	ds.PPayer=b.PPayer
join MCFIELDS l on 
	l.EMPI=ds.EMPI and 
	l.ROOT_COMPANIES_ID=@rootId
where 
	ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and 
	(
		Low_Income_Period_Start_Date<=dateadd(day,30,DIS_DATE) and Low_Income_Period_End_Date>=DATEADD(day,-365,DIS_DATE)
		or
		Low_Income_Period_End_Date is NULL
	)
	and 
	RunDate is null and
	ds.outlier=0

		
		

	-- Item 48
	
	drop table if exists #pcr_orec;
	CREATE table #pcr_orec
	(
		EMPI varchar(100),
		Meas varchar(10),
		Payer varchar(10),
		PPayer varchar(10),
		orec varchar(2)
	);
	with CTE_orec(EMPI,Meas,Payer,PPayer,DishargeDate,Rundate,PayDate,OREC,rn) as
	(
		select 
			b.EMPI
			,b.Meas
			,b.Payer
			,b.PPayer
			,b.DIS_DATE
			,m.RunDate
			,m.PayDate
			,m.OREC as OREC 
			,row_number() over (Partition by b.EMPI,Meas,Payer,PPayer order by RunDate Desc,PayDate Desc) as rn
		from #pcrbaselist b
		join MCFIELDS m on 
			m.EMPI=b.EMPI and 
			m.ROOT_COMPANIES_ID=@rootId and 
			m.RunDate between dateadd(day,-365,b.DIS_DATE) and dateadd(day,30,b.DIS_DATE) and 
			m.OREC is not null
		where 
			b.Payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP','MC','MR')

	)
	insert into #pcr_orec
	select distinct 
		EMPI
		,Meas
		,Payer
		,PPayer
		,OREC 
	from CTE_orec t1 
	where 
		rn=1
		
	update ds set ds.OREC=n.orec from #pcrdataset ds join #pcr_orec n on n.EMPI=ds.EMPI and n.Meas=ds.Meas and n.Payer=ds.Payer and n.PPayer=ds.PPayer and ds.Outlier=0
	
	update #pcrdataset set meas=concat(meas,'NON') WHERE orec=0 AND lis=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MC','MR');

	update #pcrdataset set meas=concat(meas,'LISDE') WHERE orec=0 AND lis=1 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MC','MR');

	update #pcrdataset set meas=concat(meas,'DIS') WHERE orec in(1,3) AND lis=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MC','MR');

	update #pcrdataset set meas=concat(meas,'CMB') WHERE orec in(1,3) AND lis=1 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MC','MR');
	
	update #pcrdataset set meas=concat(meas,'OT') WHERE orec in(2,9) AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MC','MR');



-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



	Delete from HDS.HEDIS_MEASURE_OUTPUT_PCR  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	INSERT INTO HDS.HEDIS_MEASURE_OUTPUT_PCR(Memid,Meas,Payer,PPayer,Epop,Ppop,Num,Outlier,SNF,ObsWt,SurgWt,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,ComorbidWt,AgeGenWt,ObsWtSNF,SurgWtSNF,DCC1SNF,DCCWt1SNF,DCC2SNF,DCCWt2SNF,DCC3SNF,DCCWt3SNF,ComorbidWtSNF,AgeGenWtSNF,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	select EMPI,Meas,Payer,PPayer,Epop,Ppop,Num,Outlier,SNF,ObsWt,SurgWt,DCC1,DCCWt1,DCC2,DCCWt2,DCC3,DCCWt3,ComorbidWt,AgeGenWt,ObsWtSNF,SurgWtSNF,DCC1SNF,DCCWt1SNF,DCC2SNF,DCCWt2SNF,DCC3SNF,DCCWt3SNF,ComorbidWtSNF,AgeGenWtSNF,Age,Gender,@meas as Measure_ID,@meas_year as Measurement_year,@reportId,@rootId from #pcrdataset


	/*

	-- Insert data into Measure Detailed Line

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService,DischargeDate)
	Select distinct
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id
		,@measurename
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,0 as Excl
		,outlier as Rexcl
		,0 as CE
		,0 as Event
		,Epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,'Readmission' as Code
		,re.ReadmissionDate as DateofService
		,b.DIS_DATE as DischargeDate
	From #pcrdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on 
		d.EMPI=a.EMPI and 
		a.Reportid=@reportId
	Left outer Join #pcr_readmissionset re on
		d.EMPI=re.EMPI and 
		SUBSTRING(d.meas,1,4)=concat('PCR',char(64+re.staynumber))
	Left outer join #pcr_base1 b on
		d.EMPI=b.EMPI and
		SUBSTRING(d.meas,1,4)=b.meas

	

	
		-- Insert data into Provider Scorecard
		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select Distinct
		Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,NUM_COUNT
		,DEN_COUNT
		,Excl_Count
		,Rexcl_Count
		,Gaps
		,Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	,Case
		when Result > Target then Floor((Den_excl*Target)/100)-NUM_COUNT
		when Result <= Target then 0
	end as To_Target
From
(
	Select 
		Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,NUM_COUNT as NUM_COUNT
		,DEN_COUNT as DEN_COUNT
		,EXCL_COUNT as Excl_Count
		,Rexcl_count as Rexcl_Count
		,NUM_COUNT as Gaps
		,Case
			when Den_excl>0 Then Round((cast(NUM_COUNT as decimal)/Den_excl)*100,2)
			else 0
		end as Result
		,@target as Target
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,DEN_EXCL
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,count(case when NUM=1 and Rexcl=0 Then 1 end) as NUM_COUNT
			,count(case when Den=1 Then 1 end) as DEN_COUNT
			,count(case when Den=1 and Rexcl=0 Then 1 end) Den_excl
			,count(case when excl=1 Then 1 end) as EXCL_COUNT
			,count(case when rexcl=1 Then 1 end) as Rexcl_count
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From RPT.MEASURE_DETAILED_LINE
		where Enrollment_Status='Active' and
			  MEASURE_ID=@measure_id and
			  REPORT_ID=@reportId
		Group by Measure_id,Measure_Name,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id
	)t1
	
)t2


	
GO


*/
GO
