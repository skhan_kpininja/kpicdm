SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [dbo].[GetReportDetail] @rundate Date ,@rootId varchar(10),@startDate Date Output,@enddate Date output,@reportId INT output,@quarter varchar(10) output
As


Set @reportId=0
Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,@rundate)), 0) 
Set @enddate=eomonth(Dateadd(month,-2,@rundate))
Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))


Select @reportId=ReportId from RPT.Report_Details where ReportEndDate=@enddate and ROOT_COMPANIES_ID=@rootId;


if(@reportId=0)
Begin

	Insert into RPT.Report_Details(ReportStartDate,ReportEndDate,ReportQuarter,Root_Companies_id) values(@startDate,@enddate,@quarter,@rootId);

End


Select @reportId=ReportId from RPT.Report_Details where ReportEndDate=@enddate and ROOT_COMPANIES_ID=@rootId



GO
