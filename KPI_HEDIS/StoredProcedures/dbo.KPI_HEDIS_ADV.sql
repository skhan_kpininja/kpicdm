SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON








CREATE PROCEDURE [dbo].[KPI_HEDIS_ADV]
AS
-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='ADV-A'

DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
Declare @quarter varchar(20);
Declare @reportId INT;


DECLARE @meas VARCHAR(10);

SET @meas='ADV';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');


-- Eligible Patient List
drop table if exists #adv_memlist; 
CREATE TABLE #adv_memlist
(
    EMPI varchar(100)
    
)
insert into #adv_memlist
SELECT DISTINCT 
	en.EMPI 
FROM member gm
join ENROLLMENT en on en.EMPI=gm.EMPI and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt and 
	YEAR(@ce_enddt)-YEAR(MEM_DOB) BETWEEN 2 AND 20 
ORDER BY 1;

select * from #adv_memlist
-- Create Temp Patient Enrollment
drop table if exists #adv_tmpsubscriber;
CREATE TABLE #adv_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #adv_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.MEM_DOB
	,gm.MEM_GENDER
	,YEAR(@ce_enddt)-YEAR(MEM_DOB) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM MEMBER gm
join ENROLLMENT en on en.EMPI=gm.EMPI and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(MEM_DOB) BETWEEN 2 AND 20   
	ORDER BY en.EMPI,en.EFF_DATE,en.TERM_DATE;



drop table if exists #advdataset;
CREATE TABLE #advdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'

) ;
insert into #advdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'ADV'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(nxtpayer)
				When (Payer in('MCR','MCS','MP','MC') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(prvpayer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('MCR','MCS','MP','MC','MR'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('MCR','MCS','MP','MC','MR'))  then TRIM(Payer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #adv_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='ADV'
Order by 1


-- Continuous Enrollment
Drop table if exists #adv_contenroll
Create table #adv_contenroll
(
	EMPI varchar(100)
)
Insert into #adv_contenroll
select 
	EMPI
from GetContinuousEnrolledEMPIwithDental(@rootId,@ce_startdt,@ce_enddt,45,0)

update ds set ds.CE=1 from #advdataset ds join #adv_contenroll ce on ds.EMPI=ce.EMPI;

---Numerator
Drop table if exists #adv_numeratorset
Create Table #adv_numeratorset
(
	EMPI varchar(100)
)
Insert into #adv_numeratorset
select * from
	(
	select distinct c.EMPI 
	from dbo.CLAIMLINE c 
	join dbo.PROVIDER_FLAGS pf
	on pf.ROOT_COMPANIES_ID = c.ROOT_COMPANIES_ID 
	and ATT_NPI = ProvID 
	where pf.ROOT_COMPANIES_ID=@rootId
	and Dentist = 'Y' and ISNULL(POS,'0')!='81'
	and FROM_DATE between @ce_startdt and @ce_enddt)Numerator

update ds Set ds.Num=1 from #advdataset ds join #adv_numeratorset n on ds.EMPI=n.EMPI

-- Identify hopsice members for exclusion
Drop table if exists #adv_hospicemembers
Create Table #adv_hospicemembers
(
	EMPI varchar(100)
)
Insert into #adv_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)

update ds Set ds.Rexcl=1 from #advdataset ds join #adv_hospicemembers n on ds.EMPI=n.EMPI

-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@ce_startdt output,@enddate=@ce_enddt output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,0 as EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0 and Payer in('MCD','MEP','MMO','MOS','MPO') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #advdataset


GO
