SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROCEDURE [dbo].[KPI_HEDIS_AMM]
AS
-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='AMM-S'



DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_intakestartdt DATE;
DECLARE @ce_intakeenddt DATE;
DECLARE @meas VARCHAR(10);

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='AMM';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_intakestartdt=concat(@meas_year-1,'-05-01');
SET @ce_intakeenddt=concat(@meas_year,'-04-30');

/*
Set @reporttype='Physician'
Set @measurename='Breast Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=85
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='22'

*/

--Step 1.1 Members with anti-depressants Determine the IPSD. Identify the date of the earliest dispensing event for an antidepressant medication (Antidepressant Medications List) during the Intake Period. 
Exec SP_KPI_DROPTABLE '#amm_memwithdepressionmedlist';
select distinct
	EMPI
	,Fill_Date as servicedate 
into #amm_memwithdepressionmedlist
from MEDICATION 
where 
	ROOT_COMPANIES_ID=@rootId and 
	MED_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
	Fill_Date between @ce_startdt1 and @ce_enddt and  
	MEDICATION_CODE in 
	(
		select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications'
	) 


-- Step 1.2 Determine the IPSD. Identify the date of the earliest dispensing event for an antidepressant medication (Antidepressant Medications List) during the Intake Period. 
Exec SP_KPI_DROPTABLE '#amm_memipsd';
select 
	EMPI
	,min(servicedate) as ipsd 
Into #amm_memipsd
from #amm_memwithdepressionmedlist 
where 
	servicedate between @ce_intakestartdt and @ce_intakeenddt 
group by 
	EMPI


-- Step 2.3 -- test Negative Medication History
Exec SP_KPI_DROPTABLE '#amm_nmh';
Select distinct
	m.EMPI
into #amm_nmh
From #amm_memwithdepressionmedlist m
Join #amm_memipsd i on
	m.EMPI=i.EMPI and
	m.servicedate between dateadd(day,-105,i.ipsd) and dateadd(day,-1,i.ipsd)

--Step 2 - Exclude members who did not have an encounter with a diagnosis of major depression during the 121-day period from 60 days prior to the IPSD, through the IPSD and the 60 days after the IPSD. Members who meet any of the following criteria remain in the eligible population:



-- Create Temp Patient Enrollment
drop table if exists #amm_tmpsubscriber;
CREATE TABLE #amm_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
	ipsd Date,
	rx int
)
insert into #amm_tmpsubscriber
Select distinct 
	*
From
(
	SELECT 
		en.EMPI
		,gm.Date_of_Birth
		,gm.Gender
		,CASE 
			WHEN dateadd(year, datediff (year, Date_of_Birth,@ce_intakeenddt), Date_of_Birth) > @ce_intakeenddt THEN datediff(year, Date_of_Birth, @ce_intakeenddt) - 1
			ELSE datediff(year, Date_of_Birth, @ce_intakeenddt)
		END as Age
		,en.PAYER_TYPE
		,en.EFF_DATE
		,en.TERM_DATE  
		,i.ipsd
		,case
			when en.DRUG_BENEFIT=1 or RX_UNITS=1 Then 1
			else 0
		end as rx
	FROM open_empi_master gm
	join ENROLLMENT en on 
		en.EMPI=gm.EMPI_ID and 
		en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
	Join #amm_memipsd i on
		gm.EMPI_ID=i.EMPI
	Left outer join #amm_nmh n on
		en.EMPI=n.EMPI
	WHERE -- en.EMPI=100007 and
		en.ROOT_COMPANIES_ID=@rootId and
		en.EFF_DATE<=@ce_enddt AND 
		TERM_DATE>=@ce_startdt1 AND 
		YEAR(@ce_intakeenddt)-YEAR(Date_of_Birth)>=18 and
		ipsd between @ce_intakestartdt and @ce_intakeenddt and
		n.EMPI is null

)t1
Where
	age>=18
ORDER BY 
	EMPI,EFF_DATE,TERM_DATE;



drop table if exists #ammdataset;
CREATE TABLE #ammdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '1',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'

) ;
insert into #ammdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'AMM2'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #amm_tmpsubscriber 
				where  
					StartDate<=DateAdd(day,231,ipsd)
					--and EMPI=109717
			)t1
			where 
				rn=1
		)t3
		
	)t4
)t5
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t5.newpayer=pm.payer and 
	pm.Measure_id='AMM'
Order by 1


insert into #ammdataset(EMPI,meas,payer,Gender,age)
Select EMPI,'AMM3',Payer,Gender,Age from #ammdataset



-- Step 2.1 -- Identify members with Major Depression
Exec SP_KPI_DROPTABLE '#amm_majordepressiondiagnosis'
Select
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
Into #amm_majordepressiondiagnosis
From GetDIagnosis(@rootId,@ce_startdt1,@ce_enddt,'Major Depression')


-- Step 2.2 Identify inpatient visits
Exec SP_KPI_DROPTABLE '#amm_inpatientstays'
Select
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
Into #amm_inpatientstays
From Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)



-- Step 2.3 Identify members with inpatient stay and Major Depression
Exec SP_KPI_DROPTABLE '#amm_reqdexclInpatientStays'
Select
	d.*
Into #amm_reqdexclInpatientStays
From #amm_inpatientstays s
join #amm_majordepressiondiagnosis d on
	s.EMPI=d.EMPI and
	s.CLAIM_ID=d.CLAIM_ID and
	s.CL_DATA_SRc=d.DIAG_DATA_SRC
Join #amm_memipsd i on
	s.EMPI=i.EMPI and
	(
		s.ADM_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		or
		s.DIS_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
	)



-- Step 2.2 ?	An acute inpatient encounter with any diagnosis of major depression: Acute Inpatient Value Set with Major Depression Value Set.
-- Step 2.3 ?	?	A nonacute inpatient encounter with any diagnosis of major depression: Nonacute Inpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_acuteandnonacuteipvisits'
Select distinct
	*
Into #amm_acuteandnonacuteipvisits
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient,Nonacute Inpatient')
	

	Union all

	Select
		EMPI,
		DIAG_START_DATE,
		CLAIM_ID,
		DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient,Nonacute Inpatient')

	Union all

	Select
		EMPI,
		VisitDate,
		NUll as CLAIM_ID,
		Visit_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient,Nonacute Inpatient')
	
)tbl


-- Step 2.2.1 Identify members with Acute and Non acute Inpatient Visit and Major depression with +-60 days of ipsd
Exec SP_KPI_DROPTABLE '#amm_reqdexclAcuteandNonAcuteInpatientVisits'
Select distinct
	*
Into #amm_reqdexclAcuteandNonAcuteInpatientVisits
From 
(


	Select
		d.*
	From #amm_acuteandnonacuteipvisits s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
		
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_acuteandnonacuteipvisits s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	



-- Step 2.4.1?	An outpatient visit with any diagnosis of major depression: Visit Setting Unspecified Value Set with Outpatient POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_outpatientPOS'
Select
	EMPI
	,FROM_DATE
	,CLAIM_ID
	,CL_DATA_SRC
Into #amm_outpatientPOS
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0') in
	(
		Select Code from HDS.VALUESET_TO_CODE where Value_Set_Name='Outpatient POS'
	)


-- STep 2.4.2 ?	An outpatient visit with any diagnosis of major depression: Visit Setting Unspecified Value Set with Outpatient POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_UnspecifiedVisit'
Select
	EMPI
	,PROC_START_DATE
	,CLAIM_ID
	,PROC_DATA_SRC
into #amm_UnspecifiedVisit
From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Visit Setting Unspecified')


-- Step 2.4.3 Identify members with Acute and Non acute Inpatient Visit and Major depression with +-60 days of ipsd
Exec SP_KPI_DROPTABLE '#amm_reqdexclUnspecifedVisits'
Select
	d.*
Into #amm_reqdexclUnspecifedVisits
From #amm_UnspecifiedVisit s
Join #amm_outpatientPOS p on
	p.EMPI=s.EMPI and
	p.CLAIM_ID=s.CLAIM_ID and
	p.CL_DATA_SRC=s.PROC_DATA_SRC
join #amm_majordepressiondiagnosis d on
	s.EMPI=d.EMPI and
	s.CLAIM_ID=d.CLAIM_ID and
	s.PROC_DATA_SRC=d.DIAG_DATA_SRC
Join #amm_memipsd i on
	s.EMPI=i.EMPI and
	(
		s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
	
	)


-- Step 2.4.1 ?	An outpatient visit with any diagnosis of major depression: BH Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_BHOutpatient'
Select distinct
	*
into #amm_BHOutpatient
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'BH Outpatient')
	

	Union all

	Select
		EMPI
		,FROM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt1,@ce_enddt,'BH Outpatient')
	
	Union all
		
	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'BH Outpatient')

	Union all
		
	Select
		EMPI
		,VisitDATE
		,null as CLAIM_ID
		,VISIT_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'BH Outpatient')
	
)tbl


-- Step 2.4.2 ?	An outpatient visit with any diagnosis of major depression: BH Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclBHOutpatient'
Select distinct
	*
Into #amm_reqdexclBHOutpatient
From 
(


	Select
		d.*
	From #amm_BHOutpatient s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
		
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_BHOutpatient s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	




-- Step 2.6.1 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Visit Setting Unspecified Value Set with Partial Hospitalization POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_partialHospitalizationPOS'
Select
	EMPI
	,FROM_DATE
	,CLAIM_ID
	,CL_DATA_SRC
Into #amm_partialHospitalizationPOS
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0') in
	(
		Select Code from HDS.VALUESET_TO_CODE where Value_Set_Name='Partial Hospitalization POS'
	)

-- Step 2.6.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Visit Setting Unspecified Value Set with Partial Hospitalization POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclUnspecifedVisitswithPHPOS'
Select
	d.*
Into #amm_reqdexclUnspecifedVisitswithPHPOS
From #amm_UnspecifiedVisit s
Join #amm_partialHospitalizationPOS p on
	p.EMPI=s.EMPI and
	p.CLAIM_ID=s.CLAIM_ID and
	p.CL_DATA_SRC=s.PROC_DATA_SRC
join #amm_majordepressiondiagnosis d on
	s.EMPI=d.EMPI and
	s.CLAIM_ID=d.CLAIM_ID and
	s.PROC_DATA_SRC=d.DIAG_DATA_SRC
Join #amm_memipsd i on
	s.EMPI=i.EMPI and
	(
		s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
	
	)



-- Step 2.7.1 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Partial Hospitalization or Intensive Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_IntensiveOutpatient'
Select distinct
	*
into #amm_IntensiveOutpatient
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Partial Hospitalization or Intensive Outpatient')
	

	Union all

	Select
		EMPI
		,FROM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt1,@ce_enddt,'Partial Hospitalization or Intensive Outpatient')
	
	Union all
		
	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Partial Hospitalization or Intensive Outpatient')

	Union all
		
	Select
		EMPI
		,VisitDATE
		,null as CLAIM_ID
		,VISIT_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Partial Hospitalization or Intensive Outpatient')
	
)tbl

 
 -- Step 2.6.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Partial Hospitalization or Intensive Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclIntensiveOutpatient'
Select distinct
	*
Into #amm_reqdexclIntensiveOutpatient
From 
(


	Select
		d.*
	From #amm_IntensiveOutpatient s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_IntensiveOutpatient s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	


-- Step 2.8.1 ?	A community mental health center visit with any diagnosis of major depression: Visit Setting Unspecified Value Set with Community Mental Health Center POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_communityPOS'
Select
	EMPI
	,FROM_DATE
	,CLAIM_ID
	,CL_DATA_SRC
Into #amm_communityPOS
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0') in
	(
		Select Code from HDS.VALUESET_TO_CODE where Value_Set_Name='Community Mental Health Center POS'
	)



-- Step 2.8.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Visit Setting Unspecified Value Set with Partial Hospitalization POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclUnspecifedVisitswithCommunityPOS'
Select
	d.*
Into #amm_reqdexclUnspecifedVisitswithCommunityPOS
From #amm_UnspecifiedVisit s
Join #amm_communityPOS p on
	p.EMPI=s.EMPI and
	p.CLAIM_ID=s.CLAIM_ID and
	p.CL_DATA_SRC=s.PROC_DATA_SRC
join #amm_majordepressiondiagnosis d on
	s.EMPI=d.EMPI and
	s.CLAIM_ID=d.CLAIM_ID and
	s.PROC_DATA_SRC=d.DIAG_DATA_SRC
Join #amm_memipsd i on
	s.EMPI=i.EMPI and
	(
		s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
	
	)


-- Step 2.9.1 ?	Electroconvulsive therapy with any diagnosis of major depression: Electroconvulsive Therapy Value Set with Major Depression Value
Exec SP_KPI_DROPTABLE '#amm_ElectroconvulsiveTherapy'
Select distinct
	*
Into #amm_ElectroconvulsiveTherapy
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Electroconvulsive Therapy')

	Union all

	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Electroconvulsive Therapy')

	Union all

	Select
		EMPI,
		DIAG_START_DATE,
		CLAIM_ID,
		DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Electroconvulsive Therapy')

	Union all

	Select
		EMPI,
		VisitDATE,
		null as CLAIM_ID,
		Visit_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Electroconvulsive Therapy')

)tbl



-- Step 2.9.2 ?	Electroconvulsive therapy with any diagnosis of major depression: Electroconvulsive Therapy Value Set with Major Depression Value
Exec SP_KPI_DROPTABLE '#amm_reqdexclElectroconvulsive'
Select distinct
	*
Into #amm_reqdexclElectroconvulsive
From 
(


	Select
		d.*
	From #amm_ElectroconvulsiveTherapy s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_ElectroconvulsiveTherapy s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	


-- Step 2.10.2 ?	Transcranial magnetic stimulation visit with any diagnosis of major depression: Transcranial Magnetic Stimulation Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_transcranial'
Select distinct
	*
Into #amm_transcranial
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Transcranial Magnetic Stimulation')

	Union all

	Select
		EMPI,
		VisitDATE,
		null as CLAIM_ID,
		Visit_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Transcranial Magnetic Stimulation')

)tbl



-- Step 2.9.2 ?	Electroconvulsive therapy with any diagnosis of major depression: Electroconvulsive Therapy Value Set with Major Depression Value
Exec SP_KPI_DROPTABLE '#amm_reqdexcltranscranial'
Select distinct
	*
Into #amm_reqdexcltranscranial
From 
(


	Select
		d.*
	From #amm_transcranial s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_transcranial s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	


-- Step 2.11.1 ?	A telehealth visit with any diagnosis of major depression: Visit Setting Unspecified Value Set with Telehealth POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_telehealthPOS'
Select
	EMPI
	,FROM_DATE
	,CLAIM_ID
	,CL_DATA_SRC
Into #amm_telehealthPOS
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0') in
	(
		Select Code from HDS.VALUESET_TO_CODE where Value_Set_Name='Telehealth POS'
	)



-- Step 2.11.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Visit Setting Unspecified Value Set with Partial Hospitalization POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclUnspecifedVisitswithtelehealthPOS'
Select
	d.*
Into #amm_reqdexclUnspecifedVisitswithtelehealthPOS
From #amm_UnspecifiedVisit s
Join #amm_telehealthPOS p on
	p.EMPI=s.EMPI and
	p.CLAIM_ID=s.CLAIM_ID and
	p.CL_DATA_SRC=s.PROC_DATA_SRC
join #amm_majordepressiondiagnosis d on
	s.EMPI=d.EMPI and
	s.CLAIM_ID=d.CLAIM_ID and
	s.PROC_DATA_SRC=d.DIAG_DATA_SRC
Join #amm_memipsd i on
	s.EMPI=i.EMPI and
	(
		s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
	)


-- Step 2.12.1 ?	An observation visit (Observation Value Set) with any diagnosis of major depression (Major Depression Value Set).
Exec SP_KPI_DROPTABLE '#amm_observation'
Select distinct
	*
Into #amm_observation
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

	Union all

	Select
		EMPI,
		VisitDATE,
		null as CLAIM_ID,
		Visit_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Observation')

)tbl



-- Step 2.12.2 ?	Electroconvulsive therapy with any diagnosis of major depression: Electroconvulsive Therapy Value Set with Major Depression Value
Exec SP_KPI_DROPTABLE '#amm_reqdexclobservation'
Select distinct
	*
Into #amm_reqdexclobservation
From 
(


	Select
		d.*
	From #amm_observation s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_observation s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	



-- Step 1.13.1 ?	An ED visit (ED Value Set) with any diagnosis of major depression (Major Depression Value Set).
Exec SP_KPI_DROPTABLE '#amm_ED'
Select distinct
	*
into #amm_ED
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')
	

	Union all

	Select
		EMPI
		,FROM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt1,@ce_enddt,'ED')
	
	Union all
		
	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'ED')

	Union all
		
	Select
		EMPI
		,VisitDATE
		,null as CLAIM_ID
		,VISIT_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'ED')
	
)tbl

 
 -- Step 2.13.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Partial Hospitalization or Intensive Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclED'
Select distinct
	*
Into #amm_reqdexclED
From 
(


	Select
		d.*
	From #amm_ED s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_ED s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	



-- Step 2.14.1 ?	An ED visit with any diagnosis of major depression: Visit Setting Unspecified Value Set with ED POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_EDPOS'
Select
	EMPI
	,FROM_DATE
	,CLAIM_ID
	,CL_DATA_SRC
Into #amm_EDPOS
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0') in
	(
		Select Code from HDS.VALUESET_TO_CODE where Value_Set_Name='ED POS'
	)



-- Step 2.11.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Visit Setting Unspecified Value Set with Partial Hospitalization POS Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclUnspecifedVisitswithEDPOS'
Select
	d.*
Into #amm_reqdexclUnspecifedVisitswithEDPOS
From #amm_UnspecifiedVisit s
Join #amm_EDPOS p on
	p.EMPI=s.EMPI and
	p.CLAIM_ID=s.CLAIM_ID and
	p.CL_DATA_SRC=s.PROC_DATA_SRC
join #amm_majordepressiondiagnosis d on
	s.EMPI=d.EMPI and
	s.CLAIM_ID=d.CLAIM_ID and
	s.PROC_DATA_SRC=d.DIAG_DATA_SRC
Join #amm_memipsd i on
	s.EMPI=i.EMPI and
	(
		s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
	)


-- Step 2.15.1 ?	A telephone visit (Telephone Visits Value Set) with any diagnosis of major depression (Major Depression Value Set).
Exec SP_KPI_DROPTABLE '#amm_telehealthvisits'
Select distinct
	*
into #amm_telehealthvisits
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')
	
	Union all
		
	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

	Union all
		
	Select
		EMPI
		,VisitDATE
		,null as CLAIM_ID
		,VISIT_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')
	
)tbl

 
 -- Step 2.15.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Partial Hospitalization or Intensive Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexcltelehealth'
Select distinct
	*
Into #amm_reqdexcltelehealth
From 
(


	Select
		d.*
	From #amm_telehealthvisits s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_telehealthvisits s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	


-- Step 2.16.1  ?	An e-visit or virtual check-in (Online Assessments Value Set) with any diagnosis of major depression (Major Depression Value Set).
Exec SP_KPI_DROPTABLE '#amm_onlinevisits'
Select distinct
	*
into #amm_onlinevisits
From
(
	Select
		EMPI,
		PROC_START_DATE,
		CLAIM_ID,
		PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')
	
	
	Union all
		
	Select
		EMPI
		,VisitDATE
		,null as CLAIM_ID
		,VISIT_DATA_SRC
	From GetVisits(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')
	
)tbl

 
 -- Step 2.16.2 ?	An intensive outpatient encounter or partial hospitalization with any diagnosis of major depression: Partial Hospitalization or Intensive Outpatient Value Set with Major Depression Value Set.
Exec SP_KPI_DROPTABLE '#amm_reqdexclonlinevisits'
Select distinct
	*
Into #amm_reqdexclonlinevisits
From 
(


	Select
		d.*
	From #amm_onlinevisits s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.CLAIM_ID=d.CLAIM_ID and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is not null

	Union all

	Select
		d.*
	From #amm_onlinevisits s
	join #amm_majordepressiondiagnosis d on
		s.EMPI=d.EMPI and
		s.PROC_START_DATE=d.DIAG_START_DATE and
		s.PROC_DATA_SRC=d.DIAG_DATA_SRC 	
	Join #amm_memipsd i on
		s.EMPI=i.EMPI and
		(
			s.PROC_START_DATE between dateadd(day,-60,i.ipsd) and dateadd(day,60,i.ipsd)
		)
	Where
		s.CLAIM_ID is null

)tbl	




-- Consolidated all required exclusions
Exec SP_KPI_DROPTABLE '#amm_allrequiredexclusions';
Select distinct
	EMPI
into #amm_allrequiredexclusions
from
(

	Select EMPI from #amm_reqdexclInpatientStays

	Union all

	Select EMPI from #amm_reqdexclAcuteandNonAcuteInpatientVisits

	Union all

	Select EMPI from #amm_reqdexclUnspecifedVisits

	Union all

	Select EMPI from #amm_reqdexclBHOutpatient

	Union all

	Select EMPI from #amm_reqdexclUnspecifedVisitswithPHPOS

	Union all

	Select EMPI from #amm_reqdexclIntensiveOutpatient

	Union all

	Select EMPI from #amm_reqdexclUnspecifedVisitswithCommunityPOS

	Union all

	Select EMPI from #amm_reqdexclElectroconvulsive

	Union all

	Select EMPI from #amm_reqdexcltranscranial

	Union all

	Select EMPI from #amm_reqdexclUnspecifedVisitswithtelehealthPOS

	Union all

	Select EMPI from #amm_reqdexclobservation

	Union all

	Select EMPI from #amm_reqdexclED

	Union all

	Select EMPI from #amm_reqdexclUnspecifedVisitswithEDPOS

	Union all

	Select EMPI from #amm_reqdexcltelehealth

	Union all

	Select EMPI from #amm_reqdexclonlinevisits

)tbl


UPdate #ammdataset set Rexcld=0 from #ammdataset ds Join #amm_allrequiredexclusions re on ds.EMPI=re.EMPI





-- Hospice Eclusion
Exec SP_KPI_DROPTABLE '#amm_hospiceexclusion';
Select distinct
	EMPI
Into #amm_hospiceexclusion
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)


UPdate #ammdataset set Rexcl=1 from #ammdataset ds Join #amm_hospiceexclusion re on ds.EMPI=re.EMPI



-- Apply COntinuous Enrollment Logic
Exec SP_KPI_DROPTABLE '#amm_contenroll';
With coverage_CTE1 (EMPI,ipsd,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
	(

		select distinct
			EMPI
			,ipsd
			,isnull(lag(EndDate,1) over(partition by EMPI order by StartDate,EndDate desc),Dateadd(day,-105,ipsd)) as lastcoveragedate
			,Case 
				when StartDate<Dateadd(day,-105,ipsd) then Dateadd(day,-105,ipsd)
				else StartDate 
			end as Startdate
			,case 
				when EndDate>Dateadd(day,231,ipsd) then Dateadd(day,231,ipsd)
				else EndDate 
			end as Finishdate
			,isnull(lead(Startdate,1) over(partition by EMPI order by StartDate,EndDate),Dateadd(day,231,ipsd)) as nextcoveragedate  
		from #amm_tmpsubscriber
		Where
			Startdate<=dateadd(day,231,ipsd) and
			EndDate>=dateadd(day,-105,ipsd) and
			rx=1
		
						        
	)
	
	select 
		t3.EMPI 
	Into #amm_contenroll 
	from
	(
		select 
			t2.EMPI 
		from
		(
		
			select 
				*
				,case 
					when rn=1 and startdate>dateadd(day,-105,ipsd) then 1 
					else 0 
				end as startgap
				,case 
					when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 end as gaps
				,datediff(day,Startdate,Finishdate)+1 as coveragedays
				,case 
					when ipsd between Startdate and newfinishdate then 1 
					else 0 
				end as anchor 
			from
			(
				Select 
					*
					,case 
						when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI order by Startdate,FinishDate),Dateadd(day,231,ipsd)) 
						else finishdate 
					end as newfinishdate
					,ROW_NUMBER() over(partition by EMPI order by Startdate,Finishdate) as rn 
				from coverage_CTE1
				--where EMPI=127300
			)t1           
		)t2  
		group by 
			EMPI,ipsd
		having
			(sum(gaps)+sum(startgap))<=1 and 
			sum(coveragedays)>=((datediff(day,Dateadd(day,-105,ipsd),Dateadd(day,231,ipsd))+1)-45) and 
			(sum(anchor))>=1
	)t3



Update ds Set CE=1 from #ammdataset ds join #amm_contenroll ce on ds.EMPI=ce.EMPI


-- Apply Numerator logic for AMM2 Effective Acute Phase Treatment

Exec SP_KPI_DROPTABLE '#amm_num_amm2';
Select
	EMPI
Into #amm_num_amm2
From
(
	select distinct
		m.EMPI
		,Fill_Date as servicedate 
		,Case
			when dateadd(day,Cast(Cast(SUPPLY_DAYS as numeric) as INT)-1,Fill_Date)>dateadd(day,115,ipsd) Then Datediff(day,FILL_DATE,dateadd(day,115,ipsd))+1
			else Cast(Cast(SUPPLY_DAYS as numeric) as INT)
		End as SUPPLY_DAYS
		
	
	--into #amm_num_antidepressants
	from MEDICATION m
	join #amm_memipsd i on
		m.EMPI=i.EMPI
	where 
		ROOT_COMPANIES_ID=@rootId and 
		Fill_Date between i.ipsd and dateadd(day,115,ipsd) and  
		MEDICATION_CODE in 
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications'
		) 
)tbl1
Group by
	EMPI
Having 
	SUM(SUPPLY_DAYS)>=84


Update ds Set Num=1 from #ammdataset ds join #amm_num_amm2 n2 on ds.EMPI=n2.EMPI and ds.meas='AMM2'

-- Apply Numerator logic - AMM3 Effective Continuation Phase Treatment 

Exec SP_KPI_DROPTABLE '#amm_num_amm3';
Select
	EMPI
Into #amm_num_amm3
From
(
	select distinct
		m.EMPI
		,Fill_Date as servicedate 
		,Case
			when dateadd(day,Cast(Cast(SUPPLY_DAYS as numeric) as INT)-1,Fill_Date)>dateadd(day,231,ipsd) Then Datediff(day,FILL_DATE,dateadd(day,231,ipsd))+1
			else Cast(Cast(SUPPLY_DAYS as numeric) as INT)
		End as SUPPLY_DAYS
	from MEDICATION m
	join #amm_memipsd i on
		m.EMPI=i.EMPI
	where 
		ROOT_COMPANIES_ID=@rootId and 
		Fill_Date between i.ipsd and dateadd(day,231,ipsd) and  
		MEDICATION_CODE in 
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications'
		) 
)tbl1
Group by
	EMPI
Having 
	SUM(SUPPLY_DAYS)>=180


Update ds Set Num=1 from #ammdataset ds join #amm_num_amm3 n3 on ds.EMPI=n3.EMPI and ds.meas='AMM3'




-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0  THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #ammdataset

/*
	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,0 as Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0  THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #bcsdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #bcs_numdetails nd on d.EMPI=nd.EMPI
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type

*/
GO
