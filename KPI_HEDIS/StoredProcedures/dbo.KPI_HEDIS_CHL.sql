SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[KPI_HEDIS_CHL]
AS

-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='CHL-S'
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_enddt2 DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_startdt4 DATE;
DECLARE @ce_startdt9 DATE;
DECLARE @ce_startdt2 DATE;
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;
DECLARE @t_planid VARCHAR(10);

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='CHL';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_enddt2=concat(@meas_year-2,'-12-31');
SET @ce_startdt4=concat(@meas_year-4,'-01-01');
SET @ce_startdt9=concat(@meas_year-9,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');

Set @reporttype='Physician'
Set @measurename='Chlamydia Screening in Women'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=38
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult & Pediatric Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='21'

/*** To Test
select memid, Meas, Payer,CE,Event, Epop, Excl,NuM,RExcl,RExclD,Age,Gender from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='CHL-S' 
except
select memid, Meas, Payer,CE, Event, Epop,Excl,NuM,RExcl,RExclD,Age,Gender  from HDS.HEDIS_MEASURE_OUTPUT where ROOT_COMPANIES_ID='CHL-S' 
except 
select memid, Meas, Payer,CE,Event, Epop,Excl,NuM,RExcl,RExclD,Age,Gender from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='CHL-S' 

select MemID, Meas, Payer,CE,Event,Excl,NuM,RExcl,RExclD,Age,Gender from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='CHL-S'
except
select EMPI, Meas, Payer,CE,Event,Excl,NuM,RExcl,RExclD,Age,Gender  from #CHLdataset

select top 10* from KPI_HEDIS.dbo.HEDIS_SCORE

SELECT * FROM HDS.HEDIS_MEASURE_OUTPUT where ROOT_COMPANIES_ID='CHL-S' AND memid=100022
SELECT * FROM KPI_HEDIS.dbo.HEDIS_SCORE WHERE ROOT_COMPANIES_ID='CHL-S' AND MEMID=100022
select * from #CHLdataset where empi=100022

*/


-- Eligible Patient List
drop table if exists #CHL_memlist; 
CREATE TABLE #CHL_memlist 
(
    EMPI varchar(100)
    
)
insert into #CHL_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt  AND 
	gm.GENDER='F' AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 16 AND 24 
ORDER BY 1;



-- Create Temp Patient Enrollment
drop table if exists #CHL_tmpsubscriber;
CREATE TABLE #CHL_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #CHL_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	gm.Gender='F' AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 16 AND 24
	ORDER BY en.EMPI,en.EFF_DATE,en.TERM_DATE;



drop table if exists #CHLdataset;
CREATE TABLE #CHLdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'

) ;

insert into #CHLdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'CHL'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(nxtpayer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(prvpayer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR'))  then TRIM(Payer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #CHL_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=139080
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='CHL'
Order by 1




-- Continuous Enrollment
Drop table if exists #CHL_contenroll
Create table #CHL_contenroll
(
	EMPI varchar(100)
)
Insert into #CHL_contenroll
SELECT 
		EMPI
	FROM
	(
	
		-- CE Check for 1st year
		SELECT 
			EMPI
			,SUM(coverage) AS coverage
			, 1 AS ceflag 
		FROM
		(
			SELECT 
				EMPI
				,PAYER_TYPE
				,start_date
				,end_date
				,DATEDIFF(day,start_date,end_date)+1 AS coverage 
			FROM
			(

				SELECT 
					s.EMPI
					,s.PAYER_TYPE
					,s.TERM_DATE
					,CASE
						WHEN EFF_DATE<@ce_startdt THEN @ce_startdt
						ELSE EFF_DATE
					END AS start_date
					,CASE
						WHEN TERM_DATE>@ce_enddt THEN @ce_enddt
						ELSE TERM_DATE
					END AS end_date
				FROM ENROLLMENT s
				JOIN 
				(
					SELECT 
						EMPI
						,SUM(diff) as diff 
					FROM
					(
						SELECT 
							EMPI
							,nextstartdate
							,TERM_DATE
							,CASE
								WHEN DATEDIFF(day,TERM_DATE,nextstartdate)<=1 THEN 0
								ELSE 1
							END AS diff
						 FROM
						 (
							 SELECT 
								EMPI
								,EFF_DATE
								,TERM_DATE
								,CASE
									WHEN nextstartdate IS NULL THEN TERM_DATE
									ELSE nextstartdate
								END AS nextstartdate FROM
								(
									SELECT 
										s.EMPI
										,EFF_DATE
										,TERM_DATE
										,(		
											SELECT Top 1 
												s1.EFF_DATE
											FROM ENROLLMENT s1
											WHERE 
												s1.ROOT_COMPANIES_ID=s.ROOT_COMPANIES_ID AND 
												s1.EMPI=s.EMPI AND 
												s.TERM_DATE<s1.EFF_DATE AND 
												Convert(date,s1.EFF_DATE)<=@ce_enddt AND 
												Convert(date,s1.TERM_DATE)>=@ce_startdt
											ORDER BY 
												s1.EMPI
												,s1.EFF_DATE
												,s1.TERM_DATE
										) AS nextstartdate
									 FROM ENROLLMENT s
									WHERE 
										ROOT_COMPANIES_ID=@rootId AND 
										s.EFF_DATE<=@ce_enddt AND 
										s.TERM_DATE>=@ce_startdt
								)t
							)t1 
						)t2 
						GROUP BY 
							EMPI 
						HAVING 
							SUM(diff)<=1
					)
					s1 ON 
						s.EMPI=s1.EMPI
					WHERE 
						s.ROOT_COMPANIES_ID=@rootId	AND 
						s.EFF_DATE<=@ce_enddt AND 
						s.TERM_DATE>=@ce_startdt
	
				)t2
			)t3 
			GROUP BY 
				EMPI 
			HAVING 
			sum(coverage)>=(DATEPART(dy, @ce_enddt)-45)

		Union All

		-- Anchor Date
	 
		SELECT 
			EMPI
			,SUM(DATEDIFF(day,start_date,end_date)) AS coverage
			, 1 AS ceflag 
		FROM
		(
			SELECT 
				EMPI
				,TERM_DATE
				,CASE
					WHEN EFF_DATE<@ce_startdt THEN @ce_startdt
					ELSE EFF_DATE
				END AS start_date
				,CASE
					WHEN TERM_DATE>@ce_enddt THEN @ce_enddt
					ELSE TERM_DATE
				END AS end_date
			FROM ENROLLMENT
			WHERE 
				ROOT_COMPANIES_ID=@rootId and
				 @ce_enddt BETWEEN EFF_DATE AND TERM_DATE
		)t4  
		GROUP BY 
			EMPI
 
	)cont_enroll 
	GROUP BY 
		EMPI  
	HAVING
		SUM(ceflag)=2 
	order by 
		EMPI;
	
	update #CHLdataset set CE=1 from #CHLdataset ds join #CHL_contenroll ce on ds.EMPI=ce.EMPI;
	

	 -- Hospice Exclusion

	drop table if exists #CHL_hospicemembers;
	CREATE table #CHL_hospicemembers
	(
		EMPI varchar(100)
		
	);
	Insert into #CHL_hospicemembers
	select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)

	update #CHLdataset set rexcl=1 from #CHLdataset ds join #CHL_hospicemembers hos on hos.EMPI=ds.EMPI;
		
	
-- Event/ Diagnosis

	drop table if exists #CHL_Event;
	CREATE table #CHL_Event
	(
		EMPI varchar(100)
		
	)
	insert into #CHL_Event
	Select distinct 
		EMPI 
	from
	(
		
		Select
		EMPI
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	Proc_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy Tests'
	)
		Union all
	Select
		EMPI
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Pregnancy')
	WHERE DIAG_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0)
		Union all
		
			Select
		EMPI
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	Proc_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
	)

		Union all
				Select
		EMPI
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	Proc_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Sexual Activity'
	)
	union all
	Select
		EMPI
	FROM LAB 
	Where
		ROOT_COMPANIES_ID=@rootId and
		LAB_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Sexual Activity')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Sexual Activity')
			)
		)
	

		Union all
		
		select 
			d.EMPI
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			--ISNULL(c.POS,'')!='81' and 
			DIAG_START_DATE between @ce_startdt and @ce_enddt and
			Diag_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) AND
			DIAG_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where code_system in('ICD9CM','ICD10CM','SNOMED CT US Edition') and Value_Set_Name='Sexual Activity'
			) 

		Union all
		select 
			EMPI 
		from MEDICATION 
		where 
			ROOT_COMPANIES_ID=@rootId  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between @ce_startdt and @ce_enddt and FILL_DATE!='' and
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications'
			)

)t1



update #CHLdataset set Event=1 from #CHLdataset ds join #CHL_Event cc on ds.empi= cc.empi
	

-- Numerator******************

	drop table if exists #CHL_numlist;
	CREATE table #CHL_numlist
	(
		EMPI varchar(100)
		
	)
	Insert into #CHL_numlist
	select distinct 
		EMPI 
	from
	(
			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Chlamydia Tests'
	)
	UNION ALL
			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	ICDPCS_CODE in 
	(
	select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Chlamydia Tests'
	)
	Union All
	Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From DIAGNOSIS
	where 
	ROOT_COMPANIES_ID=@rootId and 
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in 
	(
	select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name IN('Chlamydia Tests')
	)

	Union All
	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
			)
		)
	
	)t1

	update #CHLdataset set num=1 from #CHLdataset ds
	join #CHL_numlist num on ds.EMPI=num.EMPI ;
	


	-- Numerator Details*********

	drop table if exists #CHL_numdetails;
	CREATE table #CHL_numdetails
	(
		EMPI varchar(100),
		Code varchar(20),
		ServiceDate Date
			
	)
	Insert into #CHL_numdetails
	select
		EMPI
		,Code
		,ServiceDate
	From
	(
		select 
			EMPI 
			,Code
			,ServiceDate
			,row_number() over(partition by EMPI order by ServiceDate Desc) as rn
		from
		(

				Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Chlamydia Tests'
	)
	UNION ALL
			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	ICDPCS_CODE in 
	(
	select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Chlamydia Tests'
	)
	Union All
	Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From DIAGNOSIS
	where 
	ROOT_COMPANIES_ID=@rootId and 
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in 
	(
	select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name IN('Chlamydia Tests')
	)

	Union All
	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests')
			)
		)

	
		)t1
	)t2
	Where rn=1
	

drop table if exists #Pregnancy_Tests_Members
CREATE table #Pregnancy_Tests_Members
(
	EMPI varchar(100)
);
Insert into #Pregnancy_Tests_Members
Select
		EMPI
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	Proc_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy Tests'
	)



		
-----Required Exclusion
drop table if exists #Pregnancy_Tests_Exclusion
CREATE table #Pregnancy_Tests_Exclusion
(
	EMPI varchar(100),
	Date_S  date
);
Insert into #Pregnancy_Tests_Exclusion
Select distinct
	EMPI
	,ServiceDate
From
(

	Select
		EMPI
		,ResultDate as ServiceDate
	From Lab
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		TestCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)

	Union All

	Select
		EMPI
		,ResultDate as ServiceDate
	From Lab
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		ResultCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
	From Procedures
	Where
		ROOT_COMPANIES_ID=@rootId and
		--ISNULL(PROC_STATUS,'EVN')!='INT' and
		PROC_START_DATE between @ce_startdt and @ce_enddt and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)
	Union all

	Select
		EMPI
		,VisitDate as ServiceDate
	From visit
	Where
		ROOT_COMPANIES_ID=@rootId and
		--ISNULL(PROC_STATUS,'EVN')!='INT' and
		VisitDate between @ce_startdt and @ce_enddt and
		visittypecode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)
		Union all

	Select
		EMPI
		,DIAG_START_DATE as ServiceDate
	From DIAGNOSIS
	Where
		ROOT_COMPANIES_ID=@rootId and
		--ISNULL(PROC_STATUS,'EVN')!='INT' and
		DIAG_START_DATE between @ce_startdt and @ce_enddt and
		DIAG_CODE in
		(
			select Code_New from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy Test Exclusion')
		)

)t1	




-----BIT 1.2

drop table if exists #Medication_XRAY
CREATE table #Medication_XRAY
(
	EMPI varchar(100),
	DATE_S Date
);
Insert into #Medication_XRAY
Select distinct
	EMPI
	,ServiceDate
From
(
	Select
		EMPI
		,FILL_DATE as ServiceDate
	From MEDICATION
	Where
		ROOT_COMPANIES_ID=@rootId and
		FILL_DATE between @ce_startdt and DATEADD(d,6,@ce_enddt) and
		MEDICATION_CODE in
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Retinoid Medications'
		)

	Union all

		Select
		EMPI
		,PROC_START_DATE as ServiceDate
	From GetProcedures(@rootId,@ce_startdt,DATEADD(d,6,@ce_enddt),'Diagnostic Radiology')

	Union all

		Select
		EMPI
		,DIAG_START_DATE as ServiceDate
	From GetDiagnosis(@rootId,@ce_startdt,DATEADD(d,6,@ce_enddt),'Diagnostic Radiology')

)t1


drop table if exists #Exclusion_Window
CREATE table #Exclusion_Window
(
	EMPI varchar(100)
);
insert into #Exclusion_Window
Select Distinct 
	A.EMPI 
from #Pregnancy_Tests_Exclusion A
Inner join #Medication_XRAY B on 
	A.EMPI=B.EMPI and  
	B.Date_S BETWEEN A.Date_S AND dateadd(d,6,A.Date_S)

 --Event without Preg Test Value Set
 drop table if exists #Preg_sexualActivity_Rx_Contraceptive;
	CREATE table #Preg_sexualActivity_Rx_Contraceptive
	(
		EMPI varchar(100)
		
	)
	insert into #Preg_sexualActivity_Rx_Contraceptive
	Select distinct
	EMPI
From
(
	--	Select
	--	EMPI
	--From Diagnosis
	--where 
	--ROOT_COMPANIES_ID=@rootId and 
	--DIAG_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	--DIAG_START_DATE between @ce_startdt and @ce_enddt and
	--DIAG_CODE in 
	--(
	--select Code_New from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
	--)
	Select
		EMPI
	From GetDiagnosis(@rootId ,@ce_startdt ,@ce_enddt,'Pregnancy')
	WHERE DIAG_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0)
	
		Union all
		
			Select
		EMPI
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	Proc_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
	)

		Union all
				Select
		EMPI
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	Proc_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Sexual Activity'
	)
	union all
	Select
		EMPI
	FROM LAB 
	Where
		ROOT_COMPANIES_ID=@rootId and
		LAB_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Sexual Activity')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Sexual Activity')
			)
		)
	

		Union all
		
		select 
			d.EMPI
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			--ISNULL(c.POS,'')!='81' and 
			DIAG_START_DATE between @ce_startdt and @ce_enddt and
			Diag_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) AND
			DIAG_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where code_system in('ICD9CM','ICD10CM','SNOMED CT US Edition') and Value_Set_Name='Sexual Activity'
			) 

		Union all
		select 
			EMPI 
		from MEDICATION 
		where 
			ROOT_COMPANIES_ID=@rootId  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between @ce_startdt and @ce_enddt and 
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications'
			)
)t1

------EXcl_Preg_Test for optional Exclusion
	drop table if exists #EXCL;
	CREATE table #EXCL
	(
		EMPI varchar(100)
			
	);
	insert into #EXCL
	
	select  
		A.EMPI 
	from 

	(
		select Distinct 
			A.EMPI
			,1 as Excl 
		from #Pregnancy_Tests_Members A
		left join #Preg_sexualActivity_Rx_Contraceptive B on 
			A.EMPI=B.EMPI
		inner join #Exclusion_Window C on 
			A.EMPI=C.EMPI 
			where B.EMPI is null
			)A

	



update #CHLdataset set EXCL=1 from #CHLdataset ds join #EXCL E on E.EMPI=ds.EMPI;




-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI AS memid,meas,payer,CE, [EVENT],CASE WHEN CE='1'  AND EVENT='1' AND rexcl='0' and rexcld='0' AND PAYER not in ('MCS','MC','MCR','MP','MC','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,gender,@meas,@meas_year,@reportId,@rootId FROM #CHLdataset 

GO
