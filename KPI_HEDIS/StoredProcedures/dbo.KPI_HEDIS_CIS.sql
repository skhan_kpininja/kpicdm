SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE PROCEDURE [dbo].[KPI_HEDIS_CIS]
AS
-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='CIS-S'
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_startdt2 DATE;
DECLARE @ce_enddt2 DATE;
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;
DECLARE @t_planid VARCHAR(10);

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='CIS';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_startdt2=concat(@meas_year-2,'-10-01');
SET @ce_enddt2=concat(@meas_year-2,'-12-31');

/*
Set @reporttype='Physician'
Set @measurename='Breast Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=85
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='22'
*/





-- Create Temp Patient Enrollment
-- Create Temp Patient Enrollment
drop table if exists #cis_tmpsubscriber;
CREATE TABLE #cis_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #cis_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=Dateadd(year,2,Date_of_Birth) AND 
	TERM_DATE>=Dateadd(year,1,Date_of_Birth) AND 
	Dateadd(year,2,Date_of_Birth) between @ce_startdt and @ce_enddt
ORDER BY 
	en.EMPI,en.EFF_DATE,en.TERM_DATE;



-- Create CIS Data Set

drop table if exists #cisdataset;
CREATE TABLE #cisdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'

) ;

insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'CISDTP'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(nxtpayer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(prvpayer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR'))  then TRIM(Payer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #cis_tmpsubscriber 
				where  
					StartDate<=DATEADD(year,2,dob)
				--	and EMPI=139080
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='CIS'
Order by 1


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISOPV'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISMMR'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISHIB'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISHEPB'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISVZV'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISPNEU'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISHEPA'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISROTA'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISINFL'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'



insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISCMB3'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'
	and payer not in('MEP','MMO','MOS','MPO')


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISCMB7'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'
	and payer not in('MEP','MMO','MOS','MPO')


insert into #cisdataset(EMPI,meas,payer,Gender,age)
Select 
	EMPI
	,'CISCMB10'
	,payer
	,gender
	,age
From #cisdataset 
where
	meas='CISDTP'
	




-- Continuous Enrollment
Exec SP_KPI_DROPTABLE '#cis_contenroll'
;With coverage_CTE1 (EMPI,dob,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select distinct
		EMPI
		,dob
		,isnull(lag(EndDate,1) over(partition by EMPI order by StartDate,EndDate desc),DATEADD(year,1,dob)) as lastcoveragedate
		,Case 
			when StartDate<DATEADD(year,1,dob) then DATEADD(year,1,dob)
			else StartDate 
		end as Startdate
		,case 
			when EndDate>DATEADD(year,2,dob) then DATEADD(year,2,dob)
			else EndDate 
		end as Finishdate
		,isnull(lead(StartDate,1) over(partition by EMPI order by StartDate,EndDate),DATEADD(year,2,dob)) as nextcoveragedate  
	from #cis_tmpsubscriber 
	Where
		StartDate<=DATEADD(year,2,dob)
)
select 
	t3.EMPI 
into #cis_contenroll
from
(
	select 
		t2.EMPI 
	from
	(
		select 
			*
			,case 
				when rn=1 and startdate>DATEADD(year,1,dob) then 1 
				else 0 
			end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case 
				when DATEADD(year,2,dob) between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
		from
		(
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI order by Startdate,FinishDate),DATEADD(year,2,dob)) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by EMPI order by Startdate,Finishdate) as rn 
			from coverage_CTE1
		)t1           
	)t2  
	group by 
		EMPI,dob 
	having
		(sum(gaps)+sum(startgap))<=1 and 
		sum(coveragedays)>=((datediff(day,Dateadd(year,1,dob),DATEADD(year,2,dob))+1)-45) and 
		sum(anchor)>=1
)t3



Update ds Set CE=1 from #cisdataset ds join #cis_contenroll ce on ds.EMPI=ce.EMPI



-- Numerator DTaP 
Exec SP_KPI_DROPTABLE '#cis_dtap'
Select distinct
	*
into #cis_dtap
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='DTaP Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='DTaP Vaccine Procedure'
			)

		
)tbl





Exec SP_KPI_DROPTABLE '#cis_num_dtap';
Select
	EMPI
into #cis_num_dtap
From
(
	Select distinct
		EMPI
		,FILL_DATE
	From
	(
		Select
			*
			,Case
				When 
					--nxtsrc in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
					DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
				else 0
			End as exclude
		From
		(
			Select 
				*
				,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
				,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
			from #cis_dtap
		)tbl
	)tbl1
	Where
		exclude=0
)tbl2
Group By
	EMPI
Having
	count(*)>=4



Update ds  Set Num=1 from #cisdataset ds join #cis_num_dtap d on ds.EMPI=d.EMPI and ds.meas='CISDTP'



-- Numerator IPV
Exec SP_KPI_DROPTABLE '#cis_ipv'
Select distinct
	*
into #cis_ipv
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Inactivated Polio Vaccine (IPV) Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Inactivated Polio Vaccine (IPV) Procedure'
			)

		
)tbl





Exec SP_KPI_DROPTABLE '#cis_num_ipv';
Select
	EMPI
into #cis_num_ipv
From
(
	Select distinct
		EMPI
		,FILL_DATE
	From
	(
		Select
			*
			,Case
				When 
					--nxtsrc in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
					DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
				else 0
			End as exclude
		From
		(
			Select 
				*
				,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
				,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
			from #cis_ipv
		)tbl
	)tbl1
	Where
		exclude=0
)tbl2
Group By
	EMPI
Having
	count(*)>=3



Update ds  Set Num=1 from #cisdataset ds join #cis_num_ipv d on ds.EMPI=d.EMPI and ds.meas='CISOPV'


-- Numerator Logic for MMR

Exec SP_KPI_DROPTABLE '#cis_mmr1'
Select distinct
	*
into #cis_mmr1
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Measles, Mumps and Rubella (MMR) Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Measles, Mumps and Rubella (MMR) Vaccine Procedure'
			)

		
)tbl



Exec SP_KPI_DROPTABLE '#cis_rubella'
Select distinct
	*
into #cis_rubella
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Measles Rubella Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Measles Rubella Vaccine Procedure'
			)

		
)tbl



Exec SP_KPI_DROPTABLE '#cis_mumps'
Select distinct
	*
into #cis_mumps
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Mumps Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Mumps Vaccine Procedure'
			)

		Union all

		Select
			d.EMPI
			,DIAG_START_DATE
			,DIAG_DATA_SRC
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Mumps'
			)
		
		
)tbl


-- ?	At least one measles and rubella vaccination (Measles Rubella Immunization Value Set; Measles Rubella Vaccine Procedure Value Set) on or between the child?s first and second birthdays and one of the following: 
--	At least one mumps vaccination (Mumps Immunization Value Set; Mumps Vaccine Procedure Value Set) on or between the child?s first and second birthdays. 
--	History of mumps illness (Mumps Value Set) any time on or before the child?s second birthday. 

Exec SP_KPI_DROPTABLE '#cis_mmr2'
Select distinct
	r.EMPI
into #cis_mmr2
From #cis_rubella r
Join #cis_mumps m on
	m.EMPI=r.EMPI



-- ?	Any combination of codes from the table below that indicates evidence of all three antigens (on the same or different date of service).
Exec SP_KPI_DROPTABLE '#cis_measles'
Select distinct
	*
into #cis_measles
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Measles Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Measles Vaccine Procedure'
			)

		Union all

		Select
			d.EMPI
			,DIAG_START_DATE
			,DIAG_DATA_SRC
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Measles'
			)
		
		
)tbl



-- Rubella

Exec SP_KPI_DROPTABLE '#cis_rubellahisory'
Select distinct
	*
into #cis_rubellahisory
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Rubella Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(year,1,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Rubella Vaccine Procedure'
			)

		Union all

		Select
			d.EMPI
			,DIAG_START_DATE
			,DIAG_DATA_SRC
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Rubella'
			)
		
		
)tbl



Exec SP_KPI_DROPTABLE '#cis_mmr3'
Select distinct
	u.EMPI
into #cis_mmr3
From #cis_mumps u
join #cis_measles m on 
	u.EMPI=m.EMPI
Join #cis_rubellahisory r on
	u.EMPI=r.EMPI




Exec SP_KPI_DROPTABLE '#cis_num_mmr'
Select distinct
	EMPI
into #cis_num_mmr
From
(
	Select EMPI from #cis_mmr1
	union all
	Select EMPI from #cis_mmr2
	union all
	Select EMPI from #cis_mmr3
)tbl1



Update ds Set NUM=1 from #cisdataset ds join #cis_num_mmr n on ds.EMPI=n.EMPI and ds.meas='CISMMR'


-- Numerator HiB
Exec SP_KPI_DROPTABLE '#cis_hib'
Select distinct
	*
into #cis_hib
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Haemophilus Influenzae Type B (HiB) Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Haemophilus Influenzae Type B (HiB) Vaccine Procedure'
			)

		
)tbl





Exec SP_KPI_DROPTABLE '#cis_num_hib';
Select
	EMPI
into #cis_num_hib
From
(
	Select distinct
		EMPI
		,FILL_DATE
	From
	(
		Select
			*
			,Case
				When 
					--nxtsrc in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
					DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
				else 0
			End as exclude
		From
		(
			Select 
				*
				,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
				,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
			from #cis_hib
		)tbl
	)tbl1
	Where
		exclude=0
)tbl2
Group By
	EMPI
Having
	count(*)>=3



Update ds  Set Num=1 from #cisdataset ds join #cis_num_hib d on ds.EMPI=d.EMPI and ds.meas='CISHIB'


-- Hepatitis B


Exec SP_KPI_DROPTABLE '#cis_hepb'
Select distinct
	*
into #cis_hepb
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between Date_of_Birth and  Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Hepatitis B Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between Date_of_Birth and  Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Hepatitis B Vaccine Procedure'
			)


		Union all


		Select
			EMPI
			,PROC_START_DATE
			,PROC_DATA_SRC
		From
		(
			Select
				m.EMPI
				,m.PROC_START_DATE
				,m.PROC_DATA_SRC
				,ROW_NUMBER() over(partition By m.EMPI order By Proc_Start_Date) as rn
			From Procedures m
			Left outer Join CLAIMLINE c on
				m.EMPI=c.EMPI and
				ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
				m.CLAIM_ID=c.CLAIM_ID and
				m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
				m.PROC_DATA_SRC=c.CL_DATA_SRC
			Join open_empi_master oe on
				m.EMPI=EMPI_ID and
				m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
			Where
				m.ROOT_COMPANIES_ID=@rootId and
			
				ISNULL(c.POS,'0')!='81' and
				ISNULL(PROC_STATUS,'EVN')!='INT' and
				PROC_START_DATE between Date_of_Birth and Dateadd(day,7,Date_of_Birth) and
				(
					ICDPCS_CODE in
					(
						Select code_new from HDS.VALUESET_TO_CODE where  Value_Set_Name='Newborn Hepatitis B Vaccine Administered'
					)
					or
					PROC_CODE in
					(
						Select code_new from HDS.VALUESET_TO_CODE where  Value_Set_Name='Newborn Hepatitis B Vaccine Administered'
					)
				)

		)t1
		Where
			rn=1
		
)tbl



Exec SP_KPI_DROPTABLE '#cis_num_hepb';
Select distinct
	EMPI
into #cis_num_hepb
From
(
	

		Select
			d.EMPI
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Hepatitis B'
			)

		Union all


		Select
			EMPI
		From
		(
			Select distinct
				EMPI
				,FILL_DATE
			From
			(
				Select
					*
					,Case
						When 
							--nxtsrc in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
							DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
						else 0
					End as exclude
				From
				(
					Select 
						*
						,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
						,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
					from #cis_hepb
					--where EMPI=100011
				)tbl
			)tbl1
			Where
				exclude=0
		)tbl2
		Group By
			EMPI
		Having
			count(*)>=3

)tbl



Update ds  Set Num=1 from #cisdataset ds join #cis_num_hepb d on ds.EMPI=d.EMPI and ds.meas='CISHEPB'



-- VZV

Exec SP_KPI_DROPTABLE '#cis_num_vzv'
Select distinct
	*
into #cis_num_vzv
From
(
		Select 
			m.EMPI
			
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between Dateadd(year,1,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Varicella Zoster (VZV) Immunization'
			)

		Union all

		Select
			m.EMPI
			
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between Dateadd(year,1,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Varicella Zoster (VZV) Vaccine Procedure'
			)


		Union all

		Select
			d.EMPI
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Varicella Zoster'
			)

		
)tbl



Update ds  Set Num=1 from #cisdataset ds join #cis_num_vzv d on ds.EMPI=d.EMPI and ds.meas='CISVZV'



-- Numerator Pneumococcal conjugate
Exec SP_KPI_DROPTABLE '#cis_Pneumococcal'
Select distinct
	*
into #cis_Pneumococcal
From
(
		Select 
			m.EMPI
			,m.FILL_DATE
			,m.MED_DATA_SRC
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Pneumococcal Conjugate Immunization'
			)

		Union all

		Select
			m.EMPI
			,m.PROC_START_DATE
			,m.PROC_DATA_SRC
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between dateadd(day,42,Date_of_Birth) and Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Pneumococcal Conjugate Vaccine Procedure'
			)

		
)tbl





Exec SP_KPI_DROPTABLE '#cis_num_pneumococcal';
Select
	EMPI
into #cis_num_pneumococcal
From
(
	Select distinct
		EMPI
		,FILL_DATE
	From
	(
		Select
			*
			,Case
				When 
					--nxtsrc in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
					DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
				else 0
			End as exclude
		From
		(
			Select 
				*
				,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
				,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
			from #cis_Pneumococcal
		)tbl
	)tbl1
	Where
		exclude=0
)tbl2
Group By
	EMPI
Having
	count(*)>=4



Update ds  Set Num=1 from #cisdataset ds join #cis_num_pneumococcal d on ds.EMPI=d.EMPI and ds.meas='CISPNEU'


-- Hepatitis A

Exec SP_KPI_DROPTABLE '#cis_num_hepa'
Select distinct
	*
into #cis_num_hepa
From
(
		Select 
			m.EMPI
		
		From Medication m
		Join open_empi_master oe on
			EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			FILL_DATE between Dateadd(year,1,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
			MEDICATION_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Hepatitis A Immunization'
			)

		Union all

		Select
			m.EMPI
		
		From Procedures m
		Left outer Join CLAIMLINE c on
			m.EMPI=c.EMPI and
			ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			m.CLAIM_ID=c.CLAIM_ID and
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			m.PROC_DATA_SRC=c.CL_DATA_SRC
		Join open_empi_master oe on
			m.EMPI=EMPI_ID and
			m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
		Where
			m.ROOT_COMPANIES_ID=@rootId and
			
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between Dateadd(year,1,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
			PROC_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Hepatitis A Vaccine Procedure'
			)


		Union all


		Select
			d.EMPI
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Hepatitis A'
			)


		
		
)tbl

Update ds  Set Num=1 from #cisdataset ds join #cis_num_hepa d on ds.EMPI=d.EMPI and ds.meas='CISHEPA'







--- Rotavirus



Exec SP_KPI_DROPTABLE '#cis_2doserotavirus';
Select
	EMPI,
	FILL_DATE,
	MED_DATA_SRC
Into #cis_2doserotavirus
From
(
	Select
		*
		,Case
			When DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
			else 0
		End as exclude
	From
	(
		Select
			*
			,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
			,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
		From
		(
			Select distinct
				*
			From
			(
					Select 
						m.EMPI
						,m.FILL_DATE
						,m.MED_DATA_SRC
					From Medication m
					Join open_empi_master oe on
						EMPI=EMPI_ID and
						m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
					Where
						m.ROOT_COMPANIES_ID=@rootId and
			
						FILL_DATE between Dateadd(day,42,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
						MEDICATION_CODE in
						(
							Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Rotavirus (2 Dose Schedule) Immunization'
						)

					Union all

					Select
						m.EMPI
						,m.PROC_START_DATE
						,m.PROC_DATA_SRC
					From Procedures m
					Left outer Join CLAIMLINE c on
						m.EMPI=c.EMPI and
						ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
						m.CLAIM_ID=c.CLAIM_ID and
						m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
						m.PROC_DATA_SRC=c.CL_DATA_SRC
					Join open_empi_master oe on
						m.EMPI=EMPI_ID and
						m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
					Where
						m.ROOT_COMPANIES_ID=@rootId and
			
						ISNULL(c.POS,'0')!='81' and
						ISNULL(PROC_STATUS,'EVN')!='INT' and
						PROC_START_DATE between Dateadd(day,42,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
						PROC_CODE in
						(
							Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Rotavirus Vaccine (2 Dose Schedule) Procedure'
						)

			)tbl
		)tbl1
	)tbl2
)tbl3
Where 
	exclude=0



Exec SP_KPI_DROPTABLE '#cis_3doserotavirus';
Select
	EMPI,
	FILL_DATE,
	MED_DATA_SRC
Into #cis_3doserotavirus
From
(
	Select
		*
		,Case
			When DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
			else 0
		End as exclude
	From
	(
		Select
			*
			,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
			,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
		From
		(
			Select distinct
				*
			From
			(
					Select 
						m.EMPI
						,m.FILL_DATE
						,m.MED_DATA_SRC
					From Medication m
					Join open_empi_master oe on
						EMPI=EMPI_ID and
						m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
					Where
						m.ROOT_COMPANIES_ID=@rootId and
			
						FILL_DATE between Dateadd(day,42,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
						MEDICATION_CODE in
						(
							Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Rotavirus (3 Dose Schedule) Immunization'
						)

					Union all

					Select
						m.EMPI
						,m.PROC_START_DATE
						,m.PROC_DATA_SRC
					From Procedures m
					Left outer Join CLAIMLINE c on
						m.EMPI=c.EMPI and
						ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
						m.CLAIM_ID=c.CLAIM_ID and
						m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
						m.PROC_DATA_SRC=c.CL_DATA_SRC
					Join open_empi_master oe on
						m.EMPI=EMPI_ID and
						m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
					Where
						m.ROOT_COMPANIES_ID=@rootId and
			
						ISNULL(c.POS,'0')!='81' and
						ISNULL(PROC_STATUS,'EVN')!='INT' and
						PROC_START_DATE between Dateadd(day,42,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
						PROC_CODE in
						(
							Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Rotavirus Vaccine (3 Dose Schedule) Procedure'
						)

			)tbl
		)tbl1
	)tbl2
)tbl3
Where 
	exclude=0



Exec SP_KPI_DROPTABLE '#cis_num_rotavirus';
Select distinct
	EMPI	
Into #cis_num_rotavirus
From
(
	Select
		EMPI
	From
	(
		Select distinct
			EMPI
			,FILL_DATE
		From #cis_2doserotavirus
	)tbl
	Group by
		EMPI
	having
		count(*)>=2

	Union all

	Select
		EMPI
	From
	(
		Select distinct
			EMPI
			,FILL_DATE
		From #cis_3doserotavirus
	)tbl
	Group by
		EMPI
	having
		count(*)>=3

	Union all


	Select
		EMPI
	From
	(
		Select distinct
			*
		From
		(

			Select 
				*
			From
			(
				Select
					EMPI
					,FILL_DATE
				From
				(
					Select
						EMPI
						,FILL_DATE
						,ROW_NUMBER() over(Partition By EMPI order by FILL_DATE) as rn
					From #cis_2doserotavirus
				)tbl
				Where rn=1
			)t1

			Union all

			Select 
				*
			From
			(
				Select
					EMPI
					,FILL_DATE
				From
				(
					Select
						EMPI
						,FILL_DATE
						,ROW_NUMBER() over(Partition By EMPI order by FILL_DATE) as rn
					From #cis_3doserotavirus
				)tbl
				Where rn<=2
			)t1

		)tbl1
	)tbl2
	Group by
		EMPI
	having
		count(*)>=3
)tbl3



Update ds Set NUM=1 from #cisdataset ds join #cis_num_rotavirus r on ds.EMPI=r.EMPI and ds.meas='CISROTA'


-- Influenza

Exec SP_KPI_DROPTABLE '#cis_influenza'
Select
	EMPI,
	FILL_DATE,
	MED_DATA_SRC
into #cis_influenza
From
(
	Select
		*
		,Case
			When DATEDIFF(day,FILL_DATE,nxtfilldate) between 0 and 14 then 1
			else 0
		End as exclude
	From
	(
		Select
			*
			,Lead(MED_DATA_SRC) over(partition by EMPI order by Fill_Date asc) as nxtsrc
			,Lead(FILL_DATE) over(partition by EMPI order by Fill_Date asc) as nxtfilldate
		From
		(
			Select distinct
				*
			From
			(
					Select 
						m.EMPI
						,m.FILL_DATE
						,m.MED_DATA_SRC
					From Medication m
					Join open_empi_master oe on
						EMPI=EMPI_ID and
						m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
					Where
						m.ROOT_COMPANIES_ID=@rootId and
			
						FILL_DATE between Dateadd(day,180,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
						MEDICATION_CODE in
						(
							Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Influenza Immunization'
						)

					Union all

					Select
						m.EMPI
						,m.PROC_START_DATE
						,m.PROC_DATA_SRC
					From Procedures m
					Left outer Join CLAIMLINE c on
						m.EMPI=c.EMPI and
						ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
						m.CLAIM_ID=c.CLAIM_ID and
						m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
						m.PROC_DATA_SRC=c.CL_DATA_SRC
					Join open_empi_master oe on
						m.EMPI=EMPI_ID and
						m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
					Where
						m.ROOT_COMPANIES_ID=@rootId and
			
						ISNULL(c.POS,'0')!='81' and
						ISNULL(PROC_STATUS,'EVN')!='INT' and
						PROC_START_DATE between Dateadd(day,180,Date_of_Birth) and  Dateadd(year,2,Date_of_Birth) and
						PROC_CODE in
						(
							Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Influenza Vaccine Procedure'
						)

			)tbl
		)tbl1
	)tbl2
)tbl3
Where 
	exclude=0




				Exec SP_KPI_DROPTABLE '#cis_laivinfluenza'	
				Select
					EMPI
					,FILL_DATE
					,MED_DATA_SRC
				into #cis_laivinfluenza
				From
				(
					Select 
						EMPI
						,FILL_DATE
						,MED_DATA_SRC
						,row_number() over(partition by EMPI order by FILL_DATE) as rn
					From
					(
						Select 
							m.EMPI
							,m.FILL_DATE
							,m.MED_DATA_SRC
						From Medication m
						Join open_empi_master oe on
							EMPI=EMPI_ID and
							m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
						Where
							m.ROOT_COMPANIES_ID=@rootId and
							FILL_DATE = Dateadd(year,2,Date_of_Birth) and
							MEDICATION_CODE in
							(
								Select code from HDS.VALUESET_TO_CODE where Code_System='CVX' and Value_Set_Name='Influenza Virus LAIV Immunization'
							)

						Union all

						Select
							m.EMPI
							,m.PROC_START_DATE
							,m.PROC_DATA_SRC
						From Procedures m
						Left outer Join CLAIMLINE c on
							m.EMPI=c.EMPI and
							ISNULL(m.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
							m.CLAIM_ID=c.CLAIM_ID and
							m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
							m.PROC_DATA_SRC=c.CL_DATA_SRC
						Join open_empi_master oe on
							m.EMPI=EMPI_ID and
							m.ROOT_COMPANIES_ID=oe.Root_Companies_ID
						Where
							m.ROOT_COMPANIES_ID=@rootId and
			
							ISNULL(c.POS,'0')!='81' and
							ISNULL(PROC_STATUS,'EVN')!='INT' and
							PROC_START_DATE =  Dateadd(year,2,Date_of_Birth) and
							PROC_CODE in
							(
								Select code from HDS.VALUESET_TO_CODE where  Value_Set_Name='Influenza Virus LAIV Vaccine Procedure'
							)
					)tbl
				)tbl1
				Where
					rn=1



Exec SP_KPI_DROPTABLE '#cis_num_influenza'
Select
	EMPI
into #cis_num_influenza
From
(
	Select distinct
		EMPI
		,FILL_DATE
	From
	(
		Select
			*
		From #cis_influenza

		Union all

		Select 
			*
		From #cis_laivinfluenza
	)tbl1
)tbl2
Group By
	EMPI
Having
	count(*)>=2


Update ds Set Num=1 from #cisdataset ds join #cis_num_influenza i on ds.EMPI=i.EMPI and ds.meas='CISINFL'









Exec SP_KPI_DROPTABLE '#cis_num_cmb3'
Select distinct
	d.EMPI
into #cis_num_cmb3
From #cis_num_dtap d
Join #cis_num_ipv ipv on
	d.EMPI=ipv.EMPI
Join #cis_num_mmr mmr on
	d.EMPI=mmr.EMPI
Join #cis_num_hib hib on
	d.EMPI=hib.EMPI
Join #cis_num_hepb hb on
	d.EMPI=hb.EMPI
Join #cis_num_vzv v on
	d.EMPI=v.EMPI
Join #cis_num_pneumococcal p on
	d.EMPI=p.EMPI


Update ds Set Num=1 from #cisdataset ds join #cis_num_cmb3 i on ds.EMPI=i.EMPI and ds.meas='CISCMB3'



Exec SP_KPI_DROPTABLE '#cis_num_cmb7'
Select distinct
	d.EMPI
into #cis_num_cmb7
From #cis_num_dtap d
Join #cis_num_ipv ipv on
	d.EMPI=ipv.EMPI
Join #cis_num_mmr mmr on
	d.EMPI=mmr.EMPI
Join #cis_num_hib hib on
	d.EMPI=hib.EMPI
Join #cis_num_hepb hb on
	d.EMPI=hb.EMPI
Join #cis_num_vzv v on
	d.EMPI=v.EMPI
Join #cis_num_pneumococcal p on
	d.EMPI=p.EMPI
Join #cis_num_hepa ha on
	d.EMPI=ha.EMPI
Join #cis_num_rotavirus r on
	d.EMPI=r.EMPI



Update ds Set Num=1 from #cisdataset ds join #cis_num_cmb7 i on ds.EMPI=i.EMPI and ds.meas='CISCMB7'


Exec SP_KPI_DROPTABLE '#cis_num_cmb10'
Select distinct
	d.EMPI
into #cis_num_cmb10
From #cis_num_dtap d
Join #cis_num_ipv ipv on
	d.EMPI=ipv.EMPI
Join #cis_num_mmr mmr on
	d.EMPI=mmr.EMPI
Join #cis_num_hib hib on
	d.EMPI=hib.EMPI
Join #cis_num_hepb hb on
	d.EMPI=hb.EMPI
Join #cis_num_vzv v on
	d.EMPI=v.EMPI
Join #cis_num_pneumococcal p on
	d.EMPI=p.EMPI
Join #cis_num_hepa ha on
	d.EMPI=ha.EMPI
Join #cis_num_rotavirus r on
	d.EMPI=r.EMPI
Join #cis_num_influenza i on
	d.EMPI=i.EMPI



Update ds Set Num=1 from #cisdataset ds join #cis_num_cmb10 i on ds.EMPI=i.EMPI and ds.meas='CISCMB10'



-- Optional Exclusion
-- ?	Anaphylactic reaction to the vaccine or its components (Anaphylactic Reaction Due To Vaccination Value Set).

		Exec SP_KPI_Droptable '#cis_anaphylactic'
		Select
			d.EMPI
		into #cis_anaphylactic
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Anaphylactic Reaction Due To Vaccination'
			)


	update ds  Set Excl=1 from #cisdataset ds join  #cis_anaphylactic a on ds.EMPI=a.EMPI

	
	-- ?	Encephalopathy (Encephalopathy Due To Vaccination Value Set) with a vaccine adverse-effect code (Vaccine Causing Adverse Effect Value Set)

	Exec SP_KPI_Droptable '#cis_vaccinereaction'
		Select
			d.EMPI
			,d.CLAIM_ID
			,d.DIAG_DATA_SRC
		into #cis_vaccinereaction
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Vaccine Causing Adverse Effect'
			)

		Exec SP_KPI_Droptable '#cis_Encephalopathy'
		Select
			d.EMPI
			,d.CLAIM_ID
			,d.DIAG_DATA_SRC
		into #cis_Encephalopathy
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name='Encephalopathy Due To Vaccination'
			)





	update ds  
		Set Excl=1 
	from #cisdataset ds 
	join  #cis_Encephalopathy e on 
		ds.EMPI=e.EMPI
	join  #cis_vaccinereaction v on 
		ds.EMPI=e.EMPI
	--Where ds.meas='CISDTP'


-- MMR, VZV and influenza

Exec SP_KPI_Droptable '#cis_mmr_optionalexclusion'
		Select distinct
			d.EMPI
		into #cis_mmr_optionalexclusion
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name in('Disorders of the Immune System','HIV','HIV Type 2','Malignant Neoplasm of Lymphatic Tissue')
			)

			update ds  Set Excl=1 from #cisdataset ds join  #cis_mmr_optionalexclusion a on ds.EMPI=a.EMPI -- and ds.meas in('CISMMR','CISVZV','CISINFL')


-- rotavirus

Exec SP_KPI_Droptable '#cis_rota_optionalexclusion'
		Select distinct
			d.EMPI
		into #cis_rota_optionalexclusion
		From DIAGNOSIS d
		Join open_empi_master oe on
			d.EMPI=oe.EMPI_ID and
			d.ROOT_COMPANIES_ID=oe.ROOT_COMPANIES_ID
		Left outer join Claimline c on
			d.EMPI=c.EMPI and
			d.CLAIM_ID=c.CLAIM_ID and
			d.DIAG_DATA_SRC=c.CL_DATA_SRC and
			d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Where
			d.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.pos,'0')!='81' and
			d.DIAG_START_DATE <= Dateadd(year,2,Date_of_Birth) and
			DIAG_CODE in
			(
				Select code_new from HDS.ValueSet_To_Code where code_system in('ICD10CM','ICD9CM','SNOMED CT US Edition') and Value_Set_Name in('Intussusception','Severe Combined Immunodeficiency')
			)

			update ds  Set Excl=1 from #cisdataset ds join  #cis_rota_optionalexclusion a on ds.EMPI=a.EMPI -- and ds.meas in('CISROTA')
			

			-- Hospice Exclusion
			Exec SP_KPI_DROPTABLE '#cis_hospice'
			Select distinct
				EMPI
			into #cis_hospice
			From hospicemembers(@rootId,@ce_startdt,@ce_enddt)


			update ds  Set RExcl=1 from #cisdataset ds join  #cis_hospice a on ds.EMPI=a.EMPI -- and ds.meas in('CISROTA')



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0 and PAYER not in ('MCS','MC','MCR','MP','MC','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #cisdataset

/*
	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,0 as Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0  THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #bcsdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #bcs_numdetails nd on d.EMPI=nd.EMPI
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type

*/

GO
