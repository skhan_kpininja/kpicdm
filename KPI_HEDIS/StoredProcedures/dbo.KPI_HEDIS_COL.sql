SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE PROCEDURE [dbo].[KPI_HEDIS_COL]
AS

-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='COL-A'
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_startdt4 DATE;
DECLARE @ce_startdt9 DATE;
DECLARE @ce_startdt2 DATE;
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;
DECLARE @t_planid VARCHAR(10);

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='COL';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_startdt4=concat(@meas_year-4,'-01-01');
SET @ce_startdt9=concat(@meas_year-9,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');

Set @reporttype='Physician'
Set @measurename='Colorectal Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=84
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='23'

/*** To Test
select memid, epop from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='COL-S' 
except
select memid, epop  from HDS.HEDIS_MEASURE_OUTPUT where ROOT_COMPANIES_ID='COL-S'

select MemID,Meas,Payer,CE,Event,Excl,Num,RExcl,RExclD,Age,Gender from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='COL-S' 
except
select EMPI,Meas,Payer,CE,Event,Excl,Num,RExcl,RExclD,Age,Gender from #COLdataset

SELECT * FROM HDS.HEDIS_MEASURE_OUTPUT where ROOT_COMPANIES_ID='COL-S' AND memid=128337
SELECT * FROM KPI_HEDIS.dbo.HEDIS_SCORE WHERE MEMID=128337 AND ROOT_COMPANIES_ID='COL-S' 
select * from #COLdataset where empi=108285
*/


-- Eligible Patient List
drop table if exists #COL_memlist; 
CREATE TABLE #COL_memlist 
(
    EMPI varchar(100)
    
)
insert into #COL_memlist
SELECT DISTINCT 
	en.EMPI 
FROM member gm
join ENROLLMENT en on en.EMPI=gm.EMPI and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1  AND 
	YEAR(@ce_enddt)-YEAR(MEM_DOB) BETWEEN 51 AND 75 
ORDER BY 1;



-- Create Temp Patient Enrollment
drop table if exists #COL_tmpsubscriber;
CREATE TABLE #COL_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #COL_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.MEM_DOB
	,gm.MEM_GENDER
	,YEAR(@ce_enddt)-YEAR(MEM_DOB) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM MEMBER gm
join ENROLLMENT en on en.EMPI=gm.EMPI and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1 AND 
	YEAR(@ce_enddt)-YEAR(MEM_DOB) BETWEEN 51 AND 75
	ORDER BY en.EMPI,en.EFF_DATE,en.TERM_DATE;



drop table if exists #COLdataset;
CREATE TABLE #COLdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'

) ;

insert into #COLdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'COL'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #COL_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=139080
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='COL'
Order by 1




-- Continuous Enrollment
Drop table if exists #COL_contenroll
Create table #COL_contenroll
(
	EMPI varchar(100)
)
Insert into #COL_contenroll
select 
	t1.EMPI
from GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0) t1
join
(
	select * from GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)
)t2 on t1.EMPI=t2.EMPI





	update #COLdataset set CE=1 from #COLdataset ds join #COL_contenroll ce on ds.EMPI=ce.EMPI;


	-- AdvancedIllness

	drop table if exists #COL_advillness;
	CREATE table #COL_advillness
	(
		EMPI varchar(100),
		servicedate date,
		claimid varchar(100)
	);
	Insert into #COL_advillness
	select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID 
	from advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
	
	-- Required Exclusion

	drop table if exists #COL_reqdexcl
	CREATE table #COL_reqdexcl
	(
		EMPI varchar(100)
			
	)
	insert into #COL_reqdexcl
	Select distinct
		EMPI
	From palliativecare(@rootId,@ce_startdt,@ce_enddt)
		
	
	
	update #COLdataset set rexcld=1 from #COLdataset ds join #COL_reqdexcl re on ds.EMPI=re.EMPI;

	
	
	
	-- Members with Institutinal SNP
	UPDATE #COLdataset SET #COLdataset.rexcl=1 FROM #COLdataset ds JOIN ENROLLMENT s on ds.EMPI=s.EMPI and s.ROOT_COMPANIES_ID=@rootId WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MR') AND s.EFF_DATE<=@ce_enddt AND s.TERM_DATE>=@ce_startdt AND s.PAYER_TYPE='SN2';


	-- LTI Exclusion
	drop table if exists #COL_LTImembers;
	CREATE table #COL_LTImembers
	(
		EMPI varchar(100)
	)
	Insert into #COL_LTImembers
	SELECT DISTINCT EMPI FROM MCFIELDS WHERE ROOT_COMPANIES_ID=@rootId AND RunDate BETWEEN @ce_startdt AND @ce_enddt AND LTI=1;

	
	update #COLdataset set rexcl=1 from #COLdataset ds join #COL_LTImembers re on ds.EMPI=re.EMPI where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MR');


	 -- Hospice Exclusion

	drop table if exists #COL_hospicemembers;
	CREATE table #COL_hospicemembers
	(
		EMPI varchar(100)
		
	);
	Insert into #COL_hospicemembers
	select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)

	update #COLdataset set rexcl=1 from #COLdataset ds join #COL_hospicemembers hos on hos.EMPI=ds.EMPI;
	
	
-- Frailty Members LIST
	drop table if exists #COL_frailtymembers;
	CREATE table #COL_frailtymembers
	(
		
		EMPI varchar(100)
			
	);
	Insert into #COL_frailtymembers
	Select distinct
		EMPI
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
	Where
		DataSource not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #COL_inpatientstaylist;
	CREATE table #COL_inpatientstaylist
	(
		EMPI varchar(100),
		date_s Date,
		claimid varchar(100)
	)
	Insert into #COL_inpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	
	-- Non acute Inpatient stay list
	
	drop table if exists #COL_noncauteinpatientstaylist;
	CREATE table #COL_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		date_s DATE,
		claimid varchar(100)
	);
	Insert into #COL_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	
	-- Outpatient and other visits
	drop table if exists #COL_visitlist;
	CREATE table #COL_visitlist
	(
		EMPI varchar(100),
		date_s date,
		claimid varchar(100)
	)
	Insert into #COL_visitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1

	
	-- Required exclusion table
	drop table if exists #COL_reqdexcl1;
	CREATE table #COL_reqdexcl1
	(
		EMPI varchar(100)
			
	)
	Insert into #COL_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.date_s 
			from
			(
					select 
						EMPI
						,date_s
						,claimid  
					from #COL_visitlist 
					
					
					union all
					
					select 
						na.EMPI
						,na.Date_s
						,na.claimid 
					from #COL_noncauteinpatientstaylist na
					join #COL_inpatientstaylist inp on na.EMPI=inp.EMPI and 
													   na.claimid=inp.claimid
					
			)t1
			Join #COL_advillness a on a.EMPI=t1.EMPI and 
									  a.claimid=t1.claimid
		)t2 
		group by t2.EMPI 
		having count(t2.EMPI)>1
	)t3 
	Join #COL_frailtymembers f on f.EMPI=t3.EMPI
	
	
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;


-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #COL_reqdexcl2;
	CREATE table #COL_reqdexcl2
	(
		EMPI varchar(100)
				
	)
	insert into #COL_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
		
		)t1
		Join #COL_advillness a on a.EMPI=t1.EMPI and a.claimid=t1.CLAIM_ID
	)t2
	join #COL_frailtymembers f on f.EMPI=t2.EMPI

	
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #COL_reqdexcl3;
	CREATE table #COL_reqdexcl3
	(
		EMPI varchar(100)
			
	)
	insert into #COL_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.date_s
			,t1.claimid 
		from
		(
			select 
				inp.EMPI
				,inp.date_s
				,inp.claimid 
			from #COL_inpatientstaylist inp
			left outer join #COL_noncauteinpatientstaylist na on inp.EMPI=na.EMPI and 
																 inp.claimid=na.claimid
			where na.EMPI is null
		)t1
		join #COL_advillness a on a.EMPI=t1.EMPI and a.claimid=t1.claimid
	)t2
	join #COL_frailtymembers f on f.EMPI=t2.EMPI

		
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #COL_reqdexcl4;
	CREATE table #COL_reqdexcl4
	(
		EMPI varchar(100)
				
	)
	insert into #COL_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		select 
			EMPI 
		from MEDICATION 
		where 
			ROOT_COMPANIES_ID=@rootId  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between @ce_startdt1 and @ce_enddt and 
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)
			

	)t1
	Join #COL_frailtymembers f on f.EMPI=t1.EMPI;

		
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

	
	

-- Numerator******************

	drop table if exists #COL_numlist;
	CREATE table #COL_numlist
	(
		EMPI varchar(100)
		
	)
	Insert into #COL_numlist
	select distinct 
		EMPI 
	from
	(
			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FOBT Lab Test'
	)

	Union All
	Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From DIAGNOSIS
	where 
	ROOT_COMPANIES_ID=@rootId and 
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name IN('FOBT Lab Test','FOBT Test Result or Finding')
	)

	Union All
	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
		)
		
	
	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
		
	Union all

	-- Colonoscopy

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
		
	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')

	Union All
	
	-- CT Colonograpgy

			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt4 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography'
	)
	Union All

	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt4 and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
		)
	
	   Union all

	   -- FIT

	     			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt2 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FIT DNA Lab Test'
	)
	   Union all
	      select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt2,@ce_enddt,'FIT DNA Test Result or Finding')
		
	Union All
	
	   	Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code
		FROM LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt2 and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
			)

	)t1

	update #COLdataset set num=1 from #COLdataset ds
	join #COL_numlist num on ds.EMPI=num.EMPI ;
	


	-- Numerator Details*********

	drop table if exists #COL_numdetails;
	CREATE table #COL_numdetails
	(
		EMPI varchar(100),
		Code varchar(20),
		ServiceDate Date
			
	)
	Insert into #COL_numdetails
	select
		EMPI
		,Code
		,ServiceDate
	From
	(
		select 
			EMPI 
			,Code
			,ServiceDate
			,row_number() over(partition by EMPI order by ServiceDate Desc) as rn
		from
		(

				Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FOBT Lab Test'
	)

	Union All
		Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From DIAGNOSIS
	where 
	ROOT_COMPANIES_ID=@rootId and 
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name IN('FOBT Lab Test','FOBT Test Result or Finding')
	)

	Union All
	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
		)
		
	
	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
		
	Union all

	-- Colonoscopy

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
		
	Union All
	
	-- CT Colonograpgy

			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt4 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography'
	)
	Union All

	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt4 and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
		)
	
	   Union all

	   -- FIT

	   			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt2 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FIT DNA Lab Test'
	)
	   Union all
	   select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt2,@ce_enddt,'FIT DNA Test Result or Finding')
		
	Union All
	
	   	Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code
		FROM LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt2 and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
			)

	
		)t1
	)t2
	Where rn=1

		
	
	
-- Optional Exclussion

	-- Colorectal Cancer Value Set
	drop table if exists #COL_ColorectalCancer;
	CREATE table #COL_ColorectalCancer
	(
		EMPI varchar(100)
		
	)
	insert into #COL_ColorectalCancer
	Select distinct 
		EMPI 
	from
	(
		
		Select
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Colorectal Cancer')


		Union all
		Select
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Colorectal Cancer')


		Union all
		
		Select
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Total Colectomy')

		Union all
		Select
			EMPI
		From GetICDPCS(@rootId,'1900-01-01',@ce_enddt,'Total Colectomy')

		Union all
		select
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Total Colectomy')
		Union all
	
		select
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'History of Total Colectomy')
		

)t1



update #COLdataset set Excl=1 from #COLdataset ds join #COL_ColorectalCancer cc on ds.empi= cc.empi



	


--SES Startification

update #COLdataset set lis=1 from #COLdataset ds join MCFIELDS m on m.EMPI=ds.EMPI and m.ROOT_COMPANIES_ID=@rootId
where m.RunDate between @ce_startdt1 and @ce_enddt and m.Premium_LIS_Amount>0 and ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

update #COLdataset set lis=0
update #COLdataset 
	set lis=1 
from #COLdataset ds 
join MCFIELDS l on 
	l.EMPI=ds.EMPI and 
	l.ROOT_COMPANIES_ID=@rootId
where 
	ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and 
	(
		Low_Income_Period_Start_Date<=@ce_enddt and Low_Income_Period_End_Date>=@ce_startdt1
		or
		Low_Income_Period_Start_Date>=@ce_startdt1 and Low_Income_Period_End_Date is NULL
	)

-- Item 48
drop table if exists #COL_orec;
CREATE table #COL_orec
(
	EMPI varchar(100),
	orec varchar(2)
			
)
Insert into #COL_orec
Select
	EMPI
	,OREC
From
(
	select 
		EMPI
		,OREC
		,row_number() over (Partition by EMPI order by RunDate Desc,PayDate Desc) as rn
	From MCFIELDS
	Where
		ROOT_COMPANIES_ID=@rootId and
		RunDate between @ce_startdt1 and @ce_enddt and
		OREC is not null
)t1
Where
	rn=1


update #COLdataset set orec=o.orec from #COLdataset ds
join #COL_orec o on o.EMPI=ds.EMPI
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

update #COLdataset set meas='COLNON' WHERE orec=0 AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');


UPDATE #COLdataset SET meas='COLLISDE' WHERE orec=0 AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

UPDATE #COLdataset SET meas='COLDIS' WHERE orec IN(1,3) AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

UPDATE #COLdataset SET meas='COLCMB' WHERE orec IN(1,3) AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');


UPDATE #COLdataset SET meas='COLOT' WHERE orec IN(2,9) AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,0 as EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0 and payer!='MCD' THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,gender,@meas,@meas_year,@reportId,@rootId FROM #COLdataset 

GO
