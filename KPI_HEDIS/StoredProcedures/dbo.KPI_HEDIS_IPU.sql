SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON










CREATE PROCEDURE [dbo].[KPI_HEDIS_IPU] 
AS
-- Declare Variables


declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='IPU-A'




DECLARE @ce_startdt Date
DECLARE @ce_startdt1 Date
DECLARE @ce_enddt Date
DECLARE @ce_enddt1 Date
DECLARE @meas VARCHAR(10);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;
 

SET @meas='IPU';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');

/*
Set @reporttype='Network'
Set @measurename='ED Visits/1,000'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=310
Set @domain='Utilization Management'
Set @subdomain='ED Utilization'
Set @measuretype='UHN'
Set @measure_id='36'
*/


-- hospice Exclusion

drop table if exists #ipu_hospicemembers;
CREATE table #ipu_hospicemembers
(
	EMPI varchar(100)
		
);
Insert into #ipu_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	


-- Step 1 Identify all acute inpatient discharges on or between January 1 and December 31 of the measurement year. To identify acute inpatient discharges:
-- Step 1.1 Identifying Inpatient Stays
Drop table if exists #ipu_inpatientstays
Create Table #ipu_inpatientstays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #ipu_inpatientstays
Select
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,@ce_startdt,@ce_enddt)
Where
	CL_DATA_SRC not in(Select data_Source from Data_Source where Supplemental=1) and
	ISNULL(SV_STAT,'O') in('P','A','O','')


-- 1.2 Identify Non acute stays
Drop table if exists #ipu_nonacutestays
Create Table #ipu_nonacutestays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #ipu_nonacutestays
Select
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt,@ce_enddt)
Where
	CL_DATA_SRC not in(Select data_Source from Data_Source where Supplemental=1) and
	ISNULL(SV_STAT,'O') in('P','A','O','')


-- 1.3 Identify Acute Stays
Drop table if exists #ipu_acutestays
Create Table #ipu_acutestays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #ipu_acutestays
Select distinct
	ip.*
From #ipu_inpatientstays ip
left outer join #ipu_nonacutestays na on
	ip.EMPI=na.EMPI and
	ip.CLAIM_ID=na.CLAIM_ID and
	ip.CL_DATA_SRC=na.CL_DATA_SRC
Where
	na.EMPI is null



-- STep 2.1 Exclude discharges with a principal diagnosis of mental health or chemical dependency (Mental and Behavioral Disorders Value Set) on the discharge claim.
Drop table if exists #ipu_behavioraldisorders
Create Table #ipu_behavioraldisorders
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ipu_behavioraldisorders
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Mental and Behavioral Disorders')
Where
	DIAG_SEQ_NO=1 and
	DIAG_DATA_SRC not in(Select data_Source from Data_Source where Supplemental=1) 

-- STep 2.2 Exclude newborn care rendered from birth to discharge home from delivery (only include care rendered during subsequent rehospitalizations after the delivery discharge). Identify newborn care by a principal diagnosis of live-born infant (Deliveries Infant Record Value Set). 
Drop table if exists #ipu_infantdeliveries
Create Table #ipu_infantdeliveries
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ipu_infantdeliveries
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Deliveries Infant Record')
Where
	DIAG_SEQ_NO=1 and
	DIAG_DATA_SRC not in(Select data_Source from Data_Source where Supplemental=1) 




-- Step 2 exclude stays with behavioral disorders and infant deliveries
Drop table if exists #ipu_acutestayswithexclusion
Create Table #ipu_acutestayswithexclusion
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50),
	LOS INT default 0
)
Insert into #ipu_acutestayswithexclusion
Select
	a.*
	,Case
		when DATEDIFF(Day,ADM_DATE,DIS_DATE)>0 then DATEDIFF(Day,ADM_DATE,DIS_DATE)
		When DATEDIFF(Day,ADM_DATE,DIS_DATE)=0 then 1
	end as LOS
From #ipu_acutestays a
Left outer join #ipu_behavioraldisorders b on
	a.EMPI=b.EMPI and
	a.CLAIM_ID=b.CLAIM_ID
Left outer join #ipu_infantdeliveries i on
	a.EMPI=i.EMPI and
	a.CLAIM_ID=i.CLAIM_ID
left outer join #ipu_hospicemembers h on
	a.EMPI=h.EMPI
Where
	b.EMPI is null and
	i.EMPI is null and
	h.EMPI is null


-- Step 3 Report total inpatient, using all discharges identified after completing steps 1 and 2
-- 3.1 Identifying Payer on the day of discharge
drop table if exists #ipu_tmpsubscriber;
CREATE TABLE #ipu_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
Insert into #ipu_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,a.DIS_DATE), Date_of_Birth) > a.DIS_DATE THEN datediff(year, Date_of_Birth, a.DIS_DATE) - 1
		ELSE datediff(year, Date_of_Birth, a.DIS_DATE)
	END as Age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
Join #ipu_acutestayswithexclusion a on
	en.EMPI=a.EMPI
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt 
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;
	

--3.2 Create dataset as per hedis format
drop table if exists #ipudataset;
CREATE TABLE #ipudataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [proc] smallint DEFAULT '1',
  [MM] smallint DEFAULT '0',
  [LOS] smallint DEFAULT '0',
  
) ;
insert into #ipudataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'IPUT'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					t.EMPI
					,s.LOS
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by t.EMPI order by StartDate desc,EndDate Desc) as rn 
				from #ipu_tmpsubscriber t
				join #ipu_acutestayswithexclusion s on
					t.EMPI=s.EMPI and
					s.DIS_DATE between t.StartDate and t.EndDate

				where  
					StartDate<=@ce_enddt
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='IPU'
Order by 1




-- 3.3 Updating LOS for Total Stays
update ds Set 
	ds.LOS=t1.LOS
from #ipudataset ds
Join
(
	select
		EMPI
		,SUM(LOS) as LOS
	From #ipu_acutestayswithexclusion
	Group by 
		EMPI
)t1 on 
	ds.EMPI=t1.EMPI and
	ds.meas='IPUT'



-- Step 4 Identify Maternity stays
-- Step 4.1
Drop table if exists #ipu_maternitystays
Create table #ipu_maternitystays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #ipu_maternitystays
Select distinct
	*
From
(
	Select
		EMPI
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Maternity Diagnosis')
	where
		DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and 
		DIAG_SEQ_NO=1

	Union all

	Select
		EMPI
		,CLAIM_ID
		,CL_DATA_SRC
	From CLAIMLINE 
	Where
		ROOT_COMPANIES_ID=@rootId and
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(POS,'0')!='81' and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt and @ce_enddt and
		(
			REV_CODE in 
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Maternity')
			) 
			or 
			RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Maternity')
			)
		)
)t1




-- 4.2 join with total acute stays
Drop table if exists #ipu_ipumat
Create table #ipu_ipumat
(
	EMPI varchar(100),
	LOS INT
)
Insert into #ipu_ipumat
select
	a.EMPI
	,SUM(a.LOS) as LOS
From #ipu_acutestayswithexclusion a
Join #ipu_maternitystays m on
	a.EMPI=m.EMPI and
	a.CLAIM_ID=m.CLAIM_ID and
	a.CL_DATA_SRC=m.DATA_SRC
Group by
	a.EMPI


--4.4 Insertng data in ipudataset
Insert into #ipudataset(EMPI,meas,Payer,Age,Gender,LOS)
Select 
	ds.EMPI
	,'IPUMAT'
	,ds.Payer
	,ds.Age
	,ds.Gender
	,m.LOS
From #ipudataset ds
join #ipu_ipumat m on
	ds.EMPI=m.EMPI


-- Step 5.1 - Idnetify Surgery Stays
Drop table if exists #ipu_surgeries
Create table #ipu_surgeries
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #ipu_surgeries
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From GetRecordsByRevenueCodes(@rootId,@ce_startdt,@ce_enddt,'Surgery')
where 
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) 
	

--5.2 join with total acute stays
Drop table if exists #ipu_ipus
Create table #ipu_ipus
(
	EMPI varchar(100),
	LOS INT
)
Insert into #ipu_ipus
select
	a.EMPI
	,SUM(a.LOS) as LOS
From #ipu_acutestayswithexclusion a
Join #ipu_surgeries s on
	a.EMPI=s.EMPI and
	a.CLAIM_ID=s.CLAIM_ID and
	a.CL_DATA_SRC=s.CL_DATA_SRC
Left outer Join #ipu_maternitystays m on
	a.EMPI=m.EMPI and
	a.CLAIM_ID=m.CLAIM_ID and
	a.CL_DATA_SRC=m.DATA_SRC
Where
	m.EMPI is null
Group by
	a.EMPI

-- 5.3 inserting data in ipudataset
Insert into #ipudataset(EMPI,meas,Payer,Age,Gender,LOS)
Select 
	ds.EMPI
	,'IPUS'
	,ds.Payer
	,ds.Age
	,ds.Gender
	,m.LOS
From #ipudataset ds
join #ipu_ipus m on
	ds.EMPI=m.EMPI



-- Step 6.1 Report medicine. Categorize as medicine the discharges remaining after removing maternity (identified in step 4) and surgery (identified in step 5) from total inpatient (identified in step 3).

Drop table if exists #ipu_ipum
Create table #ipu_ipum
(
	EMPI varchar(100),
	LOS INT
)
Insert into #ipu_ipum
select
	a.EMPI
	,SUM(a.LOS) as LOS
From #ipu_acutestayswithexclusion a
left outer Join #ipu_surgeries s on
	a.EMPI=s.EMPI and
	a.CLAIM_ID=s.CLAIM_ID and
	a.CL_DATA_SRC=s.CL_DATA_SRC
Left outer Join #ipu_maternitystays m on
	a.EMPI=m.EMPI and
	a.CLAIM_ID=m.CLAIM_ID and
	a.CL_DATA_SRC=m.DATA_SRC
Where
	m.EMPI is null and
	s.EMPI is null
Group by
	a.EMPI


-- 6.2 Inserting data into ipudataset
Insert into #ipudataset(EMPI,meas,Payer,Age,Gender,LOS)
Select 
	ds.EMPI
	,'IPUM'
	,ds.Payer
	,ds.Age
	,ds.Gender
	,m.LOS
From #ipudataset ds
join #ipu_ipum m on
	ds.EMPI=m.EMPI




-- Generate Output
-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(Memid,Meas,payer,[Proc],LOS,MM,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,[Proc],LOS,MM, age,gender,@meas,@meas_year,@reportId,@rootId FROM #ipudataset

	

	/*

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	

Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Encounters,outlier,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,DateofService,Code)
Select 
	
	a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,d.payer as ProductType
	,d.encounters
	,d.Outlier
	,d.Epop
	,@reportId
	,@reporttype
	,@quarter
	,@startDate
	,@enddate
	,@rootId
	,d.EMPI
	,@measuretype
	,nd.FROM_DATE
	,nd.Code
From #edudataset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Left outer join #edu_numdetails nd on
	d.EMPI=nd.EMPI


-- Calculate total population count in a period

select @popcount=count(*) from(
select EMPI from MEMBER_MONTH where MEMBER_MONTH_START_DATE between @startDate and @enddate group by  EMPI
)t1




		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;


	Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,Encounters,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
	Select
		*
		,Case
			when Result > @target then FLOOR(cast((Encounters*Target) as decimal)/Result - Encounters)
			Else 0
		end as To_Target
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,Measure_Title
			,MEASURE_SUBTITLE
			,Measure_Type
			,Sum(Encounters) as Encounters
			,Sum(Encounters) as Gaps
			,(Cast(Sum(Encounters) as float)/@popcount)*1000 as Result
			,@target as Target
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From
		(
			Select distinct
				EMPI
				,Measure_id
				,Measure_Name
				,@domain as Measure_Title
				,@subdomain as MEASURE_SUBTITLE
				,@measuretype as Measure_Type
				,Encounters
				,outlier
				,Report_Id
				,ReportType
				,Report_Quarter
				,Period_Start_Date
				,Period_End_Date
				,Root_Companies_Id
			From RPT.MEASURE_DETAILED_LINE
			where 
				Enrollment_Status='Active'  and
				MEASURE_ID=@measure_id and
				REPORT_ID=@reportId
		)t1
		Group by Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type
	)t2




*/

GO
