SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON










CREATE PROCEDURE [dbo].[KPI_HEDIS_MPT] 
AS
-- Declare Variables


declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='MPT-B'




DECLARE @ce_startdt Date
DECLARE @ce_startdt1 Date
DECLARE @ce_enddt Date
DECLARE @ce_enddt1 Date
DECLARE @meas VARCHAR(10);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;
 

SET @meas='MPT';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');

/*
Set @reporttype='Network'
Set @measurename='ED Visits/1,000'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=310
Set @domain='Utilization Management'
Set @subdomain='ED Utilization'
Set @measuretype='UHN'
Set @measure_id='36'
*/


-- hospice Exclusion
drop table if exists #mpt_hospicemembers;
CREATE table #mpt_hospicemembers
(
	EMPI varchar(100)
		
);
Insert into #mpt_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	


-- Identify all inpatients stays during the measure period
Drop table if exists #mpt_inpatientstays
Create Table #mpt_inpatientstays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_inpatientstays
Select
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,@ce_startdt,@ce_enddt)
Where
	CL_DATA_SRC not in(Select data_Source from Data_Source where Supplemental=1) and
	ISNULL(SV_STAT,'O')!='D'
	

--Identify Partial Hospitalization POS visits
Drop table if exists #mpt_partialhospPOS;
Create table #mpt_partialhospPOS
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_partialhospPOS
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE	
Where
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt and @ce_enddt and
	POS in
	(
		Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Partial Hospitalization POS'
	)


--Identify Community Mental health POS visits
Drop table if exists #mpt_communitymentalhealthPOS;
Create table #mpt_communitymentalhealthPOS
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_communitymentalhealthPOS
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE	
Where
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt and @ce_enddt and
	POS in
	(
		Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Community Mental Health Center POS'
	)


--Identify Tele health POS visits
Drop table if exists #mpt_telehealthPOS;
Create table #mpt_telehealthPOS
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_telehealthPOS
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE	
Where
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt and @ce_enddt and
	POS in
	(
		Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Telehealth POS'
	)


--Identify Outpatient POS visits
Drop table if exists #mpt_outpatientPOS;
Create table #mpt_outpatientPOS
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_outpatientPOS
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE	
Where
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt and @ce_enddt and
	POS in
	(
		Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Outpatient POS'
	)


--Identify Ambulatory Surgical Center POS
Drop table if exists #mpt_ambsurgicalPOS;
Create table #mpt_ambsurgicalPOS
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_ambsurgicalPOS
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE	
Where
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt and @ce_enddt and
	POS in
	(
		Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Ambulatory Surgical Center POS'
	)


--Identify ED POS
Drop table if exists #mpt_EDPOS;
Create table #mpt_EDPOS
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_EDPOS
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE	
Where
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt and @ce_enddt and
	POS in
	(
		Select code from HDS.VALUESET_TO_CODE where Value_Set_Name='ED POS'
	)



-- Identify visits with Principal mental Health diagnosis
Drop table if exists #mpt_mentalhealthdiagnosis;
Create Table #mpt_mentalhealthdiagnosis
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #mpt_mentalhealthdiagnosis
Select
	EMPI
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Mental Health Diagnosis')
Where
	DIAG_DATA_SRC not in(Select Data_Source from DATA_SOURCE where Supplemental=1) and
	DIAG_SEQ_NO=1



-- MPTINP
Drop table if exists #mpt_mptinp;
Create Table #mpt_mptinp
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE DATE,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #mpt_mptinp
Select
	i.EMPI
	,i.ADM_DATE
	,i.DIS_DATE
	,i.CLAIM_ID
	,i.CL_DATA_SRC
From #mpt_inpatientstays i
Join #mpt_mentalhealthdiagnosis d on
	i.EMPI=d.EMPI and
	i.CLAIM_ID=d.CLAIM_ID and
	i.CL_DATA_SRC=d.DATA_SRC
Left outer join #mpt_hospicemembers h on
	i.EMPI=h.EMPI
Where
	h.EMPI is null


--Identify Intensive outpatient and partial hospitalization
Drop table if exists #mpt_partialhospitalizations
Create Table #mpt_partialhospitalizations
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #mpt_partialhospitalizations
Select distinct 
	t1.*
From 
(
	-- 1.1 ?	Partial Hospitalization or Intensive Outpatient Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t1.EMPI
		,PROC_START_DATE
		,t1.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedureswithOutMods(@rootId,@ce_startdt,@ce_enddt,'Partial Hospitalization or Intensive Outpatient','Telehealth Modifier') t1
	Where
		ISNULL(SV_STAT,'O')!='D'

	
	Union all

	-- 1.2 Partial Hospitalization or Intensive Outpatient Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).
	Select
		t1.EMPI
		,DIAG_START_DATE
		,t1.CLAIM_ID
		,DIAG_DATA_SRC as DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Partial Hospitalization or Intensive Outpatient') t1
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	---- 1.3 Partial Hospitalization or Intensive Outpatient Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t1.EMPI
		,FROM_DATE
		,t1.CLAIM_ID
		,t1.CL_DATA_SRC as DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt,@ce_enddt,'Partial Hospitalization or Intensive Outpatient') t1
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	-- 2.1 ?	(MPT IOP/PH Group 1 Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Partial Hospitalization POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t1.EMPI
		,PROC_START_DATE
		,t1.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedureswithOutMods(@rootId,@ce_startdt,@ce_enddt,'MPT IOP/PH Group 1,Electroconvulsive Therapy,Transcranial Magnetic Stimulation','Telehealth Modifier') t1
	Join #mpt_partialhospPOS p on
		t1.EMPI=p.EMPI and
		t1.CLAIM_ID=p.CLAIM_ID and
		t1.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'
	

	Union all

	---- 2.2 ?	(MPT IOP/PH Group 1 Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Partial Hospitalization POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_partialhospPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	-- 2.3 ?	(MPT IOP/PH Group 1 Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Partial Hospitalization POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t1.EMPI
		,DIAG_START_DATE
		,t1.CLAIM_ID
		,DIAG_DATA_SRC as DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t1
	Join #mpt_partialhospPOS p on
		t1.EMPI=p.EMPI and
		t1.CLAIM_ID=p.CLAIM_ID and
		t1.DIAG_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	-- 3.1 ?	(MPT IOP/PH Group 1 Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)

	Select
		t1.EMPI
		,PROC_START_DATE
		,t1.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedureswithOutMods(@rootId,@ce_startdt,@ce_enddt,'MPT IOP/PH Group 1,Electroconvulsive Therapy,Transcranial Magnetic Stimulation','Telehealth Modifier') t1
	Join #mpt_communitymentalhealthPOS p on
		t1.EMPI=p.EMPI and
		t1.CLAIM_ID=p.CLAIM_ID and
		t1.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	-- 3.2 ?	(MPT IOP/PH Group 1 Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_communitymentalhealthPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	-- 3.3 ?	(MPT IOP/PH Group 1 Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)

	Select
		t1.EMPI
		,DIAG_START_DATE
		,t1.CLAIM_ID
		,DIAG_DATA_SRC as DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t1
	Join #mpt_communitymentalhealthPOS p on
		t1.EMPI=p.EMPI and
		t1.CLAIM_ID=p.CLAIM_ID and
		t1.DIAG_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all
	
	--4 ?	MPT IOP/PH Group 2 Value Set with Partial Hospitalization POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider

	Select
		t1.EMPI
		,PROC_START_DATE
		,t1.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'MPT IOP/PH Group 2') t1
	Join #mpt_partialhospPOS p on
		t1.EMPI=p.EMPI and
		t1.CLAIM_ID=p.CLAIM_ID and
		t1.PROC_DATA_SRC=p.CL_DATA_SRC
	Join PROVIDER_FLAGS pf on
		ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--5 ?	MPT IOP/PH Group 2 Value Set with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set), 

	Select
		t1.EMPI
		,PROC_START_DATE
		,t1.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'MPT IOP/PH Group 2') t1
	Join #mpt_communitymentalhealthPOS p on
		t1.EMPI=p.EMPI and
		t1.CLAIM_ID=p.CLAIM_ID and
		t1.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'
	
	 
)t1
Left outer join #mpt_telehealthPOS p on
	t1.EMPI=p.EMPI and
	t1.CLAIM_ID=p.CLAIM_ID and
	t1.DATA_SRC=p.CL_DATA_SRC
Where
	DATA_SRC not in(Select data_source from Data_Source where supplemental=1) and
	p.EMPI is null





--Identify Outpatient Categories
Drop table if exists #mpt_outpatientvisits
Create Table #mpt_outpatientvisits
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #mpt_outpatientvisits
Select distinct
	t1.*
From
(
	-- 1.1 ?	MPT Stand Alone Outpatient Group 1 Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'MPT Stand Alone Outpatient Group 1','Telehealth Modifier') t
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	-- 1.2 ?	MPT Stand Alone Outpatient Group 1 Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t1.EMPI
		,FROM_DATE
		,t1.CLAIM_ID
		,t1.CL_DATA_SRC as DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt,@ce_enddt,'MPT Stand Alone Outpatient Group 1') t1
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--2.1 ?	MPT Stand Alone Outpatient Group 2 Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider.

	
	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'MPT Stand Alone Outpatient Group 2','Telehealth Modifier') t
	Join PROVIDER_FLAGS pf on
		t.ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--2.2 ?	MPT Stand Alone Outpatient Group 2 Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider.

	Select
		t1.EMPI
		,FROM_DATE
		,t1.CLAIM_ID
		,t1.CL_DATA_SRC as DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt,@ce_enddt,'MPT Stand Alone Outpatient Group 2') t1
	Join PROVIDER_FLAGS pf on
		t1.ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--3?	Observation Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider. 

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Observation','Telehealth Modifier') t
	Join PROVIDER_FLAGS pf on
		t.ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Left Outer Join #mpt_mptinp i on
		t.EMPI=i.EMPI and
		t.CLAIM_ID=i.CLAIM_ID and
		t.PROC_DATA_SRC=i.CL_DATA_SRC
	Left Outer Join #mpt_mptinp i2 on
		t.EMPI=i2.EMPI and
		t.PROC_START_DATE between Dateadd(day,-1,i2.ADM_DATE) and i2.DIS_DATE
	Where
		ISNULL(SV_STAT,'O')!='D' and
		i.EMPI is null and
		i2.EMPI is null

		
	
		

	Union all

	--4.1 ?	(Visit Setting Unspecified Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Outpatient POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Transcranial Magnetic Stimulation,Visit Setting Unspecified,Electroconvulsive Therapy,Transcranial Magnetic Stimulation') t
	Join #mpt_outpatientPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--4.2 ?	(Visit Setting Unspecified Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Outpatient POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_outpatientPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	--4.3 ?	(Visit Setting Unspecified Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Outpatient POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,DIAG_START_DATE
		,t.CLAIM_ID
		,DIAG_DATA_SRC as Data_Src
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_outpatientPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.DIAG_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	-- 5.1 ?	(Visit Setting Unspecified Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_communitymentalhealthPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	

	Union all

	-- 5.2 ?	(Visit Setting Unspecified Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)


	Select
		t.EMPI
		,DIAG_START_DATE
		,t.CLAIM_ID
		,DIAG_DATA_SRC as Data_Src
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_communitymentalhealthPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.DIAG_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	-- 5.3 ?	(Visit Setting Unspecified Value Set; Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Transcranial Magnetic Stimulation,Visit Setting Unspecified,Electroconvulsive Therapy') t
	Join #mpt_communitymentalhealthPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	--6.1 ?	(Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with (Ambulatory Surgical Center POS Value Set) with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,DIAG_START_DATE
		,t.CLAIM_ID
		,DIAG_DATA_SRC as Data_Src
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_ambsurgicalPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.DIAG_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--6.2 ?	(Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with (Ambulatory Surgical Center POS Value Set) with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy') t
	Join #mpt_ambsurgicalPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--6.3 ?	(Electroconvulsive Therapy Value Set; Transcranial Magnetic Stimulation Value Set) with (Ambulatory Surgical Center POS Value Set) with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Electroconvulsive Therapy,Transcranial Magnetic Stimulation') t
	Join #mpt_ambsurgicalPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


)t1
Left outer join #mpt_telehealthPOS p on
	t1.EMPI=p.EMPI and
	t1.CLAIM_ID=p.CLAIM_ID and
	t1.Data_Src=p.CL_DATA_SRC
Where
	p.EMPI is null and
	DATA_SRC not in(Select data_source from Data_Source where supplemental=1)




--Identify ED Categories
Drop table if exists #mpt_EDvisits
Create Table #mpt_EDvisits
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #mpt_EDvisits
Select
	t1.*
From
(
	-- 1.1 ?	ED Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider.
	Select
		t.EMPI
		,coalesce(DIS_DATE,PROC_START_DATE) as PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'ED','Telehealth Modifier') t
	Join PROVIDER_FLAGS pf on
		t.ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	-- 1.2 ?	ED Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider.

	Select
		t1.EMPI
		,coalesce(DIS_DATE,FROM_DATE)
		,t1.CLAIM_ID
		,t1.CL_DATA_SRC as DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt,@ce_enddt,'ED') t1
	Join PROVIDER_FLAGS pf on
		t1.ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		ISNULL(SV_STAT,'O')!='D'


	Union all

	-- 1.3 ?	ED Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set) billed by a mental health provider.

	Select
		t1.EMPI
		,DIAG_START_DATE
		,t1.CLAIM_ID
		,t1.DIAG_DATA_SRC as DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'ED') t1
	Join PROVIDER_FLAGS pf on
		t1.ATT_NPI=pf.ProvId and
		pf.MentalHealthProv='Y' and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--2. ?	Visit Setting Unspecified Value Set with ED POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,coalesce(DIS_DATE,PROC_START_DATE) as PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Visit Setting Unspecified') t
	Join #mpt_EDPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--3. ?	Visit Setting Unspecified Value Set with Community Mental Health Center POS Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set)

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC as Data_Src
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Visit Setting Unspecified') t
	Join #mpt_communitymentalhealthPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'


)t1
Left Outer Join #mpt_telehealthPOS p on
	t1.EMPI=p.EMPI and
	t1.CLAIM_ID=p.CLAIM_ID and
	t1.Data_Src=p.CL_DATA_SRC
Left Outer Join #mpt_mptinp i on
		t1.EMPI=i.EMPI and
		t1.CLAIM_ID=i.CLAIM_ID and
		t1.Data_Src=i.CL_DATA_SRC
Left Outer Join #mpt_mptinp i2 on
		t1.EMPI=i2.EMPI and
		t1.PROC_START_DATE between Dateadd(day,-1,i2.ADM_DATE) and i2.DIS_DATE
Where
	p.EMPI is null and
	i.EMPI is null and
	i2.EMPI is null and
	DATA_SRC not in(Select data_source from Data_Source where supplemental=1)


--Identify TeleHealth Categories
Drop table if exists #mpt_telehealthvisits
Create Table #mpt_telehealthvisits
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #mpt_telehealthvisits
Select
	t1.*
From
(

	-- 1.1?	Visit Setting Unspecified Value Set with (Telehealth Modifier Value Set; Telehealth POS Value Set) with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC
	From GetProceduresWithMods(@rootId,@ce_startdt,@ce_enddt,'Visit Setting Unspecified','Telehealth Modifier') t
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	-- 1.2?	Visit Setting Unspecified Value Set with (Telehealth Modifier Value Set; Telehealth POS Value Set) with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		t.EMPI
		,PROC_START_DATE
		,t.CLAIM_ID
		,PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Visit Setting Unspecified') t
	Join #mpt_telehealthPOS p on
		t.EMPI=p.EMPI and
		t.CLAIM_ID=p.CLAIM_ID and
		t.PROC_DATA_SRC=p.CL_DATA_SRC
	Where
		ISNULL(SV_STAT,'O')!='D'
		
	Union all

	-- 2?	Telephone Visits Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).
	--3.1 ?	Online Assessments Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Telephone Visits,Online Assessments') 
	Where
		ISNULL(SV_STAT,'O')!='D'

	Union all

	--3.2 ?	Online Assessments Value Set with a principal mental health diagnosis (Mental Health Diagnosis Value Set).

	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Telephone Visits') 
	Where
		ISNULL(SV_STAT,'O')!='D'
	
)t1
Where
	PROC_DATA_SRC not in(Select Data_Source from DATA_SOURCE where Supplemental=1)







-- Consolidate all categoories in one table
Drop table if exists #mpt_consolidated
Create table #mpt_consolidated
(
	EMPI varchar(100),
	ServiceDate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50),
	Meas varchar(10)
)
Insert into #mpt_consolidated
Select
	EMPI
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
	,meas
From
(
	Select 
		*
		,RANK() over(Partition by EMPI,meas order by DIS_DATE Asc) as rn
	From
	(
		Select
			EMPI
			,DIS_DATE
			,CLAIM_ID
			,CL_DATA_SRC
			,'MPTINP' as meas
		From #mpt_mptinp

		Union all

		Select
			p.*
			,'MPTIOP' as meas
		From #mpt_partialhospitalizations p
		Join #mpt_mentalhealthdiagnosis d on
			p.EMPI=d.EMPI and
			p.CLAIM_ID=d.CLAIM_ID and
			p.DATA_SRC=d.DATA_SRC
		Left outer join #mpt_hospicemembers h on
			p.EMPI=h.EMPI
		Where
			h.EMPI is null


		Union all

		Select
			p.*
			,'MPTOUT' as meas
		From #mpt_outpatientvisits p
		Join #mpt_mentalhealthdiagnosis d on
			p.EMPI=d.EMPI and
			p.CLAIM_ID=d.CLAIM_ID and
			p.DATA_SRC=d.DATA_SRC
		Left outer join #mpt_hospicemembers h on
			p.EMPI=h.EMPI
		Where
			h.EMPI is null 


		Union all

		Select
			p.*
			,'MPTED' as meas
		From #mpt_EDvisits p
		Join #mpt_mentalhealthdiagnosis d on
			p.EMPI=d.EMPI and
			p.CLAIM_ID=d.CLAIM_ID and
			p.DATA_SRC=d.DATA_SRC
		Left outer join #mpt_hospicemembers h on
			p.EMPI=h.EMPI
		Where
			h.EMPI is null

		Union all

		Select
			p.*
			,'MPTTEL' as meas
		From #mpt_telehealthvisits p
		Join #mpt_mentalhealthdiagnosis d on
			p.EMPI=d.EMPI and
			p.CLAIM_ID=d.CLAIM_ID and
			p.DATA_SRC=d.DATA_SRC
		Left outer join #mpt_hospicemembers h on
			p.EMPI=h.EMPI
		Where
			h.EMPI is null
	)t1
)t2
Where
	rn=1




-- Identify Member Product Line
drop table if exists #mpt_tmpsubscriber;
CREATE TABLE #mpt_tmpsubscriber 
(
    EMPI varchar(100),
	meas varchar(20),
    dob Date,
	age INT,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date,
	ServiceDate date,
	rn INT

);
insert into #mpt_tmpsubscriber
SELECT distinct
	en.EMPI
	,d.Meas
	,gm.Date_of_Birth
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,d.ServiceDate), Date_of_Birth) > d.ServiceDate THEN datediff(year, Date_of_Birth, d.ServiceDate) - 1
		ELSE datediff(year, Date_of_Birth, d.ServiceDate)
	END as Age
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE
	,d.ServiceDate
	,rank() over(partition by en.EMPI,meas order by Case when d.ServiceDate between en.EFF_DATE and en.TERM_DATE Then 1 else 0 end  desc,TERM_DATE Desc) as rn
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
Join #mpt_consolidated d on
	gm.EMPI_ID=d.EMPI
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	1 in(en.MENTAL_HEALTH_BENEFIT_IP,en.MENTAL_HEALTH_BENEFIT_INTENSIVE_OP,en.MENTAL_HEALTH_BENEFIT_OP_ED) and
	en.EFF_DATE<=@ce_enddt and
	en.TERM_DATE>=@ce_startdt
	--and en.EMPI=144061
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;


-- Create a table with payer mapping which will be used to generate member months
Drop table if exists #mpt_payermapping
Create Table #mpt_payermapping
(
	EMPI varchar(100),
	Payer varchar(50),
	Gender varchar(10),
	dob Date,
	StartDate Date,
	EndDate Date
	
)
Insert into #mpt_payermapping
Select distinct
	EMPI
	,pm.PayerMapping
	,gender
	,dob
	,StartDate
	,Enddate
From
(
	Select distinct
		EMPI
		,gender
		,dob
		,StartDate
		,EndDate
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB')) then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI,rn order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI,rn order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,dob
				,Startdate
				,EndDate
				,rn
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,payer
					,gender
					,dob
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				From
				(
					Select distinct
						e.EMPI
						,e.EFF_DATE as StartDate
						,e.TERM_DATE as EndDate
						,TRIM(PAYER_TYPE) as Payer
						,o.Gender
						,o.Date_of_Birth as dob
					from enrollment e
					join open_empi_master o on
						e.EMPI=o.EMPI_ID and
						e.ROOT_COMPANIES_ID=o.ROOT_COMPANIES_ID
					left outer join #mpt_hospicemembers h on
						e.EMPI=h.EMPI
					Where
						e.ROOT_COMPANIES_ID=@rootId and
						EFF_DATE<=@ce_enddt and
						TERM_DATE>=@ce_startdt and
						1 in(e.MENTAL_HEALTH_BENEFIT_IP,e.MENTAL_HEALTH_BENEFIT_INTENSIVE_OP,e.MENTAL_HEALTH_BENEFIT_OP_ED) and
						h.EMPI is null
				)t1
	
				--Where EMPI=100005
			)t2
			
		)t3
		
	)t4
)t5
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t5.newpayer=pm.payer and 
	pm.Measure_id='MPT'
Order by 1


-- Calculate Member Months
Drop table if exists #mpt_membermonths
Create table #mpt_membermonths
(
	EMPI varchar(100),
	meas varchar(10),
	Payer varchar(50),
	Age INT,
	Gender varchar(10),
	MM INT
)
Insert into #mpt_membermonths
Select
	EMPI
	,'MPTANY'
	,payer
	,Age
	,Gender
	,count(distinct MEMBER_MONTH_START_DATE)
From
(
	Select
		*
		,CASE 
			WHEN dateadd(year, datediff (year,dob,[MEMBER_MONTH_START_DATE]), dob) > [MEMBER_MONTH_START_DATE] THEN datediff(year, dob, [MEMBER_MONTH_START_DATE]) - 1
			ELSE datediff(year, dob, [MEMBER_MONTH_START_DATE])
		END as Age
	From
	(
		SELECT Distinct
			   CONVERT(DATE,CONVERT(CHAR(6),D.YYYYMM)+'15') AS [MEMBER_MONTH_START_DATE]
			  ,E.EMPI AS [EMPI]
			  ,E.StartDate AS [EFF_DATE]
			  ,COALESCE(E.EndDate,EOMONTH(DATEADD(m,-1,GETDATE()))) AS EndDate
			  ,E.payer
			  ,e.dob
			  ,e.gender
		FROM #mpt_payermapping e
		INNER JOIN KPI_ENGINE.[dbo].[DIM_DATE] D ON 
			D.Date BETWEEN E.StartDate AND 
			CASE 
				When E.EndDate<=@ce_enddt THEN CONVERT(DATE,e.EndDate)
				ELSE 
					CASE 
						When E.EndDate>@ce_enddt THEN @ce_enddt
						ELSE
							CASE 
								WHEN ISNULL(E.EndDate,'')='' THEN EOMONTH(DATEADD(m,-1,GETDATE()))
							END 
					END 
			END
		--Where EMPI=100241
	)t1
	Where
		MEMBER_MONTH_START_DATE between @ce_startdt and @ce_enddt and
		MEMBER_MONTH_START_DATE<=EndDate and
		MEMBER_MONTH_START_DATE>=EFF_DATE
		
)t2
Group by
	EMPI
	,payer
	,Age
	,Gender

	

-- Create dataset per hedis format
drop table if exists #mptdataset
Create table #mptdataset
(
	EMPI varchar(100),
	meas varchar(10),
	Payer varchar(50),
	Gender varchar(20),
	Age INT,
	MM INT default 0,
	[PROC] INT default 0,
	[LOS] INT default 0

)
Insert into #mptdataset(EMPI,meas,Payer,Gender,Age)
Select distinct
	EMPI
	,meas
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,meas
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB')) then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI,meas order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI,meas order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,meas
				,payer
				,gender
				,age
			From
			(
				select 
					t.EMPI
					,t.meas
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by t.EMPI,meas order by StartDate desc,EndDate Desc) as rn 
				from #mpt_tmpsubscriber t
				Where 
					rn=1
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='MPT'
Order by 1


-- Update Proc = 1 for all categories
Update ds Set 
	ds.[proc]=1
From #mptdataset ds



Insert into #mptdataset(EMPI,meas,Payer,Gender,Age,MM)
Select distinct
	EMPI
	,meas
	,Payer
	,gender
	,age
	,MM
from #mpt_membermonths



-- Identify the first service in Any category
Insert into #mptdataset(EMPI,meas,Payer,Gender,Age,[PROC])
Select
	EMPi
	,'MPTANY'
	,Payer
	,Gender
	,Age
	,[PROC]
From
(
	select 
		d.* 
		,RANK() over(partition by d.EMPI order by ServiceDate asc) as rn
	from #mptdataset d
	join #mpt_tmpsubscriber t on
		d.EMPI=t.EMPI and
		d.meas=t.meas
--	where d.EMPI=100016
)t1
Where
	rn=1


-- Combine Member months and MPTANY
drop table if exists #mptdataset1
Create table #mptdataset1
(
	EMPI varchar(100),
	meas varchar(10),
	Payer varchar(50),
	Gender varchar(20),
	Age INT,
	MM INT default 0,
	[PROC] INT default 0,
	[LOS] INT default 0

)
Insert into #mptdataset1(EMPI,meas,Payer,Age,Gender,[PROC],MM)
Select
	EMPI
	,meas
	,Payer
	,Age
	,Gender
	,Case
		when sum([Proc])>=1 then 1
		else 0
	end as [proc]
	,sum(MM) as MM
From
(
	Select
		EMPI
		,meas
		,Payer
		,Age
		,Gender
		,0 as [Proc]
		,MM
	From #mpt_membermonths
	--Where	EMPI=95409

	Union all

	Select
		EMPI
		,meas
		,Payer
		,Age
		,Gender
		,[Proc]
		,0 as MM
	From #mptdataset
	--Where	EMPI=95409

)t1
Group by
	EMPI
	,meas
	,Payer
	,Age
	,Gender




-- Generate Output
-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(Memid,Meas,payer,[Proc],LOS,MM,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,[Proc],LOS,MM, age,gender,@meas,@meas_year,@reportId,@rootId FROM #mptdataset1

	

	/*

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	

Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Encounters,outlier,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,DateofService,Code)
Select 
	
	a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,d.payer as ProductType
	,d.encounters
	,d.Outlier
	,d.Epop
	,@reportId
	,@reporttype
	,@quarter
	,@startDate
	,@enddate
	,@rootId
	,d.EMPI
	,@measuretype
	,nd.FROM_DATE
	,nd.Code
From #edudataset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Left outer join #edu_numdetails nd on
	d.EMPI=nd.EMPI


-- Calculate total population count in a period

select @popcount=count(*) from(
select EMPI from MEMBER_MONTH where MEMBER_MONTH_START_DATE between @startDate and @enddate group by  EMPI
)t1




		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;


	Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,Encounters,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
	Select
		*
		,Case
			when Result > @target then FLOOR(cast((Encounters*Target) as decimal)/Result - Encounters)
			Else 0
		end as To_Target
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,Measure_Title
			,MEASURE_SUBTITLE
			,Measure_Type
			,Sum(Encounters) as Encounters
			,Sum(Encounters) as Gaps
			,(Cast(Sum(Encounters) as float)/@popcount)*1000 as Result
			,@target as Target
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From
		(
			Select distinct
				EMPI
				,Measure_id
				,Measure_Name
				,@domain as Measure_Title
				,@subdomain as MEASURE_SUBTITLE
				,@measuretype as Measure_Type
				,Encounters
				,outlier
				,Report_Id
				,ReportType
				,Report_Quarter
				,Period_Start_Date
				,Period_End_Date
				,Root_Companies_Id
			From RPT.MEASURE_DETAILED_LINE
			where 
				Enrollment_Status='Active'  and
				MEASURE_ID=@measure_id and
				REPORT_ID=@reportId
		)t1
		Group by Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type
	)t2




*/

GO
