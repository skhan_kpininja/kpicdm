SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE  PROCEDURE [KPI_HEDIS_SPD] 
AS
-- Declare Variables

Declare @rundate date='2022-02-01'
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='SPD-A'

DECLARE @ce_startdt Date;
DECLARE @ce_enddt Date;
DECLARE @ce_startdt1 Date;
DECLARE @ce_enddt1 Date;
DECLARE @meas VARCHAR(10);


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='SPD';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');

Set @reporttype='Physician'
--Set @measurename='Hypertension: BP Control <140/90 (18-85); GAP Open/Closed by last Documented B/P for CY'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
-- set @target=82
Set @domain='Clinical Quality / Chronic Condition Management'
--Set @subdomain='Hypertension'
Set @measuretype='UHN'
--Set @measure_id='12'





-- Eligible Patient List
drop table if exists #spd_memlist; 
CREATE TABLE #spd_memlist (
    EMPI varchar(100)
    
);
insert into #spd_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.Root_Companies_ID
WHERE 
	en.Root_Companies_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1 AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 40 AND 75
ORDER BY 1;




-- Create Temp Patient Enrollment
drop table if exists #spd_tmpsubscriber;
CREATE TABLE #spd_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	age INT,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #spd_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.Root_Companies_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1 and 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 40 AND 75 
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;





-- Create temp table to store SPD data
drop table if exists #spddataset;
CREATE TABLE #spddataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
 
) ;
Insert into #spddataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'SPDA'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #spd_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='SPD'
Order by 1


Insert into #spddataset(EMPI,meas,payer,Gender,age)
Select
	EMPI
	,'SPDB'
	,Payer
	,Gender
	,Age
From #spddataset





-- Continuous Enrollment
DROP TABLE IF EXISTS #spd_contenroll1;
CREATE table #spd_contenroll1
(
	EMPI varchar(100)		
)
Insert into #spd_contenroll1
Select
	EMPI
From GetContinuousEnrolledEMPIwithPharmacy(@rootId,@ce_startdt,@ce_enddt,45,0)


DROP TABLE IF EXISTS #spd_contenroll2;
CREATE table #spd_contenroll2
(
	EMPI varchar(100)		
)
Insert into #spd_contenroll2
Select
	EMPI
From GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)

update #spddataset 
	set CE=1 
from #spddataset ds 
join #spd_contenroll1 ce1 on 
	ds.EMPI=ce1.EMPI
Join #spd_contenroll2 ce2 on
	ds.EMPI=ce2.EMPI



-- Start of Event List

-- Get members with Diabetes

DROP TABLE IF EXISTS #spd_diabetesmemlist;
CREATE table #spd_diabetesmemlist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	DIAG_DATA_SRC varchar(50)
			
);
Insert into #spd_diabetesmemlist
Select distinct
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes')
Where
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


--Members with Acute Inpatient without Telehealth Modifier
Drop table if exists #spd_acuteiplist;
CREATE table #spd_acuteiplist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	PROC_DATA_SRC varchar(50)
			
)
Insert into #spd_acuteiplist
Select distinct
	EMPI
	,PROC_START_DATE
	,CLAIM_ID
	,PROC_DATA_SRC
From GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient','Telehealth Modifier')
where
	PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


-- Identify Telehealth POS Claims
Drop table if exists #spd_telehealthvisits;
Create Table #spd_telehealthvisits
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #spd_telehealthvisits
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE
Where 
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt1 and @ce_enddt and
	ISNULL(POS,'0') in
	(
		Select code from HDS.ValueSet_TO_CODE where Value_SET_NAMe='Telehealth POS'
	)
	

-- ?	At least one acute inpatient encounter (Acute Inpatient Value Set) with a diagnosis of diabetes (Diabetes Value Set) without (Telehealth Modifier Value Set; Telehealth POS Value Set).
Drop table if exists #spd_acuteInpatientEvents;
Create Table #spd_acuteInpatientEvents
(
	EMPI varchar(100)
)
Insert into #spd_acuteInpatientEvents
select distinct
	d.EMPI
From #spd_diabetesmemlist d
Join #spd_acuteiplist a on
	d.EMPI=a.EMPI and
	d.CLAIM_ID=a.CLAIM_ID and
	d.DIAG_DATA_SRC=a.PROC_DATA_SRC
Left outer join #spd_telehealthvisits tv on
	a.EMPI=tv.EMPI and
	a.CLAIM_ID=tv.CLAIM_ID and
	a.PROC_DATA_SRC=tv.CL_DATA_SRC
Where
	tv.EMPI is null


	
-- Inpatient Member list
DROP TABLE IF EXISTS #spd_ipstaylist;
CREATE table #spd_ipstaylist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
);
insert into #spd_ipstaylist
Select distinct
	EMPI
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
Where
	CL_DATA_SRC not in(Select data_source from data_source where supplemental=1)


-- Non acute inpatient stay list
DROP TABLE IF EXISTS #spd_nonacuteipstaylist;
CREATE table #spd_nonacuteipstaylist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
);
insert into #spd_nonacuteipstaylist
Select distinct
	EMPI
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt1,@ce_enddt)


-- Identify Members with diabetes and acute Inpatient discharge
Drop table if exists #spd_acuteInpatientDischargeEvents;
Create Table #spd_acuteInpatientDischargeEvents
(
	EMPI varchar(100)
)
Insert into #spd_acuteInpatientDischargeEvents
select distinct
	t1.EMPI
from #spd_ipstaylist t1
Join #spd_diabetesmemlist d on
	t1.EMPI=d.EMPI and
	t1.CLAIM_ID=d.CLAIM_ID and
	t1.CL_DATA_SRC=d.DIAG_DATA_SRC and
	d.DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)
left outer join #spd_nonacuteipstaylist t2 on 
	t1.EMPI=t2.EMPI and 
	t1.CLAIM_ID=t2.CLAIM_ID and
	t1.CL_DATA_SRC=t2.CL_DATA_SRC
Where
	t2.EMPI is null


-- atleast 2 Outpatient,observation,Tele and online visits,Noncaute Inaptient encunter and Non acute inpatient stays with Diabetes
DROP TABLE IF EXISTS #spd_EventVisitlist;
CREATE table #spd_EventVisitlist
(
	EMPI varchar(100),
);
Insert into #spd_EventVisitlist
Select 
	EMPI
From
(
	Select distinct 
		t1.EMPI
		,t1.ServiceDate
	From
	(
		Select
			EMPI
			,PROC_START_DATE as servicedate
			,CLAIM_ID
			,PROC_DATA_SRC as DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')
		Where
			PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

		Union all

		Select
			EMPI
			,PROC_START_DATE as servicedate
			,CLAIM_ID
			,PROC_DATA_SRC as DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')
		Where
			PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

		Union all

		Select
			EMPI
			,PROC_START_DATE as servicedate
			,CLAIM_ID
			,PROC_DATA_SRC as DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')
		Where
			PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


		Union all

		Select
			EMPI
			,PROC_START_DATE as servicedate
			,CLAIM_ID
			,PROC_DATA_SRC as DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')
		Where
			PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

		Union all

		Select
			EMPI
			,PROC_START_DATE as servicedate
			,CLAIM_ID
			,PROC_DATA_SRC as DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')
		Where
			PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

		Union all

		Select
			na.EMPI
			,PROC_START_DATE as servicedate
			,na.CLAIM_ID
			,PROC_DATA_SRC as DATA_SRC
		From GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient','Telehealth Modifier') na
		left outer Join #spd_telehealthvisits tv on
			na.EMPI=tv.EMPI and
			na.CLAIM_ID=tv.CLAIM_ID and
			na.PROC_DATA_SRC=tv.CL_DATA_SRC
		Where
			PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
			tv.EMPI is null

		Union all

		Select
			ip.EMPI
			,ip.servicedate
			,ip.CLAIM_ID
			,ip.CL_DATA_SRC as DATA_SRC
		From #spd_ipstaylist ip
		join #spd_nonacuteipstaylist na on
			ip.EMPI=na.EMPI and
			ip.CLAIM_ID=na.CLAIM_ID and
			ip.CL_DATA_SRC=na.CL_DATA_SRC

		Union all

		Select
			EMPI
			,FROM_DATE as ServiceDate
			,CLAIM_ID
			,CL_DATA_SRC
		From CLAIMLINE
		Where
			ROOT_COMPANIES_ID=@rootId and
			CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
			ISNULL(POS,'0')!='81' and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				Select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('ED','Outpatient')
			)

	)t1
	Join #spd_diabetesmemlist d on
		t1.EMPI=d.EMPI and
		t1.CLAIM_ID=d.CLAIM_ID and
		t1.DATA_SRC=d.DIAG_DATA_SRC
		
)t2
Group by
	EMPI
Having
	count(EMPI)>1


-- Identify  Members who were dispensed insulin or hypoglycemics/anti-hyperglycemics on an ambulatory basis during the measurement year or the year prior to the measurement year (Diabetes Medications List).
Drop table if exists #spd_eventbymedicationlist;
Create Table #spd_eventbymedicationlist
(
	EMPI varchar(100)
)
Insert into #spd_eventbymedicationlist
Select distinct
	EMPI
From MEDICATION
Where
	ROOT_COMPANIES_ID=@rootId and
	MED_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	FILL_DATE between @ce_startdt1 and @ce_enddt and
	MEDICATION_CODE in
	(
		select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Diabetes Medications'
	)

update #spddataset set Event=0 where meas='SPDA'
update #spddataset set Event=1 from #spddataset ds join #spd_acuteInpatientEvents re on ds.EMPI=re.EMPI and ds.meas='SPDA';
update #spddataset set Event=1 from #spddataset ds join #spd_acuteInpatientDischargeEvents re on ds.EMPI=re.EMPI and ds.meas='SPDA';
update #spddataset set Event=1 from #spddataset ds join #spd_EventVisitlist re on ds.EMPI=re.EMPI and ds.meas='SPDA';
update #spddataset set Event=1 from #spddataset ds join #spd_eventbymedicationlist re on ds.EMPI=re.EMPI and ds.meas='SPDA';



-- End of Event List

	
-- Required Exclusion
-- MI (MI Value Set) on the discharge claim

DROP TABLE IF EXISTS #spd_MImemlist;
CREATE table #spd_MImemlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)		
);
Insert into #spd_MImemlist
Select distinct	
	EMPI
	,CLAIM_ID
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt1,'MI')

	


--Inpatient member list
DROP TABLE IF EXISTS #spd_ipmemlist;
CREATE table #spd_ipmemlist
(
	EMPI varchar(100),
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
);
Insert into #spd_ipmemlist
Select distinct
	EMPI
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,@ce_startdt1,@ce_enddt)


--Inpatient member list
DROP TABLE IF EXISTS #spd_nonacuteipmemlist;
CREATE table #spd_nonacuteipmemlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
);
Insert into #spd_nonacuteipmemlist
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	

	
-- 	Other revascularization
DROP TABLE IF EXISTS #spd_ORCmemlist;
CREATE table #spd_ORCmemlist
(
	EMPI varchar(100)			
);
Insert into #spd_ORCmemlist
Select distinct
	EMPI
From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Other Revascularization')

	

-- CABG Mem LIST
DROP TABLE IF EXISTS #spd_CABGmemlist;
CREATE table #spd_CABGmemlist
(
	EMPI varchar(100)			
);
Insert into #spd_CABGmemlist
select distinct 
	EMPI 
from
(
	Select
		EMPI
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'CABG')

	Union all

	Select
		EMPI
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt1,'CABG')


)t1


-- PCI List
DROP TABLE IF EXISTS #spd_PCImemlist;
CREATE table #spd_PCImemlist
(
	EMPI varchar(100)			
);
Insert into #spd_PCImemlist
select distinct 
	EMPI 
from
(
	Select
		EMPI
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'PCI')

	Union all

	Select
		EMPI
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt1,'PCI')

)t1




-- IVD Diagnosis
DROP TABLE IF EXISTS #spd_IVDmemlist;
CREATE table #spd_IVDmemlist
(
	EMPI varchar(100),
	servicedate date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
	
);
Insert into #spd_IVDmemlist
Select distinct
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'IVD')





DROP TABLE IF EXISTS #spd_IVDvisitlist;
CREATE table #spd_IVDvisitlist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #spd_IVDvisitlist
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as servicedate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

	Union all

	Select
		EMPI
		,PROC_START_DATE as servicedate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

	Union all

	Select
		EMPI
		,PROC_START_DATE as servicedate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

	Union all

	Select
		ai.EMPI
		,PROC_START_DATE as servicedate
		,ai.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient','Telehealth Modifier') ai
	Left outer join #spd_telehealthvisits tv on
		ai.EMPI=tv.EMPI and
		ai.CLAIM_ID=tv.CLAIM_ID and
		ai.PROC_DATA_SRC=tv.CL_DATA_SRC
	Where
		tv.EMPI is null

	Union all

	Select
		ip.*
	From #spd_ipmemlist ip
	Left outer join #spd_nonacuteipmemlist na on
		ip.EMPI=na.EMPI and
		ip.CLAIM_ID=na.CLAIM_ID and
		ip.CL_DATA_SRC=na.CL_DATA_SRC
	Where
		na.EMPI is null
		


	Union all

	Select
		EMPI
		,FROM_DATE as servicedate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		FROM_DATE between @ce_startdt1 and @ce_enddt and
		REV_CODE in
		(
			Select code from HDS.ValueSet_to_code where code_system='UBREV' and Value_Set_Name='Outpatient'
		)

	Union all

	Select 
		EMPI
		,VisitDate as ServiceDate
		,VisitId as CLAIM_ID
		,Visit_DATA_SRC as DATA_SRC
	from Visit 
	where 
		ROOT_COMPANIES_ID=@rootId and
		VisitDate between @ce_startdt1 and @ce_enddt and
		VisitTypeCode in
		(
			Select code from HDS.ValueSet_to_code where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Acute Inpatient')
		)
)t1


-- Identify members with IVD in both years
Drop table if exists #spd_ivdexclusionlist;
Create table #spd_ivdexclusionlist
(
	EMPI varchar(100)
)
Insert into #spd_ivdexclusionlist
Select
	EMPI
From
(
	Select 
		ivd.EMPI
		,Year(ivd.ServiceDate) as ServiceYear
	From #spd_ivdmemlist ivd
	Join #spd_ivdvisitlist v on
		ivd.EMPI=v.EMPI and
		ivd.CLAIM_ID=v.CLAIM_ID and
		ivd.DATA_SRC=v.DATA_SRC
	Where
		v.CLAIM_ID is not null

	Union all

	Select 
		ivd.EMPI
		,Year(ivd.ServiceDate) as ServiceYear
	From #spd_ivdmemlist ivd
	Join #spd_ivdvisitlist v on
		ivd.EMPI=v.EMPI and
		ivd.ServiceDate=v.ServiceDate and
		ivd.DATA_SRC=v.DATA_SRC
	Where
		v.CLAIM_ID is null
)t1
Group by
	EMPI
Having
	count(distinct ServiceYear)>1


-- Required Exclusion List
DROP TABLE IF EXISTS #spd_requiredexcllist;
CREATE table #spd_requiredexcllist
(
	EMPI varchar(100)			
);
Insert into #spd_requiredexcllist
Select distinct 
	EMPI 
from
(
	select distinct 
		s1.EMPI 
	from #spd_ipmemlist s1
	join #spd_MImemlist s2 on 
		s1.EMPI=s2.EMPI and 
		s1.CLAIM_ID=s2.CLAIM_ID
	
	Union all 
	
	select 
		EMPI 
	from #spd_ivdexclusionlist
	
	Union all
	
	select distinct
		EMPI 
	from #spd_ORCmemlist
	
	Union all
	
	select distinct
		EMPI 
	from #spd_CABGmemlist
	
	Union all
	
	select distinct
		EMPI 
	from #spd_PCImemlist

)t1





-- Required exclusions 
-- ?	Female members with a diagnosis of pregnancy (Pregnancy Value Set) during the measurement year or the year prior to the measurement year.

DROP TABLE IF EXISTS #spd_pregmemlist;
CREATE table #spd_pregmemlist
(
	EMPI varchar(100)			
);
Insert into #spd_pregmemlist
Select distinct
	EMPI
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Pregnancy')



-- ?	In vitro fertilization (IVF Value Set) in the measurement year or year prior to the measurement year

DROP TABLE IF EXISTS #spd_IVFmemlist;
CREATE table #spd_IVFmemlist
(
	EMPI varchar(100)			
)
Insert into #spd_IVFmemlist
Select distinct
	EMPI
From
(

Select 
	EMPI
From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'IVF')

Union all

Select 
	EMPI
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'IVF')
)t1




-- ?	Dispensed at least one prescription for clomiphene (Estrogen Agonists Medications List) during the measurement year or the year prior to the measurement year

DROP TABLE IF EXISTS #spd_estrogenmemlist;
CREATE table #spd_estrogenmemlist
(
	EMPI varchar(100)		
)
Insert into #spd_estrogenmemlist
Select distinct
	EMPI
From MEDICATION
Where
	ROOT_COMPANIES_ID=@rootId and
	FILL_DATE between @ce_startdt1 and @ce_enddt and
	MEDICATION_CODE in
	(
		select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Estrogen Agonists Medications'
	)
	




-- ?	ESRD (ESRD Diagnosis Value Set) or dialysis (Dialysis Procedure Value Set) during the measurement year or the year prior to the measurement year

drop table if exists #spd_esrdmemlist;
CREATE table #spd_esrdmemlist
(
	EMPI varchar(100)			
)
Insert into #spd_esrdmemlist
Select distinct
	EMPI
From
(
	Select
		EMPI
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Dialysis Procedure')

	Union All

	Select
		EMPI
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Dialysis Procedure')

	Union All

	Select
		EMPI
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'ESRD Diagnosis')
)t1




-- ?	Cirrhosis (Cirrhosis Value Set) during the measurement year or the year prior to the measurement year

Drop table if exists #spd_Cirrhosismemlist;
CREATE table #spd_Cirrhosismemlist
(
	EMPI varchar(100)			
);
Insert into #spd_Cirrhosismemlist
Select distinct
	EMPI
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Cirrhosis')




-- ?	Myalgia, myositis, myopathy or rhabdomyolysis (Muscular Pain and Disease Value Set) during the measurement year

drop table if exists #spd_MPDmemlist;
CREATE table #spd_MPDmemlist
(
	EMPI varchar(100)			
);
Insert into #spd_MPDmemlist
Select distinct
	EMPI
From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Muscular Pain and Disease')






-- ?	Members receiving palliative care (Palliative Care Assessment Value Set; Palliative Care Encounter Value Set; Palliative Care Intervention Value Set) during the measurement year

drop table if exists #spd_palliativememlist;
CREATE table #spd_palliativememlist
(
	EMPI varchar(100)			
);
Insert into #spd_palliativememlist
Select distinct
	EMPI
From palliativecare(@rootId,@ce_startdt,@ce_enddt)



update #spddataset set rexcld=0
update #spddataset set Rexcld=1 from #spddataset ds join #spd_palliativememlist re7 on ds.EMPI=re7.EMPI ;
update #spddataset set Rexcld=1 from #spddataset ds join #spd_MPDmemlist re6 on ds.EMPI=re6.EMPI ;
update #spddataset set Rexcld=1 from #spddataset ds join #spd_Cirrhosismemlist re5 on ds.EMPI=re5.EMPI ;
update #spddataset set Rexcld=1 from #spddataset ds join #spd_esrdmemlist re4 on ds.EMPI=re4.EMPI ;
update #spddataset set Rexcld=1 from #spddataset ds join #spd_estrogenmemlist re3 on ds.EMPI=re3.EMPI;
update #spddataset set Rexcld=1 from #spddataset ds join #spd_IVFmemlist re2 on ds.EMPI=re2.EMPI;
update #spddataset set Rexcld=1 from #spddataset ds join #spd_pregmemlist re1 on ds.EMPI=re1.EMPI and ds.Gender='F';
update #spddataset set rexcld=1 from #spddataset ds join #spd_requiredexcllist re on ds.EMPI=re.EMPI;


-- Exclusion

	-- AdvancedIllness

	drop table if exists #spd_advillness;
	CREATE table #spd_advillness
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spd_advillness
	select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID 
	from advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)




	-- LTI Exclusion
	drop table if exists #spd_LTImembers;
	CREATE table #spd_LTImembers
	(
		EMPI varchar(100)				
	);
	Insert into #spd_LTImembers
	Select distinct
		EMPI
	From LTImembers(@rootId,@ce_startdt,@ce_enddt)
	
	

	-- Hospice Exclusion
	drop table if exists #spd_hospicemembers;
	CREATE table #spd_hospicemembers
	(
		EMPI varchar(100)		
	);
	Insert into #spd_hospicemembers
	Select distinct
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	
	
			
-- Frailty Members LIST
	drop table if exists #spd_frailtymembers;
	CREATE table #spd_frailtymembers
	(		
		EMPi varchar(100)			
	);
	Insert into #spd_frailtymembers
	Select distinct
		EMPI
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
	Where
		DataSource not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	
	
	
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #spd_inpatientstaylist;
	CREATE table #spd_inpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spd_inpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	
	
	-- Non acute Inpatient stay list
	
	drop table if exists #spd_noncauteinpatientstaylist;
	CREATE table #spd_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spd_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	-- Outpatient and other visits
	drop table if exists #spd_visitlist;
	CREATE table #spd_visitlist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spd_visitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1

	
	-- Required exclusion table
	drop table if exists #spd_reqdexcl1;
	CREATE table #spd_reqdexcl1
	(
		EMPI varchar(100)			
	);
	Insert into #spd_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.FROM_DATE 
			from
			(
				select 
					EMPI
					,FROM_DATE
					,CLAIM_ID  
				from #spd_visitlist 
				
				union all
				
				select 
					na.EMPI
					,na.FROM_DATE
					,na.CLAIM_ID 
				from #spd_noncauteinpatientstaylist na
				join #spd_inpatientstaylist inp on
					na.EMPi=inp.EMPi and
					na.CLAIM_ID=inp.CLAIM_ID
			)t1
			Join #spd_advillness a on
				a.EMPi=t1.EMPI and 
				a.CLAIM_ID=t1.CLAIM_ID
		)t2 
		group by 
			t2.EMPI 
		having 
			count(t2.EMPi)>1
	)t3 
	Join #spd_frailtymembers f on 
		f.EMPi=t3.EMPI


	


-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #spd_reqdexcl2;
	CREATE table #spd_reqdexcl2
	(
		EMPI varchar(100)				
	);
	insert into #spd_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(
			
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
		)t1
		Join #spd_advillness a on
			a.EMPI=t1.EMPI and
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #spd_frailtymembers f on 
		f.EMPI=t2.EMPI

	
	
	
			
	-- Required exclusion 3
	drop table if exists #spd_reqdexcl3;
	CREATE table #spd_reqdexcl3
	(
		EMPI varchar(100)			
	);
	insert into #spd_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.FROM_DATE
			,t1.CLAIM_ID 
		from
		(
			select 
				inp.EMPI
				,inp.FROM_DATE
				,inp.CLAIM_ID 
			from #spd_inpatientstaylist inp
			left outer join #spd_noncauteinpatientstaylist na on 
				inp.EMPI=na.EMPI and 
				inp.CLAIM_ID=na.CLAIM_ID
			where 
				na.EMPI is null
		)t1
		join #spd_advillness a on
			a.EMPI=t1.EMPI and
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #spd_frailtymembers f on 
		f.EMPI=t2.EMPI

	
	
	

		
-- RequiredExcl 4
	drop table if exists #spd_reqdexcl4;
	CREATE table #spd_reqdexcl4
	(
		EMPI varchar(100)				
	);
	insert into #spd_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		select distinct
				EMPI
			from MEDICATION 
			where
				ROOT_COMPANIES_ID=@rootId  and 
				MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
				FILL_DATE between @ce_startdt1 and @ce_enddt and
				MEDICATION_CODE in
				(
					select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
				)
	)t1
	Join #spd_frailtymembers f on 
		f.EMPI=t1.EMPI;

		
	Update #spddataset set rexcl=0
	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

	update #spddataset set rexcl=1 from #spddataset ds join #spd_hospicemembers hos on hos.EMPI=ds.EMPI;

	update #spddataset set rexcl=1 from #spddataset ds join #spd_LTImembers re on ds.EMPI=re.EMPI where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MC','MR');

	-- Members with Institutinal SNP
	UPDATE ds 
		SET ds.rexcl=1 
	FROM #spddataset ds 
	JOIN ENROLLMENT s on 
		ds.EMPI=s.EMPI and 
		s.ROOT_COMPANIES_ID=@rootId 
	WHERE  
		ds.age>=66 AND 
		ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MC','MR') AND 
		s.EFF_DATE<=@ce_enddt AND s.TERM_DATE>=@ce_startdt AND 
		s.PAYER_TYPE='SN2';


-- End of Exclusions

	
--Numerator
	-- SPD A
	
	
DROP TABLE IF EXISTS #spd_spdamemlist;
CREATE table #spd_spdamemlist
(
	EMPI varchar(100)
	,ServiceDate Date
	,Code varchar(100)
);
Insert into #spd_spdamemlist
select distinct
	EMPI
	,FILL_DATE
	,MEDICATION_CODE
from MEDICATION 
where
	ROOT_COMPANIES_ID=@rootId and 
	FILL_DATE between @ce_startdt and @ce_enddt and 
	MEDICATION_CODE in
	(
		select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications')
	)

update #spddataset set num=1 from #spddataset ds join #spd_spdamemlist n1 on ds.EMPI=n1.EMPI and ds.meas='SPDA'





-- Create SPD Numerator Details
Drop table if exists #spd_numdetails;
Create Table #spd_numdetails
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(100),
	Rownumber INT
)
Insert into #spd_numdetails
Select
	*
From
(
	select *,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn from #spd_spdamemlist
)t1
Where
	rn=1





-- SPC B Event


 update ds set ds.Event=1 from #spddataset ds join #spddataset s1 on ds.EMPI=s1.EMPI and ds.meas='SPDB' and s1.num=1 and s1.meas='SPDA';


 
 

-- SPD B Numerator


DROP TABLE IF EXISTS #spd_statinmedmemlist;
CREATE table #spd_statinmedmemlist
(
	EMPI varchar(100),
	pdc INT			
);
Insert into #spd_statinmedmemlist
select 
	EMPI
	,sum(pdc) as pdc 
from
(
	-- adjust DOS for year end after summing
	select
		EMPI
		,Servicedate
		,case 
			when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@ce_enddt then datediff(day,servicedate,@ce_enddt)+1 
			else calcdos 
		end as pdc 
	from
	(
		select 
			EMPI
			,servicedate
			,sum(calcdaysofsupply) as calcdos 
		from
		(
			-- SUM the DOS supply after adjustment
			select distinct 
				EMPI
				,servicedate
				,calcdaysofsupply 
			from
			(
				-- Adjust DOS to accomodate year end
				select 
					*
					,case 
						when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@ce_enddt then datediff(day,servicedate,@ce_enddt)+1 
						else calcdos 
					end as calcdaysofsupply 
				from
				(
					select 
						*
						,case 
						-- different Medication on same date ,take one with more DOS
						when servicedate=nextmeddate  and medication!=nextmed and daysofsupply>nextdos then daysofsupply
						when servicedate=nextmeddate  and medication!=nextmed and daysofsupply<nextdos then nextdos
						when servicedate=nextmeddate  and medication=nextmed and MEDICATION_CODE_TYPE!=nextmedtype and daysofsupply<nextdos then nextdos
						when servicedate=nextmeddate  and medication=nextmed and MEDICATION_CODE_TYPE!=nextmedtype and daysofsupply>nextdos then daysofsupply
						when servicedate=prevmeddate   and medication!=prevmed and daysofsupply>prevdos then daysofsupply
						when servicedate=prevmeddate  and medication!=prevmed and daysofsupply<prevdos then prevdos
						when servicedate=prevmeddate  and medication=prevmed and MEDICATION_CODE_TYPE!=prevmedtype and daysofsupply<prevdos then prevdos
						when servicedate=prevmeddate  and medication=prevmed and MEDICATION_CODE_TYPE!=prevmedtype and daysofsupply>prevdos then daysofsupply
						-- Adjust DOS to accomodate overlap with next record
						when servicedate!=nextmeddate and medication!=nextmed and enddate>= ISNULL(nextmeddate,enddate) then daysofsupply-(datediff(day,ISNULL(nextmeddate,enddate),enddate)+1)
						else daysofsupply
					end as calcdos
				from
				(
					-- Get Previous and next record for further processing
					select 
						*
						,convert(varchar,cast(dateadd(day,ISNULL(NULLIF(daysofsupply,''),1)-1,servicedate) as date),112) as enddate
						,isnull(lead(servicedate,1) over(partition by EMPI order by servicedate,daysofsupply),servicedate) as nextmeddate
						,isnull(lead(Medication,1) over(partition by EMPI order by servicedate,daysofsupply),medication) as nextmed
						,isnull(lead(MEDICATION_CODE_TYPE,1) over(partition by EMPI order by servicedate,daysofsupply),MEDICATION_CODE_TYPE) as nextmedtype
						,isnull(lead(daysofsupply,1) over(partition by EMPI order by servicedate,daysofsupply),daysofsupply) as nextdos
						,isnull(lag(servicedate,1) over(partition by EMPI order by servicedate,daysofsupply),servicedate) as prevmeddate
						,isnull(lag(Medication,1) over(partition by EMPI order by servicedate,daysofsupply),medication) as prevmed
						,isnull(lag(daysofsupply,1) over(partition by EMPI order by servicedate,daysofsupply),daysofsupply) as prevdos
						,isnull(lag(MEDICATION_CODE_TYPE,1) over(partition by EMPI order by servicedate,daysofsupply),MEDICATION_CODE_TYPE) as prevmedtype
						
						
					from
					(
						Select
							*
						From
						(
							select 
								t1.EMPI
								,t1.servicedate
								,cast(t1.daysofsupply as INT) as daysofsupply
								,t2.Medication
								,MEDICATION_CODE_TYPE
							from
							(
								-- Read Mediction from Hedis_Pharm
								select
									EMPI
									,FILL_DATE as servicedate
									,Cast(SUPPLY_DAYS as numeric) as daysofsupply
									,MEDICATION_CODE as code
									,ISNULL(MEDICATION_CODE_TYPE,'') as MEDICATION_CODE_TYPE
									
								from MEDICATION 
								where 
									ROOT_COMPANIES_ID=@rootId and 
									FILL_DATE between @ce_startdt and @ce_enddt
									

						   
							)t1
							join
							(
								select 
									code
									,Medication_List_Name as Medication 
								from HDS.MEDICATION_LIST_TO_CODES 
								where 
									Medication_List_Name in
									('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications')
							)t2 on 
								t1.code=t2.code
						)t3
					)t4
				)t5
			)t6
		)t7 
	)t8 
	group by 
		EMPI
		,servicedate
)t8 
)t9
group by 
	EMPI 
order by EMPI


-- IPSD


drop table if exists 	#spd_statinmedipsdlist;
CREATE table #spd_statinmedipsdlist
(
	EMPI varchar(100),
	ipsd Date,
	treatmentperiod INT			
);
Insert into #spd_statinmedipsdlist
select 
	z1.EMPI
	,min(z1.servicedate) as ipsd
	,datediff(day,min(z1.servicedate),@ce_enddt)+1 as treatmentperiod 
from
(
	select 
		EMPI
		,FILL_DATE as servicedate
		,case 
			when convert(varchar,cast(DATEADD(day,SUPPLY_DAYS-1,FILL_DATE) as date),112) >@ce_enddt then @ce_enddt 
			else convert(varchar,cast(DATEADD(day,SUPPLY_DAYS-1,FILL_DATE) as date),112) 
		end as Enddate
		,MEDICATION_CODE as code  
	from MEDICATION 
	where 
		ROOT_COMPANIES_ID=@rootId  and 
		FILL_DATE between @ce_startdt and @ce_enddt and 
		MEDICATION_CODE in
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications')
		)
		
)z1 
group by 
	z1.EMPI


Drop table if exists #spd_rate2numlist;
CREATE table #spd_rate2numlist
(
	EMPI varchar(100)	
);
Insert into #spd_rate2numlist
select distinct 
	f1.EMPI 
from
(
	select 
		s1.EMPI
		,s1.treatmentperiod
		,s2.pdc
		,Round(((cast(pdc as float)/cast(treatmentperiod as float))*100),0) as adherence
		,cast(pdc as float)/cast(treatmentperiod as float)*100 as raw
	from #spd_statinmedipsdlist s1
	join #spd_statinmedmemlist s2 on 
		s1.EMPI=s2.EMPI
 
)f1 where f1.adherence>=80 


update #spddataset set num=1 from #spddataset ds join #spd_rate2numlist n1 on ds.EMPI=n1.EMPI and ds.meas='SPDB'


-- Optional Exclusion

-- Patients with Induced Diabetes
	drop table if exists #spd_induceddiabetesmemlist;
	CREATE table #spd_induceddiabetesmemlist
	(
		EMPI varchar(100)				
	);
	Insert into #spd_induceddiabetesmemlist
	Select distinct
		EMPI
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes Exclusions')
	
		
	-- Memebers with diabetes
	drop table if exists #spd_diabeticmemlist;
	CREATE table #spd_diabeticmemlist
	(
		EMPI varchar(100)				
	);
	Insert into #spd_diabeticmemlist
	Select distinct
		EMPI
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes')
	
	
-- Optional Exclusion
	drop table if exists #spd_optexcl;
	CREATE table #spd_optexcl
	(
		EMPI varchar(100)				
	);
	Insert into #spd_optexcl
	select 
		m.EMPI 
	from #spd_memlist m
	Left outer join #spd_diabeticmemlist d on
		m.EMPI=d.EMPI
	join #spd_induceddiabetesmemlist t2 on
		m.EMPI=t2.EMPI
	Where
		d.EMPI is null
	
	
	update #spddataset set excl=1 from #spddataset ds
	join #spd_optexcl oe on oe.EMPI=ds.EMPI;


	
---
	
		-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0 and payer not in('MMO','MOS','MPO','MEP','MC','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,Gender,@meas,@meas_year,@reportId,@rootId FROM #spddataset


		
/*
	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID in('9','49') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,Case
			When meas='SPDA' Then '9'
			When meas='SPDB' Then '49'
		End	as Measure_id
		,Case
			When meas='SPDA' Then 'Patients with a diagnosis of diabetes and compliant with statin medication therapy'
			When meas='SPDB' Then 'Medication Adherence for Oral Diabetes Medications'
		End	as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,Event
		,CASE 
			WHEN rexcl=0 and rexcld=0 and Event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #spddataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #spd_numdetails nd on d.EMPI=nd.EMPI
	where event=1
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID in('9','49') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,Case
			When Measure_id='9' Then 'Diabetes'
			When Measure_id='49' Then 'Medication Management'
		End as MEASURE_SUBTITLE
		,MEASURE_TYPE
		,Case
			When Num=1 and Excl=0 and Rexcl=0 and Event=1 Then 1
			else 0
		end as NUM_Count
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 and Event=1 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,Case
			When MEASURE_ID='9' Then 87
			When MEASURE_ID='49' Then 88
		End as Target
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID in('9','49') and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target

*/


GO
