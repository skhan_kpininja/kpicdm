SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE PROCEDURE [dbo].[KPI_HEDIS_TRC]
AS
-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='TRC-S'
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_startdt2 DATE;
DECLARE @ce_enddt2 DATE;
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;
DECLARE @t_planid VARCHAR(10);
Declare @ce_dischargeenddt Date;

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='TRC';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_dischargeenddt=concat(@meas_year,'-12-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_startdt2=concat(@meas_year-2,'-10-01');
SET @ce_enddt2=concat(@meas_year-2,'-12-31');

/*
Set @reporttype='Physician'
Set @measurename='Breast Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=85
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='22'
*/

-- Identify Eligible Population
-- 1. Identify Members who qualify for Hospice exclusion
drop table if exists #trc_hospicemembers;
CREATE table #trc_hospicemembers
(
	EMPI varchar(100)		
);
Insert into #trc_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)


--2. Event list	
drop table if exists #trc_ipstaylist;
Create table #trc_ipstaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #trc_ipstaylist
Select
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,'1900-01-01','3000-01-01')
Where
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


-- 2.1 Identfify non acute stays
drop table if exists #trc_nonacutestaylist;
Create table #trc_nonacutestaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #trc_nonacutestaylist
Select
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,'1900-01-01','3000-01-01')
Where
	CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


-- Identify Acute STays
drop table if exists #trc_acutestaylist;
Create table #trc_acutestaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #trc_acutestaylist
Select
	ip.*
From #trc_ipstaylist ip
left outer join #trc_nonacutestaylist na on
	ip.EMPI=na.EMPI and
	ip.CLAIM_ID=na.CLAIM_ID and
	ip.CL_DATA_SRC=na.CL_DATA_SRC
Where
	na.EMPI is null



--2.3 Identify acute stays which happened between nonacute stays
Drop table if exists #trc_acutestay_in_nonacutestay
Create table #trc_acutestay_in_nonacutestay
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #trc_acutestay_in_nonacutestay
Select
	a.EMPI
	,a.CLAIM_ID
	,a.CL_DATA_SRC
From #trc_acutestaylist a
join #trc_nonacutestaylist na on
	a.EMPI=na.EMPI and
	na.ADM_DATE<a.DIS_DATE and
	na.DIS_DATE>a.ADM_DATE


-- 2.4 Merge all stays 
drop table if exists #trc_directtransfers;
Create table #trc_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	t1.EMPI
	,t1.ADM_DATE
	,DIS_DATE
	,t1.CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by t1.EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) between 0 and 30 then 0 
		else 1
	end grp_start
	From
	(
		select
			ip.*
		From #trc_ipstaylist ip
		left outer join #trc_acutestay_in_nonacutestay a on
			ip.CLAIM_ID=a.CLAIM_ID and
			ip.EMPI=a.EMPI and
			ip.CL_DATA_SRC=a.CL_DATA_SRC
		where
			a.EMPI is null
		 
	)t1	
)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #trc_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1
Where
	DischargeDate between @ce_startdt and @ce_dischargeenddt


-- Create a normalized transfer table to improve lookup
drop table if exists #trc_directtransfers_normalized;
Create table #trc_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #trc_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #trc_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p




-- Identify members who are enrolled during the continuous enrollment period and are 18+
 -- Create Temp Patient Enrollment
drop table if exists #trc_tmpsubscriber;
CREATE TABLE #trc_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #trc_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) >=18 
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;




--Identify member productline which needs to be reported
drop table if exists #trc_payermap
Create table #trc_payermap
(
	EMPI varchar(100),
	Payer varchar(50),
	Age INT,
	Gender varchar(10),
	DIS_DATE DATE
)
Insert into #trc_payermap
Select distinct
	EMPI
	,pm.PayerMapping
	,age
	,gender
	,DIS_DATE
From
(
	Select distinct
		EMPI
		,gender
		,age
		,DIS_DATE
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
				,DIS_DATE
			From
			(
				select 
					s.EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,dt.DIS_DATE
					,RANK() over(partition by s.EMPI,dt.DIS_DATE order by StartDate desc,EndDate Desc) as rn 
				from #trc_tmpsubscriber s
				join #trc_directtransfers_normalized dt on
					s.EMPI=dt.EMPI and
					s.StartDate<=dateadd(day,30,dt.DIS_DATE) and
					s.EndDate>=dt.DIS_DATE
				where  
					StartDate<=dateadd(day,30,dt.DIS_DATE)
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='TRC'
Order by 1



-- Create trcdataset
drop table if exists #trcdataset;
CREATE TABLE #trcdataset (
  EMPI varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [Gender] varchar(45) NOT NULL,
  [Age] INT,
  [Rexcl] smallint DEFAULT '0',
  [Rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [Excl] smallint DEFAULT '0',
  [Num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'
) ;
Insert into #trcdataset(EMPI,Meas,Payer,Gender,Age)
Select
	EMPI
	,concat('TRCE',char(64+staynumber)) as meas
	,Payer
	,Gender
	,Age
From
(
	Select
		*	
		,Dense_rank() over(partition by EMPI order by DIS_DATE) as staynumber
	From #trc_payermap
)t1
Where 
	staynumber<=2


--Insert records for Medicine
Insert into #trcdataset(EMPI,Meas,Payer,Gender,Age)
Select
	EMPI
	,concat('TRCM',char(64+staynumber)) as meas
	,Payer
	,Gender
	,Age
From
(
	Select
		*	
		,Dense_rank() over(partition by EMPI order by DIS_DATE) as staynumber
	From #trc_payermap
)t1
Where 
	staynumber<=2



-- Create reference table with meas and discharge date
Drop table if exists #trc_discharges
Create Table #trc_discharges
(
	EMPI varchar(100),
	StayNumber INT,
	DIS_DATE Date
)
Insert into #trc_discharges
Select
	EMPI
	,staynumber
	,DIS_DATE
From
(
	Select
		*	
		,Dense_rank() over(partition by EMPI order by DIS_DATE) as staynumber
	From #trc_payermap
)t1
Where 
	staynumber<=2

-- Numerator Logic Start

-- Numerator for Patient Engagement After Inpatient Discharge

-- Identify Outpatient,telephone,Transitional care Management and online Asseements in MY
Drop table if exists #trc_PEvstlist;
Create table #trc_PEvstlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Data_Src varchar(100)
)
Insert into #trc_PEvstlist
Select distinct
	*
From
(


	Select 
		EMPI
		,PROC_START_DATE
		,PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Outpatient,Telephone Visits,Transitional Care Management Services,Online Assessments')


	Union all

	Select 
		EMPI
		,DIAG_START_DATE
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Outpatient,Telephone Visits')

	Union all

	Select 
		EMPI
		,FROM_DATE
		,CL_DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_startdt,@ce_enddt,'Outpatient')
	

	Union all

	Select 
		EMPI
		,VisitDate
		,Visit_DATA_SRC
	From GetVisits(@rootId,@ce_startdt,@ce_enddt,'Outpatient,Telephone Visits,Transitional Care Management Services,Online Assessments')
)t1



drop table if exists #trc_PENumList;
Create table #trc_PENumList
(
	EMPI varchar(100),
	meas varchar(20)
)
Insert into #trc_PENumList
select distinct 
	b.EMPI
	,concat('TRCE',char(64+staynumber))
from #trc_discharges b
join #trc_PEvstlist v on 
	b.EMPI=v.EMPI and 
	v.servicedate between DATEADD(day,1,b.DIS_DATE) and DATEADD(day,30,b.DIS_DATE)

update #trcdataset set Num=1 from #trcdataset ds join #trc_PENumList n on ds.EMPI=n.EMPI and ds.Meas=n.Meas


-- Numerator logic for Medication Reconciliation Post-Discharge
Drop table if exists #trc_medreconvstlist;
Create table #trc_medreconvstlist
(
	EMPI varchar(100),
	servicedate Date,
	Data_src varchar(50)
)
Insert into #trc_medreconvstlist
Select distinct 
	*
From
(

	Select 
		EMPI
		,PROC_START_DATE
		,PROC_DATA_SRC
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Medication Reconciliation Encounter,Medication Reconciliation Intervention','CPT CAT II Modifier') p
	Join PROVIDER_FLAGS pf on
		p.ATT_NPI=pf.ProvId and
		pf.ROOT_COMPANIES_ID=@rootId and
		'Y' in(RegisteredNurse,ClinicalPharmacist,PresribingPreviliges)
	
	Union all

	Select 
		p.EMPI
		,PROC_START_DATE
		,PROC_DATA_SRC
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Medication Reconciliation Encounter,Medication Reconciliation Intervention','CPT CAT II Modifier') p
	join visit v on
		p.EMPI=v.EMPI and
		p.PROC_DATA_SRC=v.VISIT_DATA_SRC and
		p.PROC_START_DATE=v.VisitDate and
		v.ROOT_COMPANIES_ID=@rootId
	Join PROVIDER_FLAGS pf on
		v.VisitProviderNPI=pf.ProvId and
		pf.ROOT_COMPANIES_ID=@rootId and
		'Y' in(RegisteredNurse,ClinicalPharmacist,PresribingPreviliges)
	Where
		p.CLAIM_ID is null

	Union all

	Select 
		EMPI
		,VisitDate
		,Visit_DATA_SRC
	From GetVisits(@rootId,@ce_startdt,@ce_enddt,'Outpatient,Telephone Visits,Transitional Care Management Services,Online Assessments') v
	Join PROVIDER_FLAGS pf on
		v.VisitProviderNPI=pf.ProvId and
		pf.ROOT_COMPANIES_ID=@rootId and
		'Y' in(RegisteredNurse,ClinicalPharmacist,PresribingPreviliges)

)t1


drop table if exists #trc_MedRecNumList;
Create table #trc_MedRecNumList
(
	EMPI varchar(100),
	Meas varchar(10)
)
Insert into #trc_MedRecNumList
select distinct 
	b.EMPI
	,concat('TRCM',char(64+b.staynumber)) as Meas 
from #trc_discharges b
join #trc_medreconvstlist v on 
	b.EMPI=v.EMPI and 
	v.servicedate between b.DIS_DATE and DATEADD(day,30,b.DIS_DATE)


update #trcdataset set Num=1 from #trcdataset ds join #trc_MedRecNumList n on ds.EMPI=n.EMPI and ds.Meas=n.Meas



--Check for continuous enrollment
-- Date of discharge through 30 days after discharge (31 total days).	
drop table if exists #trc_contenroll;
create table #trc_contenroll
(
	EMPI varchar(50),
	StayNumber INT
);
With coverage_CTE1 as
(

	select 
		b.EMPI
		,DIS_DATE
		,staynumber
		,isnull(lag(en.EndDate,1) over(partition by b.EMPI,staynumber order by en.Startdate,en.EndDate desc),b.DIS_DATE) as lastcoveragedate
		,Case 
			when en.StartDate<b.DIS_DATE then b.DIS_DATE
			else en.startdate 
		end as Startdate
	,case 
		when en.EndDate>dateadd(day,30,b.DIS_DATE) then dateadd(day,30,b.DIS_DATE)
		else en.EndDate 
	end as Finishdate
	,isnull(lead(en.Startdate,1) over(partition by b.EMPI,staynumber order by Startdate,EndDate),dateadd(day,30,b.DIS_DATE)) as nextcoveragedate
	from #trc_tmpsubscriber en
	join #trc_discharges b on 
		b.EMPI=en.EMPI
	where  
		en.Startdate<=dateadd(day,30,b.DIS_DATE) and 
		en.EndDate>=b.DIS_DATE
	--and b.Memid=101847
)
Insert into #trc_contenroll
select 
	t3.EMPI
	,t3.staynumber 
from
(
	select 
		t2.EMPI
		,t2.staynumber
		,sum(anchor) as anchor 
	from
	(
	
		select 
			*
			, case 
				when rn=1 and startdate>DIS_DATE then 1 
				else 0 
				end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
				end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when dateadd(day,30,DIS_DATE) between Startdate and newfinishdate then 1 
				else 0 
				end as anchor 
				from
				(
					Select 
						*
						,case 
							when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,staynumber order by Startdate,FinishDate),dateadd(day,30,DIS_DATE))
							else finishdate 
							end as newfinishdate
						,ROW_NUMBER() over(partition by EMPI,staynumber order by Startdate,Finishdate) as rn 
						from coverage_CTE1
				)t1           
		)t2  
		group by 
			EMPi
			,staynumber 
		having
			(sum(gaps)+sum(startgap)=0) and 
			sum(coveragedays)>=31
	)t3


update #trcdataset set CE=1 from #trcdataset ds join #trc_contenroll ce on ds.EMPI=ce.EMPI and SUBSTRING(ds.Meas,5,1)=char(64+ce.StayNumber)

-- Excluding Hospice patients
update #trcdataset set Rexcl=1 from #trcdataset ds join #trc_hospicemembers h on ds.EMPI=h.EMPI



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0 and payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #trcdataset

/*
	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,0 as Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0  THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #bcsdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #bcs_numdetails nd on d.EMPI=nd.EMPI
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type

*/

GO
