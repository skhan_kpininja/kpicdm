SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON








CREATE PROCEDURE [dbo].[KPI_HEDIS_URI]
AS
-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='URI-S'


DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_intakestartdt DATE;
DECLARE @ce_intakeenddt DATE;
DECLARE @meas VARCHAR(10);
Declare @ce_startdt2 Date;

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='URI';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_intakestartdt=concat(@meas_year-1,'-07-01');
SET @ce_intakeenddt=concat(@meas_year,'-06-30');

/*
Set @reporttype='Physician'
Set @measurename='Breast Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=85
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='22'
*/

-- Identify Eligible Population
-- 1. Identify Members who qualify for Hospice exclusion
drop table if exists #uri_hospicemembers;
CREATE table #uri_hospicemembers
(
	EMPI varchar(100)		
);
Insert into #uri_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)


--2. Event list	
-- Step 1.1 Identify all members who had an outpatient visit (Outpatient Value Set), a telephone visit (Telephone Visits Value Set), an e-visit or virtual check-in (Online Assessments Value Set) an observation visit (Observation Value Set) or an ED visit (ED Value Set) during the Intake Period, with a diagnosis of URI (URI Value Set). 

drop table if exists #uri_eventvisits
Create table #uri_eventvisits
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #uri_eventvisits
Select distinct
	*
From
(
	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_intakestartdt,@ce_intakeenddt,'Outpatient,Telephone Visits,Online Assessments,Observation,ED')
	where
		PROC_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1)

	Union all

	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_intakestartdt,@ce_intakeenddt,'Outpatient,Telephone Visits,ED')
	where
		DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1)

	Union all

	Select
		EMPI
		,FROM_DATE
		,CLAIM_ID
		,CL_DATA_SRC
	From GetRecordsByRevenueCodes(@rootId,@ce_intakestartdt,@ce_intakeenddt,'Outpatient,ED')
	where
		CL_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1)
)t1



--Event Step 1.2 Identify Members with URI diagnosis
drop table if exists #uri_diagnosis
Create table #uri_diagnosis
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #uri_diagnosis
Select
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_intakestartdt,@ce_intakeenddt,'URI')
where
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1)


--Event Step 2.1 Identify inpatient stays - Determine all URI Episode Dates. For each member identified in step 1, determine all outpatient, telephone, observation or ED visits, e-visits and virtual check-ins with a URI diagnosis. 
Drop table if exists #uri_inpatientstaylist
Create table #uri_inpatientstaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #uri_inpatientstaylist
Select
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,'1900-01-01',@ce_enddt)
Where
	CL_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)



-- Event Step 2.2 - Identify members with URI diagnosis on visits as per definition
--Exclude visits that result in an inpatient stay (Inpatient Stay Value Set).
drop table if exists #uri_diagnosiswithvisits
Create table #uri_diagnosiswithvisits
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #uri_diagnosiswithvisits
Select distinct
	d.*
From #uri_diagnosis d
Join #uri_eventvisits v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DATA_SRC=v.DATA_SRC
Left outer join #uri_inpatientstaylist ip on
	d.EMPI=ip.EMPI and
	(
		Datediff(day,d.FROM_DATE,ip.ADM_DATE) between 0 and 1
		or
		d.FROM_DATE between ip.ADM_DATE and ip.DIS_DATE
		or
		d.CLAIM_ID=ip.CLAIM_ID
	)
Where
	ip.EMPI is null


create nonclustered index Idx_#uri_diagnosiswithvisits on #uri_diagnosiswithvisits(EMPI)

-- Step 3 - Test for Negative Comorbid Condition History. Exclude Episode Dates when the member had a claim/encounter with any diagnosis for a comorbid condition during the 12 months prior to or on the Episode Date. A code from any of the following meets criteria for a comorbid condition: 
Drop table if exists #uri_comorbidconditions
Create Table #uri_comorbidconditions
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #uri_comorbidconditions
Select distinct
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt2,@ce_enddt,'HIV,HIV Type 2,Malignant Neoplasms,Other Malignant Neoplasm of Skin,Emphysema,COPD,Comorbid Conditions,Disorders of the Immune System')
Where
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)

create nonclustered index idx_#uri_comorbidconditions on #uri_comorbidconditions(EMPI)

-- Step 4 Test for Negative Medication History. Exclude Episode Dates where a new or refill prescription for an antibiotic medication (CWP Antibiotic Medications List) was filled 30 days prior to the Episode Date or was active on the Episode Date.
Drop Table if exists #uri_negmedhist
Create Table #uri_negmedhist
(
	EMPI varchar(100),
	FILL_DATE DATE,
	supplydays INT,
	CLAIM_ID varchar(100),
	MED_DATA_SRC varchar(50)
)
Insert into #uri_negmedhist
Select
	EMPI
	,FILL_DATE
	,cast(Cast(SUPPLY_DAYS as numeric) as INT) as supplydays
	,CLAIM_ID
	,MED_DATA_SRC
From MEDICATION
Where
	ROOT_COMPANIES_ID=@rootId and
	MED_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	MEDICATION_CODE in
	(
		Select code from HDS.Medication_list_to_Codes where Medication_list_name='CWP Antibiotic Medications'
	)

create nonclustered index idx_#uri_negmedhist on #uri_negmedhist(EMPI)

-- Step 5 Test for Negative Competing Diagnosis. Exclude Episode Dates where the member had a claim/encounter with a competing diagnosis on or three days after the Episode Date. A code from either of the following meets criteria for a competing diagnosis
Drop table if exists #uri_negcompetingdiagnosis
Create Table #uri_negcompetingdiagnosis
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #uri_negcompetingdiagnosis
Select distinct
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_intakestartdt,@ce_enddt,'Pharyngitis,Competing Diagnosis')
Where
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where Supplemental=1)
--	and EMPI=104974

create nonclustered index idx_#uri_negcompetingdiagnosis on #uri_negcompetingdiagnosis(EMPI)

-- Continuous Enrollment Step 6.1 - Create Temp Patient Enrollment
drop table if exists #uri_tmpsubscriber;
CREATE TABLE #uri_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	age INT,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date,
	rx INT,
	FROM_DATE Date

);
insert into #uri_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,d.FROM_DATE), Date_of_Birth) > d.FROM_DATE THEN datediff(year, Date_of_Birth, d.FROM_DATE) - 1
		ELSE datediff(year, Date_of_Birth, d.FROM_DATE)
	END as Age
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE
	,Case
		when en.DRUG_BENEFIT=1 or en.RX_UNITS=1 then 1
		else 0
	end as rx
	,d.FROM_DATE
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
Join #uri_diagnosiswithvisits d on
	d.EMPI=gm.EMPI_ID and
	en.EFF_DATE<=Dateadd(day,3,d.FROM_DATE) and
	en.TERM_DATE>=Dateadd(day,-30,d.FROM_DATE)
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	DATEDIFF(DAY,Date_of_Birth,d.FROM_DATE)>=90 and
	TERM_DATE>=@ce_startdt1
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;


--Check for continuous enrollment
--Step 6.2 Calculate continuous enrollment. The member must be continuously enrolled without a gap in coverage from 30 days prior to the Episode Date through 3 days after the Episode Date (34 total days).
drop table if exists #uri_contenroll;
create table #uri_contenroll
(
	EMPI varchar(50),
	FROM_DATE Date
);
With coverage_CTE1 as
(

	select 
		EMPI
		,FROM_DATE
		,isnull(lag(en.EndDate,1) over(partition by EMPI,FROM_DATE order by en.Startdate,en.EndDate desc),dateadd(day,-30,FROM_DATE)) as lastcoveragedate
		,Case 
			when en.StartDate<dateadd(day,-30,FROM_DATE) then dateadd(day,-30,FROM_DATE)
			else en.startdate 
		end as Startdate
	,case 
		when en.EndDate>dateadd(day,3,FROM_DATE) then dateadd(day,3,FROM_DATE)
		else en.EndDate 
	end as Finishdate
	,isnull(lead(en.Startdate,1) over(partition by EMPI,FROM_DATE order by Startdate,EndDate),dateadd(day,3,FROM_DATE)) as nextcoveragedate
	from #uri_tmpsubscriber en
	where  
		en.Startdate<=dateadd(day,3,FROM_DATE) and 
		en.EndDate>=dateadd(day,-30,FROM_DATE) and
		rx=1
		
	--and b.Memid=101847
)
Insert into #uri_contenroll
select 
	t3.EMPI
	,t3.FROM_DATE 
from
(
	select 
		t2.EMPI
		,t2.FROM_DATE
		,sum(anchor) as anchor 
	from
	(
	
		select 
			*
			, case 
				when rn=1 and startdate>FROM_DATE then 1 
				else 0 
				end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
				end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when dateadd(day,3,FROM_DATE) between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
			from
				(
					Select 
						*
						,case 
							when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,FROM_DATE order by Startdate,FinishDate),dateadd(day,3,FROM_DATE))
							else finishdate 
						end as newfinishdate
						,ROW_NUMBER() over(partition by EMPI,FROM_DATE order by Startdate,Finishdate) as rn 
					from coverage_CTE1
				)t1           
		)t2  
		group by 
			EMPi
			,FROM_DATE 
		having
			(sum(gaps)+sum(startgap)=0) and 
			sum(coveragedays)>=34
	)t3


create nonclustered index idx_#uri_contenroll_empi on #uri_contenroll(EMPI)

--Step 7 -- Deduplicate eligible episodes. If a member has more than one eligible episode in a 31-day period, include only the first eligible episode. For example, if a member has an eligible episode on January 1, include the January 1 visit and do not include eligible episodes that occur on or between January 2 and January 31; then, if applicable, include the next eligible episode that occurs on or after February 1. Identify visits chronologically, including only one per 31-day period. 
Drop table if exists #uri_baselist
Create table #uri_baselist
(
	EMPI varchar(100),
	FROM_DATE Date,
	meas varchar(10),
	staynumber INT
)
Insert into #uri_baselist
Select
	EMPI
	,FROM_DATE
	,concat('URI',char(64+row_number() over(partition By EMPI order by FROM_DATE))) as meas
	,row_number() over(partition By EMPI order by FROM_DATE) as StayNumber
From
(
	Select
		EMPI
		,FROM_DATE
		,CE
		,count(EMPI) over(partition by EMPI) as episodecount
	From
	(
		Select
			d.*
			,lag(d.FROM_DATE) over(partition by d.EMPI order by d.From_Date) as lastEpisodeDate
			,case
				when ce.EMPI is not null Then 1
				else 0
			end as CE
		From #uri_diagnosiswithvisits d
		join #uri_tmpsubscriber s on
			d.EMPI=s.EMPI and
			DATEDIFF(MONTH,s.dob,d.FROM_DATE)>=3
		left outer join #uri_comorbidconditions c on
			d.EMPI=c.EMPI and
			c.FROM_DATE between dateadd(MONTH,-12,d.FROM_DATE) and d.FROM_DATE
		left outer join #uri_negcompetingdiagnosis cd on
			d.EMPI=cd.EMPI and
			cd.FROM_DATE between d.FROM_DATE and Dateadd(day,3,d.FROM_DATE)
		Left outer join #uri_negmedhist m on
			d.EMPI=m.EMPI and
			(
				m.Fill_Date between dateadd(day,-30,d.FROM_DATE) and d.FROM_DATE
				or 
				d.FROM_DATE between FILL_DATE and DATEADD(day,supplydays,FILL_DATE)
			
			)
			and d.FROM_DATE!=m.FILL_DATE
		left outer join #uri_contenroll ce on
			d.EMPI=ce.EMPI and
			d.FROM_DATE=ce.FROM_DATE
		Where 
			c.EMPI is null and
			m.EMPI is null and
			cd.EMPI is null
			--and d.EMPi=100008
	)t1
	Where
		(
			lastEpisodeDate not between Dateadd(day,-31,FROM_DATE) and FROM_DATE
			or
			lastEpisodeDate is null
		)
		
)t2
Where
	(
		ce=1 and episodecount>1
	)
	or
	(
		episodecount=1 
	)


-- Create the data set as per hedis outpout format	
drop table if exists #uridataset;
CREATE TABLE #uridataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'

) ;
insert into #uridataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,meas
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,meas
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB')) then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI,meas order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI,meas order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,meas
				,payer
				,gender
				,age
			From
			(
				select 
					t.EMPI
					,b.meas
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by t.EMPI order by StartDate desc,EndDate Desc) as rn 
				from #uri_tmpsubscriber t
				join #uri_baselist b on
					t.EMPI=b.EMPI and
					t.FROM_DATE=b.FROM_DATE
				where  
					StartDate<=Dateadd(day,3,t.FROM_DATE)
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='URI'
Order by 1



--Numerator logic -Dispensed prescription for an antibiotic medication from the CWP Antibiotic Medications List on or 3 days after the Episode Date. 
Drop table if exists #uri_cwpmedicationlist
Create table #uri_cwpmedicationlist
(
	EMPI varchar(100),
	meas varchar(20)
)
Insert into #uri_cwpmedicationlist
Select
	m.EMPI
	,b.meas
From MEDICATION m
Join CLAIMLINE c on
	m.EMPI=c.EMPI and
	m.CLAIM_ID=c.CLAIM_ID and
	m.MED_DATA_SRC=c.CL_DATA_SRC
Join #uri_baselist b
	on m.EMPI=b.EMPI and
	m.FILL_DATE between b.FROM_DATE and DATEADD(day,3,b.FROM_DATE)
Where 
	m.ROOT_COMPANIES_ID=@rootId and
	m.MED_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
	MEDICATION_CODE in
	(
		Select Code from HDS.Medication_list_To_codes where Medication_List_Name='CWP Antibiotic Medications'
	)
	and
	ISNULL(c.SV_STAT,'O') in('P','A','O','')


-- Set numerator to 1
Update ds Set 
	ds.Num=1 
from #uridataset ds 
join #uri_cwpmedicationlist n on
	ds.EMPI=n.EMPI and
	ds.meas=n.meas


-- Set Continuous enrollment for members
Update ds Set 
	ds.CE=1 
from #uridataset ds 
Join #uri_baselist b on
	ds.EMPI=b.EMPI and
	ds.meas=b.meas
join #uri_contenroll ce on
	b.EMPI=ce.EMPI and
	b.FROM_DATE=ce.FROM_DATE


-- Set Exclusion for members
Update ds Set 
	ds.rexcl=1 
from #uridataset ds 
Join #uri_hospicemembers b on
	ds.EMPI=b.EMPI 
	

	

-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0  THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #uridataset

/*
	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,0 as Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0  THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #bcsdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #bcs_numdetails nd on d.EMPI=nd.EMPI
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type

*/

GO
