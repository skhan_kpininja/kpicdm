SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE PROCEDURE [dbo].[KPI_HEDIS_WCV]
AS
-- Declare Variables
declare @rundate Date='2022-02-01';
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='WCV-A'


DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @meas VARCHAR(10);

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='WCV';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');


/*
Set @reporttype='Physician'
Set @measurename='Breast Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=85
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='22'
*/



-- Eligible Patient List
drop table if exists #wcv_memlist; 
CREATE TABLE #wcv_memlist 
(
    EMPI varchar(100)
    
)
insert into #wcv_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt and 
	YEAR(@ce_enddt)-YEAR(Date_of_birth) between 3 and 21
ORDER BY 1;




-- Create Temp Patient Enrollment
drop table if exists #wcv_tmpsubscriber;
CREATE TABLE #wcv_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #wcv_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM open_empi_master gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_birth) between 3 and 21
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;



drop table if exists #wcvdataset;
CREATE TABLE #wcvdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'

) ;
insert into #wcvdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'WCV'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC') and nxtpayer in('MD','MLI','MRB','MDE'))  then TRIM(nxtpayer)
				When (Payer in('MCR','MCS','MP','MC') and prvpayer in('MD','MLI','MRB','MDE'))  then TRIM(prvpayer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('MCR','MCS','MP','MC','MR'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('MCR','MCS','MP','MC','MR'))  then TRIM(Payer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #wcv_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
					--and EMPI=115435
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='WCV'
Order by 1



-- Continuous Enrollment
Drop table if exists #wcv_contenroll
Create table #wcv_contenroll
(
	EMPI varchar(100)
)
Insert into #wcv_contenroll
select 
	EMPI
from GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0)

update ds set ds.CE=1 from #wcvdataset ds join #wcv_contenroll ce on ds.EMPI=ce.EMPI;

-- Identify Numerator
-- Identify all well care code
Drop table if exists #wcv_wellcarevisits
Create Table #wcv_wellcarevisits
(
	EMPI varchar(100),
	ServiceDate Date,
	ProviderId varchar(20),
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #wcv_wellcarevisits
Select distinct 
	*
From
(
	Select
		EMPI
		,PROC_START_DATE
		,ATT_NPI
		,CLAIM_ID
		,PROC_DATA_SRC
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Well-Care') 

	Union All

	Select
		EMPI
		,DIAG_START_DATE
		,ATT_NPI
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Well-Care') 

	Union all

	Select
		EMPI
		,VisitDate
		,VisitProviderNPI
		,'' as CLAIM_ID
		,Visit_Data_Src
	From GetVisits(@rootId,@ce_startdt,@ce_enddt,'Well-Care') 

)t1


--Identify wellcare visits with PCP and OBGYN
Drop table if exists #wcv_numeratorset
Create Table #wcv_numeratorset
(
	EMPI varchar(100)
)
Insert into #wcv_numeratorset
Select distinct 
	*
From
(
	Select
		v.EMPI
	from #wcv_wellcarevisits v
	join PROVIDER_FLAGS pf on
		v.ProviderId=pf.ProvID and
		pf.ROOT_COMPANIES_ID=@rootId
	Where
		'Y' in(pf.PCP,pf.OBGYN) and
		v.CLAIM_ID is not null
	
	Union all

	Select
		v.EMPI
	from #wcv_wellcarevisits v
	Join VISIT vst on
		v.EMPI=vst.EMPI and
		v.ServiceDate=vst.VisitDate and
		v.DATA_SRC=vst.Visit_Data_SRC and
		vst.ROOT_COMPANIES_ID=@rootId
	join PROVIDER_FLAGS pf on
		vst.VisitProviderNPI=pf.ProvID and
		pf.ROOT_COMPANIES_ID=@rootId
	Where 
		'Y' in(pf.PCP,pf.OBGYN) and
		v.CLAIM_ID is null
		--and v.EMPI=101000
)t1

update ds Set ds.Num=1 from #wcvdataset ds join #wcv_numeratorset n on ds.EMPI=n.EMPI


-- Identify hopsice members for exclusion
Drop table if exists #wcv_hospicemembers
Create Table #wcv_hospicemembers
(
	EMPI varchar(100)
)
Insert into #wcv_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)

update ds Set ds.Rexcl=1 from #wcvdataset ds join #wcv_hospicemembers n on ds.EMPI=n.EMPI


-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,0 as EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0 and Payer not in('MC','MCR','MCS','MP','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #wcvdataset

/*
	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,0 as Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0  THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #bcsdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #bcs_numdetails nd on d.EMPI=nd.EMPI
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type

*/

GO
