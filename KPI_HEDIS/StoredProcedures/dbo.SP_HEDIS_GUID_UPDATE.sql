SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
Create ProcEDURE [dbo].[SP_HEDIS_GUID_UPDATE] (@meas_year varchar(20))
AS
BEGIN
DECLARE @guid uniqueidentifier;
DECLARE @MEASURE_ID Varchar(100);
DECLARE	@SQL VARCHAR(5000);
Declare C Cursor
FOR SELECT Measure_ID from [HDS].[HEDIS_MEASURES_LIST] where GUID is null;
OPEN C
FETCH NEXT FROM C into @Measure_ID
WHILE @@FETCH_STATUS = 0
    BEGIN
	set @guid = NEWID()
	Print @MEASURE_ID
	Print @GUID
	Update [HDS].[HEDIS_MEASURES_LIST] SET [GUID]=@guid, Measure_Version=@meas_year WHERE Measure_ID=@MEASURE_ID
FETCH NEXT FROM C INTO @Measure_ID
END
CLOSE C
DEALLOCATE C
END
GO
