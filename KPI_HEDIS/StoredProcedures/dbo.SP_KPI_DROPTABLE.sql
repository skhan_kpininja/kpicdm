SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
Create PROCEDURE [dbo].[SP_KPI_DROPTABLE] (@tableName VARCHAR(255))

AS

BEGIN

    DECLARE @SQL VARCHAR(MAX);
	IF OBJECT_ID(@tableName, 'u') IS NOT NULL 
	Set @SQL='DROP TABLE ' + @tableName;
	EXEC (@SQL);
	
	IF LEFT(@tableName,1)='#'
	IF OBJECT_ID('tempdb..'+@tableName, 'u') IS NOT NULL 
	Set @SQL='DROP TABLE ' + @tableName;
	EXEC (@SQL);
END

GO
