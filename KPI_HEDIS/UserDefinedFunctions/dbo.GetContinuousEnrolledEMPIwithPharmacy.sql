SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

Create Function [dbo].[GetContinuousEnrolledEMPIwithPharmacy]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
	,@allowablegap INT
	,@anchorflag INT
)
RETURNS TABLE AS
RETURN 
(
	With coverage_CTE1 (EMPI,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
	(

		select distinct
			EMPI
			,isnull(lag(TERM_DATE,1) over(partition by EMPI order by EFF_DATE,TERM_DATE desc),@startdate) as lastcoveragedate
			,Case 
				when EFF_DATE<@startdate then @startdate
				else EFF_DATE 
			end as Startdate
			,case 
				when TERM_DATE>@enddate then @enddate
				else TERM_DATE 
			end as Finishdate
			,isnull(lead(EFF_DATE,1) over(partition by EMPI order by EFF_DATE,TERM_DATE),@enddate) as nextcoveragedate  
		from 
		(
			Select distinct 
				EMPI
				,EFF_DATE
				,TERM_DATE
				,Rank() over(partition by EMPI order by EFF_Date,Term_Date) as rn
			From ENROLLMENT 
		where 
			ROOT_COMPANIES_ID=@rootId and
			DRUG_BENEFIT=1 and
			EFF_DATE<=@enddate and 
			TERM_DATE>=@startdate
		)t1
						        
	)
	select 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select 
				*
				,case 
					when rn=1 and startdate>@startdate then 1 
					else 0 
				end as startgap
				,case 
					when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 end as gaps
				,datediff(day,Startdate,Finishdate)+1 as coveragedays
				,case 
					when @enddate between Startdate and newfinishdate then 1 
					else 0 
				end as anchor 
			from
			(
				Select 
					*
					,case 
						when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI order by Startdate,FinishDate),@enddate) 
						else finishdate 
					end as newfinishdate
					,ROW_NUMBER() over(partition by EMPI order by Startdate,Finishdate) as rn 
				from coverage_CTE1
			)t1           
		)t2  
		group by 
			EMPI 
		having
			(sum(gaps)+sum(startgap))<=1 and 
			sum(coveragedays)>=((datediff(day,@startdate,@enddate)+1)-@allowablegap) and 
			(sum(anchor)+@anchorflag)>=1
	)t3
)


GO
