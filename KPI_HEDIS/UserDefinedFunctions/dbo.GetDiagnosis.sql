SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE FUNCTION [dbo].[GetDiagnosis]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
	,@valuesetname varchar(300)
)
RETURNS TABLE AS
RETURN 
(
		select 
			d.EMPI
			,d.DIAG_START_DATE
			,d.CLAIM_ID
			,d.DIAG_CODE
			,d.DIAG_SEQ_NO
			,DIAG_DATA_SRC
			,c.ATT_NPI
			,c.SV_STAT
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and 
			DIAG_START_DATE between @startdate and @enddate	and
			DIAG_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where code_system in('ICD9CM','ICD10CM','SNOMED CT US Edition') and Value_Set_Name in (SELECT value FROM STRING_SPLIT( @valuesetname, ','))
			)
	


)

GO
