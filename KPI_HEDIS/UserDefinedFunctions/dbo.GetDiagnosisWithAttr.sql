SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE FUNCTION [dbo].[GetDiagnosisWithAttr]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
	,@valuesetname varchar(300)
	,@attrvaluesetname varchar(200)
)
RETURNS TABLE AS
RETURN 
(
		select 
			d.EMPI
			,d.DIAG_START_DATE
			,d.CLAIM_ID
			,d.DIAG_CODE
			,d.DIAG_SEQ_NO
			,DIAG_DATA_SRC
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and 
			DIAG_START_DATE between @startdate and @enddate	and
			DIAG_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name=@valuesetname
			)
			and
			ATTR_CODE in
			(
				select Code_New from HDS.VALUESET_TO_CODE where Value_Set_Name=@attrvaluesetname
			)
	


)

GO
