SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


Create FUNCTION [dbo].GetOREC
(
	@rootId varchar(10)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	Select
		EMPI
		,OREC
	From
	(
		select 
			EMPI
			,OREC
			,row_number() over (Partition by EMPI order by RunDate Desc,PayDate Desc) as rn
		From MCFIELDS
		Where
			ROOT_COMPANIES_ID=@rootId and
			RunDate between @startdate and @enddate and
			OREC is not null
	)t1
	Where
		rn=1
			   
)

GO
