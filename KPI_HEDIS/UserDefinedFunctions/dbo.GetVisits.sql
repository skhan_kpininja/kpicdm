SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





Create FUNCTION [dbo].[GetVisits]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
	,@valuesetname varchar(300)
)
RETURNS TABLE AS
RETURN 
(
	
		
		select distinct
			EMPI
			,VisitId
			,VisitDate
			,VisitTypeCode
			,VisitProviderNPI
			,Visit_DATA_SRC
		from Visit 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			VisitDate between @startdate and @enddate	and
			VisitTypeCode in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in (SELECT value FROM STRING_SPLIT( @valuesetname, ','))
			)
	
)

GO
