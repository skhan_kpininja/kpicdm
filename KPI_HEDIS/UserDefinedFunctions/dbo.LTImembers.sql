SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE FUNCTION [dbo].[LTImembers]
(
	@rootId varchar(10)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
		select distinct
			EMPI
		from MCFIELDS 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			Rundate!='' and 
			Rundate between @startdate and @enddate and 
			LTI=1
				
	

)

GO
