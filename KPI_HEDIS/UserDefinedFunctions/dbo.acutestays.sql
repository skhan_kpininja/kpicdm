SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



Create FUNCTION [dbo].[acutestays]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	with cte_inpatientstay(EMPI,FROM_DATE,ADM_DATE,DIS_DATE,CLAIM_ID,CL_DATA_SRC,SV_STAT) as
	(
		select 
			EMPI
			,FROM_DATE
			,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
			,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
			,CLAIM_ID
			,CL_DATA_SRC
			,SV_STAT
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			Coalesce(DIS_DATE,TO_DATE) between @startdate and @enddate and
			ISNULL(POS,'')!='81' and 
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
			)
	),
	cte_nonacutestay(EMPI,CLAIM_ID,CL_DATA_SRC) as
	(
		select 
			EMPI
			,CLAIM_ID 
			,CL_DATA_SRC
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			Coalesce(DIS_DATE,TO_DATE) between @startdate and @enddate and
			ISNULL(POS,'')!='81' and 
			(
				REV_CODE in 
				(
					select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
				) 
				or 
				RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
				(
					select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
				)
			)
	)
	Select
		ip.*
	From cte_inpatientstay ip
	left outer join cte_nonacutestay na on
		ip.EMPI=na.EMPI and
		ip.CLAIM_ID=na.CLAIM_ID and
		ip.CL_DATA_SRC=na.CL_DATA_SRC
	Where
		na.EMPI is null

)

GO
