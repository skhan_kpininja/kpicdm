SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE FUNCTION [dbo].[advancedillness]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	select distinct 
		d.EMPI
		,DIAG_START_DATE
		,DIAG_CODE
		,d.CLAIM_ID 
		,d.DIAG_DATA_SRC
	from DIAGNOSIS d
	Left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
								   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
								   d.EMPI=c.EMPI
	where 
		d.ROOT_COMPANIES_ID=@rootId and 
		ISNULL(c.POS,'')!='81' and 
		DIAG_START_DATE between @startdate and @enddate and 
		DIAG_CODE in(select Code_New from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')

)

GO
