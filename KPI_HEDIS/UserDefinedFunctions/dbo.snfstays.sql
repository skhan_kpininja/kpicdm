SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




CREATE FUNCTION [dbo].[snfstays]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	select 
		EMPI
		,FROM_DATE
		,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
		,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
		,CLAIM_ID
		,CL_DATA_SRC
		,SV_STAT
		,ATT_NPI
	from CLAIMLINE 
	where 
		ROOT_COMPANIES_ID=@rootId and
		Coalesce(DIS_DATE,TO_DATE) between @startdate and @enddate and
		ISNULL(POS,'')!='81' and 
		(
			REV_CODE in 
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Skilled Nursing Stay')
			) 
			or 
			RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Skilled Nursing Stay')
			)
		)
	
		


)

GO
