SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE   PROC [dbo].[GenerateUHNAttributionReport]
As

/******	Change Log: 
	2021-11-04 | KPISQL-504 | Added 92002, 92004, 92012, 92014 CPT codes to Ambulatory Specialists Logic as per Marty's request 
	2021-11-08 | KPISQL-518 | Added PoS 22 to Ambulatory Specialists Logic as per Marty's request
	CR-2021-11-24 | KPISQL-537 | Change Attribution logic to include Admission and Discharge date for Hospitalist and Inpatient Specialist|
	CR-2021-11-24 | KPISQL-537 | Change attribution logic to mark member as active based n the latest enrollment file.
--test comment
******/


BEGIN

SET NOCOUNT ON;

DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

declare @rundate Date=GetDate();
--SG: Added for Looping the logic for multiple quarters
--**Start
DECLARE @Loops int = 
		(
			SELECT LOOPS FROM DBO.AttributionRunSchedule
			WHERE Month = DATEPART(MM, @RUNDATE)
		)
		DECLARE @counter int = 1
		DECLARE @rundate2 date = 
		CASE 
			WHEN @LOOPS = 1 then NULL
			WHEN @LOOPS = 2 THEN DATEADD(dd,-1,DATEADD(yy,DATEDIFF(yy,0,@RUNDATE),0))
			ELSE getdate()
		END
--**End

WHILE @counter<=@Loops
BEGIN

IF @COUNTER = 2
	BEGIN
		SET @rundate=@rundate2;
	END

declare @meas_year varchar(4)=Year(@rundate)
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, @rundate), 0)
declare @ce_enddt Date=eomonth(@rundate);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


-- Logic to populate the Member Month Snaphot

Truncate table MemberMonth_snapshot;
Insert into  MemberMonth_snapshot
Select distinct
	EMPI
	,MEMBER_ID
	,MEMBER_MONTH_START_DATE
	,MM_DATA_SRC
From KPI_ENGINE..MEMBER_MONTH
Where
	ROOT_COMPANIES_ID=@rootId and
	MEMBER_MONTH_START_DATE = @attributiondate

--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))


-- PCP Attribution
-- Idnetify Members who has the specified CPT billed in last 2 years

Drop table if exists #amb_pcp_procs;
Create table #amb_pcp_procs
(
	CLAIM_ID varchar(100),
	EMPI varchar(50),
	MEMBER_ID varchar(100),
	PROC_DATA_SRC varchar(50)
)
Insert into #amb_pcp_procs
select distinct
	p.CLAIM_ID
	,p.EMPI
	,p.MEMBER_ID
	,p.PROC_DATA_SRC 
from KPI_ENGINE.dbo.PROCEDURES p
where 
	p.root_companies_id=@rootid and
	p.proc_code in('G0402','G0438','G0439','G2010','G2061','G2062','G2063','99201','99202','99203','99204','99205',' 99211','99212','99213','99214','99215','99241','99242','99243','99244','99245','99304','99305','99306','99307','99308','99309','99310','99381','99382','99383','99384','99385','99386','99387','99391','99392','99393','99394','99395','99396','99397','99421','99422','99423')
	and 
	p.PROC_START_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@ce_enddt)), 0) and @ce_enddt

	Create nonclustered index idx_amb_pcp_procs on #amb_pcp_procs(CLAIM_ID,EMPI,MEMBER_ID,PROC_DATA_SRC)

	-- Identify Members who were seen at specified POS in last 2 years
	Drop table if exists #amb_pcp_vsts
	Create table #amb_pcp_vsts
	(
		CLAIM_ID varchar(100),
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		CL_DATA_SRC varchar(50),
		ATT_NPI varchar(20),
		ATT_PROV_NAME varchar(200),
		FROM_DATE date,
		BILL_PROV varchar(20),
		BILL_PROV_NAME varchar(200),
		BILL_PROV_TIN varchar(20)
	)
	Insert into #amb_pcp_vsts
	select distinct
		cl.CLAIM_ID
		,cl.EMPI
		,cl.MEMBER_ID
		,cl.CL_DATA_SRC
		,cl.ATT_NPI
		,cl.ATT_PROV_NAME
		,FROM_DATE 
		,cl.BILL_PROV
		,cl.BILL_PROV_NAME
		,cl.BILL_PROV_TIN
	from KPI_ENGINE.dbo.CLAIMLINE cl 
	where 
		cl.root_companies_id=@rootid and
		POS in('02','11','12','13','14','31','32','34','50') 
		and 
		cl.FROM_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@ce_enddt)), 0) and @ce_enddt
	
	Create nonclustered index idx_amb_pcp_vsts on #amb_pcp_vsts(CLAIM_ID,EMPI,MEMBER_ID,CL_DATA_SRC,ATT_NPI,FROM_DATE)
	
	
	-- Identify visits with Physician Roster
	Drop table if exists #amp_pcp_ntwk_phy_vsts;
	Create table #amp_pcp_ntwk_phy_vsts
	(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		FROM_DATE Date,
		ATT_NPI varchar(20),
		CL_DATA_SRC varchar(50),
		PROV_NAME varchar(200),
		PROV_SPEC_DESC varchar(100),
		PROV_GRP_NAME varchar(200)
	)
	Insert into #amp_pcp_ntwk_phy_vsts
	select distinct 
		p.EMPI,
		p.MEMBER_ID,
		p.PROC_START_DATE,
		c.ATT_NPI,
		p.PROC_DATA_SRC,
		pr.PROV_NAME,
		pr.PROV_SPEC_DESC2,
		pr.PROV_GRP_NAME 
	From KPI_ENGINE.dbo.PROCEDURES p
	Join KPI_ENGINE.dbo.CLAIMLINE c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.PROC_DATA_SRC=c.CL_DATA_SRC
	join KPI_ENGINE.dbo.PROVIDER pr on c.ATT_NPI=pr.PROV_NPI 
	where
		p.ROOT_COMPANIES_ID=159 and
		pr._PROV_UDF_02_='Y' and
		pr._PROV_UDF_04_='Primary Address' and
		pr.PROV_SPEC_CODE2 in('207Q00000X','207R00000X','208D00000X','208000000X') and
		p.PROC_START_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@ce_enddt)), 0) and @ce_enddt and
		ISNULL(c.POS,'0') in('02','11','12','13','14','31','32','34','50') and
		p.proc_code in('G0402','G0438','G0439','G2010','G2061','G2062','G2063','99201','99202','99203','99204','99205',' 99211','99212','99213','99214','99215','99241','99242','99243','99244','99245','99304','99305','99306','99307','99308','99309','99310','99381','99382','99383','99384','99385','99386','99387','99391','99392','99393','99394','99395','99396','99397','99421','99422','99423')	

	Create nonclustered index idx_amp_pcp_ntwk_phy_vsts on #amp_pcp_ntwk_phy_vsts(EMPI,MEMBER_ID,FROM_DATE,ATT_NPI,CL_DATA_SRC,PROV_NAME,PROV_SPEC_DESC,PROV_GRP_NAME)

	-- Apply Ambulatory PCP Logic
	Drop table if exists #amb_pcp_attribution;
	Create table #amb_pcp_attribution
	(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		AMB_PCP_NPI varchar(20),
		AMB_PCP_NAME varchar(200),
		AMB_PCP_SPECIALTY varchar(200),
		AMB_PCP_PRACTICE varchar(200),
		AMB_PCP_VisitCount INT,
		AMB_PCP_RECENT_VISIT Date,
		DATA_SOURCE varchar(50)

	)
	Insert into #amb_pcp_attribution
	select distinct 
		EMPI
		,Member_id
		,ATT_NPI
		,PROV_NAME
		,PROV_SPEC_DESC
		,PROV_GRP_NAME
		,twoyrct as vst_ct
		,FROM_DATE as RCNT_VST 
		,CL_DATA_SRC
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by twoyrct desc,oneyrct desc,from_date desc,SourcePriority Asc,rn desc) as rnk 
		from(
			select 
				*
				,count(*) over(partition by EMPI,ATT_NPI) as twoyrct
				,count(case when FROM_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@ce_enddt)), 0) and @ce_enddt then 1 else null end) over(partition by EMPI,ATT_NPI) as oneyrct 
				,Case 
					when CL_DATA_SRC='MSSP' then 5
					when CL_DATA_SRC='HUMANA' then 1
					when CL_DATA_SRC='CIGNA' then 2
					when CL_DATA_SRC='BCBS-UHS' then 3
					when CL_DATA_SRC='BRIGHT' then 4
					when CL_DATA_SRC='MEDCO' then 6
					when CL_DATA_SRC='FlatFiles' then 7
				end as SourcePriority
				,ROW_NUMBER() over(partition by EMPI order by FROM_DATE) as rn
			from #amp_pcp_ntwk_phy_vsts
			
		)t1
	)t2 where rnk=1


	Create nonclustered index idx_amb_pcp_attribution on #amb_pcp_attribution(EMPI,MEMBER_ID,AMB_PCP_NPI,AMB_PCP_NAME,AMB_PCP_PRACTICE,AMB_PCP_VisitCount,AMB_PCP_RECENT_VISIT)

	

	-- Identify all Active and Assigned Members
	Drop table if exists #members
	Create Table #members
	(
		EMPI varchar(100),
		PayerId varchar(100),
		AlternateMemberId varchar(100),
		Medicare_Id varchar(100),
		MEMBER_LAST_NAME varchar(100),
		MEMBER_FIRST_NAME varchar(100),
		MEMBER_MIDDLE_NAME varchar(50),
		MEMBER_DOB Date,
		MEM_GENDER varchar(10),
		EnrollmentStatus varchar(20),
		AssignedStatus varchar(20),
		DATA_SOURCE varchar(50)
	)
	Insert into #members
	Select 
		EMPI
		,Payerid
		,AlternateMemberId
		,MEDICARE_ID
		,MEM_LNAME
		,MEM_FNAME
		,MEM_MNAME
		,MEM_DOB
		,MEM_GENDER
		,Enrollment_Status
		,AssignedStatus
		,Data_Source 
	from
	(
		Select 
			Dense_rank() over(partition by EMPI order by Enrollment_Status ASC,_MEM_UDF_03_ desc,PayerPriority ASC,FileDate Desc,ST_MEM_ID Desc) as rnk
			,* 
		from(
				select distinct
					o.EMPI_ID as EMPI
					,o.Org_Patient_Extension_ID as Payerid
					,ISNULL(m._MEM_UDF_02_,'') as AlternateMemberId
					,ISNULL(m.MEM_MEDICARE,'') as MEDICARE_ID
					,UPPER(Mem_lname) as MEM_LNAME
					,UPPER(Mem_fname) as MEM_FNAME
					,ISNULL(UPPER(Mem_mname),'') as MEM_MNAME
					,convert(varchar,cast(MEM_DOB as date),110) as MEM_DOB
					,MEM_GENDER
					,case 
						when _MEM_UDF_03_=0 then 'InActive'  --CR-2021-12-14 | Adding logic to mark Assignable members as inactive based on Marty's request on 12-13-2021
						when en.Member_id is not null then 'Active' 
						else 'InActive' 
					end as Enrollment_Status 
					,case
						when _MEM_UDF_03_=1 Then 'Assigned'
						else 'Assignable'
					end as AssignedStatus
					,_MEM_UDF_03_
					,o.Data_Source
					,Case
						when MEM_DATA_SRC='MSSP' Then 5
						when MEM_DATA_SRC='HUMANA' Then 4
						when MEM_DATA_SRC='CIGNA' Then 1
						when MEM_DATA_SRC='BCBS-UHS' Then 2
						when MEM_DATA_SRC='BRIGHT' Then 3
						when MEM_DATA_SRC='MEDCO' Then 6
						when MEM_DATA_SRC='FlatFiles' Then 8
						when MEM_DATA_SRC='EXPRESSSCRIPTS' Then 7
					end as PayerPriority
					,m.FileDate
					,m.ST_MEM_ID
				from KPI_ENGINE.dbo.open_empi o
				join KPI_ENGINE.dbo.MEMBER m on 
					o.Org_Patient_Extension_ID=m.MEMBER_ID
				-- CR-20[dbo].[MEMBER_MONTH]21-11-23 - Adding logic to mark member as active if it is present in latest eligibility file
				Left outer join
				(
					
					Select distinct
						en.Member_id,
						EN_DATA_SRC
					From KPI_ENGINE.dbo.Enrollment en
					Where en.Filedate=(select max(Filedate) from KPI_ENGINE.dbo.Enrollment mm1 where mm1.EN_DATA_SRC=en.EN_DATA_SRC)

				)en on o.Org_patient_extension_id=en.Member_id
				/*
				left outer join KPI_ENGINE.dbo.MEMBER_MONTH e on 
					o.EMPI_ID=e.EMPI  and 
					o.Data_Source=e.MM_DATA_SRC and
					e.MEMBER_MONTH_START_DATE= DATEADD(month, DATEDIFF(month, 0, @ce_enddt), 0)
					*/

				--where m.EMPI='EF0C5153-9B12-452E-A705-E5DBD7F2FB0C'
	
			)t1
		)t2 where rnk=1

	Create nonclustered index idx_member on #members(EMPI)

		
	-- Hospitalist PCP Attribution Logic

Drop table if exists #uhn_hospstays;
select distinct
	CLAIM_ID
	,EMPI
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CL_DATA_SRC 
	into #uhn_hospstays
from KPI_ENGINE.dbo.Claimline
where
	ROOT_COMPANIES_ID=@rootId and
	--Coalesce(DIS_DATE,TO_DATE) between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@ce_enddt)), 0) and @ce_enddt and
	Coalesce(DIS_DATE,TO_DATE) between DATEADD(yy, DATEDIFF(yy, 0, @ce_enddt), 0) and @ce_enddt and
	--2021-10-29: (SG) Changed the from date from DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@ce_enddt)), 0) to DATEADD(yy, DATEDIFF(yy, 0, @ce_enddt), 0) as requested by stefanie
	REV_CODE in
	(
		select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay','Observation Stay')
	)
	and
	(
		'1538164090' in(BILL_PROV,ATT_NPI) 
		or 
		'1346278025' in(BILL_PROV,ATT_NPI)
	)


Drop table if exists #uhn_hospstays_directtransfers;
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #uhn_hospstays
  

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
	into #uhn_hospstays_directtransfers
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1






-- Create a normalized transfer table to improve lookup
Drop table if exists #uhn_hospstays_directtransfers_normalized
select 
	*,
	row_number() over(Partition by EMPI order by AdmissionDate,dischargedate) as Staynumber
	into #uhn_hospstays_directtransfers_normalized
from 
(
	select 
		* 
	from #uhn_hospstays_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid,',') ) d
	) src
	pivot 
		(
			max(value) for src.RowN in([1],[2],[3],[4],[5])
		) p



	Drop table if exists #hosp_pcp_procedures;
	select distinct 
		cl.EMPI
		,p.PROC_START_DATE
		,cl.ATT_NPI
		,cl.CL_DATA_SRC
		,pr.PROV_NAME
		,pr.PROV_GRP_NAME
		,PROV_SPEC_DESC2 
		into #hosp_pcp_procedures
	from KPI_ENGINE.dbo.Claimline cl
	join KPI_ENGINE.dbo.PROVIDER pr on 
		cl.ATT_NPI=pr.PROV_NPI and 
		pr.PROV_SPEC_CODE2='208M00000X' and 
		pr._PROV_UDF_04_='Primary Address' and
		pr._PROV_UDF_02_='Y'
	join KPI_ENGINE.dbo.PROCEDURES p on 
		cl.EMPI=p.EMPI and
		cl.Claim_id=p.Claim_id and 
		ISNULL(cl.SV_LINE,0)=ISNULL(p.SV_LINE,0) and 
		cl.CL_DATA_SRC=p.PROC_DATA_SRC and 
		p.PROC_CODE in('99221','99222','99223','99231','99232','99233')
	where 
		cl.POS='21'


	Drop table if exists #hosp_pcp_visits;
	Select distinct
		p.EMPI,
		AdmissionDate,
		DischargeDate,
		Staynumber,
		PROC_START_DATE,
		ATT_NPI,
		CL_DATA_SRC,
		PROV_NAME,
		PROV_GRP_NAME,
		PROV_SPEC_DESC2
		into #hosp_pcp_visits
	from #uhn_hospstays_directtransfers_normalized s
	join #hosp_pcp_procedures p on
		s.EMPI=p.EMPI and
		p.PROC_START_DATE between s.AdmissionDate and s.DischargeDate


-- CR-2021-11-23 : Updated logic to include Admission and Discharge Date for Hospitalist 
	Drop table if exists #hosp_pcp_attribution
	Create table #hosp_pcp_attribution
	(
		EMPI varchar(100),
		AdmissionDate Date,
		DischargeDate date,
		HOSP_PCP_NPI varchar(20),
		HOSP_PCP_NAME varchar(200),
		HOSP_PCP_SPECIALTY varchar(100),
		HOSP_PCP_PRACTICE varchar(200),
		HOSP_PCP_VisitCount INT,
		HOSP_PCP_RECENT_VISIT Date,
		CL_DATA_SRC varchar(50),
		rank int
	)
	Insert into #hosp_pcp_attribution
	select distinct 
		EMPI
		,AdmissionDate
		,DischargeDate
		,ATT_NPI
		,PROV_NAME
		,PROV_SPEC_DESC2
		,PROV_GRP_NAME
		,ct as VST_CT
		,PROC_START_DATE as RCNT_VST
		,CL_DATA_SRC 
		,ROW_NUMBER() over(Partition By EMPI order by DischargeDate desc) as rank
	from
	(
		select 
			*
			,DENSE_RANK() over(partition by EMPI,DischargeDate order by ct desc,Proc_start_date desc,SourcePriority Asc,rn desc) as rnk 
		from(

	
			select 
				*
				,count(*) over(partition by EMPI,ATT_NPI) as ct
				,Case
					when CL_DATA_SRC='MSSP' Then 1
					when CL_DATA_SRC='HUMANA' Then 2
					when CL_DATA_SRC='CIGNA' Then 3
					when CL_DATA_SRC='BCBS-UHS' Then 4
					when CL_DATA_SRC='BRIGHT' Then 5
					when CL_DATA_SRC='MEDCO' Then 6
					when CL_DATA_SRC='FlatFiles' Then 7
				end as SourcePriority
				,ROW_NUMBER() over(partition by EMPI,DischargeDate order by Proc_start_date desc) as rn
			from #hosp_pcp_visits
		--	where empi='C4CC4A41-90DC-4766-87C0-00C0307AF99C'
	
		)t2
	)t3 where rnk=1

	

	
	Create nonclustered index idx_hosp_pcp_attribution on #hosp_pcp_attribution(EMPI)

	-- Logic to idnetify Ambulatory Specialists

	
	Drop table if exists #AMB_SPEC_VSTS_NTWK_PHYS
	Create table #AMB_SPEC_VSTS_NTWK_PHYS
	(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		FROM_DATE Date,
		ATT_NPI varchar(20),
		CL_DATA_SRC varchar(50),
		PROV_NAME varchar(200),
		PROV_SPEC_DESC varchar(100),
		PROV_GRP_NAME varchar(200)
	)
	Insert into #AMB_SPEC_VSTS_NTWK_PHYS
	select distinct 
		p.EMPI,
		p.MEMBER_ID,
		p.PROC_START_DATE,
		c.ATT_NPI,
		p.PROC_DATA_SRC,
		pr.PROV_NAME,
		pr.PROV_SPEC_DESC2,
		pr.PROV_GRP_NAME 
	From KPI_ENGINE.dbo.PROCEDURES p
	Join KPI_ENGINE.dbo.CLAIMLINE c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.PROC_DATA_SRC=c.CL_DATA_SRC
	join KPI_ENGINE.dbo.PROVIDER pr on c.ATT_NPI=pr.PROV_NPI 
	where
		p.ROOT_COMPANIES_ID=@rootId and
		--ISNULL(c.POS,'0') in('11','12','13','14','31','32','34','02') and 
		ISNULL(c.POS,'0') in('11','12','13','14','31','32','34','02','22') and 
--2021-11-08: (SG) KPISQL-518 | Added PoS 22 as per Marty's request
		p.PROC_START_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@ce_enddt)), 0) and @ce_enddt and
		p.proc_code in('G0402','G0438','G0439','G2010','G2061','G2062','G2063','99201','99202','99203','99204','99205',' 99211','99212','99213','99214','99215','99304','99305','99306','99307','99308','99309','99310','99341','99342','99343','99344','99345','99347','99348','99349','99350','99381','99382','99383','99384','99385','99386','99387','99421','99422','99423','92002', '92004', '92012', '92014') and
--2021-11-04: (SG) KPISQL-504 | Added 92002, 92004, 92012, 92014 as per Marty's request
		pr._PROV_UDF_02_='Y' and
		pr._PROV_UDF_04_='Primary Address' and
		pr.PROV_SPEC_CODE2 in
			(
					'207RC0001X','207RC0000X','207RC0000X','207RG0100X','207VG0400X','207RI0011X','207RN0300X','207V00000X','207W00000X','152W00000X','207RP1001X','207RR0500X','176B00000X'
			)
	--2021-10-29: (SG) Added 176B00000X in the spec code and 02 in the PoS as per Steff's request
	

	Create nonclustered index idx_AMB_SPEC_VSTS_NTWK_PHYS on #AMB_SPEC_VSTS_NTWK_PHYS(EMPI,MEMBER_ID,FROM_DATE,ATT_NPI,CL_DATA_SRC,PROV_NAME,PROV_SPEC_DESC)

	-- Apply Ambulatory Specialist logic

	Drop table if exists #AMB_SPEC_ATTRIBUTION
	Create table #AMB_SPEC_ATTRIBUTION
	(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		AMB_SPEC_NPI varchar(20),
		AMB_SPEC_NAME varchar(200),
		AMB_SPEC_SPECIALTY varchar(100),
		AMB_SPEC_PRACTICE_NAME varchar(200),
		AMB_SPEC_VISIT_COUNT INT,
		AMB_SPEC_RECENT_VISIT DATE,
		SpecialistCount INT

	)
	Insert into #AMB_SPEC_ATTRIBUTION
	select distinct 
		EMPI
		,Member_id
		,ATT_NPI as NPI
		,PROV_NAME as PROVIDERNAME
		,PROV_SPEC_DESC as Specialty
		,PROV_GRP_NAME
		,twoyrct
		,FROM_DATE
		,DENSE_RANK() over(partition by EMPI order by PROV_SPEC_DESC) as SpecialistCount
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI,PROV_SPEC_DESC order by twoyrct desc,oneyrct desc,from_date desc,SourcePriority asc,rn desc) as rnk
		from(
			select 
				*
				,count(*) over(partition by EMPI,ATT_NPI) as twoyrct
				,count(case when FROM_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@ce_enddt)), 0) and @ce_enddt then 1 else null end) over(partition by EMPI,ATT_NPI) as oneyrct 
				,Case 
					when CL_DATA_SRC='MSSP' then 1
					when CL_DATA_SRC='HUMANA' then 2
					when CL_DATA_SRC='CIGNA' then 3
					when CL_DATA_SRC='BCBS-UHS' then 4
					when CL_DATA_SRC='BRIGHT' then 5
					when CL_DATA_SRC='MEDCO' then 6
				end as SourcePriority
				,ROW_NUMBER() over(partition by EMPI order by FROM_date desc) as rn
				from #AMB_SPEC_VSTS_NTWK_PHYS
			--	where EMPI='E62AEAF1-ECAD-4222-995E-E1F588414B2E'
		)t1
	)t2 where rnk=1

	create nonclustered index idx_AMB_SPEC_ATTRIBUTION on #AMB_SPEC_ATTRIBUTION(EMPI,SpecialistCount)


	-- Inpatient Surgical Specialist Attribution

	-- Identify visits with Billed CPTs ('99221','99222','99223','99231','99232','99233')

	Drop table if exists #HOSP_SPEC_PROCS
	Create table #HOSP_SPEC_PROCS
	(
		EMPI varchar(100),
		FROM_DATE Date,
		ATT_NPI varchar(20),
		CL_DATA_SRC varchar(50),
		PROV_NAME varchar(200),
		PROV_SPEC_DESC varchar(100),
		PROV_GRP_NAME varchar(200)
	)
	Insert into #HOSP_SPEC_PROCS
	select distinct 
		cl.EMPI
		,p.PROC_START_DATE
		,cl.ATT_NPI
		,cl.CL_DATA_SRC
		,pr.PROV_NAME
		,pr.PROV_SPEC_DESC2
		,pr.PROV_GRP_NAME 
	from KPI_ENGINE.dbo.Claimline cl
	join KPI_ENGINE.dbo.PROVIDER pr on 
		cl.ATT_NPI=pr.PROV_NPI and 
		pr.PROV_SPEC_CODE2 in('207L00000X','208G00000X','208C00000X','208600000X','207T00000X','2086S0129X') and 
		pr._PROV_UDF_04_='Primary Address' and
		pr._PROV_UDF_02_='Y'
	join KPI_ENGINE.dbo.PROCEDURES p on 
		cl.EMPI=p.EMPI and
		cl.Claim_id=p.Claim_id and 
		ISNULL(cl.SV_LINE,0)=ISNULL(p.SV_LINE,0) and 
		cl.CL_DATA_SRC=p.PROC_DATA_SRC and 
		p.PROC_CODE in('99221','99222','99223','99231','99232','99233')
	where 
		cl.POS='21'
	
	Create nonclustered index idx_HOSP_SPEC_PROCS on #HOSP_SPEC_PROCS(EMPI,ATT_NPI,PROV_SPEC_DESC,FROM_DATE)

	-- Identify Surgical Specialists visits during the stay
	
	
	Drop table if exists #HOSP_SPEC_VISITS
	Create table #HOSP_SPEC_VISITS
	(
		EMPI varchar(100),
		
		FROM_DATE Date,
		ATT_NPI varchar(20),
		CL_DATA_SRC varchar(50),
		PROV_NAME varchar(200),
		PROV_SPEC_DESC varchar(100),
		PROV_GRP_NAME varchar(200),
		ADM_DATE Date,
		DIS_DATE Date
	)
	Insert into #HOSP_SPEC_VISITS
	select distinct 
		v.*
		-- CR-2021-11-23 - Add Admission and DischargeDate for Inpatient Surgical Specialist
		,s.AdmissionDate
		,s.DischargeDate 
	from #HOSP_SPEC_PROCS v
	join #uhn_hospstays_directtransfers_normalized s on 
		v.EMPI=s.EMPI and 
		v.FROM_DATE between s.AdmissionDate and s.DischargeDate


	-- Apply Inpatient Surgical Specialist Attribution Logic

	Drop table if exists #InpatientSurgicalSpecialist
	Create table #InpatientSurgicalSpecialist
	(
		EMPI varchar(100),
		InpatientSurgicalSpecialist_NPI varchar(20),
		InpatientSurgicalSpecialist_Name varchar(200),
		InpatientSurgicalSpecialist_Specialty varchar(100),
		InpatientSurgicalSpecialist_Practice varchar(200),
		InpatientSurgicalSpecialist_VisitCount Int,
		InpatientSurgicalSpecialist_RecentVisit Date,
		AdmissionDate Date,
		DischargeDate Date,
		SpecialtyCount INT

	)
	Insert into #InpatientSurgicalSpecialist
	select distinct 
		EMPI
		,ATT_NPI
		,PROV_NAME
		,PROV_SPEC_DESC
		,PROV_GRP_NAME
		,ct as Vst_Ct
		,FROM_DATE as RCNT_VST 
		,ADM_DATE
		,DIS_DATE
		,DENSE_RANK() over(partition by EMPI order by ATT_NPI) as SpecialtyCount
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI,ATT_NPI,PROV_SPEC_DESC order by ct desc,rn desc) as rnk 
		from(
				select 
					*
					,count(*) over(partition by EMPI,ATT_NPI order by FROM_DATE desc) as ct
					,Case
						when CL_DATA_SRC='MSSP' Then 1
						when CL_DATA_SRC='HUMANA' Then 2
						when CL_DATA_SRC='CIGNA' Then 3
						when CL_DATA_SRC='BCBS-UHS' Then 4
						when CL_DATA_SRC='BRIGHT' Then 5
						when CL_DATA_SRC='MEDCO' Then 6
						when CL_DATA_SRC='FlatFiles' Then 7
					end as SourcePriority
					,ROW_NUMBER() over (partition by EMPI,DIS_DATE order by FROM_DATE desc) as rn
				from #HOSP_SPEC_VISITS
				
		)t2
	)t3 where rnk=1


	Create nonclustered index idx_InpatientSurgicalSpecialist on #InpatientSurgicalSpecialist(EMPI,SpecialtyCount)

			drop table if exists #uhn_attributiondoc;
			Select
				*
			into #uhn_attributiondoc
			From
			(
				SELECT 
					EMPI,
				   InpatientSurgicalSpecialist_NPI as NPI, 
				   InpatientSurgicalSpecialist_Name as Provider, 
				   InpatientSurgicalSpecialist_Specialty as Specialty,
				   InpatientSurgicalSpecialist_Practice as Practice,
					InpatientSurgicalSpecialist_RecentVisit as RecentVisit,
					InpatientSurgicalSpecialist_VisitCount as VisitCount,
					'IP_Surg_Specialist' as AttributionType,
					AdmissionDate,
					DischargeDate,
					SpecialtyCount as Rank
				From #InpatientSurgicalSpecialist
			
				Union all

				SELECT 
					EMPI,
				   HOSP_PCP_NPI as NPI, 
				   HOSP_PCP_NAME as Provider, 
				   HOSP_PCP_SPECIALTY as Specialty,
				   HOSP_PCP_PRACTICE as Practice,
					HOSP_PCP_RECENT_VISIT as RecentVisit,
					HOSP_PCP_VisitCount as VisitCount,
					'Hospitalist' as AttributionType,
					AdmissionDate,
					DischargeDate,
					ROW_NUMBER() over(Partition by EMPI order by dischargeDate) as Rank
				From #hosp_pcp_attribution

				Union all

				Select
					EMPI,
					AMB_SPEC_NPI as NPI,
					AMB_SPEC_NAME as Provider,
					AMB_SPEC_SPECIALTY as Specialty,
					AMB_SPEC_PRACTICE_NAME as Practice,
					AMB_SPEC_RECENT_VISIT as RecentVisit,
					AMB_SPEC_VISIT_COUNT as VisitCount,
					'Ambulatory_Specialist' as AttributionType,
					null as AdmissionDate,
					null as DischargeDate,
					SpecialistCount as Rank
				From #AMB_SPEC_ATTRIBUTION
			)i1


			
			
	Delete from RPT.AttributionDocument where ReportId=@reportId ;
	Insert into RPT.AttributionDocument(EMPI,attribution,ReportId)
	SELECT distinct 
		EMPI,
		(
			Select
				EMPI,
				NPI,
				Provider,
				Specialty,
				Practice,
				RecentVisit,
				VisitCount,
				AttributionType,
				ISNULL(NULLIF(cast(AdmissionDate as varchar(10)),'1900-01-01'),'') as AdmissionDate,
				ISNULL(NULLIF(cast(DischargeDate as varchar(10)),'1900-01-01'),'') as DischargeDate
			From #uhn_attributiondoc i1
			where i1.EMPI=i.EMPI
			FOR JSON PATH
		) as attribution,@reportId
	FROM #uhn_attributiondoc i


	

-- Creating output in the required Format
Drop table if exists #attribution_finaloutput;
CREATE TABLE #attribution_finaloutput(
	[EMPI] [varchar](100) NULL,
	[PayerId] [varchar](100) NULL,
	[AlternateMemberId] [varchar](100) NULL,
	[MedicareId] [varchar](100) NULL,
	[MemberLastName] [varchar](100) NULL,
	[MemberFirstName] [varchar](100) NULL,
	[MemberMiddleName] [varchar](50) NULL,
	[MemberDOB] [date] NULL,
	MEM_GENDER varchar(10) NULL,
	[EnrollmentStatus] [varchar](20) NULL,
	[AssignedStatus] [varchar](20) NULL,
	[DATA_SOURCE] [varchar](20) NULL,
	[AmbulatoryPCPNPI] [varchar](20) NULL,
	[AmbulatoryPCPName] [varchar](200) NULL,
	[AmbulatoryPCPSpecialty] [varchar](100) NULL,
	[AmbulatoryPCPPractice] [varchar](200) NULL,
	[AmbulatoryPCPRecentVisit] [date] NULL,
	[HospitalistPCPNPI] [varchar](20) NULL,
	[HospitalistPCPName] [varchar](200) NULL,
	[HospitalistPCPSpecialty] [varchar](100) NULL,
	[HospitalistPCPPractice] [varchar](200) NULL,
	[HospitalistPCPRecentVisit] [date] NULL,
	[AmbulatorySpecialistNPI_1] [varchar](20) NULL,
	[AmbulatorySpecialistName_1] [varchar](200) NULL,
	[AmbulatorySpecialistSpecialty_1] [varchar](100) NULL,
	[AmbulatorySpecialistPractice_1] [varchar](200) NULL,
	[AmbulatorySpecialistRecentVisit_1] [date] NULL,
	[AmbulatorySpecialistNPI_2] [varchar](20) NULL,
	[AmbulatorySpecialistName_2] [varchar](200) NULL,
	[AmbulatorySpecialistSpecialty_2] [varchar](100) NULL,
	[AmbulatorySpecialistPractice_2] [varchar](200) NULL,
	[AmbulatorySpecialistRecentVisit_2] [date] NULL,
	[AmbulatorySpecialistNPI_3] [varchar](20) NULL,
	[AmbulatorySpecialistName_3] [varchar](200) NULL,
	[AmbulatorySpecialistSpecialty_3] [varchar](100) NULL,
	[AmbulatorySpecialistPractice_3] [varchar](200) NULL,
	[AmbulatorySpecialistRecentVisit_3] [date] NULL,
	[AmbulatorySpecialistNPI_4] [varchar](20) NULL,
	[AmbulatorySpecialistName_4] [varchar](200) NULL,
	[AmbulatorySpecialistSpecialty_4] [varchar](100) NULL,
	[AmbulatorySpecialistPractice_4] [varchar](200) NULL,
	[AmbulatorySpecialistRecentVisit_4] [date] NULL,
	[AmbulatorySpecialistNPI_5] [varchar](20) NULL,
	[AmbulatorySpecialistName_5] [varchar](200) NULL,
	[AmbulatorySpecialistSpecialty_5] [varchar](100) NULL,
	[AmbulatorySpecialistPractice_5] [varchar](200) NULL,
	[AmbulatorySpecialistRecentVisit_5] [date] NULL,
	[InpatientSurgicalSpecialistNPI_1] [varchar](20) NULL,
	[InpatientSurgicalSpecialistName_1] [varchar](200) NULL,
	[InpatientSurgicalSpecialistSpecialty_1] [varchar](100) NULL,
	[InpatientSurgicalSpecialistPractice_1] [varchar](200) NULL,
	[InpatientSurgicalSpecialistRecentVisit_1] [date] NULL,
	[InpatientSurgicalSpecialistNPI_2] [varchar](20) NULL,
	[InpatientSurgicalSpecialistName_2] [varchar](200) NULL,
	[InpatientSurgicalSpecialistSpecialty_2] [varchar](100) NULL,
	[InpatientSurgicalSpecialistPractice_2] [varchar](200) NULL,
	[InpatientSurgicalSpecialistRecentVisit_2] [date] NULL,
	[InpatientSurgicalSpecialistNPI_3] [varchar](20) NULL,
	[InpatientSurgicalSpecialistName_3] [varchar](200) NULL,
	[InpatientSurgicalSpecialistSpecialty_3] [varchar](100) NULL,
	[InpatientSurgicalSpecialistPractice_3] [varchar](200) NULL,
	[InpatientSurgicalSpecialistRecentVisit_3] [date] NULL,
	[InpatientSurgicalSpecialistNPI_4] [varchar](20) NULL,
	[InpatientSurgicalSpecialistName_4] [varchar](200) NULL,
	[InpatientSurgicalSpecialistSpecialty_4] [varchar](100) NULL,
	[InpatientSurgicalSpecialistPractice_4] [varchar](200) NULL,
	[InpatientSurgicalSpecialistRecentVisit_4] [date] NULL,
	[InpatientSurgicalSpecialistNPI_5] [varchar](20) NULL,
	[InpatientSurgicalSpecialistName_5] [varchar](200) NULL,
	[InpatientSurgicalSpecialistSpecialty_5] [varchar](100) NULL,
	[InpatientSurgicalSpecialistPractice_5] [varchar](200) NULL,
	[InpatientSurgicalSpecialistRecentVisit_5] [date] NULL,
	[ROOT_COMPANIES_ID] [int] NOT NULL

) 

	Insert into #attribution_finaloutput(EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE,AmbulatoryPCPNPI,AmbulatoryPCPName,AmbulatoryPCPSpecialty,AmbulatoryPCPPractice,AmbulatoryPCPRecentVisit,HospitalistPCPNPI,HospitalistPCPName,HospitalistPCPSpecialty,HospitalistPCPPractice,HospitalistPCPRecentVisit,AmbulatorySpecialistNPI_1,AmbulatorySpecialistName_1,AmbulatorySpecialistSpecialty_1,AmbulatorySpecialistPractice_1,AmbulatorySpecialistRecentVisit_1,AmbulatorySpecialistNPI_2,AmbulatorySpecialistName_2,AmbulatorySpecialistSpecialty_2,AmbulatorySpecialistPractice_2,AmbulatorySpecialistRecentVisit_2,AmbulatorySpecialistNPI_3,AmbulatorySpecialistName_3,AmbulatorySpecialistSpecialty_3,AmbulatorySpecialistPractice_3,AmbulatorySpecialistRecentVisit_3,AmbulatorySpecialistNPI_4,AmbulatorySpecialistName_4,AmbulatorySpecialistSpecialty_4,AmbulatorySpecialistPractice_4,AmbulatorySpecialistRecentVisit_4,AmbulatorySpecialistNPI_5,AmbulatorySpecialistName_5,AmbulatorySpecialistSpecialty_5,AmbulatorySpecialistPractice_5,AmbulatorySpecialistRecentVisit_5,InpatientSurgicalSpecialistNPI_1,InpatientSurgicalSpecialistName_1,InpatientSurgicalSpecialistSpecialty_1,InpatientSurgicalSpecialistPractice_1,InpatientSurgicalSpecialistRecentVisit_1,InpatientSurgicalSpecialistNPI_2,InpatientSurgicalSpecialistName_2,InpatientSurgicalSpecialistSpecialty_2,InpatientSurgicalSpecialistPractice_2,InpatientSurgicalSpecialistRecentVisit_2,InpatientSurgicalSpecialistNPI_3,InpatientSurgicalSpecialistName_3,InpatientSurgicalSpecialistSpecialty_3,InpatientSurgicalSpecialistPractice_3,InpatientSurgicalSpecialistRecentVisit_3,InpatientSurgicalSpecialistNPI_4,InpatientSurgicalSpecialistName_4,InpatientSurgicalSpecialistSpecialty_4,InpatientSurgicalSpecialistPractice_4,InpatientSurgicalSpecialistRecentVisit_4,InpatientSurgicalSpecialistNPI_5,InpatientSurgicalSpecialistName_5,InpatientSurgicalSpecialistSpecialty_5,InpatientSurgicalSpecialistPractice_5,InpatientSurgicalSpecialistRecentVisit_5,ROOT_COMPANIES_ID)
	select 
		m.EMPI
		,m.PayerId
		,m.AlternateMemberId
		,m.Medicare_Id
		,m.MEMBER_LAST_NAME
		,m.MEMBER_FIRST_NAME
		,m.MEMBER_MIDDLE_NAME
		,m.MEMBER_DOB
		,m.MEM_GENDER
		,m.EnrollmentStatus
		,m.AssignedStatus
		,m.DATA_SOURCE
		,ISNULL(apcp.AMB_PCP_NPI,'') as AMB_PCP_NPI
		,ISNULL(AMB_PCP_NAME,'') as AMB_PCP_NAME
		,ISNULL(AMB_PCP_SPECIALTY,'') as AMB_PCP_SPECIALTY
		,ISNULL(AMB_PCP_PRACTICE,'') as AMB_PCP_PRACTICE
		,NULLIF(apcp.AMB_PCP_RECENT_VISIT,'1900-01-01') as AMB_PCP_RECENT_VISIT
		,ISNULL(hpcp.HOSP_PCP_NPI,'') as HOSP_PCP_NPI
		,ISNULL(hpcp.HOSP_PCP_NAME,'') as HOSP_PCP_NAME
		,ISNULL(hpcp.HOSP_PCP_SPECIALTY,'') as HOSP_PCP_SPECIALTY
		,ISNULL(hpcp.HOSP_PCP_PRACTICE,'') as HOSP_PCP_PRACTICE
		,NULLIF(hpcp.HOSP_PCP_RECENT_VISIT,'1900-01-01') as HOSP_PCP_RECENT_VISIT
		,ISNULL(asp1.AMB_SPEC_NPI,'') as AMB_SPEC_NPI_1
		,ISNULL(asp1.AMB_SPEC_NAME,'') as AMB_SPEC_NAME_1
		,ISNULL(asp1.AMB_SPEC_SPECIALTY,'') as AMB_SPEC_SPECIALTY_1
		,ISNULL(asp1.AMB_SPEC_PRACTICE_NAME,'') as AMB_SPEC_PRACTICE_NAME_1
		,NULLIF(asp1.AMB_SPEC_RECENT_VISIT,'1900-01-01') as AMB_SPEC_RECENT_VISIT_1
		,ISNULL(asp2.AMB_SPEC_NPI,'') as AMB_SPEC_NPI_2
		,ISNULL(asp2.AMB_SPEC_NAME,'') as AMB_SPEC_NAME_2
		,ISNULL(asp2.AMB_SPEC_SPECIALTY,'') as AMB_SPEC_SPECIALTY_2
		,ISNULL(asp2.AMB_SPEC_PRACTICE_NAME,'') as AMB_SPEC_PRACTICE_NAME_2
		,NULLIF(asp2.AMB_SPEC_RECENT_VISIT,'1900-01-01') as AMB_SPEC_RECENT_VISIT_2
		,ISNULL(asp3.AMB_SPEC_NPI,'') as AMB_SPEC_NPI_3
		,ISNULL(asp3.AMB_SPEC_NAME,'') as AMB_SPEC_NAME_3
		,ISNULL(asp3.AMB_SPEC_SPECIALTY,'') as AMB_SPEC_SPECIALTY_3
		,ISNULL(asp3.AMB_SPEC_PRACTICE_NAME,'') as AMB_SPEC_PRACTICE_NAME_3
		,NULLIF(asp3.AMB_SPEC_RECENT_VISIT,'1900-01-01') as AMB_SPEC_RECENT_VISIT_3
		,ISNULL(asp4.AMB_SPEC_NPI,'') as AMB_SPEC_NPI_4
		,ISNULL(asp4.AMB_SPEC_NAME,'') as AMB_SPEC_NAME_4
		,ISNULL(asp4.AMB_SPEC_SPECIALTY,'') as AMB_SPEC_SPECIALTY_4
		,ISNULL(asp4.AMB_SPEC_PRACTICE_NAME,'') as AMB_SPEC_PRACTICE_NAME_4
		,NULLIF(asp4.AMB_SPEC_RECENT_VISIT,'1900-01-01') as AMB_SPEC_RECENT_VISIT_4
		,ISNULL(asp5.AMB_SPEC_NPI,'') as AMB_SPEC_NPI_5
		,ISNULL(asp5.AMB_SPEC_NAME,'') as AMB_SPEC_NAME_5
		,ISNULL(asp5.AMB_SPEC_SPECIALTY,'') as AMB_SPEC_SPECIALTY_5
		,ISNULL(asp5.AMB_SPEC_PRACTICE_NAME,'') as AMB_SPEC_PRACTICE_NAME_5
		,NULLIF(asp5.AMB_SPEC_RECENT_VISIT,'1900-01-01') as AMB_SPEC_RECENT_VISIT_5
		,ISNULL(iss1.InpatientSurgicalSpecialist_NPI,'') as InpatientSurgicalSpecialist_NPI_1
		,ISNULL(iss1.InpatientSurgicalSpecialist_Name,'') as InpatientSurgicalSpecialist_Name_1
		,ISNULL(iss1.InpatientSurgicalSpecialist_Specialty,'') as InpatientSurgicalSpecialist_Specialty_1
		,ISNULL(iss1.InpatientSurgicalSpecialist_Practice,'') as InpatientSurgicalSpecialist_Practice_1
		,NULLIF(iss1.InpatientSurgicalSpecialist_RecentVisit,'1900-01-01') as InpatientSurgicalSpecialist_RecentVisit_1
		,ISNULL(iss2.InpatientSurgicalSpecialist_NPI,'') as InpatientSurgicalSpecialist_NPI_2
		,ISNULL(iss2.InpatientSurgicalSpecialist_Name,'') as InpatientSurgicalSpecialist_Name_2
		,ISNULL(iss2.InpatientSurgicalSpecialist_Specialty,'') as InpatientSurgicalSpecialist_Specialty_2
		,ISNULL(iss2.InpatientSurgicalSpecialist_Practice,'') as InpatientSurgicalSpecialist_Practice_2
		,NULLIF(iss2.InpatientSurgicalSpecialist_RecentVisit,'1900-01-01') as InpatientSurgicalSpecialist_RecentVisit_2
		,ISNULL(iss3.InpatientSurgicalSpecialist_NPI,'') as InpatientSurgicalSpecialist_NPI_3
		,ISNULL(iss3.InpatientSurgicalSpecialist_Name,'') as InpatientSurgicalSpecialist_Name_3
		,ISNULL(iss3.InpatientSurgicalSpecialist_Specialty,'') as InpatientSurgicalSpecialist_Specialty_3
		,ISNULL(iss3.InpatientSurgicalSpecialist_Practice,'') as InpatientSurgicalSpecialist_Practice_3
		,NULLIF(iss3.InpatientSurgicalSpecialist_RecentVisit,'1900-01-01') as InpatientSurgicalSpecialist_RecentVisit_3
		,ISNULL(iss4.InpatientSurgicalSpecialist_NPI,'') as InpatientSurgicalSpecialist_NPI_4
		,ISNULL(iss4.InpatientSurgicalSpecialist_Name,'') as InpatientSurgicalSpecialist_Name_4
		,ISNULL(iss4.InpatientSurgicalSpecialist_Specialty,'') as InpatientSurgicalSpecialist_Specialty_4
		,ISNULL(iss4.InpatientSurgicalSpecialist_Practice,'') as InpatientSurgicalSpecialist_Practice_4
		,NULLIF(iss4.InpatientSurgicalSpecialist_RecentVisit,'1900-01-01') as InpatientSurgicalSpecialist_RecentVisit_4
		,ISNULL(iss5.InpatientSurgicalSpecialist_NPI,'') as InpatientSurgicalSpecialist_NPI_5
		,ISNULL(iss5.InpatientSurgicalSpecialist_Name,'') as InpatientSurgicalSpecialist_Name_5
		,ISNULL(iss5.InpatientSurgicalSpecialist_Specialty,'') as InpatientSurgicalSpecialist_Specialty_5
		,ISNULL(iss5.InpatientSurgicalSpecialist_Practice,'') as InpatientSurgicalSpecialist_Practice_5
		,NULLIF(iss5.InpatientSurgicalSpecialist_RecentVisit,'1900-01-01') as InpatientSurgicalSpecialist_RecentVisit_5
		,@rootId
	from #members m
	left outer join #amb_pcp_attribution apcp on m.EMPI=apcp.EMPI
	left outer join #hosp_pcp_attribution hpcp on m.EMPI=hpcp.EMPI and rank=1
	left outer join #AMB_SPEC_ATTRIBUTION asp1 on m.EMPI=asp1.EMPI and asp1.SpecialistCount=1
	left outer join #AMB_SPEC_ATTRIBUTION asp2 on m.EMPI=asp2.EMPI and asp2.SpecialistCount=2
	left outer join #AMB_SPEC_ATTRIBUTION asp3 on m.EMPI=asp3.EMPI and asp3.SpecialistCount=3
	left outer join #AMB_SPEC_ATTRIBUTION asp4 on m.EMPI=asp4.EMPI and asp4.SpecialistCount=4
	left outer join #AMB_SPEC_ATTRIBUTION asp5 on m.EMPI=asp5.EMPI and asp5.SpecialistCount=5
	left outer join #InpatientSurgicalSpecialist iss1 on m.EMPI=iss1.EMPI and iss1.SpecialtyCount=1
	left outer join #InpatientSurgicalSpecialist iss2 on m.EMPI=iss2.EMPI and iss2.SpecialtyCount=2
	left outer join #InpatientSurgicalSpecialist iss3 on m.EMPI=iss3.EMPI and iss3.SpecialtyCount=3
	left outer join #InpatientSurgicalSpecialist iss4 on m.EMPI=iss4.EMPI and iss4.SpecialtyCount=4
	left outer join #InpatientSurgicalSpecialist iss5 on m.EMPI=iss5.EMPI and iss5.SpecialtyCount=5

-- Assign all unattributed Provider to default
	
Update #attribution_finaloutput set AmbulatoryPCPNPI='1111111111',AmbulatoryPCPName='Unattributed, UHN MD',AmbulatoryPCPSpecialty='Family Medicine',AmbulatoryPCPPractice='UHN Unattributed Practice' where AmbulatoryPCPNPI=''




-- Get the Report Id

		Delete from KPI_DEV.RPT.PCP_ATTRIBUTION where ReportId=@reportId and ROOT_COMPANIES_ID=@rootId;

		Insert into KPI_DEV.RPT.PCP_ATTRIBUTION(EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE,AmbulatoryPCPNPI,AmbulatoryPCPName,AmbulatoryPCPSpecialty,AmbulatoryPCPPractice,AmbulatoryPCPRecentVisit,HospitalistPCPNPI,HospitalistPCPName,HospitalistPCPSpecialty,HospitalistPCPPractice,HospitalistPCPRecentVisit,AmbulatorySpecialistNPI_1,AmbulatorySpecialistName_1,AmbulatorySpecialistSpecialty_1,AmbulatorySpecialistPractice_1,AmbulatorySpecialistRecentVisit_1,AmbulatorySpecialistNPI_2,AmbulatorySpecialistName_2,AmbulatorySpecialistSpecialty_2,AmbulatorySpecialistPractice_2,AmbulatorySpecialistRecentVisit_2,AmbulatorySpecialistNPI_3,AmbulatorySpecialistName_3,AmbulatorySpecialistSpecialty_3,AmbulatorySpecialistPractice_3,AmbulatorySpecialistRecentVisit_3,AmbulatorySpecialistNPI_4,AmbulatorySpecialistName_4,AmbulatorySpecialistSpecialty_4,AmbulatorySpecialistPractice_4,AmbulatorySpecialistRecentVisit_4,AmbulatorySpecialistNPI_5,AmbulatorySpecialistName_5,AmbulatorySpecialistSpecialty_5,AmbulatorySpecialistPractice_5,AmbulatorySpecialistRecentVisit_5,InpatientSurgicalSpecialistNPI_1,InpatientSurgicalSpecialistName_1,InpatientSurgicalSpecialistSpecialty_1,InpatientSurgicalSpecialistPractice_1,InpatientSurgicalSpecialistRecentVisit_1,InpatientSurgicalSpecialistNPI_2,InpatientSurgicalSpecialistName_2,InpatientSurgicalSpecialistSpecialty_2,InpatientSurgicalSpecialistPractice_2,InpatientSurgicalSpecialistRecentVisit_2,InpatientSurgicalSpecialistNPI_3,InpatientSurgicalSpecialistName_3,InpatientSurgicalSpecialistSpecialty_3,InpatientSurgicalSpecialistPractice_3,InpatientSurgicalSpecialistRecentVisit_3,InpatientSurgicalSpecialistNPI_4,InpatientSurgicalSpecialistName_4,InpatientSurgicalSpecialistSpecialty_4,InpatientSurgicalSpecialistPractice_4,InpatientSurgicalSpecialistRecentVisit_4,InpatientSurgicalSpecialistNPI_5,InpatientSurgicalSpecialistName_5,InpatientSurgicalSpecialistSpecialty_5,InpatientSurgicalSpecialistPractice_5,InpatientSurgicalSpecialistRecentVisit_5,ReportId,ROOT_COMPANIES_ID)
	select EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE,AmbulatoryPCPNPI,AmbulatoryPCPName,AmbulatoryPCPSpecialty,AmbulatoryPCPPractice,AmbulatoryPCPRecentVisit,HospitalistPCPNPI,HospitalistPCPName,HospitalistPCPSpecialty,HospitalistPCPPractice,HospitalistPCPRecentVisit,AmbulatorySpecialistNPI_1,AmbulatorySpecialistName_1,AmbulatorySpecialistSpecialty_1,AmbulatorySpecialistPractice_1,AmbulatorySpecialistRecentVisit_1,AmbulatorySpecialistNPI_2,AmbulatorySpecialistName_2,AmbulatorySpecialistSpecialty_2,AmbulatorySpecialistPractice_2,AmbulatorySpecialistRecentVisit_2,AmbulatorySpecialistNPI_3,AmbulatorySpecialistName_3,AmbulatorySpecialistSpecialty_3,AmbulatorySpecialistPractice_3,AmbulatorySpecialistRecentVisit_3,AmbulatorySpecialistNPI_4,AmbulatorySpecialistName_4,AmbulatorySpecialistSpecialty_4,AmbulatorySpecialistPractice_4,AmbulatorySpecialistRecentVisit_4,AmbulatorySpecialistNPI_5,AmbulatorySpecialistName_5,AmbulatorySpecialistSpecialty_5,AmbulatorySpecialistPractice_5,AmbulatorySpecialistRecentVisit_5,InpatientSurgicalSpecialistNPI_1,InpatientSurgicalSpecialistName_1,InpatientSurgicalSpecialistSpecialty_1,InpatientSurgicalSpecialistPractice_1,InpatientSurgicalSpecialistRecentVisit_1,InpatientSurgicalSpecialistNPI_2,InpatientSurgicalSpecialistName_2,InpatientSurgicalSpecialistSpecialty_2,InpatientSurgicalSpecialistPractice_2,InpatientSurgicalSpecialistRecentVisit_2,InpatientSurgicalSpecialistNPI_3,InpatientSurgicalSpecialistName_3,InpatientSurgicalSpecialistSpecialty_3,InpatientSurgicalSpecialistPractice_3,InpatientSurgicalSpecialistRecentVisit_3,InpatientSurgicalSpecialistNPI_4,InpatientSurgicalSpecialistName_4,InpatientSurgicalSpecialistSpecialty_4,InpatientSurgicalSpecialistPractice_4,InpatientSurgicalSpecialistRecentVisit_4,InpatientSurgicalSpecialistNPI_5,InpatientSurgicalSpecialistName_5,InpatientSurgicalSpecialistSpecialty_5,InpatientSurgicalSpecialistPractice_5,InpatientSurgicalSpecialistRecentVisit_5,@reportId,ROOT_COMPANIES_ID from #attribution_finaloutput

	

	
	-- Pull the list of unattributed Members and assign Physician Specialty and Facility

	Drop table if exists #uhn_nonattributedmembers
	Create table #uhn_nonattributedmembers
	(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		FROM_DATE DATE,
		ATT_NPI varchar(20),
		ATT_PROV_NAME varchar(200),
		Specialty varchar(100),
		CL_DATA_SRC varchar(50),
		BILL_PROV varchar(200),
		BILL_PROV_NAME varchar(200),
		BILL_PROV_TIN varchar(20)
	)
	Insert into #uhn_nonattributedmembers
	select distinct 
		af.EMPI
		,af.PayerId
		,c.FROM_DATE
		,c.ATT_NPI
		,Case
			When n.Provider_last_name!='' then Concat(n.Provider_last_name,' ',n.Provider_First_Name)
			When ATT_PROV_NAME!='' and ATT_PROV_NAME is not null then ATT_PROV_NAME
			else ''
		end as ATT_PROV_NAME
		,n.PrimarySpecialty
		,c.CL_DATA_SRC
		,c.BILL_PROV
		,Case
			When bn.Provider_organization_name!='' then bn.Provider_organization_name
			When c.BILL_PROV_NAME is not null or c.BILL_PROV_NAME!='' then c.BILL_PROV_NAME
			when bn.Provider_last_name!='' and bn.Provider_last_name is not null then concat(bn.Provider_last_name,' ',bn.Provider_First_Name)
			else ''
		end as BILL_PROV_NAME
		,c.BILL_PROV_TIN
	from #attribution_finaloutput af
	left outer join #amb_pcp_vsts c on af.EMPI=c.EMPI
	left outer join #amb_pcp_procs p on
		c.CLAIM_ID=p.CLAIM_ID and 
		c.CL_DATA_SRC=p.PROC_DATA_SRC and 
		p.EMPI=c.EMPI
	left outer Join KPI_ENGINE.DBO.RFT_NPI n on
		c.ATT_NPI=n.NPI and
		n.Entity_Type_Code=1
	left outer Join KPI_ENGINE.DBO.RFT_NPI bn on
		c.BILL_PROV=bn.NPI 
	
	
	



	-- Apply PCP logic for out of network physicians
	Drop table if exists #uhn_unattributed_attribution
	Create table #uhn_unattributed_attribution
	(
		EMPI varchar(100),
		MEMBER_ID varchar(100),
		NPI varchar(20),
		Provider varchar(200),
		Specialty varchar(200),
		Practice varchar(200),
		VisitCount INT,
		RECENT_VISIT Date,
		DATA_SOURCE varchar(50),
		BillingNPI varchar(20),
		BillingProviderTIN varchar(20)

	)
	Insert into #uhn_unattributed_attribution
	select distinct 
		EMPI
		,Member_id
		,ATT_NPI
		,ATT_PROV_NAME
		,Specialty
		,BILL_PROV_NAME
		,twoyrct as vst_ct
		,FROM_DATE as RCNT_VST 
		,CL_DATA_SRC
		,BILL_PROV
		,BILL_PROV_TIN
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by twoyrct desc,oneyrct desc,from_date desc,SourcePriority Asc,rn desc) as rnk 
		from(
				select 
					*
					,count(*) over(partition by EMPI,ATT_NPI,BILL_PROV_NAME) as twoyrct
					,count(case when FROM_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@ce_enddt)), 0) and @ce_enddt then 1 else null end) over(partition by EMPI,ATT_NPI,BILL_PROV_NAME) as oneyrct 
					,Case 
						when CL_DATA_SRC='MSSP' then 1
						when CL_DATA_SRC='HUMANA' then 2
						when CL_DATA_SRC='CIGNA' then 3
						when CL_DATA_SRC='BCBS-UHS' then 4
						when CL_DATA_SRC='BRIGHT' then 5
						when CL_DATA_SRC='MEDCO' then 6
					end as SourcePriority
					,ROW_NUMBER() over(partition by EMPI order by FROM_DATE) as rn
				from #uhn_nonattributedmembers
			
		)t1
	)t2 where rnk=1
	


	Delete from RPT.UHN_UnattributedMembers where ROOT_COMPANIES_ID=@rootId

	Insert into RPT.UHN_UnattributedMembers(EMPI,PayerId,Payer,Medicare_Id,MEMBER_LAST_NAME,MEMBER_FIRST_NAME,MEMBER_MIDDLE_NAME,MEMBER_DOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,NPI,Provider,Specialty,Practice,BillingNPI,BillingProviderTIN,ROOT_COMPANIES_ID,ReportId) 
	Select 
		u.EMPI
		,m.PayerId
		,m.DATA_SOURCE as Payer
		,m.Medicare_Id
		,m.MEMBER_LAST_NAME
		,m.MEMBER_FIRST_NAME
		,m.MEMBER_MIDDLE_NAME
		,m.MEMBER_DOB
		,m.MEM_GENDER
		,m.EnrollmentStatus
		,m.AssignedStatus
		,u.NPI
		,u.Provider
		,u.Specialty
		,u.Practice
		,u.BillingNPI
		,ISNULL(u.BillingProviderTIN,'') as BillingProviderTIN
		,@rootId as ROOT_COMPANIES_ID
		,@reportId as ReportId
	from #uhn_unattributed_attribution u
	join #members m on
		u.EMPI=m.EMPI
	
SET @counter += 1;

END


-- Move attribution data to Consolidated attribution Snapshot
------Truncating Table
Truncate table [KPI_DEV].[RPT].[ConsolidatedAttribution_MultiYearSnapshot]



------Dropping Index Before Insertion-----

DROP INDEX [idx_Consolidated_attribution_snapshot_EMPI] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot] WITH ( ONLINE = OFF )

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_NPI] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]




DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_Practice] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_Prov_Name] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_root_companies_id] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_Specialty] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]

--------------Inserting the latest report data-----------------------
INSERT INTO [KPI_DEV].[RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
[EMPI]
      ,[PayerId]
      ,[AlternateMemberId]
      ,[MedicareId]
      ,[MemberLastName]
      ,[MemberFirstName]
      ,[MemberMiddleName]
      ,[MemberDOB]
      ,[MEM_GENDER]
      ,[EnrollmentStatus]
      ,[AssignedStatus]
      ,[Payer]
      ,[Attribution_Type]
      ,[Rank]
      ,[NPI]
      ,[Prov_Name]
      ,[Specialty]
      ,[Practice]
      ,[RecentVisit]
	  /*CR-2021-11-24|KPISQL-537 - Adding admission and discharge dates for Hospitalist and Inpatient Specialist*/
	  ,attr_AdmissionDate
	  ,attr_DischargeDate
      ,[ReportId]
      ,[LoadDate]
      ,[ReportEndDate]
      ,[ROOT_COMPANIES_ID]
      ,[ReportQuarter]
)
SELECT [EMPI]
      ,[PayerId]
      ,[AlternateMemberId]
      ,[MedicareId]
      ,[MemberLastName]
      ,[MemberFirstName]
      ,[MemberMiddleName]
      ,[MemberDOB]
      ,[MEM_GENDER]
      ,[EnrollmentStatus]
      ,[AssignedStatus]
      ,[Payer]
      ,[Attribution_Type]
      ,[Rank]
      ,[NPI]
      ,[Prov_Name]
      ,[Specialty]
      ,[Practice]
      ,[RecentVisit]
	  ,AdmissionDate
	  ,DischargeDate
      ,[ReportId]
      ,[LoadDate]
      ,[ReportEndDate]
      ,[ROOT_COMPANIES_ID]
      ,[ReportQuarter]
  FROM [KPI_DEV].[RPT].[ConsolidatedAttribution_MultiYear]
  where ISNULL(AssignedStatus,'Assigned')='Assigned'

---------------------------------------------------------------------

------Recreating Index After Insertion-----
CREATE CLUSTERED INDEX [idx_Consolidated_attribution_snapshot_EMPI] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
	[EMPI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_NPI] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
	[NPI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_Practice] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
	[Practice] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_Prov_Name] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
	[Prov_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_Specialty] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
	[Specialty] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_root_companies_id] ON [RPT].[ConsolidatedAttribution_MultiYearSnapshot]
(
	[ROOT_COMPANIES_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]


/*
-- Map missing physicians to Provider_scorecard_Mail

Insert into RPT.Provider_Scorecard_Mail(Practice,Specialty,Provider,ProviderNPI,midlevel)
select Prov_Grp_name,Prov_Spec_desc2,Prov_name,PROV_NPI,case when _Prov_UDF_01_='midlevel' then 1 else 0 end from Provider where _PROV_UDF_02_='y' and _PROV_UDF_04_='Primary Address'
except
select Practice,Specialty,Provider,ProviderNPI,Midlevel from RPT.Provider_Scorecard_Mail
*/

/* Shanawaz - 10/01/2021 - Adding logic to Assign members to practice if the providers they were assigned to have left the Incentive program*/



Update a set Practice=t2.AmbulatoryPCPPractice
from RPT.[ConsolidatedAttribution_MultiYearSnapshot] a
join
(
	Select EMPI,AmbulatoryPCPPractice from(
Select
	a.EMPI,a.DATA_SOURCE,a.MemberLastName,a.MemberFirstName,a.MemberDOB,a.EnrollmentStatus,la.AmbulatoryPCPNPI,la.AmbulatoryPCPName,la.AmbulatoryPCPPractice,la.AmbulatoryPCPSpecialty,la.AmbulatoryPCPRecentVisit
from KPI_DEV.RPT.PCP_ATTRIBUTION a
Join
(
select * from(
Select *,ROW_NUMBER() over(Partition By EMPI order by ReportId desc) as rn from KPI_DEV.RPT.PCP_ATTRIBUTION where AmbulatoryPCPNPI!='1111111111'
)t1
where
	rn=1
)la on a.EMPI=la.EMPI
Left outer Join KPI_ENGINE..PROVIDER p on
	la.AmbulatoryPCPNPI=p.PROV_NPI
	
where
	a.AmbulatoryPCPNPI='1111111111'
	and a.ReportId=@reportId
	and p._PROV_UDF_02_!='Y' and _PROV_UDF_04_='Primary Address'
	)t1
)t2 on a.EMPI=t2.EMPI
where
	a.Attribution_Type='Ambulatory_PCP' and a.NPI='1111111111'


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC KPI_ENGINE.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
