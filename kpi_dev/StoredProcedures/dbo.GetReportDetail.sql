SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE   proc [dbo].[GetReportDetail] @rundate Date ,@rootId INT,@startDate Date Output,@enddate Date output,@reportId INT output,@quarter varchar(10) output
As

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC kpi_engine.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


Set @reportId=0
Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,@rundate)), 0) 
Set @enddate=eomonth(Dateadd(month,-2,@rundate))
Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))


Select @reportId=ReportId from RPT.Report_Details where ReportQuarter=@quarter;

if(@reportId=0)
Begin

	Insert into RPT.Report_Details(ReportStartDate,ReportEndDate,ReportQuarter,Root_Companies_id) values(@startDate,@enddate,@quarter,@rootId);

End
Else
Begin
	
	Update RPT.Report_Details Set ReportStartDate=@startDate,ReportEndDate=@enddate,ReportDateTime=GETDATE() where ReportId=@reportId;
End


Select @reportId=ReportId from RPT.Report_Details where ReportQuarter=@quarter;


SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC kpi_engine.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC kpi_engine.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC kpi_engine.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
