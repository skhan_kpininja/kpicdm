SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE       PROCEDURE [dbo].[usp_RunMeasureProcessingNew]
AS
BEGIN
	
	DECLARE 
		@rundate DATE
		,@StoredProcedureName VARCHAR(500)
		,@Counter int = 1
		,@Loops int
		,@ProcessingYear int
		,@Command Nvarchar(max)
		,@Error VARCHAR(MAX)

	IF NOT EXISTS (SELECT * FROM KPI_DEV.[RFT].[MeasureYearMapping] WHERE ProcessingYear=YEAR(GETDATE()))
	BEGIN
		INSERT INTO KPI_DEV.[RFT].[MeasureYearMapping]
		(
			MeasureId	
			,StoredProcedureName	
			,ProcessingYear	
			,Target	
			,Weight	
			,ProcessingStartDate	
			,ProcessingEndDate	
			,Active
		)
		SELECT
			MYP.Measureid
			,MYP.StoredProcedureName
			,YEAR(GETDATE())
			,MYP.Target
			,MYP.Weight
			,DATEADD(YEAR,1,ProcessingStartDate)
			,DATEADD(YEAR,1,ProcessingEndDate)
			,1
		FROM KPI_DEV.[RFT].[MeasureYearMapping] MYP
		WHERE 
			MYP.ProcessingYear = (SELECT MAX(ProcessingYear) FROM KPI_DEV.[RFT].[MeasureYearMapping])
	END

	--START: For Temporary Logging

		IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = 'MeasureRunLog')
		BEGIN
			CREATE TABLE DBO.MeasureRunLog
			(
				Id INT IDENTITY(1,1)
				,StoredProcedureName varchar(200)
				,RunStatus varchar(10)
				,Details varchar(max)
				,LogTimeStamp datetime
			)
		END

	--END: For Temporary Logging

	DROP TABLE IF EXISTS #TempMeasureYearMapping

	CREATE TABLE #TempMeasureYearMapping
	(
		RunSeq int identity(1,1)
		,StoredProcedureName VARCHAR(500)
		,ProcessingYear INT
		,ProcessingStartDate DATE
		,ProcessingEndDate DATE
	)

	INSERT INTO #TempMeasureYearMapping
	SELECT DISTINCT
		StoredProcedureName	
		,ProcessingYear
		,ProcessingStartDate	
		,ProcessingEndDate
	FROM
		kpi_dev.[RFT].[MeasureYearMapping]
	WHERE 
		GETDATE() BETWEEN ProcessingStartDate AND ProcessingEndDate
		AND StoredProcedureName IS NOT NULL
		AND Active = 1
		AND RunFlag = 1

	SET
		@Loops = (SELECT COUNT(*) FROM #TempMeasureYearMapping)

	WHILE @Counter <= @Loops
	BEGIN
		SET @StoredProcedureName = (SELECT StoredProcedureName FROM #TempMeasureYearMapping WHERE RunSeq = @Counter)
		SET @ProcessingYear = (SELECT ProcessingYear FROM #TempMeasureYearMapping WHERE RunSeq = @Counter)
		SET @rundate = 
				CASE 
					WHEN @ProcessingYear < YEAR(GETDATE()) THEN CAST(CAST(@ProcessingYear AS VARCHAR(10))+'-12-31' AS DATE)
					ELSE GETDATE()
				END
		SET @Command = 'EXEC '+@StoredProcedureName+' '''+CAST(@rundate AS VARCHAR(20))+''';'

		PRINT @Command
		SELECT @Command

		--BEGIN TRY

		--	INSERT INTO DBO.MeasureRunLog
		--	(StoredProcedureName, RunStatus, Details, LogTimeStamp)
		--	SELECT
		--		@StoredProcedureName
		--		,'Started'
		--		,'SP Execution Started for processing year: '+cast(@ProcessingYear AS varchar(5))+'. Full Command: '+@Command
		--		,getdate()

		--	EXECUTE SP_EXECUTESQL @Command;

		--	INSERT INTO DBO.MeasureRunLog
		--	(StoredProcedureName, RunStatus, Details, LogTimeStamp)
		--	SELECT
		--		@StoredProcedureName
		--		,'Completed'
		--		,'SP Execution Completed for processing year: '+cast(@ProcessingYear AS varchar(5))+'.'
		--		,getdate()

		--END TRY

		--BEGIN CATCH

		--	SET @Error = ERROR_MESSAGE();

		--	INSERT INTO DBO.MeasureRunLog
		--	(StoredProcedureName, RunStatus, Details, LogTimeStamp)
		--	SELECT
		--		@StoredProcedureName
		--		,'Failed'
		--		,'SP Execution Failed for processing year: '+cast(@ProcessingYear AS varchar(5))+', with Error: '+@Error
		--		,getdate()

		--END CATCH

		DELETE #TempMeasureYearMapping
		WHERE RunSeq = @Counter;

		SET
			@Counter += 1;

	END

	UPDATE kpi_dev.RFT.[MeasureYearMapping]
	SET 
		kpi_dev.RFT.[MeasureYearMapping].LatestReportId = a.ReportId
		,kpi_dev.RFT.[MeasureYearMapping].LatestReportQuarter = a.ReportQuarter
	FROM kpi_dev.RFT.[MeasureYearMapping]
	JOIN 
		(
			SELECT
				B.ReportId
				,RPT.Report_Details.ReportQuarter
				,ProcessingYear
			FROM kpi_dev.RPT.Report_Details
			JOIN
			(
				SELECT DISTINCT
					MAX(ReportId) OVER (PARTITION BY SUBSTRING(Report_Details.ReportQuarter,1,4) ORDER BY SUBSTRING(Report_Details.ReportQuarter,1,4) DESC) AS ReportId
					,SUBSTRING(Report_Details.ReportQuarter,1,4) AS ProcessingYear 
				FROM kpi_dev.RPT.Report_Details
			)B ON kpi_dev.RPT.Report_Details.ReportId = B.ReportId
		)A ON [MeasureYearMapping].ProcessingYear = A.ProcessingYear

	UPDATE KPI_DEV.[RFT].[MeasureYearMapping]
	SET RunFlag = 1
	WHERE GETDATE() BETWEEN ProcessingStartDate AND ProcessingEndDate

END

GO
