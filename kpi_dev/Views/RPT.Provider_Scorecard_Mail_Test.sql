SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE view [RPT].[Provider_Scorecard_Mail_Test] as 


SELECT 
	P.PROV_GRP_NAME AS Practise
	,Prov_Spec_desc2 as Specialty
	,Prov_Name as Provider
	,NULL as Report_Type
	,NULL as Report_Quarter
	,EmailAddress as Email_Id
	,UserFullName as UserName
	,PROV_NPI as ProviderNPI
	,159 as ROOT_COMPANIES_ID
	,case when _PROV_UDF_01_='mid-level' then 1 else 0 end AS midlevel
FROM 
	[kpi_dev].[RPT].[Provider_Scorecard_Users] U
	JOIN KPI_ENGINE..PROVIDER P ON U.Practice = P.PROV_GRP_NAME
WHERE 1=1
	AND role like '%PRactice%'
	AND _PROV_UDF_02_='y' 
	AND _PROV_UDF_04_='Primary Address' 
	AND U.ISDELETED = 0

UNION ALL

SELECT 
	P.PROV_GRP_NAME AS Practise
	,Prov_Spec_desc2 as Specialty
	,Prov_Name as Provider
	,NULL as Report_Type
	,NULL as Report_Quarter
	,EmailAddress as Email_Id
	,UserFullName as UserName
	,PROV_NPI as ProviderNPI
	,159 as ROOT_COMPANIES_ID
	,case when _PROV_UDF_01_='mid-level' then 1 else 0 end AS midlevel
FROM 
	[kpi_dev].[RPT].[Provider_Scorecard_Users] U
	JOIN KPI_ENGINE..PROVIDER P ON U.ProviderNPI = P.PROV_NPI
WHERE 1=1
	AND role like '%Provider%' 
	AND _PROV_UDF_02_='y' 
	AND _PROV_UDF_04_='Primary Address' 
	AND U.ISDELETED = 0
;

GO
